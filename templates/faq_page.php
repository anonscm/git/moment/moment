<?php 

$faq = new Faq();
?>
        
<section class="row" data-component="faq">
    <h2 class="text-center">
        <i class='fa fa-question-circle'> 
            <span>
                <?php echo Lang::tr('faq_title'); ?>
            </span>
        </i>
    </h2>
    <section>
        <ul class="accordion" data-accordion data-allow-all-closed="true">
              <?php 
                $cpt = 1;
                foreach ($faq->content as $content){
                    echo '<li class="accordion-item" data-accordion-item>';
                    echo '<a href="#answer'.$cpt.'" class="accordion-title">';
                    echo $cpt.'.&nbsp;'.$content['question'];
                    echo '</a>';
                    
                    echo '<div id="answer'.$cpt.'" class="accordion-content" data-tab-content>';
                    echo $content['answer'];
                    echo '</div>';
                    echo '</li>';

                    $cpt++;
                }
            ?>
        </ul>
    </section>
</section>
    