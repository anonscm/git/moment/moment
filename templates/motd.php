

<!-- This is the first modal -->
<div class="reveal" id="motd-modal" data-reveal data-animation-in="fade-in" data-animation-out="fade-out" data-multiple-opened="true">
    
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>

    <hr />
    <button data-close aria-label="Close modal" data-context="close_modal" id="motd_do_not_show_again" class="button" >{tr:i_got_it}</button>
</div>