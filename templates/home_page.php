<article class="small-12 text-center columns">
    {tr:site_title}
</article>
<article class="row disclamer text-center">
    {tr:home_page_disclamer}
    <?php if(!Auth::isAuthenticated()) { ?>
    <p>
        {tr:please_login}
    </p>
    <?php
        if (Config::get('use_wayf')) {
            Template::display('wayf_small');
        } else { ?>
            <a href="<?php echo AuthSP::logonURL(); ?>" class="show-for-small-only" data-tooltip="" title="{tr:log_in_tip}">
                <span class="survey_button button">{tr:logon}</span>
            </a>
        <?php }
    }
    ?>
</article>

<?php
if (Auth::isAuthenticated()) {
    Template::display('page_menu');
}
?>

