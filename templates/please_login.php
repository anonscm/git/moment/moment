<section class='row' data-component="login">
    <article class="error_http text-center">
        <img src="{img:disconnected_user_onpage.png}" />
        <div class="message">
            <i class="fa fa-info-circle"></i><?php if(isset($page) && $page === 'user') { ?>{tr:please_login_to_access_user_page}<?php } else { ?>{tr:please_login}<?php } ?>
        </div>
        <?php
        if (!Auth::isAuthenticated()) {
            if (Config::get('use_wayf')) {
                Template::display('wayf_small');
            } else { ?>
        <div class="login_container">
            <a href="<?php echo AuthSP::logonURL(); ?>" data-tooltip title="{tr:log_in_tip}">
                <span class="survey_button button">{tr:logon}</span>
            </a>
        </div>
            <?php }
        }
        ?>
    </article>
</section>
