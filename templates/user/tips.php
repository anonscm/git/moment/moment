<?php
/**
 * This file is part of the Moment project.
 * 2019
 * Copyright (c) RENATER
 */

if (!isset($user)) {
    throw new TemplateInternalMissingParameterException('user');
}

if (!isset($context)) {
    throw new TemplateInternalMissingParameterException('context');
}

$tips = array();

/*
 * Setting calendar types
 */
if(!Config::get('calendar.tips'))
    return;

if('reply' == $context) {
    if(!$user) {
        $tips[] = 'authenticate_to_add_calendars';
    } elseif(count($user->Calendars) == 0) {
        $tips[] = 'add_calendars_possibility';
    }
}

if('create' == $context && $user && count($user->Calendars) == 0) {
    $tips[] = 'add_calendars_possibility_creating';
}


if (count($tips) > 0) {
    ?>
    <section class="row clear">
        <?php
        foreach ($tips as $message) {
                ?>
            <div data-alert class="primary callout small small-12 hidden" data-closable data-tip-id="<?php echo substr(md5($message), 0, 8); ?>">
                <h4><i class="fa fa-info-circle"> </i> {tr:did_you_know}</h4>
                <article>
                    <?php echo Lang::tr($message); ?>
                </article>
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php
        }
        ?>
    </section>
    <?php
}
?>
