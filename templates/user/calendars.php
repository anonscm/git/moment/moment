<?php
/**
 * This file is part of the Moment project.
 * 2019
 * Copyright (c) RENATER
 */


$current_user = Auth::User();

// Retreving calendars entity of this users
$calendars = $current_user->Calendars;

$myCal = CalendarManager::getMyCalItem();

?>
<section class="row calendar-preferences" id="calendars" xmlns="http://www.w3.org/1999/html">
    <h1>{tr:my_calendars}</h1>
    <section class="calendar-list">
        <h2>{tr:connect_my_calendars}</h2>
        <p class="small-12 medium-10 ">{tr:connect_my_calendars_info} <a href="{url:user_guide}{cfg:user_guide.learn_more_paths.connect_my_calendars}" title="{tr:connect_my_calendars}" ><span class="fa fa-arrow-circle-right"></span> <i>{tr:learn_more}</i></a></p>
        <span class="small-12 medium-10 label warning no-calendar <?php echo (count($calendars) == 0)?'':'hidden'; ?>" ><span class="fa fa-calendar-check-o"></span> {tr:no_calendar_added_yet}</span>
        <div class="small-12 medium-10">
            <ul class="accordion calendars-accordion">
                <?php
                foreach ($calendars as $calendar) {
                    $isKO = $calendar->fresh_status && CalendarStatusCodes::OK != $calendar->status['code'];
                    ?>
                    <li data-calendar-id="<?php echo $calendar->id; ?>"
                        data-calendar-url="<?php echo $calendar->url; ?>"
                        data-calendar-name="<?php echo $calendar->name; ?>"
                        data-calendar-color="<?php echo $calendar->settings['color']; ?>">
                    <span class="accordion-title <?php echo $isKO?'calendar-error':''; ?>">
                        <span class="calendar-color small-1 columns"><span class="fa fa-square fa-lg" style="color:<?php echo $calendar->settings['color']; ?>"></span></span>
                        <span class="calendar-name small-6 medium-3 columns"><?php echo $calendar->name; ?></span>
                        <span class="calendar-url medium-6 hide-for-small-only columns"><a href="<?php echo $calendar->url; ?>" title="<?php echo $calendar->url; ?>"><?php echo $calendar->url; ?></a></span>
                        <menu class="calendar-actions medium-2 small-3 columns text-right">
                            <?php if($isKO) { ?>
                                <span title="<?php echo Lang::translate($calendar->status['message']); ?>" data-tooltip class="fa fa-exclamation-triangle calendar-error"></span>
                            <?php } ?>
                            <span title="{tr:edit}" data-tooltip data-action="edit_calendar" class="fa fa-pencil"></span>
                            <span title="{tr:delete}" data-tooltip data-action="delete_calendar" class="fa fa-trash-o"></span>
                        </menu>
                    </span>
                    </li>
                <?php } ?>
                <li class="hidden calendar-row-template">
                    <span class="accordion-title">
                        <span class="calendar-color small-1 columns"><span class="fa fa-square fa-lg"></span></span>
                        <span class="calendar-name small-6 medium-3 columns"></span>
                        <span class="calendar-url medium-6 hide-for-small-only columns"><a href="" title=""></a></span>
                        <menu class="calendar-actions medium-2 small-3 columns text-right">
                            <span title="{tr:edit}" data-tooltip data-action="edit_calendar" class="fa fa-pencil"></span>
                            <span title="{tr:delete}" data-tooltip data-action="delete_calendar" class="fa fa-trash-o"></span>
                        </menu>
                    </span>
                </li>
                <li class="calendar-form-row hidden">
                    <div>
                        <?php
                        echo (new CalendarForm())->getHTML();
                        ?>
                        <menu class="calendar-actions small-12 medium-10 text-right">
                            <button class="survey_button button highlight" data-action="create" title="{tr:create}" data-tooltip>{tr:validate}</button>
                            <button class="survey_button button highlight" data-action="cancel" title="{tr:cancel}" data-tooltip>{tr:cancel}</button>
                        </menu>
                    </div>
                </li>
            </ul>
        </div>
        <span class="survey_button" data-action="add_calendar"><span class="fa fa-plus-circle"></span> {tr:add_calendar}</span>

        <h2>{tr:connect_my_moment_calendar}</h2>
        <p class="small-12 medium-10">{tr:connect_my_moment_calendar_info} <a href="{url:user_guide}{cfg:user_guide.learn_more_paths.my_moment_calendar}" title="{tr:connect_my_moment_calendar}" > <span class="fa fa-arrow-circle-right"></span> <i>{tr:learn_more}</i></a></p>
        <div class="small-12 medium-10">
            <ul class="accordion calendars-accordion moment">
                <li>
                <span class="accordion-title">
                    <span class="calendar-color small-1 columns"><span class="fa fa-square fa-lg"></span></span>
                    <span class="calendar-name small-6 medium-3 columns"><?php echo $myCal['name']; ?></span>
                    <span class="calendar-url medium-6 hide-for-small-only columns"><a href="<?php echo $myCal['url']; ?>" title="<?php echo $myCal['url']; ?>"><?php echo $myCal['url']; ?></a></span>
                    <menu class="calendar-actions medium-2 small-3 columns text-right">
                        <span title="{tr:copy_link_to_clipboard}" data-tooltip data-content="<?php echo $myCal['url']; ?>" data-action="copy_to_clipboard" class="fa fa-clipboard"></span>
                        <span title="{tr:regenerate_link}" data-tooltip data-calendar-hash="<?php echo $myCal['hash']; ?>" data-action="refresh_calendar_link" class="fa fa-refresh fa-fw"></span>
                        <a href="{url:user_guide}{cfg:user_guide.learn_more_paths.my_moment_calendar}"><span title="{tr:access_user_guide_moment_calendar}" data-tooltip class="fa fa-question-circle"></span></a>
                    </menu>
                </span>
                </li>
            </ul>
        </div>
    </section>
    <h2>{tr:view_calendars}</h2>
    <p class="small-12 medium-10">{tr:view_calendars_info} <a href="{url:user_guide}{cfg:user_guide.learn_more_paths.view_my_calendars}" title="{tr:view_calendars}" > <span class="fa fa-arrow-circle-right"></span> <i>{tr:learn_more}</a></i></p>
    <section class="view-my-calendars-container small-12 medium-10">
        <div class="view-my-calendars-overlay">
            <div class="small-5 small-centered text-center align-middle">
                <span data-loader class="fa fa-circle-o-notch fa-spin fa-fw"></span>
                {tr:loading_calendars}
            </div>
        </div>
        <div id='calendar'></div>
    </section>
</section>