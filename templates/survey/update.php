<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
$survey = Survey::fromId($id);

$current_user = SurveyUserUtil::getCurrentUser($survey);

if ($current_user['role'] !== SurveyUserRoles::OWNER) throw new SurveyAccessDeniedException($survey, $current_user);

if ($survey->is_closed)
    throw new SurveyClosedException($survey, $current_user);

$initialStep = (isset($_GET['step']) && in_array($_GET['step'], array('general', 'questions', 'guests'))) ? $_GET['step'] : 'general';
?>

<section data-component="survey-wizard"
         data-survey-id="<?php echo $survey->id; ?>"
         data-survey-timezone="<?php echo $survey->time_zone; ?>"
        <?php echo ($survey->participants_count > 0)?'data-has-participants':''; ?>>
    <nav class="row">
        <span data-action="goto_step" data-step="general" class="small-4">{tr:general}</span>
        <span data-action="goto_step" data-step="questions" class="small-4">{tr:questions}</span>
        <span data-action="goto_step" data-step="guests" class="small-4">{tr:guests_step_header}</span>
    </nav>
    <input type="hidden" name="initialStep" value="<?php echo $initialStep; ?>"/>
    <input type="hidden" name="is_draft" value="<?php echo $survey->is_draft?1:0; ?>"/>

    <section data-container="steps" class="invisible row">
        <?php

        Template::display('survey/wizard/step_general', array('survey' => $survey));
        Template::display('survey/wizard/step_questions', array('survey' => $survey));
        Template::display('survey/wizard/step_guests', array('survey' => $survey));

        ?>
        <section class="columns wizard_actions text-center">
            <div class="button-group">
                <span data-action="cancel_creation" class="wizard_action button" tabindex="0">{tr:cancel}</span>
                <span data-action="previous" class="wizard_action button" tabindex="0">{tr:previous}</span>
                <?php if ($survey->is_draft) { ?>
                    <span data-action="save_draft" class="wizard_action button" tabindex="0">{tr:save_draft}</span>
                    <span data-action="preview_survey" class="wizard_action button" tabindex="0">{tr:preview_the_survey}</span>
                    <span data-action="update_survey" class="wizard_action button highlight" tabindex="0">{tr:create_the_survey}</span>
                <?php } else { ?>
                    <span data-action="update_survey" class="wizard_action button highlight" tabindex="0">{tr:save_modifications}</span>
                <?php } ?>
                <span data-action="next" class="wizard_action button highlight" tabindex="0">{tr:next}</span>
            </div>
        </section>
    </section>
</section>

