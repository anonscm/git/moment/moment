<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
if(($reason === 'requires_authentication') || ($reason === 'not_allowed_to_see_results')) {
    if (!Auth::isAuthenticated()) {
        //Trigger authentication process
        AuthSP::trigger(Utilities::getCurrentURL());
    }
}
?>

    <section class="row" data-component="access-denied">
        <article class="error_http text-center">
            <img src="{img:access_denied.png}" />
            <div class="message">
                <i class="fa fa-exclamation-circle"></i>
                <?php echo ($reason) ? Lang::tr($reason) : ''; ?>
                <?php echo (isset($alternative) && $alternative != '') ? (' ' . $alternative . '') : ''; ?>
            </div>
        </article>
        </article>
    </section>

