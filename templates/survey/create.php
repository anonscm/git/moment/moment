<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
$current_user = SurveyUserUtil::getCurrentUser();

//TODO check if there is a better exception to throw
if ($current_user['role'] !== SurveyUserRoles::CREATOR) throw new AuthUserNotAllowedException();

//Setting the id
$survey_id = 'new';

$initialStep = 'general';

?>

<section data-component="survey-wizard" data-status="create" data-survey-id="<?php //echo $survey_id; ?>">
    <!--    <div class="small-12 medium-9 medium-centered columns text-center"><h3 class="page-title">{tr:create_a_survey}</h3></div>-->


    <nav class="row">
        <span data-action="goto_step" data-step="general" class="small-4">{tr:general}</span>
        <span data-action="goto_step" data-step="questions" class="small-4">{tr:questions}</span>
        <span data-action="goto_step" data-step="guests" class="small-4">{tr:guests_step_header}</span>
    </nav>
    <input type="hidden" name="initialStep" value="<?php echo $initialStep; ?>"/>

    <section data-container="steps" class="row invisible">
        <?php
        Template::display('survey/wizard/step_general');
        Template::display('survey/wizard/step_questions');
        Template::display('survey/wizard/step_guests');
        ?>
        <section class="wizard_actions text-center">
            <div class="button-group">
                <span data-action="cancel_creation" class="wizard_action button" tabindex="0">{tr:cancel}</span>
                <span data-action="previous" class="wizard_action button" tabindex="0">{tr:previous}</span>
                <span data-action="save_draft" class="wizard_action button" tabindex="0">{tr:save_draft}</span>
                <span data-action="preview_survey" class="wizard_action button" tabindex="0">{tr:preview_the_survey}</span>
                <span data-action="next" class="wizard_action button highlight" tabindex="0">{tr:next}</span>
                <span data-action="save_survey" class="wizard_action button highlight" tabindex="0">{tr:create_the_survey}</span>
            </div>
        </section>
    </section>
    <div class="reveal" id="surveySaved" data-reveal>
        <h2>{tr:survey_saved}</h2>
        <p>
            <i>{tr:survey_saved_info}</i>
            <br/><br/>
            <section class="share_survey">
                <label>{tr:share_survey}</label>
                <span class="small-11 column nopadding">
                    <input type="text" value="" name="survey_share_link" placeholder="{tr:survey_share_link_not_ready}"
                           readonly/>
                </span>
                <span class="small-1 fa fa-clipboard" data-share-link
                      title="{tr:copy_to_clipboard}" data-action="copy_to_clipboard" data-content="">
                </span>
            </section>
            <section class="text-center clear">
                <i class="info hidden">{tr:you_will_be_redirected_to_manage_page_in} <span class="counter"></span>
                    {tr:time_seconds}</i>
            </section>
            <div class="primary small callout small-12">
                <p>{tr:email_with_link}</p>
            </div>
            <section class="buttons clear text-center">
                <span data-action="goto-manage" role="button" class="button">{tr:ok}</span>
            </section>
        </p>
        <button class="close-button" data-close aria-label="{tr:close_modal}" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</section>