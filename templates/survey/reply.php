<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
$survey = Survey::fromHumanReadableId($human_readable_id);
$current_user = SurveyUserUtil::getCurrentUser($survey);

if (SurveyUserRoles::REPLIER > $current_user['role'])
    throw new SurveyAccessDeniedException($survey, $current_user);

if ($survey->is_closed)
    throw new SurveyClosedException($survey, $current_user);

$survey->applyConstraints();

$participant = null;
try {
    //Trying to get a participant for the current replier
    $participant = Participant::getParticipant($survey, array(), false);

} catch (ParticipantNotFoundException $e) {
    //No participant found with data -> Not that important at the moment
}
?>
<?php if ($survey->is_draft) { ?>
    <section class="small-12 medium-9 text-center small-centered">
        <h4 aria-label="{tr:draft_message}">{tr:draft_message} (<a href="<?php echo $survey->update_path; ?>?step=guests">{tr:comeback_to_edition}</a>)
        </h4>
    </section>
<?php } ?>
<h4 class="page-title row" aria-label="{tr:reply_to_survey}"><?php Template::display('survey/tools/short_authorization', array('survey' => $survey)); ?>
    {tr:reply_to_survey}
    <small>
        <?php
        // A declarative participant is prioritary over authenticated
        // (An authenticated user can with the priority answer a previously answered survey with a declarative identity)
        if ((isset($participant) && ParticipantTypes::DECLARATIVE === $participant->type)) {
            if ($participant->isAnonymous()) {
                echo '('.Lang::translate('now_replying_anonymously').')';
            } else {
                echo '('.Lang::translate('now_replying_as') . ' <i>' . $participant->email . '</i>)';
            }
        } else if(isset($current_user['name'])) {
            echo '('.Lang::translate('now_replying_as') . ' ' . $current_user['name'].')';
        }
        ?>
    </small>
</h4>
<section data-component="survey-reply" class="row" data-survey-title="<?php echo htmlentities($survey->title); ?>" data-survey-id="<?php echo $survey->id; ?>"
         data-survey-timezone="<?php echo $survey->time_zone; ?>" <?php echo (isset($participant)?'data-participant-id="'.$participant->id.'"':''); ?>
         data-user-role="<?php echo $current_user['role']; ?>"
         data-disable-answer-edition="<?php echo $survey->disable_answer_edition; ?>"
         <?php echo $survey->has_date_questions?'data-survey-has-date-questions':''; ?>>
    <?php Template::display('user/tips', array('user' => Auth::user(), 'context' => 'reply')); ?>
    <?php Template::display('survey/tools/header', array('survey' => $survey, 'current_user' => $current_user, 'context' => 'reply')); ?>
    <section id="replier_info" data-form="replier" class="replier_info" data-abide>
        <?php
        // A declarative participant is prioritary over authenticated
        // (An authenticated user can with the priority answer a previously answered survey with a declarative identity)
         if ((isset($participant) && ParticipantTypes::DECLARATIVE === $participant->type)) {
             $participant_form = new ParticipantForm($survey, $participant);
             echo $participant_form->getHTML();
         } elseif (!Auth::user()) {
             $participant_form = new ParticipantForm($survey);
             echo $participant_form->getHTML();
         }

         if(!isset($participant) && !Auth::user()) {
         ?>
             <div class="participant_recovery_by_email">
                <a href="#" title="{tr:i_already_answered_to_this_survey}" data-action="send_me_a_recovery_email">{tr:i_already_answered_to_this_survey}</a>
                <i class="fa fa-info-circle" data-tooltip title="{tr:i_already_answered_to_this_survey_explanation}"></i>
             </div>
        <?php
         }
        ?>
    </section>
    <div class="medium-3 small-12 columns">
    <?php if(Auth::user() && $survey->has_date_questions) {
        Template::display('survey/tools/calendar_filter');
    } ?>
    </div>
    <?php
    if($survey->has_timezone_sensible_questions) {
        echo '<div class="show-results-container columns small-12 medium-5">';
        echo '<p class="label secondary"><span class="hide-for-small-only hide-for-medium-only">{tr:time_zone_info_hours_large}</span><span class="hide-for-large">{tr:time_zone_info_hours_small}</span> : <span data-localize="time_zone"></span></p>';
        echo '</div>';
    }
    
    //Showing toggle only when !hiding answers and there is answers
    if (!$survey->hide_answers && $survey->participants_count > 0) { ?>
        <div class="toggle_results show-results-container medium-4 small-12 columns hide-for-small-only">
    <?php
        $results_switch = new FormFieldSwitch('show_results', array('label' => 'show_others_answers'), false);
        echo $results_switch->getHTML();?>
        </div>
        <section class="show-results-for-small text-center button-group show-for-small-only">
            <a class="button survey_button"
               href="{cfg:application_url}survey/results/<?php echo $survey->id; ?>">{tr:show_results}</a>
        </section>
    <?php } ?>
    <div class="loading-answers medium-4 small-12 columns"></div>
<?php
    Template::display('question/reply_form', array('survey' => $survey,
        'current_user' => $current_user,
        'participant' => $participant));
    ?>
</section>