<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
$survey = Survey::fromId($id);

$current_user = SurveyUserUtil::getCurrentUser($survey);

//TODO check if there is a better exception to throw
if ($current_user['role'] < SurveyUserRoles::REPLIER) throw new SurveyAccessDeniedException($survey, $current_user);

if ($survey->hide_answers && $current_user['role'] <= SurveyUserRoles::REPLIER) {
    $current_user['reason'] = 'not_allowed_to_see_results';
    throw new SurveyAccessDeniedException($survey, $current_user);
}
?>
<h4 class="page-title row">{tr:results_of_survey}</h4>
<section data-component="survey-results" class="row" data-survey-id="<?php echo $survey->id; ?>" data-survey-timezone="<?php echo $survey->time_zone; ?>" data-user-role="<?php echo $current_user['role']; ?>">
    <?php Template::display('survey/tools/header', array('survey' => $survey, 'current_user' => $current_user, 'context' => 'results')); ?>
    <?php
    if ($current_user['role'] >= SurveyUserRoles::OWNER) {
        $decision_making_tool_switch = new FormFieldSwitch('decision_making_tool_switch', array('label' => 'toggle_decision_making_tool'), false);
        echo '<div class="toggle_decision columns small-12">';
        if($survey->has_timezone_sensible_questions) {
            echo '<p class="label secondary margin-right"><span class="hide-for-small-only hide-for-medium-only">{tr:time_zone_info_hours_large}</span><span class="hide-for-large">{tr:time_zone_info_hours_small}</span> : <span data-localize="time_zone"></span></p>';
        }
        echo $decision_making_tool_switch->getHTML();
        echo ' <i class="fa fa-info-circle top" data-tooltip aria-haspopup="true" data-disable-hover="false" tabindex="1" data-v-offset="15" title="{tr:toggle_decision_making_tool_info}"></i>';
        echo '</div>';

    } else{
        if($survey->has_timezone_sensible_questions) {
            echo '<div class="columns small-12">';
            echo '<p class="label secondary"><span class="hide-for-small-only hide-for-medium-only">{tr:time_zone_info_hours_large}</span><span class="hide-for-large">{tr:time_zone_info_hours_small}</span> : <span data-localize="time_zone"></span></p>';
            echo '</div>';
        }
    }
    ?>
    <div class="loading-answers small-6 columns"></div>
    <section class="survey-questions small-12">
        <?php
        $ultimate_participant = $survey->ultimate_participant;

        foreach ($survey->Questions as $index => $question) {
            Template::display('answer/answers',
                array('question' => $question, 'current_user' => $current_user));
        }
        ?>
    </section>
    <?php
    /**
     * Final answer selection (input)
     **/
    if (SurveyUserRoles::OWNER <= $current_user['role'] && !$ultimate_participant) { ?>
        <section class="survey-questions clear text-center">
            <div class="hidden small-12 media-6 error_hint invalid-answer-message all-questions-must-be-answered">
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                <span>{tr:please_select_value_for_all_questions}</span>
            </div>
            <span data-action="send_final_answers" class="survey_button button">{tr:validate_final_answers}</span>
        </section>
    <?php }
    ?>
</section>

