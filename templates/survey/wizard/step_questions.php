<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
//Default values
$current_questions = array();

//We get all known question options
$settings = QuestionSettings::getSettings();
//Default values
$current_settings = array();
foreach ($settings as $setting_name => $setting) {
    $current_settings[$setting_name] = $setting['default'];
}

//Status of the step
$data_status = 'waiting';

//If we are updating a survey
if (isset($survey)) {
    $current_questions = $survey->Questions;
    $data_status = 'ready';
}

?>
<section data-step="questions" data-status="<?php echo $data_status; ?>" class="hidden">
    <?php Template::display('user/tips', array('user' => Auth::user(), 'context' => ((isset($survey) && !$survey->is_draft)?'update':'create'))); ?>
    <?php
        if (isset($survey) && count($survey->Participants) > 0) {
    ?>
    <div data-alert class="primary callout small small-12" data-closable>
        {tr:question_edit_warning}
        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <?php } ?>

    <?php Template::display('survey/tools/calendar_filter'); ?>
    <div data-alert class="alert no-question-found hidden error_hint" data-closable>
        <article>
            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
            {tr:no_question_found}
        </article>
    </div>
    <ul class="empty-form-container hidden">
        <?php Template::display('question/question_form', array('index' => -1, 'question' => null)); ?>
    </ul>
    <ul class="questions_list">
        <?php foreach ($current_questions as $index => $question) {
            Template::display('question/question_form', array('index' => $index, 'question' => $question));
        } ?>
    </ul>
    <aside class="small-12 columns step_header add_question_button">
        <span id="add_questions_buttons" data-toggler=".hide">
        <button data-action="add_date_question" class="wizard_action_add button">
            <span class="fa fa-plus fa-lg"></span> {tr:add_date_question}<legend>{tr:add_date_question_legend}</legend>
        </button>
        <button data-action="add_date_range_question" class="wizard_action_add button">
            <span class="fa fa-plus fa-lg"></span> {tr:add_date_range_question}<legend>{tr:add_date_range_question_legend}</legend>
        </button>
        <button data-action="add_text_question" class="wizard_action_add button" >
            <span class="fa fa-plus fa-lg"></span> {tr:add_other_question}<legend>{tr:add_other_question_legend}</legend>
        </button>
        </span>        
        <button data-toggle="add_questions_buttons" data-action="compress" class="wizard_action_add button top" data-tooltip aria-haspopup="true" data-disable-hover="false" data-v-offset="15" tabindex="1" title="{tr:compress_expand_view_info}">
                <span class="fa fa-compress fa-lg"></span> {tr:compress}<legend>{tr:compress_legend}</legend>
        </button>
        <button data-toggle="add_questions_buttons" data-action="expand" class="wizard_action_add button hidden ">
                <span class="fa fa-expand fa-lg"></span> {tr:expand}
        </button>        
    </aside>
    <div id="advanced-options-container" class="advanced-options-container small-12">
        <span data-action="toggle-advanced-options" role="button" tabindex="0"><i class="fa fa-caret-right" aria-hidden="true"></i> {tr:advanced_options}</span>
        <div class="small-12 column advanced-options hidden" tabindex="0">
            <div class="proposition_constraint">
                <form class="proposition_constraint_form" data-abide>
                    <table class="constraints_table">
                        <tr>
                            <th colspan="5">{tr:constraints}</th>
                        </tr>
                        <tr class="constraint_header">
                            <th>{tr:question}</th>
                            <th>{tr:proposition}</th>
                            <th>{tr:constraint}</th>
                            <th>{tr:value}</th>
                            <th> </th>
                        </tr>
                        <?php foreach ($current_questions as $index => $question) {
                            $proposition_index = -1;
                            foreach ($question->propositions as $proposition) {
                                $proposition_index++;

                                if (!array_key_exists('constraints', $proposition->options))
                                    continue;

                                foreach ($proposition->options['constraints'] as $constraint) { ?>
                                    <tr class="constraint_row">
                                        <td class="question" data-question-id="<?php echo $question->id; ?>">
                                            <?php echo $question->statement; ?>
                                        </td>
                                        <td class="proposition"
                                            data-proposition-id="<?php echo $proposition->id; ?>"
                                            data-proposition-index="<?php echo $proposition_index; ?>">
                                            <?php echo $proposition->label; ?>
                                        </td>
                                        <td class="constraint"
                                            data-constraint-type="<?php echo $constraint['type']; ?>"
                                            data-constraint-value="<?php echo $constraint['value']; ?>">
                                            <?php echo Lang::tr($constraint['type']); ?>
                                        </td>
                                        <td><?php echo $constraint['value']; ?></td>
                                        <td><button data-action="delete_constraint" class="fa fa-trash-o"></button></td>
                                    </tr>
                                <?php }
                            }
                        } ?>
                        <tr class="no_constraints_row hidden">
                            <th colspan="5">
                                {tr:no_known_constraints}
                            </th>
                        </tr>
                        <tr class="constraints_form_row hidden">
                            <th>
                                <select name="question" data-no-binding required>
                                </select>
                            </th>
                            <th>
                                <select name="proposition" data-no-binding required>
                                </select>
                            </th>
                            <th>
                                <select name="constraint_type" data-no-binding>
                                    <option value="limit_choice">{tr:limit_choice}</option>
                                </select>
                            </th>
                            <th>
                                <input class="input" name="constraint_value" type="number" pattern="positive_integer" min="0" placeholder="{tr:constraint_value_placeholder}" data-no-binding required/>
                                <i class="fa fa-info-circle top constraint_value_info" data-tooltip title="{tr:constraint_value_info}"></i>
                            </th>
                            <th>
                                <span class="wizard_action button square highlight small" data-action="send_constraint" role="button" tabindex="0">{tr:validate}</span>
                            </th>
                        </tr>
                    </table>
                </form>
            </div>
            <span class="column" data-action="add_constraint" role="button" tabindex="0">
                <span class="fa fa-plus-circle" aria-hidden="true"></span> {tr:add_a_constraint}
            </span>
        </div>
    </div>
</section>
