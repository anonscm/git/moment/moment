<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
//Default values
$current_settings = array();
$current_guests = array();
$share_link = '';
$dont_receive_invitation_copy = SurveySettings::getSurveySettings()['dont_receive_invitation_copy']['default'];
$reply_access = SurveySettings::getSurveySettings()['reply_access']['default'];
$enable_anonymous_answer = SurveySettings::getSurveySettings()['enable_anonymous_answer']['default'];
$auto_close = SurveySettings::getSurveySettings()['auto_close']['default'];

//Status of the step
$data_status = 'ready';

//If we are updating a survey
if (isset($survey)) {
    $current_guests = $survey->Guests;
    $share_link = $survey->path;
    $dont_receive_invitation_copy = $survey->dont_receive_invitation_copy;
    $reply_access = $survey->reply_access;
    $enable_anonymous_answer = $survey->enable_anonymous_answer;
    $auto_close = $survey->auto_close;
}
?>
<section data-step="guests" data-status="<?php echo $data_status; ?>" class="hidden">
    <section class="small-12 large-6 column">
        <?php
        if(SurveySettings::getSurveySettings()['auto_close']['properties']['min']
            <  SurveySettings::getSurveySettings()['auto_close']['properties']['max']) { ?>
            <span data-form="auto_close">
            <?php
            // Tip explains when survey will be definitively deleted
            echo SurveySettings::getInput('auto_close',
                array('tip' => sprintf(Lang::tr('settings_auto_close'),
                    (Config::get('conservation_duration')?Config::get('conservation_duration'):6)
                )), $auto_close);
            ?>

                <span data-action="open-auto-close" class="fa fa-calendar-o"></span>
            </span>
        <?php } else {?>
            <label data-form="auto_close" for="auto_close" class="small-12 medium-6 large-12">
                <span data-label-text="">{tr:auto_close}</span>
                <span class="small-12" data-timestamp="<?php echo $auto_close; ?>" data-localize="date">{tr:auto_close}</span>
                <input type="hidden" value="<?php echo $auto_close; ?>" name="settings.auto_close" />
            </label>
        <?php } ?>
        <?php
            echo SurveySettings::getInput('reply_access', array(), $reply_access);
            echo ' <i class="fa fa-info-circle top reply_access_info" data-tooltip data-allow-html="true" aria-haspopup="true" data-disable-hover="false" tabindex="1" data-v-offset="15" title="{tr:reply_access_info}"></i>';
        ?>
        <div class="warning small callout small-12 large-10 authenticated-guests-info" data-closable>
            <p>{tr:authenticated_guests_info}</p>
            <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="warning small callout small-12 large-10 authenticated-users-info" data-closable>
            <p>{tr:authenticated_users_info}</p>
            <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="primary small callout small-12 large-10 all-users-info" data-closable>
            <p>{tr:all_users_info}</p>
            <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php
            echo SurveySettings::getInput('enable_anonymous_answer', array(), $enable_anonymous_answer);     
        ?>
    </section>
    <section class="guests_invitation small-12 large-6 end column">
        <label class="label-header" name="invite_repliers">
            <span data-label-text="">{tr:invite_repliers}</span>
        </label>
        <div class="primary small callout small-11" data-closable>
            <p>{tr:add_guest_notice}</p>
            <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div id="guests_list" class="small-11">
            <?php foreach ($current_guests as $index => $guest) { ?>
                <label>
                <span class="guest_email" data-index="<?php echo $index; ?>"
                      data-id="<?php echo $guest->hash; ?>"><?php echo $guest->email; ?></span>
                    <span class="guest_actions float-right">
                    <span data-action="edit_guest" class="fa fa-pencil" tabindex="0"></span>
                    <span data-action="delete_guest" class="fa fa-trash-o" tabindex="0"></span>
                </span>
                </label>
            <?php } ?>
        </div>
        <section>
            <section data-form="guest" class="hidden">
                <label for="email" class="small-8 medium-6 end">
                    <span data-label-text="">{tr:email}</span>
                    <input class="input" type="email" name="guests.email" data-no-binding data-suggest="survey.guest_email"
                           required value="" placeholder="{tr:guest_email_input_notice}" />
                </label>
                <span data-action="send_guest" role="button" class="wizard_action highlight button small square padding-left padding-right" tabindex="0">{tr:validate}</span>
                <span data-action="cancel_guest" role="button" class="wizard_action highlight button small square padding-left padding-right" tabindex="0">{tr:cancel}</span>
            </section>
            <span data-action="add_guest" tabindex="0"><span class="fa fa-plus-circle"></span> {tr:add_guest}</span>
        </section>
        <section>
            <?php
            echo SurveySettings::getInput('dont_receive_invitation_copy', array(), $dont_receive_invitation_copy);
            ?>
        </section>
        <?php if (isset($survey)) { ?>
            <span class="share_survey">
                <label class="label-header">
                    <span data-label-text="">{tr:share_survey}</span>
                </label>
                <span class="small-11 column nopadding-left">
                    <input class="input" type="text" value="<?php echo $share_link; ?>" name="survey_share_link"
                           placeholder="{tr:survey_share_link_not_ready}" readonly/>
                </span>
                <span class="small-1 fa fa-clipboard" data-tooltip aria-haspopup="true"
                      title="{tr:copy_to_clipboard}" data-action="copy_to_clipboard"
                      data-content="<?php echo $share_link ?>">
                </span>
            </span>
        <?php } ?>
    </section>
<?php
        if (Config::get('must_accept_terms') && (!isset($survey) || $survey->is_draft)) { ?>
        <section class="accept_terms_container clear small-12 text-center">
            <?php
            $accept_terms =  new FormFieldCheckbox('accept_terms', array('label' => 'accept_terms', 'required' => false), false);
            echo $accept_terms->getHTML();
            ?>
        </section>
    <?php } ?>
</section>

