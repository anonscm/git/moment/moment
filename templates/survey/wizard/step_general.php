<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
//Default values
$id = 'new';
$title = '';
$place = '';
$description = '';

//Status of the step
$data_status = 'waiting';

//Status of advanced option
$open_advanced_options = false;

//We get all known settings
$settings = SurveySettings::getSurveySettings(isset($survey)?$survey:null);

//Default values
$current_settings = array();
foreach ($settings as $setting_name => $setting) {
    $current_settings[$setting_name] = $setting['default'];
}

$current_owners = array();

//If we are updating a survey
if (isset($survey)) {
    $id = $survey->id;
    $title = $survey->title;
    $place = $survey->place;
    $description = $survey->raw_description;

    foreach ($settings as $setting_name => $setting) {
        $current_settings[$setting_name] = $survey->$setting_name;
    }
    $current_owners = $survey->Owners;

    //If something is used in advanced option, we open advanced option
    $open_advanced_options = $current_settings['hide_answers'] === 1
        || $current_settings['hide_comments'] === 1
        || $current_settings['time_zone'] === 1
        || $current_settings['limit_participants'] === 1
        || $current_settings['disable_answer_edition'] === 1
        || $current_settings['dont_notify_on_reply'] === 1
        || count($current_owners) > 1;

    $data_status = 'ready';
}
?>
<section data-step="general" data-form="general" data-status="<?php echo $data_status; ?>">
    <section class="parameters small-collapse large-uncollapse">
        <section class="small-12 medium-12 large-6 column">
            <label for="title" class="small-12 medium-6 large-12">
                <span data-label-text="">{tr:surveys_title}</span>
                <input class="input" type="text" name="title" data-suggest="survey.title" required value="<?php echo htmlspecialchars($title); ?>"
                       placeholder="{tr:surveys_title}"/>
            </label>
            <label for="description">
                <span data-label-text="">{tr:description}</span>
                <textarea class="input" name="description" rows="2" cols="80"><?php echo $description; ?></textarea>
            </label>
        </section>
        <section class="small-12 medium-12 large-6 column options">
            <label><span data-label-text="">{tr:options}</span></label>
            <section class="small-12 small-collapse large-uncollapse column advanced-options">
                <section class="small-12 medium-12 column">
                    <label>{tr:in_input_of_participants_answers}</label>
                    <ul>
                        <?php
                        echo '<li>'.SurveySettings::getInput('limit_participants', array(), $current_settings['limit_participants']);
                        echo SurveySettings::getInput('limit_participants_nb', array(), $current_settings['limit_participants_nb']);
                        echo ' <i class="fa fa-info-circle top" data-tooltip aria-haspopup="true" data-disable-hover="false" data-v-offset="15" tabindex="1" title="{tr:limited_participant_number_example}"></i>';
                        echo '</li>';
                        echo '<li>'.SurveySettings::getInput('disable_answer_edition', array(), $current_settings['disable_answer_edition']);
                        echo ' <i class="fa fa-info-circle top" data-tooltip aria-haspopup="true" data-disable-hover="false" data-v-offset="15" tabindex="1" title="{tr:disable_answer_edition_example}"></i>';
                        echo '</li>';
                        echo '<li>'.SurveySettings::getInput('dont_notify_on_reply', array(), $current_settings['dont_notify_on_reply']).'</li>';
                        ?>
                    </ul>
                    <label class="margin-top">{tr:in_results_display}</label>
                    <ul>
                        <?php
                        echo '<li>'.SurveySettings::getInput('hide_answers', array(), $current_settings['hide_answers']).'</li>';
                        echo '<li>'.SurveySettings::getInput('hide_comments', array(), $current_settings['hide_comments']).'</li>';
                        echo SurveySettings::getInput('time_zone', array('hide' => true), $current_settings['time_zone']);
                        ?>
                    </ul>
                </section>
                <section class="small-12 column">
                    <label class="margin-top">{tr:owners}</label>
                    <div class="small-12 medium-9 large-12">
                        <span class="owner_item owner_editor columns">
                            <label class="owner_email">
                                <span><?php echo strtolower(Auth::user()->email); ?></span>
                            </label>
                            <span class="float-right">
                                <i class="fa fa-info-circle top reply_access_info" data-tooltip="" data-allow-html="true"
                                   data-disable-hover="false" data-v-offset="15" title="{tr:no_delete_survey_owner}"></i>
                            </span>
                        </span>
                    </div>
                    <div id="owners_list" class="small-12 medium-9 large-12">
                        <?php
                        $index = 0;
                        foreach ($current_owners as $owner) {
                            //Do not show current owner in list
                            if(strtolower($owner->email) === strtolower(Auth::user()->email))
                                continue;
                            ?>
                            <span class="owner_item columns" data-index="<?php echo $index; ?>">
                            <label class="inputSwitch owner_email small-10">
                                <span></span>
                                <input type="email" name="owners[<?php echo $index++; ?>].email"
                                       value="<?php echo $owner->email; ?>" maxlength="255"/>
                            </label>
                            <span class="owner_actions float-right">
                                <span data-action="edit_owner" class="fa fa-pencil" tabindex="0"></span>
                                <span data-action="delete_owner" class="fa fa-trash-o" tabindex="0"></span>
                            </span>
                        </span>
                        <?php } ?>
                    </div>
                </section>
                <section class="small-12 column">
                    <span data-action="add_owner" tabindex="0"><span class="fa fa-plus-circle"></span> {tr:add_owner}</span>
                </section>
            </section>
        </section>
    </section>
</section>
<!-- Adding owner form -->
<section data-form1="owner" class="clearfix hidden">
    <label for="email" class="small-8 end">
        <span data-label-text="">{tr:email}</span>
        <input type="email" name="owners.email" data-no-binding maxlength="255" required
               value=""
               placeholder="{tr:owner_email_input_notice}"
        />
    </label>
    <span data-action="send_owner" class="wizard_action highlight button square small" tabindex="0">{tr:validate}</span>
    <span data-action="cancel_owner" class="wizard_action highlight button square small" tabindex="0">{tr:cancel}</span>
</section>
