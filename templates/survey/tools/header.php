<?php
/**
 * This file is part of the Moment project.
 * 2024
 * Copyright (c) RENATER
 */
if (!isset($survey)) {
    throw new TemplateInternalMissingParameterException('survey');
}
if (!isset($current_user)) {
    throw new TemplateInternalMissingParameterException('current_user');
}
?>
<div>
    <h4 class="survey_title small-12 medium-9 columns">
        <?php echo $survey->title; ?>
        <span class="help-text">({tr:organized_by} : <?php echo $survey->owner_names; ?>) </span>
    </h4>
    <?php if (!$survey->hide_answers || (isset($context) && $context === 'results' && $current_user['role'] >= SurveyUserRoles::OWNER)) {
        $csvSeparator = Config::get('result_csv_export_separator')?Config::get('result_csv_export_separator'):',';
        $csvSeparatorInfo = Lang::tr([';' => 'semicolons', ',' => 'commas'][$csvSeparator])->out(); ?>
    <p class="text-right small-12 medium-3 columns">
        <a data-action="download_csv"
           href="{cfg:application_url}rest.php/survey/<?php echo $survey->id; ?>/results?format=download:csv:<?php echo $csvSeparator; ?>&tz=">
            <span class="fa fa-download"></span> {tr:download_csv}</a>
        <i class="fa fa-info-circle top" data-tooltip title="<?php echo Lang::tr('download_csv_info')->r(['separators' => $csvSeparatorInfo])->out(); ?>"></i>
    </p>
    <?php } ?>
    <?php Template::display('survey/tools/info', array('survey' => $survey)); ?>
    <div class="survey-description clear">
        <?php echo $survey->parsed_description; ?>
    </div>
</div>