<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
if (!isset($survey)) {
    throw new TemplateInternalMissingParameterException('survey');
}

$messages = (new Event('survey_info_on_reply', $survey))->trigger(function ($event) {
    return $event->infos();
});
if (count($messages) > 0) {
?>
<div class="small-12 columns">
    <?php
    foreach ($messages as $level => $message) {
        foreach ($message as $content) {
            ?>
            <div data-alert class="small-12 <?php echo count($messages) === 3?'medium-4':'medium-6' ?> columns" data-closable>
                <div class="<?php echo $level; ?> callout small">
                    <p><?php echo $content; ?></p>
                    <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>
<?php } ?>
