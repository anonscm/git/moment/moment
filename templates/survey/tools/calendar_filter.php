<ul class="dropdown menu filter_my_calendars" data-dropdown-menu data-disable-hover="true" data-click-open="true">
    <li>
        <a href="#">
            <img class="hide-for-small-only" src="{img:filter.svg}" alt="{tr:filter_my_calendars}" data-options="disable_for_touch:true"/>
            {tr:filter_my_calendars}
        </a>
        <ul class="menu">
            <li>
                <label>
                    <input class="filter-calendar hidden" type="checkbox" data-calendar-id="all_calendars" name="all_calendars" checked/>
                    <span class="calendar-color fa fa-square fa-lg" style="color:white"></span>
                    {tr:all_calendars}
                </label>
            </li>
            <hr/>
            <?php
            $myCal = CalendarManager::getMyCalItem();
            ?>
            <li>
                <label>
                    <input class="filter-calendar hidden" type="checkbox" data-calendar-id="<?php echo $myCal['id']; ?>" checked/>
                    <span class="calendar-color fa fa-square fa-lg" style="color:<?php echo $myCal['settings']['color']; ?>"></span>
                    <?php echo $myCal['name']; ?>
                </label>
            </li>
            <?php foreach(Auth::user()->Calendars as $calendar) { ?>
                <li>
                    <label>
                        <input class="filter-calendar hidden" type="checkbox" data-calendar-id="<?php echo $calendar->id; ?>" checked/>
                        <span class="calendar-color fa fa-square fa-lg" style="color:<?php echo isset($calendar->settings['color'])?$calendar->settings['color']:CalendarUtil::getColor(); ?>"></span>
                        <?php echo $calendar->name; ?>
                    </label>
                </li>
                <?php
            }?>
        </ul>
    </li>
    <li class="filter_my_calendars_info">
        <label><i class="fa fa-info-circle top" data-tooltip title="{tr:filter_my_calendars_tip}"></i></label>
    </li>
</ul>