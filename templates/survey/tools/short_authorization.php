<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
if (!isset($survey)) {
    throw new TemplateInternalMissingParameterException('survey');
}

$icon = '';
$tip = '';
switch ($survey->reply_access) {
    case Survey::OPENED_TO_EVERYONE:
        $icon = 'fa-globe';
        $tip = Lang::tr('tip_opened_to_everyone');
        break;
    case Survey::OPENED_TO_AUTHENTICATED:
        $icon = 'fa-users';
        $tip = Lang::tr('tip_opened_to_authenticated');
        break;
    case Survey::OPENED_TO_GUESTS:
        $icon = 'fa-envelope-square';
        $tip = Lang::tr('tip_opened_to_guests');
        break;
    case Survey::OPENED_TO_NOONE:
        $icon = 'fa-ban';
        $tip = Lang::tr('tip_opened_to_noone');
        break;
}
?>
<i class="fa <?php echo $icon; ?>" aria-hidden="true"
   data-tooltip aria-haspopup="true" data-disable-hover="false"
   title="<?php echo $tip; ?>"></i>