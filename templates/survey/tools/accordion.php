<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 *
 * @var $is_guest wether user is guest to those survey (second tab of manage page)
 * @var array $actions list of enabled actions
 * @var $surveys Surveys to show
 * @var $filters filters to use
 */
$count = 1;

$total_count = 0;
$hide = false;

$start4 = microtime(true);

//Initializing actions keys (Three known types draft, open, closed)
$action_icons = SurveyActionUtil::initActionButtonsForTypes($actions, array('opened', 'draft', 'closed', 'opened_wo_results', 'closed_wo_results'));

$answered_icon = '<i class="fa fa-check-square-o" aria-hidden="true" data-tooltip title="'. Lang::tr('you_answered').'"></i>';
$not_answered_icon = '<i class="fa fa-square-o" aria-hidden="true" data-tooltip title="'. Lang::tr('you_have_not_answered').'"></i>';

?>
<ul class="surveys-filters columns medium-2 menu horizontal medium-vertical">
    <?php
    $survey_filters = SurveyFilters::getSurveyFilters($surveys, true, $filters);

    foreach ($survey_filters as $filter_name => $filter) {
        //Only required filters are shown (+'all' in all cases)
        if (!in_array($filter_name, $filters) && $filter_name != 'all') continue;
        ?>
        <li class="<?php echo isset($filter['filter_class']) ? $filter['filter_class'] : ''; ?>"
              data-action="filter"
              data-filter="<?php echo $filter['filter'] ?>">
                <?php echo Lang::tr($filter_name); ?> <span class="count hide-for-small-only">(<?php echo $filter['count']; ?>)</span>
            </li>
    <?php }
    ?>
</ul>
<div class="surveys small-12 medium-10 columns">
    <ul class="accordion surveys-accordion" data-accordion data-allow-all-closed="true">
        <?php
        //Reinit total counter
        $total_count = 0;
        foreach ($surveys as $survey) {

            //If survey matches no filters for this accordion, no need to continue
            if (SurveyFilters::matchesNo($filters, $survey))
                continue;

            //updating counts
            $total_count++;

            //Getting css classes matching filters
            $css_classes = SurveyFilters::applyFilters($filters, $survey);

            //If there is more than nb_max_displayed surveys it needs to be initially hidden

            //check is focus is requested for this survey
            $css_classes .= ($focus_id === $survey->id) ? ' focus' : '';

            ?>
            <li data-accordion-item class="survey-item-header <?php echo $css_classes . (($hide) ? ' hidden' : ''); ?>"
                data-survey-id="<?php echo $survey->id; ?>"
                data-human-readable-id="<?php echo $survey->human_readable_id; ?>">
                <a href="#" class="accordion-title <?php echo $css_classes; ?>"
                   data-index="<?php echo($total_count - 1); ?>">
                    <h3 class="survey-title small-4 large-8" data-pad-content title="<?php echo htmlentities($survey->title);?>">
                        <?php
                        //Idea to distinguish Answered from To be answered
                        if ($is_guest) {
                            echo (Participant::authenticatedUserHasAnswered($survey))?$answered_icon:$not_answered_icon;
                        }
                        echo substr($survey->title,0,50);
                        ?>
                        <span class="survey-description show-for-large"><?php echo $survey->description ? ' - ' . $survey->description : ''; ?></span>
                    </h3>
                    <menu class="survey-actions small-8 medium-8 large-4 columns text-right">
                        <?php echo SurveyActionUtil::getForSurvey($survey, $action_icons, $is_guest);  ?>
                    </menu>
                </a>
                <div class="survey-item-content accordion-content" data-tab-content>
                    <span class="hidden"><?php echo $survey->description . $survey->id; ?></span>
                    <div class="row large-11 medium-12">
                        <dl>
                            <div class="columns small-12 medium-3">
                                <dt>{tr:last_update} :</dt>
                                <dd data-timestamp="<?php echo $survey->updated; ?>" data-localize="date_time"></dd>
                            </div>
                            <div class="columns small-12 medium-3">
                                <dt>{tr:created} :</dt>
                                <dd data-timestamp="<?php echo $survey->created; ?>" data-localize="date_time"></dd>
                            </div>
                            <?php if ($survey->isClosed()) { ?>
                            <div class="columns small-12 medium-3">
                                <dt>{tr:closed} :</dt>
                                <dd data-timestamp="<?php echo $survey->closed; ?>" data-localize="date_time"></dd>
                            </div>
                            <?php } else if (is_numeric($survey->auto_close)) { ?>
                            <div class="columns small-12 medium-3">
                                <dt>{tr:auto_close} :</dt>
                                <dd data-timestamp="<?php echo $survey->auto_close; ?>" data-localize="date"></dd>
                            </div>
                            <?php } ?>
                            <div class="columns small-12 medium-3">
                                <dt>{tr:nb_questions} :</dt>
                                <dd><?php echo $survey->questions_count; ?></dd>
                            </div>
                            <?php if (!$is_guest || !$survey->hide_answers) { ?>
                            <div class="columns small-12 medium-3">
                                <dt>{tr:nb_participants} :</dt>
                                <dd><?php
                                    echo $survey->participants_count
                                        .($survey->limit_participants?'/' . $survey->limit_participants_nb:'');
                                    ?>
                                </dd>
                            </div>
                            <?php } ?>
                        </dl>
                    </div>
                    <?php if (!$survey->isClosed()) { ?>
                        <span class="row">{tr:link_to_survey}&nbsp;:
                            <a href="<?php echo $survey->path; ?>"><?php echo $survey->shortened_path; ?></a>
                        </span>
                    <?php } ?>
                </div>
            </li>
            <?php
            if (++$count > 10) {
                $hide = true;
            }
        }
        ?>
    </ul>
    <nav class="survey-accordion-bottom large-12 text-center">
        <?php
        Logger::info(microtime(true) - $start4);
        if ($count > 10) { ?>
            <span data-action="show_more" class="survey_button button">{tr:show_more}</span>
        <?php } ?>
        <span class="empty-message-container">
            {tr:no_survey}
        </span>
    </nav>
</div>