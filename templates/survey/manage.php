<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
//Recheck
$admin_page = Auth::isAdmin() && $admin_page;

//Filters shown on owner tab (Can be overriden)
$filtersForOwner = (new Event('filters_for_owner'))->trigger(function () {
    return array(
        'opened',
        'draft',
        'closed'
    );
});

//Filters shown on participant tab (Can be overriden)
$filtersForParticipant = (new Event('filters_for_participant'))->trigger(function () {
    return array(
        'opened',
        'closed',
        'answered',
        'not_answered'
    );
});

// Actions available on owner tab
$actionsByOwner = array(
    'edit' => array(
        'icon' => 'fa-pencil-square-o',
        'for' => array('opened', 'draft')
    ),
    'show_results' => array(
        'icon' => 'fa-bar-chart',
        'for' => array('opened', 'closed')
    ),
    'send_invitations_reminder' => array(
        'icon' => 'fa-envelope-o',
        'for' => array('opened')
    ),
    'duplicate' => array(
        'icon' => 'fa-files-o',
        'for' => array('opened', 'closed')
    ),
    'end_survey' => array(
        'icon' => 'fa-gavel',
        'for' => array('opened')
    ),
    'reopen_survey' => array(
        'icon' => 'fa-unlock-alt',
        'for' => array('closed')
    ),
    'reopen_survey_disabled' => array(
        'icon' => 'fa-unlock-alt disabled',
        'for' => array('closed')
    ),
    'delete' => array(
        'icon' => 'fa-trash-o',
        'for' => array('opened', 'draft', 'closed')
    )
);

// Actions available on participant tab
$actionsByInvited = array(
    'reply' => array(
        'icon' => 'fa-reply',
        'for' => array('opened', 'opened_wo_results')
    ),
    'show_results' => array(
        'icon' => 'fa-bar-chart',
        'for' => array('opened', 'closed')
    ),
    'duplicate' => array(
        'icon' => 'fa-files-o',
        'for' => array('opened', 'closed', 'opened_wo_results', 'closed_wo_results')
    )
);

$focus_id = '';
if (isset($_GET['focus_id'])) {
    $focus_id = $_GET['focus_id'];
}

?>
<section data-component="survey-manage" class="row collapse">
    <?php if (Config::get('use_survey_search')) { ?>
    <aside>
        <article class="hide-for-small medium-6 columns nopadding">
            &nbsp;
        </article>
        <article class="survey-search-container small-12 medium-6 columns nopadding">
            <input type="search" name="search_field" class="" placeholder="{tr:search_in_surveys}" tabindex="1"/>
        </article>
    </aside>
    <?php } ?>

    <ul class="tabs medium-centered text-center" data-tabs id="surveys-tabs">
        <?php if (!$admin_page) { ?>
            <li class="tabs-title small-5 small-offset-2 is-active" title="{tr:owned_by_tip}" data-tooltip>
                <a href="#ownedby" aria-selected="true">{tr:owned_by}</a>
            </li>
            <li class="tabs-title small-5" title="{tr:invited_to_tip}" data-tooltip>
                <a href="#invitedto">{tr:invited_to}</a>
            </li>
        <?php } else { ?>
            <li class="tabs-title small-5 small-offset-2 is-active" title="{tr:all_surveys_tip}" data-tooltip>
                <a href="#allSurveys">{tr:all_surveys}</a>
            </li>
        <?php } ?>
    </ul>
    <section data-container="tabs" class="tabs-content" data-tabs-content="surveys-tabs">
        <?php if (!$admin_page) { ?>
            <section id="ownedby" data-component="ownedby" class="tabs-panel is-active">
                <?php
                $count = 1;
                $surveys = Survey::ownedBy(Auth::user());

                Template::display('survey/tools/accordion', array('surveys' => $surveys,
                    'actions' => $actionsByOwner,
                    'filters' => $filtersForOwner,
                    'focus_id' => $focus_id,
                    'is_guest' => false
                ));
                ?>
            </section>
            <section id="invitedto" data-component="invitedto" class="tabs-panel">
                <?php
                $count = 1;
                //In this tab we display all survey answered and all survey where user is invited
                $surveys_invited = Survey::fromGuestUser(Auth::user());
                $surveys_answered = Survey::answeredBy(Auth::user());

                $surveys = array();
                $surveys_count_max = count($surveys_answered) + count($surveys_invited);

                //List must be sort with two dates sources (we must compare participant last activity AND last survey update)
                for ($i = 0; $i < $surveys_count_max; $i++) {
                    if (isset($surveys_answered[$i]) && isset($surveys_invited[$i])) { //conflict
                        $participant = Participant::getParticipant($surveys_answered[$i], array());
                        $last_activity = max($participant->answers_updated, $participant->answers_created);
                        if ($last_activity > $surveys_invited[$i]->updated) {
                            $surveys[] = $surveys_answered[$i];
                            array_splice($surveys_answered, $i, 1);
                        } else {
                            $surveys[] = $surveys_invited[$i];
                            array_splice($surveys_invited, $i, 1);
                        }
                        $i--;
                    } else if (isset($surveys_answered[$i])) {
                        $surveys[] = $surveys_answered[$i];
                    } else if (isset($surveys_invited[$i])) {
                        $surveys[] = $surveys_invited[$i];
                    } else {
                        break;
                    }
                }
                //We can be invited to a survey and had answered
                $surveys = array_unique($surveys, SORT_REGULAR);

                Template::display('survey/tools/accordion', array('surveys' => $surveys,
                    'actions' => $actionsByInvited,
                    'filters' => $filtersForParticipant,
                    'focus_id' => $focus_id,
                    'is_guest' => true
                ));
                ?>
            </section>
        <?php } else { ?>
            <section id="allSurveys" data-component="allSurveys" class="tabs-panel is-active">
                <?php
                $count = 1;
                $surveys = Survey::allSurveys();

                Template::display('survey/tools/accordion', array('surveys' => $surveys,
                    'actions' => $actionsByOwner,
                    'filters' => $filtersForOwner,
                    'focus_id' => $focus_id,
                    'is_guest' => false
                ));
                ?>
            </section>
        <?php } ?>
    </section>
</section>


