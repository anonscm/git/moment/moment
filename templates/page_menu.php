<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
$sub = isset($path) ? array_shift($path) : '';
?>
<nav class="row">
    <ul class="small-12 medium-8 large-6 small-centered">
        <li class="small-6 columns">
            <a class="radius tip-top create_survey <?php echo ($sub === 'create') ? 'selected' : ''; ?>"
               data-options="disable_for_touch:true" data-menu-action="" href="{url:survey/create}"
               data-tooltip  data-template-classes="tooltip-for-small" aria-haspopup="true" title="{tr:create_a_survey}">
                <span>
                    <img src="{img:create.svg}" alt="{tr:create_a_survey}" data-options="disable_for_touch:true"/>
                </span>
                <label>{tr:create_a_survey}</label>
            </a>
        </li>
        <li class="small-6 columns">
            <a class="radius tip-top manage_surveys <?php echo ($sub === 'manage') ? 'selected' : ''; ?>"
               data-options="disable_for_touch:true" data-menu-action="" href="{url:survey/manage}"
               data-tooltip  data-template-classes="tooltip-for-small" aria-haspopup="true" title="{tr:manage_surveys}" >
                <span>
                    <img src="{img:manage.svg}" alt="{tr:manage_surveys}" data-options="disable_for_touch:true"/>
                </span>
                <label>{tr:manage_surveys}</label>
            </a>
        </li>
    </ul>
</nav>
