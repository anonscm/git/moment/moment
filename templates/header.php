<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo Lang::getCode() ?>"
      xml:lang="<?php echo Lang::getCode() ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <title><?php echo MomentUtilities::getPageTitle(); ?></title>

    <?php MomentUtilities::includePageSpecificStylesheets(); ?>

    <?php GUI::includeStylesheets() ?>

    <?php GUI::includeFavicon() ?>

    <?php if(isset($no_header) && !$no_header) { ?>
        <script type="text/javascript" src="{path:app-data.js.php}"></script>

        <?php GUI::includeScripts() ?>

        <?php MomentUtilities::includePageSpecificScripts(); ?>
    <?php } ?>

    <meta name="robots" content="<?php echo GUI::getPage(null, 0) === null?'index, follow':'noindex, nofollow'; ?>" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body
        data-security-token="<?php echo Utilities::getSecurityToken() ?>"
        data-auth-type="<?php echo Auth::type() ?>"
        data-user-uid="<?php echo Utilities::userUID() ?>"
        data-dictionnary-uid="<?php echo Lang::getDictionnaryUID() ?>"
>
<div class="background"></div>
<header>
    <div class="title-bar hide-for-medium" data-responsive-toggle="main-menu" data-hide-for="medium">
        <button class="menu-icon top-bar-right" type="button" data-toggle></button>
        <div class="top-bar-left">
            <a href="{cfg:application_url}" title="{tr:home_page}">
                <?php GUI::includeLogo() ?>
            </a>
        </div>
    </div>
    <nav class="top-bar" id="main-menu">
        <menu class="top-bar-left">
            <ul class="dropdown menu" data-dropdown-menu>
                <li class="menu-text hide-for-small-only">
                    <a href="{cfg:application_url}" title="{tr:home_page}">
                        <?php GUI::includeLogo() ?>
                    </a>
                </li>
            </ul>
        </menu>
        <?php if(isset($no_header) && !$no_header) { ?>
            <menu class="top-bar-right">
                <ul class="menu icon-top dropdown"
                    data-back-button="<li class='js-drilldown-back'><a tabindex='0'>{tr:back}</a></li>"
                    data-responsive-menu="drilldown medium-dropdown">
                    <?php Template::display('menu') ?>
                </ul>
                <ul class="menu wayf_menu">
                    <?php
                    if (!Auth::isAuthenticated()) {
                        if (Config::get('use_wayf')) {
                            Template::display('wayf');
                        }
                    }
                    ?>
                </ul>
            </menu>
        <?php } ?>
    </nav>
    <!--            <h1>{cfg:application_name}</h1>-->
</header>
