<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */

// User_guide in redirection mode, every /user_guide/xxx will be redirected to path.guide_path/xxx
if(Config::get('user_guide.type') === 'redirection' && Config::get('user_guide.path') && preg_match('`^https?://`', Config::get('user_guide.path'))) {
    $baseUrl = Config::get('user_guide.path').Config::get('user_guide.guide_path');
    $baseUrl .= !(@substr_compare($baseUrl, '/', -strlen('/'))==0)?'/':'';
    header('Location: '.$baseUrl.implode('/', $path));
    exit;
}

//Getting sub page
$sub = array_shift($path);

//If no sub page is set, then use guide home page
if(Config::get('user_guide.home') && empty($sub)) {
    $sub = Config::get('user_guide.home');
}

?>
<section class='row dokuwiki' data-component="guide" data-current="<?php echo $sub; ?>">
    <?php
        $userGuide = new UserGuide();
        if ($userGuide->enabled){

            //Displaying table of content
            if(Config::get('user_guide.toc') && $userGuide->pagesContent[Config::get('user_guide.toc')] !== false) {
                echo $userGuide->pagesContent[Config::get('user_guide.toc')];
            }

            //If this page exists and is reachable, then display
            if(isset($userGuide->pagesContent[$sub]) && $userGuide->pagesContent[$sub] !== false) {
                echo $userGuide->pagesContent[$sub];
            } else {
                echo Template::display('user_guide_unreachable_page', array('page' => $sub));
            }

            echo "<br/>";
        }
    ?>
</section>