<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */

if (!isset($participant)) {
    throw new TemplateInternalMissingParameterException('participant');
}
if (!isset($current_user)) {
    $current_user = SurveyUserUtil::getCurrentUser($participant->Survey);
}

$envelope_icon = '';
if (!$participant->Survey->isClosed()
    && $current_user['role'] >= SurveyUserRoles::OWNER
    && ParticipantTypes::DECLARATIVE === $participant->type
    && $participant->name != '' && $participant->email != ''
) {
    $envelope_icon = '<span class="participant-actions">
                          <i class="fa fa-envelope-o" data-action="send_token_email"  data-participant-id="' . $participant->id . '" title="' . Lang::tr('send_token_email_notice') . '">
                          </i>
                      </span>';
}

?>
<th class="participant-name first participant-<?php echo $participant->hash; ?>" data-participant-ref="<?php echo $participant->hash; ?>">
    <label title="<?php echo $participant->ensure_name; ?>"><?php echo $participant->ensure_name; ?></label><?php echo $envelope_icon; ?>
</th>