<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */

if (!isset($current_user))
    $current_user = SurveyUserUtil::getCurrentUser($question->Survey);

if (!isset($is_results_page))
    $is_results_page = false;

/**
 * We are constructing the two rows at the same time, there maybe be better ways
 */
$participant_count = 0;

$header_row = '<tr class="header_row"><td class="first">'
    . Lang::tr('participant_count')->r(array('count' => $question->answers_count))
    . '</td>';
$bottom_row = '<tr class="bottom_row"><th class="first"></th>';

$onePropositionHasVariant = false;
$proposition_counter = 0;

foreach ($question->grouped_propositions as $groupName => $groupMembers) {
    $header_row .= '<th colspan="' . max(1, count($groupMembers)) . '" ';

    $top_winner = '';
    $header_hash = substr(sha1($groupName), 0, 8);

    $header_row .= 'data-proposition-header-ref="' . $header_hash . '" ';
    $header_row .= 'data-type="' . $question->type . '" ';

    $hasVariant = false;
    foreach ($groupMembers as $proposition) {

        $winner = (in_array($proposition->id, $winners)) ? 'winner' : '';

        $top_winner = (!empty($winner)) ? 'top-winner' : $top_winner;

        // If variant is long , then we had long-text class (length threshold is almost arbitrary)
        $long_variant = (QuestionTypes::TEXT == $question->type && strlen($proposition->text) > 28) ? ' long-text' : '';

        $th_start = '<th class="' . $winner .$long_variant. '" data-proposition-id="' . $proposition->id . '" data-proposition-header-ref="' . $header_hash . '" data-position="'.$proposition_counter.'">';
        $th_end = '</th>' . "\r\n";

        if (QuestionTypes::DATE == $question->type
            && $proposition instanceof DateProposition
        ) {
            $proposition_counter++;
            switch ($proposition->type) {
                case PropositionTypes::DAY_HOUR:
                    $hasVariant = true;
                    $bottom_row .= $th_start
                        . '<span data-timestamp="' . ($proposition->base_day + $proposition->base_time) . '" data-localize="time">'
                        . $proposition->base_time
                        . '</span>'
                        . $th_end;
                    break;
                case PropositionTypes::RANGE_OF_HOURS:
                    $hasVariant = true;

                    $bottom_row .= $th_start
                        . '<span class="'.PropositionTypes::RANGE_OF_HOURS.'"><span data-timestamp="' . ($proposition->base_day + $proposition->base_time) . '" data-localize="time">'
                        . $proposition->base_time
                        . '</span><br/>'
                        . ' '.Lang::tr('to_hour').' <br/><span data-timestamp="' . ($proposition->base_day + $proposition->end_time) . '" data-localize="time">'
                        . $proposition->end_time
                        . '</span></span>'
                        . $th_end;
                    break;
            }

        } else if ($proposition->text && $proposition->text != '') {
            $proposition_counter++;
            $hasVariant = true;
            $bottom_row .= $th_start . $proposition->text . $th_end;
        }


    }

    // If header is long , then we had long-text class
    $long_text = (QuestionTypes::TEXT == $question->type && strlen($groupName) > 28) ? 'long-text' : '';

    $header_row .= 'class="' . $top_winner . ' ' . $long_text . '"';

    if (!$hasVariant) {
        $header_row .= ' rowspan="2" data-proposition-id="' . $groupMembers[0]->id . '" data-position="'.$proposition_counter.'"';
        $proposition_counter++;
    }

    if (QuestionTypes::DATE == $question->type) {
        if (PropositionTypes::RANGE_OF_DAYS === $groupMembers[0]->type && $groupMembers[0]->base_day != $groupMembers[0]->end_day) {
            $header_row .= '><span data-timestamp="' . ($groupMembers[0]->base_day) . '" data-localize="date_utc_readable_single_line">' . $groupMembers[0]->base_day . '</span>';
            $header_row .= '<br/> '.Lang::tr('to_day').' <br/><span data-timestamp="' . ($groupMembers[0]->end_day) . '" data-localize="date_utc_readable_single_line">' . $groupMembers[0]->end_day . '</span>';;
        } else if (PropositionTypes::DAY === $groupMembers[0]->type
            || (PropositionTypes::RANGE_OF_DAYS === $groupMembers[0]->type && $groupMembers[0]->base_day == $groupMembers[0]->end_day)) {
            // base_day is midnight on UTC we convert it to midnight in Survey TimeZone
            $header_row .= ' data-timestamp="' . ($groupMembers[0]->base_day) . '" data-localize="date_utc_readable">' . $groupMembers[0]->base_day;
        } else {
            // when a time is fixed, we take this time into account for conversion
            $header_row .= ' data-timestamp="' . ($groupMembers[0]->base_day + $groupMembers[0]->base_time) . '" data-localize="date_readable">' . $groupMembers[0]->base_day;
        }
        $header_row .= '</th>';
    } else {
        $header_row .= '>' . $groupName . '</th>';
    }
    $header_row .= "\r\n";

    $onePropositionHasVariant |= $hasVariant;
}

$header_row .= '</tr>' . "\r\n";
$bottom_row .= '</tr>' . "\r\n";

if ($onePropositionHasVariant) {
    echo $header_row . $bottom_row;
} else {
    echo $header_row;
}


