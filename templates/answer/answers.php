<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */

if (!isset($question))
    throw new TemplateNotFoundException('missing_question');

//It returns proposition sorted by occurence of "yes" decreasing
$propositions_sorting = AnswerUtil::countPropositions($question);

$winners = array();
$winner_count = -1;

//Proposition_sorting is sorted by count descending
foreach ($propositions_sorting as $proposition_id => $counts) {
    $count = $counts[YesNoMaybeValues::YES];

    if ($count < $winner_count)
        break;

    if ($count === 0)
        continue;

    if ($winner_count == -1 || $winner_count === $count) {
        $winners[$proposition_id] = $proposition_id;
        $winner_count = $count;
    } else {
        break;
    }
}

$question_icon = 'calendar-icon';
if (QuestionTypes::DATE === $question->type && PropositionTypes::RANGE_OF_DAYS == $question->proposition_type) {
    $question_icon = 'calendar-period-icon';
}
if (QuestionTypes::TEXT === $question->type) {
    $question_icon = 'file-text-icon';
}
$dataInputMode = $question->input_mode?'data-question-input-mode="'.$question->input_mode.'"':'';

$momentsOfDay = Config::get('moments_of_day');
$momentsOfDayInfo = $momentsOfDayInfo = MomentUtilities::getMomentsOfDayInfos($momentsOfDay);
?>
<section class="survey-question <?php echo $question_icon; ?>">
    <h4 class="small-12 medium-9 ">
        <?php echo $question->statement; ?>
    </h4>
    <div class="hidden small-12 media-6 error_hint invalid-answer-message">
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
        <span></span>
    </div>
    <?php
    $hide_answers_initially = false;
    $ultimate_participant = $question->Survey->ultimate_participant;
    if ($ultimate_participant) {
        $hide_answers_initially = true;
        $answer = $ultimate_participant->sorted_answers[$question->id];
        $label = $answer->getLabels();
        ?>
        <section class="final-answer text-center">
            <section>
                <p class="callout success small-11 small-centered medium-8 medium-centered large-6 large-centered" data-closable=""> {tr:selected_final_answer} :<br /><b><?php echo $label; ?></b></p>
            </section>
            <span data-action="show_answers" class="survey_button button">{tr:show_answers}</span>
        </section>
    <?php }
    ?>
    <section class="<?php echo $hide_answers_initially ? 'hidden' : ''; ?>">
        <section class="answers survey-question-container small-12">
            <section class="answers-container">
                <table class="answer-table invisible" data-question-id="<?php echo $question->id; ?>" <?= $dataInputMode; ?>>
                    <thead>
                    <?php Template::display('answer/header_rows', array('question' => $question, 'winners' => $winners, 'is_results_page' => true)); ?>
                    </thead>
                    <tbody></tbody>
                    <tfoot><?php
                    Template::display('answer/sum_row',
                        array('question' => $question,
                            'winners' => $winners,
                            'propositions_sorting' => $propositions_sorting,
                            'is_results_page' => true,
                            'current_user' => $current_user
                        ));

                    /**
                     * Action row (get participants emails)
                     */
                    if (SurveyUserRoles::OWNER <= $current_user['role']) {
                        Template::display('answer/action_row', array('question' => $question));
                    }

                    /**
                     * Final answer selection (input)
                     **/
                    if (SurveyUserRoles::OWNER <= $current_user['role'] && !$ultimate_participant) {

                        $ultimate_answer = false;
                        if ($ultimate_participant && isset($ultimate_participant->sorted_answer[$question->id])) {
                            $ultimate_answer = $ultimate_participant->sorted_answer[$question->id];
                        }

                        Template::display('answer/final_row', array('question' => $question, 'ultimate_answer' => $ultimate_answer));
                    }
                    ?>
                    </tfoot>
                </table>
            </section>
            <?php if($question->input_mode === 'moments') {?>
            <span class="propositions_info_label small-12 medium-2">
                {tr:moments_of_day_info} <span class="fa fa-info-circle top" data-tooltip title="<?= $momentsOfDayInfo; ?>"></span>
            </span>
            <?php } ?>
        </section>
    </section>
</section>


