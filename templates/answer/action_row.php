<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */

/**
 * Template displaying a row corresponding to actions
 */
if (!isset($question))
    throw new TemplateInternalMissingParameterException('missing_question');

?>
<tr class="action_row" data-question-id="<?php echo $question->id; ?>">
    <th class="first">
        {tr:participants_emails}
        <span data-action="show_participants_emails" title="{tr:display_participants_emails}">@</span>
    </th>
    <?php
    foreach ($question->grouped_propositions as $groupName => $groupMembers) {
        foreach ($groupMembers as $proposition) {
            ?>
            <td class="proposition-actions final-choice"
                data-proposition-id="<?php echo $proposition->id; ?>"
            >
                <span data-action="show_participants_emails" title="<?php echo Lang::tr('display_participants_who_answered_yes_to').' '.$proposition->raw_label; ?>" data-tooltip>@</span>
            </td>
        <?php }
    }
    ?>
</tr>