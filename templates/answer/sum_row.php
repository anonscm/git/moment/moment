<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */

/**
 * Template displaying a row corresponding to a participant answer
 */
if (!isset($question))
    throw new TemplateInternalMissingParameterException('missing_question');

if (!isset($propositions_sorting))
    throw new TemplateInternalMissingParameterException('missing_propositions_sorting');

if (!isset($current_user))
    $current_user = SurveyUserUtil::getCurrentUser($question->Survey);

if (!isset($is_results_page))
    $is_results_page = false;

$total_answers_count = $question->answers_count;

?>
<tr class="results sum_row  <?php echo !$is_results_page?'hidden':''; ?>">
    <th class="first"><?php echo ($is_results_page && SurveyUserRoles::OWNER <= $current_user['role'] && $question->has_maybe_answers)?'{tr:sum_yes}':'{tr:sum}'; ?></th>
    <?php
    foreach ($question->grouped_propositions as $groupName => $groupMembers) {
        foreach ($groupMembers as $proposition) {
            //Checking if current wins
            $winner = isset($winners[$proposition->id]) ? 'winner' : '';

            $percentage = ($total_answers_count > 0) ? ($propositions_sorting[$proposition->id][YesNoMaybeValues::YES] / $total_answers_count) : 0;

            //Sum text can be overwritten
            $sum_text = (new Event('sum_text_init', $propositions_sorting[$proposition->id][YesNoMaybeValues::YES], $proposition))->trigger(function ($sum) {
                return $sum;
            });
            ?>
            <td class="sum <?php echo $winner . ' proposition_' . $proposition->id; ?>"
                data-percentage="<?php echo sprintf('%.2f', $percentage); ?>"
                data-proposition-id="<?php echo $proposition->id; ?>"
                title="{tr:satisfaction_rate} : <?php echo sprintf('%.2f%%', ($percentage * 100)); ?>">
                <?php echo $sum_text; ?>
            </td>
        <?php }
    }
    ?>
</tr>
<?php if($is_results_page && SurveyUserRoles::OWNER <= $current_user['role'] && $question->has_maybe_answers) {
    $maybe_winners = array();
    $maybe_winner = -1;

    //Finding yes+maybe winners
    foreach ($propositions_sorting as $proposition_id => $counts) {
        $count = $counts[YesNoMaybeValues::YES] + $counts[YesNoMaybeValues::MAYBE];

        if ($count === 0)
            continue;

        if ($count > $maybe_winner) {
            $maybe_winner = $count;
            $maybe_winners = array();
        }

        if ($count === $maybe_winner) {
            $maybe_winners[$proposition_id] = $proposition_id;
        }
    }
?>
<tr class="results sum_yes_plus_maybe_row">
    <th class="first">{tr:sum_yes_plus_maybe}</th>
    <?php
    foreach ($question->grouped_propositions as $groupName => $groupMembers) {
        foreach ($groupMembers as $proposition) {

            //Checking if current wins
            $winner = isset($maybe_winners[$proposition->id]) ? 'maybe-winner' : '';

            //Sum text is number of yes + numer of maybe (can be overwritten)
            $sum_text = (new Event('sum_text_init', $propositions_sorting[$proposition->id][YesNoMaybeValues::YES] + $propositions_sorting[$proposition->id][YesNoMaybeValues::MAYBE], $proposition))->trigger(function ($sum) {
                return $sum;
            });
            ?>
            <td class="sum_yes_plus_maybe <?php echo $winner; ?> proposition_<?php echo $proposition->id; ?>"
                data-proposition-id="<?php echo $proposition->id; ?>">
                <?php echo $sum_text; ?>
            </td>
        <?php }
    }
    ?>
</tr>
<?php } ?>