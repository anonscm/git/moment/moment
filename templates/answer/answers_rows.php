<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */

/**
 * Template displaying all answers row for a question
 */
if (!isset($question))
    throw new TemplateNotFoundException('missing_question');

if (!isset($winners))
    throw new TemplateNotFoundException('missing_winners');

if (!isset($current_user))
    $current_user = SurveyUserUtil::getCurrentUser($question->Survey);

if (!isset($is_results_page))
    $is_results_page = false;

// Should we display comment column
if (!isset($display_comment_column))
    $display_comment_column = !$question->Survey->hide_comments || (SurveyUserRoles::OWNER === $current_user['role'] && $is_results_page);

/**
 * Getting all propositions ids sorted
 */
$proposition_ids = array();
$proposition_classes = array();
foreach ($question->sorted_propositions as $proposition) {
    $proposition_ids[] = $proposition->id;
    //Checking if current wins
    $proposition_classes[$proposition->id] = isset($winners[$proposition->id]) ? 'winner' : '';
}

//Caching translation
$show_comment_translated = Lang::tr('unmask_comment');
//Cell counter
$cell_count = 0;

?>
<?php
foreach ($question->Answers as $answer) {
    if (isset($participant) && $answer->Participant->id === $participant->id)
        continue;

    //Ultimate participant should not be shown
    if (ParticipantTypes::ULTIMATE === $answer->Participant->type)
        continue;

    //Keeping current answer participant
    $answer_participant = $answer->Participant;
    ?>
    <tr class="results answer_row">
        <?php Template::display('answer/participant_informations', array('participant' => $answer_participant, 'current_user' => $current_user));
        foreach ($proposition_ids as $proposition_id) {
            $value = isset($answer->choices[$proposition_id])?$answer->choices[$proposition_id]->value:Answer::UNDEFINED;
?><td class="choice <?php echo $proposition_classes[$proposition_id] . ' proposition_' . $proposition_id . ' ' . $value; ?>" data-proposition-id="<?php echo $proposition_id; ?>" data-participant-ref="<?php echo $answer_participant->hash; ?>" data-value="<?php echo $value; ?>">
    <span class="<?php echo YesNoMaybeValues::$ICONS[$value]['icon']; ?>"></span>
</td><?php
        }

        //Comment column
        if ($display_comment_column) {
            $show_comment = $answer->comment
                && (!$answer->hide_comment
                    || ($answer->hide_comment && SurveyUserRoles::OWNER === $current_user['role']));

            $data_answer_id = '';
            if ($show_comment) {
                $data_answer_id = 'data-answer-id="' . $answer->id . '"';
            }
            ?>
            <th class="comment-column last" <?php echo $data_answer_id; ?> data-participant-ref="<?php echo $answer_participant->hash; ?>">
                <?php if ($show_comment) { ?>
                    <i class="fa fa-comment-o <?php echo ($answer->hide_comment) ? 'comment-masked' : ''; ?>" aria-hidden="true" title="<?php echo $show_comment_translated; ?>" data-action="show_comment"></i>
                <?php } else { ?>
                    <i class="fa fa-comment-o vhidden"></i>
                <?php } ?>
            </th>
        <?php } ?>
    </tr>
<?php } ?>