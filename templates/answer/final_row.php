<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */

if (!isset($question))
    throw new TemplateNotFoundException('missing_question');

if (!isset($ultimate_answer))
    throw new TemplateNotFoundException('missing_question');

$radio_uid = uniqid('proposition_');

?>
<tr class="final_answer_row" data-question-id="<?php echo $question->id; ?>">
    <th class="first">{tr:final_answer}</th>
    <?php
    foreach ($question->grouped_propositions as $groupName => $groupMembers) {
        foreach ($groupMembers as $proposition) {
            ?>
            <td class="final-choice" data-proposition-id="<?php echo $proposition->id; ?>"><?php
                $checked_id = null;
                if ($ultimate_answer) {
                    //If current value is 1 then radio must be checked
                    $checked_id = ($ultimate_answer->getValue($proposition->id) == YesNoMaybeValues::YES) ? $proposition->id : '';
                }
                echo (new FormFieldSingleRadio($radio_uid, array('checked_value' => $checked_id), $proposition->id))->getHTMLInput();
                ?>
            </td>
        <?php }
    }
    ?>
</tr>
