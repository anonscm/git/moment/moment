<section class="row" data-content='not_allowed'>
    <h3 class="small-12 medium-9 medium-centered columns text-center">{tr:user_guide_unreachable_title}</h3>
    <p>{tr:user_guide_unreachable_content}</p>
    <p>{tr:user_guide_unreachable_download} <a href="<?php echo Config::get('user_guide.path').Config::get('user_guide.guide_path').$page.'?do=export_pdf'; ?>" target="_blank">{tr:here}</a>.</p>
</section>