<div class="scroll-top-wrapper ">
        <span class="scroll-top-inner">
                <i class="fa fa-2x  fa-arrow-up"></i>
        </span>
</div>


<footer>
    <article class="hide-for-small-only">
    </article>
    <aside>
        <p class="row small-collapse medium-uncollapse">
            <span class="small-12 columns text-right">
                <a data-tooltip="" data-options="disable_for_touch:true" aria-haspopup="true"
                   href="{url:terms}" title="{tr:terms}">{tr:terms}</a> |
                <a data-tooltip="" data-options="disable_for_touch:true" aria-haspopup="true"
                   href="{url:legal}" title="{tr:legal_notice}">{tr:legal_notice}</a> |
                <a data-tooltip="" data-options="disable_for_touch:true" aria-haspopup="true"
                href="{url:user_guide}" title="{tr:help}">{tr:help}</a> |
                <a href="#" data-open="about_reveal" title="{tr:about}">{tr:about}</a> |
                <a href="https://sourcesup.renater.fr/projects/moment" target="_blank">{tr:moment_project}</a>
             </span>
        </p>
    </aside>
</footer>
<div class="reveal" id="about_reveal" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
  {tr:about_reveal}
</div>

</body>
</html>
