<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
$sub = array_shift($path);
if (Auth::isAdmin()) {
    if (in_array($sub, array('manage', 'update', 'results'))) {

        array_unshift($path, $sub);

        Template::display('survey_page', array('path' => $path, 'admin_page' => true));

        return;
    }
} else {
    throw new AuthUserNotAllowedException();
}


?>
<h4 class="page-title row">{tr:administration_of} {cfg:application_name}</h4>
<section class="row" data-component="admin">
    <article class="stats">
        <h3 class="">{tr:administration_actions}</h3>
        <ul class="menu ">
            <li class="button">
                <a href="{url:admin_users}">
                    {tr:user_management}
                </a>
            </li>
        </ul>
    </article>

    <article class="stats">
        <h3 class="">{tr:usage_stats}</h3>
        <?php $counter = 0; $closed = true;
        foreach (SurveyStats::get() as $stat_name => $value) {
            if(is_array($value)) {?>
                <h4><?php echo Lang::tr($stat_name); ?></h4>
                <?php
                    foreach ($value as $stat_name => $stat_value) {
                        $counter++;
                        if($counter == 1) { $closed = false; ?>
                            <div class="row">
                        <?php } ?>
                        <div class="small-3 columns end">
                            <p><?php echo Lang::tr($stat_name); ?></p>
                            <div class="stat"><?php echo $stat_value; ?></div>
                        </div>
                        <?php  if($counter == 4) { $counter= 0 ; $closed = true; ?>
                            </div>
                        <?php }
                    }
                    if(!$closed) {?>
                        </div>
                    <?php }
                    $counter = 0;
                ?>
            <?php
            } else {
            ?>
                <div class="small-3 columns">
                <p><?php echo Lang::tr($stat_name); ?></p>
                <div class="stat"><?php echo $value; ?></div>
                </div>
        <?php }
            } ?>
    </article>
</section>

