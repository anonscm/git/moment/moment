<?php
    $wayf = 'https://discovery.renater.fr/' . (preg_match('`^(dev|preprod)-`', $_SERVER['SERVER_NAME']) ? 'test' : 'renater') . '/WAYF';
    $host = urlencode(Config::get('application_url'));
?>

<div id="embeddedWAYFsmall" class="show-for-small-only">
    <h1>{tr:logon}</h1>
    <!-- EMBEDDED-WAYF-START -->
    <script type="text/javascript">
        $(function() {
            if($('#embeddedWAYFsmall').is(':visible'))
                $('#wayf_div').appendTo('#embeddedWAYFsmall');
        });
    </script>
    <noscript>
        <p>
            <strong>Login:</strong> Javascript is not available for your web browser. Therefore, please <a
                    href="<?php echo $wayf ?>?entityID=<?php echo $host; ?>&return=<?php echo $host; ?>Shibboleth.sso%2FLogin%3FSAMLDS%3D1">proceed
                manually</a>.
        </p>
    </noscript>

    <!-- EMBEDDED-WAYF-END -->
</div>
