<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
if (count($survey->Questions) != 0) { ?>
    <section class="survey-questions">
        <?php
        //Getting current answers
        $answers = array();

        if ($participant) {
            $answers = $participant->sorted_answers;
        }

        $momentsOfDay = Config::get('moments_of_day');
        $momentsOfDayInfo = MomentUtilities::getMomentsOfDayInfos($momentsOfDay);

        foreach ($survey->Questions as $question) {
            $question_icon = 'calendar-icon';

            if (QuestionTypes::DATE === $question->type && PropositionTypes::RANGE_OF_DAYS == $question->proposition_type) {
                $question_icon = 'calendar-period-icon';
            }

            if (QuestionTypes::TEXT === $question->type) {
                $question_icon = 'file-text-icon';
            }
            $proposition_count = count($question->propositions);
            $dataInputMode = $question->input_mode?'data-question-input-mode="'.$question->input_mode.'"':'';

            ?>

            <section class="survey-question question-type <?php echo $question_icon; ?> row collapse">
                <h4 class="small-12 medium-9 " tabindex="0" aria-label="<?php echo $question->statement; ?>"><?php echo $question->statement; ?>
                    <small class="help-text">
                        <?php
                        echo ($question->force_unique_choice || $proposition_count == 1) ?
                            '(' . Lang::tr('one_possible_choice')->r(array('count' => $proposition_count)) . ')'
                            : '(' . Lang::tr('several_possible_choices')->r(array('count' => $proposition_count)) . ')';
                        ?>
                    </small>
                </h4>
                <?php
                //This is the main table (header_row answers_row answereing_row sum_row)
                $answer = isset($answers[$question->id]) ? $answers[$question->id] : null;
                ?>
                <div class="hidden small-12 media-6 error_hint invalid-answer-message">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    <span></span>
                </div>
                <section class="answers survey-question-container small-12">
                    <section class="answers-container">
                        <table class="answer-table invisible"
                               data-form="question-form-reply"
                               data-force-unique-choice="<?= $question->force_unique_choice; ?>"
                               data-question-id="<?= $question->id; ?>"
                               data-question-title="<?= $question->statement; ?>"
                               <?= $dataInputMode; ?>
                               data-answer-id="<?= isset($answer) ? $answer->id : ''; ?>">
                            <thead>
                            <?php
                            Template::display('answer/header_rows',
                                array('question' => $question,
                                    'winners' => array(), 'participant' => $participant));
                            ?>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                            <?php
                            echo (new FormFieldChoice('proposition', $question, $answer))->getHTML();

                            if (!$question->Survey->hide_answers) {
                                Template::display('answer/sum_row',
                                    array('question' => $question,
                                        'winners' => array(),
                                        'propositions_sorting' => AnswerUtil::countPropositions($question)));
                            }

                            ?>
                            </tfoot>
                        </table>
                    </section>
                </section>
                <?php
                //Displaying select-for-all only when necessary (no answer selected and answer edition disabled)
                if (!$question->force_unique_choice && !(isset($answer) && $question->Survey->disable_answer_edition)) {
                    ?>
                <span class="select-for-all-container small-12 large-4">
                    <span class="select-for-all-label">
                        {tr:select_for_all} :
                    </span>
                    <label class="button tiny success hollow" title="" data-value="selected_value_yes" role="button" tabindex="0">
                        {tr:selected_value_yes}
                    </label>
                    <?php
                    if ($question->enable_maybe_choices) {
                        ?>
                        <label class="button tiny warning hollow" title="" data-value="selected_value_maybe" role="button" tabindex="0">
                        {tr:selected_value_maybe}
                        </label>
                    <?php
                        }
                    ?>
                    <label class="button tiny alert hollow" title="" data-value="selected_value_no" role="button" tabindex="0">
                        {tr:selected_value_no}
                    </label>
                </span>
                    <?php
                }
                if ((!$question->force_unique_choice && !(isset($answer) && $question->Survey->disable_answer_edition) && !$question->enable_maybe_choices) || ($question->input_mode === 'moments')) {?>
                <span class="small-12 large-8">
                <?php } ?>
                <?php if (!$question->force_unique_choice && !(isset($answer) && $question->Survey->disable_answer_edition) && !$question->enable_maybe_choices) {?>
                <span class="propositions_info_label">
                    {tr:maybe_answers_disabled} <span class="fa fa-info-circle top" data-tooltip title="{tr:maybe_answers_disabled_info}"></span>
                </span>
                <?php } ?>
                <?php if($question->input_mode === 'moments') {?>
                <span class="propositions_info_label">
                    {tr:moments_of_day_info} <span class="fa fa-info-circle top" data-tooltip title="<?= $momentsOfDayInfo; ?>"></span>
                </span>
                <?php } ?>
                <?php if ((!$question->force_unique_choice && !(isset($answer) && $question->Survey->disable_answer_edition) && !$question->enable_maybe_choices) || ($question->input_mode === 'moments')) {?>
                </span>
                <?php } ?>
                <section class="question-comment-container clear">
                    <?php
                    //Finally the comment form
                    $has_commented = isset($answers[$question->id]) ? strlen($answers[$question->id]->comment) > 0 : false;
                    $comment_switch = new FormFieldSwitch('leave_a_comment', array('label' => 'leave_a_comment'), $has_commented);
                    echo $comment_switch->getHTML();
                    ?>
                    <div class="comment-form <?php echo ($has_commented) ? 'shown' : 'hidden' ?>">
                        <?php
                        $comment_field = new FormFieldTextarea('comment',
                            array('maxlength' => 200),
                            isset($answers[$question->id]) ? $answers[$question->id]->comment : '');
                        echo $comment_field->getHTMLInput();
                        ?>
                    </div>
                </section>
            </section>
        <?php } ?>
        <?php if (Config::get('must_accept_terms') && !$survey->is_draft && !isset($participant)) { ?>
        <section class="accept_terms_container small-12 medium-centered text-center">
            <?php
                $accept_terms =  new FormFieldCheckbox('accept_terms', array('label' => 'accept_terms', 'required' => false), false);
                echo $accept_terms->getHTML();
            ?>
        </section>
        <?php } ?>
        <?php
            $update = count($answers) > 0
        ?>
        <section class="survey-reply-controls text-center <?php echo(($update) ? 'button-group' : ''); ?>">
            <?php
            //If answer edition is disabled we don't show the button, (action is also inefficient)
            if ($update && $survey->disable_answer_edition) { ?>
                <button title="{tr:answers_cannot_be_deleted}" data-tooltip class="survey_button button disabled">{tr:delete_answers}</button>
                <button title="{tr:answers_cannot_be_edited}" data-tooltip class="survey_button button disabled">
                    {tr:validate_answers}
                </button>
            <?php } else if (!$survey->is_draft) { ?>
                <?php if($update) { ?>
                    <button data-action="delete_answers" class="survey_button button">{tr:delete_answers}</button>
                <?php } ?>
                <button data-action="send_answers" data-sub-action="<?php echo($update ? 'update' : 'send') ?>"
                        class="survey_button button <?php echo(($update && !$survey->disable_answer_edition) ? 'highlight' : ''); ?>">{tr:validate_answers}
                </button>
            <?php } ?>
            <div class="reveal" id="answersSaved" data-reveal>
                <h2>{tr:answers_saved}</h2>
                <p>
                    <i>{tr:answers_saved_info}</i>
                    <br/><br/>
                    <?php if (!$survey->disable_answer_edition) { ?>
                        {tr:you_can_edit_your_answers} <a id="answer-edition-link" href="">{tr:here}</a>
                    <?php } ?>
                </p>
                <i class="info hidden">{tr:you_will_be_redirected_to_edition_page_in} <span class="counter"></span>
                    {tr:time_seconds}</i>
                <button class="close-button" data-close aria-label="Close modal" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="reveal participant_already_exists" id="participant_already_exists" data-form="recovery-form" data-reveal data-options="closeOnClick:false">
                <h2>{tr:you_already_answered_this_survey}</h2>
                <p>
                    {tr:participant_with_this_email_already_exists}
                </p>
                <div class="declarative_participant_recovery hidden">
                    <p>{tr:declarative_participant_already_exists}</p>
                    <?php
                        echo (new FormFieldText('recovery_code',
                                array('required' => true,
                                    'placeholder' => Lang::tr('enter_the_code_here')->out(),
                                    'label' => 'please_fill_in_the_code'
                                )))->getHTML();
                    ?>
                    <div class="small-4 columns">
                        <span data-action="send_recovery_code" tabindex="0">{tr:send_me_another_recovery_code}</span>
                        <span class="fa fa-info-circle top" data-tooltip data-v-offset="15" tabindex="0" title="{tr:if_dont_receive_code}"></span>
                    </div>
                    <div class="small-8 columns text-right">
                    <button data-action="send_answers_with_recovery_code" data-sub-action="update"
                            class="survey_button button disabled left"
                            data-tooltip data-v-offset="15" tabindex="0" title="{tr:no_code}"
                            aria-disabled="true" >{tr:update_answer}
                    </button>
                    <button class="survey_button button" data-close aria-label="{tr:cancel}" type="button">{tr:cancel}</button>
                    </div>
                </div>
                <div class="authenticated_participant_recovery hidden">
                    <p>{tr:authenticated_participant_already_exists}</p>
                    <span data-action="log_in_recovery" data-logon-url="<?php echo AuthSP::logonURL(); ?>" class="survey_button button">{tr:logon}</span>
                    <button class="survey_button button" data-close aria-label="{tr:cancel}" type="button">{tr:cancel}</button>
                </div>
            </div>
            <div class="reveal participant_already_exists" id="participant_already_exists_edition_disabled" data-reveal data-options="closeOnClick:false">
                <h2>{tr:answers_modification_impossible}</h2>
                <p>
                    {tr:participant_with_this_email_already_exists_edition_disabled}
                </p>
                <div class="declarative_participant_recovery hidden">
                    {tr:declarative_participant_already_exists_edition_disabled}<br/>
                    <a data-action="send_token_email" href="#">{tr:send_link_to_previous_answers}</a>
                </div>
                <div class="authenticated_participant_recovery hidden">
                    {tr:authenticated_participant_already_exists_edition_disabled}
                    <span data-action="log_in" data-logon-url="<?php echo AuthSP::logonURL(); ?>" class="survey_button button">{tr:logon}</span>
                    <button class="survey_button button" data-close aria-label="{tr:cancel}" type="button">{tr:cancel}</button>
                </div>
            </div>
        </section>

    </section>
<?php } ?>