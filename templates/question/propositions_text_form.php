<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
$hidden = '';
if (array_key_exists('type', $question_data)) {
    if($question_data['type'] !== QuestionTypes::TEXT) return;
} else {
    $hidden = 'hidden';
    $question_data['type'] = QuestionTypes::TEXT;
}
?>
<section class="small-12 medium-centered <?= $hidden ?>" data-form-proposition="text">
    <h1 class="your_propositions"><?= Lang::tr('your_text_propositions'); ?></h1>
    <p class="small-12 medium-8 large-6 small callout primary no-proposition-found hidden">
        <span class="fa fa-info-circle"></span> <?= Lang::tr('please_add_at_least_one_proposition'); ?>
    </p>
    <div class="propositions_container">
        <div class="same_variants_for_all_row row small-collapse medium-uncollapse">
            <label>
                <input type="checkbox" name="same_variants_for_all"/>
                <?= Lang::tr('same_variants_for_all'); ?>
            </label>
        </div>
        <ul class="propositions">
            <?php if (empty($question_data['propositions'])) { ?>
                <li class="proposition" data-index="0">
                    <div>
                        <label for="proposition_1">
                            <?= Lang::tr('proposition'); ?> 1
                        </label>
                        <button data-action="delete_proposition" class="fa fa-trash" title="<?= Lang::tr('delete_proposition'); ?>" type="button"></button>
                        <span class="proposition_controls">
                            <button data-action="add_proposition_variant"
                                title="<?= Lang::tr('add_proposition_variant'); ?>" type="button">
                                <span class="fa fa-plus-circle" type="button"></span>
                                <?= Lang::tr('add_proposition_variant'); ?>
                            </button>
                        </span>
                    </div>
                    <input class="input medium" type="text" name="proposition_header" placeholder="<?= Lang::tr('proposition_header'); ?>" data-no-binding required value=""/>
                    <input class="input small" type="text" name="proposition_variant" placeholder="<?= Lang::tr('variant'); ?>" data-no-binding value=""/><!-- Do not delete
                    --><input class="input small" type="text" name="proposition_variant" placeholder="<?= Lang::tr('variant'); ?>" data-no-binding value=""/><!-- Do not delete
                    --><input class="input small" type="text" name="proposition_variant" placeholder="<?= Lang::tr('variant'); ?>" data-no-binding value=""/>
                    <span class="variants_labels">
                    </span>
                </li>
            <?php } else if ($question_data['type'] === QuestionTypes::TEXT) {
                $index = 0;
                foreach ($question_data['propositions'] as $header => $propositions) { ?>
                    <li class="proposition">
                        <div>
                            <label for="proposition_<?php echo($index + 1); ?>">
                                <?= Lang::tr('proposition'); ?> <?php echo($index + 1); ?>
                            </label>
                            <button data-action="delete_proposition" class="fa fa-trash" title="<?= Lang::tr('delete_proposition'); ?>" type="button"></button>
                            <span class="proposition_controls">
                            <button data-action="add_proposition_variant"
                                    title="<?= Lang::tr('add_proposition_variant'); ?>" type="button">
                                <span class="fa fa-plus-circle" type="button"></span>
                                <?= Lang::tr('add_proposition_variant'); ?>
                            </button>
                        </span>
                        </div>
                        <input class="input medium" type="text" name="proposition_header" placeholder="<?= Lang::tr('proposition_header'); ?>"
                               data-proposition-id="<?php echo $propositions[0]['id']; ?>"
                               data-no-binding required value="<?php echo htmlspecialchars($header); ?>"/>
                        <?php
                        foreach ($propositions as $proposition) {
                            if (!empty($proposition['text'])) {
                                ?><input data-proposition-id="<?php echo $proposition['id']; ?>" class="input small" type="text"
                                       name="proposition_variant" placeholder="<?= Lang::tr('variant'); ?>" data-no-binding
                                       value="<?php echo htmlspecialchars($proposition['text']); ?>"/><?php
                            }
                        }
                        ?>
                        <span class="variants_labels">
                        </span>
                    </li>
                    <?php
                    $index++;
                }
            }
            ?>
        </ul>
    </div>
    <div class="small-12 medium-8 large-6  hidden error_hint invalid-question-message">
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
        <span><?= Lang::tr('invalid_question'); ?></span>
    </div>
    <span data-action="add_text_question_proposition" tabindex="0"><span class="fa fa-plus-circle"></span> <?= Lang::tr('add_text_question_proposition'); ?></span>
</section>