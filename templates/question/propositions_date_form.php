<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
$hidden = '';
if (array_key_exists('type', $question_data)) {
    if($question_data['type'] !== QuestionTypes::DATE) return;
} else {
    $hidden = 'hidden';
    $question_data['type'] = QuestionTypes::DATE;
}

$multiPickerValue = '';

if($question_data['type'] === QuestionTypes::DATE && !empty($question_data['propositions'])) {
    $tmp = array();
    foreach ($question_data['propositions'] as $index => $propositions) {
        $tmp[$propositions[0]['base_day']] = $propositions[0]['base_day'];
    }
    $multiPickerValue = implode(',', $tmp);
}

$range = (isset($question_data['proposition_type']) && PropositionTypes::RANGE_OF_DAYS == $question_data['proposition_type']);

$showSameHourForAll = !isset($question_data['id'])
    || PropositionTypes::DAY == $question_data['proposition_type']
    || PropositionTypes::DAY_HOUR == $question_data['proposition_type']
    || PropositionTypes::RANGE_OF_HOURS == $question_data['proposition_type'];


$momentsOfDay = Config::get('moments_of_day');
$inputModes = ['hours'];
$inputModesInfo = [Lang::tr('input_mode_info_hours')->out()];
if($momentsOfDay && count($momentsOfDay) > 0) {
    $inputModes[] = 'moments';
    $inputModesInfo[] = Lang::tr('input_mode_info_moments')->out().MomentUtilities::getMomentsOfDayInfos($momentsOfDay);
}
$inputModesInfo = implode('<br /><br />', $inputModesInfo);


$inputMode = 'hours';
$tz = '';
if($momentsOfDay && isset($question_data['options']) && isset($question_data['options']['input_mode'])) {
    $inputMode = $question_data['options']['input_mode'];
    $tz = ($question_data['options']['input_mode'] === 'moments' && isset($question_data['options']['time_zone'])) ? $question_data['options']['time_zone']:'';
}

?>
<section class="medium-centered <?= $hidden ?>" data-form-proposition="date">
    <h1 class="your_propositions">
        <span><?= Lang::tr('your_date_propositions'); ?></span> <i class="fa fa-info-circle" data-make-tooltip title="<?= Lang::tr('your_date_propositions_info'); ?>"></i>
    </h1>
    <div class="column large-4 nopadding">
        <?php if (!isset($question_data['id']) || !$range) { ?>
        <div class="date-picker-multi-container <?php echo $range?'hidden':''; ?>">
            <input class="input date-picker-multi" data-localize="date" data-timestamp="<?php echo $multiPickerValue; ?>" type="text" placeholder="<?= Lang::tr('please_select_dates'); ?>" value=""/>
        </div>
        <?php } ?>
        <?php if (!isset($question_data['id']) || $range) { ?>
        <div class="date-picker-range-container <?php echo $range?'':'hidden'; ?>">
            <div class="clear">
                <input class="input date-picker-range" type="text" placeholder="<?= Lang::tr('please_select_dates'); ?>" value=""/>
            </div>
            <div class="date-picker-range-input-container">
                <div class="columns small-5 nopadding text-center">
                    <label><?= Lang::tr('start_date'); ?>
                        <input class="input date-picker-range-start" type="text" placeholder="<?= Lang::tr('readable_date_format'); ?>" value="" data-no-binding />
                    </label>
                </div>
                <div class="columns small-2 text-center"><br/><?= Lang::tr('to_day'); ?></div>
                <div class="columns small-5 nopadding text-center">
                    <label><?= Lang::tr('end_date'); ?>
                        <input class="input date-picker-range-end" type="text" placeholder="<?= Lang::tr('readable_date_format'); ?>" value="" data-no-binding />
                    </label>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="column large-8">
        <p class="callout small primary no-proposition-found hidden">
            <?= Lang::tr('no_date_selected'); ?>
        </p>
        <p class="timezone-label label secondary hidden">
            <span class="hide-for-small-only hide-for-medium-only timezone-large"><?= Lang::tr('time_zone_info_hours_large'); ?></span>
            <span class="hide-for-large timezone-small"><?= Lang::tr('time_zone_info_hours_small'); ?></span> : <span class="timezone-value" data-localize="time_zone"></span>
        </p>
        <div class="propositions_container">
            <?php if ($showSameHourForAll) { ?>
            <div class="same_for_all_row row collapse small-12">
                <?php if(count($inputModes) > 1) { ?><label class="columns small-11 medium-7">
                    <span data-label-text=""><?= Lang::tr('input_mode'); ?></span>
                    <select data-no-binding name="input_mode">
                        <?php foreach($inputModes as $value) { ?>
                            <option value="<?= $value ?>" <?php if($inputMode === $value) echo 'selected'; ?>><?= Lang::tr('input_mode_' . $value); ?></option>
                        <?php } ?>
                    </select>
                    <input data-no-binding type="hidden" name="question_time_zone" value="<?= $tz ?>" />
                    <span class="fa fa-info-circle" data-make-tooltip title="<?= $inputModesInfo ?>"></span>
                    </label><?php } ?>
                <label class="columns small-12 medium-5">
                    <input type="checkbox" name="same_for_all" checked="checked"/>
                    <span><?= Lang::tr('same_hours_for_all'); ?></span>
                </label>
            </div>
            <?php } ?>
            <ul class="propositions <?= $showSameHourForAll?'same_for_all':''; ?> <?= $inputMode; ?>">
                <?php if (empty($question_data['propositions'])) { ?>
                    <li class="proposition proposition_date_input row small-collapse medium-uncollapse">
                        <div class="date_container" >
                            <label for="proposition_1">
                                <?= Lang::tr('date'); ?> 1
                            </label>
                            <button data-action="delete_proposition" class="fa fa-trash" title="<?= Lang::tr('delete_date'); ?>" type="button"></button>
                            <span class="proposition_controls">
                                <button data-action="add_time_picker" type="button">
                                    <span class="fa fa-plus-circle" title="<?= Lang::tr('add_time_picker'); ?>"></span>
                                    <?= Lang::tr('add_time_picker'); ?>
                                </button>
                            </span>
                        </div>
                        <span class="small-12 medium-3 large-2 columns">
                            <input type="text" name="base_date"
                                   placeholder="<?= Lang::tr('readable_date_format'); ?>"
                                   class="date_picker medium"
                                   data-no-binding
                                   aria-autocomplete="none"
                                   autocomplete="off"
                                   value=""
                                   required />
                        </span>
                        <span class="hours_container small-10 end columns nopadding">
                            <span class="time_pickers">
                                <span class="time_picker_container">
                                    <span class="time_slot">
                                        <span class="time_container base_time_container">
                                            <input type="text" name="base_time" placeholder="<?= Lang::tr('readable_time_format'); ?>"
                                                   class="time_picker small" value="" autocomplete="off" aria-autocomplete="none" data-no-binding>
                                        </span>
                                        <span class="to date_container hidden">
                                            <?= Lang::tr('to_day'); ?>
                                            <input type="text" name="end_date" placeholder="<?= Lang::tr('readable_date_format'); ?>"
                                                   class="date_picker medium" value="" autocomplete="off" aria-autocomplete="none" data-no-binding>
                                        </span>
                                        <span class="to time_container hidden">
                                            <?= Lang::tr('to_hour'); ?>
                                            <input type="text" name="end_time" placeholder="<?= Lang::tr('readable_time_format'); ?>"
                                                   class="time_picker small" value="" autocomplete="off" aria-autocomplete="none" data-no-binding>
                                        </span>
                                    </span>
                                    <button class="fa fa-trash" data-action="remove_time_picker" title="<?= Lang::tr('delete_time_picker'); ?>" type="button"></button>
                                </span>
                            </span>
                            <span class="moments_pickers">
                                <label><input type="checkbox" name="morning" /><?= Lang::tr('morning'); ?></label>
                                <label><input type="checkbox" name="noon" /><?= Lang::tr('noon'); ?></label>
                                <label><input type="checkbox" name="afternoon" /><?= Lang::tr('afternoon'); ?></label>
                                <label><input type="checkbox" name="evening" /><?= Lang::tr('evening'); ?></label>
                            </span>
                        </span>
                        <span class="hours_labels"></span>
                        <span class="moments_labels"></span>
                        <div class="advanced_controls small-12 columns">
                            <label>
                                <input type="checkbox" name="whole_day" />
                                <?= Lang::tr('whole_day'); ?>
                            </label>
                            <label>
                                <input type="checkbox" name="slot" checked="checked"/>
                                <?= Lang::tr('slot'); ?>
                            </label>
                        </div>
                    </li>
                <?php } else if ($question_data['type'] === QuestionTypes::DATE) {
                    $count = 0;
                    foreach ($question_data['propositions'] as $index => $propositions) {
                        ?>
                        <li class="proposition proposition_date_input row small-collapse medium-uncollapse">
                            <div>
                                <label for="proposition_<?php echo($count + 1); ?>" class="<?php echo $range?'day_slot_label secondary label':''; ?>">
                                    <span data-timestamp="<?php echo $propositions[0]['base_day'] + (array_key_exists('base_time', $propositions[0]) ?$propositions[0]['base_time']:0); ?>"
                                          data-localize="date<?php echo array_key_exists('base_time', $propositions[0]) ?'':'_utc'; ?>">
                                    </span>
                                <?php
                                    if(array_key_exists('end_day', $propositions[0])) { ?>
                                        <?= Lang::tr('to_day'); ?>
                                        <span data-timestamp="<?php echo $propositions[0]['end_day'] + (array_key_exists('base_time', $propositions[0]) ?$propositions[0]['base_time']:0); ?>"
                                              data-localize="date<?php echo array_key_exists('base_time', $propositions[0]) ?'':'_utc'; ?>">
                                        </span>
                                <?php }
                                ?></label>
                                <button data-action="delete_proposition" class="fa fa-trash" title="<?= Lang::tr('delete_date'); ?>" type="button"></button>
                                <span class="proposition_controls">
                                    <button data-action="add_time_picker" type="button">
                                        <span class="fa fa-plus-circle" title="<?= Lang::tr('add_time_picker'); ?>"></span>
                                        <?= Lang::tr('add_time_picker'); ?>
                                    </button>
                                </span>
                            </div>
                            <span class="small-12 medium-3 large-2 columns">
                                <input type="text" name="base_date" class="date_picker medium"
                                       placeholder="<?= Lang::tr('readable_date_format'); ?>"
                                       data-proposition-id="<?php echo $propositions[0]['id']; ?>"
                                       data-timestamp="<?php echo $propositions[0]['base_day'] + (array_key_exists('base_time', $propositions[0]) ?$propositions[0]['base_time']:0); ?>"
                                       data-localize="date<?php echo array_key_exists('base_time', $propositions[0]) ?'':'_utc'; ?>"
                                       data-no-binding
                                       aria-autocomplete="none"
                                       autocomplete="off"
                                       value=""/>
                            </span>
                            <span class="hours_container small-10 end columns nopadding">
                                <span class="time_pickers">
                                <?php
                                foreach ($propositions as $index => $time_proposition) { ?>
                                    <span class="time_picker_container" data-proposition-id="<?php echo $time_proposition['id']; ?>">
                                        <span class="time_slot">
                                            <span class="time_container base_time_container">
                                                <input type="text" name="base_time" placeholder="<?= Lang::tr('readable_time_format'); ?>"
                                                       class="time_picker small" autocomplete="off"
                                                       data-timestamp="<?php echo(array_key_exists('base_time', $time_proposition) ? $time_proposition['base_day'] + $time_proposition['base_time'] : ''); ?>"
                                                       data-localize="time" value="" data-no-binding>
                                            </span>
                                            <span class="to date_container <?php echo(array_key_exists('end_day', $time_proposition) ? '' : 'hidden'); ?>">
                                                <?= Lang::tr('to_day'); ?>
                                                <input type="text" name="end_date" placeholder="<?= Lang::tr('readable_date_format'); ?>"
                                                       class="date_picker medium"
                                                       data-timestamp="<?php echo(array_key_exists('end_day', $time_proposition) ? $time_proposition['end_day'] : ''); ?>"
                                                       data-localize="date_utc" value="" autocomplete="off" aria-autocomplete="none" data-no-binding>
                                            </span>
                                            <span class="to time_container">
                                                <?= Lang::tr('to_hour'); ?>
                                                <input type="text" name="end_time" placeholder="<?= Lang::tr('readable_time_format'); ?>"
                                                       class="time_picker small"
                                                       data-timestamp="<?php echo(array_key_exists('end_time', $time_proposition) ? $time_proposition['base_day'] + $time_proposition['end_time'] : ''); ?>"
                                                       data-localize="time" value="" autocomplete="off" aria-autocomplete="none" data-no-binding>
                                            </span>
                                        </span>
                                        <button class="fa fa-trash" data-action="remove_time_picker" title="<?= Lang::tr('delete_time_picker'); ?>" type="button">
                                        </button>
                                    </span><?php }
                                ?></span>
                                <span class="moments_pickers">
                                    <label><input type="checkbox" name="morning" /><?= Lang::tr('morning'); ?></label>
                                    <label><input type="checkbox" name="noon" /><?= Lang::tr('noon'); ?></label>
                                    <label><input type="checkbox" name="afternoon" /><?= Lang::tr('afternoon'); ?></label>
                                    <label><input type="checkbox" name="evening" /><?= Lang::tr('evening'); ?></label>
                                </span>
                            </span>
                            <span class="hours_labels"></span>
                            <span class="moments_labels"></span>
                            <div class="advanced_controls advanced_controls small-12 columns">
                                <label>
                                    <input type="checkbox" name="whole_day"
                                        <?php $whole_day = (PropositionTypes::DAY === $propositions[0]['type'] || PropositionTypes::RANGE_OF_DAYS === $propositions[0]['type']);
                                            echo $whole_day ? 'checked="checked"' : ''; ?>/>
                                </label>
                                <label>
                                    <input type="checkbox" name="slot"
                                        <?php  echo (PropositionTypes::RANGE_OF_DAYS === $propositions[0]['type'] || PropositionTypes::RANGE_OF_HOURS === $propositions[0]['type']) ? 'checked="checked"' : ''; ?>/>
                                </label>
                            </div>
                        </li>
                        <?php
                        $count++;
                    }
                } ?>
            </ul>
        </div>
        <div class="hidden error_hint invalid-question-message">
            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
            <span><?= Lang::tr('invalid_question'); ?></span>
        </div>
    </div>
</section>