<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */
$title = '';
$question_data = array(
    'title' => '',
    'options' => array(
        'force_unique_choice' => QuestionSettings::getDefaultSetting('force_unique_choice'),
        'enable_maybe_choices' => QuestionSettings::getDefaultSetting('enable_maybe_choices')
    )
);

$dataAttr = '';
$dataType = '';
$dataPropositionType = '';
$selected_date = '';
$selected_text = '';
$question_icon = '';
$day_slots_question = '';
if ($question) {
    $question_data = Question::cast($question);
    $dataAttr = 'data-question-id="' . $question->id . '" data-index=' . $index . ' ';
    $dataType = 'data-type="' . $question->type . '"';
    $dataPropositionType = 'data-proposition-type="' . $question->proposition_type . '"';
    $selected_date = ($question->type == QuestionTypes::DATE) ? 'selected' : '';
    $selected_text = ($question->type == QuestionTypes::TEXT) ? 'selected' : '';
    $question_icon = ($question->type == QuestionTypes::DATE) ? 'fa-calendar-o' : 'fa-file-text-o';
    $day_slots_question = ($question->proposition_type == PropositionTypes::RANGE_OF_DAYS) ? 'selected' : '';
}
?>
<li data-form="question" class="small-6 question-form <?php echo $day_slots_question; ?>" <?php echo $dataAttr;
echo ' ' . $dataType;
echo ' ' . $dataPropositionType;
?>>
    <form data-abide novalidate id="main-question-form" onsubmit="return false;">
        <section class="small-12 compressed hidden">
            <span class="question-type fa <?php echo $question_icon; ?>"></span>
            <span class="question-title"><?php echo $question_data['title']; ?></span>
        </section>
        <section class="expanded small-collapse large-uncollapse">
            <section class="small-6 large-3 text-right question_actions column float-right">
                <span data-action="delete_question" class="wizard_action button highlight margin-top" tabindex="0">{tr:delete_question}</span>
            </section>
            <section class="small-6 large-4 column nopadding">
                <label for="question">
                    <span data-label-text="">{tr:question_statement} <span class="help-text">({tr:optional})</span></span>
                    <input class="input" placeholder="{tr:please_set_question_statement}" type="text" name="questions[<?php echo $index; ?>].title"
                           data-no-binding value="<?php echo htmlspecialchars($question_data['title']); ?>"/>
                </label>
            </section>
            <section class="small-12 large-5 column nopadding">
                <ul class="options ">
                    <li class="option">
                        <?php
                        echo (new FormFieldSwitch('force_unique_choice', array('label' => 'force_unique_choice'),
                            $question_data['options']['force_unique_choice']))->getHTML();
                        ?>
                        <i class="fa fa-info-circle" aria-hidden="true" data-make-tooltip title="{tr:force_unique_choice_explanation}"></i>
                    </li>
                    <li class="option">
                        <?php
                        echo (new FormFieldSwitch('enable_maybe_choices',
                            array('label' => 'enable_maybe_choices',
                                'disabled' => $question_data['options']['force_unique_choice']
                            ),
                            $question_data['options']['enable_maybe_choices']))->getHTML();
                        ?>
                    </li>
                    <?php
                        $hasLimitChoiceConstraint = $question && $question->hasConstraints('limit_choice');
                    ?>
                    <li class="option limit_choice <?php echo ($hasLimitChoiceConstraint?'hide':''); ?>">
                        <?php
                        echo (new FormFieldSwitch('question_limit_choice',
                            array('label' => 'question_limit_choice'),
                            false))->getHTML();
                        echo (new FormFieldNumber('question_limit_choice_nb',
                            array('label' => 'question_limit_choice_nb', 'min' => 1, 'hint' => 'constraint_value_placeholder'),
                            1))->getHTML();
                        ?>
                        <i class="fa fa-info-circle top" data-make-tooltip title="{tr:question_limit_choice_example}"></i>
                        <div>
                            {tr:to_define_constraint}
                            <a href="#advanced-options-container" data-action="open-advanced-options">{tr:manage_constraints}</a>
                        </div>
                    </li>
                    <li class="option some_constraints_defined <?php echo (!$hasLimitChoiceConstraint?'hide':''); ?>">
                        {tr:some_constraints_are_defined}
                        <a href="#advanced-options-container" data-action="open-advanced-options">{tr:see_constraints}</a>
                    </li>
                </ul>
            </section>
            <?php Template::display('question/propositions_text_form', array('question_data' => $question_data)); ?>
            <?php Template::display('question/propositions_date_form', array('question_data' => $question_data)); ?>
        </section>
    </form>
</li>