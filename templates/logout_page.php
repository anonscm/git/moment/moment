
<section class='row' data-component="logout">
    <article class=" error_http text-center">
            <img src="{img:disconnected_user_onpage.png}" />
        <div class="message">
            <i class="fa fa-info-circle"></i> {tr:logout_disclamer}
        </div>
    </article>
    </article>
</section>
