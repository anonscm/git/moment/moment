<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo Lang::getCode() ?>" xml:lang="<?php echo Lang::getCode() ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        
        <title>{cfg:application_name}</title>
        
        <?php GUI::includeStylesheets() ?>
        
        <?php GUI::includeFavicon() ?>
        
        <script type="text/javascript" src="{path:app-data.js.php}"></script>
        
        <?php GUI::includeScripts() ?>
        
        <meta name="robots" content="noindex, nofollow" />
        
        <meta name="auth" content="noindex, nofollow" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    </head>
    
    <body
        data-security-token="<?php echo Utilities::getSecurityToken() ?>"
        data-auth-type="<?php echo Auth::type() ?>"
        data-user-uid="<?php echo Utilities::userUID() ?>"
        data-dictionnary-uid="<?php echo Lang::getDictionnaryUID() ?>"
    >
        <header>
            <div class="title-bar" data-responsive-toggle="main-menu" data-hide-for="medium">
                <button class="menu-icon top-bar-right" type="button" data-toggle></button>
                <div class="title-bar-title top-bar-right">{tr:menu}</div>
                <div class="top-bar-left">
                        <a href="{cfg:application_url}" title="{tr:home_page}">
                            <?php GUI::includeLogo() ?>
                        </a>
                </div>
            </div>
            <nav class="top-bar" id="main-menu">
              <menu class="top-bar-left">
                <ul class="dropdown menu" data-dropdown-menu>
                  <li class="menu-text hide-for-small-only">
                        <a href="{cfg:application_url}" title="{tr:home_page}">
                            <?php GUI::includeLogo() ?>
                        </a>
                  </li>
                </ul>
              </menu>
              <menu class="top-bar-right">
                <ul class="menu icon-top" data-back-button="<li class='js-drilldown-back'><a tabindex='0'>{tr:back}</a></li>" data-responsive-menu="drilldown medium-dropdown" data-back-button='<li class="js-drilldown-back"><a tabindex="0">{tr:back}</a></li>'>
                    <?php Template::display('menu') ?>
                </ul>
              </menu>
            </nav>            
            <h1>{cfg:application_name}</h1>
        </header>
