<?php
    if(!Auth::isAdmin())
        throw new AuthUserNotAllowedException();
?>

<article class="row">
    <h1>{tr:user_management}</h1>
    
    <input type="text" name="search" placeholder="{tr:search_for_user}"/> <span data-action="search" class="button"><span class="fa fa-search"></span> {tr:search}</span>
    
    <section data-container="users">
        <section data-template="user" data-id="">
            <span data-property="email"></span>
            <span data-property="id"></span>
            <span class="fa fa-user-secret" data-action="impersonate" title="{tr:impersonate}"></span>
        </section>
    </section>
</article>
