<?php $page = GUI::getPage('home'); ?>

<main data-page="<?php echo $page ?>">
    <?php Template::display('page_menu') ?>
    
    <noscript>
        <div class="error message">
            {tr:noscript}
        </div>
    </noscript>
    
    <?php Template::display($page.'_page') ?>
</main>
