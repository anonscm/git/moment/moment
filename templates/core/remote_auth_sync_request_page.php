<?php

$remote = array_shift($path);
if(!$remote)
    throw new AuthRemoteUserRejectedException($user->id, 'no remote application name');

if(!Config::get('auth_remote_user_enabled'))
    throw new AuthRemoteUserRejectedException($user->id, 'remote auth disabled');

if(!Auth::user()->auth_secret)
    throw new AuthRemoteUserRejectedException($user->id, 'no secret set');

$code = substr(Utilities::generateUID(), -6);

$_SESSION['remote_auth_sync_request'] = array(
    'code' => $code,
    'expires' => time() + Config::get('remote_auth_sync_request_timeout')
);

echo '<section class="row">'.Lang::tr('remote_auth_sync_request')->r(array(
    'remote' => Utilities::sanitizeOutput($remote),
    'code' => $code
)).'</section>';
