<nav>
    <ul class="row">
        <li class="small-12 medium-4 columns">
            <a class="radius tip-top" data-options="disable_for_touch:true" data-menu-action="" href="#">
                Home menu link 1
            </a>
        </li>
        
        <li class="small-12 medium-4 columns">
            <a class="radius tip-top" data-options="disable_for_touch:true" data-menu-action="" href="#">
                Home menu link 2
            </a>
        </li>
        
        <li class="small-12 medium-4 columns">
            <a class="radius tip-top" data-options="disable_for_touch:true" data-menu-action="" href="#">
                Home menu link 3
            </a>
        </li>
    </ul>
</nav>
