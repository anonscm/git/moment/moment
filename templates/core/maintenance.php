<div id="page" class="maintenance">
    
    <noscript>
        <div class="error message">
            {tr:noscript}
        </div>
    </noscript>
    
    <?php Template::display('header'); ?>
        
    <h2>{tr:undergoing_maintenance}</h2>
        
    <?php Template::display('footer'); ?>
</div>
