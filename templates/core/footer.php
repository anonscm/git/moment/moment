<div class="scroll-top-wrapper ">
        <span class="scroll-top-inner">
                <i class="fa fa-2x fa-arrow-circle-up"></i>
        </span>
</div>

<footer>
    <article class="row">
        <ul class="medium-up-3">
            <li class="column">
                <ul>
                    <li><img src="{img:core/logo_renater_blanc.png}" /></li>
                    <li>Réseau National de télécommunications pour la Technologie l’Enseignement et la Recherche.</li>
                    <li>
                        <a href="https://www.renater.fr/" target="_blank"><img src="{img:core/renater_france_square.png}" /></a>
                        <a href="https://www.facebook.com/gip.renater" target="_blank"><span class="fa fa-facebook-square"></span></a>
                        <a href="https://twitter.com/RENATERnews" target="_blank"><span class="fa fa-twitter-square"></span></a>
                    </li>
                </ul>
            </li>
            <li class="column">
                <ul>
                    <li><h2>{tr:resources}</h2></li>
                    <?php foreach((new Event('resource_items'))->trigger(function() {
                        return array(
                            '<a href="{url:user_guide}">{tr:user_guide}</a>',
                            '<a href="{url:faq}">{tr:faq}</a>'
                        );
                    }) as $item) echo '<li>'.$item.'</li>'."\n"; ?>
                </ul>
            </li>
            <li class="column">
                <ul>
                    <li><h2>{tr:contact}</h2></li>
                    <li><span class="fi-marker"></span> 23-25, rue Daviel - 75013 PARIS</li>
                    <?php if($fburl = Config::get('feedback.url')) { ?>
                    <li>
                        <a id="feedback" href="<?php echo $fburl ?>" target="_blank" data-tooltip aria-haspopup="true" title="{tr:feedback_title}"><span class="fa fa-comments"></span>{tr:feedback}</a>
                    </li>
                    <?php } ?>
              </ul>
            </li>
        </ul>
    </article>
</footer>
<aside class="row footer-copyright">
    <ul>
        <li class="small-12 medium-6 columns">&copy; RENATER <?php echo date('Y') ?> - {tr:all_rights_reserved}</li>
        <li class="small-12 medium-6 columns text-right"></li>
    </ul>
</aside>
    </body>
</html>
