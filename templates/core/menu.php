<li>
    <a href="{cfg:application_url}" title="{tr:home_page}">
        <span class="fi-home"></span> <label>{tr:home_page}</label>
    </a>
</li>

<?php if(Auth::isAdmin()) { ?>
    <li>
        <a href="{url:admin}" title="{tr:admin_page}">
            <span class="fa fa-gears"></span> <label>{tr:admin_page}</label>
        </a>
    </li>
<?php } ?>

<?php if(Config::get('lang.selector_enabled')) { ?>
<li class="has-submenu" data-action="select_language">
    <a href="#" title="{tr:select_language}">
        <span class="fa fa-globe"></span> <label>{tr:language}</label>
    </a>
    <ul class="submenu menu vertical" data-submenu>
        <?php foreach(Lang::getAvailableLanguages() as $lang => $content) { ?>
        <li>
            <a class="<?php if($lang == Lang::getCode()) echo 'nocursor' ?>" data-lang="<?php echo $lang ?>" title="<?php echo $content['name'] ?>">
                <img src="{img:core/<?php echo $lang ?>.png}" /> <label><?php echo $content['name'] ?></label>
                <?php if($lang == Lang::getCode()) { ?>
                <span class="fa fa-check"></span>
                <?php } ?>
            </a>
        </li>
        <?php } ?>
    </ul>
</li>
<?php } ?>

<?php if(Auth::isAuthenticated() && Auth::isSP()) { ?>
<li class="has-submenu show-for-medium-up">
    <a href="#">
        <span class="fa fa-user"></span><label><?php echo Auth::user()->name ?></label>
    </a>
    
    <ul class="submenu menu vertical" data-submenu>
        <li>
            <a href="{url:user}">
                <span class="fa fa-cogs"></span> <label>{tr:user_page}</label>
            </a>
        </li>
        
        <li>
            <a href="<?php echo AuthSP::logoffURL() ?>">
                <span class="fi-power"></span> <label>{tr:logoff}</label>
            </a>
        </li>
    </ul>
</li>

<li class="show-for-small-only">
    <a href="{url:user}">
        <span class="fa fa-cogs"></span> <label>{tr:user_page}</label>
    </a>
</li>

<li class="show-for-small-only">
    <a href="<?php echo AuthSP::logoffURL() ?>">
        <span class="fi-power"></span> <label>{tr:logoff}</label>
    </a>
</li>
<?php } ?>

<?php
if(!Auth::isAuthenticated()) {
    if(Config::get('use_wayf')) {
        Template::display('wayf');
        
    } else { ?>
<li>
    <a href="<?php echo AuthSP::logonURL(Utilities::getCurrentURL()) ?>">
        <span class="fa fa-user"></span> <label>{tr:logon}</label>
    </a>
</li>
<?php }
}
?>
