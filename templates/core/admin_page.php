<?php
    if(!Auth::isAdmin())
        throw new AuthUserNotAllowedException();
?>

<article class="row">
    <ul>
        <li><a href="{url:admin_users}">{tr:user_management}</a></li>
    </ul>
</article>
