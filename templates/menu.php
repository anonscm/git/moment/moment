<li>
    <a href="{cfg:application_url}" title="{tr:home_page}">
        <img class="hide-for-small-only home_img" src="{img:home.svg}" alt="{tr:home_page}" data-options="disable_for_touch:true"/>
        <span class="show-for-small-only">
            <span class="fi-home"></span>
            <label>{tr:home_page}</label>
        </span>
    </a>
</li>

<!-- SUPPORT -->
<li class="has-submenu hide-for-small-only">
    <a href="#"  title="{tr:support}">
        <img class="support_img" src="{img:support.svg}" alt="{tr:support}" data-options="disable_for_touch:true"/>
    </a>

    <ul class="submenu menu vertical" data-submenu>
        <li>
            <a href="{url:user_guide}">
                <span class="fa fa-book"></span>
                <label>{tr:user_guide}</label>
            </a>
        </li>
        <li>
            <a href="{url:faq}">
                <span class="fa fa-question"></span>
                <label>{tr:faq}</label>
            </a>
        </li>
        <li>
            <a href="{url:terms}">
                <span class="fa fa-tasks"></span>
                <label>{tr:terms}</label>
            </a>
        </li>
    </ul>
</li>
<li class="show-for-small-only">
    <a href="{url:user_guide}">
        <span class="fa fa-book"></span>
        <label>{tr:user_guide}</label>
    </a>
</li>
<li  class="show-for-small-only">
    <a href="{url:faq}">
        <span class="fa fa-question"></span>
        <label>{tr:faq}</label>
    </a>
</li>
<li class="show-for-small-only">
    <a href="{url:terms}">
        <span class="fa fa-tasks"></span>
        <label>{tr:terms}</label>
    </a>
</li>


<?php if (Auth::isAdmin()) { ?>
    <!-- ADMIN -->
    <li>
        <a href="{url:admin}" title="{tr:admin_page}">
            <span class="fa fa-gears"></span>
            <label class="show-for-small-only">{tr:admin_page}</label>
        </a>
    </li>
<?php } ?>

<?php if (Config::get('lang.selector_enabled')) { ?>
    <!-- LANGUAGE -->
    <li class="has-submenu hide-for-small-only"  data-action="select_language">
        <a href="#" title="{tr:select_language}">
            <img class="lang_img" src="{img:lang.svg}" alt="{tr:language}" data-options="disable_for_touch:true"/>
            <label class="show-for-small-only">{tr:language}</label>
        </a>
        <ul class="submenu menu vertical" data-submenu>
            <?php foreach (Lang::getAvailableLanguages() as $lang => $content) { ?>
                <li>
                    <a class="<?php if ($lang == Lang::getCode()) echo 'nocursor' ?>" data-lang="<?php echo $lang ?>"
                       title="<?php echo $content['name'] ?>">
                        <img src="{img:core/<?php echo $lang ?>.png}"/> <label><?php echo $content['name'] ?></label>
                        <?php if ($lang == Lang::getCode()) { ?>
                            <span class="fa fa-check"></span>
                        <?php } ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </li>
    <?php foreach (Lang::getAvailableLanguages() as $lang => $content) { ?>
    <li class="show-for-small-only" data-action="select_language">
        <a class="<?php if ($lang == Lang::getCode()) echo 'nocursor' ?>" data-lang="<?php echo $lang ?>"
           title="<?php echo $content['name'] ?>">
            <img src="{img:core/<?php echo $lang ?>.png}"/> <label><?php echo $content['name'] ?></label>
            <?php if ($lang == Lang::getCode()) { ?>
                <span class="fa fa-check"></span>
            <?php } ?>
        </a>
    </li>
    <?php } ?>
<?php } ?>

<?php if (Auth::isAuthenticated() && Auth::isSP()) { ?>
    <!-- USER -->
    <li class="has-submenu hide-for-small-only" data-name="user_connected">
        <a href="#">
            <img class="user_connected_img" src="{img:connected_user.svg}" alt="{tr:connected_user}" title="<?php echo Auth::user()->name ?>" data-options="disable_for_touch:true"/>
        </a>
        <ul class="submenu menu vertical" data-submenu>
            <li>
                <a href="{url:user}">
                    <span class="fa fa-user-circle-o"></span> <label>{tr:user_page}</label>
                </a>
            </li>
            <li>
                <a href="<?php echo AuthSP::logoffURL() ?>">
                    <span class="fi-power"></span> <label>{tr:logoff}</label>
                </a>
            </li>
        </ul>
    </li>

    <li class="show-for-small-only">
        <a href="{url:user}">
            <span class="fa fa-user-circle-o"></span> <label>{tr:user_page}</label>
        </a>
    </li>
    <li class="show-for-small-only">
        <a href="<?php echo AuthSP::logoffURL() ?>">
            <span class="fi-power"></span>
            <label>{tr:logoff}</label>
        </a>
    </li>
<?php } ?>

<?php
if (!Auth::isAuthenticated()) {
    if (!Config::get('use_wayf')) { ?>
        <li>
            <a href="<?php echo AuthSP::logonURL(Utilities::ensureFullURL(Config::get('landing_page'))); ?>" data-tooltip="" title="{tr:log_in_tip}">
                <img class="hide-for-small-only disconnected_user_img" src="{img:disconnected_user.svg}" alt="{tr:disconnected_user}" data-options="disable_for_touch:true"/>
                <span class="show-for-small-only">
                    <span class="fa fa-user"></span>
                    <label>{tr:logon}</label>
                </span>
            </a>
        </li>
    <?php }
}
?>
