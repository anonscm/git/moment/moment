BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//<?php echo Config::get('application_name'); ?>_v<?php echo Config::get('version'); ?>//EN
METHOD:<?php echo $ics->options['method'].PHP_EOL ?>
<?php foreach($ics->events as $event) { ?>
BEGIN:VEVENT
UID:<?php echo $event->uid.PHP_EOL; ?>
SEQUENCE:<?php echo $event->sequence.PHP_EOL; ?>
TRANSP:OPAQUE
DTSTAMP:<?php echo $event->created.PHP_EOL; ?>
CREATED:<?php echo $event->created.PHP_EOL; ?>
LAST-MODIFIED:<?php echo $event->lastmodified.PHP_EOL; ?>
ORGANIZER;CN=<?php echo $event->organizer; ?>:mailto:<?php echo $event->organizer.PHP_EOL; ?>
<?php foreach($event->attendees as $attendee) { ?>
ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE;PARTSTAT=<?php echo isset($attendee['partstat'])?$attendee['partstat']:IcsConstants::PARTSTAT_NEEDS_ACTION; ?>:<?php echo $attendee['email'].PHP_EOL; ?>
<?php } ?>
DTSTART:<?php echo $event->dtstart.PHP_EOL; ?>
DTEND:<?php echo $event->dtend.PHP_EOL; ?>
SUMMARY:<?php echo $event->summary.PHP_EOL; ?>
X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE html>\n<html lang="fr">\n<body>\n<?php echo $event->description_html; ?>\n</body>\n</html>
DESCRIPTION:<?php echo $event->description.PHP_EOL; ?>
<?php if ($event->location != '') { ?>
LOCATION:<?php echo $event->location.PHP_EOL; ?>
<?php } ?>
STATUS:<?php echo $event->status.PHP_EOL; ?>
CLASS:PUBLIC
X-MICROSOFT-CDO-INTENDEDSTATUS:<?php echo $event->status.PHP_EOL; ?>
END:VEVENT
<?php } ?>
END:VCALENDAR