<?php
    $wayf = 'https://discovery.renater.fr/' . (preg_match('`^(dev|preprod)-`', $_SERVER['SERVER_NAME']) ? 'test' : 'renater') . '/WAYF' ;
    // It is now possible to add a custom url for wayf
    if(Config::get('wayf_url')) {
        $wayf = Config::get('wayf_url');
    }

    $host = urlencode(Config::get('application_url'));
?>

<li id="embeddedWAYF">
    <h1>{tr:logon}</h1>
    <!-- EMBEDDED-WAYF-START -->
    <script type="text/javascript">
        var wayf_URL = '<?php echo $wayf ?>';
        var wayf_sp_entityID = '{cfg:application_url}';
        var wayf_sp_handlerURL = '{cfg:application_url}Shibboleth.sso';
        var wayf_return_url = '{cfg:application_url}{cfg:landing_page}';
        var wayf_show_remember_checkbox = false;
        var wayf_force_remember_for_session = false;
        var wayf_use_small_logo = true;
        var wayf_font_size = 12;
        var wayf_font_color = '#000000';
        var wayf_border_color = 'transparent';
        var wayf_background_color = 'transparent';
        var wayf_hide_logo = true;
        var wayf_auto_login = false;
        var wayf_hide_after_login = true;
        var wayf_show_categories = true;
        var wayf_use_discovery_service = true;
        var wayf_sp_samlDSURL = wayf_sp_handlerURL + '/Login';
        var wayf_overwrite_intro_text = '';
        var wayf_use_improved_drop_down_list = true;
    </script>

    <script type="text/javascript" src="<?php echo $wayf . '/' . Lang::getCode(); ?>/embedded-wayf.js"></script>

    <noscript>
        <p>
            <strong>Login:</strong> Javascript is not available for your web browser. Therefore, please <a
                    href="<?php echo $wayf ?>?entityID=<?php echo $host; ?>&return=<?php echo $host; ?>Shibboleth.sso%2FLogin%3FSAMLDS%3D1">proceed
                manually</a>.
        </p>
    </noscript>

    <!-- EMBEDDED-WAYF-END -->
</li>
