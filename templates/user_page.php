<?php
/**
 * moment- user_page.php
 *
 * Initial version by: Florian Bruneau-Voisine
 * Initial version created on: 23/09/19
 *
 * Copyright (c) RENATER
 */

?>
<section data-component="user-page">
    <section class="row user-info">
        <h1>{tr:my_profile}</h1>
        <div class="row columns"><label class="small-2 medium-1">{tr:name} :</label><span><?php echo Auth::user()->name ?></span></div>
        <div class="row columns"><label class="small-2 medium-1">{tr:email} :</label><span><?php echo Auth::user()->email ?></span></div>
        <?php
        $pref = new PreferencesForm(Auth::user());
        if($pref->hasInput()) { ?>
            <h2>{tr:my_preferences}</h2>
        <?php
            echo (new PreferencesForm(Auth::user()))->getHTML();
        }
        ?>
    </section>
    <?php Template::display('user/calendars'); ?>
</section>
