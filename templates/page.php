<?php

/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */

$page = 'home';
$path = array();
if (array_key_exists('PATH_INFO', $_SERVER) && $_SERVER['PATH_INFO']) {
    $path = array_filter(explode('/', $_SERVER['PATH_INFO']));
    $page = array_shift($path);
}
if (Auth::isAuthenticated()) {
    //Recording user activity
    Auth::user()->recordActivity();
}
?>

<main data-page="<?php echo $page ?>">
    <?php
    if (Auth::isAuthenticated() && $page !== 'legal' && $page !== 'terms' && $page !== 'accessibility') {
        Template::display('page_menu', array('path' => $path));
    }
    ?>

    <noscript>
        <div class="callout error message">
            {tr:noscript}
        </div>
    </noscript>

    <?php
        if(in_array($page, array('survey', 'home', 'user'))) { ?>
            <div class="reveal-overlay no-script-loaded" tabindex="-1" aria-hidden="true">
                <div class="reveal">
                    <div class="callout error text-left">
                        {tr:no_javascript_loaded}
                    </div>
                </div>
            </div>
        <?php }
    ?>

    <?php //Displaying MOTD
    if (file_exists(EKKO_ROOT.'/templates/motd.php') && Config::get('motd.enabled')) {
        Template::display('motd');
    }
    ?>

    <?php Template::display($page . '_page', array('path' => $path)) ?>
</main>
