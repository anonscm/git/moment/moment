<?php
/**
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */

$sub = array_shift($path);

if (!isset($admin_page)) {
    $admin_page = false;
}

try {
    if (in_array($sub, array('create', 'manage', 'update', 'results'))) {

        //All those action require authentication
        if (Auth::isAuthenticated()) {
            //Recording user activity
            Auth::user()->recordActivity();

            switch ($sub) {
                //Create or manage doesn't need id
                case 'create':
                case 'manage':

                    // Create form
                    Template::display('survey/' . $sub, array('admin_page' => $admin_page));
                    break;
                //Update or results require an id
                case 'update':
                case 'results':
                    Template::display('survey/' . $sub, array('id' => array_shift($path)));
                    break;
            }

        } else if ($sub === 'results') {
            // Results display doesn't always require authentication (managed by itself)
            Template::display('survey/results', array('id' => array_shift($path)));
        } else {
            Template::display('please_login');
        }
    } else {

        //If param "force_auth" is set and true, we trigger authentication, likely to recover an authenticated participant
        if(!Auth::isAuthenticated() && array_key_exists('force_auth', $_GET) && $_GET['force_auth']) {
            AuthSP::trigger(Utilities::getCurrentURL());
        }

        //$sub maybe a human_readable_id
        Template::display('survey/reply', array('human_readable_id' => $sub));
    }
} catch (SurveyAccessDeniedException $e) {
    Template::display('survey/access_denied', array('reason' => $e->current_user['reason']));
} catch (SurveyClosedException $e) {
    $alternative = '';
    if ($e->survey && $e->current_user
        && !$e->survey->hide_answers && $e->current_user['role'] >= SurveyUserRoles::REPLIER
    ) {
        $alternative .= '<a href="' . $e->survey->results_path . '" title="' . Lang::tr('show_results') . '" >' . Lang::tr('show_results') . '</a>';
    }

    Template::display('survey/access_denied', array('reason' => 'survey_is_closed', 'alternative' => $alternative));
} catch (SurveyNotFoundException $e) {
    Template::display('survey/access_denied', array('reason' => 'survey_not_found'));
} catch (NotFoundException $e) {
    Template::display('survey/access_denied', array('reason' => 'survey_not_found'));
}
     