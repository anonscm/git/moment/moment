<main data-page="<?php echo $page ?>">
    <section class='row' data-component="404">
        
        <article class="error_http text-center">
            <?php if($exception->getCode() == '404') { ?>
            <img src="{img:404.png}" />
            <?php } ?>
            <?php if($exception->getCode() == '403') { ?>
                <img src="{img:403.png}" />
            <?php } ?>
            <div class="message"><i class="fa fa-exclamation-circle"></i>
                <?php echo Lang::tr(Utilities::sanitizeOutput($exception->getMessage())) ?>
            </div>
            
        </article>
    </section>

</main>