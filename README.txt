=========
 Moment
=========

Introduction
========

Moment is a web based application of event organization and decision making.
Users can create surveys to ask people for their availability/opinion.


Installing
========

Moment is a PHP application, you can use a web server as Apache to host it. The document root is </path/to/moment/sources>/view/.

Running user (e.g. www-data) must access 'data/core' 'tmp' 'view/cache' 'logs'.

Home page is served at root, so 'https://localhost/' should show you home. (Depending on web server configuration)

Configuration
========

You must create a 'config.php' in the folder ./config. (You can copy config_sample.php)

Minimal configuration :

 - Database configuration : 'db' that must match your MySQL server configuration
 - Application URL : 'application_url' that must match your server configuration

Developer
========

An important thing to know when developing for Moment is that it is possible to emulate an authentication.

Add those lines to 'config.php' :
$config['auth_sp'] = array(
    'type' => 'fake',
    'authenticated' => true,

    'attributes' => array(
        'name' => 'Foo Bar',
        'emails' => 'foo.bar@example.com',
        'uid' => 'foo.bar@example.com',
        'affiliation' => array("Member","admin","Employee"),
        'eppn' => 'foo.bar@example.com',
    )
);

With those lines every pages will be shown as authenticated as 'Foo Bar' would saw it.


Authentication methods available
========

- 'shibboleth', authentication via Shibboleth and HTTP Headers
- 'remote', authentication for a remote application via shared secret


Authentication methods to implement :

- 'standalone', local authentication with a local credentials stored in database
