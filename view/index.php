<?php

/**
 *     Moment - index.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
ob_start();

try {
    require_once('../includes/core/init.php');
    
    ApplicationContext::setProcess(ProcessTypes::GUI);
    
    (new Event('ui_started'))->trigger();
    $authException = null;
    try{
        Auth::user();
    } catch(Exception $e) {
        if (preg_match('`^Auth(Remote|SPBad|SPMissingAttribute|User)`',get_class($e))) {
            $authException = $e;
        } else {
            throw $e;
        }
    }

    Template::display('!header', array('no_header' => isset($authException)));

    try { // At that point we can render exceptions using nice html

        if(isset($authException)) {
            throw $authException;
        }
        
        if (Config::get('maintenance')) {
            Template::display('maintenance');
        } else {
            $page = GUI::getPage(null, 0);

            if($page == 'plugin') {
                Template::display('plugin:'.GUI::getPage(null, 1).':ui');
                
            } else {
                //Controlling access before trying to display pages
                MomentUtilities::accessControl();

                Template::display('page');
            }
        }
    } catch(MomentPleaseLoginException $e) {
        RestUtilities::sendResponseCode($e->getCode());
        Template::display('please_login', array('exception' => $e, 'page' => $page?$page:''));
    } catch(MomentHttpException $e) {
        RestUtilities::sendResponseCode($e->getCode());
        Template::display('exception_http', array('exception' => $e, 'page' => $page?$page:''));
    } catch(Exception $e) {
        Template::display('exception', array('exception' => $e));
    }
    
    Template::display('!footer');
    
} catch(Exception $e) {
    // If all exceptions are catched as expected we should not get there
    die('An exception happened : '.$e->getMessage());
}
