<?php

/**
 * This file is part of the BaseProject project.
 * 2015 
 * Copyright (c) RENATER
 */

/**
 * Client side configuration handling
 */
 
require_once('../includes/core/init.php');

ApplicationContext::setProcess(ProcessTypes::GUI);

header('Content-Type: text/javascript');

$config = (new Event('client_config'))->trigger(function() {
    return array(
        'application_url' => Config::get('application_url'),
        'session_check_interval' => Config::get('session_check_interval'),
        'default_language' => Config::get('lang.default'),
        'max_cookie_duration' => Config::get('max_cookie_duration'),
        'application_name' => Config::get('application_name'),
        'logon_url' => AuthSP::logonURL(),
        'logoff_url' => AuthSP::logoffURL(),
        'motd' => Config::get('motd'),
        'moments_of_day' => Config::get('moments_of_day')
    );
});
?>
 
if(!('app' in window)) window.app = {};

window.app.config = <?php echo json_encode($config) ?>;
