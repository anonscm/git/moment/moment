<?php

/**
 *     Moment - plugin.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


require_once('../includes/core/init.php');

ApplicationContext::setProcess(ProcessTypes::GUI);

if(!Config::get('debug'))
    die();

if(!array_key_exists('PATH_INFO', $_SERVER))
    die();

// Split request path to get tokens
$path = array_filter(explode('/', $_SERVER['PATH_INFO']));

if(!count($path))
    die();

$plugin = array_shift($path);
$resource = array_shift($path);

$path = PluginManager::getPluginPath($plugin);

if(!file_exists($path.$resource))
    die();

switch($resource) {
    case 'script.js':  header('Content-Type: application/javascript; charset=UTF-8'); break;
    case 'styles.css': header('Content-Type: text/css; charset=UTF-8'); break;
}

readfile($path.$resource);
