/**
 * First thing to do, clear javascript failure prevent block
 */
$(document).ready(function() {
    //Remove .no-script-loaded as javascript is loaded
    $('.no-script-loaded').remove();
    $('[data-component="faq"] .accordion .accordion-content').css('display','none');
});

/**
 * Actions triggered once app is started
 */
app.event.on('started', function() {

    //Initializing datetimepicker
    $.datetimepicker.setDateFormatter({
        parseDate: function (date, format) {
            var d = moment(date, format);
            return d.isValid() ? d.toDate() : false;
        },

        formatDate: function (date, format) {
            return moment(date).format(format);
        }
    });

    //Reiniting datetimepickers once dateformatter is set
    $('[data-picker]').each(function() {
        var options = $(this).data('xdsoft_datetimepicker').data('options');
        $(this).datetimepicker('destroy');
        options.dayOfWeekStart = moment().localeData().firstDayOfWeek();
        $(this).datetimepicker(options);
    });


    if(typeof flatpickr !== 'undefined') {
        //Setting locale for flatpickr
        const flatSetting = "flatpickr.localize(flatpickr.l10ns." + app.lang.getCode() + ")";
        eval(flatSetting);


        flatpickr.setDefaults({
            dateFormat: app.lang.translate('date_format').text,
            parseDate: function (date, format) {
                var d = moment(date, format);
                return d.isValid() ? d.toDate() : false;
            },

            formatDate: function (date, format) {
                return moment(date).format(format);
            }
        });
    }

    //Localizing time
    window.app.ui.timeLocalizer().init();
    //Adapting view on results/answers
    window.app.ui.adapter();
    //Init suggestion fields
    window.app.ui.suggestionInitializer();
    //Init copy to clipboard action handler
    window.app.utilities.clipboardLinkInitializer();

    // Show MOTD if needed
    window.app.motd.startup();

    // Show tips if needed
    window.app.tips.startup();


    /**
     * Toggle class 'disabled' when switch is not checked
     */
    let disableInputSwitchWhenNotChecked = function() {
        $(this).parent().toggleClass('disabled', !$(this).is(':checked'));
    };

    $('input.switch-input').on('change', disableInputSwitchWhenNotChecked);
    $('input.switch-input').each(disableInputSwitchWhenNotChecked);

    /**
     * Toggle switch/checkbox with keyboard (space and enter)
     */
    $(document).on('keyup', '.switch-container', function(e) {
        if ((e.keyCode === 13) || (e.keyCode === 32)) {
            const $checkbox = $(this).find('input.switch-input');
            $checkbox.prop('checked', !$checkbox.prop('checked')).trigger('change');
        }
    });

    /**
     * Triggering click when pressing stroking "enter" on data-action focusable
     */
    $(document).on('keyup', '[data-action][tabindex="0"]', function(e) {
        if (e.keyCode === 13) {
            $(this).trigger('click');
        }
    });

    /**
     * Select language anywhere
     */
    $('[data-action="select_language"] [data-lang]').off('click');
    $('[data-action="select_language"] [data-lang]').on('click', function() {
        var sel = $(this).attr('data-lang');
        if(sel === $('html').attr('lang')) return;

        app.ui.redirect(null, {lang: sel});
    });

    /**
     * Log in button anywhere
     */
    $('[data-action="log_in"][data-logon-url]').on('click', function() {
        //Disable any warn on leaving
        app.ui.disableWarnOnLeaving();
        window.location.replace($(this).attr('data-logon-url'));
    });

    /**
     * Handling pending surveys
     */
    if(app.utilities.isLocalStorageAvailable()) {
        let pendingSurvey =  JSON.parse(localStorage.getItem('pending_survey'));

        if(pendingSurvey !== null && app.session.user) {
            app.session.get(function(user) {
                //user must have a session and we should not be on wizard page
                if(user === undefined && $('[data-component="survey-wizard"]').length > 0)
                    return;

                //We are not currently editing a survey
                let survey = pendingSurvey.attributes;

                //Check validity
                if(survey === undefined || survey.length === 0
                    || survey.settings === undefined || survey.settings.length === 0
                    || survey.title === undefined || survey.title === '') {
                    localStorage.removeItem('pending_survey');
                    return;
                }
                let message = app.lang.tr('pending_survey_detected');
                message += ' (' + app.lang.tr('title') + ' : ' + survey.title + ')';

                let popup = app.ui.popup.create(lang.tr('survey_recovery_dialog'),
                    {
                        yes: {
                            type : 'highlight',
                            onclick : function() {
                                //Show loader
                                app.ui.toggleLoader(true, app.lang.tr('saving_update').out());

                                //Draft or not existing survey
                                if((survey.is_draft !== undefined && survey.is_draft === 1)
                                    || survey.id === undefined
                                    || survey.id === '') {

                                    //We are saving a new survey it will be stored as draft
                                    survey.is_draft = 1;

                                    app.client.post('/survey', survey, function (path, data) {
                                        localStorage.removeItem('pending_survey');

                                        //Hide loader
                                        app.ui.toggleLoader(false);

                                        window.location.replace(app.config.application_url + "survey/update/" + data.data.id)
                                    });
                                } else if(survey.id !== undefined) { //For existing survey we update it
                                    app.client.put('/survey/' + survey.id, survey, function (data) {
                                        localStorage.removeItem('pending_survey');

                                        //Hide loader
                                        app.ui.toggleLoader(false);

                                        window.location.replace(app.config.application_url + "survey/update/" + data.data.id)
                                    });
                                }
                            }
                        },
                        delete_pending_survey: function() { //Ignoring pending survey
                            localStorage.removeItem('pending_survey');
                        },
                        later: function() {
                            //Nothing to do
                        }
                    }, {no_close: true});

                popup.html(message);
                app.ui.popup.open(popup);
            })
        }

        /**
         * Cheking if there are pending informations to show
         * @type {any}
         */
        let pendingInfo =  JSON.parse(localStorage.getItem('pending_success'));
        if(pendingInfo !== null && pendingInfo) {
            window.app.ui.notify('success', pendingInfo.message);
            localStorage.removeItem('pending_success');
        }

    }

    // Open requested accordion element #<id>
    if (window.location.hash) {
        var element = $(window.location.hash + '-label');
        if (element.length) {
            element.trigger('click');
        }
    }




});