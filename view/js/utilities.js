if (!('app' in window))
    window.app = {};

if (!('ui' in window.app))
    window.app.ui = {};

window.app.ui.binder = function(object_id) {
    // Use a jQuery object as simple PubSub
    var pubSub = jQuery({});

    // We expect a `data` element specifying the binding
    // in the form: data-bind-<object_id>="<property_name>"
    var data_attr = "bind-" + object_id,
            message = object_id + ":change";

    // Listen to change events on elements with the data-binding attribute and proxy
    // them to the PubSub, so that the change is "broadcasted" to all connected objects
    jQuery(document).on("init change", "[data-" + data_attr + "]", function (evt) {
        var $input = jQuery(this);
        var val = $input.val();
        var real_val = val;

        //If type  checkbox, we need to know if checked
        if($input.attr('type') === 'checkbox') {
            val = ($input.is(':checked')?1:0);
            real_val = val;
        }

        //If there is a date picker, it's a date
        if($input.attr('data-picker') === 'date') {
            var baseDay = moment.utc($input.val(), app.lang.translate('moment_date_format').text).local();

            if (baseDay.isValid()) {
                real_val = baseDay.utc().unix();


            } else {
                //Datetime picker is already initialized
                var d = $input.datetimepicker('getValue');

                if (d instanceof Date) {
                    real_val = d ? parseInt(d.getTime() / 1000) - (d.getTimezoneOffset() * 60) : null;
                }
            }
        }
        pubSub.trigger(message, [$input.data(data_attr), val, real_val]);
    });

    // PubSub propagates changes to all bound elements, setting value of
    // input tags or HTML content of other tags
    pubSub.on(message, function (evt, prop_name, new_val) {
        jQuery("[data-" + data_attr + "='" + prop_name + "']").each(function () {
            var $bound = jQuery(this);

            if ($bound.is("input, textarea, select")) {
                $bound.val(new_val);
            } else {
                $bound.html(new_val);
            }
        });
    });

    return pubSub;
}

window.app.ui.currentYear =  new Date().getFullYear();
window.app.ui.timeLocalized = false;
window.app.ui.timeLocalizer =  function() {

    this.localize = function(type, timestamp) {
        var dateMoment = moment.unix(timestamp);

        if (dateMoment.isValid()) {
            if (type === 'date') {
                return dateMoment.format(app.lang.translate('moment_date_format').text);
            }
            if (type === 'date_readable' || type === 'date_readable_single_line') {
                let format = app.lang.translate('moment_date_format_readable').text;

                //If not current year we show the year
                if(dateMoment.year() != window.app.ui.currentYear)
                    format += ' <br/>YYYY';

                //If we want a single line
                if(type === 'date_readable_single_line')
                    format = format.replace(/<br\/?>/g, "");

                return dateMoment.format(format).replace(/ /g, "&nbsp;").capitalize();
            }
            if (type === 'date_utc') {
                return dateMoment.utc().format(app.lang.translate('moment_date_format').text);
            }
            if (type === 'date_utc_readable' || type === 'date_utc_readable_single_line') {
                let format = app.lang.translate('moment_date_format_readable').text;

                //If not current year we show the year
                if(dateMoment.year() != window.app.ui.currentYear)
                    format += ' <br/>YYYY';

                //If we want a single line
                if(type === 'date_utc_readable_single_line')
                    format = format.replace(/<br\/?>/g, "");

                return dateMoment.utc().format(format).replace(/ /g, "&nbsp;").capitalize();
            }
            if (type === 'time') {
                return dateMoment.format(app.lang.translate('moment_time_format').text);
            }
            if (type === 'date_time') {
                return dateMoment.format(app.lang.translate('moment_datetime_format').text);
            }
            if (type === 'date_time_readable') {
                return dateMoment.format(app.lang.translate('moment_datetime_format_readable').text).replace(/ /g, "&nbsp;").capitalize();
            }
        }
        return false;
    };

    this.localizeTimestamp = function(element) {

        // var newVal = window.app.ui.timeLocalizer().localize(element.attr('data-localize'), element.attr('data-timestamp'));

        let timestamps = element.attr('data-timestamp').split(',');
        let tmp = [];
        timestamps.forEach(function(timestamp) {
            tmp.push(window.app.ui.timeLocalizer().localize(element.attr('data-localize'), timestamp));
        })

        let newVal = tmp.join(',');

        if (newVal) {
            if (element.is("input, textarea, select")) {
                element.val(newVal);
            } else {
                element.html(newVal);
            }
            element.attr('data-local', newVal);
        }
    };

    this.init = function() {
        //No need to work twice
        if (!window.app.ui.timeLocalized) {
            //Localizing dates hours
            $('[data-timestamp]').each(function () {
                if($(this).attr('data-timestamp') !== '') {
                    window.app.ui.timeLocalizer().localizeTimestamp($(this));
                }

                $(this).css('visibility', 'visible');
            });
            //Showing guessed time zone in which dates have been localized
            var timezone = moment.tz.guess();
            if(timezone) {
                $('[data-localize="time_zone"]').each(function () {
                    if ($(this).is("input, textarea, select")) {
                        $(this).val(timezone);
                    } else {
                        $(this).html(timezone);
                    }
                });
            }
        }
        window.app.ui.timeLocalized = true;

        /**
         * We trigger an event 'timeLocalized' because some action requires time to ba localized
         */
        $(window).trigger('timeLocalized');
    }

    return this;
}

window.app.utilities.getSecondsOffset = function(timeAsString, dateAsString) {
    if(timeAsString === undefined || timeAsString === '')
        return undefined;

    var tmp = moment.utc(dateAsString, app.lang.translate('moment_datetime_format').text);

    let time;
    //if dateAsString is set we get moment at this date
    if(dateAsString === undefined) {
        time = moment(timeAsString, app.lang.translate('moment_time_format').text);
    } else {
        time = moment(dateAsString + ' ' + timeAsString, app.lang.translate('moment_datetime_format').text);
    }
    var timeUtc = time.utc();

    if (time.isValid()) {
        // Your moment at midnight
        // var mmtMidnight = tmp.utc().clone().startOf('day');

        // console.log(tmp.clone().startOf('day').unix());
        // var mmtMidnight = tmp.clone().startOf('day').utc();
        // var mmtMidnight = timeUtc.clone().startOf('day');
        var mmtMidnight = tmp.clone().startOf('day');
        // seconds is the number of seconds between midnight and selected time
        return timeUtc.clone().diff(mmtMidnight, 'seconds');
    }
    return undefined;
}

window.app.utilities.getDatesBetweenDates = function(startDate, endDate) {
    var dates = [];
    var currDate = moment(startDate.local()).startOf('day');
    var lastDate = moment(endDate.local()).startOf('day');
    //Include starting point
    dates.push(currDate.clone());

    //If it's an events that starts on one day and hour and ends another day and hour
    if(!currDate.isSame(lastDate) && !lastDate.isSame(endDate)) {
        dates.push(lastDate.clone());
    }

    while(currDate.add(1, 'days').diff(lastDate) < 0) {
        dates.push(currDate.clone());
    }

    return dates;
};

window.app.utilities.getMomentNextDuration = function(baseHour, format, duration, units) {
    let newMoment = moment(baseHour, format).clone().add(duration, units);
    if(moment(baseHour, format).isAfter(moment(newMoment.format(format), format))) {
        newMoment.startOf('day').add(-1, 'minute');
    }
    return newMoment;
}

window.app.utilities.getNextHour = function(baseHour, format) {
    return window.app.utilities.getMomentNextDuration(baseHour, format, 1, 'hour').format(format);
}

window.app.utilities.hasOverflow = function(element) {
    if(element.length > 0) {
        var _elm = element[0];
        return (_elm.offsetHeight < _elm.scrollHeight) || (_elm.offsetWidth < _elm.scrollWidth);
    }
    return false;
}

/**
 * 'small' screen detection
 * @returns {boolean}
 */
window.app.ui.isSmallScreen = function() {
    return 'small' === Foundation.MediaQuery.current;
}

/**
 * Adapting answers table with its own size
 * @returns {Window.app.ui}
 */
window.app.ui.adapter = function() {
    let expandViewHTML = '<i class="fa fa-expand hide-for-small-only"'
        + ' data-action="expand_view" title="'+app.lang.tr('expand_reduce_view')+'" data-tooltip'
        + ' aria-hidden="true" ></i>';

    $('.answers-container').each(function() {
        let container = $(this);
        let $question = $(this).parents('.survey-question');

        //If participant-table is already here, no action
        if($question.find('.participant-table').length > 0 || !container.is(':visible'))
            return;

        let answerTable = container.find('table.answer-table');

        if(window.app.utilities.hasOverflow(container) && !window.app.ui.isSmallScreen()) {
            $question.find('.answers').removeClass('small-12').addClass('small-10');

            let participantContainer = $('<section class="participant-container float-left small-2">').insertBefore($question.find('.answers'));
            let participantTable = $('<table class="participant-table"><thead></thead><tbody></tbody><tfoot></tfoot></table>').appendTo(participantContainer);

            // Copying first elements of row to participant-table (.first .comment-column and participant-name)
            answerTable.find('tr').each(function() {
                let $tr = $('<tr>');
                $(this).find('.first, .comment-column, .participant-name').each(function() {
                    if($(this).outerHeight() > 0) {
                        $(this).css('height', $(this).outerHeight());
                    }
                    $tr.append($(this).clone());
                });
                if ($(this).parents('thead').length > 0) {
                    $tr.appendTo(participantTable.find('thead'));
                } else if ($(this).parents('tfoot').length > 0) {
                    $tr.appendTo(participantTable.find('tfoot'));
                } else {
                    $tr.appendTo(participantTable);
                }
            });
            answerTable.find('.first, .comment-column, .participant-name').remove();

            participantTable.find('[title][title!=""]').attr('data-tooltip','').foundation();

            participantTable.show();
            // Add expand/compress button to expand answer view
            $question.find('h4').after(expandViewHTML);
            $question.find('[data-action="expand_view"]').foundation();

        } else {
            $question.find('.answer-table [title][title!=""]').attr('data-tooltip','').foundation();
        }
    });

    $(window).trigger('adapted');

    //We can now show tables
    $('.answers-container table').css({opacity: 0, visibility: "visible"}).animate({opacity: 1}, 100);

    $('.answers-container').each(function() {
        var $currentContainer = $(this);

        //Merging same date only header
        $(this).find('tr.header_row th[data-type="date"]:not([rowspan="2"])').each(function() {
            var $currentDateHeader = $(this);

            //No merging on already grouped
            if($(this).attr('colspan') > 1)
                return;

            var $sameDateHeaders = $(this).nextUntil(':not(th[data-local="' + $(this).attr('data-local') + '"]), th[data-proposition-id]');

            //Changing references in the bottom_row
            $sameDateHeaders.each(function() {
                if($(this).hasClass('top-winner')) {
                    $currentDateHeader.addClass('top-winner');
                }

                $currentContainer
                    .find('[data-proposition-header-ref="' + $(this).attr('data-proposition-header-ref') + '"]')
                    .attr('data-proposition-header-ref', $currentDateHeader.attr('data-proposition-header-ref'));
            });

            $(this).attr('colspan', $sameDateHeaders.length + 1);
            $sameDateHeaders.remove();
        });


        //Adds a fixed header to answer-table
        $(this).headervisible();

        if(window.app.utilities.hasOverflow($(this)) && !window.app.ui.isSmallScreen()) {
            //Adds arrows to scroll
            $(this).arrowscrolling();
        }
    });

    //Moving radio input to label-for-small to ease reading
    $('.label-for-small.show-for-small-only:visible').each(function() {
        let $input = $(this).parent().find('input[type="radio"][name^="proposition_"].single_choice');
        if($input.length > 0) {
            $input.removeClass('hide-for-small-only');
            $('<div>').append($input).appendTo($(this));
        }
    });

    $('.proposition_date_input').each(function() {
        var $currentLi = $(this);
        var $currentUl = $currentLi.parent();

        //Merging same date only header
        $(this).find('input[name="base_date"][data-localize="date"]').each(function() {

            var slot = $currentLi.find('[name="slot"]').prop('checked');
            var $sameDateLi = $currentUl
                .find('.proposition_date_input')
                .has('input[name="base_date"][data-localize="date"][data-local="'+$(this).attr('data-local')+'"]')
                .not($currentLi);

            $sameDateLi.each(function() {
                // if($(this).find('[name="slot"]').prop('checked') == slot) {
                    $currentLi.find('.time_pickers').append($(this).find('.time_picker_container'));
                    $(this).remove();
                // }
            });


        });
        //Refreshing index
        $(this).find('label[for^="proposition_"]').attr('for', 'proposition_' + ($(this).index() + 1));
        // $(this).find('label[for^="proposition_"]').text(app.lang.translate('date') + ' ' + ($(this).index() + 1));
    });

    $('[data-form="question"]').trigger('propositions_grouped');

    return this;
};

window.app.ui.suggestionInitializer =  function() {

    $('input[data-suggest]').each(function () {
        if($(this).attr('data-suggest').length > 0) {
            var currentInput = $(this);
            app.client.get('/suggestion/' + $(this).attr('data-suggest').replace('.','/'),
                function(data) {
                    currentInput.autocomplete({
                        source : data,
                        close: function() {
                            currentInput.trigger('change');
                        }
                    });
                }
            );
        }
    });
}

window.app.utilities.clipboardLinkInitializer = function() {
    $('[data-action="copy_to_clipboard"]').on('click', function() {
        if(typeof $(this).attr('data-content') === 'string' || $(this).attr('data-content') instanceof String) {
            window.app.utilities.copyToClipboard($(this).attr('data-content'));
        }
    });
}

/**
 * Checking if user checked 'accept_terms' checkbox if present.
 *
 * @returns {boolean}
 */
window.app.utilities.hasAcceptedTerms = function() {
    var accept_terms = $('input[type="checkbox"][name="accept_terms"]');
    var terms_accepted = true;
    if(accept_terms.length > 0) {
        if(accept_terms.is(':checked')) {
            accept_terms.parents('.accept_terms_container').find('.error_hint').remove();
        } else {
            terms_accepted = false;
            if(accept_terms.parents('.accept_terms_container').find('.error_hint').length === 0) {
                var tooltip = $('<div class="error_hint" />').insertAfter(accept_terms.parent());
                tooltip.text(lang.tr('accept_terms_required'));
            }
        }
    }
    return terms_accepted;
}

/**
 * Get emails from a string.
 * String can be "foo@bar.com" "foobar1@baz.com foobar2@baz.com" or '"Foo Bar" <foo@bar.com>, "Foo Bar 2" <foo@bar2.com>' ...
 * @param input
 * @returns {Array}
 */
window.app.utilities.getEmailsFromString = function(input) {
    var ret = [];

    //Email with name/gecko (match[1] is the name and match[2] is the email)
    // var email = /(?:\s*\"?([^\"^\>,]+)\"?\s+?)?\<([^\>]+)\>[;,]?/g;

    //Email between < >
    var email = /\<([^\>]+)\>[;,]?/g;

    var match;
    while (match = email.exec(input)) {
        if(app.utilities.isValidEmailAddress(match[1]) && match[1].length <= 255) {
            ret.push({'name': '', 'email': match[1]})
        }
    }
    //If no email found trying another method
    if(ret.length === 0) {

        //Email separated with space or comma (or semicolon) (Actually this methods extracts emails from any text
        var emails = input.split(/[;,\s"]+/);
        for(var i = 0; i < emails.length; i++) {
            if(app.utilities.isValidEmailAddress(emails[i]) && emails[i].length <= 255) {
                ret.push({'name': '', 'email': emails[i]})
            }
        }
    }

    return ret;
}


/**
 * Allows to put content into the clipboard
 *
 * @param content
 */
window.app.utilities.copyToClipboard = function(content){
    var $txa = $("<textarea />",{val:content,css:{position:"fixed"}}).appendTo("body").select();

    if(document.execCommand('copy')){
        app.ui.notify('success',lang.tr('copied_to_clipboard_success'));
    } else {
        var prompt = app.ui.popup.prompt(lang.tr('warning'));

        var msg = $('<span data-prompt="clipboard"/>').html(lang.tr('clipboard_not_supported').out());

        var input = $('<input type="text" value="'+content+'" data-input-value="clipboard_content"/>' )
            .appendTo(msg);

        prompt.html(msg);
        app.ui.popup.open(prompt);
        input.select();
    }
    $txa.remove();
}

/**
 * Check if device has touch capabilities
 * @returns {boolean|*}
 */
window.app.utilities.isTouchDevice = function() {
    return 'ontouchstart' in window        // works on most browsers
        || navigator.maxTouchPoints;       // works on IE10/11 and Surface
};

/**
 * Checks if str is a valid url
 * @param str
 * @returns {boolean}
 */
window.app.utilities.isValidHttpUrl = function(str) {
    var pattern = new RegExp('^(https?:\\/\\/)'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\.)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '.*','i');
    return pattern.test(str);
}

window.app.ui.errorLegacy = window.app.ui.error;
window.app.ui.error = function(message,onclose) {
    var popup = window.app.ui.errorLegacy(message,onclose);
    popup.find('p:first-of-type').addClass('callout alert');
    popup.find('pre').addClass('small-6 text-left').css('margin','0 auto');
    var details = popup.find('pre, p:last-of-type').wrapAll('<div class="callout primary"></div>').parent();
    details.hide();
    var moreDetails = $('<a href="#">'+app.lang.tr('more_details')+'</a>').css({'margin-bottom':'1rem','display':'block'}).insertBefore(details);
    moreDetails.on('click', function(){
        details.toggle();
    })
}
window.app.ui.toggleLoaderLegacy = window.app.ui.toggleLoader;
window.app.ui.toggleLoader = function(show, message){
    if (message === undefined)
        message = app.lang.tr('please_wait').out();

    message = '<span class="fa fa-spinner fa-lg fa-spin fa-pulse"></span>  ' + message + '...';

    if(show) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#fff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: 0.95,
                color: '#000',

                width: '30%',
                position: 'fixed',

                top:'calc(50%)',
                left:'calc(50% - 15%)',

                fadeIn: 200
            },
            message: message
        });
    } else {
        $.unblockUI({fadeOut:false});
    }
}

/**
 * Updates the Foundation Tooltip of element with the requested tip
 * @param element
 * @param tip
 */
window.app.ui.updateTooltip = function(element, tip) {

    //Clearing older tip
    if ($(element).attr('data-tooltip') !== undefined) {
        $(element).foundation('hide');
        $(element).foundation('destroy');
    }

    //New title
    $(element).attr('title', tip);
    $(element).attr('title-backup', tip);

    //We show has foundation tips
    $(element).attr('data-tooltip', '');
    new Foundation.Tooltip($(element));
}

/**
 * Overriding app.ui.popup.create to make button keyboard focusable and usable
 * @type {Window.app.ui.popup.create}
 */
app.ui.popup.legacyCreate = app.ui.popup.create;
app.ui.popup.create = function(message, onyes, onno) {
    let ctn = app.ui.popup.legacyCreate(message, onyes, onno);
    ctn.closest('.reveal').find('.button').attr('tabindex', 0).on('keyup', function(e) {
        if (e.keyCode === 13) {
            $(this).trigger('click');
        }
    });
    setTimeout(function() {
        ctn.closest('.reveal').find('.button:first').focus();
    }, 100);
    return ctn;

};

/**
 * Add h hours to a date
 * @param h
 * @returns {Date}
 */
Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h*60*60*1000));
    return this;
}

/**
 * Capitilize first letter
 * @returns {string}
 */
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

/**
 * Diff between array
 * @param a first array
 * @param b second array
 * @returns {*[]}
 */
window.app.utilities.arrayDiff = function(a, b) {
    return a.filter(function(i) {return b.indexOf(i) < 0;});
}

/**
 * Disable window.onbeforeunload
 */
app.ui.disableWarnOnLeaving = function() {
    window.onbeforeunload = null;
    $(window).unbind('beforeunload');
}

/**
 * Enable window.onbeforeunload
 * @param message
 */
app.ui.warnOnLeaving = function(message) {
    if(message === undefined) {
        message = lang.tr('confirm_leave_page').out();
    }
    if(window.onbeforeunload === null) {
        // Set message to display if the user changes pages / close tab / close browser
        window.onbeforeunload = function () {
            return message; // Ask for leaving confirmation
        };
    }
}