app.event.on('started', function() {

    var answersComponent = $('[data-component="survey-reply"], [data-component="survey-results"]');
    if(!answersComponent.length) return;

    var replyComponent = new app.view(answersComponent);

    /**
     * Add timezone to download url
     */
    replyComponent
        .find('[data-action="download_csv"]')
        .attr('href',
            replyComponent.find('[data-action="download_csv"]').attr('href') + encodeURIComponent(moment.tz.guess()));

    /**
     * Get a loading answer spinner
     * @returns {*|jQuery}
     */
    app.ui.getAnswersSpinner = function() {
        var spinner = $('.loading-answers');
        var sp2 = $('<div>').addClass('small float-right callout primary text-right');
        sp2.append(app.lang.tr('loading_answers').text + ' ');
        sp2.append($('<i>').addClass('fa fa-lg fa-spinner fa-pulse'));
        spinner.append(sp2);
        return spinner;
    }

    /**
     * Get all answers by page of page size (except the answer of participantId)
     * @param questionId
     * @param pageSize
     * @param participantId
     */
    app.ui.getAllAnswers = function(questionId, pageSize, participantId) {

        let offset = {};
        let icons = {
            "selected_value_yes":"fa-check",
            "selected_value_no":"fa-times",
            "selected_value_maybe":"fa-check",
            "undefined":"fa-question"
        };


        let getPropositionOrder = function(qId) {
            let answerTable = $('.answer-table[data-question-id="' + + qId + '"]');

            return answerTable.find('th[data-proposition-id]')
                .map(function(){return parseInt($(this).attr("data-proposition-id"));}).get();
        }
        let propositionOrder = getPropositionOrder(questionId);
        let propositionPosition = {};
        for(let i = 0; i < propositionOrder.length; i++) {
            propositionPosition[propositionOrder[i]] = i;
        }

        let getTemplateRow = function(qId)  {
            let $row = $('<tr>').addClass('results answer_row');
            let sortedProposition = answersComponent
                .find('.answer-table[data-question-id="' + + qId + '"] tr')
                .find('th[data-proposition-id]')
                .sort(function(header1, header2) {
                    return parseInt($(header1).attr('data-position')) - parseInt($(header2).attr('data-position'));
                });

            sortedProposition.each(function(){

                if($(this).attr("data-proposition-id") === undefined)
                    return;

                let choice_value = 'undefined';
                let $cell = $('<td>').addClass('choice ' + choice_value);
                $cell.attr('data-proposition-id', $(this).attr("data-proposition-id"));
                $cell.attr('data-participant-ref', '');
                $cell.attr('data-value', choice_value);

                if($(this).hasClass('winner') || $(this).hasClass('top-winner')) {
                    $cell.addClass('winner');
                }

                $row.append($cell.append($('<span>').addClass('fa ' + icons[choice_value])));

            });
            return $row;
        }


        let getAnswers = function(qId, pSize, success_callback, pId) {
            if(offset[qId] === undefined)
                offset[qId] = 0;

            let filter = '';
            if(pId !== undefined) {
                filter = 'filter=' +  JSON.stringify({not:{equals:{participant_id:pId}}}) +'&'
            }

            filter = '?' + filter + 'format=json&count=' + pSize + '&startIndex=' + offset[qId];
            app.client.get('/question/' + qId + '/answer' + filter,
                function(data) {success_callback(data)},
                {error: function(e) {
                    app.event.trigger('error_when_loading_answers');
                    app.ui.error(e);
                }}
            );
            offset[qId] += pSize;
        }

        let handleAnswers = function(data){
            let answerTable = $('.answer-table[data-question-id="' + + questionId + '"] tbody');
            let participantTable = answerTable.parents('.survey-question').find('.participant-table tbody');
            let $templateRow = getTemplateRow(questionId);
            let hasComment = false;

            if(answerTable.length > 0) {
                for(let answer_index in data) {
                    let answer_data = data[answer_index];
                    let $row = $templateRow.clone();

                    // Participant column
                    let $participantCell = $('<th>')
                        .addClass('participant-name participant-' + answer_data.participant.participant_hash);
                    $participantCell.attr('data-participant-ref', answer_data.participant.participant_hash);

                    let $participantLabel = $('<label>').attr('title', answer_data.participant.name);
                    $participantLabel.append(answer_data.participant.name);
                    $participantCell.append($participantLabel);
                    //Owner can perform actions on participants
                    if(answer_data.participant.has_email
                        && answer_data.participant.type === 'DECLARATIVE'
                        && answersComponent.attr('data-user-role') === '2') {
                        let $participantAction = $('<span>').addClass('participant-actions');
                        $participantAction.append($('<i>').addClass('fa fa-envelope-o').attr({
                            'data-action': 'send_token_email',
                            'data-participant-id': answer_data.participant.id,
                            'title': app.lang.tr('send_token_email_notice')
                        }));

                        $participantCell.append($participantAction);
                    }

                    let $commentCell = $('<th>').addClass('comment-column');
                    $commentCell.attr('data-participant-ref', answer_data.participant.participant_hash);
                    if(answer_data.has_comment) {
                        hasComment = true;

                        $commentCell.attr('data-answer-id', answer_data.answer_id);
                        let $commentIcon = $('<i>')
                            .addClass('fa fa-comment-o')
                            .addClass((answer_data.comment_masked?'comment-masked':''))
                            .attr({
                                'aria-hidden': 'true',
                                'title': app.lang.translate('unmask_comment'),
                                'data-action': 'show_comment'
                            });

                        $commentCell.append($commentIcon);
                    } else {
                        $commentCell.append('<i class="fa fa-comment-o vhidden"></i>');
                    }

                    if(participantTable.length === 0) {
                        $row.prepend($participantCell).prepend($commentCell);
                    } else {
                        $('<tr>').addClass('participant-row').append($commentCell).append($participantCell).appendTo(participantTable);
                    }

                    // Answers columns
                    if(answer_data.choices !== undefined) {
                        //Sorting
                        answer_data.choices.sort(function(a, b) {
                            return propositionOrder
                                .indexOf(parseInt(a.proposition_id)) - propositionOrder.indexOf(parseInt(b.proposition_id))
                        });
                        for(let choice_index in answer_data.choices) {
                            let choice_value = answer_data.choices[choice_index].value;
                            let $cell = $row
                                .find('[data-proposition-id="' + answer_data.choices[choice_index].proposition_id + '"]')
                                .addClass('choice ' + choice_value);
                            $cell.attr('data-value', choice_value);
                            let $icon = $('<span>').addClass('fa ' + icons[choice_value]);
                            $cell.html($icon);
                        }
                        $row.find('[data-participant-ref]').attr('data-participant-ref', answer_data.participant.participant_hash);
                    }
                    $row.appendTo(answerTable);
                }
                // If there is no comment we hide comment column
                if(hasComment === false && answerTable.attr('has-comments') !== 'yes') {
                    answerTable.parents('.survey-question').find('th.comment-column').addClass('no-comment').hide();
                }

                //If for this question one answer has a comment, we should show all comment-column
                if(hasComment && answerTable.attr('has-comments') !== 'yes') {
                    answerTable.parents('.survey-question').find('th.comment-column').removeClass('no-comment').show();
                    answerTable.parents('.survey-question').find('.first').attr('colspan', 2);
                    answerTable.attr('has-comments', 'yes');
                }
            }

            if(data.length === pageSize) {
                getAnswers(questionId, pageSize, handleAnswers, participantId);
            } else {
                app.event.trigger('answers_loaded_' + questionId);
            }
        }
        getAnswers(questionId, pageSize, handleAnswers, participantId);
    }

    app.ui.loadAllAnswers = function() {
        if(answersComponent.find('.answer-table[data-question-id] .answer_row').length === 0) {
            let spinner = app.ui.getAnswersSpinner().hide();
            spinner.fadeIn(200);

            let questionCount = answersComponent.find('.answer-table[data-question-id]').length;
            let questionsProcessed = 0;

            app.event.on('error_when_loading_answers', function() {
                spinner.fadeOut(300);
            });

            answersComponent.find('.answer-table[data-question-id]').each(function () {
                app.event.on('answers_loaded_' + $(this).attr('data-question-id'), function () {
                    questionsProcessed++;
                    if(questionCount === questionsProcessed) {
                        app.event.trigger('all_answers_loaded');
                        answersComponent.find('[title][title!=""]:visible').attr('data-tooltip','').foundation();
                        spinner.fadeOut(300);
                    }

                    //We changed answer-table we might need to recompute headervisible cell size
                    $('[data-shown="1"]').attr('data-shown', 0);
                });
                app.ui.getAllAnswers($(this).attr('data-question-id'), 50, replyComponent.attr('data-participant-id'));
            });
        }
    }

    /**
     * Update a question sums
     * @param $currentQuestion
     */
    let questionSumUpdater = function($currentQuestion) {
        let maxSum = 0;
        let maxSumYesPlusMaybe = 0;
        let winners = [];
        let maybeWinners = [];

        let satisfactionRateTranslation = app.lang.tr('satisfaction_rate').out();

        $currentQuestion.find('tr.header_row th[data-proposition-header-ref].top-winner').removeClass('top-winner');
        $currentQuestion.find('[data-proposition-id].winner').removeClass('winner');
        $currentQuestion.find('[data-proposition-id].maybe-winner').removeClass('maybe-winner');

        $currentQuestion.find('.sum').each(function() {
            let sum = $currentQuestion
                .find('td.choice[data-proposition-id="' + $(this).attr('data-proposition-id') + '"].selected_value_yes:not(.disabled)').length;

            let sum_yes_plus_maybe = sum + $currentQuestion
                .find('td.choice[data-proposition-id="' + $(this).attr('data-proposition-id') + '"].selected_value_maybe:not(.disabled)').length;

            let total = $currentQuestion
                .find('td.choice[data-proposition-id="' + $(this).attr('data-proposition-id') + '"]:not(.disabled)').length;

            if($(this).find('.real-sum').length > 0) {
                $(this).find('.real-sum').text(sum);
            } else {
                $(this).text(sum);
            }

            let $sumYesPlusMaybeCell = $currentQuestion
                .find('td.sum_yes_plus_maybe[data-proposition-id="' + $(this).attr('data-proposition-id') + '"]');

            if($sumYesPlusMaybeCell.find('.real-sum').length > 0) {
                $sumYesPlusMaybeCell.find('.real-sum').text(sum_yes_plus_maybe);
            } else {
                $sumYesPlusMaybeCell.text(sum_yes_plus_maybe);
            }

            let percentage = 0.00;
            if(total !== 0) {
                percentage = parseFloat(sum / total).toFixed(4);
            }

            $(this).attr('data-percentage', percentage);
            $(this).attr('title', satisfactionRateTranslation + ' : ' + (percentage * 100).toFixed(2) + '%');

            if($(this).data('tip') !== undefined) {
                $(this).data('tip').template.html($(this).attr('title'));
                $(this).removeAttr('title');
            }

            $('#' + $(this).attr('data-toggle')).html($(this).attr('title')); // assign span content

            if(sum > maxSum) {
                maxSum = sum;
                //Max changed, winners changes
                winners = [];
                winners.push($(this));
            } else if(sum === maxSum) {
                winners.push($(this));
            }

            if(sum_yes_plus_maybe > maxSumYesPlusMaybe) {
                maxSumYesPlusMaybe = sum_yes_plus_maybe;
                //Max changed, maybeWinners changes
                maybeWinners = [];
                maybeWinners.push($sumYesPlusMaybeCell);
            } else if(sum_yes_plus_maybe === maxSumYesPlusMaybe) {
                maybeWinners.push($sumYesPlusMaybeCell);
            }
        });

        for(let i=0; i<winners.length; i++) {
            winners[i].addClass('winner');

            // Now setting class winner to all winner cells except header_row
            let propositionHeaderRef = $currentQuestion
                .find('tr.bottom_row th[data-proposition-id="' + winners[i].attr('data-proposition-id') + '"]')
                .addClass('winner')
                .attr('data-proposition-header-ref');
            $currentQuestion.find('td.choice[data-proposition-id="' + winners[i].attr('data-proposition-id') + '"]')
                .addClass('winner');
            // Now setting class top-winner to all winner cells except
            $currentQuestion.find(
                'tr.header_row th[data-proposition-header-ref="' + propositionHeaderRef + '"],' +
                'tr.header_row th[data-proposition-id="' + winners[i].attr('data-proposition-id') + '"]')
                .addClass('top-winner');

        }

        for(let i=0; i<maybeWinners.length; i++) {
            maybeWinners[i].addClass('maybe-winner');
        }
    }

    /**
     * Update all questions sums
     */
    var sumUpdater = function() {
        replyComponent.find('.survey-question').each(function() { questionSumUpdater($(this)); });
    }

    /**
     * Get proposition label from an element of this proposition column
     * @param columnElement (must refer proposition via data-proposition-id
     * @returns {string}
     */
    var getPropositionLabel = function(columnElement) {
        const propositionRef = $(columnElement).attr('data-proposition-id');

        if(propositionRef === undefined)
            return '';

        const $currentQuestion = $(columnElement).parents('.survey-question');
        const propositionHeaderRef = $currentQuestion
            .find('tr.bottom_row th[data-proposition-id="' + propositionRef + '"]')
            .attr('data-proposition-header-ref');
        const $currentHeaderColumn = $currentQuestion
            .find('tr.header_row th[data-proposition-header-ref="' + propositionHeaderRef + '"]');

        //Sometimes there is no header ref because there is no bottom_row
        let proposition = undefined;
        if (propositionHeaderRef !== undefined) {
            proposition = $currentHeaderColumn.filter(':first').text();
        } else { //In this case its in the header_row
            proposition = $currentQuestion.find('tr.header_row th[data-proposition-id="' + propositionRef + '"]:first').text()
        }

        proposition += ' ' + $currentQuestion.find('tr.bottom_row th[data-proposition-id="' + propositionRef + '"]:first').text();

        return proposition;
    }

    /**
     * Init all bindings
     */
    var initBindings = function() {

        /**
         * Add titles to each inputs
         */
        $(window).on('adapted', function() {

            if(answersComponent.attr('data-survey-timezone') === moment.tz.guess()) {
                let momentsOfDay = app.config.hasOwnProperty('moments_of_day') ? app.config.moments_of_day : {};
                answersComponent.find('.answer-table[data-question-input-mode="moments"]').each(function () {
                    $(this).find('th[data-proposition-id]').each(function () {
                        for (let mmt in momentsOfDay) {
                            if ((momentsOfDay[mmt].bt + momentsOfDay[mmt].et) === $(this).find('[data-localize="time"]').text()) {
                                $(this).prepend(app.lang.tr(mmt).out()).find('.range_of_hours').remove();
                            }
                        }
                    });
                });
            }

            if(answersComponent.attr('data-component') === 'survey-results')
                app.ui.loadAllAnswers();

            //Updating tip on show_participants_emails button
            if(answersComponent.find('[data-action="show_participants_emails"]').length > 0) {
                answersComponent.find('td.proposition-actions').each(function() {
                    window.app.ui.updateTooltip($(this).find('[data-action="show_participants_emails"]'),
                        app.lang.tr('display_participants_who_answered_yes_to') + ' '
                        + getPropositionLabel(this)
                    );
                })
            }

        });

        /*******************************************************
         * Comment mask/unmask
         *******************************************************/
        answersComponent.on('click', '[data-action="show_comment"]', function() {
            //Show loader
            app.ui.toggleLoader(true);
            var answerId = $(this).parent().attr('data-answer-id');
            var element = $(this);

            if(answerId === undefined)
                return;

            app.client.get('/answer/' + answerId + '/comment',
                function(data) {
                    app.ui.toggleLoader(false);
                    var message = $('<div>');
                    var callback = {
                        close: undefined
                    };
                    message.attr('id', 'commentModal' + answerId);

                    if(data.date !== undefined) {
                        var date_info = $('<i>');
                        var date_itself = $('<span>').text(window.app.ui.timeLocalizer().localize('date_time', data.date));
                        date_info.text('(' + app.lang.tr('on_date') + ' ').append(date_itself).append(')');
                        message.append(date_info);
                    }

                    if(data.content !== undefined) {
                        message.append('<p><i>"' + data.content + '"</i></p>');
                    }

                    if(data.can_perform_action !== undefined && data.can_perform_action && undefined !== answerId) {
                        if (element.hasClass('comment-masked')) {
                            callback.unmask_comment = function () {
                                app.client.post('/answer/' + answerId + '/unmask_comment', {}, function (path, data) {
                                    window.app.ui.notify('info', app.lang.tr('comment_unmasked'));
                                    element.removeClass('comment-masked')
                                });
                            };
                        } else {
                            callback.mask_comment = function () {
                                app.client.post('/answer/' + answerId + '/mask_comment', {}, function (path, data) {
                                    window.app.ui.notify('info', app.lang.tr('comment_masked'));
                                    element.addClass('comment-masked')
                                });
                            };
                        }
                    }

                    var popup = app.ui.popup.create(lang.tr('comment_of') + ' ' + ((data.participant_name !== undefined)?data.participant_name:''), callback);

                    popup.html(message);
                    app.ui.popup.open(popup);
                }
            );
        });

        /*******************************************************
         * Hover effect on choices
         *******************************************************/
        var hovering;
        answersComponent.on('mouseenter','tr [data-proposition-id], tr [data-participant-ref]', function() {

            var that = this;
            clearTimeout(hovering);
            hovering = setTimeout(function() {

                const participantRef = $(that).attr('data-participant-ref');
                const propositionRef = $(that).attr('data-proposition-id');
                const $currentQuestion = $(that).parents('.survey-question');
                const propositionHeaderRef = $currentQuestion
                    .find('tr.bottom_row th[data-proposition-id="' + propositionRef + '"]').attr('data-proposition-header-ref');

                const $currentColumn = $currentQuestion.find('tr [data-proposition-id="' + propositionRef + '"]');
                const $currentRow = $currentQuestion.find('tr [data-participant-ref="' + participantRef + '"]');
                const $currentHeaderColumn = $currentQuestion.find('tr.header_row th[data-proposition-header-ref="' + propositionHeaderRef + '"]');

                $currentColumn.addClass('hovered');
                $currentRow.addClass('hovered');
                $currentHeaderColumn.addClass('hovered');

                //If screen is small we don't show those tooltips
                let isSmall = false;
                try {
                    isSmall = (Foundation.MediaQuery.current === 'small');
                } catch (e) {}

                if($(that).attr('data-value') !== undefined && !isSmall) {

                    // Computing new tip
                    const participantName = $currentRow.filter('.participant-name:first').find('label').text();
                    //Sometimes there is no header ref because there is no bottom_row
                    let proposition = '';
                    if (propositionHeaderRef !== undefined) {
                        proposition = $currentHeaderColumn.filter(':first').text();
                    } else { //In this case its in the header_row
                        proposition = $currentQuestion
                            .find('tr.header_row th[data-proposition-id="' + propositionRef + '"]:first').text()
                    }

                    proposition += ' ' + $currentQuestion
                        .find('tr.bottom_row th[data-proposition-id="' + propositionRef + '"]:first').text();

                    let title = ((participantName !== '')?participantName + ' - ':'')
                        + proposition
                        + ' - ' + app.lang.tr($(that).attr('data-value'));

                    if($(that).attr('title-backup') !== title) {
                        window.app.ui.updateTooltip(that, title);
                        $(that).off('mouseenter.zf.tooltip');
                    }
                    $(that).foundation('show');
                }
            }, 10);
        });

        answersComponent.on('mouseleave', 'tr [data-proposition-id], tr [data-participant-ref]', function() {
            clearTimeout(hovering);

            replyComponent.find('.hovered').removeClass('hovered');
        });

        /***************************************
         * Decision making bindings
         ***************************************/

        /**
         * Show/Hide checkboxes to ignore an answer
         */
        replyComponent.find('input[name="decision_making_tool_switch"]').on('change', function() {
            var display = $(this).is(':checked');

            replyComponent.find('th.participant-name[data-participant-ref] label').each(function() {
                var i = $('<input type="checkbox" checked="checked" data-action="ignore_answer" />');

                if($(this).find('input[type="checkbox"]').length === 0) {
                    $(this).prepend(i);
                } else {
                    i = $(this).find('input[type="checkbox"]');
                }
                i.toggle(display);
            });
            //Removing previously disabled tags
            if(!display) {
                replyComponent.find('tr [data-participant-ref].disabled').removeClass('disabled');
            }
            //We check all checkboxes
            replyComponent.find('input[data-action="ignore_answer"]').prop('checked', true);

            //Recomputing sum
            if(replyComponent.find('input[data-action="ignore_answer"]').length > 0)
                sumUpdater();

        });

        /**
         * Handling change on the ignore checkbox
         */
        answersComponent.on('change', 'input[data-action="ignore_answer"]', function(e) {
            if(replyComponent.find('input[name="decision_making_tool_switch"]').is(':checked')) {
                var participantRef = $(this).parents('th').attr('data-participant-ref');

                //If not checked => then disabled
                if (!$(this).is(':checked')) {
                    $(this).parents('.survey-question').find('tr [data-participant-ref="' + participantRef + '"]').addClass('disabled');
                } else {
                    $(this).parents('.survey-question').find('tr [data-participant-ref="' + participantRef + '"]').removeClass('disabled');
                }
                //Update current question sums
                questionSumUpdater($(this).parents('.survey-question'));
            }
        });

        /***************************************
         * Sending token mail
         ***************************************/
        answersComponent.on('click', '[data-action="send_token_email"]', function() {

            if($(this).attr('data-participant-id') !== undefined) {
                //Show loader
                app.ui.toggleLoader(true, app.lang.tr('sending_recovery_email').out());

                app.client.post('/participant/participant_token', {
                        participant_id : $(this).attr('data-participant-id')
                    },
                    function (path, data) {
                        if(data) {
                            window.app.ui.notify('info', app.lang.tr('token_sent'));
                        } else {
                            window.app.ui.notify('error', app.lang.tr('recovery_email_could_not_be_sent_try_later'));
                        }
                        app.ui.toggleLoader(false);
                    });
            }
        });

        /***************************************
         * Sending final answers
         ***************************************/
        answersComponent.on('click', '[data-action="send_final_answers"]', function() {

            var final_answers = [];
            //One proposition must be choosen by question
            var valid = true;

            answersComponent.find('.final_answer_row').each(function() {
                //It's not a final answer selection row
                if($(this).find('.final-choice').length === 0)
                    return;

                var answer_valid = false;
                var answer = {
                    participant : {
                        type : 'ULTIMATE'
                    },
                    question_id : $(this).attr('data-question-id'),
                    choices : [],
                    comment : ''
                }

                $(this).find('.final-choice').each(function() {
                    var choice = {
                        proposition_id: $(this).attr('data-proposition-id')
                    }

                    if($(this).find('[name^="proposition"]').is(':checked')) {
                        choice.value = 'selected_value_yes';

                        answer_valid = true;
                    } else {
                        choice.value = 'selected_value_no';
                    }

                    answer.choices.push(choice);
                });

                $(this).parents('.survey-question').find('.invalid-answer-message').toggle(!answer_valid);

                if(!answer_valid) {
                    $(this).parents('.survey-question').find('.invalid-answer-message span')
                        .text(app.lang.tr('please_select_value_for_question'));
                }

                valid = valid && answer_valid;
                final_answers.push(answer);
            });

            //If all question have a selected answer
            if(valid) {
                var message = app.lang.tr('final_answer_selection_warning');

                //Hiding global message
                $(this).parent().find('.all-questions-must-be-answered').hide();

                app.ui.popup.confirm(message, function() {
                    app.ui.toggleLoader(true);
                    app.client.post('/answer', final_answers, function (path, data) {
                        app.ui.toggleLoader(false);

                        window.app.ui.notify('info', app.lang.tr('answer_saved'));

                        if (data.answers !== undefined) {
                            window.location.reload();
                        }
                    });
                });
            } else {
                //Showing global error message
                $(this).parent().find('.all-questions-must-be-answered').show();
            }

        });


        /***************************************
         * Show participants answers
         ***************************************/
        answersComponent.on('click', '[data-action="show_answers"]', function() {
            $(this).hide();

            $(this).parents('.survey-question').find('section.hidden').show();

            window.app.ui.adapter();
        });

        /***************************************
         * Expand/compress answers
         ***************************************/
        answersComponent.on('click', '[data-action="expand_view"]', function() {
            if(!$(this).parents('.survey-question').hasClass('maximized')) {
                //Offseting to the left to
                var marginLeft = -1 * ($(this).parents('.survey-question').offset().left - ($(window).width() * 0.025));
                $(this).parents('.survey-question').css('margin-left', marginLeft);
            } else {
                $(this).parents('.survey-question').css('margin-left', '');
            }

            $(this).parents('.survey-question').toggleClass('maximized');

            var answerContainer = $(this).parents('.survey-question').find('.answers-container');
            //We wait before event because of CSS transition being (0.5s)
            setTimeout(function() {
                answerContainer.trigger('scroll');
            }, 700);


            $(this).toggleClass('fa-expand');
            $(this).toggleClass('fa-compress');
        });

        /***************************************
         * Show participants emails
         ***************************************/
        answersComponent.on('click', '[data-action="show_participants_emails"]', function() {

            var surveyId = answersComponent.attr('data-survey-id');

            var filter = '';
            if($(this).parent().attr('data-proposition-id') !== undefined && $(this).parents('tr').attr('data-question-id') !== undefined) {
                filter = '?filter={"hasAnswered":{"question_id":"'
                    + $(this).parents('tr').attr('data-question-id')
                    +'","proposition_id":"'
                    + $(this).parent().attr('data-proposition-id') + '","value":"yes"}}'
            }

            app.client.get('/survey/' + surveyId + '/participants/@gecos' + filter,
                function(data) {
                    var message = $('<div>');
                    message.addClass('participants-emails-modal');
                    if(filter.length > 0) {
                        message.append('<span>' + app.lang.tr('participants_emails_with_yes_for_this_proposition') + ' :</span>');
                    } else {
                        message.append('<span>' + app.lang.tr('participants_emails') + ' :</span>');
                    }
                    var emailListArea = $('<textarea>');
                    emailListArea.addClass('small-11');
                    emailListArea.addClass('columns');
                    var emails = '';
                    if(data.length > 0) {
                        emails = data.join(';\n');
                        emailListArea.val(emails);
                    } else {
                        emailListArea.val(app.lang.tr('no_participant_email_found'));
                    }
                    message.append(emailListArea);

                    var copyToClipBoard = $('<span class="small-1 columns fa fa-clipboard" data-share-link title="'
                        + app.lang.tr('copy_to_clipboard')
                        +'" data-action="copy_to_clipboard" data-content=""> </span>');
                    copyToClipBoard.attr('data-content', emails);
                    copyToClipBoard.on('click', function() {
                        window.app.utilities.copyToClipboard(emails.replace(/[\n\r]/g, ''));
                    });

                    message.append(copyToClipBoard);
                    var popup = app.ui.popup.create(lang.tr('participants_emails_dialog'));
                    popup.html(message);
                    app.ui.popup.open(popup);
                }
            );

        });
    }
    initBindings();
});
