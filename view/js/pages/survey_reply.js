/**
 * Controller for reply page
 */
class ReplyController {

    constructor(replyComponent) {
        this.replyComponent = new app.view(replyComponent);
        this.answersLoaded = false;
        this.conflicts = undefined;

        this.initBindings();

        this.checkForPending();

        this.loadAllConflicts();
    }

    /**
     * collects all answers in page
     * @returns {[]|*[]}
     */
    collectAnswers() {
        let answers = [];
        let validAnswers = true;

        this.replyComponent.find('[data-form="question-form-reply"]').each(function() {

            let answer = {
                question_id : $(this).attr('data-question-id'),
                choices : [],
                comment : ''
            }

            //Collecting answer id if set
            if($(this).attr('data-answer-id') !== undefined && $(this).attr('data-answer-id') !== '') {
                answer.answer_id = $(this).attr('data-answer-id');
            }

            if($(this).parents('section.survey-question').length > 0
                && $(this).parents('section.survey-question').find('input[name="leave_a_comment"]').is(':checked')
                && $(this).parents('section.survey-question').find('[name="comment"]').length > 0) {
                answer.comment = $(this).parents('section.survey-question').find('[name="comment"]').val();
            }

            //Collecting each proposition choices
            $(this).find('td.proposition').each(function() {
                answer.choices.push({
                    proposition_id : $(this).attr('data-proposition-id'),
                    value: $(this).attr('data-value')
                });
            });
            //A force unique choice require that at least and at most one choice must be equal to 'selected_value_yes'
            if('1' === $(this).attr('data-force-unique-choice')) {
                let valid = false;
                for(let i = 0; i < answer.choices.length; i++) {
                    valid = ('selected_value_yes' === answer.choices[i].value);
                    if(valid) break;
                }
                $(this).parents('.survey-question').find('.invalid-answer-message').toggle(!valid);
                if(!valid) {
                    $(this).parents('.survey-question').find('.invalid-answer-message span')
                        .text(app.lang.tr('please_select_value_for_question'));
                    //An answer is not valid, we must not send those answers
                    validAnswers = false;
                }
            }

            answers.push(answer);
        });

        if(!validAnswers)
            return [];

        return answers;
    };

    /**
     * Callback for PUT and POST answers
     * @param data
     */
    sendAnswersCallback(data) {
        app.ui.toggleLoader(false);
        //Answers are saved, no need to warn on leaving
        app.ui.disableWarnOnLeaving();

        window.app.ui.notify('success', app.lang.tr('answer_saved'));

        let replyURL = window.location.protocol + '//' + window.location.host + window.location.pathname;

        $('#answersSaved').find('#answer-edition-link').attr('href',  replyURL);
        $('#answersSaved').foundation('open');

        if(data.answers !== undefined) {

            if(data.answers[0].participant.participant_token !== undefined) {
                let replyURLwToken = replyURL + "?participant_token=" + data.answers[0].participant.participant_token;
                $('#answersSaved').find('#answer-edition-link').attr('href',  replyURLwToken);

                //Setting up participant token
                this.replyComponent.find('input[name="participant_token"]').val(data.answers[0].participant.participant_token);

                //Disable any warn on leaving
                // $(window).unbind();
                app.ui.disableWarnOnLeaving();

                //Automatic redirect in n seconds
                let timeBeforeRedirection = 8000;
                $('#answersSaved').find('.info').show();
                $('#answersSaved').find('.counter').text(timeBeforeRedirection / 1000);
                let updateCounter = setInterval(function() {
                    timeBeforeRedirection -= 1000;
                    $('#answersSaved').find('.counter').text(timeBeforeRedirection / 1000);
                }, 1000)

                setTimeout(function() {
                    clearInterval(updateCounter);
                    window.location.replace(replyURLwToken);
                }, timeBeforeRedirection);
            }

            //Propagating new answers id
            for(let i = 0; i < data.answers.length; i++) {
                this.replyComponent
                    .find('[data-form="question-form-reply"][data-question-id="' + data.answers[i].question_id + '"]')
                    .attr('data-answer-id', data.answers[i].answer_id);
            }

            //Changing action of send_answers button
            this.replyComponent.find('[data-action="send_answers"]').attr("data-sub-action", "update");
        }
    }

    /**
     * Save answers
     *
     * @param answers
     * @param options
     */
    sendAnswers(answers, options) {
        let that = this;

        //Keeping answers collection
        app.ui.answers = answers;

        //Disabling send_answer button when answer-edition is disabled
        if(this.replyComponent.attr('data-disable-answer-edition') === '1') {
            that.replyComponent.find('[data-action="send_answers"]').addClass('disabled');
        }
        app.ui.toggleLoader(true);
        if(that.replyComponent.find('[data-action="send_answers"]').attr('data-sub-action') === 'send') {
            options.error = that.handleAnswerPostError;
            app.client.post('/answer', answers, function(path, data) {
                that.sendAnswersCallback(data);
            }, options);
        } else {
            app.client.put('/answer', answers, function(data) {
                that.sendAnswersCallback(data);
            }, options);
        }
    }

    /**
     * Handle POST survey error
     */
    handleAnswerPostError(error) {
        // Enabled send_answers
        $('[data-action="send_answers"]').removeClass('disabled');

        if(error.message === 'participant_already_exists_for_survey') {

            if(error.details !== undefined && error.details.participant_type !== undefined && error.details.editable_answers !== undefined) {
                if ('DECLARATIVE' === error.details.participant_type) {
                    $('.reveal.participant_already_exists').find('.authenticated_participant_recovery').addClass('hidden');
                    $('.reveal.participant_already_exists').find('.declarative_participant_recovery').removeClass('hidden');
                }
                if ('AUTHENTICATED' === error.details.participant_type) {
                    $('.reveal.participant_already_exists').find('.declarative_participant_recovery').addClass('hidden');
                    $('.reveal.participant_already_exists').find('.authenticated_participant_recovery').removeClass('hidden');
                }

                if(error.details.editable_answers) {
                    $('#participant_already_exists').foundation('open');
                } else {
                    $('#participant_already_exists_edition_disabled').foundation('open');
                }
            }

        }
    }

    updatePropositionInput(propositionInput) {
        let propositionTd = propositionInput.parents('td');

        if(propositionInput.attr('type') === 'radio') {
            if (propositionInput.hasClass('yes_no_maybe_radio')) {
                propositionTd.attr('data-value', propositionInput.filter(':checked').val());
                propositionTd.find('label').removeClass('selected').addClass('hollow');

                propositionTd.find('input.yes_no_maybe_radio').filter(':checked').parents('label').addClass('selected').removeClass('hollow');
            } else {
                propositionInput.parents('tr').find('td.proposition').attr('data-value', 'selected_value_no');
                propositionTd.parents('tr').find('input[type="radio"]').filter(':checked').parents('td').attr('data-value', 'selected_value_yes');
            }
        } else {
            propositionTd.attr('data-value', propositionInput.val());
        }

        //Clear title
        propositionTd.removeAttr('title');
        propositionTd.trigger('mouseenter');
    }

    /**
     * Update sum row
     */
    updateSums() {
        let that = this;

        this.replyComponent.find('td.proposition').each(function () {
            let sum = that.replyComponent.find('td.selected_value_yes[data-proposition-id="' + $(this).attr('data-proposition-id') + '"]').length;

            if($(this).attr('data-value') === 'selected_value_yes') {
                sum += 1;
            }
            if (that.replyComponent.find('td.sum.proposition_' + $(this).attr('data-proposition-id')).find('.real-sum').length > 0) {
                that.replyComponent.find('td.sum.proposition_' + $(this).attr('data-proposition-id') + ' .real-sum').text(sum);
            } else {
                that.replyComponent.find('td.sum.proposition_' + $(this).attr('data-proposition-id')).text(sum);
            }
        })
    }

    /**
     * Init all listeners on page
     */
    initBindings() {
        let that = this;

        /**
         * Add titles to each inputs
         */
        $(window).on('adapted', function() {

            //Hiding participant column
            that.replyComponent.find('.first').hide();
            that.replyComponent.find('.answers').each(function() {
                if($(this).parents('.survey-question').find('.participant-table').length > 0) {
                    $(this).removeClass('small-10').addClass('small-12');
                }
            });

            $('input[name^="proposition_"]').each(function() {
                if($(this).attr('type') === 'radio' && $(this).hasClass('yes_no_maybe_radio')) {
                    //Switch on proposition type
                    $(this).parents('label').attr('title',
                        app.lang.tr($(this).val()+'_'+$(this).parents('td.proposition').attr('data-proposition-type')+'_notice'));
                }
            });
        });

        /**
         * Delegating click on td.proposition to real input
         */
        this.replyComponent.node.on('click', 'tr.your_choice td.proposition', function(e) {
            //Do nothing if .header was not directly clicked
            if(e.target !== e.currentTarget) return;

            $(this).find('.yes_no_selector, .yes_no_maybe_selector, input[type="radio"]').trigger('click');
        });

        this.replyComponent.node.find('.yes_no_maybe_radio').parents('label')
        this.replyComponent.node.on('keypress', '.field-yes-no-maybe-radio label, .select-for-all-container label', function(e) {
            $(this).trigger('click');
        });

        /***************************************************
         * Select for all
         ***************************************************/
        this.replyComponent.node.on('click', '.select-for-all-container .button', function() {
            //Css button edition
            $(this).parents('.select-for-all-container').find('.button').removeClass('selected').addClass('hollow');
            $(this).removeClass('hollow');

            //Changing all values
            var value = $(this).attr('data-value');
            var $surveyQuestion = $(this).parents('.survey-question');

            $surveyQuestion.find('input[value="' + value + '"][type="radio"]').prop('checked', true);
            $surveyQuestion.find('input[type="hidden"]').val(value)

            that.updatePropositionInput($surveyQuestion.find('input[value="' + value + '"][type="radio"]'));
            that.updateSums();

            var element = $(this);
            setTimeout(function() {element.addClass('hollow');}, 200);
        });

        this.replyComponent.find('.survey-questions').on('change', '[name="comment"]', function() {
            //Comment as changed
            app.ui.warnOnLeaving();
        });

        /**
         * Handling value change (Update CSS classes + counts)
         */
        this.replyComponent.find('[data-form="question-form-reply"]').on('change', 'input[name^="proposition_"]', function() {

            //Answer as changed
            app.ui.warnOnLeaving();

            that.updatePropositionInput($(this));

            //We update all counts
            that.updateSums();
        });

        /**
         * Results toggler
         */
        this.replyComponent.find('input[name="show_results"]').on('change', function() {

            let $toggle = $(this);

            let displayAnswers = function($answers) {
                let $surveyQuestion = $answers.parents('.survey-question');
                if ($surveyQuestion.find('.participant-table').length > 0) {
                    $answers.toggleClass('small-10', $toggle.is(':checked'));
                    $answers.toggleClass('small-12', !$toggle.is(':checked'));
                }

                if($toggle.is(':checked')) {
                    $surveyQuestion.find('th.first, td.first, th.comment-column:not(.no-comment)').show();
                    $surveyQuestion.find('tr.results:not(:visible)').show();
                    $surveyQuestion.find('.participant-table').show();
                } else {
                    $surveyQuestion.find('tr.results, th.first, td.first, th.comment-column').hide();
                    $surveyQuestion.find('.participant-table').hide();
                }
            };

            if($toggle.is(':checked')) {
                if(!that.answersLoaded) {
                    app.event.on('all_answers_loaded', function() {
                        //refresh sums
                        that.updateSums();
                        $toggle.removeAttr('disabled');
                        $toggle.parents('.switch').removeClass('locked');
                        that.answersLoaded = true;
                    });
                    $('.answer-table[data-question-id]').each(function () {
                        displayAnswers($(this).parents('.answers'));
                    });

                    app.ui.loadAllAnswers();
                } else {
                    $('.answer-table[data-question-id]').each(function () {
                        displayAnswers($(this).parents('.answers'));
                    });
                }
            } else {
                displayAnswers(that.replyComponent.find('.answers'));
            }
        });

        /**
         * Comment form toggler
         */
        this.replyComponent.find('input[name="leave_a_comment"]').on('change', function() {
            $(this).parents('section.question-comment-container').find('.comment-form').toggle($(this).is(':checked'));
        });

        /**
         * Handle send_answers action
         */
        this.replyComponent.onAction('send_answers', function() {
            //If replier is requested to set is name and email
            let $replierForm = that.replyComponent.find('.replier_info');

            //If send_answer is disabled we do nothing
            if(that.replyComponent.find('[data-action="send_answers"]').hasClass('disabled'))
                return;

            let validationCallback = function() {

                //Getting answers
                let answers = that.collectAnswers();
                let options = {
                    headers:[]
                }

                //answers = false when something is wrong in the answer
                if(answers && answers.length > 0) {
                    answers[0].participant = {
                        name : $replierForm.find('input[name="name"]').val(),
                        email : $replierForm.find('input[name="email"]').val()
                    }

                    //There may be a participant token hidden
                    if($replierForm.find('input[name="participant_token"]').length > 0
                        && $replierForm.find('input[name="participant_token"]').val().length > 0) {
                        options.headers['X-PARTICIPANT-TOKEN'] = $replierForm.find('input[name="participant_token"]').val();

                        //If user used a recovery code, we had a specific header X-PARTICIPANT-RECOVERY
                        let $recoveryCode = $('input[name="recovery_code"]');
                        if($recoveryCode.length > 0
                            && $recoveryCode.val().length > 0) {
                            options.headers['X-PARTICIPANT-RECOVERY'] = 1;
                        }
                    }

                    // check if user accepted terms (if present it's mandatory)
                    if(!window.app.utilities.hasAcceptedTerms())
                        return;

                    that.sendAnswers(answers, options);
                }
            }

            //Trimming trailing spaces
            $replierForm.find('input[name="name"]:required').val($.trim($replierForm.find('input[name="name"]:required').val()))

            //If there are inputs for replier, we must validate it
            if($replierForm.find('input:required').length > 0) {
                //replier info
                app.forms.get('replier').getData().then(validationCallback);
            } else {
                validationCallback();
            }
        });

        this.replyComponent.onAction('delete_answers', function() {
            let surveyTitle = that.replyComponent.attr('data-survey-title');

            app.ui.popup.confirm(app.lang.tr('confirm_answers_deletion') +' : ' + surveyTitle + ' ?', function() {
                //If replier is requested to set is name and email
                let $replierForm = that.replyComponent.find('.replier_info');
                let options = {headers:[]};
                //There may be a participant token hidden
                if($replierForm.find('input[name="participant_token"]').length > 0
                    && $replierForm.find('input[name="participant_token"]').val().length > 0) {
                    options.headers['X-PARTICIPANT-TOKEN'] = $replierForm.find('input[name="participant_token"]').val();
                }

                // Show loader
                app.ui.toggleLoader(true, app.lang.tr('deleting').out());

                app.client.delete('/participant/'+ that.replyComponent.attr('data-participant-id'),  function(path, data){

                    app.ui.toggleLoader(false);

                    if(app.utilities.isLocalStorageAvailable()) {
                        localStorage.setItem('pending_success', JSON.stringify({message:app.lang.tr('your_answers_have_been_deleted').out()}));
                    }

                    let replyURL = window.location.protocol + '//' + window.location.host + window.location.pathname;
                    app.ui.disableWarnOnLeaving();
                    window.location.replace(replyURL);
                }, options);
            });
        });

        /**
         * Recovery management
         */
        $(document).on('click', '#participant_already_exists:visible [data-action="send_recovery_code"]', function(e) {
            //Show loader
            app.ui.toggleLoader(true, app.lang.tr('sending_recovery_code').out());

            app.client.post('/participantRecovery/send_code', {
                    survey_id : that.replyComponent.attr('data-survey-id'),
                    participant_email : that.replyComponent.find('input[name="email"]').val()
                },
                function (path, data) {
                    if(data) {
                        window.app.ui.notify('info', app.lang.tr('recovery_code_sent'));
                    }
                    app.ui.toggleLoader(false);

                    //Reshow input
                    $('#participant_already_exists:visible input[name="recovery_code"]').val('').removeClass('is-invalid-input').show();
                    $('#participant_already_exists:visible .too_much_tries').remove();
                });
        });

        $(document).on('input', '#participant_already_exists:visible input[name="recovery_code"]', function(e) {
            let $sendAnswersButton = $('#participant_already_exists:visible [data-action="send_answers_with_recovery_code"]');

            $sendAnswersButton.toggleClass('disabled', $(this).val().length == 0);
            $sendAnswersButton.attr('aria-disabled', $(this).val().length == 0);

            //Clearing tooltip
            if ($sendAnswersButton.attr('data-tooltip') !== undefined) {
                $sendAnswersButton.foundation('hide').foundation('destroy').attr('title', '');
            }
            if($(this).val().length == 0) {
                //New title
                $sendAnswersButton.attr('title', app.lang.tr('no_code')).attr('data-tooltip', '');
                new Foundation.Tooltip($sendAnswersButton);
            }

        });

        $(document).on('click', '#participant_already_exists:visible [data-action="send_answers_with_recovery_code"]:not(.disabled)', function(e) {
            let $sendAnswersButton = $(this);

            let recoveryForm = app.forms.get('recovery-form');

            recoveryForm.getData().then(function() {

                $('#participant_already_exists:visible .error_hint').remove();

                app.client.post('/participantRecovery/token', {
                        survey_id : that.replyComponent.attr('data-survey-id'),
                        participant_email : that.replyComponent.find('input[name="email"]').val(),
                        code : $('#participant_already_exists:visible input[name="recovery_code"]').val()
                    },
                    function (path, data) {
                        if(data && data.participant_token !== undefined) {
                            that.replyComponent.find('input[name="participant_token"]').val(data.participant_token);

                            $('#participant_already_exists').foundation('close');

                            that.replyComponent.find('[data-action="send_answers"]').attr('data-sub-action', 'update').trigger('click');
                        }

                        if(data && data.tries_left !== undefined) {
                            if(data.tries_left <= 0) {
                                $('#participant_already_exists:visible input[name="recovery_code"]').before('<div class="callout alert too_much_tries">' + app.lang.tr('too_much_tries') + '</div>');
                                $('#participant_already_exists:visible input[name="recovery_code"]').hide();
                                $sendAnswersButton.addClass('disabled');
                            } else {
                                let error = app.lang.tr('n_tries_left').out().replace('%d', data.tries_left);

                                $('#participant_already_exists:visible input[name="recovery_code"]').val('').trigger('input').focus();
                                $('#participant_already_exists:visible input[name="recovery_code"]').addClass('is-invalid-input');

                                $('<div class="error_hint" />')
                                    .insertAfter($('#participant_already_exists:visible input[name="recovery_code"]'))
                                    .text(error);
                            }
                        }
                    }
                );
            });
        });

        $(document).on('click', '#participant_already_exists:visible [data-action="log_in_recovery"]', function() {
            if(app.ui.answers !== undefined && app.utilities.isLocalStorageAvailable()) {
                let surveyId = that.replyComponent.attr('data-survey-id');

                //Storing current answers in local storage
                localStorage.setItem(
                    'pending_answers',
                    JSON.stringify({
                        'survey_id': surveyId,
                        'answers' : app.ui.answers
                    })
                );

                //Disable any warn on leaving
                app.ui.disableWarnOnLeaving();
                window.location.replace($(this).attr('data-logon-url'));
            }
        });

        let sendLink =  function(callback) {
            // Show loader
            app.ui.toggleLoader(true, app.lang.tr('sending_recovery_email').out());

            app.client.post('/participantRecovery/send_link', {
                    survey_id : that.replyComponent.attr('data-survey-id'),
                    participant_email : that.replyComponent.find('input[name="email"]').val(),
                },
                function (path, data) {
                    if(data) {
                        window.app.ui.notify('info', app.lang.tr('your_recovery_email_is_sent'));
                    } else {
                        window.app.ui.notify('error', app.lang.tr('recovery_email_could_not_be_sent_try_later'));
                    }
                    app.ui.toggleLoader(false);

                    if(callback != undefined) {
                        callback();
                    }
                });
        }

        $(document).on('click', '#participant_already_exists_edition_disabled:visible [data-action="send_token_email"]', function(e) {
            e.preventDefault();

            $('#participant_already_exists_edition_disabled').foundation('close');

            sendLink();
        });

        $(document).on('click', '[data-action="send_me_a_recovery_email"]', function(e) {
            e.preventDefault();

            let $replierForm = that.replyComponent.find('.replier_info');
            let email = $replierForm.find('input[name="email"]').val().trim();

            $replierForm.find('.error_hint').remove();

            if(!app.utilities.isValidEmailAddress(email)) {
                let error = app.lang.tr('please_fill_in_the_email_so_that_we_can_send_recovery_email').out();
                $('<div class="error_hint required_for_recovery_email" />')
                    .insertAfter($('input[name="email"]').parents('label'))
                    .text(error);

                return;
            }
            let $input = $(this);

            sendLink(function() {
                let $parent = $input.parent();
                $parent.html('<span class="callout small primary">' + app.lang.tr('recovery_email_has_been_sent').out()
                    + '<i class="fa fa-info-circle" data-tooltip title="' + app.lang.tr('recovery_email_has_been_sent_explanation').out() + '"></i></span>');
                new Foundation.Tooltip($parent.find('[data-tooltip]'));
            });
        });

        this.replyComponent.find('input[name="email"]').on('change blur', function() {
            that.replyComponent.find('.replier_info').find('.error_hint.required_for_recovery_email').remove();
        });


        /**
         * Calendar filtering
         */
        app.userManager.initCalendarFilter();
        app.event.on('calendar_filter_changed', function() {
            that.applyCalendarFilters();
        });

        $(window).on('conflicts_loaded', function() {
            //Moving calendar-info on small screen
            $('.label-for-small.show-for-small-only:visible').each(function () {
                if($(this).find('.calendar-info').length > 0)
                    return;

                let $calendarInfo = $(this).parents('.answer-table').find('th[data-proposition-id="' + $(this).parent().attr('data-proposition-id') + '"].has-conflict');
                if($calendarInfo.length) {
                    $(this).addClass('has-conflict')
                        .append($calendarInfo.find('.calendar-info'))
                        .attr('title', $calendarInfo.attr('title-backup'))
                        .attr('data-tooltip', true);
                    new Foundation.Tooltip($(this));
                }
            });
        });

    }

    checkForPending() {
        if(!app.utilities.isLocalStorageAvailable() || app.session.user === null) {
            return;
        }
        let surveyId = this.replyComponent.attr('data-survey-id');
        let pendingAnswers =  JSON.parse(localStorage.getItem('pending_answers'));

        if(surveyId.length > 0 && pendingAnswers) {
            if(pendingAnswers.survey_id !== surveyId) {
                localStorage.removeItem('pending_answers');
                return;
            }

            let answers = pendingAnswers.answers;

            if(answers[0].participant !== undefined) {
                answers[0].participant = {};
            }

            //Propagating new answers id
            for(let i = 0; i < answers.length; i++) {
                for(let j = 0; j < answers[i].choices.length; j++) {
                    this.setValue(answers[i].choices[j].proposition_id, answers[i].choices[j].value);
                }

                if(answers[i].comment !== undefined && answers[i].comment.length > 0) {
                    let $question = this.replyComponent
                        .find('[data-form="question-form-reply"][data-question-id="' + answers[i].question_id + '"]');

                    $question.parents('.survey-question').find('input[name="leave_a_comment"]').prop('checked', true).trigger('change');
                    $question.parents('.survey-question').find('[name="comment"]').val(answers[i].comment);
                }
            }

            this.replyComponent.find('[data-action="send_answers"]').attr('data-sub-action', 'update').trigger('click');

            localStorage.removeItem('pending_answers');
        }
    }

    setValue(proposition_id, value) {
        let $inputs = this.replyComponent.find('input[name="proposition_' + proposition_id + '"], input[name^="proposition"][type="radio"][value="' + proposition_id + '"]');

        if($inputs.length > 0) {
            $inputs.each(function() {
                if($(this).attr('type') === 'radio'
                    && ($(this).val() === value
                        || ($(this).val() === proposition_id && 'selected_value_yes' === value))) {
                    $(this).prop("checked", true);
                }

                if($(this).attr('type') === 'hidden') {
                    $(this).val(value);
                }

                $(this).trigger('change');
            })
        }
    }

    showConflicts(conflicts) {
        let getConflictTip = function(conflict) {
            let tip = '';

            let ds = window.app.ui.timeLocalizer().localize('date', conflict.start_ts);
            let ts = window.app.ui.timeLocalizer().localize('time', conflict.start_ts);
            let de = window.app.ui.timeLocalizer().localize('date', conflict.end_ts);
            let te = window.app.ui.timeLocalizer().localize('time', conflict.end_ts);

            tip += '<span class="event-dates">';

            if(conflict.allDay !== undefined && conflict.allDay) {
                de = moment.unix(conflict.end_ts - 1)
                    .utc()
                    .format(app.lang.translate('moment_date_format').text);

                if (de === ds) {
                    tip += ds + ' ' + app.lang.tr('all_day');
                } else {
                    tip += ds + ' ' + app.lang.tr('to_day') + ' ' + de;
                }
            } else {
                if(de === ds) {
                    tip += ds + ' ' + app.lang.tr('from_hour').text.toLowerCase() + ' ' + ts;
                    if(ts !== te) {
                        tip += ' ' + app.lang.tr('to_hour') + ' ' + te;
                    }
                } else {
                    tip += ds + ' ' + ts + ' ' + app.lang.tr('to_day') + ' ' + de + ' ' + te;
                }
            }
            tip += '</span>';

            if (conflict.title !== undefined) {
                tip += ' ' + conflict.title;
            }

            return tip;
        }

        let showCalendarName = function(calendar) {
            let color = '';
            if(calendar.settings.color !== undefined) {
                color = '<span class="calendar-color fa fa-square fa-lg" style="color:' + calendar.settings.color + '"></span>';
            }
            return '<li class="calendar-name" data-calendar-id="'+calendar.id+'">' + color + calendar.name +'</li>';
        }

        let showConflictsTipsForProposition = function(conflicts, propositionId) {
            if (conflicts.length > 0) {
                let tip = '<div>' +  ((conflicts.length > 1)?app.lang.tr('conflicts'):app.lang.tr('conflict')) + ' :</div>';
                tip += '<ul class="events-list">';
                let currentCalendarId = -1;
                let calendarsConflicting = [];
                for (let i in conflicts) {
                    let classes = '';
                    //Show calendar name if it's different from previous
                    if(conflicts[i].calendar.id !== currentCalendarId && conflicts[i].calendar.name != null) {
                        tip += showCalendarName(conflicts[i].calendar);
                        currentCalendarId = conflicts[i].calendar.id;
                        calendarsConflicting.push(conflicts[i].calendar.id + '');
                    }

                    tip += '<li class="' + classes + '" data-calendar-id="'+conflicts[i].calendar.id+'">' + getConflictTip(conflicts[i]) +'</li>';
                }
                tip += '</ul>';

                let propositionHeader = $('th[data-proposition-id="' + propositionId + '"]');
                if (propositionHeader.length > 0) {
                    let calendarInfo = propositionHeader.find('.calendar-info');
                    if (calendarInfo.length === 0) {
                        calendarInfo = $('<span class="calendar-info fa fa-exclamation-triangle">').appendTo(propositionHeader);
                        calendarInfo.attr('data-proposition-id-ref', propositionId);
                    }

                    //Setting tip
                    propositionHeader.attr('title', tip);
                    propositionHeader.attr('title-backup', tip);
                    new Foundation.Tooltip(propositionHeader, {clickOpen: true});
                    propositionHeader.addClass('has-conflict');
                    propositionHeader.data('conflicting-calendars', calendarsConflicting);
                }
            }
        }
        //Clearing old display
        $('th[data-proposition-id].has-conflict .calendar-info').remove();
        $('th[data-proposition-id].has-conflict').each(function() {
            if($(this).attr('data-tooltip') !== undefined) {
                $(this).foundation('destroy');
            }
            $(this).attr('title','');
            $(this).removeClass('has-conflict');
        });

        for(let questionId in conflicts) {
            for (let propositionId in conflicts[questionId]) {
                showConflictsTipsForProposition(conflicts[questionId][propositionId], propositionId);
            }
        }
    }

    applyCalendarFilters() {
        var excludedCalendarIds = app.userManager.getExcludedCalendarIds();
        var filteredConflicts =  [];
        for (var questionId in this.conflicts) {
            for (var propositionId in this.conflicts[questionId]) {
                for(var i in this.conflicts[questionId][propositionId]) {
                    if (excludedCalendarIds.indexOf(this.conflicts[questionId][propositionId][i].calendar.id + '') < 0) {
                        if(!(questionId in filteredConflicts))
                            filteredConflicts[questionId] = [];
                        if(!(propositionId in filteredConflicts[questionId]))
                            filteredConflicts[questionId][propositionId] = [];

                        filteredConflicts[questionId][propositionId].push(this.conflicts[questionId][propositionId][i]);
                    }
                }
            }
        }
        this.showConflicts(filteredConflicts);
    };

    loadAllConflicts() {
        let that = this;
        var surveyId = this.replyComponent.attr('data-survey-id');

        //We load conflicts only when user is authenticated and survey has at least one date question
        if(app.session.user !== null && surveyId.length > 0 && this.replyComponent.node.is('[data-survey-has-date-questions]')) {
            if(this.conflicts !== undefined) {
                app.ui.toggleLoader(false);
                this.showConflicts(this.conflicts);
                return;
            }

            app.client.get('/CalendarConflict/' + surveyId,
                function(data) {
                    that.conflicts = data;
                    that.showConflicts(data);
                    //Enabled toggle conflicts
                    that.replyComponent.find('.toggle_conflicts .disabled, .toggle_conflicts input[disabled]').removeClass('disabled').removeAttr('disabled');
                    window.app.ui.notify('success', app.lang.tr('conflicts_are_now_loaded'));
                    $(window).trigger('conflicts_loaded');
                }, {
                    error : function() {
                        window.app.ui.notify('error', app.lang.tr('conflicts_loading_failed'));
                    }

                }
            );
        }
    }
}

app.event.on('started', function() {
    let replyComponent = $('[data-component="survey-reply"]');
    if(!replyComponent.length) return;

    if(app.ui.controller === undefined)
        app.ui.controller = {};

    app.ui.controller.replyController = new ReplyController(replyComponent);
});
