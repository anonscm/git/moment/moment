
class SurveyQuestionsController {

    constructor(wizard) {
        this.wizard = wizard;
        this.stepQuestions = wizard.find('section[data-step="questions"]');
        this.questionForm = wizard.find('.empty-form-container [data-form="question"]');

        this.localeDateFormat = app.lang.translate('moment_date_format').text;
        this.localeTimeFormat = app.lang.translate('moment_time_format').text;
        this.localeDateTimeFormat = app.lang.translate('moment_datetime_format').text;
        this.localeDayOfWeekStart = moment().localeData().firstDayOfWeek();

        this.momentsOfDay = app.config.hasOwnProperty('moments_of_day')?app.config.moments_of_day:{};

        // Adapting moment of the day in thee original time zone to current time zone
        let currentTz = moment.tz.guess();
        if(wizard.settings.time_zone !== currentTz) {
            for (let mmt in this.momentsOfDay) {
                this.momentsOfDay[mmt].bt = moment.tz(this.momentsOfDay[mmt].bt, this.localeTimeFormat, wizard.settings.time_zone).tz(currentTz).format(this.localeTimeFormat);
                this.momentsOfDay[mmt].et = moment.tz(this.momentsOfDay[mmt].et, this.localeTimeFormat, wizard.settings.time_zone).tz(currentTz).format(this.localeTimeFormat);
            }
        }

        this.questions = [];

        this.validator = new QuestionValidator(this);

        //Some tips may need to be initialized after load
        this.stepQuestions
            .find('ul.questions_list [title][title!=""]:not([data-tooltip])[data-make-tooltip]')
            .removeAttr('data-make-tooltip').attr('data-tooltip','').foundation();

        this.initBindings();

        let that = this;

        //Allow sorting for propositions and question
        this.wizard.find('ul.propositions').sortable({
            items: "li.proposition",
            stop : function(event, ui) {
                that.updatePropositionsList(this);
            }
        });

        this.stepQuestions.find('ul.questions_list').sortable();

        /**
         * Binding when something has changed in the survey
         */
        app.ui.survey.binder.on('change', function() { that.updateStatus(); });


        // return this;
    }

    /**
     * Add a question of required type
     * @param type
     */
    addQuestion(type, options) {

        //Clearing error classes
        this.stepQuestions.find('form').foundation('removeErrorClasses', this.questionForm.find('input'));

        //Hiding 'no question found'
        this.stepQuestions.find('.no-question-found').addClass('hidden');

        //Cloning form
        let newQuestionForm = this.questionForm.clone();

        let newDatePropositionForm = newQuestionForm.find('[data-form-proposition="date"]');
        let newTextPropositionForm = newQuestionForm.find('[data-form-proposition="text"]');
        let propositionForms = newQuestionForm.find('[data-form-proposition]');

        this.stepQuestions.find('ul.questions_list').append(newQuestionForm);

        //Changing type of form (either text or date)
        newQuestionForm.attr('data-type', type);
        newQuestionForm.show();

        //Computing index
        var index = this.stepQuestions.find('[data-form]').length - 2;
        newQuestionForm.attr('data-index', index);

        //Setting a new id for switches
        newQuestionForm.find('input[type="checkbox"]').each(function() {
            if($(this).attr('id') !== undefined &&  $(this).attr('id').startsWith('switch')) {
                var newId = $(this).attr('id') + index;
                $(this).attr('id', newId);
                $(this).parent().find('label').attr('for', newId);
            }
        });

        propositionForms.hide();

        propositionForms.find('li.proposition:first').find('input').removeAttr('data-abide-ignore');

        // Creating tooltips for newly shown question
        newQuestionForm.find('[title][title!=""]:not([data-tooltip])[data-make-tooltip]').removeAttr('data-make-tooltip').attr('data-tooltip','');

        newQuestionForm.find('form').foundation(/*'_reflow'*/);

        newQuestionForm.uniqueId();

        //Displaying the required proposition form
        switch(type) {
            case 'date' :
                newTextPropositionForm.remove();
                newDatePropositionForm.show();
                newQuestionForm.find('span.question-type').addClass('fa-calendar-o');

                newDatePropositionForm.find('.propositions_container').hide();
                newDatePropositionForm.find('.no-proposition-found').show();
                // newDatePropositionForm.find('li:first').find('input').uniqueId();
                if(options !== undefined && options.slot !== undefined && options.slot === true) {
                    //Remove "first" date hour proposition template
                    newDatePropositionForm.find('.same_for_all_row').remove();
                    newDatePropositionForm.find('li.proposition:first').remove();
                    this.initDateRangePickr(newQuestionForm)
                } else {
                    this.initMultiDatePickr(newQuestionForm);
                }

                break;
            case 'text' :
                newDatePropositionForm.remove();
                newTextPropositionForm.show();
                newQuestionForm.find('span.question-type').addClass('fa-file-text-o');

                newTextPropositionForm.find('li:first').find('input').uniqueId();
                break;
        }

        this.stepQuestions.trigger('step_question_change');

    }

    /**
     * Delete the question corresponding to the $button
     * @param $button
     */
    deleteQuestion($button) {
        //Only a delete action can be effective
        if($button.attr('data-action') === 'delete_question') {

            let question = $button.parents('li');
            let questionTitle = question.find('[name$=".title"]').val();
            let message = app.lang.tr('confirm_question_deletion') + ' ?';
            if(questionTitle.trim() !== '') {
                message = app.lang.tr('confirm_question_deletion') + ' : ' + questionTitle + ' ?';
            }

            let that = this;

            app.ui.popup.confirm(message, function() {
                question.remove();

                //updating model
                that.sendQuestion();
            });
        }
    };

    /**
     * Adds a date proposition to the $question form
     * @param $question
     */
    addDateProposition($question, options) {

        let propositionDateLI = $(this.questionForm.find('[data-form-proposition="date"] li.proposition')[0].outerHTML)
            .appendTo($question.find('ul.propositions'));
        propositionDateLI.find('input').uniqueId();
        //clearing abide validation class
        propositionDateLI.find('input').removeClass('is-invalid-input');

        //Updating time form with previous look and feel
        propositionDateLI.find('[name="whole_day"]').prop('checked', propositionDateLI.prev('li.proposition').find('[name="whole_day"]').is(':checked'));
        propositionDateLI.find('[name="slot"]').prop('checked', 'checked');

        //If options are forced
        if (options !== undefined) {
            if (options.wholeDay !== undefined) {
                propositionDateLI.find('[name="whole_day"]').prop('checked', options.wholeDay);
                if (options.wholeDay && options.slot && options.endDay) {
                    propositionDateLI.find('[name="end_date"]').val(options.endDay);
                }
            }
            if (options.slot !== undefined) {
                propositionDateLI.find('[name="slot"]').prop('checked', options.slot);
            }

            if (options.baseDay !== undefined) {
                propositionDateLI.find('[name="base_date"]').val(options.baseDay);
            }
        }
        if ($question.hasClass('day_slots_question')) {
            propositionDateLI.find('label').addClass('day_slot_label label secondary');
        } else {
            if ($question.find('[name="same_for_all"]').is(':checked')) {
                let firstPropositionDateLi = $question.find('ul.propositions li.proposition_date_input:first');
                this.reproduceTimes(firstPropositionDateLi, propositionDateLI);

                this.updateHoursLabels($question);
            } else {
                propositionDateLI.find('.hours_container').show();
            }
        }

        $(this).trigger('proposition_added', propositionDateLI);

        this.updatePropositionsList(propositionDateLI.parents('ul.propositions'));

        propositionDateLI.closest('[data-form-proposition="date"]').find('.no-proposition-found').hide();
        propositionDateLI.closest('[data-form-proposition="date"]').find('.timezone-label').css('display','inline-block');
        propositionDateLI.parents('.propositions_container').show();

        propositionDateLI.parents('form').foundation('_reflow');
    }

    /**
     * Removes a Time picker
     * @param e
     * @param $button
     */
    removeTimePicker(e, $button) {
        e.stopPropagation();
        e.preventDefault();

        let $questionForm = $button.parents('[data-form="question"]');

        if($questionForm.find('[name="same_for_all"]').is(':checked')) {
            let index = $button.parents('.time_pickers').find('.time_picker_container').index($button.parents('.time_picker_container'));
            $questionForm.find('li.proposition:not(:first)').each(function () {
                $($(this).find('.time_picker_container').get(index)).remove();
            });
        }


        //We remover the time picker container (WAS hiding)
        $button.parent().remove();

        //If no time or end date picker is shown, then its a whole day
        if($button.parents('li.proposition').find('.time_container:visible, .date_container:visible').length === 0) {
            $button.parents('li.proposition').find('[name="whole_day"]').prop('checked', true);
            $button.parents('li.proposition').find('[name="slot"]').prop('checked', false);
        }

        this.updateHoursLabels($questionForm);
    };

    /**
     * Adds a Time picker
     * @param dinput
     * @param value
     */
    addTimePicker(dinput, value) {
        let propositionTimePicker
            = this.questionForm.find('[data-form-proposition="date"] li.proposition .time_picker_container')[0].outerHTML;

        let $targets = $(dinput);

        if(dinput.parents('[data-form="question"]').find('[name="same_for_all"]').is(':checked')) {
            $targets = dinput.parents('.propositions').find('.proposition_date_input');
        }

        let timePickerContainer = $(propositionTimePicker).appendTo($targets.find('.time_pickers')).uniqueId();

        let that = this;
        //If base_time is set, we preset new base_time to current + 1 hour (same for endtime)
        timePickerContainer.each(function() {
            that.presetTimePicker($(this))
        });

        return timePickerContainer;
    };

    /**
     * Initialize a Time picker
     * @param $input
     */
    initTimePicker($input) {

        if($input.hasClass('has_time_picker'))
            return;

        let minput = $input.closest('.time_picker_container');

        let tmpLocaleTimeFormat = this.localeTimeFormat;

        $input.datetimepicker({
            datepicker: false,
            format: this.localeTimeFormat,
            formatTime: this.localeTimeFormat,
            onShow: function () {
                if($input.attr('name') === 'end_time') {
                    let baseTime = minput.find('input[name="base_time"]').val();

                    let minTime = false;
                    if(baseTime.length > 0) {
                        //Min endtime is base time + X hour
                        minTime = moment(baseTime, tmpLocaleTimeFormat).add(1, 'minute').format('HH:mm');
                    }
                    this.setOptions({
                        minTime:minTime
                    });
                }
            },
            onChangeDateTime: function () {
                if($input.attr('name') === 'base_time') {

                    let baseTime = $input.val();
                    let $endTimeInput = minput.find('input[name="end_time"]');
                    $endTimeInput.removeClass('is-invalid-input');
                    if ($endTimeInput.val().length === 0) {
                        let defaultTime = false;
                        if(baseTime.length > 0) {
                            defaultTime = window.app.utilities.getNextHour(baseTime, tmpLocaleTimeFormat);
                        }

                        if($endTimeInput.hasClass('has_time_picker')) {
                            //Min endtime is base time + X hour
                            $endTimeInput.datetimepicker('setOptions', {
                                defaultTime: defaultTime
                            });
                        } else {
                            $endTimeInput.attr('data-default-time', defaultTime);
                        }
                    }
                }
            }
        });

        if($input.attr('data-min-time') !== undefined) {
            $input.datetimepicker('setOptions', {minTime:$input.attr('data-min-time')});
        }
        if($input.attr('data-default-time') !== undefined) {
            $input.datetimepicker('setOptions', {defaultTime:$input.attr('data-default-time')});
            $input.removeAttr('data-default-time');
        }

        $input.addClass('has_time_picker');
        $input.datetimepicker('show');
    }

    /**
     * Preset time picker with "next hour" form previus if exists
     * @param $input
     */
    presetTimePicker($timePicker) {

        let previousTimePicker = $timePicker.prev();
        if(previousTimePicker.length === 0) {
            return;
        }

        let timeFormat = app.lang.translate('moment_time_format').text;
        let previousBaseTime = previousTimePicker.find('input[name="base_time"]').val();
        let previousBaseMoment = moment(previousBaseTime, timeFormat);
        let previousEndTime = previousTimePicker.find('input[name="end_time"]').val();
        let previousEndMoment = moment(previousEndTime, timeFormat);
        let maxBaseTime = previousBaseMoment.clone().endOf('day').add(-1, 'minutes');

        if (previousBaseTime.length > 0) {
            let newBaseMoment = window.app.utilities.getMomentNextDuration(previousBaseTime, timeFormat, 1, 'hour');
            if (previousEndTime.length > 0) {
                newBaseMoment = previousEndMoment;
                let duration = previousEndMoment.diff(previousBaseMoment, 'minutes');

                let newEndMoment = window.app.utilities.getMomentNextDuration(previousEndTime, timeFormat, duration, 'minutes');
                if (newEndMoment.isAfter(newBaseMoment)) {
                    $timePicker.find('input[name="end_time"]')
                        .val(newEndMoment.format(timeFormat));
                }
            }

            if(newBaseMoment.isAfter(previousBaseMoment) && newBaseMoment.isBefore(maxBaseTime)) {
                $timePicker.find('input[name="base_time"]')
                    .val(newBaseMoment.format(timeFormat));

                this.updateHoursLabelsOfPropositionLi($timePicker.parents('li.proposition'));
            }
        }
    }

    /**
     * Initialize a date picket for $dateInput
     * @param $dateInput
     */
    initDatePicker($dateInput) {

        //If date_picker is already initialized we stop here
        if ($dateInput.hasClass('has_date_picker')) {
            //If we use flatpickr we should open it (because it only opens on focus, if already have focus it wouldn't open
            if($dateInput[0]._flatpickr !== undefined) {
                $dateInput[0]._flatpickr.open();
            }
            return;
        }

        //If we are initilizing end_date, min should be base_date + 1 day
        //There is no base_date min
        let minDate = null;
        if ($dateInput.attr('name') === 'end_date'
            && $dateInput.parents('li.proposition_date_input').find('input[name="base_date"]').val() !== '') {
            let minDateMoment = moment($dateInput.parents('li.proposition_date_input').find('input[name="base_date"]').val(), this.localeDateFormat);

            minDate = minDateMoment.toDate();
            minDate.setDate(minDate.getDate() + 1);
        }

        let $currentDateLi = $dateInput.parents('.proposition_date_input');

        let mode = 'single';
        if(typeof app.config.datepicker !== "undefined" && app.config.datepicker.allowMultiple) {
            mode = ($dateInput.attr('name') === 'base_date' && $dateInput.val() === '') ? 'multiple' : 'single';

            if (mode === 'multiple' && $currentDateLi.find('[name="whole_day"]').is(':checked')
                && $currentDateLi.find('[name="slot"]').is(':checked')) {
                mode = 'range';
            }
        }
        //At this moment we are not jumping to a required date
        this.stepQuestions.jumping = false;

        /**
         * Handling MonthChange and YearChange of flatpickr
         * @param selectedDates
         * @param dateStr
         * @param flatpickrItem
         */
        let that = this;

        $dateInput.flatpickr({
            mode: mode,
            minDate: minDate,
            allowInput : true,
            onChange: function (currentDateTimes, value, flatpickrItem) {
                let $input = $(flatpickrItem.element);
                if (currentDateTimes.length !== null && currentDateTimes.length === 1 && $input.attr('name') === 'base_date') {

                    //We store last base_date selected
                    that.stepQuestions.datePickerPosition = currentDateTimes[0];

                    // end_date min is always base_date + 1 day
                    if ($input.parents('.proposition_date_input').find('input[name="end_date"]').hasClass('has_date_picker')) {
                        let minEndDate = currentDateTimes[0];
                        minEndDate.setDate(minEndDate.getDate() + 1);
                        $input.parents('.proposition_date_input').find('input[name="end_date"]')[0]._flatpickr.set('minDate', minEndDate);
                    }
                }
            },
            onClose: function(selectedDates, dateStr, flatpickrItem) {

                if(selectedDates.length === 0)
                    return;

                if(flatpickrItem.config.mode === "multiple") {
                    let $input = $(flatpickrItem.element);

                    for (let i = 1; i < selectedDates.length; i++) {
                        let baseDay = moment(selectedDates[i]);
                        if (baseDay.isValid()) {
                            let currentQuestion = $input.parents('[data-form="question"]');
                            currentQuestion.find('[data-action="add_date_question_proposition"]').trigger('add-date');
                            currentQuestion.find('li.proposition:last input[name="base_date"]').val(baseDay.format(that.localeDateFormat)).trigger('change');
                        }
                    }
                    //Destroying multiple date picker
                    flatpickrItem.destroy();
                    $input.removeClass('has_date_picker');
                    $input.val(moment(selectedDates[0]).format(that.localeDateFormat));
                }

                if(flatpickrItem.config.mode === "range") {
                    let $input = $(flatpickrItem.element);

                    let $endDateInput = $input.parents('li.proposition').find('input[name="end_date"]');

                    //Destroying range date picker
                    flatpickrItem.destroy();
                    $input.removeClass('has_date_picker');
                    $input.val(moment(selectedDates[0]).format(that.localeDateFormat));

                    if($endDateInput.hasClass('has_date_picker')) {
                        $endDateInput.get(0)._flatpickr.setDate(selectedDates[1]);
                    } else {
                        $endDateInput.val(moment(selectedDates[1]).format(that.localeDateFormat));
                    }
                }
            },
            onReady: function(selectedDates, dateStr, flatpickrItem) {

                //Trick to avoid from refreshing Unavailability three times (jumpToDate triggers onMonthChange and on onYearChange)
                that.stepQuestions.jumping = true;
                flatpickrItem.jumpToDate(that.stepQuestions.datePickerPosition, true);
                that.stepQuestions.jumping = false;

                if(flatpickrItem.config.mode === "multiple") {
                    let $validButton = $('<span class="wizard_action button">' + app.lang.tr('validate') + '</span>');
                    $validButton.on('click', function () {
                        flatpickrItem.close();
                    });

                    $validButton.appendTo(flatpickrItem.calendarContainer);
                }
            }
        }).open();

        $dateInput.addClass('has_date_picker');
    }

    initMultiDatePickr($question) {
        let that = this;

        $question.find('.date-picker-range-container').remove();

        $question.find('.date-picker-multi-container').show();
        $question.find('.need_hours_label span').text(app.lang.tr('your_date_propositions'));
        $question.find('.no-proposition-found').html(app.lang.tr('no_date_selected').out());
        $question.find('[name="same_for_all"]').parent().show();

        $question.find('.date-picker-multi').each(function() {
            let elem = $(this);
            let currentQuestion = elem.parents('[data-form="question"]');
            $(this).multipickr('multi')
                .on('multipickr_date_added', function (e, date) {
                    that.addDateProposition(currentQuestion, {baseDay: date});
                })
                .on('multipickr_date_deleted', function (e, date) {
                    that.deleteDateProposition(currentQuestion, date);
                });
        });

    }

    initDateRangePickr($question) {
        let that = this;

        $question.find('.date-picker-multi-container').remove();
        $question.find('[name="same_for_all"]').parent().remove();

        $question.find('.date-picker-range-container').show();
        $question.find('.your_propositions span').text(app.lang.tr('your_date_range_propositions'));
        app.ui.updateTooltip($question.find('.your_propositions i[data-tooltip]'),app.lang.tr('your_date_range_propositions_info'));
        $question.find('.no-proposition-found').html(app.lang.tr('no_date_slot_selected').out());
        $question.find('.timezone-label').remove();

        $question.find('.date-picker-range').multipickr('range').on('multipickr_range_added', function (e, range) {
            let currentQuestion = $(this).parents('[data-form="question"]');
            range.wholeDay = true;
            range.slot = true;
            that.addDateProposition(currentQuestion, range);
        });

        $question.addClass('day_slots_question');
    }

    /**
     * Initialize the checkbox same_for_all value for the $question
     *
     * @param $question
     */
    initSameHoursForAll($question) {
        let hours = '';
        let same_hours = true;
        $question.find('.proposition_date_input').each(function() {
            let tmp = '';
            $(this).find('[name$="_time"]').each(function() {
                tmp += $(this).val();
            });
            if(hours !== '' && tmp !== hours) {
                //not all the same hours
                same_hours = false;
            } else {
                hours = tmp;
            }
        });

        $question.find('[name="same_for_all"]').prop('checked', same_hours).trigger('change');
    }

    /**
     * Initialize the input mode the $question
     *
     * @param $question
     */
    initInputMode($question) {
        let that = this;
        let $inputMode = $question.find('[name="input_mode"]');

        if($inputMode.val() === 'moments') {
            let areMomentsOfDay = true;
            $question.find('.time_picker_container').each(function() {
                if(!areMomentsOfDay)
                    return false;

                let bt = $(this).find('[name="base_time"]').val();
                let et = $(this).find('[name="end_time"]').val();
                let $currentDateLi = $(this).parents('.proposition_date_input');
                let isMomentOfDay = false;
                for(let mmt in that.momentsOfDay) {
                    if(bt === that.momentsOfDay[mmt].bt && et === that.momentsOfDay[mmt].et) {
                        $currentDateLi.find('input[name="' + mmt + '"]').prop('checked', true);
                        $(this).addClass('moments ' + mmt);
                        isMomentOfDay = true;
                    }
                }
                // Current proposition is not a moment of day
                if(!isMomentOfDay) {
                    areMomentsOfDay = false;
                }
            });
            //Some proposition does not match a moment, we toggle "hours" mode
            if(!areMomentsOfDay)
                $inputMode.val('hours');

            this.updateHoursLabels($question);
        }

        $inputMode.trigger('change');
    }

    /**
     * Add a text proposition to the question
     * @param $question
     */
    addTextProposition($question) {

        let propositionTextLi = $(this.questionForm.find('[data-form-proposition="text"] li.proposition')[0].outerHTML);

        propositionTextLi.appendTo($question.find('ul.propositions'))
            .find('input').uniqueId().removeClass('is-invalid-input');

        $question.find('.propositions_container').show();

        if ($question.find('[name="same_variants_for_all"]').is(':checked')) {
            let firstPropositionLi = $question.find('ul.propositions li.proposition:first');
            this.reproduceVariants(firstPropositionLi, propositionTextLi);
            this.updateVariantsLabels($question);
        }

        this.updatePropositionsList(propositionTextLi.parents('ul.propositions'));

        propositionTextLi.closest('[data-form-proposition="text"]').find('.no-proposition-found').hide();
        propositionTextLi.parents('form').foundation('_reflow');
    }

    /**
     * Add a text variant for a text proposition
     * @param dinput
     * @param value
     */
    addPropositionVariant(dinput, value) {
        let $targets = $(dinput);

        if(dinput.parents('[data-form="question"]').find('[name="same_variants_for_all"]').is(':checked')) {
            $targets = dinput.parents('.propositions').find('.proposition');
        }

        let $variantInputs = $(this.questionForm.find('[data-form-proposition="text"] li.proposition input[name^="proposition_variant"]')[0].outerHTML)
            .appendTo($targets).uniqueId();

        if(value !== undefined) {
            $variantInputs.each(function() {
                $(this).val(value);
            })
        }
    }

    reproduceVariants($currentLi, $targets) {
        let that = this;

        //Getting all proposition_variant to be replicated
        let currentVariantInputs = $currentLi.find('[name="proposition_variant"]').toArray();

        if($targets === undefined)
            $targets = $currentLi.siblings('.proposition');

        //For all other date proposition
        $targets.each(function() {

            let inputsAlreadyAdded = [];
            let $currentSibling = $(this);

            // Each existing variant must have an id
            $currentSibling.find('[name="proposition_variant"][data-proposition-id][data-proposition-id!=""]').each(function() {
                var otherVariantInput = $(this);
                $(currentVariantInputs).each(function () {
                    if(otherVariantInput.val() === $(this).val() && !$(this).attr('data-copied') && !otherVariantInput.attr('data-to-keep')) {
                        otherVariantInput.attr('data-to-keep', true);
                        inputsAlreadyAdded.push(this);
                        $(this).attr('data-copied', true);
                    }
                });
            });

            //Removing useless proposition_variant
            $currentSibling.find('[name="proposition_variant"]:not([data-to-keep])').remove();

            //Each input not already added will be added
            $(currentVariantInputs).not(inputsAlreadyAdded).each(function() {
                let $newVariant = $(this).clone().uniqueId();

                //Cleaning business data
                $newVariant.removeAttr('data-proposition-id');
                $currentSibling.append($newVariant);
            });
            //Removing to-keep tag
            $currentSibling.find('[name="proposition_variant"][data-to-keep]').removeAttr('data-to-keep');

            //Remove copied tag on current inputs
            $(currentVariantInputs).removeAttr('data-copied');
        });
    }

    /**
     * Initialize the checkbox same_variants_for_all value for the $question
     *
     * @param $question
     */
    initSameVariantsForAll($question) {
        let variants = '';
        let same_variants = false;

        if($question.find('.proposition').length > 1) {

            same_variants = true;
            $question.find('.proposition').each(function () {
                let tmp = '';
                $(this).find('[name="proposition_variant"]').each(function () {
                    tmp += $(this).val();
                });
                if (variants !== '' && tmp !== variants) {
                    //not all the same variants
                    same_variants = false;
                } else {
                    variants = tmp;
                }
            });
        }
        $question.find('[name="same_variants_for_all"]').prop('checked', same_variants).trigger('change');
    }

    /**
     * Delete a proposition
     * @param $button
     */
    deleteProposition($button) {
        let currentForm = $button.parents('[data-form="question"]');

        let date = $button.closest('li').find('[name="base_date"]').val();
        $button.closest('li').remove();

        if(currentForm.length > 0) {

            //Remove this date from multipickr
            currentForm.find('.date-picker-multi').each(function() {
                this._multipickr.deleteDate(date);
            });

            //Re-sorting propositions
            this.updatePropositionsList(currentForm.find('ul.propositions'));

            if(currentForm.find('ul.propositions li.proposition').length === 0) {
                currentForm.find('.no-proposition-found').show();
                currentForm.find('.timezone-label').hide();
                currentForm.find('.propositions_container').hide();
            }
        }

        //updating model
        this.sendQuestion();
    };

    /**
     * Delete a proposition
     * @param $button
     */
    deleteDateProposition($question, date) {
        let $button = $question
            .find('input[name="base_date"]')
            .filter(function(){return this.value===date}).closest('li').find('[data-action="delete_proposition"]');
        this.deleteProposition($button);
    }

    /**
     * Updates a proposition list
     * @param list
     */
    updatePropositionsList(list) {
        list = $(list);
        let that = this;
        let propositions = list.find('li.proposition');

        for(var i = 0; i < propositions.length; i++) {
            //Updating label
            $(propositions[i]).attr('data-index', i);

            var proposition_label =
                ($(propositions[i]).parents('[data-form-proposition]').attr('data-form-proposition') === 'text')?
                    'proposition':'date';
            proposition_label = app.lang.translate(proposition_label) + ' ' + (i + 1);

            if($(propositions[i]).find('[name="base_date"]').val() !== undefined && $(propositions[i]).find('[name="base_date"]').val().length) {
                proposition_label = $(propositions[i]).find('[name="base_date"]').val();
                if($(propositions[i]).find('[name="end_date"]').val() !== undefined && $(propositions[i]).find('[name="end_date"]').val().length) {
                    proposition_label += ' ' + app.lang.tr('to_day') + ' '  + $(propositions[i]).find('[name="end_date"]').val();

                    let baseDay = moment($(propositions[i]).find('[name="base_date"]').val(), that.localeDateFormat);
                    let endDay = moment($(propositions[i]).find('[name="end_date"]').val(), that.localeDateFormat);
                    if(baseDay.isValid() && endDay.isValid()) {
                        let diff = endDay.diff(baseDay, 'days');
                        if(!isNaN(diff)) {
                            proposition_label += '<span class="day_slot_duration"> (' + (diff + 1) + ' ' + app.lang.tr('days') + ')</span>';

                            //Styling slots has other day_slots
                            if(!$(propositions[i]).find('label[for^="proposition_"]').hasClass('day_slot_label')) {
                                $(propositions[i]).find('label[for^="proposition_"]').addClass('day_slot_label secondary label');
                            }
                            //Removing/hiding useless inputs/controls for a day_slot
                            $(propositions[i]).find('.proposition_controls, .time_container, .hours_labels, .hours_container [data-action="remove_time_picker"]').remove();
                            $(propositions[i]).find('.hours_container').hide();
                        }
                    }
                }
            }

            $(propositions[i]).find('label[for^="proposition_"]').attr('for', 'proposition_' + (i + 1));
            $(propositions[i]).find('label[for^="proposition_"]').html(proposition_label);
        }

        let $questionForm = list.parents('[data-form="question"]');
        if($questionForm.attr('data-type') === 'text') {
            that.updateVariantsLabels($questionForm);
        }

        list.sortable({
            items: "li.proposition",
            stop : function(event, ui) {
                that.updatePropositionsList(this);
            }
        });
    };

    reproduceTimes($currentDateLi, $otherDateLis) {
        let that = this;

        //Getting all picker container to be replicated
        let inputs = $currentDateLi.find('.time_picker_container').toArray();

        if($otherDateLis === undefined)
            $otherDateLis = $currentDateLi.siblings();

        //For all other date proposition
        $otherDateLis.each(function() {

            let inputsAlreadyAdded = [];
            let otherDateSibling = $(this);

            //If we have the same type of date proposition, we must check if replicating values are not the same as current
            if($(this).find('[name="whole_day"]').prop('checked') === $currentDateLi.find('[name="whole_day"]').prop('checked')
                && $(this).find('[name="slot"]').prop('checked') === $currentDateLi.find('[name="slot"]').prop('checked')) {
                // Each existing time picker (must have an id)
                otherDateSibling.find('.time_pickers .time_picker_container[data-proposition-id][data-proposition-id!=""]').each(function() {
                    let otherTimePickerContainer = $(this);
                    $(inputs).each(function () {
                        let currentTimePickerContainer = $(this);
                        let alreadyAdded = true;

                        //All inputs must match
                        currentTimePickerContainer.find('input').each(function() {
                            let matchingInput = otherTimePickerContainer.find('input[name="' + $(this).attr('name') + '"]');
                            alreadyAdded = alreadyAdded
                                && matchingInput.length > 0
                                && matchingInput.val() === $(this).val();
                        });

                        //If all input match, no need to replicate
                        if(alreadyAdded) {
                            otherTimePickerContainer.attr('data-to-keep', true);
                            inputsAlreadyAdded.push(this);
                        }
                    });
                });
            }
            //Removing useless time_picker_container
            otherDateSibling.find('.time_picker_container:not([data-to-keep])').remove();

            //Each input not already added will be added
            $(inputs).not(inputsAlreadyAdded).each(function() {
                let newTimePickerContainer = $(this).clone();

                //Renewing ids
                newTimePickerContainer.removeAttr('id').find('input').uniqueId();
                //Cleaning business data
                newTimePickerContainer.removeAttr('data-proposition-id').find('.has_time_picker').removeClass('has_time_picker');
                otherDateSibling.find('.time_pickers').append(newTimePickerContainer);
            });
            //Updating checkboxes
            otherDateSibling.find('[name="whole_day"]').prop('checked', $currentDateLi.find('[name="whole_day"]').is(':checked'));
            otherDateSibling.find('[name="slot"]').prop('checked', $currentDateLi.find('[name="slot"]').is(':checked'));

            otherDateSibling.find('.moments_pickers input[type="checkbox"]').each(function() {
                let $src = $currentDateLi.find('.moments_pickers input[name="' + $(this).attr('name') + '"]');

                if($src !== undefined)
                    $(this).prop('checked', $src.is(':checked'));
            });

            //Removing to-keep tag
            otherDateSibling.find('.time_picker_container').removeAttr('data-to-keep');
        });
    };

    updateHoursLabelsOfPropositionLi($propositionLi) {
        let $hoursLabels = $propositionLi.find('.hours_labels');
        $hoursLabels.html('');

        $propositionLi.find('.time_picker_container').each(function() {
            let bt = $(this).find('[name="base_time"]').val();
            let et = $(this).find('[name="end_time"]').val();

            if(bt === '')
                return;

            let $span = $('<span>');
            $span.addClass('hour_label label secondary');
            $span.append(bt + ' ');
            if(et !== '') {
                $span.append(app.lang.tr('to_hour') +' ' + et + ' ');
            }

            $hoursLabels.append($span)
        });

        let $momentsLabels = $propositionLi.find('.moments_labels');
        $momentsLabels.html('');
        $propositionLi.find('.moments_pickers input[type="checkbox"]').each(function() {
            if($(this).is(':checked')) {
                let $span = $('<span>');
                $span.addClass('hour_label label secondary');
                $span.append(app.lang.tr($(this).attr('name')).out());
                $momentsLabels.append($span)
            }
        });
    }

    updateHoursLabels($questionForm) {
        let that = this;
        $questionForm.find('li.proposition:not(:first)').each(function() {
            that.updateHoursLabelsOfPropositionLi($(this));
        });
    }


    updateVariantsLabelsOfPropositionLi($propositionLi) {
        let $variantsLabels = $propositionLi.find('.variants_labels');
        $variantsLabels.html('');

        $propositionLi.find('[name="proposition_variant"]').each(function() {
            let variant = $(this).val();

            if(variant === '')
                return;

            let $span = $('<span>');
            $span.addClass('variant_label label secondary');
            $span.append(variant);

            $variantsLabels.append($span)
        });
    }

    updateVariantsLabels($questionForm) {
        let that = this;
        $questionForm.find('li.proposition:not(:first)').each(function() {
            that.updateVariantsLabelsOfPropositionLi($(this));
        });
    }

    /**
     * Initializes all event bindings of the question step
     */
    initBindings() {

        let that = this;

        /********************************
         * Binding the add question buttons actions
         ********************************/
        this.wizard.onAction('add_date_question', function() {
            that.addQuestion('date')
        });
        this.wizard.onAction('add_date_range_question', function() {
            that.addQuestion('date', {slot:true})
        });
        this.wizard.onAction('add_text_question', function() {
            that.addQuestion('text')
        });

        /********************************
         * Binding Expand/Compress
         ********************************/
        that.wizard.onAction('compress', function() {
            that.wizard.find('[data-action="compress"]').foundation('hide');
            that.wizard.find('[data-action="compress"]').hide();
            that.wizard.find('[data-action="expand"]').css('display','block');

            let count = 1;
            that.wizard.find('.questions_list [data-form="question"]').each(function() {
                var current = $(this);

                //If there is a title
                if(current.find('input[name$="title"]').val().length > 0) {
                    current.find('span.question-title').html(current.find('input[name$="title"]').val());
                } else {
                    current.find('span.question-title').html(app.lang.tr('question') + ' ' + count);
                }
                if(current.attr('data-type') === 'date') {
                    current.find('span.question-type').addClass('fa-calendar-o');

                    //if it's a day_slot question icon is slightly different
                    if(current.hasClass('day_slots_question')) {
                        current.find('span.question-type').addClass('fa-calendar-period');
                    }
                } else {
                    current.find('span.question-type').addClass('fa-file-text-o');
                }
                count++

                current.find('section.expanded').slideUp(200);
                current.find('section.compressed').show();

            });

            that.stepQuestions.find('.advanced-options-container').hide();

        });
        this.wizard.onAction('expand', function() {
            that.wizard.find('[data-action="compress"]').show();
            that.wizard.find('[data-action="expand"]').hide();

            that.wizard.find('[data-form="question"]').each(function() {
                var current = $(this);

                current.find('section.compressed').hide();
                current.find('section.expanded').slideDown(200);
            });

            that.stepQuestions.find('.advanced-options-container').show();
        });

        /********************************
         * Binding the question actions
         ********************************/

        /**
         * Inits pickers only when needed
         */
        this.stepQuestions.on('click','.date_picker', function() { that.initDatePicker($(this)) });
        this.stepQuestions.on('click', '.time_picker', function() { that.initTimePicker($(this)) });

        /**
         * On touch device we try not to show soft keyboard on date pickers
         */
        this.wizard.node.on('focus','.date_picker', function(e) {
            if(window.app.utilities.isTouchDevice()) {
                this.blur();
            }
        });

        this.stepQuestions.on('click', '[data-action="add_text_question_proposition"]', function() {
            that.addTextProposition($(this).parent());
        });
        this.stepQuestions.on('click', '[data-action="add_proposition_variant"]',
            function() {
                that.addPropositionVariant($(this).closest('li.proposition'));
            }
        );
        this.stepQuestions.on('click', '[data-action="reproduce_variants"]', function() {
            let $currentLi = $(this).closest('li.proposition');

            let selectedVariants = $currentLi.find('input[name="proposition_variant"]').map(function() {
                return $(this).val();
            }).get();
            let $propositionInputs = $currentLi.siblings();
            //Adding variants if necessary
            $propositionInputs.each(function() {
                $(this).find('input[name="proposition_variant"]').remove();

                for(let i = 0; i < selectedVariants.length; i++) {
                    that.addPropositionVariant($(this), selectedVariants[i]);
                }
            });
        });

        this.stepQuestions.on('click add-date', '[data-action="add_date_question_proposition"]', function() {
            that.addDateProposition($(this).parent());
        });
        this.stepQuestions.on('click', '[data-action="add_time_picker"]',
            function() {
                that.addTimePicker($(this).closest('li.proposition_date_input'));
            }
        );
        this.stepQuestions.on('click', '[data-action="remove_time_picker"]', function(e) {
            that.removeTimePicker(e, $(this));
        });
        this.stepQuestions.on('click', '[data-action="reproduce_times"]', function() {
            that.reproduceTimes($(this).closest('li.proposition_date_input'));
        });

        this.stepQuestions.on('change', '.questions_list input[type="text"]', function()  {
            that.sendQuestion();
        });

        /**
         * Toggle enable_maybe_choice depending on force_unique_choice value
         */
        this.stepQuestions.on('change', 'input[name="force_unique_choice"]', function() {
            let maybeField = $(this).parents('[data-form="question"]').find('input[name="enable_maybe_choices"]');
            maybeField.prop('disabled', $(this).is(':checked'));
            if($(this).is(':checked')) {
                maybeField.prop('checked', false);
            }
            maybeField.parents('label').toggleClass('disabled', $(this).is(':checked'));
        });

        /**
         * Possibility to add few dates propostions at once (separated by ; or space ...
         */
        this.stepQuestions.on('blur', '.questions_list input[name="base_date"]', function()  {

            let dates = $(this).val().split(/[;\s"]+/);

            if(dates.length <= 1)
                return;

            for(let i = 1; i < dates.length; i++) {
                let baseDay = moment(dates[i], that.localeDateTimeFormat);
                if(baseDay.isValid()) {
                    let currentQuestion = $(this).parents('[data-form="question"]');
                    currentQuestion.find('[data-action="add_date_question_proposition"]').trigger('add-date');
                    currentQuestion.find('li.proposition:last input[name="base_date"]').val(dates[i]).trigger('change');
                }
            }
            $(this).val(dates[0]);
        });

        this.stepQuestions.on('click', '[data-action="delete_proposition"]', function () {
            that.deleteProposition($(this));
        });
        this.stepQuestions.on('click', '[data-action="delete_question"]', function () {
            that.deleteQuestion($(this));
        });

        this.stepQuestions.on('change', '[name="same_for_all"]', function() {
            let $questionForm = $(this).parents('[data-form="question"]')
            if($(this).is(':checked')) {
                $questionForm.find('ul.propositions').addClass('same_for_all');

                let firstPropositionDateLi = $questionForm.find('ul.propositions li.proposition_date_input:first');
                that.reproduceTimes(firstPropositionDateLi);
                that.updateHoursLabels($questionForm);
            } else {
                $questionForm.find('ul.propositions').removeClass('same_for_all');
            }
        });

        this.stepQuestions.on('focus', '[data-form="question"] li.proposition [name$="base_time"]', function(e) {
            let $container = $(this).closest('.time_picker_container');
            if($container.data('duration') === undefined) {
                let baseTime = moment($container.find('input[name="base_time"]').val(), that.localeTimeFormat);
                let endTime = moment($container.find('input[name="end_time"]').val(), that.localeTimeFormat);

                if(baseTime.isValid() && endTime.isValid()) {
                    $container.data('duration', endTime.diff(baseTime, 'minutes'));
                }
            }
        });

        /**
         * When one time picker changes, it may change other pickers
         */
        this.stepQuestions.on('change', '[data-form="question"] li.proposition [name$="_time"]', function(e) {
            let $container = $(this).closest('.time_picker_container');
            let baseTime = moment($container.find('input[name="base_time"]').val(), that.localeTimeFormat);
            let endTime = moment($container.find('input[name="end_time"]').val(), that.localeTimeFormat);

            if(baseTime.isValid() && endTime.isValid()) {

                // End changed => duration changed
                if ($(this).attr('name') === 'end_time') {
                    $container.data('duration', endTime.diff(baseTime, 'minutes'));
                }

                // Base changed => End should be changed to Base + duration
                if ($(this).attr('name') === 'base_time'
                    && $container.data('duration') !== undefined
                    && !isNaN($container.data('duration'))) {

                    let newMomentValue = baseTime.clone().add($container.data('duration'), 'minute');
                    let newValue = newMomentValue.format(that.localeTimeFormat);
                    let minValue = baseTime.clone().add(1, 'minute').format('HH:mm');

                    //Max end time is 23:59
                    if(moment(newValue, that.localeTimeFormat).isBefore(baseTime)) {
                        newValue = baseTime.clone().endOf('day').format(that.localeTimeFormat);
                    }
                    //If base time is 23:59, end time must be empty
                    if(baseTime.format('HH:mm') === baseTime.clone().endOf('day').format('HH:mm')) {
                        newValue = undefined;
                    }
                    //We set end time min and value
                    let $endTimeInput = $container.find('input[name="end_time"]');
                    $endTimeInput.removeClass('is-invalid-input');
                    if ($endTimeInput.hasClass('has_time_picker')) {
                        //Min endtime is base time + X hour
                        $endTimeInput.datetimepicker('setOptions', {
                            minTime: minValue
                        });
                    }
                    $endTimeInput.val(newValue);
                    $endTimeInput.attr('data-min-time', minValue);
                }
            }

            //We may need to reproduce those new values
            let $questionForm = $(this).parents('[data-form="question"]');
            if($questionForm.find('[name="same_for_all"]').is(':checked')
                && $(this).parents('li.proposition').is(':first-of-type')) {
                let index = $(this).parents('.time_pickers').find('.time_picker_container').index($(this).parents('.time_picker_container'));
                $questionForm.find('li.proposition:not(:first)').each(function () {
                    let $matchingPicker = $($(this).find('.time_picker_container').get(index));
                    $matchingPicker.find('[name="base_time"]').val($container.find('input[name="base_time"]').val());
                    $matchingPicker.find('[name="end_time"]').val($container.find('input[name="end_time"]').val());

                    if(baseTime.isValid() && endTime.isValid()) {
                        $matchingPicker.data('duration', endTime.diff(baseTime, 'minutes'));
                    }
                });

                that.updateHoursLabels($questionForm);
            }
        });

        /**
         * When a variant changes we may need to reproduces its value
         */
        this.stepQuestions.on('change', '[data-form="question"] li.proposition [name$="_variant"]', function(e) {
            let $variantInput = $(this);

            let $questionForm = $variantInput.parents('[data-form="question"]');
            if($questionForm.find('[name="same_variants_for_all"]').is(':checked')
                && $variantInput.parents('li.proposition').is(':first-of-type')) {
                let index = $(this).parents('li.proposition').find('[name="proposition_variant"]').index($(this));
                $questionForm.find('li.proposition:not(:first)').each(function () {
                    let $matchingVariantInput = $($(this).find('[name="proposition_variant"]').get(index));
                    $matchingVariantInput.val($variantInput.val());
                });

                that.updateVariantsLabels($questionForm);
            }
        });

        this.stepQuestions.on('change', '[name="same_variants_for_all"]', function() {
            let $questionForm = $(this).parents('[data-form="question"]');
            if($(this).is(':checked')) {
                $questionForm.find('ul.propositions').addClass('same_for_all');

                let firstPropositionDateLi = $questionForm.find('ul.propositions li.proposition:first');
                that.reproduceVariants(firstPropositionDateLi);
                that.updateVariantsLabels($questionForm);
            } else {
                $questionForm.find('ul.propositions').removeClass('same_for_all');
            }
        });

        this.stepQuestions.on('change', '[data-form="question"] li.proposition .moments_pickers input[type="checkbox"]', function() {
            let $questionForm = $(this).parents('[data-form="question"]');
            let $currentDateLi = $(this).parents('.proposition_date_input');

            let $targets = $($currentDateLi);

            if($currentDateLi.parents('[data-form="question"]').find('[name="same_for_all"]').is(':checked')) {
                $targets = $currentDateLi.parents('.propositions').find('.proposition_date_input');
            }
            let currentValue = $(this).attr('name');

            $targets.find('[name="whole_day"]').prop('checked', true);
            $targets.find('[name="slot"]').prop('checked', false);
            $targets.find('[name="'+ currentValue +'"').prop('checked', $(this).is(':checked'));

            if($(this).is(':checked')) {
                $targets.find('.time_picker_container:not(.moments)').remove();
                $targets.find('[name="whole_day"]').prop('checked', false);
                $targets.find('[name="slot"]').prop('checked', true);

                //addTimePicker adds to $targets to
                let $pickerContainer = $currentDateLi.find('.time_picker_container.moments.' + currentValue);
                if($pickerContainer.length === 0)
                    $pickerContainer = that.addTimePicker($currentDateLi);
                let baseTime = '9:00';
                let endTime = '18:00';

                if(that.momentsOfDay.hasOwnProperty(currentValue)) {
                    baseTime = that.momentsOfDay[currentValue].bt;
                    endTime = that.momentsOfDay[currentValue].et;
                }
                $pickerContainer.find('[name="base_time"]').val(baseTime);
                $pickerContainer.find('[name="end_time"]').val(endTime);
                $pickerContainer.addClass(currentValue).addClass('moments');
            } else {
                $targets.find('.time_picker_container.' + currentValue).remove();
            }
            that.updateHoursLabels($questionForm);
        });

        this.stepQuestions.on('focus', '[name="input_mode"]', function() {
            $(this).data('previous', $(this).val());
        });
        this.stepQuestions.on('change', '[name="input_mode"]', function() {
            let $questionForm = $(this).parents('[data-form="question"]');
            let $sameForAll = $questionForm.find('[name="same_for_all"]');
            $sameForAll.parent().find('span').text(app.lang.tr('same_' + $(this).val() + '_for_all'));

            $questionForm.find('ul.propositions').removeClass('hours moments').addClass($(this).val());

            $questionForm.find('.timezone-label').removeClass('alert').addClass('secondary').find('.timezone-info').remove();
            $questionForm.find('.timezone-label .timezone-large').text(app.lang.tr('time_zone_info_'+ $(this).val() +'_large'));
            $questionForm.find('.timezone-label .timezone-small').text(app.lang.tr('time_zone_info_'+ $(this).val() +'_small'));

            if($(this).val() === 'hours') {
                $questionForm.find('.time_pickers > .moments').removeClass('moments');
                $questionForm.find('.timezone-label .timezone-value').text(moment.tz.guess());
            }

            if($(this).val() === 'moments') {
                $questionForm.find('.timezone-label .timezone-value').text(that.wizard.settings.time_zone +'.');
                if(moment.tz.guess() !== that.wizard.settings.time_zone) {
                    $questionForm.find('.timezone-label')
                        .removeClass('secondary').addClass('alert')
                        .append('<span class="hide-for-small-only hide-for-medium-only timezone-info"> ' + app.lang.tr('time_zone_info_moments_info') +  '</span>')
                }
            }

            if($(this).data('previous') !== undefined && $(this).data('previous') !== $(this).val()) {
                $questionForm.find('.time_pickers .time_picker_container:not(:first-of-type)').remove();
                $questionForm.find('.time_pickers .time_picker_container:first-of-type [name$="_time"]').val('');
                $questionForm.find('.moments_pickers input[type="checkbox"]').prop('checked', false);
                that.updateHoursLabels($questionForm);
            }

        });


        /********************************
         * Binding wizard event
         ********************************/
        this.stepQuestions.on('validate', function() {
            that.stepQuestions.find('li.question-form form').foundation('validateForm');

            //Validating question
            var alerts = that.sendQuestion();

            //Triggering event on valid/invalid
            if(alerts.length === 0) {
                that.stepQuestions.trigger('valid');
            } else {
                that.stepQuestions.find('[data-form="question"].has-error').each(function() {
                    $(this).find('.is-invalid-input.silent').removeClass('silent');
                    $(this).find('.invalid-question-message').show();

                    let questionTop = $('[data-form="question"].has-error').offset().top;
                    if($(window).scrollTop() > questionTop ||  $(window).scrollTop() + $(window).innerHeight() < questionTop) {
                        $(document).scrollTop(questionTop);
                    }
                });

                that.stepQuestions.trigger('invalid', [alerts]);
            }
        });

        this.stepQuestions.on('update', function() {
            var questionIndex = 0;
            that.stepQuestions.find('[data-form="question"][data-type]').each(function() {

                if(app.ui.survey.attributes.questions[questionIndex] !== undefined) {
                    $(this).attr('data-question-id', app.ui.survey.attributes.questions[questionIndex].id);
                }
                questionIndex++;
            });

            that.sendQuestion();
        });

        /*********************************
         * Internal events
         *********************************/
        this.stepQuestions.on('step_question_change', function() {

            //Triggering change on constraint select question
            that.stepQuestions.find('select[name="question"]').trigger('change');

            if(app.ui.controller !== undefined && app.ui.controller.constraintsController !== undefined) {
                //Updating constraint list
                app.ui.controller.constraintsController.updateConstraintsList();
            }

            let questionFormCount = that.stepQuestions.find('[data-form="question"][data-type]').length;

            //If we are not compressed, check if we need to display compress button
            if(that.stepQuestions.find('section.compressed:visible').length === 0) {
                that.stepQuestions.find('[data-action="compress"]').toggle(questionFormCount >= 2);

                that.stepQuestions.find('.advanced-options-container').toggle(questionFormCount > 0);
            }

        });
        this.stepQuestions.trigger('step_question_change');

        $(window).on('adapted', function() {
            that.wizard.find('.questions_list [data-form="question"][data-type="date"]').each(function() {
                if($(this).attr('data-proposition-type') === 'range_of_days') {
                    that.initDateRangePickr($(this));
                } else {
                    that.initMultiDatePickr($(this));

                    //Showing timezone-label
                    $(this).find('.timezone-label').css('display', 'inline-block');
                }

                $(this).on('propositions_grouped', function() {
                    that.updateHoursLabels($(this));
                    that.initSameHoursForAll($(this));
                    that.initInputMode($(this));
                });

                that.updatePropositionsList($(this).find('ul.propositions'));
            });

            that.wizard.find('.questions_list [data-form="question"][data-type="text"]').each(function() {
                that.initSameVariantsForAll($(this));
            });

        });

        /**
         * Calendar filtering
         */
        app.userManager.initCalendarFilter();
    }

    /**
     * Update the status of step
     */
    updateStatus() {

        if(app.ui.survey !== typeof undefined) {

            let stepQuestions = this.wizard.find('[data-container="steps"] [data-step="questions"]');

            //To validate this step, a question with ate least one proposition is needed
            let newStatus = (app.ui.survey.attributes.questions.length > 0
                && app.ui.survey.attributes.questions[0].propositions.length > 0
                && stepQuestions.attr('data-invalid-questions-count') <= 0
            )?'ready':'waiting';
            if(newStatus !== stepQuestions.attr('data-status')) {
                stepQuestions.attr('data-status',newStatus);

                app.ui.survey.binder.trigger('statusChanged', [stepQuestions]);
            }
        }
    }

    /**
     * Stores the question in the survey
     */
    sendQuestion() {
        //Validating question
        let alerts = this.validator.validateQuestions();

        //Updating constraint form
        let selectedQuestion = this.stepQuestions.find('select[name="question"]').val();
        this.stepQuestions.find('select[name="question"]').html('');
        for(let i = 0; i < this.questions.length; i++) {
            let statement = this.questions[i].title;
            if(statement === "") {
                statement = app.lang.tr('question') + ' ' + (i+1);
            }

            let selected = (selectedQuestion === i.toString())?'selected':''

            this.stepQuestions.find('select[name="question"]').append('<option value="' + i + '" '+ selected +  '>' + statement + '</option>')
        }

        //Linking constraint to propositions
        for(let i = 0; i < app.ui.controller.constraintsController.allConstraints.length; i++) {
            for(let j = 0; j < this.questions.length; j++) {
                let questionTmpId = (this.questions[j].id !== undefined)?this.questions[j].id:this.questions[j].tmp_id;
                let constraintOnAllPropositions = undefined;
                if (questionTmpId === app.ui.controller.constraintsController.allConstraints[i].question.id) {
                    for (let k = 0; k < this.questions[j].propositions.length; k++) {
                        let propositionTmpId = (this.questions[j].propositions[k].id !== undefined)?this.questions[j].propositions[k].id:this.questions[j].propositions[k].tmp_id;
                        if(propositionTmpId === app.ui.controller.constraintsController.allConstraints[i].proposition.id
                            || app.ui.controller.constraintsController.allConstraints[i].proposition.id === -1) {
                            if (this.questions[j].propositions[k].options === undefined) {
                                this.questions[j].propositions[k].options = {constraints: []};
                            }
                            this.questions[j].propositions[k].options.constraints.push(app.ui.controller.constraintsController.allConstraints[i]);

                            if(app.ui.controller.constraintsController.allConstraints[i].proposition.id === -1
                                && constraintOnAllPropositions === undefined)
                                constraintOnAllPropositions = app.ui.controller.constraintsController.allConstraints[i];
                        }
                    }
                }

                if(constraintOnAllPropositions !== undefined) {
                    this.questions[j].constraints = [constraintOnAllPropositions];
                }
            }
        }

        //Setting new question list
        app.ui.survey.set('questions', this.questions);

        this.stepQuestions.trigger('step_question_change', [[this.questions]]);

        return alerts;
    }

    /**
     * Compute label for proposition (header - variant ...)
     * @param proposition
     * @returns {*|jQuery}
     */
    static getPropositionLabel(proposition) {
        var propositionLabel = '';
        if(proposition.header !== undefined) {
            propositionLabel = proposition.header;
            if(proposition.text !== undefined) {
                propositionLabel += ' - ' +  proposition.text
            }
        } else {
            propositionLabel =  window.app.ui.timeLocalizer().localize('date',proposition.local_base_day);
            if(proposition.base_time !== undefined) {
                propositionLabel += ' - ' +  window.app.ui.timeLocalizer().localize('time',proposition.base_day + proposition.base_time);
            }
            if(proposition.end_day !== undefined) {
                propositionLabel += ' ' + app.lang.tr('to_day') + ' '  +  window.app.ui.timeLocalizer().localize('date',proposition.end_day);
            }
            if(proposition.end_time !== undefined) {
                propositionLabel += ' ' + app.lang.tr('to_hour') + ' ' + window.app.ui.timeLocalizer().localize('time',proposition.base_day + proposition.end_time);
            }
        }
        return propositionLabel;
    }

    /**
     * Find question in model by id (or tempid)
     * @param questionId
     * @returns {*}
     */
    static findQuestion(questionId) {
        for(let i = 0; i < app.ui.survey.attributes.questions.length; i++) {
            if(app.ui.survey.attributes.questions[i].id === questionId
                || app.ui.survey.attributes.questions[i].tmp_id === questionId) {
                //adding index to object (may be useful)
                app.ui.survey.attributes.questions[i].index = i;
                return app.ui.survey.attributes.questions[i];
            }
        }
        return undefined;
    }

    /**
     * Find propostion in question model by id (or tempid)
     * @param propositionId
     * @returns {*}
     */
    static findProposition(questionId, propositionId) {
        for(let i = 0; i < app.ui.survey.attributes.questions.length; i++) {
            if(app.ui.survey.attributes.questions[i].id === questionId
                || app.ui.survey.attributes.questions[i].tmp_id === questionId) {
                for(let j = 0; j < app.ui.survey.attributes.questions[i].propositions.length; j++) {
                    if(app.ui.survey.attributes.questions[i].propositions[j].id === propositionId
                        || app.ui.survey.attributes.questions[i].propositions[j].tmp_id === propositionId) {
                        //adding index to object (may be useful)
                        app.ui.survey.attributes.questions[i].propositions[j].index = i;
                        return app.ui.survey.attributes.questions[i].propositions[j];
                    }
                }
            }
        }
        return undefined;
    }

}

class QuestionValidator {

    constructor(parent) {
        this.parent = parent;
    }

    initQuestionWithForm(currentForm) {
        //Trimming title
        currentForm.find('input[name$=".title"]').val($.trim(currentForm.find('input[name$=".title"]').val()));

        let type = currentForm.attr('data-type');

        let question = {
            title : currentForm.find('input[name$=".title"]').val(),
            type : type,
            proposition_type: 'default',
            position: $('.questions_list [data-form="question"]').index(currentForm),
            options: {},
            propositions: [],
            constraints: []
        };

        if(currentForm.attr('data-question-id') !== undefined) {
            question.id = currentForm.attr('data-question-id');
        }
        // Keep reference to the question form
        question.tmp_id = currentForm.uniqueId().attr('id');

        //Dealing with options (feature idea : make it dynamic)
        question.options.force_unique_choice = currentForm.find('input[name$="force_unique_choice"]').is(':checked')?1:0;
        question.options.enable_maybe_choices = currentForm.find('input[name$="enable_maybe_choices"]').is(':checked')?1:0;
        question.options.input_mode = currentForm.find('[name="input_mode"]').val();

        return question;
    };

    validateTextQuestion(questionForm, question, alerts) {
        //Dealing with propositions
        questionForm.find('[data-form-proposition="text"] li.proposition').each(function() {
            //Validate proposition
            if($(this).find('input[name$="proposition_header"]').val() === undefined
                || $(this).find('input[name$="proposition_header"]').val().length === 0) {
                $(this).find('label').addClass('has-error');
                $(this).find('input[name$="proposition_header"]').addClass('is-invalid-input');
                $(this).find('input[name$="proposition_header"]').addClass('has-error');

                //Error is "silent" till wanted
                $(this).find('input[name$="proposition_header"]').addClass('silent');

                alerts.push({message: app.lang.translate('proposition_empty_error') + ' : ' + $(this).find('label:first').text().trim() + '.'})
            } else {

                let proposition_header = $(this).find('input[name$="proposition_header"]').val();
                let hasVariant = false;

                $(this).find('input[name$="proposition_variant"]').each(function() {

                    if($(this).val().length > 0) {
                        hasVariant = true;

                        let proposition = {
                            header: proposition_header,
                            text: $(this).val(),
                            id: ($(this).attr('data-proposition-id') !== typeof undefined)?
                                $(this).attr('data-proposition-id'):undefined,
                            tmp_id: $(this).uniqueId().attr('id')
                        };

                        proposition.label =  SurveyQuestionsController.getPropositionLabel(proposition);

                        question.propositions.push(proposition);
                    }
                })

                //If it's only a header
                if(!hasVariant) {
                    let proposition = {
                        header: proposition_header,
                        id: ($(this).find('input[name$="proposition_header"]').attr('data-proposition-id') !== typeof undefined)?
                            $(this).find('input[name$="proposition_header"]').attr('data-proposition-id'):undefined,
                        tmp_id: $(this).find('input[name$="proposition_header"]').attr('id')
                    };
                    proposition.label =  SurveyQuestionsController.getPropositionLabel(proposition);
                    question.propositions.push(proposition);
                }
            }

        });
    }

    validateDateQuestion(questionForm, question, alerts) {
        let localeDateTimeFormat = app.lang.translate('moment_datetime_format').text;
        questionForm.find('[data-form-proposition="date"] .proposition_date_input').each(function () {

            if($(this).find('.date_picker').val() === undefined) return;

            let dates = $(this).find('.date_picker').val().split(/[;\s"]+/);

            let uncompleteDayHour = 0;
            for(let i = 0; i < dates.length; i++) {

                let baseDay = moment(dates[i], localeDateTimeFormat);
                let currentLine = $(this);
                if (baseDay.isValid()) {
                    //Datetime picker is already initialized
                    let d = baseDay.toDate();
                    let unixBaseDay = parseInt(d.getTime() / 1000) - (d.getTimezoneOffset() * 60);

                    let proposition = {
                        id: $(this).find('.date_picker').attr('data-proposition-id'),
                        tmp_id: $(this).find('.date_picker').attr('id')+i,
                        type: 'day',
                        base_day: unixBaseDay,
                        local_base_day: unixBaseDay
                    }

                    // whole day and not slot or no time picker (if all time_pickers have been deleted, it's a date)
                    if($(this).find('.time_pickers .time_picker_container').length === 0) {
                        proposition.label =  SurveyQuestionsController.getPropositionLabel(proposition);

                        //Date only
                        question.propositions.push(JSON.parse(JSON.stringify(proposition)));

                    } else {
                        let current_date_proposition_type = proposition.type;
                        $(this).find('.time_pickers .time_picker_container').each(function () {
                            let endDay = moment($(this).find('[name="end_date"]').val(), localeDateTimeFormat);
                            //We must take care of date of this time (to get the correct seconds offset)
                            let baseTime = app.utilities.getSecondsOffset($(this).find('[name="base_time"]').val(), dates[i]);
                            let pBaseDay = unixBaseDay;

                            //If diff between midnight UTC and actual time > 86400 (one day) it means baseTime must refer next day
                            if(baseTime >= 86400) {
                                pBaseDay += 86400;
                            }
                            //If diff between midnight UTC and actual time < 0 it means baseTime must refer previous day
                            if(baseTime < 0) {
                                pBaseDay -= 86400;
                            }

                            let endTime = undefined;
                            if(endDay.isValid()) {
                                endTime = app.utilities.getSecondsOffset($(this).find('[name="end_time"]').val(), $(this).find('[name="end_date"]').val());
                            } else {
                                endTime = app.utilities.getSecondsOffset($(this).find('[name="end_time"]').val(), dates[i]);
                            }

                            if ($(this).css("display") === 'none')
                                return;

                            proposition = {
                                //Re-init fields
                                id: $(this).attr('data-proposition-id'),
                                //temporary id from time_picker_container id
                                tmp_id: $(this).uniqueId().attr('id'),
                                base_day: pBaseDay,
                                local_base_day: unixBaseDay,
                                type: 'day'
                            }

                            if(endDay.isValid()) {
                                if(endDay.isAfter(baseDay) || endDay.isSame(baseDay)) {
                                    let dateEndDay = endDay.toDate();
                                    proposition.end_day = parseInt(dateEndDay.getTime() / 1000) - (dateEndDay.getTimezoneOffset() * 60);
                                    proposition.type = 'range_of_days';
                                } else {
                                    $(this).find('[name="end_date"]').addClass('is-invalid-input');

                                    alerts.push({
                                        message: app.lang.translate('end_date_prior_to_base_date'),
                                        node: $(this).find('[name="end_date"]')
                                    });
                                }
                            }

                            if (baseTime !== undefined) {
                                proposition.base_time = baseTime;
                                proposition.type = 'day_hour';
                            }
                            if (endTime !== undefined) {
                                proposition.end_time = endTime;
                                proposition.type = 'range_of_hours';

                                if (baseTime === undefined) {
                                    uncompleteDayHour++;
                                    $(this).find('[name="base_time"]').addClass('is-invalid-input');
                                    alerts.push({
                                        message: app.lang.translate('proposition_empty_error') + ' (bt)',
                                        node: $(this).find('[name="base_time"]')
                                    });
                                }

                                if(endTime <= baseTime) {
                                    $(this).find('[name="end_time"]').addClass('is-invalid-input');
                                    alerts.push({
                                        message: app.lang.translate('end_time_prior_to_base_time'),
                                        node: $(this).find('[name="end_time"]')
                                    });
                                }
                            }


                            //If there is no alerts we push the proposition
                            if (alerts.length === 0 && uncompleteDayHour < 1
                                && (proposition.base_time !== undefined
                                    || current_date_proposition_type === 'day'
                                    || current_date_proposition_type === 'range_of_days')) {
                                proposition.label = SurveyQuestionsController.getPropositionLabel(proposition);
                                question.propositions.push(JSON.parse(JSON.stringify(proposition)));

                                current_date_proposition_type = proposition.type;
                            }
                        });
                    }
                } else {
                    if($(this).find('.date_picker').length > 0 && $(this).find('.date_picker').val().length === 0) {
                        $(this).find('.date_picker').addClass('is-invalid-input');
                        alerts.push({
                            message: app.lang.translate('proposition_empty_error') + ' : ' +$(this).find('label:first').text().trim()+ '.',
                            node: $(this).find('.date_picker')
                        })
                    }
                }
            }

        });
    }

    validateQuestion(questionForm) {
        questionForm.removeClass('has-error');
        questionForm.find('input').removeClass('has-error').removeClass('is-invalid-input');
        questionForm.find('label').removeClass('has-error');

        //Trimming spaces
        questionForm.find('input[type="text"]').each(function() {
            $(this).val($.trim($(this).val()));
        });

        let question = this.initQuestionWithForm(questionForm);

        let alerts = [];

        //Filling propositions
        switch(questionForm.attr('data-type')) {
            case 'date' :
                this.validateDateQuestion(questionForm, question, alerts);
                break;
            case 'text' :
                this.validateTextQuestion(questionForm, question, alerts);
                break;
        }

        if(question.propositions.length === 0) {
            alerts.push({message: app.lang.translate('no_proposition_found') + ''});
        }

        if(alerts.length > 0) {
            questionForm.addClass('has-error');
        }

        return {
            'alerts' : alerts,
            'question' :question
        };
    }

    validateQuestions() {
        //We get all question forms with a type (not the template)
        let questionForms = this.parent.stepQuestions.find('[data-form="question"][data-type]');

        questionForms.find('.invalid-question-message').hide();

        let alerts = [];
        let invalidQuestionsCount = 0;

        let that = this;
        this.parent.questions = [];

        questionForms.each(function() {
            let results = that.validateQuestion($(this));
            if(results.question)
                that.parent.questions.push(results.question);

            //Displaying error message for the question
            if(results.question.propositions.length === 0) {
                $(this).find('.invalid-question-message > span').text(app.lang.tr('you_must_select_at_least_one_proposition'));
            } else if($(this).find('.is-invalid-input').length > 0) {
                $(this).find('.invalid-question-message > span').text(app.lang.tr('question_has_invalid_inputs'));
            }

            if($(this).find('.is-invalid-input').length > 0 || results.alerts.length > 0) {
                invalidQuestionsCount++;
            }

            alerts = alerts.concat(results.alerts);
        });

        if(this.parent.questions.length === 0) {
            alerts.push({message: app.lang.translate('no_question_found')});
        }

        //Storing invalid question count (if > 0 step is not valid)
        this.parent.stepQuestions.attr('data-invalid-questions-count', invalidQuestionsCount);

        return alerts;
    }
}