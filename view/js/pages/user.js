window.app.userManager = {

    view: undefined,

    preferences: undefined,

    preferencesForm: undefined,

    calendarList: undefined,

    newCalendarButton: undefined,

    newCalendarRow: undefined,

    newCalendarForm: undefined,

    myFullCalendar: undefined,

    myCalendars: undefined,

    calendarCount: undefined,

    hideCalendarActions : function() {
        this.calendarList.find('li[data-calendar-id]').find('.calendar-actions').hide();
        this.newCalendarButton.hide();
    },

    showCalendarActions : function() {
        this.calendarList.find('li[data-calendar-id]').find('.calendar-actions').show();
        this.newCalendarButton.show();
    },

    clearNewCalendarForm : function() {
        this.newCalendarRow.find(':input:not([type="color"])').val('');
        this.newCalendarRow.find('[data-loader]').remove();
        this.newCalendarRow.find('.error_hint').html('');
        this.newCalendarForm.lock(false);
    },

    hideNewCalendarForm : function() {
        this.clearNewCalendarForm();
        this.newCalendarButton.show();
        this.showCalendarActions();
        this.newCalendarRow.hide();

        //we show "no-calendar" if necessary
        if(this.calendarList.find('li[data-calendar-id]').length === 0)
            this.view.find('.no-calendar').show();
    },

    showNewCalendarForm : function() {
        this.clearNewCalendarForm();
        this.newCalendarButton.hide();
        this.hideCalendarActions();
        //Remove all other forms
        this.calendarList.find('li[data-calendar-id].calendar-form-row').remove();
        this.calendarList.find('li[data-calendar-id]').show();

        this.newCalendarRow.show();
        this.newCalendarRow.removeClass('hidden');

        //we hide "no-calendar"
        this.view.find('.no-calendar').hide();
    },

    calendarValidator : function (fulfill, reject, data) {
        var calendarRow = app.userManager.calendarList.find('[data-form^="calendar_form"]:visible').closest('li');

        calendarRow.find('[data-action]').hide();
        calendarRow.find('menu').prepend('<span data-loader class="fa fa-circle-o-notch fa-spin fa-fw" />');

        app.client.post('/CalendarCheck', {url: encodeURIComponent(data)}, function (path, data) {
            calendarRow.find('[data-loader]').remove();
            calendarRow.find('[data-action]').show();

            if (typeof data.code === "undefined"
                || typeof data.message === "undefined") {
                reject('something_went_wrong_when_checking_calendar');
            } else {
                if (data.code === 200) {
                    fulfill();
                } else {
                    reject(data.message);
                }
            }
        });
    },

    /**
     * Add a calendar list item from template
     * @param calendar
     */
    addCalendarRow : function(calendar) {
        var templateRow = this.calendarList.find('li.calendar-row-template');
        var row = templateRow.clone()
            .attr('data-calendar-id', calendar.id)
            .attr('data-calendar-url', calendar.url)
            .attr('data-calendar-name', calendar.name)
            .attr('data-calendar-color', calendar.settings.color)
            .removeClass('hidden calendar-row-template');

        row.find('.calendar-color .fa').css('color', calendar.settings.color);
        row.find('.calendar-name').html(calendar.name);
        row.find('.calendar-url a').attr('href',calendar.url).attr('title',calendar.url).html(calendar.url);
        row.insertBefore(templateRow);
        return row;
    },

    /**
     * Add an edition row under calendarRow
     * @param calendarRow
     */
    addEditCalendarRow : function(calendarRow) {
        //Creating new form for update
        var editCalendarRow = this.newCalendarRow.clone();
        var formId = 'calendar_form_' + (editCalendarRow.uniqueId().attr('id'));
        editCalendarRow.attr('data-calendar-id', calendarRow.attr('data-calendar-id'));
        editCalendarRow.find('[data-form]').attr('data-form', formId);
        editCalendarRow.find('[data-action="create"]').attr('data-tooltip','').attr('data-action', 'update').attr('title', lang.tr('update'));
        editCalendarRow.find('[data-action="cancel"]').attr('data-tooltip','').attr('title', lang.tr('cancel'));
        editCalendarRow.find('input[name="id"]').val(calendarRow.attr('data-calendar-id'));
        editCalendarRow.find('input[name="name"]').val(calendarRow.attr('data-calendar-name'));
        editCalendarRow.find('input[name="url"]').val(calendarRow.attr('data-calendar-url'));
        editCalendarRow.find('input[name="color"]').val(calendarRow.attr('data-calendar-color'));

        calendarRow.after(editCalendarRow);

        //Remove color picker before form init
        editCalendarRow.find('input[name="color_picker"]').remove();

        var editCalendarForm = app.forms.get(formId);
        editCalendarForm.addValidator(this.calendarValidator, 'url');

        //Add the new color picker
        var $colorInput = $('<input type="color" name="color_picker" />');
        $colorInput.val(calendarRow.attr('data-calendar-color'));
        editCalendarRow.find('input[name="color"]').closest('label').prepend($colorInput);

        editCalendarRow.find(':input').off('change input');

        //Initialize listeners
        editCalendarRow.find('[data-action="cancel"]').on('click', function(e) {
            app.userManager.initDisplay();
            calendarRow.show();
        });

        editCalendarRow.find('[data-action="update"]').on('click', function(e) {
            editCalendarForm.getData().then(app.userManager.updateCalendar);
        });

        //Show new form
        calendarRow.hide();
        editCalendarRow.show();

        return editCalendarRow;
    },

    /**
     * Creates calendar entity with data
     * @param data
     */
    createCalendar : function(data) {
        //Show loader
        app.ui.toggleLoader(true, app.lang.tr('saving_calendar').out());

        app.client.post('/Calendar', data, function(path, res) {
            if(res && typeof res.data.id !== "undefined") {
                app.ui.notify('success',lang.tr('calendar_added_with_success'));
                app.userManager.addCalendarRow(res.data);
                app.userManager.initDisplay();
                app.userManager.refreshMyCalendars();
            } else {
                app.ui.notify('error',lang.tr('calendar_adding_failed'));
            }
            //Hide loader
            app.ui.toggleLoader(false);
        }, { error: function(error) {
            app.userManager.handleCalendarError(error, lang.tr('calendar_adding_failed'));
        }});
    },

    /**
     * Update calendar with data
     * @param data
     */
    updateCalendar : function(data) {
        //Show loader
        app.ui.toggleLoader(true, app.lang.tr('updating_calendar').out());

        app.client.put('/Calendar/' + data.id, data, function(res) {
            if(res && typeof res.data.id !== "undefined") {
                let calendar = res.data;
                app.userManager.initDisplay();
                app.userManager.refreshMyCalendars();

                var calendarRow = app.userManager.calendarList.find('li[data-calendar-id="'+calendar.id+ '"]');
                calendarRow.find('.calendar-url a').attr('href', calendar.url).html(calendar.url);
                calendarRow.find('.calendar-name').html(calendar.name);
                calendarRow.find('.calendar-color .fa').css('color', calendar.settings.color);
                calendarRow.attr('data-calendar-url', calendar.url);
                calendarRow.attr('data-calendar-name', calendar.name);
                calendarRow.attr('data-calendar-color', calendar.settings.color);
                //Update succeed this calendar is not in error
                calendarRow.find('.fa-exclamation-triangle.calendar-error').remove();
                calendarRow.find('.accordion-title.calendar-error').removeClass('calendar-error');
                calendarRow.slideDown(350);

                app.ui.notify('success', lang.tr('calendar_updated_with_success'));
            }  else {
                app.ui.notify('error',lang.tr('calendar_updating_failed'));
            }
            //Hide loader
            app.ui.toggleLoader(false);
        }, { error: function(error) {
            app.userManager.handleCalendarError(error, lang.tr('calendar_updating_failed'));
        }});
    },

    /**
     * Delete action to realize when click on delete button of a calendarRow
     * @param calendarRow
     */
    deleteCalendarRow : function(calendarRow) {
        var message = app.lang.tr('confirm_calendar_deletion').text.replace('%s', calendarRow.attr('data-calendar-url'));

        app.ui.popup.confirm(message, function() {
            app.client.delete('/Calendar/' + calendarRow.attr('data-calendar-id'), function (data) {
                if (data) {
                    calendarRow.remove();
                    app.userManager.initDisplay();
                    app.userManager.refreshMyCalendars();

                    app.ui.notify('success',lang.tr('calendar_removed_with_success'));
                } else {
                    app.ui.notify('success',lang.tr('calendar_removal_failed'));
                }
            }, {
                error: function(e) {
                    app.userManager.handleCalendarError(e, lang.tr('calendar_removal_failed'));
                }
            });
        });
    },

    /**
     * Handle POST calendar error
     */
    handleCalendarError : function(error, message) {
        app.ui.rawError(error);
        app.ui.notify('error', message);
    },

    /**
     * /Calendar/<id>/events return
     * @param events
     * @param calendar
     * @param callback
     */
    handleCalendarEventsReturn : function (events, calendar, callback) {
        var structured = [];
        for(var j in events) {
            events[j].startDateObj = moment.unix(events[j].start_ts);
            events[j].endDateObj = moment.unix(events[j].end_ts);

            var days = window.app.utilities.getDatesBetweenDates(
                events[j].startDateObj,
                events[j].endDateObj);

            for(var k in days) {
                var strDate  = days[k].format('YYYYMMDD');
                if(typeof structured[strDate] == 'undefined')
                    structured[strDate] = [];

                structured[strDate].push(events[j]);
            }
        }

        app.userManager.myCalendars.push({
            'id' : calendar.id,
            'name' : calendar.name,
            'settings' : calendar.settings,
            'events' : events,
            'structured' : structured
        });
        //All calendars have been loaded
        if(app.userManager.myCalendars.length === app.userManager.calendarCount) {
            //@MyCal must be last
            app.userManager.myCalendars.sort(function(a,b) {
                if(a.id === '@MyCal')
                    return 1;
                if(b.id === '@MyCal')
                    return -1;
                return 0;
            });

            callback(app.userManager.myCalendars);

            app.userManager.loading_calendars = false;
            $(window).trigger('calendars_loaded');
        }
    },

    /**
     * Handle /Calendar/@me return
     * @param data
     * @param callback
     */
    handleCalendarAtMeReturn : function (data, callback) {
        if (data && $.isArray(data)) {
            // counting OK calendars
            app.userManager.calendarCount = 0;
            for(var i in data) {
                if (data[i].status.code === 200) {
                    app.userManager.calendarCount++;
                }
            }
            //Load external calendar
            app.userManager.myCalendars = [];
            for(var i in data) {
                if (data[i].status.code === 200) {
                    let calendar = data[i];
                    app.client.get('/Calendar/' + calendar.id + '/events', function (events) {
                        app.userManager.handleCalendarEventsReturn(events, calendar, callback);
                    });
                }
            }
        }
    },

    /**
     * Get all events of calendars of current user
     * @param callback the call back that wiull get calendars
     * @returns {*}
     */
    getMyEvents : function (callback) {
        //Calendars are loaded
        if(typeof app.userManager.myCalendars != 'undefined')
            return callback(app.userManager.myCalendars);

        //Calendars are being loaded
        if(app.userManager.loading_calendars) {
            $(window).on('calendars_loaded', function() {
                app.userManager.getMyEvents(callback);
            });
        } else {
            //Load calendars
            app.userManager.loading_calendars = true;

            app.client.get('/Calendar/@me', function (data) {
                app.userManager.handleCalendarAtMeReturn(data, callback);
            }, { error: function(error) {
                app.ui.notify('warning', lang.tr('loading_calendars_unavailability_failed'));
            }});
        }
    },

    /**
     * Sets current EventSources to FullCalendar instance myFullCalendar
     * @returns {Array}
     */
    setEventSources : function() {
        app.client.get('/Calendar/@me', function (data) {
            if (data && $.isArray(data)) {
                for(var i in data) {
                    if(data[i].status.code === 200) {
                        app.userManager.myFullCalendar.addEventSource(
                            {
                                id:data[i].id,
                                url:'rest/Calendar/' + data[i].id + '/events',
                                color:data[i].settings.color
                            }
                        )
                    }
                }
            }
        });

        return [];
    },

    /**
     * inits FullCalendar instance myFullCalendar
     */
    initMyCalendars : function() {

        this.myFullCalendar = new FullCalendar.Calendar(document.getElementById('calendar'), {
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,dayGridWeek,dayGridDay'
            },
            locale: app.lang.getCode(),
            editable: false,
            dayMaxEvents: true, // allow "more" link when too many events
            moreLinkClick: "day",
            navLinks: true,
            views: {
                dayGrid: {
                    // options apply to dayGridMonth, dayGridWeek, and dayGridDay views
                    displayEventEnd: true
                }
            },

            firstDay: 1,
            loading: function (isLoading) {
                if(isLoading) {
                    app.userManager.view.find('.view-my-calendars-overlay').show();
                } else {
                    app.userManager.view.find('.view-my-calendars-overlay').hide();
                }
            },
        });
        this.setEventSources();
        this.myFullCalendar.render();
    },

    /**
     * Refresh myFullCalendar events
     */
    refreshMyCalendars : function() {
        this.myFullCalendar.removeAllEventSources();
        this.setEventSources();
    },

    /**
     * Get excluded calendar ids
     * @returns {jQuery}
     */
    getExcludedCalendarIds : function() {
        return  $('input.filter-calendar:not(:checked)').map(function() {
            return $(this).attr('data-calendar-id');
        }).toArray()
    },

    /**
     * Refresh user unavailability in picker
     * @param flatpickrItem
     */
    refreshUnavailability : function(flatpickrItem) {

        let localeDateFormat = app.lang.translate('moment_date_format').text;
        let localeTimeFormat = app.lang.translate('moment_time_format').text;
        let localeDateTimeFormat = app.lang.translate('moment_datetime_format').text;

        let clearUnavailability = function(fpickrItem) {
            //Clearing previously set events and title
            $(fpickrItem.days).find('[data-uid] .events').remove();
            $(fpickrItem.days).find('[data-uid]').removeAttr('title');
            //Disable showing older tips
            $(fpickrItem.days).find('[data-uid][data-tooltip!=""]').off('mouseenter.zf.tooltip');
            $('.tooltip > .events-list').parent().remove();
        }

        /**
         * Gte a title for every events of current day
         * @param eventOfDay array of events of the day
         * @returns {string}
         */
        let getEventsTitleForDay = function(eventOfDay) {
            var title = '';
            for(var k in eventOfDay) {
                // Excluding events of the current survey
                if(eventOfDay[k].uid !== undefined) {
                    let items = eventOfDay[k].uid.split('_');
                    if(items.length > 1 && items[1] === app.ui.survey.attributes.id)
                        continue;
                }

                title += '<li><span class="event-dates">';

                var start = eventOfDay[k].startDateObj.local();
                var end = eventOfDay[k].endDateObj.local();

                if(eventOfDay[k].allDay) {
                    title += app.lang.translate('all_day');
                } else if(start.format(localeDateFormat) === end.format(localeDateFormat)) {
                    title += app.lang.translate('from_hour')
                        + ' ' + start.format(localeTimeFormat)
                        + ' ' + app.lang.translate('to_hour')
                        +' ' + end.format(localeTimeFormat);
                } else {
                    title += app.lang.translate('from_day')
                        + ' ' + start.format(localeDateTimeFormat)
                        + ' ' + app.lang.translate('to_day') +' '
                        + end.format(localeDateTimeFormat);
                }

                if(eventOfDay[k].title.length > 0) {
                    title += '</span> ' + eventOfDay[k].title;
                }
                title += '</li>';
            }
            return title;
        }

        /**
         * Adds a span event to every day that is busy
         * @param calendar
         */
        let applyEventsOfCalendar = function(calendar) {
            for(let j in calendar.structured) {
                let eventsLi = getEventsTitleForDay(calendar.structured[j])
                if (eventsLi.length === 0)
                    continue;

                //Add 'events' span
                let $day = $(flatpickrItem.days).find('[data-uid="' + j + '"]');
                let $events = $day.find('span.events');
                if ($events.length === 0) {
                    $events = $("<span class='events'></span>").appendTo($day);
                }

                //Add 'event' span to events (for dots)
                if ($events.find('span.event').length < 3) {
                    let $event = $('<span class="event"></span>')
                        .attr('data-calendar-id', calendar.id)
                        .css('background-color', calendar.settings.color);
                    $events.append($event);
                } else {
                    //Add plus-sign span to events (for dots)
                    if ($events.find('span.event').length === 3) {
                        let $event = $('<span class="event plus-sign"></span>');
                        $events.append($event);
                    }
                }

                //Add event titles to $day title
                if ($day.attr('title') === undefined)
                    $day.attr('title', '');

                let color = '';
                if(calendar.settings.color !== undefined) {
                    color = '<span class="calendar-color fa fa-square fa-lg" style="color:' + calendar.settings.color + '"></span>';
                }
                let calendarLi = '<li class="calendar-name" data-calendar-id="'+calendar.id+'">' + color + calendar.name +'</li>';
                $day.attr('title', $day.attr('title') + calendarLi + eventsLi);
            }
        };

        clearUnavailability(flatpickrItem);
        app.userManager.getMyEvents(function(calendars) {
            var excluded = app.userManager.getExcludedCalendarIds();
            for(var i in calendars) {
                //If calendar is excluded we don't show its events
                if(excluded.indexOf(calendars[i].id + '') < 0) {
                    applyEventsOfCalendar(calendars[i]);
                }
            }
            // Make tooltips
            $(flatpickrItem.days).find('[title]').each(function() {
                $(this).attr('data-click-open', false);
                window.app.ui.updateTooltip(this,
                    app.lang.translate('unavailability') + ' :<br/>'
                    + '<ul class="events-list">'
                    + $(this).attr('title')
                    + '</ul>');
                $(this).on('mousedown', function() { $(this).foundation('hide'); });
            });
        });
    },


    initCalendarFilter : function() {
        $('input.filter-calendar').on('change', function() {
            if ($(this).attr('name') === 'all_calendars') {
                $(this).closest('ul').find('li input.filter-calendar').not(this)
                    .prop('checked', $(this).is(':checked'));
            } else {
                // All calendar check may be updated
                let checkedCount = $('input.filter-calendar:not([name="all_calendars"]):checked').length;
                let allCount = $('input.filter-calendar:not([name="all_calendars"])').length;
                $('input.filter-calendar[name="all_calendars"]')
                    .prop('checked', checkedCount === allCount);
            }

            app.event.trigger('calendar_filter_changed');
        });
    },

    /**
     * Init display
     */
    initDisplay : function() {
        this.showCalendarActions();
        this.clearNewCalendarForm();
        this.hideNewCalendarForm();

        //Remove all other forms
        this.calendarList.find('li[data-calendar-id].calendar-form-row').slideUp(350, function() {
            $(this).remove();
        });

        //we show "no-calendar" if necessary
        if(this.calendarList.find('li[data-calendar-id]').length === 0) {
            this.view.find('.no-calendar').show();
        } else {
            this.view.find('.no-calendar').hide();
        }
    },

    /**
     * Init all bindings
     */
    initBindings : function() {

        setTimeout(function() {
            app.userManager.preferences.on('change', 'input', function(event) {
                let setting = {};
                let name = $(this).attr('name');
                setting[name] = $(this).val();
                if ($(this).attr('type') === 'checkbox') {
                    setting[name] = $(this).is(':checked') ? 1 : 0;
                }
                app.userManager.preferencesForm.getData().then(function(data) {
                    app.client.put('/UserPreferences', setting, function(res) {});
                    app.ui.disableWarnOnLeaving();
                });
            });
        }, 100)

        this.newCalendarButton.on('click', function() {
            app.userManager.showNewCalendarForm();
        });

        this.newCalendarRow.find('[data-action="cancel"]').on('click', function(e) {
            app.userManager.hideNewCalendarForm();
        });

        this.newCalendarRow.find('[data-action="create"]').on('click', function(e) {
            app.userManager.newCalendarRow.find('.error_hint').html('');
            app.userManager.newCalendarRow.find('input[name="color"]').val(app.userManager.newCalendarRow.find('input[name="color_picker"]').val());
            app.userManager.newCalendarForm.getData().then(app.userManager.createCalendar);
        });

        this.newCalendarRow.find(':input').off('change input');

        this.component.on('click', '[data-action="edit_calendar"]', function() {
            app.userManager.hideNewCalendarForm();
            app.userManager.hideCalendarActions();
            app.userManager.addEditCalendarRow($(this).closest('li[data-calendar-id]'));
        });

        this.component.on('click', '[data-action="delete_calendar"]', function() {
            app.userManager.deleteCalendarRow($(this).closest('li[data-calendar-id]'));
        });

        this.component.on('change', 'input[type="color"]', function() {
            $(this).siblings('input[data-color-input]').val($(this).val()).trigger('change');
        });

        /**
         * Refresh calendar link
         */
        this.component.find('[data-action="refresh_calendar_link"]').on('click', function() {
            let row = $(this).closest('li');
            let calendarHash = $(this).attr('data-calendar-hash');

            app.ui.popup.confirm(app.lang.tr('confirm_refresh_calendar_link'), function() {
                row.find('[data-action="refresh_calendar_link"]').hide().after('<span data-loader class="fa fa-circle-o-notch fa-spin fa-fw" />');

                app.client.post('/MyCal/'+ calendarHash +'/regenerate', {}, function(path, calendar) {
                    if(calendar && typeof calendar.id !== "undefined") {
                        app.ui.notify('success',lang.tr('calendar_link_regenerated'));
                    } else {
                        app.ui.notify('error',lang.tr('calendar_link_regenerate_failed'));
                    }
                    row.find('.calendar-url a').attr('href',calendar.url).attr('title',calendar.url).html(calendar.url);
                    row.find('[data-loader]').hide();
                    row.find('[data-action="refresh_calendar_link"]').attr('data-calendar-hash', calendar.hash).show();
                    row.find('[data-action="copy_to_clipboard"]').attr('data-content', calendar.url);

                }, { error: function(error) {
                        app.ui.notify('error',lang.tr('calendar_link_regenerate_failed'));
                        row.find('[data-loader]').hide();
                        row.find('[data-action="refresh_calendar_link"]').show();
                }});
            });
        });


    },

    /**
     * Startup function.
     * Init MOTD list, show on UI if needed
     */
    startup: function(){
        this.component = $('[data-component="user-page"]');
        if(!this.component.length) return;

        app.ui.focus = function() {
            //Disable focusing on fields
        };

        this.view = new app.view(this.component);

        this.preferences = this.view.find('[data-form="preferences_form"]');

        if(this.preferences.length)
            this.preferencesForm = app.forms.get('preferences_form');

        this.calendarList = this.view.find('ul.calendars-accordion');
        this.newCalendarRow = this.view.find('.calendar-form-row');
        this.newCalendarButton = this.view.find('[data-action="add_calendar"]');

        //replier info
        this.newCalendarForm = app.forms.get('calendar_form');

        //Add the new color picker
        var $colorInput = $('<input type="color" name="color_picker" />');
        $colorInput.val(this.newCalendarRow.find('input[name="color"]').val());
        this.newCalendarRow.find('input[name="color"]').closest('label').prepend($colorInput);

        this.initBindings();
        this.newCalendarForm.addValidator(this.calendarValidator, 'url');

        //Initialize calendar view
        this.initMyCalendars();
    }
}

app.event.on('started', function() {
    app.userManager.startup();
});
