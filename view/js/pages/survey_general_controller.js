/**
 * Controller for general step
 * @param wizard
 * @constructor
 */
class SurveyGeneralController {

    constructor(wizard) {
        this.stepGeneral = wizard.find('section[data-step="general"]');
        this.wizard = wizard;
        this.tmpSettingLimitParticipantNb = '';

        this.initBindings();

        /**
         * Start markdown editor
         */
        setTimeout(this.initMarkdownEditor, 20);
    }


    /*****************************************
     * Step General
     *****************************************/

    /**
     * Check step validity
     */
    isValid() {
        let that = this;

        //Trimming spaces
        this.stepGeneral.find('input[name="title"]').val($.trim(this.stepGeneral.find('input[name="title"]').val()));

        //Updating in model (Meltdown doesn't trigger change when adding items)
        this.stepGeneral.find('[name="description"]').trigger('change');

        let generalForm = app.forms.get('general');
        generalForm.getData().then(function() {
            if ('ready' !== that.stepGeneral.attr('data-status')) {
                that.stepGeneral.attr('data-status', 'ready');
                app.ui.survey.binder.trigger('statusChanged', [that.stepGeneral]);
            }
            that.stepGeneral.trigger('valid');

        }, function(errors) {
            if ('waiting' !== that.stepGeneral.attr('data-status')) {
                that.stepGeneral.attr('data-status', 'waiting');
                app.ui.survey.binder.trigger('statusChanged', [that.stepGeneral]);
            }
            //If error is on an inputswitch field we must trigger click on it to show input
            if(errors.subject.node.hasClass('inputSwitch')) {
                errors.subject.node.find('span').click();
            }

            that.stepGeneral.trigger('invalid', [[]]);
        });
    }

    /**
     * Markdown initializing
     */
    initMarkdownEditor() {

        //TODO Think about i18n
        $.meltdown.controlDefs.bold.altText = app.lang.tr('bold');
        $.meltdown.controlDefs.italics.altText = app.lang.tr('italics');
        $.meltdown.controlDefs.ul.altText = app.lang.tr('ul');
        $.meltdown.controlDefs.ol.altText = app.lang.tr('ol');
        $.meltdown.controlDefs.table.altText = app.lang.tr('table');
        $.meltdown.controlDefs.h1.altText = app.lang.tr('title_1');
        $.meltdown.controlDefs.h2.altText = app.lang.tr('title_2');
        $.meltdown.controlDefs.h3.altText = app.lang.tr('title_3');
        $.meltdown.controlDefs.h4.altText = app.lang.tr('title_4');
        $.meltdown.controlDefs.h5.altText = app.lang.tr('title_5');
        $.meltdown.controlDefs.h6.altText = app.lang.tr('title_6');
        $.meltdown.controlDefs.link.altText = app.lang.tr('link');
        $.meltdown.controlDefs.img.altText = app.lang.tr('img');
        $.meltdown.controlDefs.blockquote.altText = app.lang.tr('blockquote');
        $.meltdown.controlDefs.codeblock.altText = app.lang.tr('codeblock');
        $.meltdown.controlDefs.code.altText = app.lang.tr('code');
        $.meltdown.controlDefs.footnote.altText = app.lang.tr('footnote');
        $.meltdown.controlDefs.hr.altText = app.lang.tr('horizontal_rule');

        //Add markdown editor to description
        $('textarea[name="description"]').meltdown({
            previewHeight: 'auto',
            previewCollapses : false,
            controls: $.meltdown.controlsGroup("", "", [
                "bold",
                "italics",
                "ul",
                "ol",
                "|",
                "table",
                $.meltdown.controlsGroup("h", app.lang.tr('headers') + ' ', ["h1", "h2", "h3", "h4", "h5"]),
                "|",
                $.meltdown.controlsGroup("kitchenSink", "Kitchen Sink", [
                    "link",
                    "img",
                    "blockquote",
                    "codeblock",
                    "code",
                    "footnote",
                    "hr"
                ])
            ])
        });
    }

    /**
     * Foreach owner in model we display a line
     */
    updateOwnerList() {
        this.wizard.find('#owners_list').html('');
        for (var i = 0, len = app.ui.survey.attributes.owners.length; i < len; i++) {
            var label = '<span class="owner_item columns" data-index="'+ i +'">'
                  + '<label class="inputSwitch owner_email small-10">'
                  +     '<span class="" ></span>'
                  +     '<input type="email" name="owners['+ i +'].email" value="' + app.ui.survey.attributes.owners[i].email +'" />'
                  +  '</label>';
            label += '<span class="owner_actions">'
                    + ' <span class="float-right">'
                    + ' <span data-action="edit_owner" class="fa fa-pencil" tabindex="0"></span>'
                    + ' <span data-action="delete_owner" class="fa fa-trash-o" tabindex="0"></span>'
                    + '</span>'
                    + '</span>';
            label += '</span>';

            this.wizard.find('#owners_list').append(label);
        }
        this.stepGeneral.inputswitch();
    };

    deleteOwner(element) {
        element = $(element);

        var index = element.parents('.owner_item').attr('data-index');
        if(index !== undefined
            && app.ui.survey.attributes.owners[index] !== undefined
        ) {
            app.ui.survey.attributes.owners.splice(index, 1);
        }

        var general = app.forms.get('general');
        delete general.fieldset.fields['owners[' + index + '].email'];

        element.parents('.owner_item').remove();

        this.updateOwnerList();
    }

    /**
     * Add bindings on field, dom
     */
    initBindings() {
        let that = this;

        /**
         * Toggle the limit participant number input on demand
         */
        let limitParticipantChange =  function() {
            let $limitParticipantsInput = $('input[name="settings.limit_participants"]');
            let $limitParticipantsNbInput = $('input[type="number"][name="settings.limit_participants_nb"]');

            $('label[data-field-path="settings.limit_participants"]').css('display', 'inline');
            $limitParticipantsNbInput
                .attr('disabled', !$limitParticipantsInput.is(':checked'))
                .attr('required', $limitParticipantsInput.is(':checked'));
            $('label[data-field-path="settings.limit_participants_nb"]').css('display', 'inline').toggleClass('hide', !$limitParticipantsInput.is(':checked'));
            $('label[data-field-path="settings.limit_participants_nb"] span[data-label-text=""]').addClass('hide');

            let general = app.forms.get('general');
            if(!$limitParticipantsInput.is(':checked')) {
                that.tmpSettingLimitParticipantNb = general.fieldset.fields['settings.limit_participants_nb'];
                delete general.fieldset.fields['settings.limit_participants_nb'];
                $limitParticipantsNbInput.parent('label').next('.error_hint').hide();
            } else {
                if(that.tmpSettingLimitParticipantNb !== '')
                    general.fieldset.fields['settings.limit_participants_nb'] = that.tmpSettingLimitParticipantNb;
            }

            if($limitParticipantsNbInput.val() === '') {
                $limitParticipantsNbInput.val(1);
            }
        };
        this.stepGeneral.on('change', 'input[name="settings.limit_participants"]', limitParticipantChange);
        limitParticipantChange();

        /**
         * Enable input switching (used for owners)
         */
        this.stepGeneral.inputswitch();

        /**
         * Adding owner
         */
        var newOwnerForm = this.wizard.find('[data-form1="owner"]:not(.owner_edit)').clone();
        this.wizard.onAction('add_owner', function() {
            that.wizard.find('[data-action="add_owner"]').hide();
            that.wizard.find('#owners_list').after(newOwnerForm);
            newOwnerForm.find('input[name="owners.email"]').val('');
            newOwnerForm.find('.error_hint').remove();
            newOwnerForm.attr('data-form', 'owner');
            newOwnerForm.show();
            app.forms.get('owner');
            newOwnerForm.find('[data-action="send_owner"]').off('click').on('click', function() {
                if(app.utilities.isValidEmailAddress(newOwnerForm.find('input[name="owners.email"]').val())) {
                    var owner_email = newOwnerForm.find('input[name="owners.email"]').val();

                    app.ui.survey.attributes.owners.push({email:owner_email});

                    that.updateOwnerList();

                    newOwnerForm.hide();
                    that.wizard.find('[data-action="add_owner"]').show();
                }
            });
        });

        this.stepGeneral.on('keyup', '[data-form="owner"]', function (e) {
            if (e.keyCode === 13) {
                $(this).find('[data-action="send_owner"]').trigger('click');
            }
        });

        /**
         * Changing owner
         */
        this.stepGeneral.on('change', 'input[name^="owners"]', function(e) {
            if($(this).val().trim() === '') {
                that.deleteOwner(this);
                e.stopImmediatePropagation();
            }

        });

        /**
         * Editing owner
         */
        this.stepGeneral.on('click', '[data-action="edit_owner"]', function(){
            //Transmitting click to the right place
            $(this).parents('.owner_item').find('label span').trigger('click');
        });

        /**
         * Deleting owner
         */
        this.stepGeneral.on('click', '[data-action="delete_owner"]', function() {
            that.deleteOwner(this);
        });

        /**
         * Cancelling
         */
        this.stepGeneral.on('click', '[data-action="cancel_owner"]', function() {
            //Hiding form
            $(this).parents('[data-form1="owner"]').hide();

            that.stepGeneral.find('[data-action="add_owner"]').show();
        });

        /**
         * when wizard asks to validate step
         */
        this.stepGeneral.on('validate', function() { that.isValid(); } );

        /**
         * opening calendar
         */
        this.wizard.find('[data-step="general"]').on('click','[data-action="open-auto-close"]', function() {
            that.wizard.find('[name="settings.auto_close"]').trigger('focus');
        });
    }



}

