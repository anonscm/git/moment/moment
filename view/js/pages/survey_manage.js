/**
 * jQuery contains + case insensitive
 *
 */
jQuery.expr[":"].icontains = jQuery.expr.createPseudo(function(arg) {
    return function( elem ) {
        return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

app.event.on('started', function() {
    let wizard = $('[data-component="survey-manage"]');
    if(!wizard.length) return;

    wizard = new app.view(wizard);

    let currentFilter = '';

    let currentRequest = '';

    let collapseAll = function() {
        wizard.find('[data-accordion]').foundation('up', wizard.find('.accordion-content:visible'), true);
    };

    let showMore = 10;

    let updateCounts = function(tab) {

        let matching = '';
        if(wizard.find('input[name="search_field"]').length > 0) {
            matching = '.matching';
        }

        tab.find('span.count').each(function() {
            if($(this).parent().attr('data-filter') !== undefined) {
                $(this).html('(' +
                    tab.find('.survey-item-header'+ $(this).parent().attr('data-filter') + matching).length
                + ')')
            }
        });
    };

    let tagAsMatching = function() {
        $(this).addClass('matching');

        var currentFilterClass = currentFilter.substr(1);

        if(currentFilter.length === 0 || $(this).hasClass(currentFilterClass)) {
            $(this).show();
        }
    };

    let search = function(tab, query) {
        collapseAll();

        tab.find('.survey-item-header').css('display','none').removeClass('matching');

        let requestedStrings = query.split(' ');
        currentRequest = '';

        for(var i = 0; i < requestedStrings.length; i++){
            currentRequest += ':icontains("'+ requestedStrings[i] +'")';
        }

        tab.find('.survey-item-header' + currentRequest).each(function() {
            tagAsMatching.call(this);
        });

        tab.find('.survey-item-content'+ currentRequest)
            .prev('.survey-item-header').each(function() {
            tagAsMatching.call(this);
        });

        let shown = tab.find('.survey-item-header.filtered.matching');
        shown.css('display','none').slice(0, showMore).css('display','block');

        if(shown.filter(':visible').length < showMore) {
            tab.find('[data-action="show_more"]').hide();
        } else {
            tab.find('[data-action="show_more"]').show();
        }

        //No matching
        if(tab.find('.survey-item-header.filtered.matching').length === 0){
            tab.find('.empty-message-container').text(app.lang.tr('no_matching_survey'));
            tab.find('.empty-message-container').show();
        } else {
            tab.find('.empty-message-container').hide();
        }

    };

    let applyFilter = function(source, filter) {
        collapseAll();

        let tab = source.closest('section[data-component]');
        tab.attr('data-filter', filter);

        source.parent().find('[data-action]').removeClass('selected');
        source.addClass('selected');

        let handles = tab.find('.survey-item-header');
        handles.removeClass('filtered');

        //No filter
        if(filter.length === 0) {
            handles.css('display','block')
        } else {
            handles.filter(':not(' + filter + ')').css('display','none');
            handles.filter(filter).css('display','block');
        }
        handles.filter(':visible').addClass('filtered');

        currentFilter = filter;

        const query = (wizard.find('input[name="search_field"]').length > 0)?wizard.find('input[name="search_field"]').val():'';
        search(tab, query);

    };

    /**
     * Get reveal content for reopening form
     * @param survey
     * @returns {string}
     */
    let getReopeningForm = function(survey) {
        let form = '<div class="reopening-form"><label data-nest-level="0" data-field-path="settings.auto_close" data-valid="yes">' +
            '<span data-label-text="">' + app.lang.tr('select_new_auto_close_date') +'</span>' +
            '<input type="text" name="settings.auto_close" />' +
            '</label>' +
            '<span data-action="open-auto-close" class="fa fa-calendar-o"></span>';

        //Participant number limit is the reason it has been closed. Number should be increased
        if(survey.settings.limit_participants && survey.nb_participants >= survey.settings.limit_participants_nb) {

            const minLimit = survey.nb_participants + 1;
            const label = app.lang.tr('select_new_participant_limit_nb').out().replace('%nb_participants%', survey.nb_participants);

            form += '<label data-nest-level="0" data-field-path="settings.auto_close" data-valid="yes">' +
                '<span data-label-text="">' + label +'</span>' +
                '<input type="number" name="settings.limit_participants_nb" value="' + minLimit + '" min="' + minLimit + '"/>' +
                '</label>';
        }
        form += '</div>';

        return form;
    };

    /**
     * Constructs the reopening popup
     * @param survey
     * @returns {*|Dialog}
     */
    let getReopeningPopup = function(survey) {

        const title = app.lang.tr('survey_reopening_title') + ' "' + survey.title + '"';

        return  app.ui.popup.create(title,
        {
            validate : function() {

                $(this).find('.error_hint').remove();

                const settings = {};
                const $auto_close_input = $(this).find('input[name="settings.auto_close"]');

                //Checking validity
                settings.auto_close = moment.utc($auto_close_input.val(), app.lang.translate('moment_date_format').text);

                if(!settings.auto_close.isValid()
                    || settings.auto_close.unix() > moment.unix(survey.settings.auto_close_max).unix()
                    || settings.auto_close.unix() < moment.unix(survey.settings.auto_close_min).unix()
                ) {
                    $auto_close_input.before('<div class="error_hint">' + app.lang.tr('invalid_new_auto_close_date') + '</div>');
                    return false;
                }
                //Timestamp conversion
                settings.auto_close = settings.auto_close.unix();

                const $limit_input = $(this).find('input[name="settings.limit_participants_nb"]');

                if($limit_input.length > 0) {
                    settings.limit_participants_nb = $limit_input.val();

                    if(isNaN(settings.limit_participants_nb)) {
                        $limit_input.before('<div class="error_hint">' + app.lang.tr('invalid_new_limit_participants_nb') + '</div>');
                        return false;
                    }

                    if(settings.limit_participants_nb < (survey.nb_participants + 1)) {
                        $limit_input.before('<div class="error_hint">' + app.lang.tr('limit_participants_nb_too_low') + '</div>');
                        return false;
                    }
                }

                app.ui.toggleLoader(true);

                // Trigger reopening
                app.client.post('/survey/' + survey.id + '/reopen',
                    {
                        settings: settings
                    },
                    function (path, data) {
                        app.ui.toggleLoader(false);

                        window.app.ui.notify('info', app.lang.tr('survey_reopened'));

                        //Redirecting to updated view
                        window.location.replace(app.config.application_url + "survey/manage?focus_id=" + survey.id);
                    }
                );
                return true;
            },
            cancel : function() {
                app.ui.popup.close(this);
            }
        });
    };


    /**
     * Show the reopening for for this surveyId
     *
     * @param surveyId
     */
    let displayReopeningForm = function(surveyId) {

        app.client.get('/survey/' + surveyId, function(data) {

            if(Array.isArray(data) && data.length > 0) {
                const survey = data.shift();
                const message = getReopeningForm(survey);
                const popup = getReopeningPopup(survey);
                popup.html(message);
                app.ui.popup.open(popup);

                popup.find('[name="settings.auto_close"]').val(moment().add(1, 'week').format(app.lang.translate('moment_date_format').text));
                popup.find('[name="settings.auto_close"]').datetimepicker({
                    timepicker: false,
                    scrollInput: false,
                    format: app.lang.translate('moment_date_format').text,
                    formatDate: app.lang.translate('moment_date_format').text,
                    dayOfWeekStart: moment().localeData().firstDayOfWeek(),
                    minDate: moment.unix(survey.settings.auto_close_min),
                    maxDate: moment.unix(survey.settings.auto_close_max)
                });
            }
        });
    };

    /**
     * Search field
     */
    wizard.find('input[name="search_field"]').on('input', function() {

        let tab = wizard.find('section[data-component].is-active');
        search(tab, $(this).val());
        updateCounts(tab);

        let otherTab = wizard.find('section[data-component]:not(.is-active)');
        search(otherTab, $(this).val());
        updateCounts(otherTab);
    });

    /**
     * Filters (Show ALL/OPEN/DRAFT/CLOSED
     */
    wizard.onAction('filter', function() {
        applyFilter($(this), $(this).attr('data-filter'));
    });


    /**
     * Prevent accordion opening on click on actions
     */
    wizard.find('[data-action]').on('click', function(event){
        event.stopPropagation();
        event.preventDefault();
    });

    /**
     * Show more handling
     */
    wizard.onAction('show_more', function() {
        let tab = wizard.find('section[data-component].is-active');

        if(tab.attr('data-filter') === undefined) {
            applyFilter(tab.find('[data-action="filter"][data-filter=""]'), '');
        }

        // wizard.find('.survey-item-header.filtered.matching.hidden').css('display', 'block');
        tab.find('.survey-item-header.filtered.matching.hidden').slice(0,showMore).css('display','block');
        tab.find('.survey-item-header.filtered.matching.hidden').slice(0,showMore).removeClass('hidden');

        showMore += 10;

        if(tab.find('.survey-item-header.filtered.matching.hidden').length === 0) {
            $(this).hide();
        }
        Foundation.reInit(tab.find('[data-accordion]'));
    });

    /**
     * Initial
     */
    let currentTab = wizard.find('section[data-component].is-active');

    //Filtering all
    applyFilter(currentTab.find('[data-action="filter"][data-filter=""]'), '');

    currentTab.find('[data-accordion]').foundation('down', currentTab.find('.survey-item-header.focus').find('.accordion-content'), false);

    /**
     * Opening focused survey
     */
    // currentTab.find(".surveys-accordion").accordion('option','active', active);
    wizard.find(".survey-actions [data-action]").unbind('mousedown');

    wizard.find(".survey-actions [data-action]").on('keyup', function(e) {
        if (e.keyCode === 13) {
            var event = jQuery.Event( "mousedown" );
            event.which = 1;
            $(this).trigger(event);
        }
    });

    wizard.find(".survey-actions [data-action]").on('mousedown', function(e) {
        e.stopImmediatePropagation();

        let currentSurvey = $(this).parents('.survey-item-header');

        let surveyId = currentSurvey.attr('data-survey-id');
        let humanReadableId = currentSurvey.attr('data-human-readable-id');
        let surveyTitle = currentSurvey.find('.survey-title').contents().filter(function(){return this.nodeType === 3}).text();

        let handleGoTo = function(e, url) {
            switch(e.which) {
                case 2 :
                    window.open(url);
                    break;
                case 1 :
                    window.location.assign(url);
                    break;
                default :
                    break;
            }
        }

        switch($(this).attr('data-action')) {
            case 'reply' :
                handleGoTo(e, app.config.application_url +"survey/" +humanReadableId);
                break;
            case 'edit' :
                handleGoTo(e, app.config.application_url + "survey/update/" +surveyId);
                break;
            case 'show_results' :
                handleGoTo(e, app.config.application_url + "survey/results/" +surveyId);
                break;
            case 'duplicate' :
                if(e.which !== 1) return;

                app.ui.popup.confirm(app.lang.tr('confirm_survey_duplication') + ' :' + surveyTitle + '?', function() {
                    app.ui.toggleLoader(true);
                    app.client.post('/survey/' + surveyId + '/duplicate', {}, function (path, data) {

                        window.app.ui.notify('info', app.lang.tr('survey_duplicated'));

                        app.ui.toggleLoader(false);
                        //Redirecting to newly created survey
                        window.location.assign(app.config.application_url +"survey/update/" +data.data.id);
                    });
                });
                break;
            case 'delete' :
                if(e.which !== 1) return;

                let text = app.lang.tr('confirm_survey_deletion') + ' :' + surveyTitle + '?';
                text += '<div class="callout warning">' + app.lang.tr('confirm_survey_deletion_warning') + '</div>';
                app.ui.popup.confirm(text, function() {
                    app.ui.toggleLoader(true);
                    app.client.delete('/survey/' + surveyId, function (path, data) {
                        $('#' + currentSurvey.find("[data-toggle]").data('toggle')).remove();
                        currentSurvey.next('.survey-item-content').remove();
                        currentSurvey.remove();

                        app.ui.toggleLoader(false);
                        window.app.ui.notify('info', app.lang.tr('survey_deleted'));

                        updateCounts(wizard.find('section[data-component].is-active'));
                    });
                });
                break;
            case 'end_survey' :
                if(e.which !== 1) return;

                app.ui.popup.confirm(app.lang.tr('confirm_survey_ending') + ' :' + surveyTitle + '?', function() {
                    app.ui.toggleLoader(true);
                    app.client.post('/survey/' + surveyId + '/close', {}, function (path, data) {

                        app.ui.toggleLoader(false);

                        window.app.ui.notify('info', app.lang.tr('survey_closed'));

                        //Redirecting to updated view
                        window.location.replace(app.config.application_url + "survey/manage?focus_id=" + surveyId);
                    });
                });
                break;
            case 'send_invitations_reminder' :
                if(e.which !== 1) return;

                app.ui.popup.confirm(app.lang.tr('confirm_sending_invitations_reminder') + '?', function() {
                    app.ui.toggleLoader(true);
                    app.client.post('/survey/' + surveyId + '/send_invitations_reminder', {}, function (path, data) {

                        app.ui.toggleLoader(false);
                        window.app.ui.notify('info', app.lang.tr('invitations_sent'));
                    });
                });
                break;
            case 'reopen_survey' :
                if(e.which !== 1) {
                    return;
                }

                displayReopeningForm(surveyId);
                break;
        }
    });

});
