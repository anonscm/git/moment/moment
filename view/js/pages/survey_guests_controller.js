/**
 * Controller for guest step
 * @param wizard current wizard
 * @constructor
 */
class SurveyGuestsController {

    constructor(wizard) {
        this.stepGuests = wizard.find('section[data-step="guests"]');
        this.newGuestForm = wizard.find('[data-form="guest"]:not(.guest_edit)');
        this.wizard = wizard;

        $('.reply_access_info').each(function(){
            $(this).appendTo($(this).prev('label').find('span'));
        })

        this.initBindings();

        /**
         * Initialising model
         */
        this.initGuestsFromForm();
    }



    /*****************************************
     * Step Guests
     *****************************************/
    addGuestToModel(guest) {

        //we don't add guest twice
        let found = false;
        for (let i = 0, len = app.ui.survey.attributes.guests.length; i < len; i++) {
            if(app.ui.survey.attributes.guests[i].id === guest.id) {
                //found
                found = true;
                if(app.ui.survey.attributes.guests[i].email !== guest.email) {
                    app.ui.survey.attributes.guests[i].email = guest.email;

                    //It'a new guest
                    app.ui.survey.attributes.new_guests.push(guest.email);
                }
                break;
            }
        }

        //If guest doesn't exists
        if(!found) {
            //Pushing value
            app.ui.survey.attributes.guests.push(guest);

            //It'a new guest
            app.ui.survey.attributes.new_guests.push(guest.email);
        }

        //Return added count
        return (found)?0:1;
    }

    /**
     * Foreach guest in model we display a line
     */
    updateGuestList() {
        this.wizard.find('#guests_list').html('');
        for (let i = 0, len = app.ui.survey.attributes.guests.length; i < len; i++) {
            let label2 = $('<label />').attr({'data-index': i}).appendTo($('#guests_list'));

            $('<span />').attr('data-id', app.ui.survey.attributes.guests[i].id).addClass('guest_email')
                .html(app.ui.survey.attributes.guests[i].email)
                .appendTo(label2);

            $('<span />').addClass('guest_actions').addClass('float-right')
                .append('<span data-action="edit_guest" class="fa fa-pencil"></span>')
                .append('<span data-action="delete_guest" class="fa fa-trash-o"></span>')
                .appendTo(label2);
        }
    };

    /**
     * Parses input to get guests emails (Most likely one, but it's possible to have a few)
     * @param e
     */
    sendGuest($button) {

        let newEmailsCount = 0;
        let currentForm = $button.closest('[data-form]');

        let email = currentForm.find('input[name="guests.email"]');

        let emails = null;
        if ($button.data('emails') !== undefined) {
            emails = $button.data('emails');
        } else {
            //There may be more than one email
            emails = window.app.utilities.getEmailsFromString(email.val());
        }

        for (let i = 0, len = emails.length; i < len; i++) {
            //If only one email no need to increment id
            this.addGuestToModel({
                id: email.attr('id') + ((i === 0)?'':i),
                email: emails[i].email,
                name: emails[i].name
            });

            newEmailsCount++;
        }

        if(newEmailsCount > 0) {
            //Clearing input
            email.val('');

            //Notifying
            if(newEmailsCount > 1) {
                window.app.ui.notify('info', newEmailsCount + ' ' + app.lang.tr('guests_added'));
            }

            //Hiding form
            currentForm.hide();
            currentForm.find('.error_hint').hide();
            currentForm.find('[data-action="send_guest"]').addClass('disabled');
            this.wizard.find('[data-action="add_guest"]').show();
        }

        this.updateGuestList();
    };

    /**
     * Deletes guest from model
     */
    deleteGuest($button) {
        let email_index = $button.closest('label').attr('data-index');

        //Removing from new guest list
        let new_guest_email = app.ui.survey.attributes.new_guests.indexOf(app.ui.survey.attributes.guests[email_index].email);

        app.ui.survey.attributes.guests.splice(email_index, 1);
        app.ui.survey.attributes.new_guests.splice(new_guest_email, 1);

        this.updateGuestList();
    }

    /**
     * Edit guest creates a form for an existing guest
     * @param e
     */
    editGuest($button) {

        //Closing all guest_edit form
        this.wizard.find('[data-form="guest"].guest_edit').each(function() {
            let gItem = $(this).parents('label');

            //Hide edit button
            gItem.find('.guest_email').show();
            gItem.find('[data-action="edit_guest"]').show();

            $(this).hide();
        });


        let guestItem = $button.closest('label');

        //Hide edit button
        guestItem.find('.guest_email').hide();
        guestItem.find('[data-action="edit_guest"]').hide();
        guestItem.find('[data-action="delete_guest"]').hide();

        let form = null;
        if(guestItem.find('[data-form="guest"].guest_edit').length > 0) {
            form = guestItem.find('[data-form="guest"].guest_edit');
        } else {
            form = this.wizard.find('[data-form="guest"]:not(.guest_edit)').clone();
            form.addClass('guest_edit');
            form.find('.error_hint').remove();
            form.find('[data-action="send_guest"]').removeClass('disabled');

            let input = form.find('input[name="guests.email"]');
            input.val(guestItem.find('.guest_email').html());
            input.attr('id', guestItem.find('.guest_email').attr('data-id'));

            guestItem.find('.guest_email').after(form);

            //We just added email to the input, we must revalidate it
            input.trigger('submit');
        }

        form.show();
    }

    /**
     * Cancel action
     */
    cancelGuest($button) {
        let guestItem = $button.closest('[data-form]').parent();

        //Hide edit button
        guestItem.find('.guest_email').show();
        guestItem.find('[data-action="edit_guest"]').show();
        guestItem.find('[data-action="delete_guest"]').show();

        //Hiding form
        guestItem.find('[data-form]').hide();

        this.stepGuests.find('[data-action="add_guest"]').show();
    }

    /**
     * Inits model from displayed guests
     */
    initGuestsFromForm() {
        this.wizard.find('#guests_list .guest_email').each(function() {
            if(app.utilities.isValidEmailAddress($(this).html())) {
                app.ui.survey.attributes.guests.push({
                    id: $(this).attr('data-id'),
                    email: $(this).html()
                })
            }
        });
    };

    /**
     * Add bindings on field, dom
     */
    initBindings() {
        let that = this;

        /**
         * Displaying enable_anonymous_answer only when reply_access is set to 'opened_to_everyone'
         */
        this.stepGuests.on('change', 'select[name="settings.reply_access"]', function (e) {
            that.stepGuests.find('[name="settings.enable_anonymous_answer"]').parents('label').toggle($(this).val() === 'opened_to_everyone');
            that.stepGuests.find('.all-users-info').toggle($(this).val() === 'opened_to_everyone');

            that.stepGuests.find('.authenticated-guests-info').toggle($(this).val() === 'opened_to_guests');
            that.stepGuests.find('.authenticated-users-info').toggle($(this).val() === 'opened_to_authenticated');

        });
        this.stepGuests
            .find('[name="settings.enable_anonymous_answer"]')
            .parents('label')
            .toggle(this.stepGuests.find('select[name="settings.reply_access"]').val() === 'opened_to_everyone');
        this.stepGuests
            .find('.authenticated-guests-info')
            .toggle(this.stepGuests.find('select[name="settings.reply_access"]').val() === 'opened_to_guests');
        this.stepGuests
            .find('.authenticated-users-info')
            .toggle(this.stepGuests.find('select[name="settings.reply_access"]').val() === 'opened_to_guests');

        /**
         * Binding input on email field
         */
        this.stepGuests.on('input autocompleteclose', 'input[name="guests.email"]', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();

            let emails = window.app.utilities.getEmailsFromString($(this).val());

            $(this).parents('[data-form]').find('[data-action="send_guest"]').toggleClass('disabled',
                emails.length === 0
            );

            //Storing emails
            $(this).parents('[data-form]').find('[data-action="send_guest"]').data('emails', emails);
        });

        /**
         * Adding guest
         */
        this.wizard.onAction('add_guest', function () {
            that.wizard.find('[data-action="add_guest"]').hide();
            that.newGuestForm.find('input[name="guests.email"]').val('');
            that.newGuestForm.find('input[name="guests.email"]').removeUniqueId().uniqueId();
            that.newGuestForm.show();
            that.newGuestForm.find('[data-action="send_guest"]').addClass('disabled');
            that.newGuestForm.find('[data-action="send_guest"]').removeData('emails');
        });


        /**
         * Editing guest
         */
        this.stepGuests.on('click', '[data-action="edit_guest"], .guest_email', function() {
            that.editGuest($(this));
        });

        /**
         * Cancelling
         */
        this.stepGuests.on('click', '[data-action="cancel_guest"]', function() {
            that.cancelGuest($(this));
        });

        /**
         * Deleting guests
         */
        this.stepGuests.on('click', '[data-action="delete_guest"]',  function() {
            that.deleteGuest($(this));
        });

        /**
         * Submit guest
         */
        this.stepGuests.on('mousedown', '[data-action="send_guest"]', function() {
            that.sendGuest($(this));
        });
        this.stepGuests.on('keyup', '[data-form="guest"]', function (e) {
            if (e.keyCode === 13) {
                $(this).find('[data-action="send_guest"]').trigger('mousedown');
            }
        });

        /**
         * Binding when something has changed in the survey
         */
        app.ui.survey.binder.on('change', function() {
            that.updateGuestList();
        });

        /**
         * When wizard requires step validation
         */
        this.wizard.find('[data-step="guests"]').on('validate', function () {
            //auto_close date validation
            const autoCloseForm = app.forms.get('auto_close');
            autoCloseForm.getData().then(function() {
                that.stepGuests.trigger('valid');
            }, function(errors) {
                that.stepGuests.trigger('invalid', [[]]);
            });
        });
    }
}

