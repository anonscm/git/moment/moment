
Foundation.Abide.defaults.patterns['positive_integer'] = /^[+]?\d+$/;

class SurveyConstraintsController {

    /*****************************************
     * Step Questions - Constraints
     *****************************************/

    constructor(wizard) {
        this.stepQuestions = wizard.find('section[data-step="questions"]');
        this.allConstraints = [];

        this.initialized = false;

        this.initBindings();
    }


    /**
     * Initialize constraint list
     */
    initConstraintsList() {
        this.initialized = true;
        this.allConstraints = [];
        let that = this;
        $('.constraints_table .constraint_row').each(function() {
            let question_data = $(this).find('.question');
            let proposition_data = $(this).find('.proposition');
            let constraint_data = $(this).find('.constraint');
            let question = SurveyQuestionsController.findQuestion(question_data.attr('data-question-id'));
            let constraint = {
                question: {
                    id: (question.id !== undefined)?question.id:question.tmp_id,
                    index: question.index,
                    tmp_id: question.tmp_id
                },
                proposition: {
                    id: proposition_data.attr('data-proposition-id'),
                    label: proposition_data.text(),
                },
                type: constraint_data.attr('data-constraint-type'),
                label: constraint_data.text(),
                value: constraint_data.attr('data-constraint-value'),
                tmp_id: Date.now().toString(36) + Math.random().toString(36).substring(2)
            }
            that.allConstraints.push(constraint);
        });
    };

    /**
     * Add constraint
     * @param question
     * @param proposition
     * @param type
     * @param label
     * @param value
     */
    addConstraint(question, proposition, type, label, value) {
        let tmpID = Date.now().toString(36) + Math.random().toString(36).substring(2);
        const constraint = {
            question: {
                id: (question.id !== undefined)?question.id:question.tmp_id,
                index: question.index,
                tmp_id: question.tmp_id
            },
            proposition: proposition,
            type: type,
            label: label,
            value: value,
            tmp_id: tmpID
        }
        this.allConstraints.push(constraint);
        this.updateConstraintsList();
        app.event.trigger('constraint.added', constraint);

        return tmpID;
    }

    /**
     * get constraint
     * @param tmpID
     */
    getConstraint(tmpID) {
        for(let constraint of this.allConstraints) {
            if (constraint.tmp_id === tmpID)
                return constraint;
        }

        return undefined;
    }

    /**
     * get constraints
     * @param tmpID
     */
    getConstraintsForQuestion(questionID) {
        let constraints = [];
        for(let constraint of this.allConstraints) {
            if (constraint.question.id === questionID || constraint.question.tmp_id === questionID)
                constraints.push(constraint);
        }

        return constraints;
    }

    /**
     * Update constraint
     * @param tmpID
     * @param newConstraint
     */
    updateConstraint(tmpID, newConstraint) {
        let newConstraints = [];
        for(let constraint of this.allConstraints) {
            if(constraint.tmp_id === tmpID) {
                newConstraints.push(newConstraint);
            } else {
                newConstraints.push(constraint);
            }
        }
        this.allConstraints = newConstraints;
        app.event.trigger('constraint.updated', newConstraint);
        this.updateConstraintsList();
    }

    /**
     * Remove constraint
     * @param tmpID
     */
    removeConstraint(tmpID) {
        let newConstraints = [];
        let removedConstraint = undefined;
        for(let constraint of this.allConstraints) {
            if(constraint.tmp_id !== tmpID) {
                newConstraints.push(constraint);
            } else {
                removedConstraint = constraint;
            }
        }
        this.allConstraints = newConstraints;
        app.event.trigger('constraint.deleted', removedConstraint);
        this.updateConstraintsList();
    }

    /**
     * Updates constraint table
     */
    updateConstraintsList() {
        if(!this.initialized && this.allConstraints.length === 0) {
            //reads all constraints and init allConstraints
            this.initConstraintsList();
        }
        let constraints = this.allConstraints;
        let newConstraints = [];
        let constraintsTable = this.stepQuestions.find('table.constraints_table');

        constraintsTable.find('.constraint_row').remove();
        for(let i = 0; i < constraints.length; i++) {
            let question = SurveyQuestionsController.findQuestion(constraints[i].question.id);
            //If proposition has changed label, it removes the constraint
            if(question !== undefined) {
                // If question has a real id we must update the constraint
                if(question.id !== undefined && question.id !== constraints[i].question.id) {
                    constraints[i].question.id = question.id;
                }

                let proposition = SurveyQuestionsController.findProposition(question.id, constraints[i].proposition.id);
                if(proposition !== undefined
                    && proposition.label !== constraints[i].proposition.label) {
                    constraints[i].proposition.label = proposition.label;
                }

                let statement = question.title;
                if(statement === "") {
                    statement = app.lang.tr('question') + ' ' + (question.position + 1);
                }

                constraintsTable.find('.no_constraints_row').before(
                    '<tr class="constraint_row" data-constraint-index="' + i + '" data-constraint-id="' + constraints[i].tmp_id + '"><td>' + statement + '</td>' +
                    '<td>' + constraints[i].proposition.label + '</td>' +
                    '<td>' + constraints[i].label + '</td>' +
                    '<td>' + constraints[i].value + '</td>' +
                    '<td><button data-action="delete_constraint" class="fa fa-trash-o"></button></td>' +
                    '</tr>');

                newConstraints.push(constraints[i]);
            }
        }

        constraintsTable.find('.no_constraints_row').toggle(0 === newConstraints.length);

        this.allConstraints = newConstraints;
    }

    initBindings() {
        let that = this;

        /**
         * Binding adding constraint
         */
        this.stepQuestions.on('click','[data-action="add_constraint"]', function() {
            let form = that.stepQuestions.find('tr.constraints_form_row');

            //Updating model
            app.ui.controller.questionsController.sendQuestion();

            that.stepQuestions.find('.no_constraints_row').hide();

            form.show();
        });

        let validate =  function() {
            that.stepQuestions.trigger('update');
            that.stepQuestions.one('mouseleave','.advanced-options-container', function() {
                that.stepQuestions.one('mouseenter','.constraints_form_row', validate);
            });
        }

        /**
         * Binding changing question
         */
        this.stepQuestions.one('mouseenter','.constraints_form_row', validate);

        /**
         * Binding changing question
         */
        this.stepQuestions.on('change','select[name="question"]', function() {
            if(app.ui.survey.attributes.questions[$(this).val()] !== undefined) {
                //Selecting
                var propositionSelect = $(this).parents('.proposition_constraint_form').find('select[name="proposition"]');

                var formerQuestion = propositionSelect.attr('data-question-value');
                propositionSelect.attr('data-question-value', $(this).val());

                var formerValue = propositionSelect.val();

                //Clearing
                propositionSelect.html('');

                //First option is all_propositions
                propositionSelect.append('<option value="-1">' + app.lang.tr('all_propositions') + '</option>');

                for(var i=0; i < app.ui.survey.attributes.questions[$(this).val()].propositions.length; i++) {
                    var proposition = app.ui.survey.attributes.questions[$(this).val()].propositions[i];

                    if(proposition.options === undefined || proposition.options.constraints === undefined || proposition.options.constraints.length === 0) {
                        propositionSelect.append('<option value="' + i +'">' + SurveyQuestionsController.getPropositionLabel(proposition) + '</option>');
                    }
                }

                if(formerQuestion === $(this).val() && formerValue && propositionSelect.find('option[value="' + formerValue + '"]')) {
                    propositionSelect.val(formerValue);
                }
            }
        });

        /**
         * When constraint is valid
         */
        this.stepQuestions.on("formvalid.zf.abide", '.proposition_constraint_form', function(ev,frm) {
            frm.find('tr.constraints_form_row').hide();

            const questionIndex = frm.find('[name="question"]').val();
            const propositionIndex = frm.find('[name="proposition"]').val();
            const question = app.ui.survey.attributes.questions[questionIndex];
            question.index = questionIndex;
            let proposition;
            if(propositionIndex !== '-1') {
                proposition = question.propositions[propositionIndex];
            } else {
                proposition = {
                    id: -1,
                    label: frm.find('[name="proposition"] option[value="' + frm.find('[name="proposition"]').val() + '"]').text()
                }
            }

            that.addConstraint(
                question,
                {
                    id: (proposition.id !== undefined)?proposition.id:proposition.tmp_id,
                    index: propositionIndex,
                    label: proposition.label,
                },
                frm.find('[name="constraint_type"]').val(),
                frm.find('[name="constraint_type"] option[value="' + frm.find('[name="constraint_type"]').val() + '"]').text(),
                frm.find('[name="constraint_value"]').val()
            )

            frm.find('[name="constraint_value"]').val('');
        });

        /**
         * Saving the constraint
         */
        this.stepQuestions.on('click','[data-action="send_constraint"]', function() {
            $(this).parents('.proposition_constraint_form').foundation('validateForm');
        });

        /**
         * Deleting the constraint
         */
        this.stepQuestions.on('click','[data-action="delete_constraint"]', function() {
            that.removeConstraint($(this).parents('.constraint_row').attr('data-constraint-id'))
        });

        this.stepQuestions.on('change', 'input[name="question_limit_choice"]', function() {
            app.ui.controller.questionsController.sendQuestion();
            let $form = $(this).parents('[data-form="question"]');
            let $questionLimitChoiceNbContainer = $form.find('label[data-field-path="question_limit_choice_nb"]');
            let $questionLimitChoiceNbInput = $questionLimitChoiceNbContainer.find('input[type="number"][name="question_limit_choice_nb"]');

            $questionLimitChoiceNbInput
                .attr('disabled', !$(this).is(':checked'))
                .attr('required', $(this).is(':checked'));
            $questionLimitChoiceNbContainer.css('display', 'inline').toggleClass('hide', !$(this).is(':checked'));

            if($(this).is(':checked')) {
                let question = SurveyQuestionsController.findQuestion($form.attr('id'));
                if (question !== undefined && $form.attr('data-lc-constraint-id') === undefined) {
                    that.addConstraint(
                        question,
                        {id: -1, label: app.lang.tr('all_propositions').out()},
                        'limit_choice',
                        app.lang.tr('limit_choice').out(),
                        $questionLimitChoiceNbInput.val()
                    );
                }
            } else {
                if ($form.attr('data-lc-constraint-id') !== undefined) {
                    that.removeConstraint($form.attr('data-lc-constraint-id'));
                    $(this).parents('[data-form="question"]').removeAttr('data-lc-constraint-id');
                }
            }
        });

        this.stepQuestions.on('input', 'input[name="question_limit_choice_nb"]', function() {
            if(isNaN($(this).val()) || $(this).val() < 1)
                return;

            let constraintId = $(this).parents('[data-form="question"]').attr('data-lc-constraint-id');
            let constraint = that.getConstraint(constraintId);
            if(constraint !== undefined) {
                constraint.value = $(this).val();
                that.updateConstraint(constraintId, constraint);
            } else {
                $(this).parents('[data-form="question"]').removeAttr('data-lc-constraint-id');
            }
        });

        let refreshQuestionLimitChoice = function(constraint) {
            if(constraint.hasOwnProperty('question') && constraint.question.hasOwnProperty('tmp_id')) {
                let $form = $('[data-form="question"]#' + constraint.question.tmp_id);
                if($form === undefined)
                    return;
                $form.removeAttr('data-lc-constraint-id');
                $form.find('label[data-field-path="question_limit_choice"]').closest('.option').removeClass('hide').show();
                $form.find('[name="question_limit_choice"]').prop('checked', false).change();
                $form.find('[name="question_limit_choice_nb"]').val(1);
                $form.find('.option.some_constraints_defined').removeClass('hide').hide();

                let constraints = that.getConstraintsForQuestion(constraint.question.tmp_id);
                for(let questionConstraint of constraints) {
                    if(questionConstraint.type !== 'limit_choice')
                        continue;

                    if(questionConstraint.proposition.id !== -1) {
                        $form.find('label[data-field-path="question_limit_choice"]').closest('.option').hide();
                        $form.find('[name="question_limit_choice"]').prop('checked', false).change();
                        $form.find('.option.some_constraints_defined').show();
                        break;
                    }

                    if(questionConstraint.proposition.id === -1) {
                        $form.attr('data-lc-constraint-id', questionConstraint.tmp_id);
                        $form.find('[name="question_limit_choice_nb"]').val(questionConstraint.value);
                        $form.find('[name="question_limit_choice"]').prop('checked', true).change();
                        break;
                    }
                }
            }
        }

        app.event.on('constraint.added', function(e, constraint) {
            refreshQuestionLimitChoice(constraint);
        });
        app.event.on('constraint.deleted', function(e, constraint) {
            refreshQuestionLimitChoice(constraint);
        });
    }
}

