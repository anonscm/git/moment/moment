app.event.on('started', function() {
    var wizard = $('[data-component="survey-wizard"]');
    if(!wizard.length) return;

    /**
     * When we are updating/creating a survey we must warn on leaving
     */
    app.ui.warnOnLeaving();

    var wizard = new app.view(wizard);

    /**
     * Storing steps
     */
    wizard.steps = wizard.find('[data-container="steps"] [data-step]');

    /**
     * Storing actions
     * @type {{previous: *, next: *, cancel: *, preview: *, save: *}}
     */
    wizard.actions = {
        previous : wizard.find('[data-action="previous"]'),
        next : wizard.find('[data-action="next"]'),
        cancel : wizard.find('[data-action="cancel_creation"], [data-action="cancel_edition"]'),
        preview : wizard.find('[data-action="preview_survey"]'),
        save : wizard.find('[data-action="save_survey"]'),
        save_draft : wizard.find('[data-action="save_draft"]'),
        update : wizard.find('[data-action="update_survey"]')
    }

    /**
     * Settings (Type of easing between steps ...)
     * @type {{animation: {type: string, duration: number}}}
     */
    wizard.settings = {
        animation : {
            type : 'fade',
            duration : 300
        }
    }
    
    wizard.steps.hide();

    // Storing current survey time zone
    wizard.settings.time_zone = wizard.attr('data-survey-timezone') !== undefined  ? wizard.attr('data-survey-timezone') : moment.tz.guess();

    //Guessing time zone
    wizard.find('[name="settings.time_zone"]').val(moment.tz.guess());
    wizard.find('[name="settings.time_zone"]').trigger('change');

    /**
     * Update navigation button (in nav bar and "Next" and "Previous")
     */
    var updateNavBar = function() {
        //Disable every button where previous is forbidden or waiting
        wizard.find('nav [data-action="goto_step"]').each(function() {
            var step = wizard.find('[data-container="steps"] [data-step="' + $(this).attr('data-step') + '"]');

            if(step.length) {
                var prevStep = step.prevAll('section[data-step]:first');
                var prevGotoStep = $(this).prevAll('nav [data-action="goto_step"]:first');
                if(prevStep.length > 0) {
                    $(this).toggleClass('forbidden', ((prevStep.attr('data-status') === 'waiting') || (prevGotoStep.hasClass('forbidden'))));
                }

            }
        });

        //Disabling "Next" or "Previous" when needed
        wizard.actions.next.removeClass('forbidden');
        wizard.actions.next.addClass(function() {
            return (wizard.find('nav [data-action="goto_step"].current').nextAll('nav [data-action="goto_step"]:first.forbidden').length > 0)?'forbidden':'';
        });
        wizard.actions.save_draft.removeClass('forbidden');
        wizard.actions.save_draft.addClass(function() {
            return (wizard.find('nav [data-action="goto_step"].current').nextAll('nav [data-action="goto_step"]:first.forbidden').length > 0)?'forbidden':'';
        });
        wizard.actions.previous.removeClass('forbidden');
        wizard.actions.previous.addClass(function() {
            return (wizard.find('nav [data-action="goto_step"].current').prevAll('nav [data-action="goto_step"]:first.forbidden').length > 0)?'forbidden':'';
        });

    }

    /**
     * Init update counter (storing old value to be compared later)
     */
    var initUpdatesCount = function() {
        app.ui.inputCount = 0;
        wizard.find('input, textarea, select').each(function() {
            if ($(this).attr('type') === 'checkbox') {
                $(this).data('oldValue', $(this).is(':checked') ? 1 : 0);
            } else {
                $(this).data('oldValue', $(this).val());
            }
            app.ui.inputCount++;
        });
    }

    /**
     * Get estimated update count
     */
    var getUpdatesCount = function() {
        var updatesCount = 0;
        var updates = [];
        var inputCount = 0;
        $('input, select, textarea').each(function() {
            var val = $(this).val();
            //If type  checkbox, we need to know if checked
            if($(this).attr('type') === 'checkbox') {
                val = ($(this).is(':checked')?1:0);
            }

            if(($(this).data('oldValue') !== undefined && val !== $(this).data('oldValue'))
                || ($(this).data('oldValue') === undefined && val !== undefined && val !== '')) {
                updatesCount++;
            }
            inputCount++;
        });
        if(app.ui.inputCount > inputCount) {
            updatesCount += app.ui.inputCount - inputCount;
        }

        return updatesCount;
    }

    /**
     * Survey init with wizard
     */
    var initSurvey = function() {

        var id = wizard.attr('data-survey-id');

        app.ui.survey = new Survey(id);
  
        //Initializing data binding
        wizard.find('input, textarea, select').each(function() {
            var noBind = $(this).attr('data-no-binding');
            if(typeof noBind === typeof undefined) {
                $(this).attr('data-bind-' + id, function() { return $(this).attr('name')});
                
                //Initialize value
                $(this).trigger("init");
            }
        });


        //Everytime a step status change -> update of the navigation bar
        app.ui.survey.binder.on('statusChanged', updateNavBar);

        if (!('controller' in window.app.ui))
            window.app.ui.controller = {};

        //Creating step controllers
        app.ui.controller = {
            generalController: new SurveyGeneralController(wizard),
            guestsController: new SurveyGuestsController(wizard),
            questionsController: new SurveyQuestionsController(wizard),
            constraintsController: new SurveyConstraintsController(wizard)
        }

        if(id.length > 0) {
            $(window).on('timeLocalized', function() {
                wizard.steps.trigger('validate');

                //Storing old values
                initUpdatesCount();
            });
        }

        //Initializing navigation bar state
        updateNavBar();
    };

    /**
     * Display step
     */
    var gotoStep = function(step) {
        var now = wizard.find('nav [data-step="' + step + '"]');
        var prev = wizard.find('nav .current');

        now.addClass('current info');

        if(prev.length) {
            if(prev.attr('data-step') === step) return;
            prev.removeClass('current info');
            var opts = {direction: (now.index() > prev.index()) ? 'left' : 'right'};
            var opts_reverse = {direction: (now.index() <= prev.index()) ? 'left' : 'right'};
            wizard.find('[data-container="steps"] [data-step="' + prev.attr('data-step') + '"]')
                .hide(wizard.settings.animation.type, opts, wizard.settings.animation.duration);

            wizard.find('[data-container="steps"] [data-step="' + step + '"]')
                .delay(wizard.settings.animation.duration)
                .show(wizard.settings.animation.type, opts_reverse, updateNavBar);
        } else {
            wizard.find('[data-container="steps"] [data-step="' + step + '"]').delay(10).show(0);
        }

        var firstStepHidden = {display: now.is('[data-step]:first-child') ? 'none' : 'inline-block'};
        var firstStepDisplay = {display: now.is('[data-step]:first-child') ? 'inline-block' : 'none'};
        var lastStepHidden = {display: now.is('[data-step]:last-child') ? 'none' : 'inline-block'};
        var lastStepDisplay = {display: now.is('[data-step]:last-child') ? 'inline-block' : 'none'};

        wizard.actions.previous.css(firstStepHidden);
        wizard.actions.next.css(lastStepHidden);
        wizard.actions.cancel.css(firstStepDisplay);
        wizard.actions.preview.css(lastStepDisplay);
        //If create page (display save button only on last step)
        if(wizard.actions.save.length > 0) {
            wizard.actions.save.css(lastStepDisplay);
        }
        //If update page, highlight update button only on last step
        if (wizard.actions.update.length > 0) {
            wizard.actions.update.toggleClass('highlight', now.is('[data-step]:last-child'));
        }
        //If update draft, show update button only on last step
        if(wizard.actions.save_draft.length > 0) {
            wizard.actions.update.css(lastStepDisplay);
        }
    };

    /**
     * Notify all alerts as a toast
     * @param alerts
     */
    var notifyAlerts = function(alerts) {
        if(typeof alerts !== "undefined") {
            for (var i = 0; i < alerts.length; ++i) {
                if (typeof alerts[i] === 'string' || alerts[i] instanceof String) {
                    window.app.ui.notify('error', alerts[i]);
                } else if (alerts[i].message !== undefined) {
                    window.app.ui.notify('error', alerts[i].message);
                }
            }
        }
    };

    /**
     * Handle invalid state
     */
    var handleInvalidState = function(e, alerts) {
        if(e.delegateTarget === typeof undefined)
            return;

        var step = $(e.delegateTarget);

        if(step.attr('data-step') === undefined)
            return;

        //Hide loader
        app.ui.toggleLoader(false);

        gotoStep(step.attr('data-step'));

        notifyAlerts(alerts);

        wizard.steps.unbind('valid');
        wizard.steps.unbind('invalid');
    }
    
    /**
     * Handle disconnection (app.ui.survey must be up to date to be saved in localstorage)
     */
    var handleDisconnection = function() {
        if(app.utilities.isLocalStorageAvailable()) {
            //Storing current survey in local storage
            localStorage.setItem(
                'pending_survey',
                JSON.stringify(app.ui.survey)
            );

            app.ui.popup.confirm(app.lang.tr('connection_lost_try_to_reconnect'), function () {

                app.ui.disableWarnOnLeaving();
                window.location.href = app.config.logon_url;
            });
        }
    }

    /**
     * Handle POST survey error
     */
    var handleSurveyPostError = function(error) {
        var reason = '';
        if(error.details.reason !== undefined) {
            reason = error.details.reason;
            error.details.reason = app.lang.tr(reason);
        }

        if(reason === 'requires_authentication' && app.utilities.isLocalStorageAvailable()) {
            handleDisconnection();
        } else {
            app.ui.error(error);
        }
    }

    /**
     * Send the draft
     */
    var sendDraft = function() {
        // This is a draft, no validation needed
        app.ui.survey.attributes.is_draft = 1;
        app.client.post('/survey', app.ui.survey.attributes, function (path, data) {
            window.app.ui.notify('info', app.lang.tr('draft_saved'));

            //We keed a knowledge of new guests
            var new_guests = app.ui.survey.attributes.new_guests;

            app.ui.survey.attributes = data.data;

            app.ui.survey.attributes.new_guests = new_guests;

            app.event.trigger('draft_saved');

            wizard.steps.trigger('update');
        }, {
            //Handle error on post (lost authentication...)
            error: handleSurveyPostError
        });
    }

    /**
     * save draft
     */
    var saveDraft = function() {

        if(wizard.actions.save_draft.hasClass('forbidden')) return;

        //Show loader
        app.ui.toggleLoader(true, app.lang.tr('saving_draft').out());

        if(app.ui.survey.attributes.title.length === 0) {
            window.app.ui.notify('error', app.lang.tr('please_set_title'));
            gotoStep('general');
        } else {
            const autoCloseForm = app.forms.get('auto_close');
            autoCloseForm.getData().then(function() {
                sendDraft();
            }, function() {
                gotoStep('guests');
            });

        }

        //Hide loader
        app.ui.toggleLoader(false);
    }

    /**
     * Saved callback
     */
    var surveySavedCallBack = function (data) {
        //Hide loader
        app.ui.toggleLoader(false);

        window.app.ui.notify('success', app.lang.tr('survey_saved'));

        app.ui.disableWarnOnLeaving();

        if($('#surveySaved').length > 0) {

            //Updating reply path
            $('[name="survey_share_link"]').val(data.data.path);
            $('[data-share-link]').attr('data-content', data.data.path);


            $('#surveySaved').find('[data-action="goto-manage"]').on('click', function () {
                window.location.replace(app.config.application_url + "survey/manage?focus_id=" + data.data.id);
            });

            //Automatic redirect in n seconds
            var timeBeforeRedirection = 8000;
            $('#surveySaved').find('.info').show();
            $('#surveySaved').find('.counter').text(timeBeforeRedirection / 1000);
            var updateCounter = setInterval(function () {
                timeBeforeRedirection -= 1000;
                $('#surveySaved').find('.counter').text(timeBeforeRedirection / 1000);
            }, 1000)

            setTimeout(function () {
                clearInterval(updateCounter);
                window.location.replace(app.config.application_url + "survey/manage?focus_id=" + data.data.id)
            }, timeBeforeRedirection);

            //Showing modal
            $('#surveySaved').foundation('open');
        } else {
            window.location.replace(app.config.application_url + "survey/manage?focus_id=" + data.data.id)
        }


    }

    /**
     * Add bindings on field, dom
     */
    var initBindings = function() {

        /******************************************
         * Showing wizard when everything is ready
         ******************************************/
        $(window).on('adapted', function() {
            wizard.find('[data-container="steps"]').css({opacity: 0, visibility: "visible"}).animate({opacity: 1}, 50);
        });

        /******************************************
         * Navigation in the wizard
         ******************************************/
        //Binding nav -> goToStep
        wizard.onAction('goto_step', function() {
            //Validating before clicking going to step current must be valid
            var current = wizard.find('nav .current').attr('data-step');
            var destination = $(this);

            wizard.find('[data-container="steps"] [data-step="' + current + '"]').one('valid invalid', function(e) {
                if(!destination.hasClass('forbidden')) {
                    gotoStep(destination.attr('data-step'));
                }
                //We stop listening for events
                $(this).off('valid invalid');
            });

            wizard.find('[data-container="steps"] [data-step="' + current + '"]').trigger('validate');
        });

        wizard.onAction('previous', function() {
            gotoStep(wizard.find('nav .current').prev('[data-step]').attr('data-step'));
        });

        wizard.onAction('next', function() {

            //Validating before clicking next
            var current = wizard.find('nav .current').attr('data-step');

            wizard.find('[data-container="steps"] [data-step="' + current + '"]').one('valid invalid', function(e, alerts) {

                //Show No question found if necessary
                wizard.find('[data-container="steps"] [data-step="' + current + '"] .no-question-found').toggleClass('hidden', app.ui.survey.attributes.questions.length !== 0);

                if(!wizard.find('nav .current').next('[data-step]').hasClass('forbidden')) {
                    gotoStep(wizard.find('nav .current').next('[data-step]').attr('data-step'));
                }

                // Display errors when invalid
                if(e.type === 'invalid') {
                    handleInvalidState(e, alerts);
                }

                //We stop listening for events
                $(this).off('valid invalid');
            });

            wizard.find('[data-container="steps"] [data-step="' + current + '"]').trigger('validate');
        });

        /**
         * Binding navigation with arrow keys
         */
        $(document).keydown(function(e) {
            //Only when nothing is focused
            if ($('*:focus').length === 0) {
                switch (e.which) {
                    case 37: // left
                        if (wizard.find('nav .current').prev('[data-step]').length > 0) {
                            gotoStep(wizard.find('nav .current').prev('[data-step]').attr('data-step'));
                        }
                        break;
                    case 39: // right
                        if (wizard.find('nav .current').next('[data-step]').length > 0
                            && !wizard.find('nav .current').next('[data-step]').hasClass('forbidden')) {
                            gotoStep(wizard.find('nav .current').next('[data-step]').attr('data-step'));
                        }
                        break;
                    default:
                        return; // exit this handler for other keys
                }
                e.preventDefault(); // prevent the default action (scroll / move caret)
            }
        });


        /**
         * Toggles the correct advanced options
         */
        wizard.onAction('toggle-advanced-options', function() {
            $(this).parents('[data-step]').find('.advanced-options').toggle();
            $(this).find('i').toggleClass('fa-caret-right');
            $(this).find('i').toggleClass('fa-caret-down');
        });

        /**
         * Open the correct advanced options
         */
        $(document).on('click', '[data-action="open-advanced-options"]', function(e) {
            e.stopImmediatePropagation();
            e.preventDefault();
            let step = $(this).parents('[data-step]');
            step.find('.advanced-options').show();
            step.find('[data-action="toggle-advanced-options"] > i').removeClass('fa-caret-right');
            step.find('[data-action="toggle-advanced-options"] > i').addClass('fa-caret-down');
            $('html,body').animate({ scrollTop: parseInt(step.find('.advanced-options').offset().top) }, 400);
            step.find('.advanced-options').get(0).focus();
        });


        /**********************************************
         * Binding actions for survey (save/update/savedraft ...)
         **********************************************/
        //Binding save draft
        wizard.onAction('save_draft', function() {


            var current = wizard.find('nav .current').attr('data-step');

            wizard.find('[data-container="steps"] [data-step="' + current + '"]').one('valid invalid', function(e, alerts) {

                //Show No question found if necessary
                wizard.find('[data-container="steps"] [data-step="' + current + '"] .no-question-found').toggleClass('hidden', app.ui.survey.attributes.questions.length !== 0);

                saveDraft();

                // Display errors when invalid
                if(e.type === 'invalid') {
                    handleInvalidState(e, alerts);
                }

                //We stop listening for events
                $(this).off('valid invalid');
            });

            wizard.find('[data-container="steps"] [data-step="' + current + '"]').trigger('validate');
        });

        //Binding preview survey (Basically saves a draft and show)
        wizard.onAction('preview_survey', function() {

            //Show loader
            app.ui.toggleLoader(true, app.lang.tr('preparing_preview').out());

            //Collecting every steps data
            wizard.steps.trigger('validate');

            app.event.on('draft_saved', function() {
                //Draft is saved we can now go on preview page
                app.ui.disableWarnOnLeaving();
                window.location.replace(app.config.application_url + "survey/preview-" + app.ui.survey.attributes.id);
            });
            //Saving draft
            saveDraft();

            //Hide loader
            app.ui.toggleLoader(false);

        });

        //Binding survey creation
        wizard.onAction('save_survey', function() {

            // check if user accepted terms (if present it's mandatory)
            if(!window.app.utilities.hasAcceptedTerms())
                return;

            //Show loader
            app.ui.toggleLoader(true, app.lang.tr('saving_survey').out());

            var stepsCount = 0;
            var stepsValidState = [];
            wizard.steps.each(function() {
                stepsCount++;

                $(this).one('valid', function(e) {
                    stepsValidState.push($(this).attr('data-step'));

                    //Every steps are valid
                    if(stepsCount === stepsValidState.length) {
                        wizard.steps.unbind('valid');
                        wizard.steps.unbind('invalid');

                        //Survey must not be known as draft anymore
                        app.ui.survey.attributes.is_draft = 0;

                        app.client.post('/survey', app.ui.survey.attributes,
                            function(path, data) { surveySavedCallBack(data); },
                            {
                                //Handle error on post (lost authentication...)
                                error: handleSurveyPostError
                            }
                        );
                    }
                });

                $(this).one('invalid', handleInvalidState);
            });

            //Collecting every steps data
            wizard.steps.trigger('validate');
        });

        //Binding survey edition
        wizard.onAction('update_survey', function() {

            // check if user accepted terms (if present it's mandatory)
            if(!window.app.utilities.hasAcceptedTerms())
                return;

            var stepsCount = 0;
            var stepsValidState = [];
            wizard.steps.each(function() {
                stepsCount++;

                $(this).one('valid', function() {
                    stepsValidState.push($(this).attr('data-step'));

                    //Every steps are valid
                    if(stepsCount === stepsValidState.length) {
                        wizard.steps.unbind('valid');
                        wizard.steps.unbind('invalid');

                        //Get update count
                        var updatesCount = getUpdatesCount();

                        //If survey has participants, owner may want to warn them about modification
                        var hasParticipants = $('[data-has-participants]').length > 0;

                        var sendSurvey = function() {
                            //Show loader
                            app.ui.toggleLoader(true, app.lang.tr('saving_update').out());

                            //Survey must not be known as draft anymore
                            app.ui.survey.attributes.is_draft = 0;

                            app.client.put('/survey/' + app.ui.survey.attributes.id,
                                app.ui.survey.attributes,
                                surveySavedCallBack,
                                {
                                    //Handle error on put (lost authentication...)
                                    error: handleSurveyPostError
                                }
                            );
                        }

                        //If we have new guests or updates and existing participants
                        if(app.ui.survey.attributes.new_guests.length > 0
                            || (hasParticipants && updatesCount > 0)
                        ) {
                            // 3 Comfirm (updates and new guests) (updates) (new_guests)
                            let confirm = (hasParticipants && updatesCount > 0)?
                                (
                                    app.ui.survey.attributes.new_guests.length > 0?
                                    'send_update_notification_and_invitation_to_new_guests'
                                    :'send_update_notification'
                                )
                                :'send_invitation_to_new_guests';

                            app.ui.popup.confirm(app.lang.tr(confirm),
                                function() { //If yes
                                    if(confirm === 'send_update_notification_and_invitation_to_new_guests'
                                        || confirm === 'send_invitation_to_new_guests') {
                                        app.ui.survey.attributes.notify_new_guests = true;
                                    }
                                    if(confirm === 'send_update_notification_and_invitation_to_new_guests'
                                        || confirm === 'send_update_notification') {
                                        app.ui.survey.attributes.notify_update = true;
                                    }

                                    sendSurvey();
                                }, function() { //If no
                                    if(confirm === 'send_update_notification_and_invitation_to_new_guests'
                                        || confirm === 'send_invitation_to_new_guests') {
                                        app.ui.survey.attributes.notify_new_guests = false;
                                    }
                                    if(confirm === 'send_update_notification_and_invitation_to_new_guests'
                                        || confirm === 'send_update_notification') {
                                        app.ui.survey.attributes.notify_update = false;
                                    }

                                    sendSurvey();
                                }
                            );
                        } else {
                            sendSurvey();
                        }

                    }
                });

                $(this).one('invalid', handleInvalidState);
            });

            //Collecting every steps data
            wizard.steps.trigger('validate');
        });


        //Binding cancel edition
        wizard.onAction('cancel_edition', function() {
            //The only normal way to be in edition is to pass through manage screen
            window.location.assign(app.config.application_url + 'survey/manage');
        });

        //Binding cancel creation
        wizard.onAction('cancel_creation', function() {
            window.location.assign(app.config.application_url);
        });

        /******************************************
         * Saving draft when clicking on link that requires saving
         ******************************************/
        $('a[target!="_blank"].must_save_draft').on('click', function(e) {
            e.preventDefault();

            var href = $(this).attr('href');
            var popup = app.ui.popup.create(app.lang.tr('leaving_survey_creation_form'),
                {
                    ok: {
                        type: 'highlight',
                        onclick: function () {
                            wizard.actions.save_draft.removeClass('forbidden');
                            //Collecting every steps data
                            wizard.steps.trigger('validate');
                            saveDraft();
                            app.ui.disableWarnOnLeaving();
                            window.location.assign(href);
                        }
                    }
                });
            var message = '';
            //Link can provide a specific message to show in popup
            if($(this).attr('data-save-draft-message').length > 0) {
                message += $(this).attr('data-save-draft-message')+'<br/>';
            }
            message += app.lang.tr('survey_data_will_be_saved_in_draft');
            popup.html(message);
            app.ui.popup.open(popup);
        });

        $(document).on('app.session_ended', function() {
            handleDisconnection();
        });

    }

    /**
     * Initial step
     */
    //Displaying general step first
    gotoStep('general');

    /**
     * Survey mapping with wizard
     */
    initSurvey();


    //If an other step is required
    if(wizard.find('[name="initialStep"]').val() !== 'general') {
        gotoStep(wizard.find('[name="initialStep"]').val());
    }

    /**
     * Init bindings
     */
    initBindings();

    /**
     * Initial state of advanced option (can be opened on ready)
     */
    wizard.find('[data-action="toggle-advanced-options"]').each(function() {
        if($(this).attr('data-initial-state') === 'open') {
            $(this).trigger('click');
        }
    });

});
