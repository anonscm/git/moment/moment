/**
 * This file is part of the ApplicationBase project.
 * 2016
 */

class Survey {

    constructor(uid) {
        this.binder = new window.app.ui.binder(uid);
        this.attributes = {
            id: uid,
            title: '',
            place: '',
            description: '',
            created: '',
            updated: '',
            closed: '',
            settings: {},
            questions: [],
            owners: [],
            guests: [],
            new_guests: [],
            notify_new_guests: false,
            notify_update: false
        };

        // Subscribe to the PubSub
        this.binder.on(uid + ":change", (function (evt, attr_name, new_val, real_val, initiator) {
            if (initiator !== this) {
                this.set(attr_name, real_val);
            }
        }).bind(this));
    }

    get(attr_name) {
        return this.attributes[attr_name];
    }

    // The attribute setter publish changes using the DataBinder PubSub
    set(attr_name, val) {

        var object_name = attr_name;
        var object_attr_name = undefined;
        var object_index = -1;

        if (object_name.lastIndexOf('.') !== -1) {
            object_name = attr_name.substr(0, attr_name.indexOf('.'));
            object_attr_name = attr_name.substr(attr_name.indexOf('.') + 1);
        }
        if (object_name.lastIndexOf('[') !== -1) {
            object_index = object_name.substring(object_name.lastIndexOf('[') + 1, object_name.lastIndexOf(']'));
            object_name = object_name.substring(0, object_name.lastIndexOf('['));
        }

        if (this.attributes[object_name] instanceof Array) {
            if (object_index === -1) {
                if (val instanceof Array) {
                    this.attributes[object_name] = val;
                } else {
                    //Adding a new item to the array
                    if (object_attr_name === undefined) {
                        this.attributes[object_name].push(val);
                    } else {
                        //We create a new item with the property named
                        eval("this.attributes[ object_name ].push({" + object_attr_name + ":val})");
                    }
                }
            } else {
                //Updating an item in the array
                if (object_attr_name === undefined) {
                    this.attributes[object_name][object_index] = val;
                } else {
                    if (this.attributes[object_name][object_index] === undefined) {
                        this.attributes[object_name][object_index] = {};
                    }

                    //We create a new item with the property named
                    eval("this.attributes[ object_name ][object_index]." + object_attr_name + " = val");
                }

            }
        } else if (typeof this.attributes[object_name] === 'object'
            && object_attr_name !== undefined) {
            eval("this.attributes[ object_name ]." + object_attr_name + " = val");
        } else {
            this.attributes[object_name] = val;
        }

        this.binder.trigger("change", [object_name, val, this]);
    }
}

