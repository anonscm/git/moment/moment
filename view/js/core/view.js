/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * Provides view base constructor shortcut
 * 
 * @param {string|object} src template identifier or existing node
 * @param {object} parent view to use as parent
 */
window.app.view = function(src, parent) {
    return new app.view._base(src, parent);
};

/**
 * Define new view
 * 
 * @param {string} name
 * @param {function} init
 */
window.app.view.define = function(name, init) {
    var view = function() {
        app.view._base.call(this, name);
        if(init) init.apply(this, arguments);
    };
    
    for(var k in app.view._base.prototype) {
        if(k.match(/^(constructor)$/)) continue;
        view.prototype[k] = app.view._base.prototype[k];
    }
    
    view.prototype.constructor = view;
    
    // Register the view
    this[name] = view;
};

/**
 * Provides base view constructor
 * 
 * @param {string|object} src template identifier or existing node
 * @param {object} parent view to use as parent
 */
window.app.view._base = function(src, parent) {
    this.id = '';
    
    if(src.jquery) { // Existing node
        if(src.is('[data-template]')) { // Already a template
            this.tpl = src;
            this.id = src.attr('data-template');
            this.node = src.clone(true).removeAttr('data-template');
            
        } else { // Basic node
            this.tpl = null;
            this.node = src;
        }
        
    } else { // Clone template from id
        this.id = src;
        var sel = '[data-template="' + src + '"]';
        
        if(parent) {
            if(parent.node) {
                this.tpl = parent.node.find(sel + ':not([data-template] [data-template])');
                
            } else {
                if(!parent.jquery)
                    parent = $(parent);
                
                this.tpl = parent.find(sel);
            }
            
        } else {
            this.tpl = $('body > ' + sel);
        }
        
        if(!this.tpl.length)
            throw 'template_not_found';
        
        this.node = this.tpl.clone(true).removeAttr('data-template');
    }
    
    this.parent = parent;
    
    if(!parent) this._data = {};
    
    if(this.tpl) // Not section
        this.node.attr({'data-view': this.id}).data('view', this);
};

/**
 * Get parent view (ignores sections)
 * 
 * @return {object} view
 */
window.app.view._base.prototype.parentView = function() {
    return this.node.parent().closest('[data-view]').data('view');
};

/**
 * String casting
 * 
 * @return {string}
 */
window.app.view._base.prototype.toString = function() {
    var ids = [], v = this;
    
    do {
        var id = '?';
        if(v.tpl) {
            id = 'tpl(' + v.tpl.attr('data-template') + ')';
            
        } else if(v.node.attr('data-section')) {
            id = 'section(' + v.node.attr('data-section') + ')';
        }
        
        ids.unshift(id);
        
        v = v.parent;
    } while(v);
    
    return 'view:' + ids.join('/');
};

/**
 * Set properties placeholders values
 * 
 * @param {string|object} p property name or object listing properties
 * @param {any} v property value (if previous is a propert name)
 * 
 * @return {object} the view
 */
window.app.view._base.prototype.property = function(p, v) {
    var k;
    if(typeof p === 'string') {
        k = p;
        p = {};
        p[k] = v;
    }
    
    var not = ['[data-template] [data-property]'];
    
    if(this.node.attr('data-section')) {
        var sid = this.node.attr('data-section');
        not.push('[data-section="' + sid + '"] [data-section] [data-property]');
        
    } else {
        not.push('[data-section] [data-property]');
    }
    
    for(k in p) // Replace in properties of the current view, not in subtemplates
        this.node.find('[data-property="' + k + '"]:not(' + not.join(', ') + ')').text(p[k]);
    
    return this;
};

/**
 * Get sub section as a view
 * 
 * @param {string} section_id
 * 
 * @return {object} view
 */
window.app.view._base.prototype.section = function(section_id) {
    var section_node = this.node.find('[data-section="' + section_id + '"]');
    
    if(!section_node.length)
        throw 'section_not_found';
    
    return new app.view._base(section_node, this);
};

/**
 * Create sub view from sub template
 * 
 * @param {string} tpl_id
 * 
 * @return {object} view
 */
window.app.view._base.prototype.subView = function(tpl_id) {
    return new app.view._base(tpl_id, this);
};

/**
 * Bind listener to click on action sub-node
 * 
 * @param {string} action
 * @param {function} handler
 * 
 * @return {object} the view
 */
window.app.view._base.prototype.onAction = function(action, handler) {
    action = action.split(':');
    if(action.length < 2) action.push('click');
    
    var not = '[data-template] [data-action]';
    this.node.find('[data-action="' + action[0] + '"]:not(' + not + ')').on(action[1], function(event) {
        var view = $(this).closest('[data-view]').data('view');
        
        if(typeof handler === 'function') {
            handler.call(this, event, view);
            
        } else if(handler[action[0]]) {
            handler[action[0]].call(handler, action[1], view);
        }
    });
    
    return this;
};

/**
 * Set / get attributes
 * 
 * @param {string|object} what
 * 
 * @return {string|object} the view / attribute value
 */
window.app.view._base.prototype.attr = function(what) {
    if(typeof what === 'string') {
        if(what.substr(0, 5) !== 'data-')
            what = 'data-' + what;
        
        return this.node.attr(what);
    }
    
    for(var k in what) {
        if(k.substr(0, 5) !== 'data-') {
            what['data-' + k] = what[k];
            delete what[k];
        }
    }
    
    this.node.attr(what);
    
    return this;
};

/**
 * Set / get data from upmost level view
 * 
 * @param {string} key
 * @param {any|undefined} value
 * 
 * @return {any|object} the view / value
 */
window.app.view._base.prototype.data = function(key, value) {
    if(value !== undefined) {
        this._data[key] = value;
        
        return this;
    }
    
    return this._data[key];
};

/**
 * Run a jQuery selector against the view DOM tree
 * 
 * @param {string} selector
 * 
 * @return {object} jQuery collection
 */
window.app.view._base.prototype.find = function(selector) {
    return this.node.find(selector);
};

/**
 * Add to dom
 * 
 * @param {string} mode (JQuery method)
 * @param {any} target other view, jQuery collection, jQuery selector or DOM node
 * 
 * @return {object} the view
 */
window.app.view._base.prototype.addToDOM = function(mode, target) {
    if(!target)
        throw 'bad_target';
    
    if(target.node) // Supposed to be another view
        target = target.node;
    
    this.node[mode](target);
    
    return this;
};

/**
 * Prepend view to other view or dom node
 * 
 * @param {object} target
 * 
 * @return {object} the view
 */
window.app.view._base.prototype.prependTo = function(target) {
    return this.addToDOM('prependTo', target);
};

/**
 * Append view to other view or dom node
 * 
 * @param {object} target
 * 
 * @return {object} the view
 */
window.app.view._base.prototype.appendTo = function(target) {
    return this.addToDOM('appendTo', target);
};

/**
 * Insert view before other view or dom node
 * 
 * @param {object} target
 * 
 * @return {object} the view
 */
window.app.view._base.prototype.insertBefore = function(target) {
    return this.addToDOM('insertBefore', target ? target : this.tpl);
};

/**
 * Insert view after other view or dom node
 * 
 * @param {object} target
 * 
 * @return {object} the view
 */
window.app.view._base.prototype.insertAfter = function(target) {
    return this.addToDOM('insertAfter', target ? target : this.tpl);
};

/**
 * Remove view
 */
window.app.view._base.prototype.destroy = function() {
    this.node.remove();
    this.tpl = null;
    this.parent = null;
};
