/*
 * This file is part of the ReferentielSearch project.
 * 2014 - 2015
 */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * ReferentielSearch UI
 */
window.app.ui.popup = {
    /**
     * Open a popup
     *
     * @param {string|object} title
     * @param {object} buttons lang id to action
     * @param {object} options
     *
     * @return Dialog
     */
    create: function(title, buttons, options) {
        if (typeof title !== 'string') {
            if (title.out) {
                title = title.out();
            } else if (!title.jquery) {
                title = title.toString();
            }
        }

        if(!options) options = {};
        if(options.close_icon === undefined) options.close_icon = true;

        var mtid = app.ui.getUID();

        var settings = {};
        for(var k in options)
            if(!k.match(/^(close_icon|onclose|no_close)$/))
                settings['data-' + k] = (typeof options[k] === 'boolean') ? (options[k] ? 'true' : 'false') : '' + options[k];

        if(options.no_close) {
            settings['close-on-click'] = 'false';
            settings['close-on-esc'] = 'false';
            options.close_icon = false;
        }

        /*if(options.onclose)
            settings.close = options.onclose;*/

        settings['data-reveal'] = '';
        settings['app-uid'] = mtid;
        settings['aria-labelledby'] = mtid;
        settings['aria-hidden'] = 'true';
        settings['role'] = 'dialog';

        var reveal = $('<div class="reveal" />').attr(settings).appendTo('body'); // Needed for open event to fire

        //$(document).foundation('reveal', 'events'); // Needed for all events to fire

        if(options.close_icon) {
            $('<a class="close-reveal-modal" />')
                .appendTo(reveal)
                .attr({'aria-label': app.lang.tr('close').out()})
                .on('click', function() {
                    $(this).closest('.reveal').foundation('close');
                });
        }

        if(options.onclose) reveal.on('closed.zf.reveal', options.onclose);

        $('<h2 id="' + mtid + '" />').text(title).appendTo(reveal);

        var content = $('<section />').appendTo(reveal);

        if(buttons) {
            var btns = $('<section class="buttons" />').appendTo(reveal);
            for(var lid in buttons) {
                if(buttons[lid] === undefined)
                    buttons[lid] = true;

                var onclick = buttons[lid].onclick ? buttons[lid].onclick : buttons[lid];

                var classes = ['button'];
                if(buttons[lid].type)
                    classes.push(buttons[lid].type);

                $('<span role="button" />')
                    .text(app.lang.tr(lid).out())
                    .addClass(classes.join(' '))
                    .appendTo(btns)
                    .on('click', (function(h) {
                        return function() {
                            var close = (h && h.call) ? h.call(content) : true;
                            if((close === undefined) || close) app.ui.popup.close(this);
                        };
                    })(onclick));
            }
        }

        reveal = new Foundation.Reveal(reveal);

        return content;
    },

    /**
     * Resolve popup
     *
     * @param {string|object} node selector or node
     *
     * @return {object}
     */
    getReveal: function(node) {
        if(!node) return null;

        node = $(node);
        if(!node.is('[data-reveal]'))
            node = node.closest('[data-reveal]');

        return node.length ? node : null;
    },

    /**
     * Open popup
     *
     * @param {string|object} node selector or node
     */
    open: function(node) {
        node = this.getReveal(node);

        if(node) node.foundation('open');
    },

    /**
     * Close popup
     *
     * @param {string|object} node selector or node
     */
    close: function(node) {
        node = this.getReveal(node);

        if(node) node.foundation('close');
    },

    /**
     * Reflow popup
     *
     * @param {string|object} node selector or node
     */
    reflow: function(node) {
        node = this.getReveal(node);

        if(node) node.foundation('reflow');
    },

    /**
     * Display a nice alert like dialog
     *
     * @param {string} type "info", "success" or "error"
     * @param {string} message
     * @param {function} onclose
     */
    alert: function(type, message, onclose) {
        if (typeof message !== 'string') {
            if (message.out) {
                message = message.out();
            } else if (!message.jquery) {
                message = message.toString();
            }
        }

        var ctn = this.create(lang.tr(type + '_dialog'), null, {onclose: onclose});
        this.getReveal(ctn).addClass(type);
        ctn.html(message);
        this.open(ctn);
        return ctn;
    },

    /**
     * Display a confirm box
     *
     * @param {string|object} message
     * @param {function} onyes
     * @param {function} onno
     */
    confirm: function(message, onyes, onno) {
        if (typeof message !== 'string') {
            if (message.out) {
                message = message.out();
            } else message = message.toString();
        }

        var ctn = this.create(lang.tr('confirm_dialog'), {yes: onyes, no: onno}, {no_close: true});
        ctn.html(message);
        this.open(ctn);
        return ctn;
    },

    /**
     * Display a prompt box
     *
     * @param {string|object} title
     * @param {function} onok
     * @param {function} oncancel
     */
    prompt: function(title, onok, oncancel) {
        return this.create(title, {ok: onok, cancel: oncancel}, {onclose: oncancel});
    },

    /**
     * Show a notification
     *
     * @param {string} txt Text to show
     * @param {string} type Type of notification to be shown
     * @param {integer} autoclose Time (in milliseconds) to auto close the popup
     */
    notify: function(txt, type, autoclose) {
        if (type === undefined) {
            type = '';
        }

        if (autoclose === undefined)
            autoclose = 4000;

        txt += '';
        var color = '';

        switch (type) {
            case 'danger':
                color = 'red';
                break;
            case 'warning':
                color = 'black';
                break;
            case 'success':
                color = 'green';
                break;
            case 'info':
            default:
               color = 'blue';
        }

        new jBox('Notice', {
            content: txt,
            color: color,
            stack: false,
            autoClose:autoclose,
            position: {
                y: 'top',
                x: 'center'
            }});
    }
};
