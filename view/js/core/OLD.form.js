/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

/* global app, moment */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * Automated form behaviour
 */


/**
 * Form handler
 *
 * @param {object|string} node Current form node
 */
window.app.form = function(node) {
    if(typeof this.forms === 'undefined')
        this.forms = {};

    if (typeof node === 'string')
        node = $('[data-form="'+node+'"]');

    node = $(node);

    var id = node.attr('data-form');
    if(!id) return null;

    if(!this.forms[id])
        this.forms[id] = new (app.form.controller)(node);

    return this.forms[id];
};

/**
 * Multiple emails format specification
 *
 * @type {{separator_pattern: RegExp}}
 */
window.app.form.multipleEmails = {
    separator_pattern: /[\s,;:]+/
};

/**
 * Form controller constructor
 *
 * @param {object} node DOM node or jQuery object
 */
window.app.form.controller = function(node) {
    this.node = node;
    this.validators = [];
    this.eventHandlers = {};
    this.uid = 1;

    var nesting = 0;
    $('[data-multiple]', node).each(function() {
        var m = $(this);
        var n = m.parents('[data-multiple]').length;
        m.attr({'data-nested': n});
        nesting = Math.max(nesting, n + 1);
    });

    node.attr({'data-nesting': nesting});
    this.nesting = nesting;

    this.map = {};
    this.map = this.explore(node, 0);

    this.setupControls();

    node.data('form-controller', this);

    app.event.trigger('form.' + this.node.attr('data-form') + '.ready', this);
};

/**
 * Explore form and build map
 *
 * @param {object} node form DOM node or jQuery object
 * @param {number} nesting
 *
 * @returns {object}
 */
window.app.form.controller.prototype.explore = function(node, nesting) {

    var map = {};
    var fc = this;

    // Inputs at level (radio processed later)
    $(':input:not([type="radio"], button, [data-nested="' + nesting + '"] :input)', node).each(function() {
        var i = $(this);

        var type = fc.getTypeOf(i);

        var constraints = {required: i.is('[required]')};
        switch(type) {
            case 'date':
            case 'datetime':
            case 'time':
                constraints.min = i.attr('data-picker-min');
                constraints.max = i.attr('data-picker-max');
                break;

            case 'number':
            case 'range':
                constraints.min = i.attr('min');
                constraints.max = i.attr('max');
                break;

            case 'text':
            case 'textarea':
            case 'email':
            case 'url':
            case 'password':
                constraints.maxlength = i.attr('maxlength');
                break;
        }

        for(var k in constraints)
            if(k !== 'required' && constraints.hasOwnProperty(k) && typeof constraints[k] !== 'undefined')
                constraints[k] = parseInt(constraints[k]);

        map[i.attr('name')] = {
            type: type,
            name: i.attr('name'),
            constraints: constraints,
            node: i,
            controller: fc,
            path: fc.getNodePath(i)
        };

        i.closest('label').find('[data-hint]').each(function () {
            i.attr({title: $(this).text()});
            $(this).remove();
        });

        if (i.closest('[data-multiple-emails]').length){
            // Adding multiple emails in one shot
            i.on('change', function() {
                var value = $(this).val();
                if (!value) return;

                value = value.split(app.form.multipleEmails.separator_pattern);
                var emails = [];
                for (var i = 0; i < value.length ; i ++){
                    if (value[i]) emails.push(value[i]);
                }

                // First entry
                $(this).val(emails.shift());

                var multiple = fc.getEntryFromNode($(this).closest('[data-multiple-emails]'));
                var ref = $(this).closest('[data-entry]');

                // Create fields
                while (emails.length){
                    ref = fc.addMultipleEntry(multiple, {email: emails.shift()}, ref).node;
                }

                fc.getMultipleData(multiple); // Validates
            });

        }else{
            // Adding single email
            i.on('change', function() {
                fc.getEntryData(this); // Validates
            });
        }


        if(type === 'range') {
            var p = $('<span class="preview" data-range="' + i.attr('name') + '" />').insertAfter(i).text(i.val());

            i.on('input change', function() {
                p.text($(this).val());
            });
        }
    });

    // Radio, get and group
    $('[data-radio]:not([data-nested="' + nesting + '"])', node).each(function() {
        var i = $(this);
        map[i.attr('data-radio')] = {
            type: 'radio',
            name: i.attr('name'),
            constraints: {required: i.find('[required]').length > 0},
            node: i,
            choices: i.find(':input'),
            controller: fc,
            path: fc.getNodePath(i)
        };

        i.find(':input').on('change', function() {
            fc.getEntryData(this); // Validates
        });
    });

    // At bottom
    if(nesting === this.nesting) return map;

    // Sub nests
    $('[data-multiple][data-nested="' + nesting + '"]', node).each(function() {
        var m = $(this);

        var tpl = m.clone();
        tpl.find('> legend').remove();
        tpl.removeAttr('data-multiple data-multiple-emails data-min-count data-max-count data-start-count');

        var start = JSON.parse(m.attr('data-start-values'));
        if(!start) {
            start = [];
            for(var i=0; i<parseInt(m.attr('data-start-count')); i++)
                start.push({});
        }

        map[m.attr('data-multiple')] = {
            type: 'multiple',
            name: m.attr('name'),
            min: parseInt(m.attr('data-min-count')),
            max: parseInt(m.attr('data-max-count')),
            nested: parseInt(m.attr('data-nested')),
            node: m,
            sub: fc.explore(m, nesting + 1),
            tpl: tpl,
            start: start,
            entries: [],
            controller: fc,
            path: fc.getNodePath(m)
        };

        fc.setupMultipleEntry(map[m.attr('data-multiple')]);
    });

    return map;
};

/**
 * Get entry type
 *
 * @param {object} entry map entry or jQuery object
 *
 * @returns {string}
 */
window.app.form.controller.prototype.getTypeOf = function(entry) {
    if(entry.node) entry = entry.node;

    var type = entry[0].tagName.toLowerCase();
    if(type === 'input') {
        type = entry.attr('type');

        if(type === 'text') {
            var dt = entry.attr('data-picker');
            if(dt)
                type = dt;
        }
    }

    if(type === 'fieldset') {
        if(entry.is('[data-entry]')) return 'entry';
        if(entry.is('[data-multiple]')) return entry.attr('data-multiple');
        if(entry.is('[data-radio]')) return 'radio';
    }

    return type;
};

/**
 * Get entry name
 *
 * @param {object} entry map entry or jQuery object
 *
 * @returns {string}
 */
window.app.form.controller.prototype.getNameOf = function(entry) {
    if(entry.node) entry = entry.node;

    if(entry.is(':input[type="radio"], [data-radio]'))
        return entry.closest('[data-radio]').attr('data-radio');

    if(entry.is(':input'))
        return entry.attr('name');

    if(entry.is('[data-entry]'))
        return entry.closest('[data-multiple]').find('[data-entry]').index(entry);

    if(entry.is('[data-multiple]'))
        return entry.attr('data-multiple');

    return null;
};

/**
 * Get node path
 *
 * @param {object} node (jQuery object)
 *
 * @returns {object} array of path elements
 */
window.app.form.controller.prototype.getNodePath = function(node) {
    var path = [this.getNameOf(node)];
    if(path[0] === null)
        return null;

    if(typeof path[0] === 'number')
        path.unshift(node.closest('[data-multiple]').attr('data-multiple'));

    if(node.node) node = node.node;
    node.parents('[data-entry]').each(function() {
        var e = $(this);
        path.unshift(e.closest('[data-multiple]').find('[data-entry]').index(e));
        path.unshift(e.closest('[data-multiple]').attr('data-multiple'));
    });

    return path;
};

/**
 * Get entry from path
 *
 * @param {object} path
 *
 * @returns {object}
 */
window.app.form.controller.prototype.getEntryFromPath = function(path) {
    var entry = this.map;

    while(path.length) {
        var p = path.shift();

        if(entry.type && entry.type === 'multiple') {
            if(entry.entries.length <= p) return null;

            entry = entry.entries[p];

        } else {
            if (entry.fields) entry = entry.fields;

            if(!entry[p]) return null;

            entry = entry[p];
        }
    }

    return entry;
};

/**
 * Get entry from DOM node
 *
 * @param {object|string} node
 *
 * @returns {object|null}
 */
window.app.form.controller.prototype.getEntryFromNode = function(node) {
    node = $(node).closest(':input:not(button), [data-entry], [data-multiple]');

    if(!node.length) return null;

    return this.getEntryFromPath(this.getNodePath(node));
};

/**
 * Clone multiple block entry template
 *
 * @param {object} multiple
 * @param {object} insertAfter
 *
 * @returns {{type: string, uid: number, node: *, multiple: *, fields}}
 */
window.app.form.controller.prototype.cloneMultipleEntry = function(multiple, insertAfter) {
    var uid = this.uid++;

    var clone = multiple.tpl.clone().attr({'data-entry': uid});
    if (insertAfter)
        clone.insertAfter(insertAfter);
    else
        clone.insertBefore(multiple.add);

    var map = {
        type: 'entry',
        uid: uid,
        node: clone,
        multiple: multiple,
        fields: this.explore(clone, multiple.nested + 1)
    };

    multiple.entries.push(map);

    return map;
};

/**
 * Init multiple block
 *
 * @param {object} multiple
 */
window.app.form.controller.prototype.setupMultipleEntry = function(multiple) {
    var legend = multiple.node.find('> legend');
    multiple.node.empty();
    if(legend.length) legend.appendTo(multiple.node);

    multiple.add = $('<button class="button" data-action="add" />').text(lang.tr('add')+'');
    multiple.add.prepend('<span class="fa fa-plus-circle" /> ');

    var fc = this;
    multiple.add.appendTo(multiple.node).on('click', function() {
        var multiple = fc.getEntryFromNode(this);
        if(!multiple) return;

        fc.addMultipleEntry(multiple, {}, null);
    });

    for(var i=0; i<multiple.start.length; i++)
        this.addMultipleEntry(multiple, multiple.start[i], null);
};

/**
 * Update multiple block buttons
 *
 * @param {object} multiple
 */
window.app.form.controller.prototype.updateMultipleEntry = function(multiple) {
    var max = multiple.max && (multiple.entries.length >= multiple.max);
    multiple.node.find('[data-action="add"]').prop('disabled', max);

    var min = multiple.min && (multiple.entries.length <= multiple.min);
    multiple.node.find('[data-action="del"]').prop('disabled', min);
};

/**
 * Add entry to multiple block
 *
 * @param {object} multiple
 * @param {object} values
 * @param {object} insertAfter
 */
window.app.form.controller.prototype.addMultipleEntry = function(multiple, values, insertAfter) {
    var map = null;

    if(!multiple.max || (multiple.entries.length < multiple.max)) {
        map = this.cloneMultipleEntry(multiple, insertAfter);

        if(typeof values === 'object') {
            for (var k in values) {
                if (values.hasOwnProperty(k)) {
                    map.node.find(':input[name="' + k + '"]').val(values[k]);
                    map.node.find('[data-radio="' + k + '"] [type="radio"][value="' + values[k] + '"]').prop('checked', true);
                }
            }

        } else {
            var i = map.node.find('[data-radio]').eq(0);
            if(i.length) {
                i.find('[type="radio"][value="' + values + '"]').prop('checked', true);

            } else {
                map.node.find(':input').eq(0).val(values);
            }
        }

        map.del = $('<button class="button" data-action="del" />').text(lang.tr('delete') + '');
        map.del.prepend('<span class="fa fa-trash" /> ');

        var fc = this;
        map.del.appendTo(map.node).on('click', function () {
            var entry = fc.getEntryFromNode(this);
            if (!entry) return;

            fc.deleteMultipleEntry(entry);
        });

        // Refresh buttons
        this.updateMultipleEntry(multiple);
    }

    return map;
};

/**
 * Delete multiple block entry
 *
 * @param {object} entry
 */
window.app.form.controller.prototype.deleteMultipleEntry = function(entry) {
    if(!entry.multiple.min || (entry.multiple.entries.length > entry.multiple.min)) {
        var idx = -1;
        for(var i=0; i<entry.multiple.entries.length && idx < 0 ; i++){
            if (entry.uid === entry.multiple.entries[i].uid)
                idx = i;
        }

        if (idx >= 0) {
            entry.node.remove();
            entry.multiple.entries.splice(idx, 1);

            // Refresh buttons
            this.updateMultipleEntry(entry.multiple);
        }
    }
};

/**
 * Validation callback
 *
 * @callback formValidator
 *
 * @this {object} the entry or map
 *
 * @param {function} validate to be called if data is valid
 * @param {function} reject to be called if data is invalid with error string as argument
 * @param {*} data
 */

/**
 * Add custom data checker
 *
 * @param {formValidator} validator
 * @param {string} target input matcher (use of * allowed)
 */
window.app.form.controller.prototype.addValidator = function(validator, target) {
    if(target) {
        target = target.replace(/\./g, '\.');
        target = target.replace(/\*/g, '.+');
        target = target.replace(/\[\]/g, '\.[0-9]+');
    }

    this.validators.push({
        validator: validator,
        matcher: target ? new RegExp('^' + target + '$') : null
    });
};

/**
 * Gather data from multiple type field
 *
 * @param {object} multiple
 *
 * @returns {app.promise}
 */
window.app.form.controller.prototype.getMultipleData = function(multiple) {
    return new app.promise(
        function(fulfill, reject) {
            var data = [];
            var getters = [];
            var i;

            for(i=0; i<this.entries.length; i++) {
                if (this.entries[i] !== undefined) {
                    getters.push(
                        new app.promise(
                            function (s_fulfill, s_reject) {
                                this.controller.getEntriesData(this.fields).then(
                                    /* fulfilled */ function (d) {
                                        if(d)
                                            data.push(d);

                                        s_fulfill(d);
                                    },
                                    s_reject
                                );
                            },
                            this.entries[i]
                        ));
                }
            }

            app.promise.all(getters).then(
                /* fulfilled */ function() {
                    if(typeof this.min !== 'undefined') {
                        if(data.length < multiple.min) {
                            reject({entry: this, error: 'not_enough_entries'});
                            return;
                        }
                    }

                    if(typeof this.max !== 'undefined') {
                        if (data.length > multiple.max) {
                            reject({entry: this, error: 'too_many_entries'});
                            return;
                        }
                    }

                    fulfill(data);
                },
                reject
            );
        },
        /* promise context is the multiple */
        multiple
    );
};

/**
 * Get and validate entry data
 * @param entry
 *
 * @returns {app.promise}
 */
window.app.form.controller.prototype.getEntryData = function(entry) {
    var fc = this;
    return new app.promise(function(fulfill, reject) {
        if (!entry.node) entry = fc.getEntryFromNode(entry);

        var data = null;
        var d;

        switch (entry.type) {
            case 'multiple':
                fc.getMultipleData(entry).then(fulfill, reject);
                return;

            case 'date':
            case 'datetime':
                d = entry.node.datetimepicker('getValue');
                data = d ? parseInt(d.getTime() / 1000) - (d.getTimezoneOffset() * 60) : null;
                break;

            case 'time':
                d = entry.node.datetimepicker('getValue');
                data = d ? ( d.getHours() * 3600 ) + (d.getMinutes() * 60) : null;
                break;

            case 'number':
            case 'range':
                data = parseInt(entry.node.val());
                break;

            case 'checkbox':
                data = entry.node.is(':checked');
                break;

            case 'radio':
                data = entry.choices.filter(':checked').val();
                if (typeof data === 'undefined') data = '';
                break;

            case 'select':
                data = entry.node.val();
                break;

            case 'hidden':
                data = entry.node.val();
                break;

            case 'text':
            case 'textarea':
            case 'email':
            case 'url':
            case 'password':
                data = entry.node.val();

                if (typeof entry.constraints.maxlength !== 'undefined') {
                    if (data.length > entry.constraints.maxlength) {
                        reject({entry: entry, error: 'too_long'});
                        return;
                    }
                }

                break;

            default:
                reject({entry: entry, error: 'unknown_type'});
                return;
        }

        if (typeof data === 'undefined')
            data = null;

        if (entry.constraints && entry.constraints.required) {
            if (data === null || !('' + data).length) {
                reject({entry: entry, error: 'value_required'});
                return;
            }
        }

        if ((entry.type !== 'radio') && entry.node[0].checkValidity) {
            if (!entry.node[0].checkValidity()) {
                reject({entry: entry, error: 'bad_' + entry.type});
                return;
            }
        }

        if (entry.type === 'email') {
            if (!app.utilities.isValidEmailAddress(data)) {
                reject({entry: entry, error: 'bad_email'});
                return;
            }
        }

        if (typeof entry.constraints.min !== 'undefined') {
            if (data < entry.constraints.min) {
                reject({entry: entry, error: 'value_too_low'});
                return;
            }
        }

        if (typeof entry.constraints.max !== 'undefined') {
            if (data > entry.constraints.max) {
                reject({entry: entry, error: 'value_too_high'});
                return;
            }
        }

        var validators = [];
        var path = entry.path.join('.');
        for (var i = 0; i < fc.validators.length; i++) {
            if (fc.validators[i].matcher && path.match(this.validators[i].matcher)) {
                validators.push(
                    new app.promise(
                        function(v_fulfill, v_reject, validator, data) {
                            var error = validator.call(this, data);

                            if(error) {
                                v_reject({entry: this, error: error});
                            } else {
                                v_fulfill();
                            }
                        },
                        entry,
                        [fc.validators[i].validator, data]
                    )
                );
            }
        }

        if(validators.length) {
            app.promise.all(validators).then(fulfill, reject);

        } else {
            fulfill(data);
        }

    }).catch(function(e) {
        fc.displayError(e.error, entry);
    });
};

/**
 * Display entry error
 *
 * @param {string} error (translatable)
 * @param {object} entry
 */
window.app.form.controller.prototype.displayError = function(error, entry) {
    var tooltip;

    if(error) {
        entry.node.attr({'data-valid': 'no'});

        tooltip = entry.node.data('error_tooltip');
        if(!tooltip) {
            if (entry.type === 'multiple') {
                tooltip = $('<div class="error_hint" />').insertAfter(entry.node.find('[data-entry]:last'));

            } else if (entry.type === 'email') {
                tooltip = $('<div class="error_hint" />').appendTo(entry.node.parent().parent());

            } else {
                tooltip = $('<div class="error_hint" />').insertAfter(entry.node);
            }


            entry.node.data('error_tooltip', tooltip);
        }

        tooltip.text(lang.tr(error)+'');

    } else {
        entry.node.attr({'data-valid': 'yes'});

        tooltip = entry.node.data('error_tooltip');
        if(tooltip) {
            tooltip.remove();
            entry.node.removeData('error_tooltip');
        }
    }
};

/**
 * Gather entries data
 *
 * @param {object} entries map fragment
 *
 * @returns {app.promise}
 */
window.app.form.controller.prototype.getEntriesData = function(entries) {
    return new app.promise(
        function(fulfill, reject) {
            var data = {};
            var getters = [];

            for(var k in entries) {
                getters.push(
                    new app.promise(
                        function(s_fulfill, s_reject) {
                            this.controller.getEntryData(this).then(
                                /* fulfilled */ function(d) {
                                    data[k] = d;
                                    s_fulfill(d);
                                },
                                s_reject
                            );
                        },
                        entries[k]
                    )
                );
            }

            app.promise.all(getters).then(
                /* fulfilled */ function() {
                    fulfill(data);
                },
                reject
            );
        },
        /* the promise context is the form controller */
        this
    );
};

/**
 * Gather data, validate in the process
 *
 * @return {app.promise}
 */
window.app.form.controller.prototype.getData = function() {
    return new app.promise(
        function(fulfill, reject) {
            this.getEntriesData(this.map).then(
                /* fulfilled */ function(data) {
                    var validators = [];
                    for (var i = 0; i < this.validators.length; i++) {
                        if (!this.validators[i].matcher) {
                            validators.push(
                                new app.promise(
                                    function(v_fulfill, v_reject, validator) {
                                        var error = validator.call(this.map, data);

                                        if(error) {
                                            v_reject(error);
                                        } else {
                                            v_fulfill();
                                        }
                                    },
                                    this,
                                    [this.validators[i]]
                                )
                            );
                        }
                    }

                    if(validators.length) {
                        app.promise.all(validators).then(
                            /* fulfilled */ function() {
                                fulfill(data);
                            },
                            /* rejected */ function(e) {
                                app.ui.notify('error', lang.tr(e.error).out());
                                reject(e);
                            }
                        );

                    } else {
                        fulfill(data);
                    }
                },
                /* rejected */ function(e) {
                    if(e.entry)
                        app.ui.focus(e.entry.node);

                    app.ui.notify('error', lang.tr('invalid_fields').out());
                }
            );
        },
        /* the form controller is the promise context */
        this
    );
};

/**
 * Locks controls
 *
 * @param {boolean} lock
 */
window.app.form.controller.prototype.lock = function(lock) {
    var ctn = this.node.find('[data-controls]');
    if (lock){
        this.node.find(':input').prop('disabled', true);
        ctn.find('[data-control]').addClass('disabled');
        ctn.append('<span data-loader class="fa fa-circle-o-notch fa-spin fa-2x fa-fw" />');
    }else{
        this.node.find(':input').prop('disabled', false);
        ctn.find('[data-control]').removeClass('disabled');
        ctn.find('[data-loader]').remove();
    }
};

/**
 * Add event handler
 *
 * @param {string} event
 * @param {function} callback
 */
window.app.form.controller.prototype.on = function(event, callback) {
    this.eventHandlers[event] = callback;
};

/**
 * Setup controls
 */
window.app.form.controller.prototype.setupControls = function() {
    var fc = this;
    this.node.find('[data-control]').on('click', function() {
        var ctrl = $(this);
        if(ctrl.hasClass('disabled')) return;

        var action = ctrl.attr('data-control');
        var resource = ctrl.attr('data-control-resource');

        var goto = ctrl.attr('data-control-goto');

        if(action === 'cancel' && goto) {
            app.ui.redirect(goto.replace(/^\//g, ''));
            return;
        }

        var success = ctrl.attr('data-control-success');
        var failure = ctrl.attr('data-control-failure');
        if(!success) success = action + '_success';
        if(!failure) failure = action + '_failure';

        fc.getData().then(
            /* fulfilled */ function(data) {
                // Disable controls and inputs
                fc.lock(true);

                var success_callback = function(data, alt) {
                    if(typeof data !== 'object') data = alt; // create

                    app.ui.notify('success', lang.tr(success), function() {
                        if(goto) {
                            goto = goto.replace(/:[a-z][a-z0-9_-]+/g, function(m) {
                                var k = m.substr(1);
                                return data[k] ? encodeURIComponent(data[k]) : '?' + k;
                            });

                            app.ui.redirect(goto.replace(/^\//g, ''));

                        } else {
                            app.ui.reload();
                        }
                    });
                };

                var failure_callback = function(error) {
                    var notify = true;
                    if(fc.eventHandlers.failure)
                        notify = fc.eventHandlers.failure.call(fc, error);

                    fc.lock(false);

                    if(notify)
                        app.ui.notify('error', lang.tr(failure));
                };

                if(!resource) {
                    if(fc.eventHandlers[action]) {
                        fc.eventHandlers[action].call(fc, data, function(success, result_data){
                            if(success) {
                                success_callback(result_data);
                            } else {
                                fc.lock(false);
                                failure_callback(result_data);
                            }
                        });
                    }

                    return;
                }

                var entity = resource.split('?')[0].split('/');
                if(action === 'create') {
                    entity = entity[entity.length - 1];

                } else {
                    entity = entity[entity.length - 2];
                }

                var copts = {error: failure_callback};

                if(action === 'create') {
                    app.client.post(resource, data, success_callback, copts);

                } else if(action === 'update') {
                    app.client.put(resource, data, success_callback, copts);

                } else if(action === 'delete') {
                    var confirm = ctrl.attr('data-control-confirm');

                    if(confirm) {
                        if(confirm === '1')
                            confirm = 'delete_entity';

                        app.ui.popup.confirm(lang.tr(confirm).r({entity: entity}), function() {
                            app.client.delete(resource, success_callback, copts);
                        }, function(){
                            fc.lock(false);
                        });

                    } else {
                        app.client.delete(resource, success_callback, copts);
                    }
                }
            },
            null
        ); // Validates
    });
};

// Init fields and forms
app.event.on('started', function() {
    $('[data-form] [data-picker="date"], [data-form] [data-picker="datetime"], [data-form] [data-picker="time"]').each(function() {
        var i = $(this);

        // datetimepicker parameters
        var o = {
            scrollInput: false
        };

        var type = i.attr('data-picker');
        switch (type){
            case 'date':
                // Do not show date picker
                o.timepicker = false;
                o.format = lang.tr('dp_date_format').out();
                o.formatDate = o.format;
                break;

            case 'datetime':
                o.format = lang.tr('dp_datetime_format').out();
                o.formatDate = o.format;
                break;

            case 'time':
                // Do not show date picker
                o.datepicker = false;
                o.format = lang.tr('dp_time_format').out();
                break;
        }

        // Setting start date
        var startDate = i.attr('data-picker-start-date');
        if (typeof startDate !== 'undefined')
            o.startDate = startDate;

        // Setting default time
        var defaultTime = i.attr('data-picker-default-time');
        if (typeof defaultTime !== 'undefined')
            o.defaultTime = app.utilities.timestampToFormatedTime(defaultTime);

        // Getting minimum value for current input
        var min = i.attr('data-picker-min');
        min = (typeof min !== 'undefined' && !isNaN(min)) ? parseInt(min) : null;

        // Getting maximum value for current input
        var max = i.attr('data-picker-max');
        max = (typeof max !== 'undefined' && !isNaN(max)) ? parseInt(max) : null;

        if(type === 'time') {

            // Set allowed values for time picker
            var allowedTimes = [];
            if (min === null) min = 0;
            if (max === null) max = 86399; // 3600*24 -1

            var step = i.attr('data-picker-step');
            step = (typeof step !== 'undefined' && !isNaN(step)) ? parseInt(step) : 3600;


            for (var t = min; t<=max; t+=step){
                allowedTimes.push(app.utilities.timestampToFormatedTime(t));
            }

            // Init time picker
            o.allowTimes = allowedTimes;

        }else {
            // Translate moment in user timezone
            var tr = 'moment_date';

            if(type === 'datetime')
                tr = 'moment_datetime_format';

            tr = lang.tr(tr).out();

            if (min !== null)
                o.minDate = moment(min,'X').format(tr);

            if (max !== null)
                o.maxDate = moment(max,'X').format(tr);
        }

        // Init date picker
        i.datetimepicker(o);
    });

    $('[data-form]').each(function() {
        app.form(this);
    });
});


