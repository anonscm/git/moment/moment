/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

/* global app */

if(!('app' in window))
    window.app = {};

/**
 * Promise subject
 *
 * @callback promiseSubject
 * @param {function} fulfill to be called if promise is fulfilled
 * @param {function} reject to be called if promise is rejected
 */

/**
 * Promise fulfilled
 *
 * @callback promiseFulfilled
 * @param {*} result data returned by the subject
 */

/**
 * Promise rejected
 *
 * @callback promiseRejected
 * @param {*} result data returned by the subject
 */

/**
 * Promise constructor
 *
 * @param {promiseSubject} subject stuff to do, takes resolve and reject arguments
 * @param {*} [context] for all (subject and defered
 * @param {*[]} [args] aditionnal arguments for subject and defered
 */
window.app.promise = function(subject, context, args) {
    this.status = null;
    this.result = null;
    this.callbacks = {fulfilled: [], rejected: []};

    if(context === undefined)
        context = window;

    this.context = context;
    this.args = args ? args : [];

    if(subject) {
        var me = this;
        args = [
            function (result) {
                me.resolve(result);
            },
            function (result) {
                me.reject(result);
            }
        ].concat(this.args);

        window.setTimeout(
            function() {
                subject.apply(context, args);
            },
            0
        );
    }
};

/**
 * All resolved
 *
 * @param {object} calls array
 * @param {*} [context] for all (subject and defered
 * @param {*[]} [args] aditionnal arguments for subject and defered
 * @param {function} [post] result post process
 *
 * @return {app.promise}
 */
window.app.promise.all = function(calls, context, args, post) {
    if(context === undefined)
        context = window;

    return new app.promise(
        function(resolve, reject) {
            if(!calls.length)
                resolve(post ? post.apply(context, [undefined].concat(args)) : undefined);

            var fulfilled = [];
            for(var i=0; i<calls.length; i++) {
                var p = calls[i].then ? calls[i] : new app.promise(calls[i], context, args);

                p.then(
                    function(result) {
                        fulfilled.push(result);

                        if(fulfilled.length === calls.length)
                            resolve(post ? post.apply(context, [fulfilled].concat(args)) : fulfilled);
                    },
                    reject
                );
            }
        },
        context,
        args
    );
};

/**
 * One resolved
 *
 * @param {object} calls array
 * @param {*} [context] for all (subject and defered
 * @param {*[]} [args] aditionnal arguments for subject and defered
 *
 * @return {app.promise}
 */
window.app.promise.race = function(calls, context, args) {
    if(context === undefined)
        context = window;

    return new app.promise(
        function(resolve, reject) {
            if(!calls.length)
                resolve();

            for(var i=0; i<calls.length; i++) {
                var p = calls[i].then ? calls[i] : new app.promise(calls[i], context, args);

                p.then(resolve, reject);
            }
        },
        context,
        args
    );
};

/**
 * Statuses
 */
window.app.promise.STATUS_NONE = null;
window.app.promise.STATUS_FULFILLED = 'fulfilled';
window.app.promise.STATUS_REJECTED = 'rejected';

/**
 * Cast to resolved promise
 *
 * @param {*} [result] promise result
 * @param {*} [context] for all (subject and defered
 * @param {*[]} [args] aditionnal arguments for subject and defered
 *
 * @return {app.promise}
 */
window.app.promise.cast = function(result, context, args) {
    var p = new app.promise(null);
    p.status = app.promise.STATUS_FULFILLED;
    p.result = result;
    p.context = context;
    p.args = args ? args : [];

    return p;
};

/**
 * Create resolved promise
 *
 * @param {*} [result] promise result
 * @param {*} [context] for all (subject and defered
 * @param {*[]} [args] aditionnal arguments for subject and defered
 *
 * @return {app.promise}
 */
window.app.promise.resolve = function(result, context, args) {
    return window.app.promise.cast(result, context, args);
};

/**
 * Create rejected promise
 *
 * @param {*} [result] promise error
 * @param {*} [context] for all (subject and defered
 * @param {*[]} [args] aditionnal arguments for subject and defered
 *
 * @return {app.promise}
 */
window.app.promise.reject = function(result, context, args) {
    var p = new app.promise(null);
    p.status = app.promise.STATUS_REJECTED;
    p.result = result;
    p.context = context;
    p.args = args ? args : [];

    return p;
};

/**
 * Fulfilled check
 *
 * @return {boolean}
 */
window.app.promise.prototype.isFulfilled = function() {
    return this.status === app.promise.STATUS_FULFILLED;
};

/**
 * Rejected check
 *
 * @return {boolean}
 */
window.app.promise.prototype.isRejected = function() {
    return this.status === app.promise.STATUS_REJECTED;
};

/**
 * Call defered thens
 */
window.app.promise.prototype.callDefered = function() {
    if(!this.status)
        return;

    for(var i=0; i<this.callbacks[this.status].length; i++)
        this.callbacks[this.status][i].apply(this.context, [this.result].concat(this.args));

    this.callbacks.fulfilled = [];
    this.callbacks.rejected = [];
};

/**
 * Resolve
 *
 * @param {*|app.promise} [result]
 */
window.app.promise.prototype.resolve = function(result) {
    if(this.status)
        return;

    if(result && result.then) { // Looks like a promise, propagate state
        if(result.isFulfilled()) {
            this.resolve(result.result);

        } else if(result.isRejected()) {
            this.reject(result.result);

        } else {
            throw new Error('Cannot resolve promise if other promise is not resolved');
        }

    } else {
        this.status = app.promise.STATUS_FULFILLED;
        this.result = result;

        this.callDefered();
    }
};

/**
 * Reject
 *
 * @param {object} [result]
 */
window.app.promise.prototype.reject = function(result) {
    if(this.status)
        return;

    this.status = app.promise.STATUS_REJECTED;
    this.result = result;

    this.callDefered();
};

/**
 * Then
 *
 * @param {promiseFulfilled} onFulfilled
 * @param {promiseRejected} [onRejected]
 *
 * @return {app.promise}
 */
window.app.promise.prototype.then = function(onFulfilled, onRejected) {
    if(typeof onFulfilled !== 'function')
        onFulfilled = null;

    if(typeof onRejected !== 'function')
        onRejected = null;

    if(!onFulfilled && !onRejected)
        return this;

    if(this.status === app.promise.STATUS_FULFILLED) {
        if(onFulfilled)
            onFulfilled.apply(this.context, [this.result].concat(this.args));

    } else if(this.status === app.promise.STATUS_REJECTED) {
        if(onRejected)
            onRejected.apply(this.context, [this.result].concat(this.args));

    } else {
        if(onFulfilled)
            this.callbacks.fulfilled.push(onFulfilled);

        if(onRejected)
            this.callbacks.rejected.push(onRejected);
    }

    return this;
};

/**
 * Catch
 *
 * @param {promiseRejected} onRejected
 *
 * @return {app.promise}
 */
window.app.promise.prototype.catch = function(onRejected) {
    return this.then(null, onRejected);
};
