app.event.on('started', function () {
    // Impersonation add-in
    app.session.get(function (user) {
        if (!user || !user.impersonated)
            return;
        
        $('<section class="impersonating" data-action="cancel-impersonation" />').text(app.lang.tr('impersonating')).on('click', function() {
            app.client.put('/user/@me', {impersonated_user_id: null}, function() {
                app.ui.reload();
            });
        }).prependTo('header nav.top-bar');
    });
    
    // Admin part
    var adm = $('main[data-page="admin_users"]');
    if(!adm.length)
        return;
    
    var results = adm.find('[data-container="users"]');
    
    app.template('user', results).onAction('impersonate', function(event, view) {
        app.ui.toggleLoader(true);
        app.client.put('/user/@me', {impersonated_user_id: view.attr('id')}, function() {
            app.ui.redirect('home');
        });
    });
    
    adm.find('[data-action="search"]').on('click', function() {
        var v = adm.find('input[name="search"]').val();
        if(v.length < 3) return;
        
        var flt = {or: [
            {contains: {id: v}},
            {contains: {attributes: v}}
        ]};
        
        app.ui.toggleLoader(true);
        app.client.get('/user', function(users) {
            results.find('[data-view="user"]').remove();
            
            for(var i=0; i<users.length; i++) {
                var user = users[i];
                
                var v = new app.view('user', results).appendTo(results);
                
                v.attr({id: user.id});
                v.property('id', user.id);
                v.property('email', user.email);
            }
            
            if(!users.length)
                results.text(app.lang.tr('no_results'));
            
            app.ui.toggleLoader(false);
            
        }, {args: {filter: JSON.stringify(flt)}});
    });
});
