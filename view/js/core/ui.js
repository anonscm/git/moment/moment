/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * ReferentielSearch UI
 */
window.app.ui = {
    _uid_counter: 0,

    /**
     * Get unique id
     *
     * @return string
     */
    getUID: function() {
        this._uid_counter++;

        return 'APPUID_' + this._uid_counter;
    },

    /**
     * Redirect user to url
     *
     * @param {string} [url]
     * @param {object} [args]
     * @param {string} [anchor]
     */
    redirect: function(url, args, anchor) {
        if(!args) args = {};
        var bits = window.location.search.substr(1).split('&'), k;
        for(var i=0; i<bits.length; i++) {
            if (bits[i] === '') continue;
            var bit = bits[i].split('=');
            k = bit.shift();
            if(k in args) continue;
            args[k] = bit.join('=');
        }

        if (typeof url === 'string') {
            url = app.config.application_url + url;
        } else {
            url = window.location.origin + window.location.pathname;
        }

        var search = [];
        for(k in args)
            search.push(k + '=' + args[k]);

        search = search.length ? '?' + search.join('&') : '';
        anchor = anchor ? '#' + anchor : '';
        window.location.href = url + search + anchor;
    },

    /**
     * Reload whole ui
     */
    reload: function() {
        window.location.reload();
    },

    /**
     * Display structured error to user
     */
    error: function(error) {
        var msg = '<p>'+lang.tr(error.message)+'</p>';

        if(error.details) {
            msg += "<pre>";
            $.each(error.details, function(k, v) {
                if(isNaN(k)) v = lang.tr(k) + ': ' + v;
                msg += "\n\t" + v;
            });
            msg += "</pre>";
        }

        if(error.uid)
            msg += "<p>" + lang.tr('you_can_report_exception') + ': <b>' + error.uid+'</b></p>';

        return app.ui.popup.alert('error', msg);
    },

    /**
     * Display raw error to user
     * TODO replace with popup
     */
    rawError: function(error) {
        console.log('An error happened');
        if (error) console.log(error);
    },

    /**
     * Display a notification
     *
     * @param type "info", "success" or "error"
     * @param message
     * @param ondisapear
     */
    notify: function(type, message, ondisapear) {
        if (typeof message !== 'string') {
            if (message.out) {
                message = message.out();
            } else if (!message.jquery) {
                message = message.toString();
            }
        }

        var ctn = $('#notifications');
        if(!ctn.length) ctn = $('<div id="notifications" />').appendTo('body');

        var id = message.replace(/[^a-z0-9]/gi,'');

        var n = ctn.find('[data-msgid="'+id+'"]');
        if (n.length === 0) {
            n = $('<div data-msgid="' + id + '" class="notification ' + type + '" />').html(message).appendTo(ctn);

            window.setTimeout(function () {
                n.fadeOut(1500, 'linear', function () {
                    n.remove();
                    if (ondisapear)
                        ondisapear.call(this);
                });
            }, 2000);
        }

        return n;
    },

    /**
     * Setup smart labels
     */
    smartLabels: function() {
        $('label').on('click', function(e) {
            if(e.target !== this) return;

            var input = $(this).find(':input');

            if(input.is(':checkbox'))
                input.prop('checked', !input.is(':checked'));

            if(input.is(':radio') && input.attr('name') && input.attr('value'))
                $(':radio[name="' + input.attr('name') + '"]').val([input.attr('value')]);

            if(input.is('input, textarea'))
                input.eq(0).focus();
        });
    },

    /**
     * Function used to toggle loading UI
     *
     * @param {boolean} show True to show loading, false to hide
     * @param {string} message Message to show
     */
    toggleLoader: function(show, message){
        if (message === undefined)
            message = app.lang.tr('please_wait').out();

        if (show){
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#fff',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: 0.95,
                    color: '#000',

                    width: '30%',
                    position: 'fixed',

                    top:'calc(50%)',
                    left:'calc(50% - 15%)'

                },
             message: message
            });
        }else{
            $.unblockUI();
        }
    },


    /**
     * Allows to focus on an element
     * @param {string} selector Jquery selector
     * @param {integer} duration Duration to show the focus hilight (in ms), default is 3000
     * @param {function} callback when completed
     */
    focus: function(selector, duration, callback){
        if (!duration)
            duration = 3000;

        // Ensure conference exists
        if ($(selector).length){
            // Focus on newly created conference
            $('html, body').animate({
                scrollTop: $(selector).offset().top -50
            });
            $(selector).show().effect({
                effect: 'highlight',
                duration: duration,
                complete: callback
            });
        }
    }
};
