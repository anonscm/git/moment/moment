/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

/* global app */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * AJAX webservice client
 */
window.app.client = {
    // REST service base path
    base_path: null,
    
    // Security token
    security_token: null,
    
    // Webservice maintenance flag
    maintenance: false,
    
    // Pending requests
    pending_requests: [],
    
    // Handling authentication required
    authentication_required: false,
    
    // Send a request to the webservice
    call: function(method, resource, data, callback, options) {
        if(!this.base_path) {
            this.base_path = app.config.application_url + 'rest.php';
        }

        if (this.security_token === null)
            this.security_token = $('body').attr('data-security-token');

        if(!options) options = {};
        
        var args = {}, k;
        if(options.args) for(k in options.args) args[k] = options.args[k];
        args._ = (new Date()).getTime(); // Defeat cache
        var urlargs = [];
        for(k in args) urlargs.push(k + '=' + args[k]);
        
        if(urlargs.length) resource += (resource.match(/\?/) ? '&' : '?') + urlargs.join('&');
        
        if(data) {
            var raw = options && ('rawdata' in options) && options.rawdata;
            
            if(!raw) data = JSON.stringify(data);
        }else data = undefined;
        
        var errorhandler = function(error) {
            app.ui.error(error);
        };
        if(options.error) {
            errorhandler = options.error;
            delete options.error;
        }
        
        var headers = {};
        if(options.headers) headers = options.headers;
        
        if(this.security_token /*&& (method != 'get')**/) headers['X-Security-Token'] = this.security_token;
        
        var settings = {
            cache: false,
            contentType: 'application/json;charset=utf-8',
            context: window,
            data: data,
            processData: false,
            dataType: 'json',
            beforeSend: function(xhr) {
                for(var k in headers) xhr.setRequestHeader(k, headers[k]);
            },
            success: callback,
            complete: function(xhr, status) { // Update security token if it was changed
                var new_security_token = xhr.getResponseHeader('X-Security-Token');
                if(new_security_token) app.client.security_token = new_security_token;
                
            },
            type: method.toUpperCase(),
            url: this.base_path + resource
        };
        
        // Needs to be done after "var settings" because handler needs that settings variable exists
        settings.error = function(xhr, status, error) {
            app.ui.toggleLoader(false);
            
            var msg = xhr.responseText.replace(/^\s+/, '').replace(/\s+$/, '');
            
            if( // Ignore 40x, 50x and timeouts if undergoing maintenance
                (xhr.status >= 400 || status === 'timeout') &&
                app.client.maintenance
            ) return;
            
            try {
                error = JSON.parse(msg);

                if (error.message === 'auth_user_not_allowed') // Should have been already reported by html ui
                    return;

                if(
                    (error.message === 'rest_authentication_required' || error.message === 'rest_xsrf_token_did_not_match') &&
                    (options.ignore_authentication_required || app.client.authentication_required)
                )
                    return;
                
                if(
                    (error.message === 'rest_authentication_required' || error.message === 'rest_xsrf_token_did_not_match') &&
                    (options.auth_prompt === undefined || options.auth_prompt)
                ) {
                    app.client.authentication_required = app.ui.popup(
                        lang.tr('authentication_required'),
                        app.config.logon_url ? {
                            logon: function() {
                                app.ui.redirect(app.config.logon_url);
                            }
                        } : {
                            ok: function() {}
                        },
                        {noclose: true}
                    );
                    app.client.authentication_required.text(lang.tr('authentication_required_explanation'));
                    return;
                }

                if(error.message === 'undergoing_maintenance') {
                    if(app.client.maintenance) return;

                    app.ui.log('Webservice entered maintenance mode, keeping requests to run them when maintenance ends');
                    if(app.ui.transfer) app.ui.transfer.maintenance(true);

                    app.client.maintenance = window.setInterval(function() {
                        app.client.get('/info', function(info) {
                            // Got data instead of "undergoing_maintenance" exception, maintenance is over, lets restart stacked requests
                            window.clearInterval(app.client.maintenance);
                            app.client.maintenance = false;

                            app.ui.maintenance(false);

                            app.ui.log('Webservice maintenance mode ended, running pending requests');

                            if(app.ui.transfer) app.ui.transfer.maintenance(false);
                            for(var i=0; i<app.client.pending_requests.length; i++)
                                jQuery.ajax(app.client.pending_requests[i]);

                            app.ui.log('Ran all pending requests from webservice maintenance period');
                        }, {maintenanceCheck: true});
                    }, 60 * 1000);

                    app.client.pending_requests.push(settings);

                    app.ui.maintenance(true);

                    return;
                }

                errorhandler(error);
            } catch(e) {
                app.ui.rawError(msg);
            }
        };

        for(k in options) settings[k] = options[k];

        if(this.maintenance && !options.maintenanceCheck) {
            this.pending_requests.push(settings);
            return;
        }
        
        return jQuery.ajax(settings);
    },
    
    get: function(resource, callback, options) {
        return this.call('get', resource, undefined, callback, options);
    },
    
    post: function(resource, data, callback, options) {
        return this.call('post', resource, data, function(data, status, xhr) {
            callback.call(this, xhr.getResponseHeader('Location'), data);
        }, options);
    },
    
    put: function(resource, data, callback, options) {
        return this.call('put', resource, data, callback, options);
    },
    
    delete: function(resource, callback, options) {
        return this.call('delete', resource, undefined, callback, options);
    }
};
