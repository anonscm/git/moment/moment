/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * Handles Callbacks
 */
window.app.event =  {
    /**
     * Trigger application event
     * 
     * @param {string} event_name
     * @param {object} data
     */
    trigger: function(event_name, data) {
        $(document).trigger('app.' + event_name, data);
    },
    
    /**
     * Register a listener
     * 
     * @param{string} event_name
     * @param {function} callback
     */
    on: function(event_name, callback) {
        $(document).on('app.' + event_name, callback);
    }
};
