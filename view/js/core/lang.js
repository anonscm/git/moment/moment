/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

/**
 * Client side language handling
 */

if (!window.hasOwnProperty('app'))
    window.app = {};

window.app.lang = {
    /**
     * Translations dictionary
     */
    dictionary: {},
    
    /**
     * Storage key
     */
    store_key: 'app_lang_store',
    
    /**
     * Load dictionary
     * 
     * Checks local storage, fetches from API if either not up to date, not already loaded or unavailable
     * 
     * @param {function} callback
     */
    loadDictionary: function(callback) {
        var b = $('body');
        var vid = b.attr('data-dictionnary-uid');
        
        if(app.utilities.isLocalStorageAvailable()) {
            var store = localStorage.getItem(this.store_key);
            if(store) store = JSON.parse(store);
            
            if(store && store.vid === vid) {
                this.dictionary = store.dictionary;
                
                callback();
                
                return;
            }
        }
        
        app.client.get('/lang', function(dictionary) {
            app.lang.dictionary = dictionary;
            
            if(app.utilities.isLocalStorageAvailable())
                localStorage.setItem(
                    app.lang.store_key,
                    JSON.stringify({vid: vid, dictionary: dictionary})
                );
            
            callback();
        }, {
            error: callback
        });
    },
    
    /**
     * Drop locally stored dictionary
     */
    dropDictionary: function() {
        if(!app.utilities.isLocalStorageAvailable()) return;
        
        localStorage.removeItem(this.store_key);
    },
    
    /**
     * Set dictionary
     */
    setDictionary: function(dictionary) {
        this.dictionary = dictionary;
    },
    
    /**
     * Translation string class, handles replacements
     */
    translation: function(text, allow_replace) {
        this.text = text;
        this.allow_replace = allow_replace;
        
        this.replace = function(placeholder, value) {
            if(!this.allow_replace)
                return this;

            if (typeof placeholder === 'string')
                placeholder = {placeholder: value};

            var text = this.text;
            for(var k in placeholder)
                text = text.replace('{' + k + '}', placeholder[k]);
            
            return new app.lang.translation(text, true);
        };
        
        this.r = function(placeholder, value) {
            return this.replace(placeholder, value);
        };
        
        this.out = function() {
            return this.text;
        };
        
        this.values = function(separator) {
            var values = this.text.split(separator ? separator : ',');
            for(var i=0; i<values.length; i++)
                values[i] = values[i].replace(/^\s+/g, '').replace(/\s+$/g, '');
            return values;
        };
        
        this.toString = function() {
            return this.out();
        };
        
        this.valueOf = function() {
            return this.out();
        };
    },
    
    translate: function(id) {
        id = id.toLowerCase();

        if (typeof this.dictionary[id] === 'undefined') {
            if (app.config.debug)
                console.log('No translation found for "' + id + '"');

            return new this.translation('{' + id + '}');
        }

        return new this.translation(this.dictionary[id], true);
    },
    
    tr: function(id) {
        return this.translate(id);
    },

    getCode: function(){
        return $('html').attr('lang');
    }
};

// Shorthand
window.lang = window.app.lang;
