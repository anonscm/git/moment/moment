/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * Handles Callbacks
 */
window.app.session =  {
    /**
     * Session holder
     */
    user: null,
    
    /**
     * Load session data
     * 
     * @param {function} callback
     * @param {boolean} refresh force reload
     */
    load: function(callback, refresh) {
        if(this.user && !refresh) {
            callback(this.user);
            return;
        }
        
        app.client.get('/user/@me', function(user) {
            app.session.user = user;
            
            callback(user);
        });
    },
    
    /**
     * Get user
     * 
     * @see load
     */
    get: function(callback) {
        this.load(callback, false);
    },
    
    /**
     * Trigger session openning
     */
    open: function() {
        app.ui.redirect(app.config.logon_url);
    },
    
    /**
     * Trigger session close
     */
    close: function() {
        app.ui.redirect(app.config.logoff_url);
    }
};
