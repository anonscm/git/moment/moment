/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * Handles Callbacks
 */
window.app.utilities = {
    /**
     * Get fine type
     * 
     * @param {any|object} thing
     * 
     * @return string
     */
    whatIs: function (thing) {
        var t = typeof thing;
        if (t !== 'object')
            return t;

        if (thing === null)
            return 'null';

        var c = Object.prototype.toString.call(thing);

        t = c.match(/^\[object\s+(.+)\]$/);
        if (!t)
            return 'undefined';
        t = t[1].toLowerCase();

        return t.match(/^(array|object)$/) ? t : 'object:' + t;
    },

    /**
     * Crawl through an object
     * 
     * @param {object} obj object to explore
     * @param {function} func callable, will receive path as first argument and value as second, ctx will be propagated as context
     * @param {any} ctx context for func to run in
     * @param {string} path
     */
    crawlObject: function (obj, func, ctx, path) {
        var type = this.whatIs(obj);
        if (!path)
            path = '';

        if (!type.match(/^(array|object:?)/)) {
            func.call(ctx, path, obj);

        } else if (type === 'array') {
            for (var i = 0; i < obj.length; i++)
                this.crawlObject(obj[i], func, ctx, path + '[' + i + ']');

        } else {
            for (var k in obj)
                this.crawlObject(obj[k], func, ctx, path + (path ? '.' : '') + k);
        }
    },

    /**
     * Put to uppercase first letter of each word in a string
     *
     * @param {string} str String to convert
     */
    ucwords: function (str) {
        str = str.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
        return str;
    },

    /**
     * Allows to know if localstorage is available
     *
     * @returns {Boolean} True if available, false otherwhise
     */
    isLocalStorageAvailable: function () {
        return ('localStorage' in window) && (window.localStorage !== null);
    },

    /**
     * Checks if a value exists in an array
     * 
     * @param {string} needle The searched value.
     * @param {object} haystack The array
     *
     * @returns {boolean}
     */
    inArray: function (needle, haystack) {
        var length = haystack.length;
        for (var i = 0; i < length; i++) {
            if (haystack[i] === needle)
                return true;
        }
        return false;
    },

    /**
     * Convert localized, human readable date to epoch
     *
     * @param {string} date
     * @param {boolean} has_time
     *
     * return {integer}
     */
    humanDateToEpoch: function (date, has_time) {
        var fmt = app.lang.tr(has_time ? 'moment_datetime_format' : 'moment_date_format').out();
        return parseInt(moment(date, fmt).format('X'));
    },

    /**
     * This function allows to store a cookie.
     * 
     * @param {string} cname: The cookie name
     * @param {string} cvalue: The value to store
     * @param {integer} exdays: Cookie duration
     *
     * @todo use jquery cookie
     */
    setCookie: function (cname, cvalue, exdays) {
        if (!exdays)
            exdays = app.config.max_cookie_duration;

        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = 'expires=' + d.toUTCString();
        document.cookie = cname + '=' + cvalue + '; ' + expires;
    },

    /**
     * This function allows to get a cookie.
     *
     * @param {string} cname: The cookie name
     *
     * @returns {string}
     *
     * @todo use jquery cookie
     */
    getCookie: function (cname) {
        var name = cname + '=';
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ')
                c = c.substring(1);
            if (c.indexOf(name) === 0)
                return c.substring(name.length, c.length);
        }
        return '';
    },

    /**
     * Launch parallel actions and run callback at the end
     * 
     * @param {object} actions array of callables
     * @param {function} callback
     * 
     * @return {object} tracker
     */
    parallelize: function (actions, callback) {
        var tracker = {
            done: 0,
            total: actions.length,
            callback: callback
        };

        for (var i = 0; i < actions.length; i++)
            actions[i](function () {
                tracker.done++;

                if (tracker.done < tracker.total)
                    return;

                var callback = tracker.callback;
                tracker.callback = null; // Avoid multiple runs

                if (callback)
                    callback();
            });

        return tracker;
    },
    timestampToFormatedTime: function (t) {
        var placeholders = {
            h: Math.floor(t / 3600), // Number of hours
            i: Math.floor(((t / 60) % 60))  // Number of minutes
        };
        placeholders.H = (placeholders.h < 10 ? '0' : '') + placeholders.h;
        placeholders.i = (placeholders.i < 10 ? '0' : '') + placeholders.i;

        var time = lang.tr('time_format').out();

        for (var k in placeholders) {
            time = time.replace(k, placeholders[k]);
        }

        return time;
    },
    
    
    emailValidationPattern: /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i,
    /**
     * Check wether an email address is valid
     *
     * @param {string} emailAddress
     *
     * @return {boolean}
     */
    isValidEmailAddress: function (emailAddress) {
        return !!emailAddress.match(this.emailValidationPattern);
    },

    /**
     * Dups object
     *
     * @param {object} o
     *
     * @returns {object}
     */
    clone: function(o, deep) {
        if(typeof o !== 'object')
            return o;

        var n = {};
        for(var k in o)
            if(o.hasOwnProperty(k))
                n[k] = deep ? this.clone(o[k]) : o[k];

        return n;
    },

    /**
     * Bind callable context
     *
     * @param {object} context
     * @param {function} callable
     *
     * @returns {function}
     */
    bind: function(context, callable) {
        return function() {
            return callable.apply(context, arguments);
        };
    }
};

// Fix bug in Safari and IE
String.prototype.startsWith = function (prefix) {
    return this.indexOf(prefix) === 0;
};
