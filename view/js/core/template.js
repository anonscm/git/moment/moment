/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * Provides view constructor
 * 
 * @param {string|object} src template identifier or existing node
 * @param {object} parent view to use as parent
 */
window.app.template = function(src, parent) {
    var tpl = {
        parent: parent,
        
        /**
         * String casting
         * 
         * @return {string}
         */
        toString: function() {
            var ids = [], v = this;
            
            do {
                var id = '?';
                if(v.tpl) {
                    id = 'tpl(' + v.tpl.attr('data-template') + ')';
                    
                } else if(v.node.attr('data-section')) {
                    id = 'section(' + v.node.attr('data-section') + ')';
                }
                
                ids.unshift(id);
                
                v = v.parent;
            } while(v);
            
            return ids.join('/');
        },

        /**
         * Set properties placeholders values
         * 
         * @param {string|object} p property name or object listing properties
         * @param {any} v property value (if previous is a propert name)
         * 
         * @return {object} the template
         */
        property: function(p, v) {
            var k;
            if (typeof p === 'string') {
                k = p;
                p = {};
                p[k] = v;
            }
            
            var not = [];
            
            if(this.node.attr('data-section')) {
                var sid = this.node.attr('data-section');
                not.push('[data-section="' + sid + '"] [data-section] [data-property]');
                
            } else {
                not.push('[data-section] [data-property]');
            }
            
            for(k in p) // Replace in properties of the current view, not in subtemplates
                this.node.find('[data-property="' + k + '"]:not(' + not.join(', ') + ')').text(p[k]);
            
            return this;
        },

        /**
         * Get sub section as a view
         * 
         * @param {string} section_id
         * 
         * @return {object} template
         */
        section: function(section_id) {
            var section_node = this.node.find('[data-section="' + section_id + '"]');
            
            if(!section_node.length)
                throw 'section_not_found';
            
            return app.template(section_node, this);
        },

        /**
         * Get sub template
         * 
         * @param {string} tpl_id
         * 
         * @return {object} template
         */
        subTemplate: function(tpl_id) {
            return app.template(tpl_id, this);
        },

        /**
         * Bind listener to click on action sub-node
         * 
         * @param {string} action
         * @param {function} handler
         * 
         * @return {object} the template
         */
        onAction: function(action, handler) {
            action = action.split(':');
            if (action.length < 2) action.push('click');
            
            var tpl = this.node;
            this.node.find('[data-action="' + action[0] + '"]').filter(function() {
                return $(this).closest('[data-template]').is(tpl);
            }).on(action[1], function(event) {
                var view = $(this).closest('[data-view]').data('view');

                if (typeof handler === 'function') {
                    handler.call(this, event, view);

                } else if (handler[action[0]]) {
                    handler[action[0]].call(handler, action[1], view);
                }
            });

            return this;
        },

        /**
         * Add / get attributes
         * 
         * @param {string|object} what
         * 
         * @return {string|object} the template / attribute value
         */
        attr: function(what) {
            if (typeof what === 'string') {
                if (what.substr(0, 5) !== 'data-')
                    what = 'data-' + what;

                return this.node.attr(what);
            }
            
            for(var k in what) {
                if (k.substr(0, 5) !== 'data-') {
                    what['data-' + k] = what[k];
                    delete what[k];
                }
            }
            
            this.node.attr(what);
            
            return this;
        }
    };
    
    if(src.jquery) { // Existing node
        tpl.node = src;
        
    } else { // Clone template from id
        var sel = '[data-template="' + src + '"]';
        
        if(parent) {
            if(parent.node) {
                tpl.node = parent.node.find(sel + ':not([data-template="' + parent.node.attr('data-template') + '"] [data-template] [data-template])');
                
            } else {
                if(!parent.jquery)
                    parent = $(parent);
                
                tpl.node = parent.find(sel);
            }
            
        } else {
            tpl.node = $('body > ' + sel);
        }
        
        if(!tpl.node.length)
            throw 'template_not_found';
    }
    
    return tpl;
};
