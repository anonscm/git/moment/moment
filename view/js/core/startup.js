/**
 * This file is part of the ApplicationBase project.
 * 2015 
 * Copyright (c) RENATER
 */

/**
 * Application startup file
 */

if (!window.hasOwnProperty('app'))
    window.app = {};

$(document).ready(function() {
    // Load non-static dependencies in parallel
    app.utilities.parallelize([
        // Translation dictionary
        function(done) {
            app.lang.loadDictionary(done);
        },
        
        // User session
        function(done) {
            app.session.load(done);
        }
    ], function() {
        var lang = $('html').attr('lang');

        // Setup date picker
        jQuery.datetimepicker.setLocale(lang);
        
        // Add session_ended and session_started event
        // Use to check user session status
        var interval = app.config.session_check_interval ;
        if (!interval) interval = 300000;
        
        window.setInterval(function(){
            var session = !!app.session.user;
            app.session.load(function(user){
                if (session && !user){
                    // Session ended
                    app.event.trigger('session_ended');
                }
                
                if (!session && user){
                    // Session started
                    app.event.trigger('session_started');
                }
            }, true);
        }, interval);

        // Init moment locale
        moment.locale(lang);
        
        // Propagate event upon startup completion
        app.event.trigger('started');
    });
    
    // Tooltips and such
    $(document).foundation();
    
    // Top button appears with page scrolling
    $(document).on( 'scroll', function(){
        if ($(window).scrollTop() > 100) {
            $('.scroll-top-wrapper').addClass('show');
        } else {
            $('.scroll-top-wrapper').removeClass('show');
        }
    });
    
    var scrollToTop = function() {
        var element = $('body');
        var offset = element.offset();
        var offsetTop = offset.top;
        $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
    };
    
    $('.scroll-top-wrapper').on('click', scrollToTop);    
    
    $('header [data-action="select_language"] [data-lang]').on('click', function() {
        var sel = $(this).attr('data-lang');
        if(sel === $('html').attr('lang')) return;
        
        app.ui.redirect(null, {lang: sel});
    });
});
