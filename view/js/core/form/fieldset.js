/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

/* global app, moment */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * Form block constructor
 *
 * @class
 *
 * @param {object} node DOM node or jQuery object
 * @param {object} parent
 */
window.app.form.fieldset = function(node, parent) {
    this.type = 'fieldset';
    this.parent = parent.form ? parent : null;
    this.form = parent.form ? parent.form : parent;
    this.node = node;
    this.nest_level = parent ? parent.nest_level : 0;
    this.node.attr({'data-nest-level': this.nest_level});

    this.node.data('fieldset', this);

    this.fields = {};
    this.explore();
};

/**
 * Explore and gather sub fields
 */
window.app.form.fieldset.prototype.explore = function() {
    var inputs = this.node.find(':input:not([type="radio"], button, [data-nested="' + this.nest_level + '"] :input)');

    inputs.add('[data-radio]:not([data-nested="' + this.nest_level + '"])', this.node);

    var fs = this;
    var radios = {};
    inputs.each(function() {
        var i = $(this);
        if(i.is(':radio')) {
            var r = i.closest('[data-radio]').attr('data-radio');
            if (r in radios)
                return;

            radios[r] = true;
        }

        var f = new app.form.field(i, fs);
        f.fieldset = fs;
        fs.fields[f.name] = f;
    });

    // At bottom
    if(this.nest_level === this.form.nesting) return;

    // Sub nests
    this.node.find('[data-multiple][data-nested="' + this.nest_level + '"]').each(function() {
        var m = new app.form.multiple($(this), fs);
        m.fieldset = fs;
        fs.fields[m.name] = m;
    });

    // Sub nests
    this.node.find('[data-indexed-multiple][data-nested="' + this.nest_level + '"]').each(function() {
        var m = new app.form.indexed_multiple($(this), fs);
        m.fieldset = fs;
        fs.fields[m.name] = m;
    });
};

/**
 * Get path
 *
 * @returns {string}
 */
window.app.form.fieldset.prototype.getPath = function() {
    return this.parent ? this.parent.getPath() : '';
};

/**
 * Get data
 *
 * @returns {app.promise}
 */
window.app.form.fieldset.prototype.getData = function() {
    var getters = [];
    for(var k in this.fields)
        if(this.fields.hasOwnProperty(k))
            getters.push(this.fields[k].getData(true)); // Get as name-data pairs instead of just data

    // Restore mapping
    return app.promise.all(getters, this, [], function(rawdata) {
        var data = {};
        for(var i=0; i<rawdata.length; i++)
            data[rawdata[i].name] = rawdata[i].data;

        return data;
    });
};

/**
 * Set data
 *
 * @param {object} data
 */
window.app.form.fieldset.prototype.setData = function(data) {
    var k;

    if(typeof data === 'object') {
        for (k in data)
            if (data.hasOwnProperty(k) && this.fields[k])
                this.fields[k].setData(data[k]);

    } else {
        var f = null;
        var fc = 0;
        for(k in this.fields) {
            if (this.fields.hasOwnProperty(k)) {
                fc++;
                if(!f)
                    f = this.fields[k];
            }
        }

        if(fc === 1)
            f.setData(data);
    }
};
