/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

/* global app, moment */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * Form field handler
 *
 * @class
 *
 * @property form {app.form}
 *
 * @param {object} input
 * @param {object} parent
 */
window.app.form.field = function(input, parent) {
    this.parent = parent;
    this.form = parent.form;
    this.nest_level = parent.nest_level;

    this.input = $(input);

    this.type = this.input[0].tagName.toLowerCase();
    if(this.type === 'input') {
        this.type = this.input.attr('type');

        if(this.type === 'text') {
            var dt = this.input.attr('data-picker');
            if(dt)
                this.type = dt;
        }
    }

    if(this.type === 'radio') {
        this.node = this.input.closest('[data-radio]');
        this.name = this.node.attr('data-radio');

        this.input = this.node.find(':radio');

        this.constraints = {required: this.node.is('[data-mandatory]')};

    } else {
        this.node = this.input.closest('label');
        this.name = this.input.attr('name');

        this.constraints = {required: this.input.is('[required]')};

        switch(this.type) {
            case 'date':
            case 'datetime':
            case 'time':
                this.constraints.min = parseInt(this.input.attr('data-picker-min'));
                this.constraints.max = parseInt(this.input.attr('data-picker-max'));
                break;

            case 'number':
            case 'range':
                this.constraints.min = parseInt(this.input.attr('min'));
                this.constraints.max = parseInt(this.input.attr('max'));
                break;

            case 'text':
            case 'textarea':
            case 'email':
            case 'url':
            case 'password':
                this.constraints.maxlength = parseInt(this.input.attr('maxlength'));
                break;
        }
    }

    for(var k in this.constraints)
        if(this.constraints.hasOwnProperty(k))
            if(this.constraints[k] === undefined || isNaN(this.constraints[k]))
                delete this.constraints[k];

    this.form.registerField(this);
    this.node.attr({
        'data-nest-level': this.nest_level,
        'data-field-path': this.getPath()
    });

    this.node.data('field', this);

    if(this.input.is('[data-picker]')) {
        // datetimepicker parameters
        var options = {
            scrollInput: false
        };

        var type = this.input.attr('data-picker');
        switch (type){
            case 'date':
                // Do not show date picker
                options.timepicker = false;
                options.format = lang.tr('dp_date_format').out();
                options.formatDate = options.format;
                break;

            case 'datetime':
                options.format = lang.tr('dp_datetime_format').out();
                options.formatDate = options.format;
                break;

            case 'time':
                // Do not show date picker
                options.datepicker = false;
                options.format = lang.tr('dp_time_format').out();
                break;
        }

        // Setting start date
        var startDate = this.input.attr('data-picker-start-date');
        if (typeof startDate !== 'undefined')
            options.startDate = startDate;

        // Setting default time
        var defaultTime = this.input.attr('data-picker-default-time');
        if (typeof defaultTime !== 'undefined')
            options.defaultTime = app.utilities.timestampToFormatedTime(defaultTime);

        // Getting minimum value for current input
        var min = this.input.attr('data-picker-min');
        min = (typeof min !== 'undefined' && !isNaN(min)) ? parseInt(min) : null;

        // Getting maximum value for current input
        var max = this.input.attr('data-picker-max');
        max = (typeof max !== 'undefined' && !isNaN(max)) ? parseInt(max) : null;

        if(type === 'time') {

            // Set allowed values for time picker
            var allowedTimes = [];
            if (min === null) min = 0;
            if (max === null) max = 86399; // 3600*24 -1

            var step = this.input.attr('data-picker-step');
            step = (typeof step !== 'undefined' && !isNaN(step)) ? parseInt(step) : 3600;


            for (var t = min; t<=max; t+=step){
                allowedTimes.push(app.utilities.timestampToFormatedTime(t));
            }

            // Init time picker
            options.allowTimes = allowedTimes;

        }else {
            // Translate moment in user timezone
            var tr = 'moment_date_format';

            if(type === 'datetime')
                tr = 'moment_datetime_format';

            tr = lang.tr(tr).out();

            if (min !== null)
                options.minDate = moment(min,'X').format(tr);

            if (max !== null)
                options.maxDate = moment(max,'X').format(tr);
        }

        // Init date picker
        this.input.datetimepicker(options);
    }

    var h = this.node.find('[data-hint]');
    if(h.length) {
        this.node.attr({title: h.text()});
        h.remove();
    }

    var field = this;
    if (this.node.closest('[data-multiple-emails]').length){
        // Adding multiple emails in one shot
        this.input.on('change', function() {
            var value = $(this).val();
            if (!value) return;

            value = value.split(app.form.field.multipleEmails.separatorPattern);
            var emails = [];
            for (var i = 0; i < value.length ; i ++){
                if (value[i]) emails.push(value[i]);
            }

            // First entry
            $(this).val(emails.shift());

            var multiple = $(this).closest('[data-multiple-emails]').data('multiple');
            var ref = $(this).closest('[data-entry]').data('entry');

            // Create fields
            while (emails.length){
                ref = multiple.addEntry({email: emails.shift()}, ref.node);
            }
            field.displayError(null);
            multiple.getData(); // Validates
        });

    }else{
        this.input.on('change', function() {
            field.displayError(null);
            field.getData(); // Validates
        });
    }

    if(this.type === 'range') {
        this.preview = $('<span class="preview" data-range="' + this.input.attr('name') + '" />').insertAfter(this.input).text(this.input.val());

        this.input.css({width: '95%'}).on('input change', function() {
            field.preview.text($(this).val());
        });
    }

    this.input.on('input, change', function(){
        $(this).trigger(jQuery.Event('field.change', {
            field: field
        }));
    });
};

/**
 * Multiple emails format specification
 *
 * @type {{separator_pattern: RegExp}}
 */
window.app.form.field.multipleEmails = {
    separatorPattern: /[\s,;:]+/
};


/**
 * Get path
 *
 * @returns {string}
 */
window.app.form.field.prototype.getPath = function() {
    if(this.path === undefined) {
        this.path = this.parent ? this.parent.getPath() : '';
        this.path += (this.path ? '.' : '') + this.name;
    }

    return this.path;
};

/**
 * Get raw data
 *
 * @returns {*}
 */
window.app.form.field.prototype.getRawData = function() {
    var d;

    switch (this.type) {
        case 'date':
            d = this.input.datetimepicker('getValue');
            if (d) {
                // We want only the date, not the hours/minutes/seconds
                d = parseInt(d.getTime() / 1000) - (d.getTimezoneOffset() * 60);
                d -= (d % 86400);
            }
            return d;

        case 'datetime':
            d = this.input.datetimepicker('getValue');
            return d ? parseInt(d.getTime() / 1000) : null;

        case 'time':
            d = this.input.datetimepicker('getValue');
            return d ? ( d.getHours() * 3600 ) + (d.getMinutes() * 60) : null;

        case 'number':
            if (this.input.attr('step')) {
                var step = parseInt(this.input.attr('step'));

                if (step == this.input.attr('step'))
                    return parseInt(this.input.val());
                else
                    return parseFloat(this.input.val());
            }
            return parseInt(this.input.val());

        case 'range':
            return parseInt(this.input.val());

        case 'checkbox':
            return this.input.is(':checked');

        case 'radio':
            d = this.input.filter(':checked').val();
            return (d === undefined) ? '' : d;

        case 'select':
            return this.input.val();

        case 'text':
        case 'textarea':
        case 'email':
        case 'url':
        case 'password':
        case 'hidden':
            return this.input.val();
    }
};

/**
 * Get and validate field data
 *
 * @param {boolean} [get_obj] get name to value object instead of just value
 *
 * @returns {app.promise}
 */
window.app.form.field.prototype.getData = function(get_obj) {
    return new app.promise(
        function(fulfill, reject) {
            var data = this.getRawData();

            if(data === undefined) {
                reject({subject: this, reason: 'unknown_type'});
                return;
            }

            if (this.constraints.maxlength !== undefined) {
                if (data.length > this.constraints.maxlength) {
                    reject({subject: this, reason: 'too_long'});
                    return;
                }
            }

            if (this.constraints && this.constraints.required) {
                if (data === null || !('' + data).length) {
                    reject({subject: this, reason: 'value_required'});
                    return;
                }
            }

            if (this.type === 'email') {
                if (data && !app.utilities.isValidEmailAddress(data)) {
                    reject({subject: this, reason: 'bad_email'});
                    return;
                }
            }

            if (this.constraints.min !== undefined) {
                if (data < this.constraints.min) {
                    reject({subject: this, reason: 'value_too_low'});
                    return;
                }
            }

            if (this.constraints.max !== undefined) {
                if (data > this.constraints.max) {
                    reject({subject: this, reason: 'value_too_high'});
                    return;
                }
            }

            if ((this.type !== 'radio') && this.input[0].checkValidity) {
                if (!this.input[0].checkValidity()) {
                    reject({subject: this, reason: 'bad_' + this.type});
                    return;
                }
            }

            app.promise.all(this.form.getFieldValidators(this, data), this).then(
                function() {
                    fulfill(get_obj ? {name: this.name, data: data} : data);
                },
                reject
            );
        },
        /* promise context is the entry */
        this
    ).catch(
        function(error) {
            var e = jQuery.Event('field.error', {
                field: this
            });
            this.input.trigger(e);
            if (!e.isDefaultPrevented())
                this.displayError(error.reason);
        }
    );
};

/**
 * Set data
 *
 * @param {object} data
 */
window.app.form.field.prototype.setData = function(data) {
    if(this.type === 'radio') {
        this.input.prop({checked: false}).filter('[value="' + data + '"]').prop({checked: true});

    } else {
        this.input.val(data);
    }
};

/**
 * Display entry error
 *
 * @param {string} error (translatable)
 */
window.app.form.field.prototype.displayError = function(error) {
    this.form.displayError(error, this);
};
