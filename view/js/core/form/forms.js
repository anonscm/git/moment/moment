/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

/* global app, moment */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * Form handler
 *
 * @param {object|string} node Current form node
 */
window.app.forms = {
    registry: {},

    register: function(form) {
        this.registry[form.id] = form;
    },

    get: function(id) {
        if(this.registry[id])
            return this.registry[id];

        var node = $('[data-form="' + id + '"]');

        if(!node.length)
            throw new Error('Unnown form ' + id);

        var form = new app.form(node);
        this.register(form);

        return form;
    },

    exists: function(id){
        return $('[data-form="' + id + '"]').length;
    }
};

// Init fields and forms
app.event.on('started', function() {
    $('[data-form]').each(function() {
        app.forms.register(new app.form(this));
    });
    $('select[multiple]').multiselect({
        texts: {
            placeholder    : lang.tr('mlt_select_placeholder'),    // text to use in dummy input
            search         : lang.tr('mlt_select_search'),         // search input placeholder text
            selectedOptions: lang.tr('mlt_select_selected'),       // selected suffix text
            selectAll      : lang.tr('mlt_select_select_all'),     // select all text
            unselectAll    : lang.tr('mlt_select_unselect_all'),   // unselect all text
            noneSelected   : lang.tr('mlt_select_none_selected')   // None selected text
        }
    });
});

$(window).on('beforeunload', function(){
    for (var k in window.app.forms.registry){
        if (window.app.forms.registry[k].warnEdited && window.app.forms.registry[k].status == 'edited')
            return lang.tr('confirm_leave_page').out(); // Ask for leaving confirmation
    }
});