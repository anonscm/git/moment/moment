/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

/* global app, moment */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * Form indexed_multiple constructor
 *
 * @class
 *
 * @param {object} node DOM node or jQuery object
 * @param {object} parent
 */
window.app.form.indexed_multiple = function(node, parent) {
    this.type = 'indexed-multiple';
    this.parent = parent;
    this.form = parent.form;
    this.node = node;
    this.nest_level = parent ? parent.nest_level : 0;
    this.node.attr({'data-nest-level': this.nest_level});

    this.tpl = this.node.clone();
    this.tpl.find('> legend').remove();
    this.tpl.removeAttr('data-indexed-multiple data-min-count data-max-count data-start data-lock data-indexes');

    this.start = JSON.parse(this.node.attr('data-values'));
    if(!this.start){
        this.start = {};
        var idx = JSON.parse(this.node.attr('data-start'));
        for (var i = 0 ; i < idx.length ; i++){
            this.start[idx[i]] = null;
        }
    }

    this.lock = JSON.parse(this.node.attr('data-lock'));
    this.indexes = JSON.parse(this.node.attr('data-indexes'));

    this.name = this.node.attr('data-indexed-multiple');
    this.entryLabel = this.node.attr('data-entry-label');

    this.min = parseInt(this.node.attr('data-min-count'));
    this.max = parseInt(this.node.attr('data-max-count'));

    this.node.data('indexed-multiple', this);

    this.entries = {};

    this.setup();
};

/**
 * Init contents
 */
window.app.form.indexed_multiple.prototype.setup = function() {
    var legend = this.node.find('> legend');
    this.node.empty();
    if(legend.length) legend.appendTo(this.node);

    this.add_ctrl = $('<button class="button" data-action="add" />').text(lang.tr('add')+'');
    this.add_ctrl.prepend('<span class="fa fa-plus-circle" /> ');

    this.add_select = $('<select />');
    for (var k in this.indexes){
        $('<option />').attr({value: k}).append(this.indexes[k]).appendTo(this.add_select);
    }

    var m = this;
    this.add_ctrl.appendTo(this.node).on('click', function() {
        if (!m.add_select.val()) return;
        m.addEntry(m.add_select.val(), {}, null);
        m.node.trigger(jQuery.Event('field.change', {field:m}));
    });

    this.add_select.insertBefore(this.add_ctrl);

    for (var k in this.start)
        this.addEntry(k, this.start[k], null);

    app.event.trigger('form.indexed_multiple.ready', this);
};

/**
 * Update multiple block buttons
 */
window.app.form.indexed_multiple.prototype.updateButtons = function() {
    this.add_select.find('option').show().removeClass('hidden');

    var m = this;
    this.node.find('[data-index][data-nest-level="' + (this.nest_level + 1) + '"]').each(function(){
        m.add_select.find('[value="' + $(this).attr('data-index') + '"]').hide().addClass('hidden');
    });
    this.add_select.val(this.add_select.find('option:not(.hidden)').eq(0).attr('value'));

    this.add_ctrl.toggleClass('disabled', !this.add_select.find('option:not(.hidden)').length);

    var over_max = this.max && (this.entries.length >= this.max);
    this.node.find('[data-action="add"]:not([data-nested="' + (this.nest_level + 1) + '"] [data-action])').prop('disabled', over_max);

    var under_min = this.min && (this.entries.length <= this.min);
    this.node.find('[data-action="del"]:not([data-nested="' + (this.nest_level + 1) + '"] [data-action])').prop('disabled', under_min);
};

/**
 * Create multiple entry
 *
 * @param {string} index
 * @param {object} insertAfter
 *
 * @returns {app.form.indexed_multiple.entry}
 */
window.app.form.indexed_multiple.prototype.createEntry = function(index, insertAfter) {
    var node = this.tpl.clone();
    var ctn = $('<div class="small-10 columns" />').append(node.children());
    node.empty().append(ctn);

    if (insertAfter)
        node.insertAfter(insertAfter);
    else
        node.insertBefore(this.add_select);

    return new app.form.indexed_multiple.entry(index, node, this);
};

/**
 * Add entry to multiple
 *
 * @param {string} index
 * @param {object} values
 * @param {object} insertAfter
 */
window.app.form.indexed_multiple.prototype.addEntry = function(index, values, insertAfter) {
    var entry = null;

    if((!this.max || (this.entries.length < this.max)) && this.node.find('[data-index="' + index + '"]').length === 0) {
        entry = this.createEntry(index, insertAfter);

        if(values)
            entry.setData(values);

        var fc = 0;
        for(var k in entry.fieldset.fields)
            if(entry.fieldset.fields.hasOwnProperty(k))
                fc++;

        if(fc === 1 && !entry.node.find('[data-label-text]').length && !entry.node.find('[data-label-constraints]').length)
            entry.node.attr({'data-single-field-no-label': 1});

        if (fc === 1){
            var label = entry.node.find('[data-label-text]');
            if (!label.length)
                label = $('<span data-label-text/>').prependTo(entry.node);

            var txt = this.indexes[index];

            if (this.entryLabel)
                txt = this.entryLabel.replace('{index}', txt);

            label.text(txt);
        }else{
            // TODO
            console.log('TODO');
        }

        entry.del_ctrl = $('<button class="button" data-action="del" />').text(lang.tr('delete') + '');
        entry.del_ctrl.prepend('<span class="fa fa-trash" /> ');

        var ctn = $('<div class="small-2 columns text-center" />').appendTo(entry.node);
        ctn.css({height: entry.node.find('> div').height() + 'px'});

        entry.del_ctrl.appendTo(ctn).on('click', function () {
            entry.multiple.deleteEntry(entry);
            entry.multiple.node.trigger(jQuery.Event('field.change', {field:entry}));
        });

        entry.del_ctrl.css({'margin-top': 'calc(' + (entry.node.find('> div').height() / 2) + 'px - 1rem)'});

        if (this.isLocked(index))
            entry.del_ctrl.addClass('disabled');

        this.entries[index] = entry;
        entry.node.find(':input:first').focus();

        // Refresh buttons
        this.updateButtons();
    }

    return entry;
};


/**
 * Check if elemtn is locked
 *
 * @param index
 */
window.app.form.indexed_multiple.prototype.isLocked = function(index){
    if (this.lock){
        for (var i = 0 ; i < this.lock.length ; i++){
            if (this.lock[i] === index) return true;
        }
    }
    return false;
};

/**
 * Delete multiple block entry
 *
 * @param {object} entry
 */
window.app.form.indexed_multiple.prototype.deleteEntry = function(entry) {
    if (this.isLocked(entry.index))
        return;

    if(!this.min || (this.entries.length > this.min)) {
        entry.node.remove();
        delete this.entries[entry.index];

        // Refresh buttons
        this.updateButtons();
    }
};

/**
 * Get path
 *
 * @returns {string}
 */
window.app.form.indexed_multiple.prototype.getPath = function() {
    if(this.path === undefined) {
        this.path = this.parent ? this.parent.getPath() : '';
        this.path += (this.path ? '.' : '') + this.name;
    }

    return this.path;
};

/**
 * Get data
 *
 * @param {boolean} [get_obj] get name to value object instead of just value
 *
 * @returns {app.promise}
 */
window.app.form.indexed_multiple.prototype.getData = function(get_obj) {
    var getters = [];
    for(var k in this.entries)
        getters.push(this.entries[k].getData(true)); // Get as index-data pairs instead of just data

    // Restore order
    return app.promise.all(getters, this, [], function(rawdata) {
        if (rawdata == undefined)
            return get_obj ? {name: this.name, data: []} : [];

        var data = {};
        for(var i=0; i<rawdata.length; i++)
            data[rawdata[i].index] = rawdata[i].data;

        return get_obj ? {name: this.name, data: data} : data;
    });
};

/**
 * Multiple entry
 *
 * @class
 *
 * @param {string} index
 * @param {object} node DOM node or jQuery object
 * @param {app.form.indexed_multiple} multiple
 */
window.app.form.indexed_multiple.entry = function(index, node, multiple) {
    this.type = 'multiple-entry';
    this.multiple = multiple;
    this.form = multiple.form;
    this.nest_level = multiple.nest_level + 1;
    this.node = node;
    this.index = index;
    this.node.attr({'data-index': this.index});

    this.node.data('entry', this);

    this.fieldset = new app.form.fieldset(node, this);
};

/**
 * Get path
 *
 * @returns {string}
 */
window.app.form.indexed_multiple.entry.prototype.getPath = function() {
    if(this.path === undefined) {
        this.path = this.multiple.getPath();
        this.path += '[' + this.index + ']';
    }

    return this.path;
};

/**
 * Get data
 *
 * @param {boolean} [get_obj] get index to value object instead of just value
 *
 * @returns {app.promise}
 */
window.app.form.indexed_multiple.entry.prototype.getData = function(get_obj) {
    return new app.promise(
        function(fulfill, reject) {
            this.fieldset.getData().then(
                /* success */ function(data) {
                    fulfill(get_obj ? {index: this.parent.index, data: data} : data);
                },
                reject
            );
        },
        this
    );
};

/**
 * Set data
 *
 * @param {object} data
 */
window.app.form.indexed_multiple.entry.prototype.setData = function(data) {
    this.fieldset.setData(data);
};
