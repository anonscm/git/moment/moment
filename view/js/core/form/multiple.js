/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

/* global app, moment */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * Form multiple constructor
 *
 * @class
 *
 * @param {object} node DOM node or jQuery object
 * @param {object} parent
 */
window.app.form.multiple = function(node, parent) {
    this.type = 'multiple';
    this.subtype = node.is('[data-multiple-emails]') ? 'emails' : null;
    this.parent = parent;
    this.form = parent.form;
    this.node = node;
    this.nest_level = parent ? parent.nest_level : 0;
    this.node.attr({'data-nest-level': this.nest_level});

    this.tpl = this.node.clone();
    this.tpl.find('> legend').remove();
    this.tpl.removeAttr('data-multiple data-multiple-emails data-min-count data-max-count data-start-count');

    this.start = JSON.parse(this.node.attr('data-start-values'));
    if(!this.start) {
        this.start = [];
        for(var i=0; i<parseInt(this.node.attr('data-start-count')); i++)
            this.start.push({});
    }

    this.name = this.node.attr('data-multiple');

    this.min = parseInt(this.node.attr('data-min-count'));
    this.max = parseInt(this.node.attr('data-max-count'));

    this.node.data('multiple', this);

    this.entries = [];

    this.setup();
};

/**
 * Init contents
 */
window.app.form.multiple.prototype.setup = function() {
    var legend = this.node.find('> legend');
    this.node.empty();
    if(legend.length) legend.appendTo(this.node);

    this.add_ctrl = $('<button class="button" data-action="add" />').text(lang.tr('add')+'');
    this.add_ctrl.prepend('<span class="fa fa-plus-circle" /> ');

    var m = this;
    this.add_ctrl.appendTo(this.node).on('click', function() {
        m.addEntry({}, null, true);
        m.node.trigger(jQuery.Event('field.change', {field:m}));
    });

    for(var i=0; i<this.start.length; i++)
        this.addEntry(this.start[i], null);
};

/**
 * Update multiple block buttons
 */
window.app.form.multiple.prototype.updateButtons = function() {
    var over_max = this.max && (this.entries.length >= this.max);
    this.node.find('[data-action="add"]:not([data-nested="' + (this.nest_level + 1) + '"] [data-action])').prop('disabled', over_max);

    var under_min = this.min && (this.entries.length <= this.min);
    this.node.find('[data-action="del"]:not([data-nested="' + (this.nest_level + 1) + '"] [data-action])').prop('disabled', under_min);
};

/**
 * Create multiple entry
 *
 * @param {object} insertAfter
 *
 * @returns {app.form.multiple.entry}
 */
window.app.form.multiple.prototype.createEntry = function(insertAfter) {
    var node = this.tpl.clone();
    var ctn = $('<div class="small-10 columns" />').append(node.children());
    node.empty().append(ctn);

    if (insertAfter)
        node.insertAfter(insertAfter);
    else
        node.insertBefore(this.add_ctrl);

    return new app.form.multiple.entry(node, this);
};

/**
 * Add entry to multiple
 *
 * @param {object} values
 * @param {object} insertAfter
 */
window.app.form.multiple.prototype.addEntry = function(values, insertAfter, focus) {
    var entry = null;

    if(!this.max || (this.entries.length < this.max)) {
        entry = this.createEntry(insertAfter);

        if(values)
            entry.setData(values);

        var fc = 0;
        for(var k in entry.fieldset.fields)
            if(entry.fieldset.fields.hasOwnProperty(k))
                fc++;

        if(fc === 1 && !entry.node.find('[data-label-text]').length && !entry.node.find('[data-label-constraints]').length)
            entry.node.attr({'data-single-field-no-label': 1});

        entry.del_ctrl = $('<button class="button" data-action="del" />').text(lang.tr('delete') + '');
        entry.del_ctrl.prepend('<span class="fa fa-trash" /> ');

        var ctn = $('<div class="small-2 columns text-center" />').appendTo(entry.node);
        ctn.css({height: entry.node.find('> div').height() + 'px'});

        entry.del_ctrl.appendTo(ctn).on('click', function () {
            entry.multiple.deleteEntry(entry);
            entry.multiple.node.trigger(jQuery.Event('field.change', {field:entry}));
        });

        entry.del_ctrl.css({'margin-top': 'calc(' + (entry.node.find('> div').height() / 2) + 'px - 1rem)'});

        this.entries.push(entry);

        if (focus) entry.node.find(':input:first').focus();

        // Refresh buttons
        this.updateButtons();
    }

    return entry;
};

/**
 * Delete multiple block entry
 *
 * @param {object} entry
 */
window.app.form.multiple.prototype.deleteEntry = function(entry) {
    if(!this.min || (this.entries.length > this.min)) {
        var idx = -1;
        for(var i=0; i<this.entries.length && idx < 0 ; i++){
            if (entry.uid === this.entries[i].uid)
                idx = i;
        }

        if (idx >= 0) {
            entry.node.remove();
            this.entries.splice(idx, 1);

            // Refresh buttons
            this.updateButtons();
        }
    }
};

/**
 * Get path
 *
 * @returns {string}
 */
window.app.form.multiple.prototype.getPath = function() {
    if(this.path === undefined) {
        this.path = this.parent ? this.parent.getPath() : '';
        this.path += (this.path ? '.' : '') + this.name;
    }

    return this.path;
};

/**
 * Get data
 *
 * @param {boolean} [get_obj] get name to value object instead of just value
 *
 * @returns {app.promise}
 */
window.app.form.multiple.prototype.getData = function(get_obj) {
    var getters = [];
    for(var i=0; i<this.entries.length; i++)
        getters.push(this.entries[i].getData(true)); // Get as index-data pairs instead of just data

    // Restore order
    return app.promise.all(getters, this, [], function(rawdata) {
        if (rawdata == undefined)
            return get_obj ? {name: this.name, data: []} : [];

        rawdata.sort(function(a, b) {
            return a.index - b.index;
        });

        var data = [];
        for(var i=0; i<rawdata.length; i++)
            data.push(rawdata[i].data);

        return get_obj ? {name: this.name, data: data} : data;
    });
};

/**
 * Multiple entry
 *
 * @class
 *
 * @param {object} node DOM node or jQuery object
 * @param {app.form.multiple} multiple
 */
window.app.form.multiple.entry = function(node, multiple) {
    this.type = 'multiple-entry';
    this.multiple = multiple;
    this.form = multiple.form;
    this.nest_level = multiple.nest_level + 1;
    this.node = node;
    this.uid = this.form.getUID();
    this.node.attr({'data-entry': this.uid});

    this.node.data('entry', this);

    this.fieldset = new app.form.fieldset(node, this);
};

/**
 * Get path
 *
 * @returns {string}
 */
window.app.form.multiple.entry.prototype.getPath = function() {
    if(this.path === undefined) {
        this.path = this.multiple.getPath();
        this.path += '[' + this.node.index('[data-entry]') + ']';
    }

    return this.path;
};

/**
 * Get data
 *
 * @param {boolean} [get_obj] get index to value object instead of just value
 *
 * @returns {app.promise}
 */
window.app.form.multiple.entry.prototype.getData = function(get_obj) {
    return new app.promise(
        function(fulfill, reject) {
            this.fieldset.getData().then(
                /* success */ function(data) {
                    fulfill(get_obj ? {index: this.node.index('[data-entry]'), data: data} : data);
                },
                reject
            );
        },
        this
    );
};

/**
 * Set data
 *
 * @param {object} data
 */
window.app.form.multiple.entry.prototype.setData = function(data) {
    this.fieldset.setData(data);
};
