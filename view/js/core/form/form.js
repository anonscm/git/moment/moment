/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */

/* global app, moment */

if (!window.hasOwnProperty('app'))
    window.app = {};

/**
 * Validation callback
 *
 * @callback formValidator
 *
 * @this {object} the entry or map
 *
 * @param {function} validate to be called if data is valid
 * @param {function} reject to be called if data is invalid with error string as argument
 * @param {*} data
 */

/**
 * Form controller constructor
 *
 * @class
 *
 * @param {object} node DOM node or jQuery object
 */
window.app.form = function(node) {
    this.node = $(node);
    this.id = this.node.attr('data-form');
    if(!this.id)
        throw new Error('Form has no id');

    this.validators = [];
    this.eventHandlers = {};
    this.uid = 1;
    this.nest_level = 0;
    this.status = 'new';

    var nesting = 0;
    this.node.find('[data-multiple],[data-indexed-multiple]').each(function() {
        var m = $(this);
        var n = m.parents('[data-multiple],[data-indexed-multiple]').length;
        m.attr({'data-nested': n});
        nesting = Math.max(nesting, n + 1);
    });

    this.node.attr({'data-nesting': nesting});
    this.nesting = nesting;

    this.index = {}; // Fields by path
    this.fieldset = new app.form.fieldset(this.node, this);

    this.warnEdited = this.node.is('[data-warn-edited]');

    var form = this;
    this.node.on('field.change', function(event){
        form.status = 'edited';
    });

    this.setupControls();

    this.node.data('form', this);

    app.event.trigger('form.' + this.node.attr('data-form') + '.ready', this);
};

/**
 * Get unique id
 *
 * @returns string
 */
window.app.form.prototype.getUID = function() {
    return 'uid_' + this.uid++;
};

/**
 * Register field
 *
 * @param {object} field
 */
window.app.form.prototype.registerField = function(field) {
    this.index[field.getPath()] = field;
};

/**
 * Get entry from path
 *
 * @param {string} path
 *
 * @returns {object|null}
 */
window.app.form.prototype.getEntryFromPath = function(path) {
    return this.index[path] ? this.index[path] : null;
};

/**
 * Get entry from DOM node
 *
 * @param {object|string} node
 *
 * @returns {object|null}
 */
window.app.form.prototype.getEntryFromNode = function(node) {
    var path = $(node).closest('[data-field-path]').attr('data-field-path');

    return this.getEntryFromPath(path);
};

/**
 * Add custom data checker
 *
 * @param {formValidator} validator
 * @param {string} target input matcher (use of * allowed)
 */
window.app.form.prototype.addValidator = function(validator, target) {
    if(target) {
        target = target.replace(/\./g, '\.');
        target = target.replace(/\*/g, '.+');
        target = target.replace(/\[\]/g, '\.[0-9]+');
    }

    this.validators.push({
        validator: validator,
        target: target ? new RegExp('^' + target + '$') : null
    });
};

/**
 * Get validator promises for field
 *
 * @param {object} field
 * @param {*} data
 *
 * @return {function[]}
 */
window.app.form.prototype.getFieldValidators = function(field, data) {
    var promises = [];
    for(var i=0; i<this.validators.length; i++) {
        if(!this.validators[i].target) continue;
        if(!this.validators[i].target.test(field.getPath())) continue;

        promises.push(
            new app.promise(
                function(fulfill, reject, validator, data) {
                    (new app.promise(validator, this, [data])).then(
                        fulfill,
                        function(error) {
                            reject({subject: this, reason: error});
                        }
                    );
                },
                /* context */ field,
                [this.validators[i].validator, data]
            )
        );
    }

    return promises;
};

/**
 * Get validator promises for form
 *
 * @param {*} data
 *
 * @return {function[]}
 */
window.app.form.prototype.getValidators = function(data) {
    var promises = [];
    for(var i=0; i<this.validators.length; i++) {
        if(this.validators[i].target) continue;

        promises.push(
            new app.promise(
                function(fulfill, reject, validator, data) {
                    (new app.promise(validator, this, [data])).then(
                        fulfill,
                        function(error) {
                            reject({subject: this, reason: error});
                        }
                    );
                },
                /* context */ this,
                [this.validators[i].validator, data]
            )
        );
    }

    return promises;
};

/**
 * Display entry error
 *
 * @param {string} error (translatable)
 * @param {object} entry
 */
window.app.form.prototype.displayError = function(error, entry) {
    var tooltip;

    if(error) {
        entry.node.attr({'data-valid': 'no'});

        tooltip = entry.node.data('error_tooltip');
        if(!tooltip) {
            if (entry.type === 'multiple' || entry.type === 'indexed-multiple') {
                tooltip = $('<div class="error_hint" />').insertAfter(entry.node.find('[data-entry]:last'));

            } else {
                tooltip = $('<div class="error_hint" />').insertAfter(entry.node);
            }


            entry.node.data('error_tooltip', tooltip);
        }

        tooltip.text(lang.tr(error)+'');

    } else {
        entry.node.attr({'data-valid': 'yes'});

        tooltip = entry.node.data('error_tooltip');
        if(tooltip) {
            tooltip.remove();
            entry.node.removeData('error_tooltip');
        }
    }
};

/**
 * Gather data, validate in the process
 *
 * @return {app.promise}
 */
window.app.form.prototype.getData = function() {
    return new app.promise(
        function(fulfill, reject) {
            var form = this;
            this.fieldset.getData().then(
                /* success */ function(data) {
                    app.promise.all(form.getValidators(data)).then(
                        /* valid */ function() {
                            fulfill(data);
                        },
                        reject
                    );
                },
                reject
            );
        },
        /* the form is the promise context */
        this
    ).catch(function(error) {
        app.ui.notify('error', lang.tr('invalid_fields').out());
        app.ui.focus(error.subject.node);
    });
};

/**
 * Locks controls
 *
 * @param {boolean} lock
 */
window.app.form.prototype.lock = function(lock) {
    var ctn = this.node.find('[data-controls]');
    if (lock) {
        this.node.find(':input').prop('disabled', true);
        ctn.find('[data-control]').addClass('disabled');
        ctn.append('<span data-loader class="fa fa-circle-o-notch fa-spin fa-2x fa-fw" />');
    }else{
        this.node.find(':input').prop('disabled', false);
        ctn.find('[data-control]').removeClass('disabled');
        ctn.find('[data-loader]').remove();
    }
};

/**
 * Add event handler
 *
 * @param {string} event
 * @param {function} callback
 */
window.app.form.prototype.on = function(event, callback) {
    this.eventHandlers[event] = callback;
};

/**
 * Setup controls
 */
window.app.form.prototype.setupControls = function() {
    var form = this;
    this.node.find('[data-control]').on('click', function() {
        var ctrl = $(this);
        if(ctrl.hasClass('disabled')) return;

        var action = ctrl.attr('data-control');
        var resource = ctrl.attr('data-control-resource');

        var goto = ctrl.attr('data-control-goto');

        if(action === 'cancel' && goto) {
            app.ui.redirect(goto.replace(/^\//g, ''));
            return;
        }

        var success = ctrl.attr('data-control-success');
        var failure = ctrl.attr('data-control-failure');
        if(!success) success = action + '_success';
        if(!failure) failure = action + '_failure';

        form.getData().then(
            /* fulfilled */ function(data) {
                // Disable controls and inputs
                this.lock(true);

                var promise = new app.promise(
                    function(fulfill, reject, data) {
                        if(resource) {
                            var entity = resource.split('?')[0].split('/');
                            if(action === 'create') {
                                entity = entity[entity.length - 1];

                            } else {
                                entity = entity[entity.length - 2];
                            }

                            var copts = {error: reject};

                            if(action === 'create') {
                                app.client.post(resource, data, function(location, data) {
                                    fulfill({location: location, data: data});
                                }, copts);

                            } else if(action === 'update') {
                                app.client.put(resource, data, function(data) {
                                    fulfill({data: data});
                                }, copts);

                            } else if(action === 'delete') {
                                var confirm = ctrl.attr('data-control-confirm');

                                if(confirm) {
                                    if(confirm === '1')
                                        confirm = 'delete_entity';

                                    app.ui.popup.confirm(lang.tr(confirm).r({entity: entity}), function() {
                                        app.client.delete(resource, function(data) {
                                            fulfill({data: data});
                                        }, copts);
                                    }, function(){
                                        this.lock(false);
                                    });

                                } else {
                                    app.client.delete(resource, function(data) {
                                        fulfill({data: data});
                                    }, copts);
                                }
                            }

                        } else {
                            if(this.eventHandlers[action]) {
                                (new app.promise(this.eventHandlers[action], this, [data])).then(fulfill, reject);

                            } else {
                                throw new Error('No resource and no handler for ' + action + ' in ' + this.id);
                            }
                        }
                    },
                    this,
                    [data]
                );

                promise.then(
                    function(result) {
                        app.ui.notify('success', lang.tr(success), function() {
                            form.status = 'done';
                            if(goto) {
                                goto = goto.replace(/:[a-z][a-z0-9_-]+/g, function(m) {
                                    var k = m.substr(1);
                                    return result.data[k] ? encodeURIComponent(result.data[k]) : '?' + k;
                                });

                                if (goto.match(/^https?:\/\//))
                                    window.location.href = goto;
                                else
                                    window.location.href = app.config.application_url + goto;

                            } else {
                                app.ui.reload();
                            }
                        });
                    },
                    function(error) {
                        var p = this.eventHandlers.failure ? new app.promise(this.eventHandlers.failure, this, error) : app.promise.resolve(error, this);

                        p.then(
                            function(error) {
                                this.lock(false);

                                if (error)
                                    app.ui.notify('error', lang.tr(typeof error == 'string' ? error : failure));
                            }
                        );
                    }
                );
            }
        );
    });
};
