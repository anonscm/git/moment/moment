/**
 * This file is part of the ApplicationBase project.
 * 2014 - 2015
 */


window.app.clientStorage = {

    localStorageAvailable: null,

    /**
     * @return boolean|null true if localstorage is available, false otherwise
     */
    localStorage: function(){
        if (this.localStorageAvailable === null)
            this.localStorageAvailable = ('localStorage' in window) && (window['localStorage'] !== null);

        return this.localStorageAvailable;
    },


    /**
     * Store an key with his associated key
     *
     * @param key key name
     * @param data data to store
     *
     * @return
     */
    set: function(key, data) {
        if (this.localStorage) {
            // local storage available
            window.localStorage.setItem(key, ''+data);

        } else {
            // local storage not available, use cookies instead
            var exdays = app.config.max_cookie_duration;

            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = key + "=" + data + "; " + expires;
        }
    },


    /**
     * Get value from storage
     *
     * @param key key name to get
     * @returns {*}
     */
    get: function(key){
        if (this.localStorage) {
            // local storage available
            return window.localStorage.getItem(key);

        }else{
            // local storage not available, use cookies instead
            var name = key + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') c = c.substring(1);
                if (c.indexOf(name) === 0) return c.substring(name.length,c.length);
            }
            return null;
        }
    },


    /**
     * Remove an item from storage
     * @param string key to remove
     */
    remove: function(key){
        if (this.localStorage) {
            // local storage available
            window.localStorage.removeItem(key);
        }else{
            // local storage not available, use cookies instead

        }
    }

}