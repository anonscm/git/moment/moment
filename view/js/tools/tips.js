/*
 * This file is part of the Moment project.
 * 2019
 * Copyright (c) RENATER
 */

window.app.tips = {

    /**
     * Key used for storing data into local storage
     */
    local_storage_key: 'user_hide_tips',


    /**
     * True if user is authenticated, false otherwise
     */
    userAuthenticated: undefined,

    /**
     * All tips elements
     */
    tips: undefined,

    /**
     * Local tip ids
     */
    localIDS : [],


    /**
     * Get list of currently displayed tips
     *
     * @returns {Promise}
     */
    getTips: function(){
        this.tips = $('[data-tip-id]');
    },

    /**
     * Get from local storage  list of tips ids user has click on "do not show again"
     * If there is
     */
    initDoNotShowAgainState: function(){
        // Init local ids list
        var ids = app.clientStorage.get(app.tips.local_storage_key);
        if (ids) this.localIDS = JSON.parse(ids);
    },

    /**
     * Check if user already click on "do not show again" for a tip id
     *
     * @param id
     *
     * @returns {boolean}
     */
    userDoNotShowAgain: function(id){
        var checkFn = function(ids){
            for (var i in ids) {
                if (ids[i] === id) return true;
            }
            return false;
        };

        if (checkFn(app.tips.localIDS)) return true;

        return false;
    },

    /**
     * Push into local storage and webservice list of tips ids user has click on "do not show again"
     */
    pushDoNotShowAgainState: function(){
        app.clientStorage.set(app.tips.local_storage_key, JSON.stringify(app.tips.localIDS));
    },

    /**
     * Show Tips on UI
     */
    show: function(){
        if (this.tips.length) {
            this.tips.each(function() {
                if (app.tips.userDoNotShowAgain($(this).attr('data-tip-id'))) {
                    return;
                }

                $(this).find('[data-action="hide_tip"]').on('click', function() {
                    var currentTip = $(this).closest('[data-tip-id]');
                    app.tips.localIDS.push(currentTip.attr('data-tip-id'))
                    app.tips.pushDoNotShowAgainState();
                    currentTip.remove();
                });

                $(this).show();
            });
        }
    },


    /**
     * Startup function.
     * Init MOTD list, show on UI if needed
     */
    startup: function(){
        this.initDoNotShowAgainState();

        this.getTips();

        this.show();
    }
};
