/**
 * Adds arrows to scroll left or right
 * @param opts
 */
jQuery.fn.inputswitch = function (opts) {

    var $inputSwitches = $(".inputSwitch"),
        $inputs = $inputSwitches.find("input"),
        $spans = $inputSwitches.find("span");

    $spans.on("click", function() {
        var $this = $(this);
        $this.hide().siblings("input").show().focus().select();
    }).each( function() {
        var $this = $(this);
        $this.text($this.siblings("input").val());
    });

    let switchBack = function($this) {
        $this.hide().siblings("span").text($this.val()).show();
        $inputSwitches.trigger('inputswitch_change');
    }
    $inputs.on("blur", function() {
        switchBack($(this));
    }).on('keydown', function(e) {
        if (e.which === 9) {
            e.preventDefault();
            if (e.shiftKey) {
                $(this).blur().parent().prevAll($inputSwitches).first().find($spans).click();
            } else {
                $(this).blur().parent().nextAll($inputSwitches).first().find($spans).click();
            }
        }
        if (e.keyCode === 13) {
            switchBack($(this));
        }
    }).hide();

    $inputSwitches.removeClass('inputSwitch');
}