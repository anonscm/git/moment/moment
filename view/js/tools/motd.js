/*
 * This file is part of the Moment project.
 * 2017
 * Copyright (c) RENATER
 */

/**
 * This file is part of the RENAvisio project.
 * 2014 - 2015
 */

if (!('app' in window))
    window.app = {};

window.app.motd = {

    /**
     * Key used for storing data into local storage
     */
    local_storage_key: 'user_hide_motd',


    /**
     * True if user is authenticated, false otherwise
     */
    userAuthenticated: undefined,

    /**
     * List of MOTDs
     */
    motds: undefined,


    /**
     * List of motd ids stored in local storage
     */
    localIDS: [],

    /**
     * List of motd ids stored remotely (using webservice)
     */
    remoteIDS: [],


    /**
     * Add a local id
     *
     * @param data
     */
    addLocalID: function(data){
        if (app.motd.localIDS.indexOf(data) < 0)
            app.motd.localIDS.push(data);
    },


    /**
     * Push into local storage and webservice list of motd ids user has click on "i got it"
     */
    pushGotItState: function(){
        /* List of IDS to push */
        var ids = [];

        /* List of IDS already seen by user */
        var oldIds = [];

        for (var i in app.motd.localIDS){
            var entry = app.motd.localIDS[i];

            // MOTD already checked by user
            if (oldIds.indexOf(entry) >= 0)
                continue;

            var data = entry.split(':');

            for (var j in app.motd.localIDS){
                var subEntry = app.motd.localIDS[j];
                var subData = subEntry.split(':');
                // Check if current motd id already in oldIds
                if (subData[0] === data[0]){
                    // Check last update time
                    if (subData[1] >= data[1]){
                        // Same motd with probably different time
                        entry = subEntry;
                        oldIds.push(entry);
                    }else{
                        oldIds.push(subEntry);
                    }
                }
            }
            ids.push(entry);
        }

        app.motd.localIDS = ids;


        // Now pushing ids into user session and local storage
        if (this.userAuthenticated)
            app.client.put('/User/@me', {last_motd_time: JSON.stringify(app.motd.localIDS)});

        app.clientStorage.set(app.motd.local_storage_key, JSON.stringify(app.motd.localIDS));

    },

    /**
     * Get from local storage and webservice list of motd ids user has click on "i got it"
     * If there is
     */
    initGotItState: function(){
        // Init remote ids list
        if (this.userAuthenticated)
            this.remoteIDS = JSON.parse(app.session.user.last_motd_time);

        // Init local ids list
        var ids = app.clientStorage.get(app.motd.local_storage_key);
        if (ids) this.localIDS = JSON.parse(ids);


    },

    /**
     * Get list of available MOTDs
     * Call to MOTD webservice (defined in config)
     *
     * @returns {Promise}
     */
    getMotds: function(){
        return new Promise(function (resolve, reject) {
            var config = app.config.motd;
            var url = config.url + config.endpoint + '/MOTD';

            jQuery.getJSON(url, undefined , function(data){
                resolve(data);

            }).fail(function(e){
                reject(e);
            });
        });
    },


    /**
     * Check if user already click on "i got it" for a MOTD ID
     *
     * @param id
     * @param timestamp
     *
     * @returns {boolean}
     */
    userGotIt: function(id, timestamp){

        var checkFn = function(ids){
            for (var i in ids) {
                var entry = ids[i];
                var tmp = entry.split(':');
                if (tmp[0] === id.toString() && tmp[1] === timestamp.toString()) return true;
            }
            return false;
        };

        if (checkFn(app.motd.localIDS)) return true;
        if (checkFn(app.motd.remoteIDS)) return true;

        return false;
    },


    /**
     * Show MOTD on UI
     */
    show: function(){
        if (this.motds.length && $('#motd-modal').length > 0) {

            var currentTime = new Date().getTime() / 1000;
            var localLang = $('html').attr('lang');

            var shownMotds = [];

            for (let i in this.motds) {
                const entry = this.motds[i];

                // Ensure motd start and end time respected
                if (entry.start_diffusion_time.raw < currentTime && (entry.end_diffusion_time === null || entry.end_diffusion_time.raw > currentTime)) {
                    // Authenticated (or not) user click on "do not show again" on this MOTD
                    // init the last update time to 0 if not set
                    if (!entry.last_update_time)
                        entry.last_update_time = {raw: 0};
                    // if no end date
                    if(!entry.end_diffusion_time)
                        entry.end_diffusion_time = {raw: -1};

                    if (this.userGotIt(entry.id, entry.last_update_time.raw)){
                        app.motd.addLocalID(entry.id+':'+entry.last_update_time.raw);
                        app.motd.pushGotItState();
                        continue;
                    }
                    shownMotds.push(entry);
                }
            }


            var mainArticle = $('<article />');

            for (let i in shownMotds){
                const entry = shownMotds[i];

                let article = $('<article />').attr({
                    'data-motd-id': entry.id,
                    'data-motd-last_update_time': entry.last_update_time.raw,
                    'data-start-diffusion-time': entry.start_diffusion_time.raw,
                    'data-end-diffusion-time': entry.end_diffusion_time.raw,
                    'data-time-zone': entry.time_zone
                }).css('text-align', 'left');

                let defaultTranslation;
                article.append('<h1>&ldquo; '+entry.title+' &rdquo;</h1>');
                let motd;
                for (let j in entry.content){
                    // Browse different translations, search for user preferred language.
                    // If not found, show FR translation
                    motd = entry.content[j];
                    if (motd.lang === localLang){
                        // Show this motd
                        article.append(motd.content);
                        defaultTranslation = false;
                        break;
                    }else if (motd.lang === 'fr'){
                        defaultTranslation = motd;
                    }
                }

                if (defaultTranslation){
                    // Unable to find a MOTD in the user translation, showing FR motd
                    article.append('<br />');
                    article.append(motd.content);
                }


                if (article.children().length)
                    mainArticle.prepend(article);
            }

            if (mainArticle.html().length){
                // At least one motd to show
                $('#motd-modal').prepend(mainArticle);
                var popup = new Foundation.Reveal($('#motd-modal'));
                popup.open();
                if ($('#motd_do_not_show_again')) {
                    $('#motd_do_not_show_again').on('click', function () {
                        $('div#motd-modal article[data-motd-id]').each(function(){
                            app.motd.addLocalID($(this).attr('data-motd-id')+':'+$(this).attr('data-motd-last_update_time'));
                        });

                        if (app.motd.localIDS.length){
                            app.motd.pushGotItState();
                        }
                    });
                }
            }
        }
    },


    /**
     * Startup function.
     * Init MOTD list, show on UI if needed
     */
    startup: function(){

        this.userAuthenticated = !! app.session.user ;

        this.initGotItState();

        if (typeof this.motds == 'undefined'){
            // Init list
            this.motds = [];

            var config = app.config.motd;
            if (config) {
                var prom = this.getMotds();

                prom.then(function(result){
                    // Got MOTD
                    if (result.length){
                        app.motd.motds = result;
                        app.motd.show();
                    }
                }).catch(function(e){
                    console.log(e);
                });
            }
        }
    }
};
