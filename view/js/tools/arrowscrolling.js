/**
 * Adds arrows to scroll left or right
 * @param opts
 */
jQuery.fn.arrowscrolling = function (opts) {

    var element = $(this);
    var parent = $(this).parent();

    //Element must at least be twice as high as arrow
    if(element.height() < 140)
        return;

    var maxScroll = $(this)[0].scrollWidth - $(this)[0].clientWidth;
    var leftArrow = $('<span class="left-arrow" title="' + app.lang.tr('left_arrow_notice') +'" data-tooltip><i class="fa fa-chevron-left" aria-hidden="true"></i></span>');
    var rightArrow = $('<span class="right-arrow" title="' + app.lang.tr('right_arrow_notice') +'" data-tooltip><i class="fa fa-chevron-right" aria-hidden="true"></i></span>');

    $(leftArrow).appendTo(parent);
    $(rightArrow).appendTo(parent);

    element.trigger('scroll');

    var arrows = parent.find('.right-arrow, .left-arrow');

    //Applying directly css, would be better in separated file
    arrows.css({
        'position': 'absolute',
        'top': '50%',
        'width': '26px',
        'height': '70px',
        'background-color': '#fafafa',
        'vertical-align': 'middle',
        'padding-top': '20px',
        'cursor': 'pointer',
        'border': 'solid 1px lightgrey',
    });

    leftArrow.css({
        'left': '0',
        'padding-left': '6px',
        '-webkit-border-radius': '0px 3px 3px 0px',
        '-moz-border-radius': '0px 3px 3px 0px',
        'border-radius': '0px 3px 3px 0px'
    });

    rightArrow.css({
        'right': '0',
        'padding-left': '8px',
        '-webkit-border-radius': '3px 0px 0px 3px',
        '-moz-border-radius': '3px 0px 0px 3px',
        'border-radius': '3px 0px 0px 3px'
    });

    $(this).on('scroll', function(e) {
        maxScroll = $(this)[0].scrollWidth - $(this)[0].clientWidth;

        leftArrow.css('display', ($(this).scrollLeft() === 0)?'none':'table-cell');
        rightArrow.css('display', ($(this).scrollLeft() === maxScroll)?'none':'table-cell');
    });
    //Initial state
    $(this).trigger('scroll');
    var scrollingInterval = null;
    rightArrow.on('mousedown', function(e) {
        scrollingInterval = setInterval(function() {
            element.scrollLeft(element.scrollLeft()+30);
        }, 50);
    });

    rightArrow.on('dblclick', function() {
        element.animate({scrollLeft:maxScroll}, 250, 'swing');
    })

    leftArrow.on('mousedown', function() {
        scrollingInterval = setInterval(function() {
            element.scrollLeft(element.scrollLeft()-30);
        }, 50);
    });

    leftArrow.on('dblclick', function() {
        element.animate({scrollLeft:0}, 250, 'swing');
    })

    arrows.on('mouseup mouseleave', function() {
        clearInterval(scrollingInterval);
    });

}