
window.app.multipickr = function(nodeList, mode) {
    this.pickr = undefined;

    var nodes = Array.prototype.slice
        .call(nodeList)
        .filter(function (x) { return x instanceof HTMLElement; });
    for (var i = 0; i < nodes.length; i++) {
        var node = nodes[i];
        try {
            this._init(node, mode);

            node._multipickr = this;
        } catch (e) {
            console.error(e);
        }
    }
    return nodes.length === 1 ? $(nodes[0]) : nodes;
}

window.app.multipickr.separator = ',';

window.app.multipickr.prototype._init = function(node, mode) {
    let elem = $(node);
    const that = this;

    let refresh = function(selectedDates, dateStr, flatpickrItem){
        app.userManager.refreshUnavailability(flatpickrItem);
    };

    if('range' === mode) {
        this.pickr = $(node).flatpickr({
            mode: 'range',
            inline: true,
            allowInput: true,
            onChange: function (currentDateTimes, value, flatpickrItem) {
                if (currentDateTimes.length !== null && currentDateTimes.length === 2) {

                    let baseDay = (new moment(currentDateTimes[0])).format(app.lang.translate('moment_date_format').text);
                    let endDay = (new moment(currentDateTimes[1])).format(app.lang.translate('moment_date_format').text);

                    $(this.input).parents('.date-picker-range-container').find('.date-picker-range-start').val(baseDay);
                    $(this.input).parents('.date-picker-range-container').find('.date-picker-range-end').val(endDay);

                    $(this.input).trigger('multipickr_range_added', {baseDay: baseDay, endDay: endDay});

                    $(this.input).parents('.date-picker-range-container').find('.date-picker-range-start, .date-picker-range-end').val('');

                    flatpickrItem.clear();
                }
                refresh(currentDateTimes, value, flatpickrItem);
            },
            onMonthChange: refresh,
            onYearChange: refresh,
            onReady: refresh,
            onDayCreate: function(dObj, dStr, fp, dayElem){
                $(dayElem).attr('data-uid', moment(dayElem.dateObj).format('YYYYMMDD'));
            }
        });

        //Alternative input method
        $(this.pickr.input).parents('.date-picker-range-container').find('.date-picker-range-end, .date-picker-range-start').on('keyup', function(e) {
            if (e.keyCode === 13 && e.target === this) {
                let $startInput = $(this).parents('.date-picker-range-container').find('.date-picker-range-start');
                let $endInput = $(this).parents('.date-picker-range-container').find('.date-picker-range-end');

                let rangeStart = new moment($startInput.val(), app.lang.translate('moment_date_format').text);
                let rangeEnd = new moment($endInput.val(), app.lang.translate('moment_date_format').text);
                if(rangeStart.isValid() && rangeEnd.isValid() && (rangeEnd.isAfter(rangeStart) || rangeEnd.isSame(rangeStart))) {
                    $(that.pickr.input).trigger('multipickr_range_added', {
                            baseDay: rangeStart.format(app.lang.translate('moment_date_format').text),
                            endDay: rangeEnd.format(app.lang.translate('moment_date_format').text)
                    });
                    $(this).parents('.date-picker-range-container').find('.date-picker-range-start, .date-picker-range-end').val('');
                } else {
                    if(!rangeStart.isValid()) {
                        window.app.ui.notify('error', app.lang.tr('invalid_range_start_date'));
                        $startInput.addClass('is-invalid-input');
                    }
                    if(!rangeEnd.isValid()) {
                        window.app.ui.notify('error', app.lang.tr('invalid_range_end_date'));
                        $endInput.addClass('is-invalid-input');
                    }
                    if(rangeStart.isValid() && rangeEnd.isValid() && rangeEnd.isBefore(rangeStart)) {
                        window.app.ui.notify('error', app.lang.tr('end_date_prior_to_base_date'));
                        $endInput.addClass('is-invalid-input');
                    }
                }
            }
        });
    } else {

        $(node).data('values', $(node).val().split(window.app.multipickr.separator));
        this.pickr = $(node).flatpickr({
            mode: 'multiple',
            inline: true,
            allowInput: true,
            conjunction: window.app.multipickr.separator,
            onChange: function (currentDateTimes, value, flatpickrItem) {
                let newValues = value.split(window.app.multipickr.separator);
                // New selected dates
                window.app.utilities.arrayDiff(newValues, elem.data('values')).forEach(function(date) {
                    if(date !== "") {
                        elem.trigger('multipickr_date_added', date);
                    }
                });

                // Dates to be deleted
                window.app.utilities.arrayDiff(elem.data('values'), newValues).forEach(function(date) {
                    elem.trigger('multipickr_date_deleted', date);
                });

                elem.data('values', newValues);

                refresh(currentDateTimes, value, flatpickrItem);
            },
            onMonthChange: refresh,
            onYearChange: refresh,
            onReady: refresh,
            onDayCreate: function(dObj, dStr, fp, dayElem){
                $(dayElem).attr('data-uid', moment(dayElem.dateObj).format('YYYYMMDD'));
            }
        });

        //Moving input after calendar (there are no Flatpickr options for this when "inline")
        $(this.pickr.input).insertAfter(this.pickr.calendarContainer);
    }

    if(this.pickr !== undefined) {
        app.event.on('calendar_filter_changed', function() {
            app.userManager.refreshUnavailability(that.pickr);
        });
    }

    return node;
}

window.app.multipickr.prototype.clear = function() {
    if(this.pickr.clear !== undefined)
        this.pickr.clear();
}

window.app.multipickr.prototype.setDate = function(date) {
    if(this.pickr.setDate !== undefined) {
        this.pickr.setDate(date);
        $(this.pickr.input).data('values', date.split(window.app.multipickr.separator));
    }
}

window.app.multipickr.prototype.deleteDate = function(date) {

    if(this.pickr === undefined)
        return;

    // deleting "date" from value with regex
    let re = new RegExp(date, 'g');
    let newDates = $(this.pickr.input).val().replace(re, '');

    this.pickr.setDate(newDates);

    $(this.pickr.input).data('values', newDates.split(window.app.multipickr.separator));

    app.userManager.refreshUnavailability(this.pickr);
}

jQuery.prototype.multipickr = function(mode) {
    return new window.app.multipickr(this, mode);
}