/**
 * Adds a fixed header display
 * @param opts
 */
jQuery.fn.headervisible = function (opts) {

    if($(this).is(':hidden') || $(this).attr('data-header-visible') !== undefined)
        return;

    var element = $(this);
    var parent = $(this).parent();

    //Fixing height and width for clone to be equally sized
    element.find('thead th, thead td').each(function() {
        $(this).uniqueId();
    });
    var $header = $(this).find("thead").clone(true);
    var $section = $('<section></section>').appendTo(parent);
    var $fixedTable = $('<table id="header-fixed"></table>').appendTo($section);
    var $fixedHeader = $fixedTable.append($header);
    $fixedTable.find('thead th, thead td').each(function() {
        $(this).attr('data-ref-id', $(this).attr('id'));
        $(this).removeAttr('id');
    });

    //Indicator already been shown once
    $section.attr('data-shown',0);
    $section.css({
        'position': 'fixed',
        'top': '0px',
        'display': 'none',
        'background-color':'white',
        'overflow-x': 'hidden',
        'overflow-y': 'hidden'
    });

    //Fixed header should not oversize parent width
    $section.css('width', parent.width());

    $fixedTable.css('width', parent.find('table').width());

    $( window ).resize(function() {
        $fixedTable.css('width', parent.width());
    });

    //Synchronizing scrolling fixed header
    $(this).on('scroll', function(e) {
        parent.find('#header-fixed').parent().scrollLeft($(this).scrollLeft());
    });

    $(window).bind("scroll", function() {
        var offset = $(this).scrollTop();
        var tableOffset = element.offset().top;

        //We must have at least 3 rows
        if(element.find("tr").length < 3)
            return;

        var tableOffsetEnd = element.find("tr").eq(-3).offset().top;

        if (offset >= tableOffset && offset < tableOffsetEnd /*&& $fixedHeader.is(":hidden")*/) {

            //We compute width and height only the first time $section is displayed
            if($section.attr('data-shown') === '0') {
                $fixedTable.find('thead th, thead td').each(function() {
                    var matchingElement = element.find('thead th#'+ $(this).attr('data-ref-id') +', thead td#'+ $(this).attr('data-ref-id'));

                    $(this).attr('colspan', matchingElement.attr('colspan'));
                    $(this).css('height', matchingElement[0].getBoundingClientRect().height);
                    $(this).css('width', matchingElement[0].getBoundingClientRect().width);
                    $(this).css('max-width', 'initial');
                });
                $section.attr('data-shown', 1);
            }

            //Updating width
            $section.css('width', parent.width());
            $fixedTable.css('width', element.find('table').width());

            $section.css('display','block');
            $section.scrollLeft(element.scrollLeft());
        } else {
            $section.hide();
        }
    });

    $(element).attr('data-header-visible','');
}