<?php

/**
 *     Moment - weekly.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Ensure user has root rights
if ((int) exec('id -u') != 0)
    die (basename(__FILE__) .' script must be launch with root privileges.'."\n");

require_once(dirname(__FILE__).'/../../includes/core/init.php');

ApplicationContext::setProcess(ProcessTypes::CRON);

// Parse arguments / env
$options = getopt('h', array('help', 'noidp'));

if(array_key_exists('h', $options) || array_key_exists('help', $options)) {
    echo "\n".'Monthly tasks execution (Stats)'."\n";
    echo "\t\n";
    echo 'Usage :'."\n";
    echo "\t".' php monthly.php [-h|--help] [--noidp]'."\n";
    echo "\t\n";
    echo "\t".'Optional parameters: '."\n";
    echo "\t".' --noidp : disable by idp indicators (for stat report)'."\n\n";
}

// By default stat report contains idp values
$with_idp = true;
if(array_key_exists('noidp', $options)) {
    $with_idp = false;
}

$startTime = microtime(true);
Logger::info('Monthly cron started');

$report = SurveyStats::generateReport(SurveyStats::MONTH, null, $with_idp);

$week_start = date(Lang::tr('date_format'), strtotime('-1 month'));
$week_end = date(Lang::tr('date_format'));

// Using ';' as separator
RestRequest::setFormat('csv:;');

MomentApplicationMail::prepareStatsEmail($report, Config::get('stats.recipients'),
    SurveyStats::MONTH,
    $week_start,
    $week_end
)->send();

(new Event('stats_report_generated', $report, SurveyStats::MONTH))->trigger();

Logger::info('Monthly cron finished (Took :'.(microtime(true) - $startTime).' seconds)');

