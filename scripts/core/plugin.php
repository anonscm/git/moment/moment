<?php

/**
 *     Moment - plugin.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Ensure user has root rights
$output = array();
exec('env', $output);

if (!(bool) count(preg_grep('`^USER\=root`', $output)))
    die (basename(__FILE__) .' script must be launch with root privileges.'."\n");

require_once(dirname(__FILE__).'/../../includes/core/init.php');

$self   = array_shift($argv);
$action = array_shift($argv);
$target = array_shift($argv);

if(!in_array($action, array('install', 'update', 'uninstall'))) {
    die('Unknown action "'.$action.'", see "help"'."\n");
}

if($action == 'help') {
    echo 'Usage : '.$self.' <action> <target>'."\n";
    echo "\n";
    echo "\t".'action must be "install", "update" or "uninstall"'."\n";
    echo "\t".'target is the name of th plugin to act upon'."\n";
    exit(0);
}

try {
    switch($action) {
        case 'install':   PluginManager::installPlugin($target);   break;
        case 'update':    PluginManager::updatePlugin($target);    break;
        case 'uninstall': PluginManager::uninstallPlugin($target); break;
    }
    
} catch(Exception $e) {
    die('Exception : '.$e->getMessage()."\n");
}

exit(0);
