<?php

/**
 *     Moment - database.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Ensure user has root rights
if ((int) exec('id -u') != 0)
    die (basename(__FILE__) .' script must be launch with root privileges.'."\n");

include dirname(__FILE__).'/../../includes/core/init.php';

ApplicationContext::setProcess(ProcessTypes::UPGRADE);
Logger::ensureErrorlog();

/**
 * Create/upgrade application's database
 */

set_error_handler(function($no, $str, $file = '', $line = '') {
    if($no == '2048') return;
    Logger::error('['.$no.'] '.$str.' in '.$file.' at line '.$line);
});

try {
    DBUpdater::updateModel();
    
    echo 'Everything went well'."\n";
    echo 'Database structure is up to date'."\n";
} catch(Exception $e) {
    $uid = ($e instanceof LoggingException) ? $e->getUid() : 'no available uid';
    die('Encountered exception : '.$e->getMessage().', see logs for details (uid: '.$uid.') ...');
}
