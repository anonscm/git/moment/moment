<?php
/**
 *     Moment - update.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Ensure user has root rights
if ((int) exec('id -u') != 0)
    die (basename(__FILE__) .' script must be launch with root privileges.'."\n");

// Parse arguments / env
$options = getopt('hvqyt:b:', array('help', 'target:', 'branch:'));

if(array_key_exists('h', $options) || array_key_exists('help', $options)) {
    echo 'Install / updates the ApplicationBase'."\n";
    echo "\t\n";
    echo 'Usage :'."\n";
    echo "\t".' php update.php [-h|--help] [-v] [-q] [-y] [-t <target>|--target=<target>] [--git-user=<user>]'."\n";
    echo "\t\n";
    echo "\t".' -b / --branch : use a specific branch of Ekko project'."\n";
    echo "\t".' -h / --help : this help'."\n";
    echo "\t".' -v : verbose mode'."\n";
    echo "\t".' -q : quiet mode'."\n";
    echo "\t".' -y : automatically answer yes to all questions'."\n";
    echo "\t".' -t <target> / --target=<target> : set target directory (will be created unless it exists)'."\n";
    exit;
}

global $quiet;
$quiet = array_key_exists('q', $options);

global $verbose;
$verbose = !$quiet && array_key_exists('v', $options);

global $yes;
$yes = array_key_exists('y', $options);

/**
 * Displays a message
 *
 * @param $msg
 */
function msg($msg) {
    global $quiet;
    if($quiet) return;
    
    echo $msg."\n";
}

/**
 * Displays a verbose message
 *
 * @param $msg
 */
function verbose($msg) {
    global $verbose;
    if(!$verbose) return;
    
    msg($msg);
}

/**
 * Displays an error and die
 *
 * @param $msg
 */
function error($msg) {
    die($msg.', exiting');
}

/**
 * Run a system command somewhere and get the result
 *
 * @param string $command
 * @param string $target_dir
 *
 * @return array
 */
function run($command, $target_dir = null) {
    if($target_dir)
        $command = 'cd '.escapeshellarg($target_dir).' && '.$command;
    
    verbose($command);
    
    $ret = null;
    $output = array();
    
    exec($command, $output, $ret);
    
    if($ret) error($command.' execution failed');
    
    return $output;
}

if(array_key_exists('t', $options) && array_key_exists('target', $options))
    error('Multiple targets found, exiting');

$target = '.';

if(array_key_exists('t', $options))
    $target = $options['t'];

if(array_key_exists('target', $options))
    $target = $options['target'];

$b = null;
$branch = '';
if(array_key_exists('b', $options))
    $b = $options['b'];

if(array_key_exists('branch', $options))
    $b = $options['branch'];

if (!is_null($b))
    $branch = '-b '.$b.' ';

$context = array_shift($argv);

if(!$target) {
    if($context == '-') {
        $target = '.';
    } else {
        $target = dirname(__FILE__).'/../../';
    }
}

// Check commands

if(!trim(shell_exec('which git')))
    error('git client not found');


// Prepare folders

$target = realpath($target);

$tmp = realpath(sys_get_temp_dir()).'/applicationbase_update_'.uniqid();

msg('Target directory : '.$target);
msg('Temp directory : '.$tmp);

if(is_dir($tmp) || is_file($tmp))
    error('Temp directory already exists');

if(!$yes && !$quiet)
    readline('Press enter key to continue ...');

if(!is_dir($target)) {
    verbose('Target directory '.$target.' does not exists, creating');
    
    if(!mkdir($target, 0777, true))
        error('Could not create '.$target);
}

if(!is_dir($tmp)) {
    verbose('Temp directory '.$tmp.' does not exists, creating');
    
    if(!mkdir($tmp, 0777, true))
        error('Could not create '.$tmp);
    
    if(!chmod($tmp, 0777))
        error('Could not chmod '.$tmp);
}

$version_file = $target.'/tmp/applicationbase.version';
$version = null;
if(file_exists($version_file)) {
    $version = trim(explode("\n", file_get_contents($version_file))[1]);
    msg('Current ApplicationBase version is '.$version);
    
} else {
    // New setup
    msg('Seems that this is a new setup');
    $quiet = false;
    $yes = false;
}


// Going into tmp
if(!chdir($tmp))
    error('Could not change current directory to tmp');


// Clone into temp
msg('Cloning remote repository into temp ...');
run('git clone git://git.renater.fr/applicationbase.git '.$branch.$tmp.($verbose ? ' -v' : '').($quiet ? ' -q' : ''));
msg('... done.');


// Get new version
$new_version = run('git rev-parse '.(is_null($b) ? 'HEAD' : $b), $tmp)[0];

if($version == $new_version) {
    msg('Up to date');
    
} else {
    msg('Going to '.($version ? 'update to' : 'install').' ApplicationBase version '.$new_version);
    
    if($version) {
        // If update get file moves
        $deleted_files = array();
        $cnt = 0;
        $ignore = '`^(config/(config|dav)\.php|logs/.*|tmp/.*|data/.*|view/cache/.*)$`';
        foreach(array_reverse(run('git rev-list '.$version.'..'.(is_null($b) ? 'HEAD' : $b), $tmp)) as $commit) {
            if(!$commit) continue;
            
            foreach(run('git show '.$commit.' | grep -B1 "deleted file" | grep "diff --git" | awk \'{print substr($3, 2)}\'', $tmp) as $file) {
                if(preg_match($ignore, $file)) {
                    verbose($file.' deletion set to be ignored');
                    continue;
                }
                
                $dir = dirname($file);
                
                if(!array_key_exists($dir, $deleted_files))
                    $deleted_files[$dir] = array();
                
                $deleted_files[$dir][] = $file;
                $cnt++;
            }
        }
        
        if($cnt) {
            verbose($cnt.' files have been deleted since last update, cleaning them up');
            ksort($deleted_files);
            
            foreach(array_reverse($deleted_files, true) as $dir => $files) {
                msg('Cleaning '.$dir);
                
                foreach($files as $file) {
                    if(file_exists($target.$file)) {
                        verbose('Removing file '.$file);
                        
                        if(!unlink($target.$file))
                            error('Could not remove file '.$target.$file);
                    }
                }
                
                // Remove directory if empty
                if(is_dir($target.$dir) && !count(preg_grep('`^([^\.]|\.[^\.])`', scandir($target.$dir)))) {
                    verbose('Directory is empty, removing it');
                    
                    if(!rmdir($target.$dir))
                        error('Could not remove directory '.$target.$dir);
                }
                
                verbose('Done for '.$dir);
            }
        }
    }
    
    
    // Cleanup clone
    msg('Removing git files from clone ...');
    $opts = array('-r', '-f');
    if($verbose) $opts[] = '-v';
    run('rm '.implode(' ', $opts).' '.$tmp.'/.git');
    msg('... done.');
    
    
    // Spawning directory tree
    msg('Spawning tree into target ...');
    run('find . -type d -exec mkdir '.($verbose ? '-v' : '').' -p \''.$target.'/{}\' \\;');
    msg('... done.');
    
    
    // Copy files
    msg('Copying files to target ...');
    if(!$yes && !$quiet)
        readline('This operation will override any modifications of core files, press enter key to continue ...');
    
    run('find . -type f -exec cp '.($verbose ? '-v' : '').' -f \'{}\' \''.$target.'/{}\' \\;');
    msg('... done.');
}

// Going into target
if(!chdir($target))
    error('Could not change current directory to target');


if($version) {
    // Look for unexpected files
    msg('Looking for unexpected files ...');
    $opts = array($yes ? '-f' : '-i');
    if($verbose) $opts[] = '-v';
    foreach(run('find . -type f | grep "/core/"') as $file) {
        if(file_exists($tmp.'/'.$file)) continue;
        msg('Found unexpected file in core : '.$file);
        msg('Maybe this is a file that was removed from the base (no application related files should be added to the core)');
        run('rm '.implode(' ', $opts).' '.$file);
        msg(''); // Line feed bug
    }
    msg('... done.');
    
    
    // Look for unexpected directories
    msg('Looking for unexpected directories ...');
    $opts = array($yes ? '-f' : '-i');
    if($verbose) $opts[] = '-v';
    foreach(run('find . -type d | grep "/core/"') as $dir) {
        if(is_dir($tmp.'/'.$dir)) continue;
        msg('Found unexpected directory in core : '.$dir);
        msg('Maybe this is a directory that was removed from the base (no application related directory should be added to the core)');
        run('rm '.implode(' ', $opts).' '.$dir);
    }
    msg('... done.');
}

// Remove temporary clone
msg('Removing temporary clone ...');
$opts = array('-r', '-f');
if($verbose) $opts[] = '-v';
run('rm '.implode(' ', $opts).' '.$tmp);
msg('... done.');


// Store version
if(!file_put_contents($version_file, "# Automatically generated\n".$new_version))
    error('Could not write to '.$version_file);


// New setup ?
if(!$version) {
    
}

// File rights management
// Get config from file

define('EKKO_ROOT', realpath(dirname(__FILE__).'/../../').'/');

$config = array();
$configFile = $target.'/config/config.php';
if (file_exists($configFile))
    include $configFile;

$ownedFolders = array('data', 'logs', 'tmp', 'view/cache'); 
if (array_key_exists('application_uid', $config)){
    $id = $config['application_uid'];
    
    if (array_key_exists('application_gid', $config)){
        $id .= ':'.$config['application_gid'];
    }
    $ownedFolders = array('data', 'logs', 'tmp', 'view/cache');
    run('chown -R '.$id.' '.  implode(' ', array_map(function($k) use ($target){
        return escapeshellarg($target.'/'.$k);
    } , $ownedFolders)));
    
} else {
    msg('Could not find application_uid in config (perhaps it does not exist yet ?), you should set your webserver as owner of the following folders and their contents: '.implode(', ', $ownedFolders));
}

// All done !
msg('Done installing / updating');

