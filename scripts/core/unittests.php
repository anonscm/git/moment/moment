<?php
/**
 *     Moment - unittests.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Ensure user has root rights
if ((int) exec('id -u') != 0)
    die (basename(__FILE__) .' script must be launch with root privileges.'."\n");

global $user;
$user = null;
foreach(explode("\n", shell_exec('env')) as $line) {
    if(preg_match('`^SUDO_USER=(.+)$`', $line, $m))
        $user = $m[1];
}

global $base;
$base = realpath(dirname(__FILE__).'/../../');

// Parse arguments / env
$options = getopt('hvqf:c:s:l:', array('help', 'test-file:', 'config-file:', 'test-suite:', 'coverage', 'phpunit:', 'log-folder:'));

if(array_key_exists('h', $options) || array_key_exists('help', $options)) {
    echo 'Run unittests'."\n";
    echo "\t\n";
    echo 'Usage :'."\n";
    echo "\t".' php unittests.php [-h|--help] [-v] [-f <file>|--test-file=<file>] [-c <file>|--config-file=<file> [-s <name>|--test-suite=<name>]] [--coverage] [--phpunit=<path>]'."\n";
    echo "\t\n";
    echo "\t".' -h / --help : this help'."\n";
    echo "\t".' -v : verbose mode'."\n";
    echo "\t".' -q : quiet mode'."\n";
    echo "\t".' -f <file> / --test-file=<file> : run specific test file (accepts as many as needed)'."\n";
    echo "\t".' -c <file> / --config-file=<file> : load config file (accepts as many as needed)'."\n";
    echo "\t".' -s <name> / --test-suite=<name> : run specific test suite from config file (accepts as many as needed)'."\n";
    echo "\t".' -l <path> / --log-folder=<path> : save log into path'."\n";
    echo "\t".' --coverage : generate code coverage'."\n";
    echo "\t".' --phpunit=<path> : specify phpunit binary path'."\n";
    exit;
}

global $quiet;
$quiet = array_key_exists('q', $options);

global $verbose;
$verbose = !$quiet && array_key_exists('v', $options);

global $phpunit;
global $coverage;

/**
 * Displays a message
 *
 * @param $msg
 */
function msg($msg) {
    global $quiet;
    if($quiet) return;
    
    echo $msg."\n";
}

/**
 * Displays a verbose message
 *
 * @param $msg
 */
function verbose($msg) {
    global $verbose;
    if(!$verbose) return;
    
    msg($msg);
}

/**
 * Displays an error and die
 *
 * @param $msg
 */
function error($msg) {
    die($msg.', exiting');
}

/**
 * Run phpunit and return result
 *
 * @param string $file
 * @param array $suites
 * @param string $logs
 *
 * @return int
 */
function runPhpunit($file, array $suites, $logs) {
    global $verbose;
    global $base;
    global $coverage;
    global $phpunit;
    
    if(!is_dir($logs) && !mkdir($logs, 0755, true))
        error('Cannot create dir '.$logs);
    
    $log = $logs.'/tests.log';
    $out = $logs.'/tests.out';
    
    $cmd = $phpunit.' --debug';
    $bootstrap_file = $base.'/unittests/core/bootstrap.php';
    
    if(preg_match('`\.xml`', $file)) {
        $init = dirname($file) . '/bootstrap.php';
        
        if (file_exists($init))
            $bootstrap_file = $init;
        
        $xml = simplexml_load_file($file);
        if (isset($xml['bootstrap']))
            $bootstrap_file = null;
    }
    
    if ($bootstrap_file)
        $cmd .= ' --bootstrap '.escapeshellarg($bootstrap_file);
    
    if($verbose) $cmd .= ' -v';
    
    if(preg_match('`\.xml`', $file)) {
        $cmd .= ' -c '.escapeshellarg($file);
        
        if(count($suites))
            $cmd .= ' --testsuite '.escapeshellarg(implode('|', $suites));
    }
    
    $cmd .= ' --log-junit '.escapeshellarg($log);
    
    if($coverage) {
        $coverage = $logs . '/coverage/';
        if (!mkdir($coverage, 0755, true))
            error('Cannot create dir ' . $coverage);
        
        $cmd .= ' --coverage-html ' . escapeshellarg($coverage);
    }
    
    if(preg_match('`\.php`', $file))
        $cmd .= ' '.escapeshellarg($file);
    
    $cmd .= ' >> '.escapeshellarg($out);
    
    verbose('Run '.$cmd);
    
    $ret = 0;
    system('cd '.escapeshellarg($base).'/unittests && '.$cmd, $ret);
    
    return $ret;
}

/**
 * Get custom log path
 *
 * @param string $logs
 * @param string $file
 *
 * @return string
 */
function getCustomLog($logs, $file) {
    global $base;
    
    $id = substr($file, 0, -4);
    if(strpos($id, $base) === 0)
        $id = substr($id, strlen($base));
    
    $id = trim(str_replace('/', '_', $id), '_');
    
    return $logs.'/'.$id;
}

$test_files = array();
if(array_key_exists('f', $options))
    $test_files = array_merge($test_files, (array)$options['f']);

if(array_key_exists('test-file', $options))
    $test_files = array_merge($test_files, (array)$options['test-file']);


$config_files = array();
if(array_key_exists('c', $options))
    $config_files = array_merge($config_files, (array)$options['c']);

if(array_key_exists('config-file', $options))
    $config_files = array_merge($config_files, (array)$options['config-file']);


$test_suites = array();
if(array_key_exists('s', $options))
    $test_suites = array_merge($test_suites, (array)$options['s']);

if(array_key_exists('test-suite', $options))
    $test_suites = array_merge($test_suites, (array)$options['test-suite']);


date_default_timezone_set('Europe/Paris');
$logs = $base.'/logs/unittests/'.date('Ymd_His');
if(array_key_exists('l', $options))
    $logs = $options['l'];

if(array_key_exists('log-folder', $options))
    $logs = $options['log-folder'];

$coverage = array_key_exists('coverage', $options);

$phpunit = 'php '.$base.'/lib/core/phpunit/phpunit.phar';
if(array_key_exists('phpunit', $options))
    $phpunit = $options['phpunit'];


// Check requisites
if(!count($test_files) && !count($config_files))
    $config_files[] = 'tests';

$suites = array();
$suites_by_config = array();
foreach($config_files as &$file) {
    if(!preg_match('`\.xml$`', $file))
        $file = $base.'/unittests/'.trim($file, '/').'.xml';
    
    if(!file_exists($file))
        error('Cannot locate config file at '.$file);
    
    $file = realpath($file);
    
    $suites_by_config[$file] = array();
    
    $xml = simplexml_load_file($file);
    foreach($xml->testsuites->testsuite as $suite) {
        $suites[] = $suite['name'];
        $suites_by_config[$file][] = $suite['name'];
    }
}

foreach($test_suites as $suite)
    if(!in_array($suite, $suites))
        error('Test suite '.$suite.' not found in config files');

// Prepare folders
if(!is_dir($logs) && !mkdir($logs, 0755, true))
    error('Cannot create dir '.$logs);

$error = 0;

// Run each config file with its specific suites if needed
foreach($config_files as $config_file) {
    msg('Run tests from '.$config_file);
    
    // Customize log location
    $log = $logs;
    if(count($config_files) + count($test_files) > 1)
        $log = getCustomLog($logs, $config_file);
    
    // Do we need to restrict suites ?
    $filter_suites = array();
    foreach($suites_by_config[$config_file] as $suite)
        if(in_array($suite, $test_suites))
            $filter_suites[] = $suite;
    
    $ret = runPhpunit($config_file, $filter_suites, $log);
    if($ret)
        $error = $ret;
}

// Run specific test files
foreach($test_files as $test_file) {
    msg('Run tests from '.$test_file);
    
    // Customize log location
    $log = $logs;
    if(count($config_files) + count($test_files) > 1)
        $log = getCustomLog($logs, $test_file);
    
    $ret = runPhpunit($test_file, array(), $log);
    if($ret)
        $error = $ret;
}

if($user)
    system('chown -R '.$user.' '.$logs.'/..');

// All done !
if($error) {
    msg('Testing failed, check the logs');
    exit($error);
    
} else {
    msg('Testing done');
}
