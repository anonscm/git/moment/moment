<?php
/**
 *     Moment - extract_lang.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
$options = getopt('hl:', array('help:','lang:'));

if(array_key_exists('h', $options) || array_key_exists('help', $options)) {
    echo 'Generate a lang file of the desired language.'."\n\n";
    echo 'Important : If lang file already exists, translations are preserved.'."\n";
    echo "\t\n";
    echo 'Usage :'."\n";
    echo "\t".' php extract_lang.php [-h|--help] [-l|--lang <local ID>]'."\n";
    echo "\n".'Example : php extract_lang.php -l fr_FR'."\n";
    echo "\t\n";
    echo "\t".' -h | --help : this help'."\n";
    echo "\t".' -l | --lang : lang of desired lang file (Ex. "fr_FR"), defaults to "", when empty it generates a sample lang.php file at language root directory'."\n";
    exit;
}

$localeID = null;
if(array_key_exists('l', $options))
    $localeID = $options['l'];

if(array_key_exists('lang', $options))
    $localeID = $options['lang'];

$lang = array();
$core_lang = array();
if(file_exists('./language/core/'.$localeID.'/lang.php')) {
    require_once './language/core/'.$localeID.'/lang.php';
}
$core_lang = $lang;

$lang = array();
if(file_exists('./language/'.$localeID.'/lang.php')) {
    require_once './language/'.$localeID.'/lang.php';

    //Backuping old file
    if (!copy('./language/'.$localeID.'/lang.php', './language/'.$localeID.'/lang.php')) {
        echo "failed to copy";
    }
}

if ( ! function_exists('glob_recursive')) {
    // Does not support flag GLOB_BRACE

    function glob_recursive($pattern, $flags = 0) {
        $files = glob($pattern, $flags);

        foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
            $files = array_merge($files, glob_recursive($dir.'/'.basename($pattern), $flags));
        }

        return $files;
    }
}

$lang_file_header = '<?php'.PHP_EOL
    .'/* ---------------------------------'.PHP_EOL
    .' * '.(($localeID)?$localeID:'<Locale ID>').' Language File'.PHP_EOL
    .' * Maintained by <Maintener name>'.PHP_EOL
    .' * ---------------------------------'.PHP_EOL
    .'*/'.PHP_EOL;


//Clearing file
$fp = fopen("./language/$localeID/lang.php", "w");

$lang_ids = array();

if($fp) {
    fwrite($fp, $lang_file_header);

    $to_explore_filenames = [];
    foreach (glob_recursive("classes/constants/*.class.php") as $filename) {
        $to_explore_filenames[] = array(
            'filename' => $filename,
            'type' => 'class'
        );
    }
    foreach (glob_recursive("classes/model/*.class.php") as $filename) {
        $to_explore_filenames[] = array(
            'filename' => $filename,
            'type' => 'class'
        );
    }
    foreach (glob_recursive("templates/*.php") as $filename) {
        $to_explore_filenames[] = array(
            'filename' => $filename,
            'type' => 'template'
        );

        $to_explore_filenames[] = array(
            'filename' => $filename,
            'type' => 'class'
        );
    }
    foreach (glob_recursive("view/*.js") as $filename) {
        $to_explore_filenames[] = array(
            'filename' => $filename,
            'type' => 'js'
        );
    }


    foreach ($to_explore_filenames as $to_explore) {

        if(strpos($to_explore['filename'], '/core/')) {
            continue;
        }



        $data = file_get_contents($to_explore['filename']);

        $matches = array();
        if($to_explore['type'] === 'class') {
            preg_match_all('/\'([a-z_]+)\'/i', $data, $matches);
        } else if($to_explore['type'] === 'template') {
            preg_match_all('/{tr:([a-z_]+)}/i', $data, $matches);
        } else if($to_explore['type'] === 'js') {
            preg_match_all('/(?:translate|tr)\(\'([a-z_]+)\'\)/i', $data, $matches);
        }

        if (!empty($matches[1])) {

            $matches = array_flip($matches[1]);

            foreach ($matches as $lang_id => $unused) {
                if(!isset($lang_ids[$lang_id]))
                    $lang_ids[$lang_id] = array();

                $lang_ids[$lang_id][] = $to_explore['filename'];
            }
        }

    }

    $to_translate = array();
    foreach($lang_ids as $lang_id => $filenames) {

        if (count($filenames) <= 10) {

            if(isset($lang[$lang_id]) && (strpos($lang[$lang_id],'{') === false)) {
                foreach ($filenames as $filename) {
                    fwrite($fp, PHP_EOL . '// ' . $filename);
                }
                fwrite($fp, PHP_EOL . '$lang[\'' . $lang_id . '\'] = \'' . addcslashes($lang[$lang_id], "'") . '\';' . PHP_EOL);
                unset($lang[$lang_id]);
            } else {
                if(!isset($core_lang[$lang_id]) || (strpos($core_lang[$lang_id],'{') !== false)) {
                    $to_translate[$lang_id] = $filenames;
                }
            }
        }
    }
    //Writing old lang values
    foreach ($lang as $lang_id => $translation) {
        
        if(strpos($translation, '{') === false) {
            $yes_or_no = readline('An obsolete translation id "' . $lang_id . '" has been found. Keep it (y/n) :');
    
            if ($yes_or_no == 'y') {
                fwrite($fp, PHP_EOL . '// Maybe obsolete ');
                fwrite($fp, PHP_EOL . '$lang[\'' . $lang_id . '\'] = \'' . addcslashes($translation, "'") . '\';' . PHP_EOL);
            }
        }
    }
    
    foreach($to_translate as $lang_id => $filenames) {
        foreach ($filenames as $filename) {
            fwrite($fp, PHP_EOL . '// ' . $filename);
        }
        fwrite($fp, PHP_EOL . '// TODO translate ' . $lang_id);
        fwrite($fp, PHP_EOL . '$lang[\'' . $lang_id . '\'] = \'{' . $lang_id . '}\';' . PHP_EOL);
    }

    fclose($fp);
}