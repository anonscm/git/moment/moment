<?php

/**
 *     Moment - init.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Adding libraries css
 */
Event::register(Event::AFTER, 'css_libraries', function (Event $event) {

    $index = array_search('lib/core/jquery-ui/css/cupertino/jquery-ui-1.10.3.custom.min.css', $event->result);
    if($index !== FALSE){
        $event->result[$index] = 'lib/jqueryui/themes/cupertino/jquery-ui.min.css';
    }

    return $event->result;
});

/**
 * Adding JS libraries
 */
Event::register(Event::AFTER, 'js_libraries', function (Event $event) {

    //Tools
    $event->result[] = 'js/tools/inputswitch.js';
    $event->result[] = 'js/tools/arrowscrolling.js';
    $event->result[] = 'js/tools/headervisible.js';
    $event->result[] = 'js/tools/motd.js';
    $event->result[] = 'js/tools/client_storage.js';
    $event->result[] = 'js/tools/tips.js';

    // Upgrades of core libraries (JQuery, JQuery-Ui, JQuery DateTimePicker, Foundation)
    $replacements = [
        'lib/core/jquery/jquery-1.11.1.min.js'                  => 'lib/jquery/jquery.min.js',
        'lib/core/jquery-ui/js/jquery-ui-1.11.1.custom.min.js'  => 'lib/jqueryui/jquery-ui.min.js',
        'lib/core/foundation/js/vendor/foundation.js'           => 'lib/custom_foundation/custom_foundation.min.js',
        'lib/core/datetimepicker/jquery.datetimepicker.full.js' => 'lib/datetimepicker/jquery.datetimepicker.full.min.js'
    ];
    foreach ($replacements as $needle => $replacement) {
        $index = array_search($needle, $event->result);
        if($index !== FALSE){
            $event->result[$index] = $replacement;
        }
    }

    // Removing Moment.js
    foreach($event->result as $index => $lib) {
        if(strpos($lib, 'lib/core/moment') !== false)
            unset($event->result[$index]);
    }
    // Adding our own version of Moment.js
    $event->result[] = 'lib/moment/moment.min.js';
    $event->result[] = 'lib/moment/locale/fr.js';
    $event->result[] = 'lib/moment-timezone/moment-timezone-with-data-10-year-range.min.js';

    return $event->result;
});

Event::register(Event::AFTER, 'page_js_libraries', function (Event $event) {

    $page = GUI::getPage(null, 0);

    //FullCalendar
    if('user' === $page) {
        $event->result[] = 'lib/fullcalendar/core/index.global.min.js';
        $event->result[] = 'lib/fullcalendar/core/locales/fr.global.js';
        $event->result[] = 'lib/fullcalendar/daygrid/index.global.min.js';
    }

    if('survey' === $page && in_array(GUI::getPage(null, 1), ['update', 'create'])) {
        //MeltDown
        $event->result[] = 'lib/meltdown/js/jquery.meltdown.min.js';
        $event->result[] = 'lib/meltdown/js/lib/element_resize_detection.min.js';
        $event->result[] = 'lib/meltdown/js/lib/js-markdown-extra.min.js';
        $event->result[] = 'lib/meltdown/js/lib/rangyinputs-jquery.min.js';

        //Flatpickr
        $event->result[] = 'lib/flatpickr/flatpickr.min.js';
        $event->result[] = 'lib/flatpickr/l10n/fr.js';

        //Multi date input
        $event->result[] = 'js/tools/multidateselector.js';
    }

    return $event->result;
});

Event::register(Event::AFTER, 'page_css_libraries', function (Event $event) {

    $page = GUI::getPage(null, 0);

    if('survey' === $page && in_array(GUI::getPage(null, 1), ['update', 'create'])) {
        //MeltDown
        $event->result[] = 'lib/meltdown/css/meltdown.css';

        //Flatpickr
        $event->result[] = 'lib/flatpickr/flatpickr.min.css';
    }

    return $event->result;
});

Event::register(Event::AFTER, 'client_config', function(Event $event) {
   $event->result = array_merge($event->result, array(
       'calendar_colors' => Config::get('calendar.colors')
   ));
   return $event->result;
});

Event::register(Event::AFTER, 'sum_text_init', function (Event $event) {
    $event->result = LimitChoiceConstraint::initSumText($event);
    return $event->result;
});

if(file_exists(EKKO_ROOT.'/includes/custom_init.php'))
    include_once EKKO_ROOT.'/includes/custom_init.php';