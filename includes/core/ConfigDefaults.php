<?php

/**
 * This file is part of the BaseProject project.
 * 2015
 * Copyright (c) RENATER
 */


  // Load default configuration
$config = array();

$config['timezone'] = 'Europe/Paris'; // Default timezone to use

$config['lang'] = array(
    'default' => 'fr', // Default language to user
    
    'use_browser' => true, // Take language from user's browser's accept-language header if provided
    'use_url' => true, // Allow URL language switching (?lang=en for example)
    'use_user_pref' => true, // Take lang from user profile
    
    'selector_enabled' => true, // Display language selector (requires use_url = true)
    'save_user_pref' => true // Save lang switching in user preferences (requires use_url = true and use_user_pref = true)
);

$config['auth_sp'] = array(
    'type' => 'shibboleth',  // Authentification type
    'uid_attribute' => 'eduPersonTargetId', // Get uid attribute from authentification service
    'email_attribute' => 'mail', // Get email attribute from authentification service
    'name_attribute' => 'cn', // Get name attribute from authentification service
    
    'save_additional_attributes' => true
);

$config['auth_remote'] = array(
    'signature_algorithm' => 'sha1',
    'timeout' => 15, // requests time out, in seconds
    
    'application' => array(
        'enabled' => false,
        'applications' => array()
    ),
    
    'user' => array(
        'enabled' => false,
        'autogenerate_secret' => true,
        'sync_request_timeout' => 60 // in seconds
    )
);

$config['email'] = array(
    'use_html' => true,   // By default, use HTML on mails
    'from' => 'sender',
    'return_path' => 'sender',
    'subject_prefix' => '[{cfg:application_name}] '
);

// Logging
$config['log_facilities'] = array(
    array(
        'type' => 'file',
        'path' => EKKO_ROOT.'/logs/',
        'rotate' => 'daily',
        'level' => 'info'
    )
);

$config['application_logouturl'] = function() {
    return Config::get('application_url').'logout';
};


if(file_exists(EKKO_ROOT.'/includes/ConfigDefaults.php'))
    include EKKO_ROOT.'/includes/ConfigDefaults.php';
