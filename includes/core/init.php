<?php

/**
 * This file is part of the BaseProject project.
 * 2015 
 * Copyright (c) RENATER
 */

define('EKKO_ROOT', realpath(dirname(__FILE__).'/../../'));

// Include classes autoloader
require_once(EKKO_ROOT.'/classes/core/autoload.php');

// Set default timezone
date_default_timezone_set(Config::get('timezone'));

// Set encoding
mb_internal_encoding('UTF-8');

if (php_sapi_name() === 'cli'){
    // Command Line Interface
    include EKKO_ROOT.'/includes/core/init_cli.php';
    ApplicationContext::setProcess(ProcessTypes::CLI);
}else{
    // Default, web server
    include EKKO_ROOT.'/includes/core/init_web.php';
    ApplicationContext::setProcess(ProcessTypes::WEB);
}
// Report all errors
ini_set('display_errors', Config::get('debug') ? '1' : '0');

PluginManager::initialize();

if(file_exists(EKKO_ROOT.'/includes/init.php'))
    include EKKO_ROOT.'/includes/init.php';

(new Event('init_done'))->trigger();

// Validate config on the fly
require_once(EKKO_ROOT.'/includes/core/ConfigValidation.php');
