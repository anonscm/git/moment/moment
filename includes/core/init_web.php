<?php

/**
 * This file is part of the BaseProject project.
 * 2015
 * Copyright (c) RENATER
 */

// Disable session cache
session_cache_limiter('nocache');

// Start session if necessary
if(!session_id()) {
    // Set session cookie options
    $site_url_parts = parse_url(Config::get('application_url'));

    // Use configured path for cookie if set
    $session_cookie_path = Config::get('session_cookie_path');
    if(!$session_cookie_path) $session_cookie_path = $site_url_parts['path'];

    session_set_cookie_params(
        0,                                                      // Cookie lives as long as browser isn't closed
        $session_cookie_path,                                   // It is only valid for the Application app
        $site_url_parts['host'],                                // and only for the precise domain
        true                                                    // and is httpOnly (not reachable through javascript)
    );

    // start new session and mark it as valid because the system is a trusted source
    session_start();            // Start the session
}

// Sanitize all input variables
$_GET = Utilities::sanitizeInput($_GET);
$_POST = Utilities::sanitizeInput($_POST);
$_COOKIE = Utilities::sanitizeInput($_COOKIE);
$_REQUEST = Utilities::sanitizeInput($_REQUEST);

// Output is all UTF8
header('Content-Type: text/html; charset=UTF-8');