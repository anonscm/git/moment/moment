<?php

/**
 * This file is part of the Ekko project.
 * @see ~/LICENCE.md file for more details.
 *
 * Copyright (c) RENATER
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

if(!file_exists(EKKO_ROOT.'/config/config.php'))
    die('Configuration file not found');

ConfigValidator::addCheck('application_url', 'string');

ConfigValidator::addCheck('admin', 'string|array');
ConfigValidator::addCheck('admin_email', 'string|array');

ConfigValidator::addCheck('email.from', 'null|string|array', '::validateOptionalEmailSender');

ConfigValidator::addCheck('email.reply_to', 'null|string|array', '::validateOptionalEmailSender');

ConfigValidator::addCheck('email.return_path', 'null|string|array', '::validateOptionalEmailSender');

ConfigValidator::addCheck('db', 'array', '::validateDBConfig');

ConfigValidator::addCheck('db_admin', 'null|array', '::validateOptionalDBConfig', function($value) {
    if(!$value) return;
    
    if(!array_key_exists('username', $value) || !$value['username'])
        throw new ConfigValidationException('db_admin[username] not set or empty');
    
    if(!array_key_exists('password', $value) || !$value['password'])
        throw new ConfigValidationException('db_admin[password] not set or empty');
});

ConfigValidator::addCheck('remote_db', 'null|array', function($value) {
    if(!$value) return;
    
    foreach($value as $db => $cfg) {
        if(!is_array($cfg))
            throw new ConfigValidationException('remote_db['.$db.'] must be an array');
        
        ConfigValidator::validateDBConfig($cfg, 'remote_db['.$db.']');
    }
});

// Corespond to the unix uid the application should run as (basically the http webserver user)
// --- @see /etc/passwd
ConfigValidator::addCheck('application_uid', 'int');

// If needed, you can set it to set the group owner of created files
ConfigValidator::addCheck('application_gid', 'int|null');

ConfigValidator::addCheck('auth_remote.user.sync_request_timeout', 'null|int');

ConfigValidator::addCheck('lang.selector_enabled', function($value) {
    if(!$value) return;
    
    if(!Config::get('lang.use_url'))
        throw new ConfigValidationException('lang.use_url must be set to true if lang.selector_enabled is, otherwise the language selector won\'t work');
});

ConfigValidator::addCheck('lang.default', function($lang) {
    if(!$lang) return;
    
    $lang = Lang::realCode($lang);
    $available = Lang::getAvailableLanguages();
    
    if(!array_key_exists($lang, $available))
        throw new ConfigValidationException('lang.default must be one of the available languages defined in locale.php ('.implode(', ', array_keys($available)).')');
});

if(file_exists(EKKO_ROOT.'/includes/ConfigValidation.php'))
    include EKKO_ROOT.'/includes/ConfigValidation.php';

ConfigValidator::run();
