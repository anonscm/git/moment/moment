<?php

/**
 *     Moment - ConfigDefaults.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

$default['result_csv_export_separator'] = ';';

$default['email'] = array(
    'use_html' => true,  // By default, use HTML on mails
    'subject_prefix' => '[{cfg:application_name}] ',
    'from' => array(
        'name' => 'Moment',
        'email' => 'nepasrepondre@renater.fr'
    ),
    'return_path' => 'nepasrepondre@renater.fr',
    'style' => array(
        'main' => 'line-height: 1.5;background-color: #fff; color:#304c6f; text-align: center; font-family: LatoWebBold, Arial, sans-serif;padding-bottom:1.5rem;margin:0 auto 1.5rem auto;width:75%;border: 3px solid #304c6f; border-radius:3px 3px 0 0;',
        'h1' => 'background-color: #304c6f;font-size:1.2rem; color: #fff; padding:0.5em; margin-top:0;',
        'table' => 'border:1px solid #304c6f; background-color:#fff;color:#304c6f;width:90%; margin:0 auto;padding:1rem;',
        'green_button' => 'display:inline-block; text-align: center;line-height: 1;cursor: pointer;-webkit-appearance: none; vertical-align: middle; border: 1px solid transparent; border-radius: 3px; padding: 0.85em 1em; margin: 1rem 0; font-size: 1.1rem; background-color: #84C33F; color: #fefefe; text-decoration:none',
        'red_button' => 'display:inline-block; text-align: center;line-height: 1;cursor: pointer;-webkit-appearance: none; vertical-align: middle; border: 1px solid transparent; border-radius: 3px; padding: 0.85em 1em; margin: 1rem 0; font-size: 1.1rem; background-color: #FF5D38; color: #fefefe; text-decoration:none'
    )
);

$default['calendar'] = array(
    'timeout' => 5, // Timeout in seconds
    'max_size' => 200000, // ~ 200KB
    'status_validity_duration' => 60, // In minutes
    'allowed_types' => array(
        'text/calendar',
        'text/ical'
    ),
    'tips' => true,
    'colors' => array(
        '#6b79b8'
    ),
    'mycal_color' => '#ff8d00',
    'mycal_past_events_limit' => '-1 month'
);

$default['faq'] = array(
    'enabled' => true,
    'type' => 'json',
    'path' => EKKO_ROOT.'/includes/faq_content.json'
);

$default['use_survey_search'] = true;

$default['closing_reminder_delay'] = 5;

$default['removal_reminder_delay'] = 7;

/**
 * User cleaning configuration
 */
$default['user_cleaning'] = [
    'enabled' => true,
    'dry_run' => true,
    'inactive_months' => 24,
    'limit' => false,
];

/**
 * Defining available moments of day
 */
$default['moments_of_day'] = [
    'morning'   => ['bt' => '08:00', 'et' => '12:00'],
    'noon'      => ['bt' => '12:00', 'et' => '14:00'],
    'afternoon' => ['bt' => '14:00', 'et' => '18:00'],
    'evening'   => ['bt' => '18:00', 'et' => '22:00']
];

$config = array_merge($config, $default);