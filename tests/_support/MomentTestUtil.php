<?php
/**
 *     Moment - MomentTestUtil.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
use AspectMock\Test as mock;

/**
 * moment - MomentTestUtil.php
 *
 * Initial version by: Florian Bruneau-Voisine
 * Initial version created on: 14/10/19
 *
 * Copyright (c) RENATER
 */
class MomentTestUtil {

    public static $model = array();

    public static $cnt = 0;

    public static $goodSurveyData = array(
        "title" => "Test survey",
        "place" => "It will be there",
        "description" => "This is the description",
        "settings" => array(
            "auto_close" => 1589004000,
            "enable_anonymous_answer" => 0,
            "disable_answer_edition" => 0,
            "enable_update_notification" => 0,
            "limit_participants" => 0,
            "limit_participants_nb" => "1",
            "reply_access" => "opened_to_everyone",
            "hide_answers" => 0,
            "hide_comments" => 0,
            "show_participant_name" => 1,
            "show_participant_email" => 0
        ),
        "questions" => array(
            array(
                "id" => 1,
                "title" => "Question 1",
                "type" => "text",
                "position" => 0,
                "options" => array(
                    "force_unique_choice" => 0,
                    "enable_maybe_choices" => 0
                ),
                "propositions" => array(
                    array(
                        "id" => 1,
                        "type" => "text",
                        "position" => 0,
                        "header" => "Proposition 1",
                        "text" => ""
                    ),
                    array(
                        "id" => 2,
                        "type" => "text",
                        "position" => 1,
                        "header" => "Proposition 2",
                        "text" => ""
                    )
                )
            ),
            array(
                "id" => 2,
                "title" => "Question 2",
                "type" => "date",
                "position" => 0,
                "options" => array(
                    "force_unique_choice" => 0,
                    "enable_maybe_choices" => 1
                ),
                "propositions" => array(
                    array(
                        "id" => 1,
                        "type" => "date",
                        "base_day" => 1485324800
                    ),
                    array(
                        "id" => 2,
                        "type" => "date",
                        "base_day" => 1475324800,
                        "base_time" => 28800
                    )
                )
            )
        ),
        "owners" => array(
            array(
                "email" => "test+owner@test.com"
            )
        ),
        "guests" => array(
            array(
                "email" => "test@test.com"
            )
        )
    );

    public static function startUp() {
        //Following var is required to process
        $_SERVER['HTTP_USER_AGENT'] = 'Test';

        self::$model = array();

        mock::double('Entity', [
            'save' => true,
            'delete' => null,
            'fromId' => function ($id) {
                return MomentTestUtil::$model[$id];
            },
            '__get' => function($property) {
                if('storedInDatabase' == $property) {
                    return true;
                }
                return __AM_CONTINUE__;
            },
            '__set' => function($property, $value) {
                if('id' === $property && property_exists($this, $property))
                    $this->id = $value;

                return __AM_CONTINUE__;
            },
            'getRelated' => function($other) {
                return MomentTestUtil::getRelated($this, $other);
            },
            'addRelated' => function($other) {
                MomentTestUtil::register($this);
                MomentTestUtil::register($other);

                MomentTestUtil::registerRelated($this, $other);

                return true;
            },
            'dropRelated' => function($other) {
                return MomentTestUtil::dropRelated($this, $other);
            },
            'all' => function($criteria, $placeholders) {
                return MomentTestUtil::all(self::name(), $criteria, $placeholders);
            }
        ]);

        mock::double('DBI', ['prepare' => new DBIStatement(new PDOStatement(''))]);
        mock::double('DBIStatement', ['execute' => null]);
        mock::double('DBIStatement', ['fetch' => true]);
        mock::double('DBIStatement', ['fetchAll' => array()]);
        mock::double('DBIStatement', ['fetchColumn' => 1]);

        mock::double('UserBase', ['createIfNotExists' => null,
            '__get' => function($property) {
                switch(strtolower($property)) {
                    case 'name':
                        return 'Test User';
                    case 'email':
                        return 'test@user.com';
                    case 'calendars':
                        return MomentTestUtil::all('Calendar');
                    default:
                        return __AM_CONTINUE__;
                }
            }]);

        mock::double('Logger', ['error' => true]);

        self::register(User::create('user1@renater.fr'), 'user1');

        self::register(User::create('user2@renater.fr'), 'user2');

        mock::double('KeyGenerator', ['generateKey' => 'calendar_hash']);

        mock::double('MomentApplicationMail', ['send' => true]);
    }

    public static function register($entity, $id = null) {
        if(!isset($id)) {
            $id = ++self::$cnt;

            if(property_exists($entity, 'id')) {
                if($entity->id == null) {
                    try {
                        $entity->id = $id;
                    } catch (PropertyAccessException $e) {
                    }
                } else {
                    $id = $entity->id;
                }
            }
        }
        self::$model[$id] = $entity;
    }

    public static function registerRelated($entity, $related) {
        if(property_exists($entity, 'id') && $entity->id != null
            && property_exists($related, 'id') && $related->id != null
        ) {
            if(!isset(self::$model[$entity->id.'_related_'.get_class($related)]))
                self::$model[$entity->id.'_related_'.get_class($related)] = [];

            self::$model[$entity->id.'_related_'.get_class($related)][] = $related->id;

            //Register the opposite relation
            if(!isset(self::$model[$related->id.'_related_'.get_class($entity)]))
                self::$model[$related->id.'_related_'.get_class($entity)] = [];

            self::$model[$related->id.'_related_'.get_class($entity)][] = $entity->id;
        }

    }

    public static function getRelated($entity, $related_class) {
        $related = array();
        if(property_exists($entity, 'id') && $entity->id != null && isset(self::$model[$entity->id.'_related_'.$related_class])) {
            $related_ids = self::$model[$entity->id.'_related_'.$related_class];
            foreach($related_ids as $id) {
                if(isset(self::$model[$id]) && $related_class == get_class(self::$model[$id])) {
                    $related[] = self::$model[$id];
                }
            }
        }

        if(count($related) == 1 && (
            in_array('@'.$related_class, $entity::getHasOne())
            || in_array($related_class, $entity::getHasOne())
            )) {
            return array_shift($related);
        }

        Logger::debug(get_class($entity).' '.$entity->id.' '.$related_class.' '.$entity->id.'_related_'.$related_class.' count :'.count($related));

        return $related;
    }

    public static function dropRelated($entity, $related_class) {
        if(property_exists($entity, 'id') && $entity->id != null && isset(self::$model[$entity->id.'_related_'.$related_class])) {
            $related_ids = self::$model[$entity->id.'_related_'.$related_class];
            foreach($related_ids as $id) {
                if(isset(self::$model[$id]) && $related_class == get_class(self::$model[$id])) {
                    unset(self::$model[$id]);
                }
            }
        }

        Logger::debug(get_class($entity).' '.$entity->id.' '.$related_class.' '.$entity->id.'_related_'.$related_class.' dropped.');
    }

    public static function fromId($id) {
        return self::$model[$id];
    }

    public static function all($type = null, $criteria = '', $placeholder = array()) {
        if(isset($type)) {
            $tmp = array();
            foreach (self::$model as $id => $item) {
                if($item instanceof $type) {
                    $tmp[$id] = $item;
                }
            }
            return $tmp;
        } else {
            return self::$model;
        }
    }

    public static function authenticate($userId = 'user1') {
        mock::double('Auth', ['user' => self::fromId($userId)]);
        return self::fromId($userId);
    }

    public static function deauthenticate() {
        mock::double('Auth', ['user' => false]);
    }

    public static function initSurvey($id = 'survey1') {
        self::authenticate();
        mock::double('Survey', [
            'save' => true,
            'delete' => null,
            'generateUID' => 'iddetest'.$id,
            'sendInvitations' => true
        ]);

        //Setting auto close 10 days later
        self::$goodSurveyData['settings']['auto_close'] = time() + 60 * 60 * 24 * 10;

        self::register(Survey::create(self::$goodSurveyData), $id);

        $survey = self::fromId($id);
        mock::double('Owner', ['fromSurvey' => array(Owner::create($survey, Auth::user()))]);

        //Mocking 'fromQuestion' for proposition types
        mock::double('DateProposition', ['fromQuestion' => function(Question $question) { return MomentTestUtil::getRelated($question, 'DateProposition'); }]);
        mock::double('TextProposition', ['fromQuestion' => function(Question $question) { return MomentTestUtil::getRelated($question, 'TextProposition'); }]);

        return $survey;
    }

    public static function initParticipant($survey_id = 'survey1', $choice = YesNoMaybeValues::YES) {
        $survey = self::fromId($survey_id);

        $participant = Participant::getParticipant($survey, [
            'participant' => [
                'name' => 'Test',
                'email' => 'test@user.com'
            ]
        ], true);

        foreach($survey->Questions as $question) {
            $choices = [];

            foreach($question->propositions as $proposition) {
                $currentChoice = $choice;

                if(count($choices) > 0 && QuestionAnswerTypes::UNIQUE == $question->answer_type)
                    $currentChoice = YesNoMaybeValues::NO;

                $choices[] = (object)[
                    'proposition_id' => $proposition->id,
                    'value' => $currentChoice,
                ];
            }

            Answer::create($participant, $question, ['choices' => $choices, 'comment' => 'This is a comment']);
        }

        return $participant;
    }

}