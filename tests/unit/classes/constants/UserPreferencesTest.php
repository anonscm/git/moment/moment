<?php
/**
 *     Moment - UserPreferencesTest.php
 *
 * Copyright (C) 2024  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use AspectMock\Test as mock;

class UserPreferencesTest extends \Codeception\Test\Unit {

    protected function _before() {
    }

    protected function _after() {
        mock::clean();
    }

    public function testGetInput() {
        $settingsMock = mock::double('Settings', ['getInput' => true]);

        mock::double('Config', ['get' => function($property) {
            switch ($property) {
                case 'user_preferences':
                    return [
                        'pref1' => [
                            'label' => 'label_pref1',
                            'type' => 'switch',
                            'default' => true
                        ],
                        'pref2' => [
                            'label' => 'label_new_pref',
                            'type' => 'text',
                            'default' => ''
                        ]
                    ];
            }
            return __AM_CONTINUE__;
        }]);

        $up = UserPreferences::getInput('pref1');

        $settingsMock->verifyInvoked('getInput', ['pref1', [], true]);

        $up = UserPreferences::getInput('pref2');

        $settingsMock->verifyInvoked('getInput', ['pref2', [], '']);

    }
}
