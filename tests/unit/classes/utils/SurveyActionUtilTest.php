<?php
/**
 *     Moment - SurveyActionUtilTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use AspectMock\Test as mock;

/**
 * Class SurveyActionUtilTest
 *
 * Tests SurveyActionUtil class
 */
class SurveyActionUtilTest extends \Codeception\Test\Unit {

    private $testActions = array(
        'edit' => array(
            'icon' => 'fa-pencil-square-o',
            'for' => array('opened', 'draft')
        ),
        'show_results' => array(
            'icon' => 'fa-bar-chart',
            'for' => array('opened', 'closed')
        ),
        'send_invitations_reminder' => array(
            'icon' => 'fa-envelope-o',
            'for' => array('opened')
        ),
        'duplicate' => array(
            'icon' => 'fa-files-o',
            'for' => array('opened', 'closed')
        ),
        'end_survey' => array(
            'icon' => 'fa-gavel',
            'for' => array('opened')
        ),
        'reopen_survey' => array(
            'icon' => 'fa-unlock-alt',
            'for' => array('closed')
        ),
        'reopen_survey_disabled' => array(
            'icon' => 'fa-unlock-alt disabled',
            'for' => array('closed')
        ),
        'delete' => array(
            'icon' => 'fa-trash-o',
            'for' => array('opened', 'draft', 'closed')
        )
    );

    private $testTypes = array('opened', 'draft', 'closed', 'opened_wo_results', 'closed_wo_results');

    /**
     * Prepare context before each test
     *
     * @throws Exception
     */
    protected function _before() {
        MomentTestUtil::startUp();
        MomentTestUtil::authenticate();
        MomentTestUtil::initSurvey();
    }

    /**
     * Clean after every test
     */
    protected function _after() {
        mock::clean();
    }

    public function testInitActionButtonsForTypes() {

        $result = SurveyActionUtil::initActionButtonsForTypes($this->testActions, $this->testTypes);

        $this->assertCount(count($this->testTypes), $result);

        //We must ensure all allowed action is present and visible for each survey type
        foreach($this->testTypes as $type) {
            $this->assertNotNull($result[$type]);

            foreach($result[$type] as $action => $icon) {
                $this->assertNotNull($this->testActions[$action]);

                // Action is allowed, it should be visible and icon must be the selected one
                if(isset(array_flip($this->testActions[$action]['for'])[$type])) {
                    $this->assertStringNotContainsString('fa-invisible', $icon);
                    $this->assertStringContainsString($this->testActions[$action]['icon'], $icon);
                } else {
                    // If action is not alloawed it's simply "hidden", of course action won't work neither
                    $this->assertStringContainsString('fa-invisible', $icon);
                }

            }
        }

    }

    public function testGetForSurvey() {

        $survey = Survey::fromId('survey1');

        $action_icons = SurveyActionUtil::initActionButtonsForTypes($this->testActions, $this->testTypes);

        $actionsAsString = SurveyActionUtil::getForSurvey($survey, $action_icons, false);

        //We cannot test more as actionsAsString contains all icons
        $this->assertNotEmpty($actionsAsString);

        $this->assertStringContainsString('data-action="end_survey"', $actionsAsString);
        $this->assertStringNotContainsString('data-action="reopen_survey"', $actionsAsString);

        mock::double('Survey', [
            '__get' => function($property) {
                if('is_closed' === $property)
                    return true;

                return __AM_CONTINUE__;
            }
        ]);

        $actionsAsString = SurveyActionUtil::getForSurvey($survey, $action_icons, false);

        $this->assertStringNotContainsString('data-action="end_survey"', $actionsAsString);
        $this->assertStringContainsString('data-action="reopen_survey"', $actionsAsString);

        // For a closed survey with an ultimate participant
        mock::double('Survey', [
            '__get' => function($property) {
                if('ultimate_participant' === $property)
                    return true;

                if('is_closed' === $property)
                    return true;

                return __AM_CONTINUE__;
            }
        ]);

        $actionsAsString = SurveyActionUtil::getForSurvey($survey, $action_icons, false);

        $this->assertStringNotContainsString('data-action="end_survey"', $actionsAsString);
        $this->assertStringNotContainsString('data-action="reopen_survey"', $actionsAsString);
        $this->assertStringContainsString('data-action="reopen_survey_disabled"', $actionsAsString);


        //For a guest and owner hides answers
        mock::double('Survey', [
            '__get' => function($property) {
                if('hide_answers' === $property)
                    return true;

                return __AM_CONTINUE__;
            }
        ]);

        $actionsAsString = SurveyActionUtil::getForSurvey($survey, $action_icons, true);

        //We should show opened without result
        $this->assertEquals(implode($action_icons['opened_wo_results']), $actionsAsString);
    }
    
}
