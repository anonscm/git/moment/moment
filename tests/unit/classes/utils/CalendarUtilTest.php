<?php
/**
 *     Moment - CalendarUtilTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// include_once __DIR__ . '/../../src/includes/core/init.php';

use AspectMock\Test as mock;

class CalendarUtilTest extends \Codeception\Test\Unit {

    protected function _before() {
    }

    protected function _after() {
        mock::clean();
    }

    /**
     * Test parsing an IFB Content
     */
    public function testParseIFB() {
        $content = <<<IFB
BEGIN:VCALENDAR
BEGIN:VFREEBUSY
DTSTAMP:20191001T112451Z
DTSTART:20190831T112451Z
DTEND:20191202T112451Z
FREEBUSY;FBTYPE=BUSY:20190902T094500Z/20190902T100000Z
FREEBUSY;FBTYPE=BUSY:20190903T094500Z/20190903T100000Z
FREEBUSY;FBTYPE=BUSY:20190904T083000Z/20190904T093000Z
FREEBUSY;FBTYPE=BUSY:20190920T220000Z/20190925T220000Z
END:VFREEBUSY
END:VCALENDAR
IFB;

        $events = CalendarUtil::parse($content);

        $this->assertEquals(count($events), 4);

        $eventsExpected = [
            [
                'title' => '',
                'start_ts' => 1567417500,
                'end_ts' => 1567418400,
                'start' => '2019-09-02T11:45:00',
                'end' => '2019-09-02T12:00:00',
            ],[
                'title' => '',
                'start_ts' => 1567503900,
                'end_ts' => 1567504800,
                'start' => '2019-09-03T11:45:00',
                'end' => '2019-09-03T12:00:00',
            ],[
                 'title' => '',
                 'start_ts' => 1567585800,
                 'end_ts' => 1567589400,
                 'start' => '2019-09-04T10:30:00',
                 'end' => '2019-09-04T11:30:00'
            ],[
                'title' => '',
                'start_ts' => 1569016800,
                'end_ts' => 1569448800,
                'start' => '2019-09-21T00:00:00',
                'end' => '2019-09-26T00:00:00',
                'allDay' => true
            ]
        ];

        $this->assertEquals($events, $eventsExpected);
    }

    /**
     * Test parsing an ICS content
     */
    public function testParseICS() {
        $content = <<<IFB
BEGIN:VCALENDAR
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-TIMEZONE:Europe/Paris
BEGIN:VTIMEZONE
TZID:Europe/Paris
X-LIC-LOCATION:Europe/Paris
BEGIN:DAYLIGHT
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
TZNAME:CEST
DTSTART:19700329T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
TZNAME:CET
DTSTART:19701025T030000
RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU
END:STANDARD
END:VTIMEZONE
BEGIN:VEVENT
DTSTART:20190824T081500Z
DTEND:20190824T091500Z
DTSTAMP:20190926T135949Z
UID:64rm6ph
CREATED:20190818T120055Z
DESCRIPTION:
STATUS:TENTATIVE
SUMMARY:Test avec caractères UTF-8 ôéè@#
TRANSP:OPAQUE
BEGIN:VALARM
ACTION:DISPLAY
DESCRIPTION:This is an event reminder
TRIGGER:-P0DT0H10M0S
END:VALARM
END:VEVENT
BEGIN:VEVENT
DTSTART:20200509T060000Z
DTEND:20200509T070000Z
DTSTAMP:20190926T135949Z
UID:6gp3ip9
CREATED:20190726T185803Z
DESCRIPTION:
LAST-MODIFIED:20190726T185803Z
LOCATION:
SEQUENCE:1
STATUS:TENTATIVE
TRANSP:OPAQUE
BEGIN:VALARM
ACTION:DISPLAY
DESCRIPTION:This is an event reminder
TRIGGER:-P0DT0H10M0S
END:VALARM
END:VEVENT
BEGIN:VEVENT
DTSTART;VALUE=DATE:20190729
DTEND;VALUE=DATE:20190817
DTSTAMP:20190926T135949Z
UID:75j68d
CREATED:20190611T135546Z
DESCRIPTION:
LAST-MODIFIED:20190611T135546Z
LOCATION:
SEQUENCE:1
STATUS:TENTATIVE
SUMMARY:Long range of days
TRANSP:OPAQUE
BEGIN:VALARM
ACTION:DISPLAY
DESCRIPTION:This is an event reminder
TRIGGER:-P0DT7H0M0S
END:VALARM
END:VEVENT
BEGIN:VEVENT
DTSTART;VALUE=DATE:20190729
DURATION:P1D
DTSTAMP:20190926T135949Z
UID:75j68d
CREATED:20190611T135546Z
DESCRIPTION:
LAST-MODIFIED:20190611T135546Z
LOCATION:
SEQUENCE:1
STATUS:TENTATIVE
SUMMARY:Event with start and duration
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART;VALUE=DATE:20191229
DTSTAMP:20190926T135949Z
UID:75j68d
CREATED:20190611T135546Z
DESCRIPTION:
LAST-MODIFIED:20190611T135546Z
LOCATION:
SEQUENCE:1
STATUS:TENTATIVE
SUMMARY:Event with date start and no end nor duration
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200509T060000Z
DTSTAMP:20190926T135949Z
UID:75j68d
CREATED:20190611T135546Z
DESCRIPTION:
LAST-MODIFIED:20190611T135546Z
LOCATION:
SEQUENCE:1
STATUS:TENTATIVE
SUMMARY:Event with datetime start and no end nor duration
TRANSP:OPAQUE
END:VEVENT
END:VCALENDAR
IFB;

        $events = CalendarUtil::parse($content);
        $this->assertEquals(count($events), 6);

        $eventsExpected = [
            [
                'uid' => '64rm6ph',
                'title' => 'Test avec caractères UTF-8 ôéè@#',
                'start_ts' => 1566634500,
                'end_ts' => 1566638100,
                'start' => '2019-08-24T10:15:00',
                'end' => '2019-08-24T11:15:00'
            ],[
                'uid' => '6gp3ip9',
                'title' => '', //No title event
                'start_ts' => 1589004000,
                'end_ts' => 1589007600,
                'start' => '2020-05-09T08:00:00',
                'end' => '2020-05-09T09:00:00',
            ],[
                'uid' => '75j68d',
                'title' => 'Long range of days',
                'start_ts' => 1564351200,
                'end_ts' => 1565992800,
                'start' => '2019-07-29T00:00:00',
                'end' => '2019-08-17T00:00:00',
                'allDay' => true
            ],[
                'uid' => '75j68d',
                'title' => 'Event with start and duration',
                'start_ts' => 1564351200,
                'end_ts' => 1564437600,
                'start' => '2019-07-29T00:00:00',
                'end' => '2019-07-30T00:00:00',
                'allDay' => true
            ],[
                'uid' => '75j68d',
                'title' => 'Event with date start and no end nor duration',
                'start_ts' => 1577574000,
                'end_ts' => 1577660400,
                'start' => '2019-12-29T00:00:00',
                'end' => '2019-12-30T00:00:00',
                'allDay' => true
            ],[
                'uid' => '75j68d',
                'title' => 'Event with datetime start and no end nor duration',
                'start_ts' => 1589004000,
                'end_ts' => 1589004000,
                'start' => '2020-05-09T08:00:00',
                'end' => '2020-05-09T08:00:00'
            ]
        ];

        $this->assertEquals($eventsExpected, $events);
    }

    /**
     * Test get color
     */
    public function testGetColor() {

        $expectedColors = [
            '#6b79b8',
            '#6b79b8',
            '#6b79b8'
        ];
        $actualColors = array();

        for($i = 0; $i < count($expectedColors); $i++) {
            $actualColors[] = CalendarUtil::getColor();
        }
        $this->assertEquals($expectedColors, $actualColors);
    }
}
