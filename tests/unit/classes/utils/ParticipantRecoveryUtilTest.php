<?php
/**
 *     Moment - MomentApplicationMailTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use AspectMock\Test as mock;

/**
 * Class MomentApplicationMailTest
 *
 * Tests Moment Application Mail class
 */
class ParticipantRecoveryUtilTest extends \Codeception\Test\Unit {
    /**
     * Prepare context before each test
     *
     * @throws Exception
     */
    protected function _before() {
        MomentTestUtil::startUp();
        MomentTestUtil::authenticate();
        MomentTestUtil::initSurvey();

        //Mocking 'fromQuestion' for proposition types
        mock::double('DateProposition', ['fromQuestion' => function(Question $question) { return MomentTestUtil::getRelated($question, 'DateProposition'); }]);
        mock::double('TextProposition', ['fromQuestion' => function(Question $question) { return MomentTestUtil::getRelated($question, 'TextProposition'); }]);

        mock::double('Participant', ['save' => true]);
    }

    /**
     * Clean after every test
     */
    protected function _after() {
        mock::clean();
    }

    public function testSendLinkToParticipant() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        mock::double('Participant', ['fromSurveyAndEmail' => $participant]);

        $mock = mock::double('MomentApplicationMail', [
            'prepareParticipantRecoveryMail' => function($participant, $survey, $email) use (&$storedContent) {
                return __AM_CONTINUE__;
            }
        ]);

        $this->assertTrue(ParticipantRecoveryUtil::sendLinkToParticipant($survey, 'participant_email'));

        $mock->verifyInvoked('prepareParticipantRecoveryMail', [$participant]);
    }

    public function testSendLinkToParticipantNotFound() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        mock::double('Participant', ['fromSurveyAndEmail' => function() use($survey) {
            throw new ParticipantNotFoundException($survey);
        }]);

        $mock = mock::double('MomentApplicationMail', [
            'prepareParticipantRecoveryMail' => function($participant, $survey, $email) use (&$storedContent) {
                return __AM_CONTINUE__;
            }
        ]);

        $this->assertTrue(ParticipantRecoveryUtil::sendLinkToParticipant($survey, 'participant_email'));

        $mock->verifyInvoked('prepareParticipantRecoveryMail', [null, $survey, 'participant_email']);
    }

    public function testGenerateCodeForParticipant() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        $storedContent = array();
        $mock = mock::double('FileIO', [
            'writeWhole' => function($filename, $content) use (&$storedContent) {
                $storedContent = unserialize($content);
            },
            'readWhole' => function($filename) use (&$storedContent) {
                return serialize($storedContent);
            }
        ]);

        ParticipantRecoveryUtil::sendCodeForParticipant($participant);

        $mock->verifyInvoked('writeWhole');

        $firstCode = $storedContent['code'];
        $firstTries = $storedContent['tries'];

        $this->assertEquals(ParticipantRecoveryUtil::RECOVERY_CODE_LENGTH, strlen($firstCode));
        $this->assertEquals(0, $firstTries);

        //Try to regenerate code must be regenerated
        ParticipantRecoveryUtil::sendCodeForParticipant($participant);

        $this->assertNotEquals($firstCode, $storedContent['code']);
    }

    public function testIsCodeValid() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        $storedContent = ['code' => '123456', 'tries' => 0];
        $mock = mock::double('FileIO', [
            'writeWhole' => function($filename, $content) use (&$storedContent) {
                $storedContent = unserialize($content);
            },
            'readWhole' => function($filename) use (&$storedContent) {
                return serialize($storedContent);
            },
            'delete' => function($filename) use (&$storedContent) {
                $storedContent = null;
            }
        ]);

        //Here file with code should exist
        $prumock = mock::double('ParticipantRecoveryUtil', [
            'fileExists' => true,
            'fileIsValid' => true,
            'revocateCode' => function() {
                return __AM_CONTINUE__;
            }
        ]);

        //Try a bad code
        $this->assertFalse(ParticipantRecoveryUtil::isCodeValid($participant, '654321'));
        $this->assertEquals(1, $storedContent['tries']);

        //Try a bad code
        $this->assertFalse(ParticipantRecoveryUtil::isCodeValid($participant, '654321'));
        $this->assertEquals(2, $storedContent['tries']);

        $this->assertEquals(1, ParticipantRecoveryUtil::getTriesLeft($participant));

        //Try a good code
        $this->assertTrue(ParticipantRecoveryUtil::isCodeValid($participant, '123456'));

        //Try a good code a second time => it should have been revocated
        $prumock->verifyInvoked('revocateCode');
        $this->assertFalse(ParticipantRecoveryUtil::isCodeValid($participant, '123456'));

        //No more tries left (code is revocated)
        $this->assertEquals(0, ParticipantRecoveryUtil::getTriesLeft($participant));

        mock::clean();
    }

    public function testHasStoredCodeWithoutCode() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        mock::double('ParticipantRecoveryUtil', [
            'getStoredCode' => function() {
                return null;
            }
        ]);

        $this->assertFalse(ParticipantRecoveryUtil::hasStoredCode($participant));
    }

    public function testHasStoredCodeWithCode() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        mock::double('ParticipantRecoveryUtil', [
            'getStoredCode' => function() {
                return 'this_is_a_code';
            }
        ]);

        $this->assertTrue(ParticipantRecoveryUtil::hasStoredCode($participant));
    }

    public function testHasStoredCodeInvalidFile() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        mock::double('ParticipantRecoveryUtil', [
            'fileExists' => true,
            'fileIsValid' => false
        ]);

        $mock = mock::double('FileIO', [
            'delete' => true
        ]);

        $this->assertFalse(ParticipantRecoveryUtil::hasStoredCode($participant));

        $mock->verifyInvoked('delete');
    }

    public function testFileIOException() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        mock::double('ParticipantRecoveryUtil', [
            'fileExists' => true,
            'fileIsValid' => false
        ]);

        $mock = mock::double('FileIO', [
            'delete' => function($filename) {
                throw new FileIONotFoundException($filename);
            },
            'writeWhole' => function($filename, $content) use (&$storedContent) {
                throw new FileIOCannotWriteFileException($filename);
            },
            'readWhole' => function($filename) use (&$storedContent) {
                throw new FileIOCannotReadFileException($filename);
            },
        ]);

        try {
            $this->assertFalse(ParticipantRecoveryUtil::hasStoredCode($participant));

            $mock->verifyInvoked('delete');
        } catch(FileIONotFoundException $e) {
            $this->fail('ParticipantRecoveryUtil should not throw FileIONotFoundException');
        }

        mock::double('ParticipantRecoveryUtil', [
            'fileExists' => true,
            'fileIsValid' => true
        ]);

        try {
            $this->assertFalse(ParticipantRecoveryUtil::hasStoredCode($participant));

        } catch(FileIOCannotReadFileException $e) {
            $this->fail('ParticipantRecoveryUtil should not throw FileIOCannotReadFileException');
        }

        try {
            $this->assertTrue(ParticipantRecoveryUtil::sendCodeForParticipant($participant));
        } catch(FileIOCannotWriteFileException $e) {
            $this->fail('ParticipantRecoveryUtil should not throw FileIOCannotReadFileException');
        }

    }
    
}
