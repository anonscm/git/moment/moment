<?php
/**
 *     Moment - MomentApplicationMailTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use AspectMock\Test as mock;

/**
 * Class MomentUtilitiesTest
 *
 * Tests Moment Utilities class
 */
class MomentUtilitiesTest extends \Codeception\Test\Unit {

    /**
     * Prepare context before each test
     *
     * @throws Exception
     */
    protected function _before() {
        MomentTestUtil::startUp();
        MomentTestUtil::authenticate();
        MomentTestUtil::initSurvey();
    }

    /**
     * Clean after every test
     */
    protected function _after() {
        mock::clean();
    }

    /**
     * Test get Page title
     *
     * @throws AnswerMissingPropertyException
     * @throws GuestInvalidEmailException
     * @throws GuestSurveyNotSavedException
     * @throws MailNoAddressesFoundException
     * @throws ParticipantNotFoundException
     */
    public function testGetPageTitle() {
        global $_SERVER;

        mock::double('Config', ['get' => function($property) {
            switch ($property) {
                case 'application_name':
                    return 'Moment';
            }
            return __AM_CONTINUE__;
        }]);

        mock::double('Lang', ['tr' => function($property) {
            return $property.'_tr';
        }]);

        mock::double('Auth', ['isAdmin' => true]);

        mock::double('MomentUtilities', ['getSurveyPageTitle' => 'survey_page_title - Moment']);


        $this->checkPageTitle('user_guide');
        $this->checkPageTitle('legal', 'legal_notice');
        $this->checkPageTitle('faq', 'faq_title');
        $this->checkPageTitle('admin', 'administration_actions');
        $this->checkPageTitle('user', 'my_profile');
        $this->checkPageTitle('accessibility');

        $_SERVER['PATH_INFO'] = '/survey';
        $this->assertEquals('survey_page_title - Moment', MomentUtilities::getPageTitle());
    }

    /**
     * Test survey closed email composition
     *
     * @throws AnswerMissingPropertyException
     * @throws GuestInvalidEmailException
     * @throws GuestSurveyNotSavedException
     * @throws MailNoAddressesFoundException
     * @throws ParticipantNotFoundException
     */
    public function testGetSurveyPageTitle() {
        global $_SERVER;

        mock::double('Config', ['get' => function($property) {
            switch ($property) {
                case 'application_name':
                    return 'Moment';
            }
            return __AM_CONTINUE__;
        }]);

        mock::double('Lang', ['tr' => function($property) {
            return $property.'_tr';
        }]);

        mock::double('Auth', ['isAdmin' => true]);

        $survey = Survey::fromId('survey1');

        mock::double('Survey', ['fromHumanReadableId' => $survey]);
        mock::double('SurveyUserUtil', ['getCurrentUser' => ['role' => SurveyUserRoles::OWNER]]);

        $_SERVER['PATH_INFO'] = '/survey/create';
        $this->assertEquals('survey_create_tr - Moment', MomentUtilities::getPageTitle());

        $_SERVER['PATH_INFO'] = '/survey/manage';
        $this->assertEquals('manage_surveys_tr - Moment', MomentUtilities::getPageTitle());

        $_SERVER['PATH_INFO'] = '/survey/update/xxxxxx';
        $this->assertEquals('updating_survey_tr - '.$survey->title.' - Moment', MomentUtilities::getPageTitle());

        $_SERVER['PATH_INFO'] = '/survey/results/xxxxxx';
        $this->assertEquals('results_of_survey_tr - '.$survey->title.' - Moment', MomentUtilities::getPageTitle());

        $_SERVER['PATH_INFO'] = '/survey/xxxxxx';
        $this->assertEquals('reply_to_survey_tr - '.$survey->title.' - Moment', MomentUtilities::getPageTitle());


        //Access denied
        mock::double('SurveyUserUtil', ['getCurrentUser' => ['role' => SurveyUserRoles::NONE]]);

        $_SERVER['PATH_INFO'] = '/survey/xxxxxx';
        $this->assertEquals('access_denied_tr - Moment', MomentUtilities::getPageTitle());

        //Survey not found => access denied

        mock::double('Survey', ['fromHumanReadableId' => function() {
            throw new NotFoundException('survey', 'not_found_id');
        }]);

        $_SERVER['PATH_INFO'] = '/survey/xxxxxx';
        $this->assertEquals('access_denied_tr - Moment', MomentUtilities::getPageTitle());


        mock::double('Survey', ['fromHumanReadableId' => function() {
            throw new SurveyNotFoundException(['id' => 'not_found_id']);
        }]);

        $_SERVER['PATH_INFO'] = '/survey/xxxxxx';
        $this->assertEquals('access_denied_tr - Moment', MomentUtilities::getPageTitle());

    }

    public function testAccessControlNonExistingPage() {

        $_SERVER['PATH_INFO'] = '/a-non-existing-page';

        $this->expectException('MomentPageNotFoundException');

        MomentUtilities::accessControl();

    }

    public function testAccessControlUserPageUnauthenticated() {
        mock::double('Auth', ['isAuthenticated' => false]);
        $_SERVER['PATH_INFO'] = '/user';

        $this->expectException('MomentPleaseLoginException');

        MomentUtilities::accessControl();
    }

    public function testAccessControlUserPageAuthenticated() {
        mock::double('Auth', ['isAuthenticated' => true]);

        $_SERVER['PATH_INFO'] = '/user';

        $this->assertTrue(MomentUtilities::accessControl());
    }

    public function testAccessControlAdminPageNotAdmin() {
        mock::double('Auth', ['isAdmin' => false]);
        $_SERVER['PATH_INFO'] = '/admin';

        $this->expectException('MomentForbiddenException');

        MomentUtilities::accessControl();
    }

    public function testAccessControlAdminPageAsAdmin() {
        mock::double('Auth', ['isAdmin' => true]);

        $_SERVER['PATH_INFO'] = '/admin';

        $this->assertTrue(MomentUtilities::accessControl());
    }


    public function testAccessControlNoPage() {
        $_SERVER['PATH_INFO'] = null;

        $this->assertTrue(MomentUtilities::accessControl());
    }

    public function testGetMomentsOfDayInfos() {
        mock::double('Lang', ['tr' => function($property) {
            if($property === 'input_mode_info_moments_row') {
                return new Translation('moment of the day {name} {bt}-{et}');
            }
            return new Translation($property.'_tr');
        }]);

        $this->assertEquals(
            '<ul><li>moment of the day morning_tr 08:00-12:00</li><li>moment of the day noon_tr 12:00-14:00</li><li>moment of the day afternoon_tr 14:00-18:00</li><li>moment of the day evening_tr 18:00-22:00</li></ul>', MomentUtilities::getMomentsOfDayInfos([
            'morning'   => ['bt' => '08:00', 'et' => '12:00'],
            'noon'      => ['bt' => '12:00', 'et' => '14:00'],
            'afternoon' => ['bt' => '14:00', 'et' => '18:00'],
            'evening'   => ['bt' => '18:00', 'et' => '22:00']
        ]));
    }

    public function testIncludePageSpecificScripts() {

        ob_start();
        MomentUtilities::includePageSpecificScripts();
        $ct = ob_get_clean();

        $this->assertEmpty($ct);

        Event::register(Event::AFTER, 'page_js_libraries', function (Event $event) {
            $event->result[] = 'test.js';
        });
        ob_start();
        MomentUtilities::includePageSpecificScripts();
        $ct = ob_get_clean();

        //Some part of script tag are too dynamic to be checked
        $this->assertStringContainsString('<script type="text/javascript"', $ct);
        $this->assertStringContainsString('test.js', $ct);
    }

    public function testIncludePageSpecificStylesheets() {

        ob_start();
        MomentUtilities::includePageSpecificStylesheets();
        $ct = ob_get_clean();

        $this->assertEmpty($ct);

        Event::register(Event::AFTER, 'page_css_libraries', function (Event $event) {
            $event->result[] = 'test.css';
        });
        ob_start();
        MomentUtilities::includePageSpecificStylesheets();
        $ct = ob_get_clean();

        //Some part of script tag are too dynamic to be checked
        $this->assertStringContainsString('<link type="text/css" rel="stylesheet"', $ct);
        $this->assertStringContainsString('test.css', $ct);
    }

    private function checkPageTitle($page, $expected = null) {
        $_SERVER['PATH_INFO'] = '/'.$page;

        if($expected !== null) {
            $title = $expected;
        } else {
            $title = $page;
        }

        $this->assertEquals($title . '_tr - Moment', MomentUtilities::getPageTitle());

    }

}
