<?php
/**
 *     Moment - CalendarManagerTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// include_once __DIR__ . '/../../src/includes/core/init.php'

use \AspectMock\Test as mock;

class CalendarManagerTest extends \Codeception\Test\Unit {

    /**
     * @var string an ifb content
     */
    private $ifbContent = <<<IFB
BEGIN:VCALENDAR
BEGIN:VFREEBUSY
DTSTAMP:20191001T112451Z
DTSTART:20190831T112451Z
DTEND:20191202T112451Z
FREEBUSY;FBTYPE=BUSY:20190902T094500Z/20190902T100000Z
FREEBUSY;FBTYPE=BUSY:20190903T094500Z/20190903T100000Z
FREEBUSY;FBTYPE=BUSY:20190904T083000Z/20190904T093000Z
FREEBUSY;FBTYPE=BUSY:20190920T220000Z/20190925T220000Z
END:VFREEBUSY
END:VCALENDAR
IFB;

    protected function _before() {
        MomentTestUtil::startUp();
        MomentTestUtil::authenticate();

        //Following var is required to process
        $_SERVER['HTTP_USER_AGENT'] = 'Test';

        //Mocking HTTPClient to answer what we need to test
        mock::double('HTTPClient',
            [
                'setOption' => true,
                'execute' => $this->ifbContent,
                'getErrNo' => false,
                'getInfo' => function($name) {
                    switch($name) {
                        case CURLINFO_HTTP_CODE:
                            return 200;
                        case CURLINFO_CONTENT_TYPE:
                            return 'text/calendar';
                    }
                }
            ]);

    }

    protected function _after() {
        mock::clean();
    }

    /**
     * testGetEvents check getEvents retrieves events in the correct way
     */
    public function testGetEvents() {
        mock::double('CalendarManager', ['loadUrl' => $this->ifbContent]);

        $events = CalendarManager::getEvents('https://url.de.test');

        $this->assertEquals(count($events), 4);

        $eventsExpected = [
            [
                'title' => '',
                'start_ts' => 1567417500,
                'end_ts' => 1567418400,
                'start' => '2019-09-02T11:45:00',
                'end' => '2019-09-02T12:00:00',
            ],[
                'title' => '',
                'start_ts' => 1567503900,
                'end_ts' => 1567504800,
                'start' => '2019-09-03T11:45:00',
                'end' => '2019-09-03T12:00:00',
            ],[
                'title' => '',
                'start_ts' => 1567585800,
                'end_ts' => 1567589400,
                'start' => '2019-09-04T10:30:00',
                'end' => '2019-09-04T11:30:00'
            ],[
                'title' => '',
                'start_ts' => 1569016800,
                'end_ts' => 1569448800,
                'start' => '2019-09-21T00:00:00',
                'end' => '2019-09-26T00:00:00',
                'allDay' => true
            ]
        ];

        $this->assertEquals($events, $eventsExpected);
    }

    /**
     * testGetEventsTwice to check caching usage
     */
    public function testGetEventsTwice() {
        mock::double('CalendarManager', ['loadUrl' => $this->ifbContent]);

        $events1 = CalendarManager::getEvents('https://url.de.test');
        $events2 = CalendarManager::getEvents('https://url.de.test');

        $this->assertEquals($events1, $events2);
    }

    /**
     * testGetMyCalEvents to check
     */
    public function testGetMyCalEvents() {
        mock::double('MyCalUtil', ['getIcsFromUser' => $this->ifbContent]);

        $eventsExpected = [
            [
                'title' => '',
                'start_ts' => 1567417500,
                'end_ts' => 1567418400,
                'start' => '2019-09-02T11:45:00',
                'end' => '2019-09-02T12:00:00',
            ],[
                'title' => '',
                'start_ts' => 1567503900,
                'end_ts' => 1567504800,
                'start' => '2019-09-03T11:45:00',
                'end' => '2019-09-03T12:00:00',
            ],[
                'title' => '',
                'start_ts' => 1567585800,
                'end_ts' => 1567589400,
                'start' => '2019-09-04T10:30:00',
                'end' => '2019-09-04T11:30:00'
            ],[
                'title' => '',
                'start_ts' => 1569016800,
                'end_ts' => 1569448800,
                'start' => '2019-09-21T00:00:00',
                'end' => '2019-09-26T00:00:00',
                'allDay' => true
            ]
        ];

        $events = CalendarManager::getMyCalEvents();

        $this->assertEquals($events, $eventsExpected);
    }

    /**
     * testGetMyCalEventsTwice to check cache
     */
    public function testGetMyCalEventsTwice() {
        mock::double('MyCalUtil', ['getIcsFromUser' => $this->ifbContent]);

        $events1 = CalendarManager::getMyCalEvents();
        $events2 = CalendarManager::getMyCalEvents();

        $this->assertEquals($events1, $events2);
    }


    /**
     * testLoadUrl test CalendarManager::LoadUrl that must retrieve content of resources url
     */
    public function testLoadUrl() {
        mock::double('CalendarManager',
            [
                'storeURLStatus' => true
            ]);

        $this->assertEquals(CalendarManager::loadUrl('https://url.de.test'), $this->ifbContent);
    }

    /**
     * testLoadUrlError test CalendarManager::LoadUrl when url is in error it must retrieve an empty string
     */
    public function testLoadUrlError() {

        mock::double('CalendarManager',
            [
                'storeURLStatus' => true,
                'getStatus' => array('code' => CalendarStatusCodes::ERROR)
            ]);
        //Clearing cache
        $this->make('CalendarManager', ['cacheContent' => array()]);

        $this->assertEquals(CalendarManager::loadUrl('https://url.de.test'), '');
    }

    /**
     * testGetStatusError if url answer is 404 status must be CalendarStatusCodes::ERROR
     */
    public function testGetStatusError() {
        $cm = mock::double('CalendarManager',
            [
                'storeURLStatus' => true
            ]);

        $this->mockHTTPClient(false, '', function($name) {
            switch($name) {
                case CURLINFO_HTTP_CODE:
                    return 404;
                case CURLINFO_CONTENT_TYPE:
                    return 'text/calendar'; //An incompatible type
            }
        });

        CalendarManager::checkUrl('https://url.de.test');

        $this->assertStatusConformity($cm, [
            'code' => CalendarStatusCodes::ERROR,
            'message' => CalendarStatusMessages::NOT_FOUND,
            'details' => [
                'code' => 404
            ]
        ]);

    }

    /**
     * testGetStatusErrorType if url answer mimt type is not compatible status must be CalendarStatusCodes::INCOMPATIBLE
     */
    public function testGetStatusErrorType() {
        $cm = mock::double('CalendarManager',
            [
                'storeURLStatus' => true
            ]);

        $this->mockHTTPClient(false, '', function($name) {
            switch($name) {
                case CURLINFO_HTTP_CODE:
                    return 200;
                case CURLINFO_CONTENT_TYPE:
                    return 'text/html'; //An incompatible type
            }
        });

        CalendarManager::checkUrl('https://url.de.test');

        $this->assertStatusConformity($cm, [
            'code' => CalendarStatusCodes::INCOMPATIBLE,
            'message' => CalendarStatusMessages::INCOMPATIBLE
        ]);

    }

    /**
     * testGetStatusErrorLoad if url loading failed status must be CalendarStatusCodes::ERROR
     */
    public function testGetStatusErrorLoad() {
        $cm = mock::double('CalendarManager',
            [
                'storeURLStatus' => true
            ]);

        $this->mockHTTPClient(-1, 'error', 500);

        CalendarManager::checkUrl('https://url.de.test');

        $this->assertStatusConformity($cm, [
            'code' => CalendarStatusCodes::ERROR,
            'message' => CalendarStatusMessages::ERROR,
            'details' => [
                'code' => -1,
                'msg' => 'error'
            ]
        ]);

    }

    /**
     * testGetStatusDown if url loading failed status must be CalendarStatusCodes::DOWN
     */
    public function testGetStatusDown() {
        $cm = mock::double('CalendarManager',
            [
                'storeURLStatus' => true
            ]);

        $this->mockHTTPClient(CURLE_OPERATION_TIMEOUTED, 'timeout', 500);

        CalendarManager::checkUrl('https://url.de.test');

        $this->assertStatusConformity($cm, [
            'code' => CalendarStatusCodes::DOWN,
            'message' => CalendarStatusMessages::DOWN,
            'details' => [
                'code' => CURLE_OPERATION_TIMEOUTED,
                'msg' => 'timeout'
            ]
        ]);

    }

    /**
     * testGetStatusOversized if url loading because file is too big status must be CalendarStatusCodes::OVERSIZED
     */
    public function testGetStatusOversized() {
        $cm = mock::double('CalendarManager',
            [
                'storeURLStatus' => true
            ]);

        //If file is too big loading is aorted by a callback
        $this->mockHTTPClient(CURLE_ABORTED_BY_CALLBACK, 'oversized', 500);

        CalendarManager::checkUrl('https://url.de.test');

        $this->assertStatusConformity($cm, [
            'code' => CalendarStatusCodes::OVERSIZED,
            'message' => CalendarStatusMessages::OVERSIZED,
            'details' => [
                'code' => CURLE_ABORTED_BY_CALLBACK,
                'msg' => 'oversized'
            ]
        ]);

    }

    /**
     * testGetStatusErrorLoad if url loading failed because of a 404 status must be CalendarStatusMessages::NOT_FOUND
     */
    public function testGetStatusNotFoundLoad() {
        $cm = mock::double('CalendarManager',
            [
                'storeURLStatus' => true
            ]);

        $this->mockHTTPClient(false, 'timeout', 404);

        CalendarManager::checkUrl('https://url.de.test');

        $this->assertStatusConformity($cm, [
            'code' => CalendarStatusCodes::ERROR,
            'message' => CalendarStatusMessages::NOT_FOUND,
            'details' => [
                'code' => 404
            ]
        ]);

    }

    /**
     * testGetStatusForbiddenLoad if url loading failed because of a 403 status must be CalendarStatusMessages::FORBIDDEN
     */
    public function testGetStatusForbiddenLoad() {
        $cm = mock::double('CalendarManager',
            [
                'storeURLStatus' => true
            ]);

        $this->mockHTTPClient(false, 'forbidden', 403);

        CalendarManager::checkUrl('https://url.de.test');

        $this->assertStatusConformity($cm, [
            'code' => CalendarStatusCodes::ERROR,
            'message' => CalendarStatusMessages::FORBIDDEN,
            'details' => [
                'code' => 403
            ]
        ]);

    }

    /**
     * testGetStatusUnauthorizedLoad if url loading failed because of a 401 status must be CalendarStatusMessages::FORBIDDEN
     */
    public function testGetStatusUnauthorizedLoad() {
        $cm = mock::double('CalendarManager',
            [
                'storeURLStatus' => true
            ]);

        $this->mockHTTPClient(false, 'unauthorized', 401);

        CalendarManager::checkUrl('https://url.de.test');

        $this->assertStatusConformity($cm, [
            'code' => CalendarStatusCodes::ERROR,
            'message' => CalendarStatusMessages::FORBIDDEN,
            'details' => [
                'code' => 401
            ]
        ]);

    }

    /**
     * testGetStatusUnauthorizedLoad if url loading failed because of a 500 status must be CalendarStatusMessages::FORBIDDEN
     */
    public function testGetStatusErrorLoading() {
        $cm = mock::double('CalendarManager',
            [
                'storeURLStatus' => true
            ]);

        $this->mockHTTPClient(false, 'error', 500);

        CalendarManager::checkUrl('https://url.de.test');

        $this->assertStatusConformity($cm, [
            'code' => CalendarStatusCodes::ERROR,
            'message' => CalendarStatusMessages::ERROR,
            'details' => [
                'code' => 500
            ]
        ]);

    }

    /**
     * testGetStatusDown if url loading times out status must be CalendarStatusCodes::DOWN
     */
    public function testLoadDown() {
        $cm = mock::double('CalendarManager',
            [
                'storeURLStatus' => true
            ]);

        $this->mockHTTPClient(CURLE_OPERATION_TIMEOUTED, 'timeout', 404);

        CalendarManager::loadUrl('https://url.de.test');

        $this->assertStatusConformity($cm, [
            'code' => CalendarStatusCodes::DOWN,
            'message' => CalendarStatusMessages::DOWN,
            'details' =>
                [
                    'code' => CURLE_OPERATION_TIMEOUTED,
                    'msg' => 'timeout'
                ]
            ]);

    }

    /**
     * testStoreURLStatus
     */
    public function testStoreURLStatus() {

        // Mocking Entity management
        mock::double('Calendar', ['refreshStatus' => false]);
        $cal = Calendar::create(User::create('toto@renater.fr'),
            [
                'url' => 'https://url.de.test',
                'name' => 'Test Calendar Name',
                'settings' => [
                    'color' => '#FFF'
                ],
                'status' => ['code' => CalendarStatusCodes::ERROR],
                'checked' => null
            ]
        );
        mock::double('Entity', ['all' => [$cal], 'save' => false]);

        //Storing calls
        $cm = mock::double('CalendarManager');

        CalendarManager::checkUrl('https://url.de.test');

        $this->assertStatusConformity($cm, [
            'code' => CalendarStatusCodes::OK,
            'message' => CalendarStatusMessages::OK
        ]);
    }

    /**
     * Checks status conformity from a Proxyfied CalendarManager class
     * @param \AspectMock\Proxy\ClassProxy $cm Proxyfied CalendarManager object
     * @param array $expected expected status
     */
    private function assertStatusConformity(AspectMock\Proxy\ClassProxy $cm, $expected) {
        $calls = $cm->getCallsForMethod('storeURLStatus');
        foreach($calls as $call) {
            //$url is the first arg $status the second
            if('https://url.de.test' == $call[0]) {
                $this->assertEquals($expected, $call[1]);
            }
        }
    }

    /**
     * @param bool $errNo
     * @param string $error
     * @param bool $infos
     */
    private function mockHTTPClient($errNo = false, $error = '',  $infos = true) {
        mock::double('HTTPClient',
            [
                'getErrNo' => $errNo,
                'getError' => $error,
                'getInfo' => $infos
            ]);
    }
}
