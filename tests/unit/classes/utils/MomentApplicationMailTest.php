<?php
/**
 *     Moment - MomentApplicationMailTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use AspectMock\Test as mock;

/**
 * Class MomentApplicationMailTest
 *
 * Tests Moment Application Mail class
 */
class MomentApplicationMailTest extends \Codeception\Test\Unit {
    /**
     * Prepare context before each test
     *
     * @throws Exception
     */
    protected function _before() {
        MomentTestUtil::startUp();
        MomentTestUtil::authenticate();
        MomentTestUtil::initSurvey();

        //Mocking 'fromQuestion' for proposition types
        mock::double('DateProposition', ['fromQuestion' => function(Question $question) { return MomentTestUtil::getRelated($question, 'DateProposition'); }]);
        mock::double('TextProposition', ['fromQuestion' => function(Question $question) { return MomentTestUtil::getRelated($question, 'TextProposition'); }]);

        mock::double('Participant', ['save' => true]);
    }

    /**
     * Clean after every test
     */
    protected function _after() {
        mock::clean();
    }

    /**
     * Test get
     */
    public function test__get() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        mock::double('Lang', ['translateEmail' => new Translation('')]);

        $mail = MomentApplicationMail::prepareNewReplierEmail($survey, $participant);

        $this->expectException('PropertyAccessException');
        $mail->__get('not_an_existing_property');
    }

    /**
     * Test survey closed email composition
     *
     * @throws AnswerMissingPropertyException
     * @throws GuestInvalidEmailException
     * @throws GuestSurveyNotSavedException
     * @throws MailNoAddressesFoundException
     * @throws ParticipantNotFoundException
     */
    public function testPrepareClosedEmail() {
        $survey = Survey::fromId('survey1');
        mock::double('Owner', ['fromSurvey' => array(Owner::create($survey, Auth::user()))]);

        /** @var MailAttachment[] $attachments */
        $attachments = [];
        $mock = mock::double('Mail', [
            'attach' => function($attachment) use(&$attachments) {
                $attachments[] = $attachment;
            }
        ]);

        // test when not owner
        MomentApplicationMail::prepareClosedEmail($survey, '', 'dummy@domain.tld');
        $this->assertEquals(0, count($attachments));

        // test when owner
        /** @var Owner $owner */
        MomentApplicationMail::prepareClosedEmail($survey, '', Auth::user()->email);
        $this->assertEquals(1, count($attachments));
        $this->assertStringEndsWith('.csv', $attachments[0]->name);
        $attachments = [];

        // Set final answer
        $participant = Participant::getUltimateParticipant($survey, true);
        mock::double('Survey', [
            '__get' => function($property) use($participant) {
                if($property === 'ultimate_participant')
                    return $participant;

                return __AM_CONTINUE__;
            }
        ]);

        foreach($survey->Questions as $question) {
            $choices = [];
            foreach($question->propositions as $proposition) {
                $choices[] = (object)[
                    'proposition_id' => $proposition->id,
                    'value' => count($choices) ? YesNoMaybeValues::NO : YesNoMaybeValues::YES,
                ];
            }

            Answer::create($participant, $question, ['choices' => $choices]);
        }

        // test when not owner
        MomentApplicationMail::prepareClosedEmail($survey, 'reason', 'dummy@domain.tld');
        $this->assertEquals(1, count($attachments));
        $this->assertEquals('invite.ics', $attachments[0]->name);
        $attachments = [];

        // test when owner
        /** @var Owner $owner */
        MomentApplicationMail::prepareClosedEmail($survey, '', Auth::user()->email);
        $this->assertEquals(2, count($attachments));
        $this->assertStringEndsWith('.csv', $attachments[0]->name);
        $this->assertEquals('invite.ics', $attachments[1]->name);
        $attachments = [];
    }

    /**
     * Test finalanswer selected email composition
     *
     * @throws AnswerMissingPropertyException
     * @throws GuestInvalidEmailException
     * @throws GuestSurveyNotSavedException
     * @throws MailNoAddressesFoundException
     * @throws ParticipantNotFoundException
     */
    public function testPrepareFinalAnswersSelectedEmail() {
        $survey = Survey::fromId('survey1');
        mock::double('Owner', ['fromSurvey' => array(Owner::create($survey, Auth::user()))]);

        /** @var MailAttachment[] $attachments */
        $attachments = [];
        $mock = mock::double('Mail', [
            'attach' => function($attachment) use(&$attachments) {
                $attachments[] = $attachment;
            }
        ]);

        // test when not owner
        MomentApplicationMail::prepareFinalAnswersSelectedEmail($survey, 'dummy@domain.tld');
        $this->assertEquals(0, count($attachments));

        // test when owner
        /** @var Owner $owner */
        MomentApplicationMail::prepareFinalAnswersSelectedEmail($survey, Auth::user()->email);
        $this->assertEquals(1, count($attachments));
        $this->assertStringEndsWith('.csv', $attachments[0]->name);
        $attachments = [];

        // Set final answer
        $participant = Participant::getUltimateParticipant($survey, true);
        mock::double('Survey', [
            '__get' => function($property) use($participant) {
                if($property === 'ultimate_participant')
                    return $participant;

                return __AM_CONTINUE__;
            }
        ]);

        foreach($survey->Questions as $question) {
            $choices = [];
            foreach($question->propositions as $proposition) {
                $choices[] = (object)[
                    'proposition_id' => $proposition->id,
                    'value' => count($choices) ? YesNoMaybeValues::NO : YesNoMaybeValues::YES,
                ];
            }

            Answer::create($participant, $question, ['choices' => $choices]);
        }

        // test when not owner
        MomentApplicationMail::prepareFinalAnswersSelectedEmail($survey, 'dummy@domain.tld');
        $this->assertEquals(1, count($attachments));
        $this->assertEquals('invite.ics', $attachments[0]->name);
        $attachments = [];

        // test when owner
        /** @var Owner $owner */
        MomentApplicationMail::prepareFinalAnswersSelectedEmail($survey, Auth::user()->email);
        $this->assertEquals(2, count($attachments));
        $this->assertStringEndsWith('.csv', $attachments[0]->name);
        $this->assertEquals('invite.ics', $attachments[1]->name);
        $attachments = [];

        $this->expectException('MailNoAddressesFoundException');
        MomentApplicationMail::prepareFinalAnswersSelectedEmail($survey, '');
    }


    /**
     * Test modification email composition
     *
     * @throws GuestInvalidEmailException
     * @throws GuestSurveyNotSavedException
     */
    public function testPrepareModificationEmail() {
        $survey = Survey::fromId('survey1');
        $owner = Owner::create($survey, Auth::user());

        $this->verifyMailTemplate('prepareModificationEmail', 'modification', $owner);
    }

    /**
     * Test modification email composition
     */
    public function testPrepareModificationEmailWithToken() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        $this->verifyMailTemplate('prepareModificationEmail', 'modification', $participant);

        //Note : there may be a way to get Translation::r args to check args passed to the template, but as it's not reals args, not easy to mock
    }

    /**
     * Test closing reminder email composition
     */
    public function testPrepareClosingReminderEmail() {
        $this->verifyMailTemplate('prepareClosingReminderEmail', 'closing.reminder');

        $survey = Survey::fromId('survey1');
        $this->expectException('MailNoAddressesFoundException');
        MomentApplicationMail::prepareClosingReminderEmail($survey, '');
    }

    /**
     * Test closed email composition
     */
    public function testPrepareClosedEmailNoRecipient() {
        $survey = Survey::fromId('survey1');

        $this->expectException('MailNoAddressesFoundException');
        MomentApplicationMail::prepareClosedEmail($survey, 'reason', '');
    }

    /**
     * Test reopened email composition
     */
    public function testPrepareReopenedEmail() {
        $this->verifyMailTemplate('prepareReopenedEmail', 'reopening');

        $survey = Survey::fromId('survey1');

        $this->expectException('MailNoAddressesFoundException');
        MomentApplicationMail::prepareReopenedEmail($survey, '');
    }

    /**
     * Test deletion email composition
     */
    public function testPrepareDeleteEmail() {
        $this->verifyMailTemplate('prepareDeleteEmail', 'deletion');
    }

    /**
     * Test deletion reminder email composition
     */
    public function testPrepareDeletionReminderEmail() {
        $this->verifyMailTemplate('prepareDeletionReminderEmail', 'deletion.reminder');
    }

    /**
     * Test new replier email composition
     */
    public function testPrepareNewReplierEmail() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        $trans = new Translation(array(
            'subject' => new Translation('subject1', true, true), // Raw outputting, will be encoded
            'plain' => new Translation('plain_text', true, true), // Raw outputting
            'html' => new Translation('html_mail', true, false)
        ));

        $lang = mock::double('Lang', ['translateEmail' => $trans]);
        $translation = mock::double('Translation', ['r' => $trans]);

        $mail = MomentApplicationMail::prepareNewReplierEmail($survey, $participant);


        $lang->verifyInvoked('translateEmail', ['new_replier']);
        $translation->verifyInvoked('r');
        $built = $mail->email->build();

        $this->assertEquals(Auth::user()->email, $built['to']);
        $this->assertEquals('subject1', $built['subject']);

        //With cache
        $mail = MomentApplicationMail::prepareNewReplierEmail($survey, $participant);
    }

    /**
     * Test participant deleted email composition
     */
    public function testPrepareParticipantDeletedMail() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        $trans = new Translation(array(
            'subject' => new Translation('subject1', true, true), // Raw outputting, will be encoded
            'plain' => new Translation('plain_text', true, true), // Raw outputting
            'html' => new Translation('html_mail', true, false)
        ));

        $lang = mock::double('Lang', ['translateEmail' => $trans]);
        $translation = mock::double('Translation', ['r' => $trans]);

        $mail = MomentApplicationMail::prepareParticipantDeletedMail($survey, $participant);


        $lang->verifyInvoked('translateEmail', ['participant_deleted']);
        $translation->verifyInvoked('r');
        $built = $mail->email->build();

        $this->assertEquals(Auth::user()->email, $built['to']);
        $this->assertEquals('subject1', $built['subject']);
    }

    /**
     * Test stats email composition
     * @throws MailNoAddressesFoundException
     */
    public function testPrepareStatsEmail() {
        $lang = mock::double('Lang', ['translateEmail' => new Translation(array(
            'subject' => new Translation('subject1', true, true), // Raw outputting, will be encoded
            'plain' => new Translation('plain_text', true, true), // Raw outputting
            'html' => new Translation('html_mail', true, false)
        ))]);

        /** @var MailAttachment[] $attachments */
        $attachments = [];
        $mock = mock::double('Mail', [
            'attach' => function($attachment) use(&$attachments) {
                $attachments[] = $attachment;
            }
        ]);

        $mail = MomentApplicationMail::prepareStatsEmail(array(array(1, 2, 3)), 'stat_recipient', SurveyStats::WEEK, 'start', 'end');

        $lang->verifyInvoked('translateEmail', ['statistics']);
        $built = $mail->email->build();

        $this->assertEquals('stat_recipient', $built['to']);
        $this->assertEquals('subject1', $built['subject']);

        //Covering other periods
        MomentApplicationMail::prepareStatsEmail(array(), 'stat_recipient', SurveyStats::DAY, 'start', 'end');
        MomentApplicationMail::prepareStatsEmail(array(), 'stat_recipient', SurveyStats::MONTH, 'start', 'end');
        MomentApplicationMail::prepareStatsEmail(array(), 'stat_recipient', SurveyStats::YEAR, 'start', 'end');

        //No recipient => exception
        $this->expectException('MailNoAddressesFoundException');
        MomentApplicationMail::prepareStatsEmail(array(), '', SurveyStats::YEAR, 'start', 'end');
    }

    /**
     * Test invitation email composition
     */
    public function testPrepareInvitationEmail() {
        $survey = Survey::fromId('survey1');

        $trans = new Translation(array(
            'subject' => new Translation('subject1', true, true), // Raw outputting, will be encoded
            'plain' => new Translation('plain_text', true, true), // Raw outputting
            'html' => new Translation('html_mail', true, false)
        ));

        $lang = mock::double('Lang', ['translateEmail' => $trans]);
        $translation = mock::double('Translation', ['r' => $trans]);

        $mail = MomentApplicationMail::prepareInvitationEmail($survey, 'guest_email');


        $lang->verifyInvoked('translateEmail', ['invitation']);
        $translation->verifyInvoked('r');
        $built = $mail->email->build();

        $this->assertEquals('guest_email', $built['to']);
        $this->assertEquals('subject1', $built['subject']);

        //With cache
        $mail = MomentApplicationMail::prepareInvitationEmail($survey, 'guest_email');
    }

    private function verifyMailTemplate($method, $mail_template, $entity = null, $reason = null) {
        $survey = Survey::fromId('survey1');
        mock::double('Owner', ['fromSurvey' => array( Owner::create($survey, Auth::user()))]);

        $trans = new Translation(array(
            'subject' => new Translation('subject1', true, true), // Raw outputting, will be encoded
            'plain' => new Translation('plain_text', true, true), // Raw outputting
            'html' => new Translation('html_mail', true, false)
        ));

        $lang = mock::double('Lang', ['translateEmail' => $trans]);
        $translation = mock::double('Translation', ['r' => $trans]);

        // test when owner
        /** @var Owner $owner */
        if($entity) {
            $mail = MomentApplicationMail::$method($survey, Auth::user()->email, $entity);
        } elseif($reason) {
            $mail = MomentApplicationMail::$method($survey, $reason, Auth::user()->email);
        } else {
            $mail = MomentApplicationMail::$method($survey, Auth::user()->email);
        }


        $lang->verifyInvoked('translateEmail', [$mail_template]);
        $translation->verifyInvoked('r');
        $built = $mail->email->build();

        $this->assertEquals(Auth::user()->email, $built['to']);
        $this->assertEquals('subject1', $built['subject']);

        //Body must contain text and html alternative
        $this->assertStringContainsString('plain_text', $built['body']);
        $this->assertStringContainsString('html_mail', $built['body']);

        // If $method is called twice it can use cache
        if($entity) {
            $mail = MomentApplicationMail::$method($survey, Auth::user()->email, $entity);
        } elseif($reason) {
            $mail = MomentApplicationMail::$method($survey, $reason, Auth::user()->email);
        } else {
            $mail = MomentApplicationMail::$method($survey, Auth::user()->email);
        }

        $lang->verifyInvoked('translateEmail', [$mail_template]);
        $translation->verifyInvoked('r');
        $built = $mail->email->build();

        $this->assertEquals('subject1', $built['subject']);

        //Body must contain text and html alternative
        $this->assertStringContainsString('plain_text', $built['body']);
        $this->assertStringContainsString('html_mail', $built['body']);
    }
}
