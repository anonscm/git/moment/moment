<?php
/**
 *     Moment - FormFieldChoice.php
 *
 * Copyright (C) 2022  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
use AspectMock\Test as mock;

class FormFieldChoiceTest extends \Codeception\Test\Unit {

    /**
     * Survey with a unique choice question
     * @var null
     */
    private $currentSurvey = null;

    /**
     * Participant to $currentSurvey
     * @var null
     */
    private $currentParticipant = null;

    private $questionMultiple = null;

    private $questionMultipleMaybe = null;

    private $questionUnique = null;


    protected function _before() {
        MomentTestUtil::startUp();
        MomentTestUtil::initSurvey('survey1');

        $this->currentSurvey = Survey::fromId('survey1');

        $questionData = array(
            "id" => 'question_unique',
            "title" => "Question 1",
            "type" => "text",
            "position" => 0,
            "options" => array(
                "force_unique_choice" => 1,
                "enable_maybe_choices" => 0
            ),
            "propositions" => array(
                array(
                    "id" => 1,
                    "type" => "text",
                    "position" => 0,
                    "header" => "Proposition 1",
                    "text" => ""
                ),
                array(
                    "id" => 2,
                    "type" => "text",
                    "position" => 1,
                    "header" => "Proposition 2",
                    "text" => ""
                )
            )
        );

        //Unique choice question
        $this->questionUnique = Question::create($this->currentSurvey, $questionData);


        //Multiple choice question
        $questionData['options']['force_unique_choice'] = 0;
        $this->questionMultiple = Question::create($this->currentSurvey, $questionData);

        //Multiple choice question + maybe
        $questionData['options']['enable_maybe_choices'] = 1;
        $this->questionMultipleMaybe = Question::create($this->currentSurvey, $questionData);

        $this->currentParticipant = MomentTestUtil::initParticipant('survey1');

    }

    protected function _after() {
        mock::clean();
    }

    public function testGetHTML() {

        $field = new FormFieldChoice('id', $this->questionMultiple);

        $html = $field->getHTML();

        $this->assertTrue(true);

        //This should get a table row
        $this->assertStringStartsWith('<tr class="your_choice">', $html);
        $this->assertStringEndsWith('</tr>'. "\n", $html);

        //There are 2 propositions in question
        $this->assertEquals(2, substr_count($html, 'field-yes-no-maybe-radio'));

        //This should contain proposition labels
        foreach($this->questionMultiple->propositions as $proposition) {
            $this->assertStringContainsString($proposition->label, $html);
        }

        $field = new FormFieldChoice('id', $this->questionUnique);
        $html = $field->getHTML();

        $this->assertEquals(2, substr_count($html, 'single_choice'));

        $field = new FormFieldChoice('id', $this->questionMultipleMaybe);
        $html = $field->getHTML();

        $this->assertEquals(2, substr_count($html, 'field-yes-no-maybe-radio'));

        //Should have maybe as an answer
        $this->assertStringContainsString('selected_value_maybe', $html);

    }

    public function testGetHTMLWithAnswerUneditable() {

        $participant = MomentTestUtil::initParticipant('survey1');

        $choices = [];
        foreach($this->questionMultiple->propositions as $proposition) {
            $choices[] = (object)[
                'proposition_id' => $proposition->id,
                'value' => count($choices) ? YesNoMaybeValues::NO : YesNoMaybeValues::YES,
            ];
        }

        $answer = Answer::create($participant, $this->questionMultiple, ['choices' => $choices]);

        mock::double('Survey', [
            '__get' => function($property) {
                if('disable_answer_edition' == $property)
                    return true;

                return __AM_CONTINUE__;
            }
        ]);


        $field = new FormFieldChoice('id', $this->questionMultiple, $answer);

        $html = $field->getHTML();

        $this->assertEquals(2, substr_count($html, 'uneditable'));
    }

    public function testGetHTMLWithAnswerUnavailable() {

        $participant = MomentTestUtil::initParticipant('survey1');

        $choices = [];
        foreach($this->questionMultiple->propositions as $proposition) {
            $choices[] = (object)[
                'proposition_id' => $proposition->id,
                'value' => count($choices) ? YesNoMaybeValues::NO : YesNoMaybeValues::YES,
            ];
        }

        $answer = Answer::create($participant, $this->questionMultiple, ['choices' => $choices]);

        mock::double('TextProposition', [
            '__get' => function($property) {
                if('is_available' == $property)
                    return false;

                return __AM_CONTINUE__;
            }
        ]);

        $field = new FormFieldChoice('id', $this->questionMultiple, $answer);

        $html = $field->getHTML();

        $this->assertEquals(1, substr_count($html, 'uneditable'));
    }

    public function testGetConstraints() {
        $field = new FormFieldChoice('id', $this->questionMultiple);

        //Should not return anything
        $this->assertNull($field->getConstraints());
    }

    public function testValidateData() {
        $field = new FormFieldChoice('id', $this->questionMultiple);

        $proposition_id = 0;

        foreach($this->questionMultiple->propositions as $proposition) {
            $proposition_id = $proposition->id;
            break;
        }

        $field->validateData($proposition_id);

        $this->expectException('FormDataValidationException');

        $field->validateData('nothing_like_a_valid_proposition_id');
    }
}
