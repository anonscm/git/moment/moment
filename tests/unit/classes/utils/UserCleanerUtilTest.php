<?php
/**
 *     Moment - UserCleanerUtilTest.php
 *
 * Copyright (C) 2024  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use AspectMock\Test as mock;

/**
 * Class UserCleanerUtilTest
 *
 * Tests UserCleanerUtil class
 */
class UserCleanerUtilTest extends \Codeception\Test\Unit {

    /**
     * Prepare context before each test
     *
     * @throws Exception
     */
    protected function _before() {

        mock::double('Config', ['get' => function($property) {
            if($property === 'user_cleaning.dry_run')
                return false;

            if($property === 'user_cleaning.inactive_months')
                return 24;

            if($property === 'user_cleaning.limit')
                return 1000;

            return __AM_CONTINUE__;
        }]);

    }

    /**
     * Clean after every test
     */
    protected function _after() {
        mock::clean();
    }

    public function testCleanInactiveUsers() {

        $userInactive1 = mock::double('User')->make();
        $userInactive2 = mock::double('User')->make();

        mock::double('User', [
            '__get' => function($property) {
                if($property === 'email')
                    return 'test@email.com';

                return __AM_CONTINUE__;
            },
            'fromUserEmail' => [$userInactive1]
        ]);

        $owner = Mock::double('Owner')->make();
        mock::double('Owner', [
            'fromEmail' => function($email) use($owner) {
                return [$owner];
            },
            '__get' => function($property) {
                if($property === 'Survey')
                    return Mock::double('Survey')->make();

                return __AM_CONTINUE__;
            }
        ]);
        $surveyMock = mock::double('Survey', [
            '__get' => function($property) use($owner) {
                if($property === 'Owners')
                    return [$owner];

                return __AM_CONTINUE__;
            },
            'delete' => true
        ]);

        $entityMock = mock::double('Entity', [
            'all' => [$userInactive1, $userInactive2],
            'delete' => true
        ]);

        //Total count
        mock::double('DBIStatement', ['execute' => null, 'fetchColumn' => 10]);
        mock::double('DBI', ['__callStatic' => mock::double('DBIStatement')->make()]);

        $mailMock = mock::double('MomentApplicationMail', ['prepareUserDeletionEmail' => mock::double('MomentApplicationMail')->make(), 'send' => true]);

        UserCleanerUtil::cleanInactiveUsers();

        //Only one notification per user
        $mailMock->verifyInvokedMultipleTimes('prepareUserDeletionEmail', 2);
        $entityMock->verifyInvokedMultipleTimes('delete', 2);

        $surveyMock->verifyInvoked('delete');

        $surveyDelete = $surveyMock->getCallsForMethod('delete');

        //Check with 2 owners
        $surveyMock = mock::double('Survey', [
            '__get' => function($property) use($owner) {
                if($property === 'Owners')
                    return [$owner, Mock::double('Owner')->make()];

                return __AM_CONTINUE__;
            },
            'delete' => true
        ]);

        UserCleanerUtil::cleanInactiveUsers();
        //Owner is deleted too
        $entityMock->verifyInvoked('delete', 3);

        //Survey should not be deleted (calls should not have changed)
        $this->assertEquals($surveyDelete, $surveyMock->getCallsForMethod('delete'));
    }


    public function testSendDeletionReminders() {

        $mailMock = mock::double('MomentApplicationMail', ['prepareUserDeletionReminderEmail' => mock::double('MomentApplicationMail')->make(), 'send' => true]);

        $userInactive1 = mock::double('User')->make();
        $userInactive2 = mock::double('User')->make();

        mock::double('Entity', [
            'all' => [$userInactive1, $userInactive2]
        ]);

        UserCleanerUtil::sendDeletionReminders();

        //4 times, each user recieve 2 reminders
        $mailMock->verifyInvokedMultipleTimes('prepareUserDeletionReminderEmail', 4);

    }
}
