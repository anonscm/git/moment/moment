<?php
/**
 *     Moment - MyCalUtilTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
use AspectMock\Test as mock;

class MyCalUtilTest extends \Codeception\Test\Unit {

    protected function _before() {
        MomentTestUtil::startUp();

        mock::double('Config', ['get' => function($property) {
            if($property === 'application_url')
                return 'https://localhost';

            return __AM_CONTINUE__;
        }]);
    }

    protected function _after() {
        mock::clean();
    }

    /**
     * Ensure cal url is correctly formed
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     */
    public function testGetMyCalUrl() {
        MomentTestUtil::authenticate();
        mock::double('KeyGenerator', ['generateKey' => 'abcdefgh']);

        $url = MyCalUtil::getMyCalUrl(MomentTestUtil::fromId('user1'));

        $this->assertEquals(Config::get('application_url').'rest/MyCal/abcdefgh', $url);
    }

    /**
     * Test Get Ics From Hash
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     */
    public function testGetIcsFromHashNoCache() {
        mock::double('KeyGenerator', ['generateKey' => 'abcdefgh']);
        mock::double('User', ['fromCalendarHash' => MomentTestUtil::fromId('user1')]);

        //No cache
        mock::double('MyCalUtil', ['getIcsFromCache' => false]);
        $content = <<<IFB
BEGIN:VCALENDAR
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
END:VCALENDAR
IFB;

        mock::double('MyCalUtil', ['getIcsFromUser' => $content]);
        $mock = mock::double('MyCalUtil', ['cacheIcsContent' => true]);

        $icsContent = MyCalUtil::getIcsFromHash('abcdefgh');

        $mock->verifyInvoked('cacheIcsContent', [MomentTestUtil::fromId('user1'), 'abcdefgh', $content]);
        $this->assertEquals($content, $icsContent);
    }

    /**
     * Test Get Ics From Hash using cache
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     */
    public function testGetIcsFromHashWithCache() {
        mock::double('KeyGenerator', ['generateKey' => 'abcdefgh']);
        mock::double('User', ['fromCalendarHash' => MomentTestUtil::fromId('user1')]);

        $content = <<<IFB
BEGIN:VCALENDAR
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
END:VCALENDAR
IFB;

        //result is cached
        mock::double('MyCalUtil', ['getIcsFromCache' => $content]);
        $mock = mock::double('MyCalUtil', ['cacheIcsContent' => true]);

        $icsContent = MyCalUtil::getIcsFromHash('abcdefgh');

        $this->assertEquals($content, $icsContent);
    }

    /**
     * Test Get Ics From Hash but hash does not match a user
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     */
    public function testGetIcsFromHashNoUser() {
        mock::double('KeyGenerator', ['generateKey' => 'abcdefgh']);
        mock::double('User', ['fromCalendarHash' => false]);

        $icsContent = MyCalUtil::getIcsFromHash('abcdefgh');

        $this->assertEquals(null, $icsContent);
    }

    /**
     * Test Get Ics From User Owned
     */
    public function testGetIcsFromUserOwned() {
        $user = User::fromId('user1');
        MomentTestUtil::authenticate();
        MomentTestUtil::initSurvey('survey1');
        $survey = Survey::fromId('survey1');

        MomentTestUtil::initSurvey('survey2');
        $survey2 = Survey::fromId('survey2');
        $survey2->close();

        mock::double('Survey', [
                'ownedBy' => [Survey::fromId('survey1'), $survey2],
                '__get' => function($property) {
                    if($property === 'ultimate_participant')
                        return false;

                    return __AM_CONTINUE__;
                }
            ]
        );
        mock::double('Participant', ['fromAuthenticatedUser' => []]);

        //We must add a date proposition in the future
        $proposition_data = [
            "type" => PropositionTypes::DAY_HOUR,
            "base_day" => time()+3600,
            "base_time" => 28800
        ];
        $proposition = DateProposition::create(Survey::fromId('survey1')->Questions[1], $proposition_data);

        //We must add a date proposition in the future (10 days events)
        $proposition_data2 = [
            "type" => PropositionTypes::RANGE_OF_DAYS,
            "base_day" => time() + 86400 +3600,
            "end_day" => time()+ 3600 + 86400 * 10
        ];
        $proposition2 = DateProposition::create(Survey::fromId('survey1')->Questions[1], $proposition_data2);

        $prodid = Config::get('application_name')."_v".Config::get('version');

        $uid = 'owned_'.$survey->id.'_'.sha1($proposition->id.$proposition->Question->id)
        .'@'.parse_url(Config::get('application_url'), PHP_URL_HOST);

        $created = DateUtil::toLocale($survey->created, 'UTC', '', IcsConstants::DATE_FORMAT_UTC);
        $updated = DateUtil::toLocale($survey->updated, 'UTC', '', IcsConstants::DATE_FORMAT_UTC);

        $dtstart = DateUtil::toLocale($proposition_data['base_day'] + $proposition_data['base_time'], 'UTC', '', IcsConstants::DATE_FORMAT_UTC);;
        $dtend = DateUtil::toLocale($proposition_data['base_day'] + $proposition_data['base_time'] + 3600, 'UTC', '', IcsConstants::DATE_FORMAT_UTC);

        $uid2 = 'owned_'.$survey->id.'_'.sha1($proposition2->id.$proposition2->Question->id)
            .'@'.parse_url(Config::get('application_url'), PHP_URL_HOST);

        $dtstart2 = DateUtil::toLocale($proposition_data2['base_day'], 'UTC', '', IcsConstants::DATE_ONLY_FORMAT);;
        $dtend2 = DateUtil::toLocale($proposition_data2['end_day'] + 86400, 'UTC', '', IcsConstants::DATE_ONLY_FORMAT);;

        $parsed_description = $survey->parsed_description.sprintf("<p><a href=\"%s\">%s</a></p>", $survey->path.'?force_auth=true', Lang::tr('access_link_to_the_survey'));
        $parsed_description = str_replace(array("\n", ";"), array("\\n", "\;"), $parsed_description);
        $parsed_description = wordwrap($parsed_description, 75, PHP_EOL."  ");

        $description = $survey->description.PHP_EOL.Lang::tr('access_link_to_the_survey').' : '.$survey->path.'?force_auth=true'.PHP_EOL;
        $description = str_replace(array("\n", ";"), array("\\n", "\;"), $description);
        $description = wordwrap($description, 75, PHP_EOL."  ");

        $content = <<<IFB
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//$prodid//EN
METHOD:REQUEST
BEGIN:VEVENT
UID:$uid
SEQUENCE:1
TRANSP:OPAQUE
DTSTAMP:$created
CREATED:$created
LAST-MODIFIED:$updated
ORGANIZER;CN=Test User <test@user.com>:mailto:Test User <test@user.com>
DTSTART:$dtstart
DTEND:$dtend
SUMMARY:$survey->title - {$survey->Questions[1]->title}
X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE html>\\n<html
  lang="fr">\\n<body>\\n$parsed_description\\n</body>\\n</html>
DESCRIPTION:$description
STATUS:TENTATIVE
CLASS:PUBLIC
X-MICROSOFT-CDO-INTENDEDSTATUS:TENTATIVE
END:VEVENT
BEGIN:VEVENT
UID:$uid2
SEQUENCE:1
TRANSP:OPAQUE
DTSTAMP:$created
CREATED:$created
LAST-MODIFIED:$updated
ORGANIZER;CN=Test User <test@user.com>:mailto:Test User <test@user.com>
DTSTART:$dtstart2
DTEND:$dtend2
SUMMARY:$survey->title - {$survey->Questions[1]->title}
X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE html>\\n<html
  lang="fr">\\n<body>\\n$parsed_description\\n</body>\\n</html>
DESCRIPTION:$description
STATUS:TENTATIVE
CLASS:PUBLIC
X-MICROSOFT-CDO-INTENDEDSTATUS:TENTATIVE
END:VEVENT
END:VCALENDAR
IFB;

        $icsContent = MyCalUtil::getIcsFromUser($user, null);

        $this->assertEquals($content, $icsContent);
    }

    /**
     * Test Get Ics From User Owned With Ultimate Participant
     */
    public function testGetIcsFromUserOwnedWithUltimateParticipant() {
        $user = User::fromId('user1');
        MomentTestUtil::authenticate();
        MomentTestUtil::initSurvey('survey1');
        $survey = Survey::fromId('survey1');

        mock::double('Survey', [
            'ownedBy' => [$survey],
            '__get' => function($property) {
                if($property === 'ultimate_participant')
                    return mock::double('Participant')->make();

                return __AM_CONTINUE__;
            }
        ]);

        $dateQuestion = null;
        foreach($survey->Questions as $question) {
            if($question->type == QuestionTypes::DATE)
                $dateQuestion = $question;
        }

        mock::double('Participant', [
            'fromAuthenticatedUser' => [],
            '__get' => function($property) use($dateQuestion) {
                if($property === 'sorted_answers')
                    return [
                        $dateQuestion->id => mock::double('Answer')->make()
                    ];

                return __AM_CONTINUE__;
            }
        ]);

        mock::double('Answer', [
            'getValue' => YesNoMaybeValues::NO
        ]);

        //We must add a date proposition in the future
        $proposition_data = [
            "type" => PropositionTypes::DAY_HOUR,
            "base_day" => time() + 3600,
            "base_time" => 28800
        ];
        DateProposition::create(Survey::fromId('survey1')->Questions[1], $proposition_data);

        //We must add a date proposition in the future (10 days events)
        $proposition_data2 = [
            "type" => PropositionTypes::RANGE_OF_DAYS,
            "base_day" => time() + 3600,
            "end_day" => time() + 3600 + 86400 * 10
        ];
        DateProposition::create(Survey::fromId('survey1')->Questions[1], $proposition_data2);

        $icsContentNo = MyCalUtil::getIcsFromUser($user, null);

        $prodid = Config::get('application_name')."_v".Config::get('version');

        $content = <<<IFB
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//$prodid//EN
METHOD:REQUEST
END:VCALENDAR
IFB;

        //An ultimate participant have been set and all choice are to NO => nothing in ics
        $this->assertEquals($content, $icsContentNo);

        mock::double('Answer', [
            'getValue' => YesNoMaybeValues::YES
        ]);

        $icsContentYes = MyCalUtil::getIcsFromUser($user, null);

        //An ultimate participant have been set and all choice are to YES => ics should not be empty
        $this->assertNotEquals($icsContentNo, $icsContentYes);
    }

    /**
     * Test Get Ics From User Participated
     */
    public function testGetIcsFromUserParticipated() {
        $user = MomentTestUtil::authenticate();
        $survey = MomentTestUtil::initSurvey('survey1');

        //We must add a date proposition in the future
        $proposition_data = [
            "type" => PropositionTypes::DAY_HOUR,
            "base_day" => time()+3600,
            "base_time" => 28800
        ];
        $proposition = DateProposition::create($survey->Questions[1], $proposition_data);

        //We must add a second date proposition in the future (half an hour event)
        $proposition_data2 = [
            "type" => PropositionTypes::RANGE_OF_HOURS,
            "base_day" => time() + 86400 +3600,
            "base_time" => 28800,
            "end_time" => 30600
        ];
        $proposition2 = DateProposition::create($survey->Questions[1], $proposition_data2);

        $participant = MomentTestUtil::initParticipant('survey1');

        mock::double('Survey', ['ownedBy' => []]);
        mock::double('Participant', ['fromAuthenticatedUser' => [$participant]]);
        mock::double('Lang', ['tr' => function($id) { return $id; }]);

        //Preparing expected data
        $prodid = Config::get('application_name')."_v".Config::get('version');

        $uid = 'participated_'.$survey->id.'_'.sha1($proposition->id.$proposition->Question->id)
            .'@'.parse_url(Config::get('application_url'), PHP_URL_HOST);

        $created = DateUtil::toLocale($survey->created, 'UTC', '', IcsConstants::DATE_FORMAT_UTC);
        $updated = DateUtil::toLocale($survey->updated, 'UTC', '', IcsConstants::DATE_FORMAT_UTC);

        $dtstart = DateUtil::toLocale($proposition_data['base_day'] + $proposition_data['base_time'], 'UTC', '', IcsConstants::DATE_FORMAT_UTC);;
        $dtend = DateUtil::toLocale($proposition_data['base_day'] + $proposition_data['base_time'] + 3600, 'UTC', '', IcsConstants::DATE_FORMAT_UTC);;

        $uid2 = 'participated_'.$survey->id.'_'.sha1($proposition2->id.$proposition2->Question->id)
            .'@'.parse_url(Config::get('application_url'), PHP_URL_HOST);


        $dtstart2 = DateUtil::toLocale($proposition_data2['base_day'] + $proposition_data2['base_time'], 'UTC', '', IcsConstants::DATE_FORMAT_UTC);;
        $dtend2 = DateUtil::toLocale($proposition_data2['base_day'] + $proposition_data2['end_time'], 'UTC', '', IcsConstants::DATE_FORMAT_UTC);;

        $parsed_description = $survey->parsed_description.sprintf("<p><a href=\"%s\">%s</a></p>", $survey->path.'?force_auth=true', Lang::tr('access_link_to_the_survey'));
        $parsed_description = str_replace(array("\n", ";"), array("\\n", "\;"), $parsed_description);
        $parsed_description = wordwrap($parsed_description, 75, PHP_EOL."  ");

        $description = $survey->description.PHP_EOL.Lang::tr('access_link_to_the_survey').' : '.$survey->path.'?force_auth=true'.PHP_EOL;
        $description = str_replace(array("\n", ";"), array("\\n", "\;"), $description);
        $description = wordwrap($description, 75, PHP_EOL."  ");

        $content = <<<IFB
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//$prodid//EN
METHOD:REQUEST
BEGIN:VEVENT
UID:$uid
SEQUENCE:1
TRANSP:OPAQUE
DTSTAMP:$created
CREATED:$created
LAST-MODIFIED:$updated
ORGANIZER;CN=Test User <test@user.com>:mailto:Test User <test@user.com>
ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE;PARTSTAT=NEEDS-ACTION:Test User
  <test@user.com>
DTSTART:$dtstart
DTEND:$dtend
SUMMARY:$survey->title - {$survey->Questions[1]->title}
X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE html>\\n<html
  lang="fr">\\n<body>\\n$parsed_description\\n</body>\\n</html>
DESCRIPTION:$description
STATUS:TENTATIVE
CLASS:PUBLIC
X-MICROSOFT-CDO-INTENDEDSTATUS:TENTATIVE
END:VEVENT
BEGIN:VEVENT
UID:$uid2
SEQUENCE:1
TRANSP:OPAQUE
DTSTAMP:$created
CREATED:$created
LAST-MODIFIED:$updated
ORGANIZER;CN=Test User <test@user.com>:mailto:Test User <test@user.com>
ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE;PARTSTAT=NEEDS-ACTION:Test User
  <test@user.com>
DTSTART:$dtstart2
DTEND:$dtend2
SUMMARY:$survey->title - {$survey->Questions[1]->title}
X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE html>\\n<html
  lang="fr">\\n<body>\\n$parsed_description\\n</body>\\n</html>
DESCRIPTION:$description
STATUS:TENTATIVE
CLASS:PUBLIC
X-MICROSOFT-CDO-INTENDEDSTATUS:TENTATIVE
END:VEVENT
END:VCALENDAR
IFB;
        error_log("h------------");
        $icsContent = MyCalUtil::getIcsFromUser($user, null);
        error_log("h------------");
        $this->assertEquals($content, $icsContent);

        mock::clean('Lang');
    }

    /**
     * Test Get Ics From User Participated with an ultimate particpant
     */
    public function testGetIcsFromUserParticipatedWithUltimateParticipant() {
        $user = MomentTestUtil::authenticate();
        $survey = MomentTestUtil::initSurvey('survey1');

        //We must add a date proposition in the future
        $proposition_data = [
            "type" => PropositionTypes::DAY_HOUR,
            "base_day" => time()+3600,
            "base_time" => 28800
        ];
        $proposition = DateProposition::create($survey->Questions[1], $proposition_data);

        //We must add a second date proposition in the future (half an hour event)
        $proposition_data2 = [
            "type" => PropositionTypes::RANGE_OF_HOURS,
            "base_day" => time()+3600,
            "base_time" => 28800,
            "end_time" => 30600
        ];
        $proposition2 = DateProposition::create($survey->Questions[1], $proposition_data2);

        $participant = MomentTestUtil::initParticipant('survey1');

        mock::double('Survey', [
            'ownedBy' => [],
            '__get' => function($property) {
                if($property === 'ultimate_participant')
                    return mock::double('Participant')->make();

                return __AM_CONTINUE__;
            }
        ]);

        $dateQuestion = null;
        foreach($survey->Questions as $question) {
            if($question->type == QuestionTypes::DATE)
                $dateQuestion = $question;
        }

        mock::double('Participant', [
            'fromAuthenticatedUser' => [$participant],
            '__get' => function($property) use($dateQuestion) {
                if($property === 'sorted_answers')
                    return [
                        $dateQuestion->id => mock::double('Answer')->make()
                    ];

                return __AM_CONTINUE__;
            }
        ]);

        mock::double('Answer', [
            'getValue' => YesNoMaybeValues::NO
        ]);

        $icsContent = MyCalUtil::getIcsFromUser($user, null);

        $prodid = Config::get('application_name')."_v".Config::get('version');

        $icsContentNo = <<<IFB
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//$prodid//EN
METHOD:REQUEST
END:VCALENDAR
IFB;

        $this->assertEquals($icsContentNo, $icsContent);

        mock::double('Answer', [
            'getValue' => YesNoMaybeValues::YES
        ]);

        $icsContentYes = MyCalUtil::getIcsFromUser($user, null);

        //An ultimate participant have been set and all choice are to YES => ics should not be empty
        $this->assertNotEquals($icsContentNo, $icsContentYes);

        mock::double('Choice', [
            '__get' => function($property) {
                if($property === 'proposition')
                    throw new NotFoundException('proposition', 'test');

                return __AM_CONTINUE__;
            }
        ]);

        //Proposition not found => no event
        $this->assertEquals($icsContentNo, MyCalUtil::getIcsFromUser($user, null));

    }


    /**
     * Test Get Ics From User Participated
     */
    public function testGetIcsFromUserExclude() {
        $user = MomentTestUtil::authenticate();
        $survey = MomentTestUtil::initSurvey('survey1');
        $participant = MomentTestUtil::initParticipant('survey1');

        mock::double('Survey', ['ownedBy' => [$survey]]);
        mock::double('Participant', ['fromAuthenticatedUser' => [$participant]]);

        $icsContent = MyCalUtil::getIcsFromUser($user, $survey);

        $prodid = Config::get('application_name')."_v".Config::get('version');

        $content = <<<IFB
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//$prodid//EN
METHOD:REQUEST
END:VCALENDAR
IFB;

        $this->assertEquals($content, $icsContent);
    }

    /**
     * Test Get Ics From Hash with cache file
     */
    public function testGetIcsFromCache() {
        mock::double('User', ['fromCalendarHash' => MomentTestUtil::fromId('user1')]);
        mock::double('FileIO', [
                'readWhole' => 'test',
                'delete' => true
            ]);
        $mock = mock::double('MyCalUtil', [
            'getCacheFileName' => __FILE__
        ]);

        $icsContent = MyCalUtil::getIcsFromHash('hash');

        $this->assertEquals('test', $icsContent);

        //Checking when cache file does not exists
        mock::double('FileIO', [
            'readWhole' => function() {
                throw new FileIOException('not found', []);
            },
            'writeWhole' => function() {
                throw new FileIOException('access denied', []);
            }]
        );

        $icsContent = MyCalUtil::getIcsFromHash('hash');
        //Cache not found getIcsFromUser should be called
        $mock->verifyInvoked('getIcsFromUser');
    }

    /**
     * Test Invalidate cache
     */
    public function testInvalidateIcsCacheForUser() {
        $mock = mock::double('FileIO', [
            'delete' => true
        ]);

        mock::double('MyCalUtil', [
            'getUserDir' => __DIR__
        ]);

        //Invalidate
        MyCalUtil::invalidateIcsCacheForUser('test@test.com');

        $mock->verifyInvoked('delete');

        //Checking when cache file does not exists
        mock::double('FileIO', [
                'delete' => function() {
                    throw new FileIOException('access denied', []);
                }]
        );

        $icsContent = MyCalUtil::invalidateIcsCacheForUser('test@test.com');

    }
}
