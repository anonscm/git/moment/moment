<?php
/**
 *     Moment - AnswerUtilTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
use AspectMock\Test as mock;

class AnswerUtilTest extends \Codeception\Test\Unit {

    /**
     * Survey with a unique choice question
     * @var null
     */
    private $currentSurvey = null;

    /**
     * Participant to $currentSurvey
     * @var null
     */
    private $currentParticipant = null;

    private $questionMultiple = null;

    private $questionUnique = null;


    protected function _before() {
        MomentTestUtil::startUp();
        MomentTestUtil::initSurvey('survey1');

        $this->currentSurvey = Survey::fromId('survey1');

        $questionData = array(
            "id" => 'question_unique',
            "title" => "Question 1",
            "type" => "text",
            "position" => 0,
            "options" => array(
                "force_unique_choice" => 1,
                "enable_maybe_choices" => 0
            ),
            "propositions" => array(
                array(
                    "id" => 1,
                    "type" => "text",
                    "position" => 0,
                    "header" => "Proposition 1",
                    "text" => ""
                ),
                array(
                    "id" => 2,
                    "type" => "text",
                    "position" => 1,
                    "header" => "Proposition 2",
                    "text" => ""
                )
            )
        );

        //Unique choice question
        $this->questionUnique = Question::create($this->currentSurvey, $questionData);


        //Multiple choice question
        $questionData['options']['force_unique_choice'] = 0;
        $this->questionMultiple = Question::create($this->currentSurvey, $questionData);


        $this->currentParticipant = MomentTestUtil::initParticipant('survey1');

    }

    protected function _after() {
        mock::clean();
    }

    /**
     * Tests completeChoices that completes answers data with default values
     */
    public function testCompleteChoices() {

        MomentTestUtil::initSurvey('survey1');
        $survey = Survey::fromId('survey1');

        foreach($survey->Questions as $question) {

            //We answer this question by selecting YES at the first proposition
            $answer_data = array(
                'choices' => array(),
                'comment' => 'This is a comment'
            );

            $pid = null;

            foreach($question->sorted_propositions as $proposition) {
                $answer_data['choices'][] = (object) array(
                    'proposition_id' => $proposition->id,
                    'value' => YesNoMaybeValues::YES
                );
                $pid = $proposition->id;
                break;
            }

            $completeData = AnswerUtil::completeChoices($question, $answer_data);

            $this->assertEquals($answer_data['comment'], $completeData['comment']);

            //A complete answer as choices for all proposition
            $this->assertEquals(count($question->sorted_propositions), count($completeData['choices']));


            //If no choice made for a proposition it should be default value (NO)
            foreach ($completeData['choices'] as $choice) {
                $choice = (array) $choice;

                if($choice['proposition_id'] == $pid) {
                    $this->assertEquals(YesNoMaybeValues::YES, $choice['value']);
                } else {
                    $this->assertEquals(YesNoMaybeValues::NO, $choice['value']);
                }

            }

        }
    }

    /**
     * Test answers data validation
     *
     * @throws AnswerMissingPropertyException
     */
    public function testValidate() {
        //We answer this question by selecting YES at the first proposition
        $answer_data = array(
            'question_id' => 'this_is_a_fake_id',
            'choices' => array(),
            'comment' => 'This is a comment'
        );

        //Answer data is valid
        $this->assertTrue(AnswerUtil::validate($answer_data));
    }

    /**
     * Test answers data validation no question id
     *
     * @throws AnswerMissingPropertyException
     */
    public function testValidateNoQuestionId() {
        //Answer data does not contain question_id
        $answer_data = array(
            'choices' => array(),
            'comment' => 'This is a comment'
        );

        $this->expectException('AnswerMissingPropertyException');
        AnswerUtil::validate($answer_data);
    }

    /**
     * Test answers data validation no choices
     *
     * @throws AnswerMissingPropertyException
     */
    public function testValidateNoChoices() {
        //Answer data does not contain choices
        $answer_data = array(
            'question_id' => 'this_is_a_fake_id',
            'comment' => 'This is a comment'
        );

        $this->expectException('AnswerMissingPropertyException');
        AnswerUtil::validate($answer_data);
    }

    /**
     * Test answers data validation no answer id
     *
     * @throws AnswerMissingPropertyException
     */
    public function testValidateNoAnswerId() {
        //Answer data does not contain choices
        $answer_data = array(
            'question_id' => 'this_is_a_fake_id',
            'choices' => array(),
            'comment' => 'This is a comment'
        );

        $this->expectException('AnswerMissingPropertyException');
        AnswerUtil::validate($answer_data, true);
    }


    /**
     * Test validate Choices
     *
     * @throws AnswerMissingPropertyException
     */
    public function testValidateChoices() {
        try {
            AnswerUtil::validateChoices($this->currentParticipant, $this->questionUnique, $this->getAnswerData($this->questionUnique, YesNoMaybeValues::YES), array());
        } catch (Exception $e) {
            $this->assertFalse(true, 'An exception have been thrown '.$e->getMessage());
        }
    }

    /**
     * Test validate Choices with maybe answer when not allowed
     *
     * @throws AnswerBadChoicesException
     * @throws AnswerBadPropertyException
     * @throws AnswerMissingException
     * @throws AnswerMissingPropertyException
     * @throws AnswerPropositionNotAvailableException
     */
    public function testValidateChoicesMaybe() {
        $this->expectException(AnswerBadChoicesException::class);
        AnswerUtil::validateChoices($this->currentParticipant, $this->questionUnique, $this->getAnswerData($this->questionUnique, YesNoMaybeValues::MAYBE), array());
    }

    /**
     * Test validate Choices with more than one yes to a unique choice question
     *
     * @throws AnswerBadChoicesException
     * @throws AnswerBadPropertyException
     * @throws AnswerMissingException
     * @throws AnswerMissingPropertyException
     * @throws AnswerPropositionNotAvailableException
     */
    public function testValidateChoicesMoreThanOneYes() {
        $this->expectException(AnswerBadChoicesException::class);
        AnswerUtil::validateChoices($this->currentParticipant, $this->questionUnique, $this->getAnswerData($this->questionUnique, YesNoMaybeValues::YES, false), array());
    }

    /**
     * Test validate Choices with only no to a unique choice question
     *
     * @throws AnswerBadChoicesException
     * @throws AnswerBadPropertyException
     * @throws AnswerMissingException
     * @throws AnswerMissingPropertyException
     * @throws AnswerPropositionNotAvailableException
     */
    public function testValidateChoicesOnlyNo() {
        $this->expectException(AnswerMissingException::class);
        AnswerUtil::validateChoices($this->currentParticipant, $this->questionUnique, $this->getAnswerData($this->questionUnique, YesNoMaybeValues::NO, false), array());
    }


    /**
     * Test validate Choices with maybe to a multiple question
     *
     * @throws AnswerBadChoicesException
     * @throws AnswerBadPropertyException
     * @throws AnswerMissingException
     * @throws AnswerMissingPropertyException
     * @throws AnswerPropositionNotAvailableException
     */
    public function testValidateChoicesMultipleMaybe() {
        $this->expectException(AnswerBadChoicesException::class);

        $propositions  = $this->questionMultiple->propositions;

        $pid = array_shift($propositions)->id;

        AnswerUtil::validateChoices($this->currentParticipant, $this->questionMultiple, $this->getAnswerData($this->questionMultiple, YesNoMaybeValues::MAYBE, false), array($pid));
    }

    public function testCountPropositions() {

        mock::double('Choice', ['getValueCount' => 1]);

        $propositions_sorting = AnswerUtil::countPropositions($this->questionMultiple);

        foreach($propositions_sorting as $pid => $counts) {
            $this->assertEquals(1, $counts[YesNoMaybeValues::YES]);
            $this->assertEquals(1, $counts[YesNoMaybeValues::MAYBE]);
        }
    }

    private function getAnswerData($question, $choice, $onlyFirst = true) {

        //We answer this question by selecting YES at the first proposition
        $answer_data = array(
            'choices' => array(),
            'comment' => 'This is a comment'
        );

        foreach ($question->sorted_propositions as $proposition) {
            $answer_data['choices'][] = (object)array(
                'proposition_id' => $proposition->id,
                'value' => $choice
            );

            if($onlyFirst) {
                break;
            }
        }

        return $answer_data;
    }
}
