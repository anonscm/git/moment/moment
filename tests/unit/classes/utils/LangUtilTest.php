<?php
/**
 *     Moment - LangUtilTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
use AspectMock\Test as mock;

/**
 * LangUtilTest test on LangUtil
 */
class LangUtilTest extends \Codeception\Test\Unit {

    /**
     * Test tr() translating fr
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    public function testTrFr() {
        $translation =  LangUtil::tr('comment_created', 'fr');
        $this->assertEquals('Commentaire ajouté', $translation);
    }

    /**
     * Test tr() translating en
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    public function testTrEn() {
        $translation =  LangUtil::tr('comment_created', 'en');
        $this->assertEquals('Comment added', $translation);
    }

    /**
     * Test tr() translating default
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    public function testTrDefault() {
        $translation =  LangUtil::tr('comment_created', 'es');

        if('fr' == Config::get('lang.default'))
            $this->assertEquals('Commentaire ajouté', $translation);

        if('en' == Config::get('lang.default'))
            $this->assertEquals('Comment added', $translation);


        $translation =  LangUtil::tr('comment_created');

        if('fr' == Config::get('lang.default'))
            $this->assertEquals('Commentaire ajouté', $translation);

        if('en' == Config::get('lang.default'))
            $this->assertEquals('Comment added', $translation);

        //Trying to use a file translation
        LangUtil::tr('home_page_disclamer');
    }

}
