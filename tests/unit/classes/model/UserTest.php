<?php
/**
 *     Moment - UserTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use AspectMock\Test as mock;

class UserTest extends \Codeception\Test\Unit {

    protected function _before() {
        MomentTestUtil::startUp();
    }

    protected function _after() {
        mock::clean();
    }

    public function testFromCalendarHash() {
        $user = MomentTestUtil::authenticate('user1');

        mock::clean();
        mock::double('Entity', ['all' => [$user]]);
        $this->assertEquals($user, User::fromCalendarHash('a_calendar_hash'));

        mock::clean();
        mock::double('Entity', ['all' => []]);
        $this->assertFalse(User::fromCalendarHash('a_calendar_hash'));
    }

    public function testFromEmail() {
        $user1 = MomentTestUtil::authenticate('user1');
        mock::clean();
        mock::double('Entity', ['all' => [$user1]]);

        $this->assertEquals([$user1], User::fromUserEmail('a_calendar_hash'));

        mock::clean();
        mock::double('Entity', ['all' => []]);
        $this->assertEquals([], User::fromUserEmail('a_calendar_hash'));
    }

    public function testGetEmailSender() {
        $user1 = MomentTestUtil::authenticate('user1');
        mock::clean();
        mock::double('User', ['__get' => function($property) {
            if($property === 'email')
                return 'test@email.com';

            return __AM_CONTINUE__;
        }]);

        $this->assertEquals('test@email.com', $user1->getEmailSender());
    }

    public function test__get() {
        $time = time();
        $user = mock::double('User')->make();
        $user->last_motd_time = $time;
        $user->first_email = 'test@email.com';
        mock::clean();

        $this->assertEquals($time, $user->__get('last_motd_time'));
        $this->assertEquals('test@email.com', $user->__get('first_email'));
    }

    public function test__set() {
        $time = time();
        $user = mock::double('User')->make();
        $user->__set('last_motd_time', $time);
        $user->__set('first_email', 'test@email.com');
        $user->__set('auth_secret', 'secret');
        $user->__set('calendar_hash', 'a_calendar_hash');
        $user->__set('lang', 'fr');
        mock::clean();

        $this->assertEquals($time, $user->last_motd_time);
        $this->assertEquals('test@email.com', $user->first_email);
        $this->assertEquals('secret', $user->auth_secret);
        $this->assertEquals('a_calendar_hash', $user->calendar_hash);
        $this->assertEquals('fr', $user->lang);
    }

    public function testGetPreference() {
        $user = mock::double('User')->make();
        mock::clean();
        mock::double('User', ['__get' => function($property) {
            if($property === 'Preferences') {
                return [
                    (object)[
                        'name' => 'pref1',
                        'value' => 'value1'
                    ],
                    (object)[
                        'name' => 'pref2',
                        'value' => 'value2'
                    ]
                ];
            }

            return __AM_CONTINUE__;
        }]);

        $this->assertEquals( (object)[
            'name' => 'pref1',
            'value' => 'value1'
        ], $user->getPreference('pref1'));

        $this->assertNull($user->getPreference('pref3'));
    }


    public function testGetPreferenceValue() {
        $user = mock::double('User')->make();
        mock::clean();
        mock::double('User', ['__get' => function($property) {
            if($property === 'Preferences') {
                return [
                    (object)[
                        'name' => 'pref1',
                        'value' => 'value1'
                    ],
                    (object)[
                        'name' => 'pref2',
                        'value' => 'value2'
                    ]
                ];
            }

            return __AM_CONTINUE__;
        }]);

        $this->assertEquals('value1', $user->getPreferenceValue('pref1'));
        $this->assertNull($user->getPreferenceValue('pref3'));
    }

}
