<?php
/**
 *     Moment - CalendarTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// include_once __DIR__ . '/../../src/includes/core/init.php';

use AspectMock\Test as mock;

class CalendarTest extends \Codeception\Test\Unit {

    private $testUser = null;

    protected function _before() {
        mock::double('Entity', ['save' => null]);
        mock::double('Entity', ['addRelated' => true]);
        mock::double('DBIStatement', ['execute' => null]);
        mock::double('DBIStatement', ['fetch' => true]);

        mock::double('UserBase', ['createIfNotExists' => null]);

        mock::double('CalendarManager', ['checkUrl' => ['code' => CalendarStatusCodes::OK, 'message' => CalendarStatusMessages::OK]]);

        $this->testUser = User::create('toto@renater.fr');
    }

    protected function _after() {
        mock::clean();
    }

    /**
     * Test for __get() : property
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function test__get() {

        $cal = Calendar::create($this->testUser,
            [
                'url' => 'https://url.de.test',
                'name' => 'Test Calendar Name',
                'settings' => [],
                'status' => [],
                'checked' => null
            ]
        );
        $properties [] = $cal->__get('url');
        $properties [] = $cal->__get('name');
        $properties [] = $cal->__get('settings');
        $properties [] = $cal->__get('status');
        $properties [] = $cal->__get('fresh_status');
        $properties [] = $cal->__get('checked');
        $properties [] = $cal->__get('storedInDatabase');

        $this->assertEquals([
                'https://url.de.test',
                'Test Calendar Name',
                [
                    'color' => '#6b79b8'
                ],
                ['code' => CalendarStatusCodes::UNKNOWN, 'message' => CalendarStatusMessages::UNKNOWN],
                ['code' => CalendarStatusCodes::UNKNOWN, 'message' => CalendarStatusMessages::UNKNOWN],
                null,
                false
            ],
            $properties
        );
    }


    /**
     * Test for __set() : Properties
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function test__set() {

        $cal = Calendar::create($this->testUser,
            [
                'url' => 'https://url.de.test',
                'name' => 'Test Calendar Name',
                'settings' => [],
                'status' => [],
                'checked' => null
            ]
        );

        $cal->__set('url', 'https://url.de.test2' );
        $cal->__set('name', 'Test Calendar Name 2' );
        $cal->__set('settings', ['color' => '#FFF'] );
        $cal->__set('status', ['code' => CalendarStatusCodes::ERROR, 'message' => CalendarStatusMessages::ERROR]);
        $cal->__set('checked', time());

        //Calling parent::_get
        $this->expectException('PropertyAccessException');
        $cal->__set('thisIsNotAValidProperty', true);

        $this->assertEquals($cal->url, 'https://url.de.test2' );
        $this->assertEquals($cal->name, 'Test Calendar Name 2' );
        $this->assertEquals($cal->settings, ['color' => '#FFF'] );
        $this->assertEquals($cal->status, ['code' => CalendarStatusCodes::ERROR, 'message' => CalendarStatusMessages::ERROR] );
        $this->assertEquals($cal->checked, time());
    }

    /**
     * Test for create() :
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function testCreate() {
        $cal = Calendar::create($this->testUser,
            [
                'url' => 'https://url.de.test',
                'name' => 'Test Calendar Name',
                'settings' => ['color' => '#FFF'],
                'status' => [],
                'checked' => null
            ]
        );

        $this->assertEquals($cal->url, 'https://url.de.test' );
        $this->assertEquals($cal->name, 'Test Calendar Name' );
        $this->assertEquals($cal->settings, ['color' => '#FFF'] );
        $this->assertEquals($cal->status, ['code' => CalendarStatusCodes::UNKNOWN, 'message' => CalendarStatusMessages::UNKNOWN] );
        $this->assertEquals($cal->checked, null);
    }

    /**
     * Test for create() that fails :
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function testCreateFails() {
        $this->expectException('MissingArgumentException');

        $cal = Calendar::create($this->testUser,
            [
                'name' => 'Test Calendar Name',
                'settings' => ['color' => '#FFF'],
                'status' => [],
                'checked' => null
            ]
        );
    }

    /**
     * Test for update() :
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function testUpdate() {
        $cal = Calendar::create($this->testUser,
            [
                'url' => 'https://url.de.test',
                'name' => 'Test Calendar Name',
                'settings' => [],
                'status' => [],
                'checked' => null
            ]
        );

        $cal->update($this->testUser,[
            'url' => 'https://url.de.test2',
            'name' => 'Test Calendar Name 2',
            'settings' => ['color' => '#000']
        ]);

        $this->assertEquals($cal->url, 'https://url.de.test2');
        $this->assertEquals($cal->name, 'Test Calendar Name 2');
        $this->assertEquals($cal->settings, ['color' => '#000']);
    }
}
