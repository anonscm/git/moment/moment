<?php
/**
 *     Moment - ParticipantTest.php
 *
 * Copyright (C) 2022  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use AspectMock\Test as mock;

class ParticipantTest extends \Codeception\Test\Unit {

    private $testSurvey = null;

    protected function _before() {
        MomentTestUtil::startUp();
        MomentTestUtil::authenticate('user1');
        MomentTestUtil::initSurvey();

        $this->testSurvey = Survey::fromId('survey1');
    }

    protected function _after() {
        unset($_GET['participant_token']);
        unset($_SERVER['HTTP_X_PARTICIPANT_TOKEN']);
        mock::clean();
    }

    public function testGetParticipantUltimate() {
        $pMock = mock::double('Participant', [
            'getUltimateParticipant' => true
        ]);

        $p = Participant::getParticipant($this->testSurvey, [
            'participant' => (object) [
                'type' => ParticipantTypes::ULTIMATE
            ]
        ]);

        $pMock->verifyInvoked('getUltimateParticipant');
        // we mocked getUltimateParticipant to return true
        $this->assertTrue($p);

    }

    public function testGetParticipantNotAllowed() {
        mock::double('Survey', [
            '__get' => function($property) {
                if('reply_access' === $property)
                    return Survey::OPENED_TO_AUTHENTICATED;

                return __AM_CONTINUE__;
            }
        ]);
        mock::double('Auth', [
           'user' => false
        ]);

        $this->expectException('ParticipantNotFoundException');
        Participant::getParticipant($this->testSurvey, []);
    }

    public function testGetParticipantWithTokenInData() {
        mock::double('Survey', [
            '__get' => function($property) {
                if('reply_access' === $property)
                    return Survey::OPENED_TO_EVERYONE;

                return __AM_CONTINUE__;
            }
        ]);

        $pMock = mock::double('Participant', [
            'fromSurveyAndDeclarativeUser' => true
        ]);

        $p = Participant::getParticipant($this->testSurvey, [
            'participant' => (object) [
                'participant_token' => 'this is a token'
            ]
        ]);

        // we mocked fromSurveyAndDeclarativeUser to return true
        $this->assertTrue($p);
        $pMock->verifyInvoked('fromSurveyAndDeclarativeUser');
    }

    public function testGetParticipantWithTokenInServer() {
        mock::double('Survey', [
            '__get' => function($property) {
                if('reply_access' === $property)
                    return Survey::OPENED_TO_EVERYONE;

                return __AM_CONTINUE__;
            }
        ]);

        $pMock = mock::double('Participant', [
            'fromSurveyAndDeclarativeUser' => true
        ]);

        $_SERVER['HTTP_X_PARTICIPANT_TOKEN'] = 'this is a token';

        $p = Participant::getParticipant($this->testSurvey, [
            'participant' => (object) []
        ]);

        // we mocked fromSurveyAndDeclarativeUser to return true
        $this->assertTrue($p);
        $pMock->verifyInvoked('fromSurveyAndDeclarativeUser');
    }

    public function testGetParticipantWithTokenInGet() {
        mock::double('Survey', [
            '__get' => function($property) {
                if('reply_access' === $property)
                    return Survey::OPENED_TO_EVERYONE;

                return __AM_CONTINUE__;
            }
        ]);

        $participants = ['p1', 'p2'];
        $pMock = mock::double('Participant', [
            'all' => $participants
        ]);

        $_GET['participant_token'] = 'this is a token';

        $p = Participant::getParticipant($this->testSurvey, [
            'participant' => (object) []
        ]);

        // we mocked fromSurveyAndDeclarativeUser to return 'p2''
        $this->assertEquals('p2', $p);
        $pMock->verifyInvoked('fromSurveyAndDeclarativeUser');
    }

    public function testGetParticipantWithInvalidTokenInGet() {
        mock::double('Survey', [
            '__get' => function($property) {
                if('reply_access' === $property)
                    return Survey::OPENED_TO_EVERYONE;

                return __AM_CONTINUE__;
            }
        ]);

        //No participant matching this token
        $participants = [];
        $pMock = mock::double('Participant', [
            'all' => $participants
        ]);

        $_GET['participant_token'] = 'this is a token';

        $this->expectException('ParticipantInvalidTokenException');
        $p = Participant::getParticipant($this->testSurvey, [
            'participant' => (object) []
        ]);
    }

    public function testGetParticipantNewNotAuth() {
        mock::double('Survey', [
            '__get' => function($property) {
                if('reply_access' === $property)
                    return Survey::OPENED_TO_EVERYONE;

                if('enable_anonymous_answer' === $property)
                    return true;

                return __AM_CONTINUE__;
            }
        ]);

        mock::double('Auth', [
            'user' => false
        ]);

        $pMock = mock::double('Participant', [
            'createDeclarativeParticipant' => true
        ]);


        $p = Participant::getParticipant($this->testSurvey, [], true);

        // we mocked fromSurveyAndDeclarativeUser to return true
        $this->assertTrue($p);
        $pMock->verifyInvoked('createDeclarativeParticipant');
    }

    public function testHasAnswered() {
        mock::double('Participant', [
            'getParticipant' => (object) [
                'answers_created' => true
            ]
        ]);

        $this->assertTrue(Participant::hasAnswered($this->testSurvey, []));

        mock::clean();
        mock::double('Participant', [
            'getParticipant' => function($survey, $data, $create_when_needed) {
                throw new ParticipantNotFoundException(null);
            }
        ]);

        $this->assertFalse(Participant::hasAnswered($this->testSurvey, []));
    }


    public function testAuthenticatedUserHasAnswered() {
        mock::double('DBIStatement', ['fetchColumn' => 1]);

        $this->assertTrue(Participant::authenticatedUserHasAnswered($this->testSurvey));

        MomentTestUtil::authenticate('user1');
        mock::double('DBIStatement', ['fetchColumn' => 0]);

        $this->assertFalse(Participant::authenticatedUserHasAnswered($this->testSurvey));
    }

    public function testFromSurveyId() {
        $participants = ['p1', 'p2'];
        mock::double('Participant', [
            'all' => $participants
        ]);

        $this->assertEquals($participants, Participant::fromSurveyId('survey_id'));
    }

    public function testFromIdOnly() {
        $participants = ['p1', 'p2'];
        mock::double('Participant', [
            'all' => $participants
        ]);
        // We take the last participant
        $this->assertEquals('p2', Participant::fromIdOnly('participant_id', ParticipantTypes::DECLARATIVE));
    }

    public function testFromIdOnlyNoParticipant() {
        $participants = [];
        mock::double('Participant', [
            'all' => $participants
        ]);
        // We no participant with this id
        $this->assertEquals(null, Participant::fromIdOnly('participant_id', ParticipantTypes::DECLARATIVE));
    }

    public function testFromSurveyAndEmail() {
        $participants = ['p1', 'p2'];
        mock::double('Participant', [
            'all' => $participants
        ]);

        // We take the last participant
        $this->assertEquals('p2', Participant::fromSurveyAndEmail($this->testSurvey, 'an_email'));
    }

    public function testFromSurveyAndEmailNotFound() {
        $participants = [];
        mock::double('Participant', [
            'all' => $participants
        ]);

        $this->expectException('ParticipantNotFoundException');
        Participant::fromSurveyAndEmail($this->testSurvey, 'an_email');
    }

    public function testFromSurveyAndDeclarativeEmail() {
        $participants = ['p1', 'p2'];
        mock::double('Participant', [
            'all' => $participants
        ]);

        $this->assertEquals('p2', Participant::fromSurveyAndDeclarativeEmail($this->testSurvey, 'an_email'));
    }

    public function testCast() {
        $participant = MomentTestUtil::initParticipant('survey1');

        //We cast as the most rightful user (Owner and participant)
        $cast = Participant::cast($participant, ['role' => SurveyUserRoles::OWNER, 'can' => ['view_answers' => true], 'email' => $participant->email]);

        $this->assertArrayHasKey('id', $cast);
        $this->assertArrayHasKey('participant_hash', $cast);
        $this->assertArrayHasKey('type', $cast);
        $this->assertEquals($participant->type, $cast['type']);
        $this->assertArrayHasKey('has_email', $cast);
        $this->assertTrue($cast['has_email']);
        $this->assertArrayHasKey('name', $cast);
        $this->assertEquals($participant->name, $cast['name']);
        $this->assertArrayHasKey('email', $cast);
        $this->assertEquals($participant->email, $cast['email']);
        $this->assertArrayHasKey('answers', $cast);
        $this->assertArrayHasKey('participant_token', $cast);
        $this->assertEquals($participant->uid, $cast['participant_token']);
    }

    public function testCastCollection() {
        $participant = MomentTestUtil::initParticipant('survey1');

        mock::double('Participant', [
            'cast' => 'testCast'
        ]);

        $casts = Participant::castCollection([$participant], ['role' => SurveyUserRoles::OWNER, 'can' => ['view_answers' => true], 'email' => $participant->email]);

        //castCollection should just call cast on each participant
        foreach($casts as $cast) {
            $this->assertEquals('testCast', $cast);
        }
    }

    public function testUpdate() {
        $participant = MomentTestUtil::initParticipant('survey1');

        mock::double('Participant', [
            'cast' => 'testCast'
        ]);

        $participant->update(['name' => 'New name']);

        $this->assertEquals('New name', $participant->name);
    }
}
