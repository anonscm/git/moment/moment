<?php
/**
 *     Moment - QuesitonTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use AspectMock\Test as mock;

class QuestionTest extends \Codeception\Test\Unit {

    private $testSurvey = null;

    private $testQuestionData = array(
        "id" => 'question_unique',
        "title" => "Question 1",
        "type" => "text",
        "position" => 0,
        "options" => array(
            "force_unique_choice" => 1,
            "enable_maybe_choices" => 0
        ),
        "propositions" => array(
            array(
                "id" => 1,
                "type" => "text",
                "position" => 1,
                "header" => "Proposition 2",
                "text" => ""
            ),
            array(
                "id" => 2,
                "type" => "text",
                "position" => 0,
                "header" => "Proposition 1",
                "text" => ""
            )
        )
    );

    protected function _before() {
        MomentTestUtil::startUp();
        $this->testSurvey = MomentTestUtil::initSurvey('survey1');
    }

    protected function _after() {
        mock::clean();
    }

    /**
     * Test for create() :
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function testCreate() {

        $question = Question::create($this->testSurvey, $this->testQuestionData);

        $this->assertEquals($this->testQuestionData['title'], $question->title);
        $this->assertCount(count($this->testQuestionData['propositions']), $question->propositions);
    }

    /**
     * Test for __get() : property
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function test__get() {

        $question = Question::create($this->testSurvey, $this->testQuestionData);

        $this->assertEquals($this->testQuestionData['title'], $question->__get('title'));
        $this->assertEquals($this->testQuestionData['type'], $question->__get('type'));
        $this->assertEquals($this->testQuestionData['position'], $question->__get('position'));
        $this->assertEquals($this->testQuestionData['options'], $question->__get('options'));

        //Statement is always set to title when given
        $this->assertEquals($this->testQuestionData['title'], $question->__get('statement'));

        //Question is a unique choice
        $this->assertEquals(QuestionAnswerTypes::UNIQUE, $question->__get('answer_type'));

        //proposition type
        $this->assertEquals(PropositionTypes::TEXT, $question->__get('proposition_type'));
        //with local cache
        $this->assertEquals(PropositionTypes::TEXT, $question->__get('proposition_type'));

        //Input mode is not defined by default
        $this->assertFalse($question->__get('input_mode'));

        //propositions
        $propositions = $question->__get('propositions');
        foreach ($propositions as $index => $proposition) {
            $this->assertEquals($this->testQuestionData['propositions'][$index]['header'], $proposition->header);
        }

        //sorted_propositions
        $propositions = $question->__get('sorted_propositions');
        foreach ($propositions as $proposition) {
            $this->assertEquals($this->testQuestionData['propositions'][$proposition->position]['header'], $proposition->header);
        }

        //grouped_propositions
        $proposition_groups = $question->__get('grouped_propositions');
        foreach ($proposition_groups as $header => $group) {
            $this->assertEquals(1, count($group));
        }

        //answers, nothing to check, there is no answers
        $question->__get('Answers');

        mock::double('DBIStatement', [
            'fetchColumn' => 12
        ]);
        $this->assertEquals(12, $question->__get('answers_count'));

        //Specific options
        $this->assertEquals($this->testQuestionData['options']['force_unique_choice'], $question->__get('force_unique_choice'));
        $this->assertEquals($this->testQuestionData['options']['enable_maybe_choices'], $question->__get('enable_maybe_choices'));

        $question->__set('has_maybe_answers', true);
        $this->assertTrue($question->__get('has_maybe_answers'));
    }

    public function test__getAnswers() {

        $question = Question::create($this->testSurvey, $this->testQuestionData);

        global $called;
        $called = 0;

        mock::double('Entity', [
            '__get' => function($property) {
                global $called;
                if('Answers' == $property) {
                    //Trick to fool phpstorm inspection
                    $classname = Answer::class;
                    return [
                        mock::double(new $classname, []),
                        mock::double(new $classname, [])
                    ];
                }
                //One authenticated and the one ultimate
                if('Participant' == $property) {
                    $type = ParticipantTypes::AUTHENTICATED;
                    if($called++ > 0) {
                        $type = ParticipantTypes::ULTIMATE;
                    }
                    //Trick to fool phpstorm inspection
                    $classname = Participant::class;
                    return mock::double(new $classname, ['__get' => $type]);
                }

                return __AM_CONTINUE__;
            }
        ]);

        $answers = $question->__get('Answers');

        //We should only have the authenticated answer
        $this->assertCount(1, $answers);
    }

    public function test__getMixedPropositionType() {
        $question = Question::create($this->testSurvey, $this->testQuestionData);

        global $called;
        $called = 0;

        mock::double('TextProposition', [
            '__get' => function($property) {
                global $called;
                if('type' == $property) {
                    $type = PropositionTypes::TEXT;
                    if($called++ > 0) {
                        $type = PropositionTypes::DAY;
                    }
                    return $type;
                }

                return __AM_CONTINUE__;
            }
        ]);


        $this->assertEquals(PropositionTypes::MIXED, $question->__get('proposition_type'));
    }

    /**
     * Test for __set() : Properties
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function test__set() {

        $question = Question::create($this->testSurvey, $this->testQuestionData);

        $question->__set('title', 'New question title');
        $this->assertEquals('New question title', $question->__get('title'));
        $question->__set('type', 'date');
        $this->assertEquals('date', $question->__get('type'));
        $question->__set('position', 1);
        $this->assertEquals(1, $question->__get('position'));
        $question->__set('options', []);
        $this->assertEquals([], $question->__get('options'));

    }


    /**
     * Test for create() that fails with no type set
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function testCreateFailsWithoutType() {
        $this->expectException('QuestionMissingPropertyException');
        $newQuestionData = $this->testQuestionData;
        //type is mandatory
        unset($newQuestionData['type']);

        Question::create($this->testSurvey, $newQuestionData);
    }

    /**
     * Test for create() that fails with no position set
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function testCreateFailsWithoutPosition() {
        $this->expectException('QuestionMissingPropertyException');
        $newQuestionData = $this->testQuestionData;
        //position is mandatory
        unset($newQuestionData['position']);

        Question::create($this->testSurvey, $newQuestionData);
    }

    /**
     * Test for create() that fails with no options set
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function testCreateFailsWithoutOptions() {
        $this->expectException('QuestionMissingPropertyException');
        $newQuestionData = $this->testQuestionData;
        //options is mandatory
        unset($newQuestionData['options']);

        Question::create($this->testSurvey, $newQuestionData);
    }

    /**
     * Test for create() that fails with no propositions set
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function testCreateFailsWithoutPropositions() {
        $this->expectException('QuestionMissingPropertyException');
        $newQuestionData = $this->testQuestionData;
        //propositions is mandatory
        unset($newQuestionData['propositions']);

        Question::create($this->testSurvey, $newQuestionData);
    }

    /**
     * Test for create() that fails with invalid type
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function testCreateFailsWithInvalidType() {
        $this->expectException('QuestionBadTypeException');
        $newQuestionData = $this->testQuestionData;
        //type is mandatory
        $newQuestionData['type'] = 'an_invalid_type';

        Question::create($this->testSurvey, $newQuestionData);
    }

    /**
     * Test for create() that fails with invalid survey
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function testCreateFailsWithInvalidSurvey() {
        $this->expectException('QuestionSurveyNotSavedException');

        mock::double('Survey', [
            '__get' => function($property) {
                if('storedInDatabase' == $property) {
                    return false;
                }
                return __AM_CONTINUE__;
            }
        ]);

        Question::create($this->testSurvey, $this->testQuestionData);
    }

    /**
     * Test for create() that fails with invalid proposition array
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function testCreateFailsWithInvalidPropositionsArray() {
        $this->expectException('QuestionBadPropertyException');

        $newQuestionData = $this->testQuestionData;
        //propositions should be an array
        $newQuestionData['propositions'] = 'an_invalid_value';

        Question::create($this->testSurvey, $newQuestionData);
    }

    /**
     * Test for update() :
     * @throws BadFormatException
     * @throws ModelViolationException
     * @throws NotFoundException
     * @throws PropertyAccessException
     */
    public function testUpdate() {

        $question = Question::create($this->testSurvey, $this->testQuestionData);

        $newQuestionData = Question::cast($question, false);

        $newQuestionData['title'] = 'This is the new title';

        $newQuestionData['options']['force_unique_choice'] = 1;

        $newQuestionData['options'] = (object)$newQuestionData['options'];

        $proposition = array_pop($newQuestionData['propositions']);
        //We try to re-create the exact same proposition as Proposition 1
        $newQuestionData['propositions'][] = array(
            "type" => "text",
            "position" => 0,
            "header" => "Proposition 1",
            "text" => ""
        );

        $newQuestionData['propositions'][] = array(
            "type" => "text",
            "position" => 0,
            "header" => "Proposition 3",
            "text" => "aaa"
        );

        mock::double('Question', [
            '__get' => function($property) {
                if('answer_type' == $property) {
                    return QuestionAnswerTypes::MULTIPLE;
                }
                if('storedInDatabase' == $property) { return true; }
                return __AM_CONTINUE__;
            }
        ]);

        $fakeProposition = mock::double('TextProposition')->make();
        $ct = 0;
        mock::double('TextProposition', [
            'fromValues' => function() use(&$ct, $fakeProposition) {
                if($ct++== 0)
                    return $fakeProposition;

                throw new PropositionNotFoundException();
            }
        ]);

        $question->update($this->testSurvey, $newQuestionData);

        $this->assertEquals('This is the new title', $question->__get('title'));

        $expectedPropositionHeader = ['Proposition 1' => 0,'Proposition 2' => 0, 'Proposition 3' => 0];
        foreach($question->__get('propositions') as $proposition) {
            $expectedPropositionHeader[$proposition->header]++;
        }
        // There should only be 1 proposition 1 etc
        $this->assertEquals(['Proposition 1' => 1,'Proposition 2' => 1, 'Proposition 3' => 1], $expectedPropositionHeader);
    }

    /**
     * Test get Questions from Survey
     * @throws Exception
     */
    public function testFromSurvey() {

        mock::double('Question', ['all' => $this->testSurvey->Questions]);

        $questions = Question::fromSurvey($this->testSurvey);

        $this->assertCount(count($this->testSurvey->Questions), $questions);

        foreach ($this->testSurvey->Questions as $question) {
            $found = false;
            foreach($questions as $q) {
                if($q->id == $question->id) {
                    $found = true;
                    break;
                }
            }
            $this->assertTrue($found);
        }

    }

    /**
     * Test comparison function
     */
    public function testCompare() {

        $question_a = [
            'position' => 1
        ];

        $question_b = [
            'position' => 0
        ];

        // B is smaller than A => Question::compare should be positive
        $this->assertGreaterThan(0 , Question::compare($question_a, $question_b));

        $question_a['position'] = 0;
        $question_b['position'] = 1;

        // B is greater than A => Question::compare should be negative
        $this->assertLessThan(0 , Question::compare($question_a, $question_b));

        $question_a['position'] = 0;
        $question_b['position'] = 0;

        // B is equals than A => Question::compare should be 0
        $this->assertEquals(0 , Question::compare($question_a, $question_b));
    }

    /**
     * Test beforeDelete
     */
    public function testBeforeDelete() {

        $question = Question::create($this->testSurvey, $this->testQuestionData);

        $mockEntity = mock::double('Entity', []);
        $mockAnswer = mock::double('Answer', []);

        mock::double('Question', [
            '__get' => function($property) {
                if('Answers' == $property) {
                    //Trick to fool phpstorm inspection
                    $classname = Answer::class;
                    $answer = mock::double(new $classname, ['delete' => true]);
                    return [$answer];
                }

                return __AM_CONTINUE__;
            }
        ]);

        $question->beforeDelete();

        //Propositions should have been deleted
        $mockEntity->verifyInvokedMultipleTimes('delete', count($this->testQuestionData['propositions']));

        //The fake answer should have been deleted too
        $mockAnswer->verifyInvokedMultipleTimes('delete', 1);

    }

    /**
     * Test applyConstraint on a question
     * @throws QuestionBadPropertyException
     * @throws QuestionBadTypeException
     * @throws QuestionMissingPropertyException
     * @throws QuestionSurveyNotSavedException
     */
    public function testApplyConstraints() {

        $question = Question::create($this->testSurvey, $this->testQuestionData);

        $mock = mock::double('Proposition', ['applyConstraints' => true]);

        $question->applyConstraints();

        //applyConstraints should have been called on all propositions
        $mock->verifyInvokedMultipleTimes('applyConstraints', count($this->testQuestionData['propositions']));
    }

    /**
     * Test hasConstraint on a question
     * @throws QuestionBadPropertyException
     * @throws QuestionBadTypeException
     * @throws QuestionMissingPropertyException
     * @throws QuestionSurveyNotSavedException
     */
    public function testHasConstraints() {

        $question = Question::create($this->testSurvey, $this->testQuestionData);

        $mock = mock::double('Proposition', ['__get' => function($property) {
            if($property === 'options') {
                return ['constraints' => [['type' => 'limit_choice', 'value' => 1], ['type' => 'an_other_type', 'value' => 1]]];
            }
        }]);

        $this->assertTrue($question->hasConstraints('limit_choice'));
        $this->assertFalse($question->hasConstraints('an_unknown_type'));

    }

    /**
     * Test applyConstraint on a question
     * @throws QuestionBadPropertyException
     * @throws QuestionBadTypeException
     * @throws QuestionMissingPropertyException
     * @throws QuestionSurveyNotSavedException
     */
    public function testCast() {

        $question = Question::create($this->testSurvey, $this->testQuestionData);


        $casted = Question::cast($question, true);


        $this->assertEquals($question->id, $casted['id']);
        $this->assertEquals($question->title, $casted['title']);
        $this->assertEquals($question->type, $casted['type']);

        $this->assertCount(count($question->propositions), $casted['propositions']);


        //Cast must throw a modelviolation if there is no proposition
        $this->expectException(ModelViolationException::class);

        mock::double('Question', ['__get' => function($property) {
            if('grouped_propositions' == $property) {
                return array();
            }
            return __AM_CONTINUE__;
        }]);

        Question::cast($question, true);

    }

    public function testUpdateCrossSurvey() {

        $question = Question::create($this->testSurvey, $this->testQuestionData);

        $otherSurvey = MomentTestUtil::initSurvey('survey2');

        $this->expectException(QuestionCrossAccessException::class);
        $question->update($otherSurvey, array());

    }

    public function testUpdateMissingMandatory() {

        $question = Question::create($this->testSurvey, $this->testQuestionData);

        $this->expectException(QuestionMissingPropertyException::class);
        $question->update($this->testSurvey, array());

    }

    public function testUpdateMissingPropositions() {

        $question = Question::create($this->testSurvey, $this->testQuestionData);

        $this->expectException(QuestionMissingPropertyException::class);
        $question->update($this->testSurvey, array('title' => 'New title', 'options' => array(), 'position' => 1));

    }

    public function testUpdateBadPropositions() {

        $question = Question::create($this->testSurvey, $this->testQuestionData);

        $this->expectException(QuestionBadPropertyException::class);
        $question->update($this->testSurvey, array('title' => 'New title', 'options' => array(), 'position' => 1, 'propositions' => 12));

    }


}
