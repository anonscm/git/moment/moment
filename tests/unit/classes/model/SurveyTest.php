<?php
/**
 *     Moment - CalendarTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// include_once __DIR__ . '/../../src/includes/core/init.php';

use AspectMock\Test as mock;

class SurveyTest extends \Codeception\Test\Unit {

    private $testUser = null;

    protected function _before() {
        MomentTestUtil::startUp();

        MomentTestUtil::authenticate('user1');

        MomentTestUtil::$goodSurveyData['settings']['auto_close'] = time() + 10 * 86400;

        mock::double('Owner', [
            'create' => true
        ]);
    }

    protected function _after() {
        mock::clean();
    }

    public function testCreate() {

        //Duplicating guests (to check deduplication)
        MomentTestUtil::$goodSurveyData['guests'][] = MomentTestUtil::$goodSurveyData['guests'][0];
        mock::clean('Owner');
        $survey = Survey::create((array)json_decode(json_encode(MomentTestUtil::$goodSurveyData)));

        $this->assertTrue($survey->storedInDatabase);
        $this->assertTrue($survey->id !== null);

        $this->assertEquals(MomentTestUtil::$goodSurveyData['title'], $survey->title);
        $this->assertEquals(MomentTestUtil::$goodSurveyData['place'], $survey->place);
        $this->assertEquals(MomentTestUtil::$goodSurveyData['description'], $survey->raw_description);
        $this->assertEquals(count(MomentTestUtil::$goodSurveyData['settings']), count((array)$survey->settings));
        foreach ((array)$survey->settings as $key => $value) {
            $this->assertEquals(MomentTestUtil::$goodSurveyData['settings'][$key], $value);
        }
        $this->assertEquals(2, count($survey->Questions));

        mock::double('Survey', ['getHumanReadableId' => 'this-is-a-human-readable-id']);
        mock::double('Config', ['get' => function($property) {
            if('application_url' === $property) {
                return 'https://moment.com/';
            }
            return __AM_CONTINUE__;
        }]);
        $this->assertEquals('https://moment.com/survey/this-is-a-human-readable-id', $survey->shortened_path);
        $this->assertEquals('this-is-a-human-readable-id', $survey->human_readable_id);

        mock::double('Owner', ['fromSurvey' => array(Owner::create($survey, ['name' => 'Test', 'email' => 'test+owner@mail.com']))]);

        $this->assertEquals('Test', $survey->owner_names);
        $this->assertEquals('test+owner@mail.com', $survey->owner_emails);

        $this->assertEquals(true, $survey->has_timezone_sensible_questions);

        $survey->delete();

    }

    public function testCreateNoTitle() {
        $this->createMissingProperty('title');
    }

    public function testCreateNoValidGuests() {
        $this->createBadProperty('guests');
    }

    public function testCreateNoQuestions() {
        $this->createMissingProperty('questions');
    }

    public function testCreateNoValidQuestions() {
        $this->createBadProperty('questions');
    }

    public function testCreateNoValidOwners() {
        $this->createBadProperty('owners');
    }

    public function testGenerateUID() {
        mock::clean('KeyGenerator');

        mock::double('DBIStatement', ['fetch' => false]);

        $survey = Survey::create((array)json_decode(json_encode(MomentTestUtil::$goodSurveyData)));

        $this->assertTrue($survey->storedInDatabase);
        $this->assertTrue($survey->id !== null);
    }

    public function testAllSurveys() {

        mock::double('Survey', [
            'all' => ['test']
        ]);

        mock::double('Auth', [
            'isAdmin' => true
        ]);


        $this->assertEquals(['test'], Survey::allSurveys());

        mock::double('Auth', [
            'isAdmin' => false
        ]);

        //Only admin can get allsurveys
        $this->expectException('AuthUserNotAllowedException');
        Survey::allSurveys();
    }

    public function testOwnedBy() {
        mock::double('Owner', [
            'fromEmail' => [(object)[
                'Survey' => 'this_is_a_survey'
            ]]
        ]);

        $this->testUser = User::create('toto@renater.fr');

        $this->assertEquals(['this_is_a_survey'], Survey::ownedBy($this->testUser));
    }

    public function testFromHumanReadableId() {
        mock::double('Survey', [
            'fromId' => 'this_is_a_survey'
        ]);

        $this->assertEquals('this_is_a_survey', Survey::fromHumanReadableId('this-is-a-human-readable-id-xxxxxxxx'));
    }

    public function testFromHumanReadableIdNotValidID() {
        mock::double('Survey', [
            'fromId' => 'this_is_a_survey'
        ]);

        $this->expectException('NotFoundException');

        $this->assertEquals('this_is_a_survey', Survey::fromHumanReadableId('this-is-a-non-valid-readable-id-xxxxx'));
    }

    public function testFromGuestUser() {
        mock::double('Guest', [
            'fromEmail' => [(object)[
                'Survey' => 'this_is_a_survey'
            ]]
        ]);

        $this->testUser = User::create('toto@renater.fr');

        $this->assertEquals(['this_is_a_survey'], Survey::fromGuestUser($this->testUser));
    }

    public function testAnswerBy() {
        mock::double('Participant', [
            'fromAuthenticatedUser' => [
                (object)[
                    'Survey' => 'this_is_a_survey',
                    'answers_updated' => '2',
                    'answers_created' => '1'
                ],
                (object)[
                    'Survey' => 'this_is_a_another_survey',
                    'answers_updated' => '3',
                    'answers_created' => '1'
                ]
            ]
        ]);

        $this->testUser = User::create('toto@renater.fr');

        //Checking sort : from newer participation to older
        $this->assertEquals(['this_is_a_another_survey', 'this_is_a_survey'], Survey::answeredBy($this->testUser));
    }

    public function testCompare() {
        $surveyA =(object)[
            'updated' => '2'
        ];

        $surveyB =(object)[
            'updated' => '1'
        ];

        $this->assertEquals(-1, Survey::compare($surveyA, $surveyB));
        $this->assertEquals(1, Survey::compare($surveyB, $surveyA));

        //Equality
        $surveyB->updated = '2';
        $this->assertEquals(-1, Survey::compare($surveyA, $surveyB));

    }

    public function testUpdate() {

        $survey = Survey::create((array)json_decode(json_encode(MomentTestUtil::$goodSurveyData)));

        $data = MomentTestUtil::$goodSurveyData;

        $data['title'] = 'new_title';

        mock::double('Question', [
            'fromId' => $survey->Questions[0],
            'update' => true
        ]);

        mock::double('Guest', [
            'update' => true
        ]);

        $survey->update($data);

        $this->assertEquals($data['title'] , $survey->title);

    }

    public function testUpdateNoQuestions() {
        $this->updateMissingProperty('questions');
    }

    public function testUpdateNoValidQuestions() {
        $this->updateBadProperty('questions');
    }

    public function testUpdateNoValidGuests() {
        $this->updateBadProperty('guests');
    }

    public function testUpdateNoValidOwners() {
        $this->updateBadProperty('owners');
    }

    private function createMissingProperty($property) {
        $data = MomentTestUtil::$goodSurveyData;

        unset($data[$property]);

        $this->expectException('SurveyMissingPropertyException');

        Survey::create((array)json_decode(json_encode($data)));
    }

    private function createBadProperty($property) {
        $data = MomentTestUtil::$goodSurveyData;

        $data[$property] = true;

        $this->expectException('SurveyBadPropertyException');

        Survey::create((array)json_decode(json_encode($data)));
    }

    private function updateBadProperty($property) {
        $data = MomentTestUtil::$goodSurveyData;

        $survey = Survey::create((array)json_decode(json_encode($data)));

        mock::double('Question', [
            'fromId' => $survey->Questions[0],
            'update' => true
        ]);

        mock::double('Guest', [
            'update' => true
        ]);

        $data[$property] = true;

        $this->expectException('SurveyBadPropertyException');
        $survey->update($data);
    }

    private function updateMissingProperty($property) {
        $data = MomentTestUtil::$goodSurveyData;

        $survey = Survey::create((array)json_decode(json_encode($data)));

        mock::double('Question', [
            'fromId' => $survey->Questions[0],
            'update' => true
        ]);

        mock::double('Guest', [
            'update' => true
        ]);

        unset($data[$property]);

        $this->expectException('SurveyMissingPropertyException');
        $survey->update($data);
    }
}
