<?php
/**
 *     Moment - PreferenceTest.php
 *
 * Copyright (C) 2022  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use AspectMock\Test as mock;

class PreferenceTest extends \Codeception\Test\Unit {

    protected function _before() {
        MomentTestUtil::startUp();
        MomentTestUtil::authenticate('user1');
    }

    protected function _after() {
        mock::clean();
    }

    public function testCreate() {
        $user = MomentTestUtil::authenticate('user1');
        mock::double('Preference', ['save' => null]);

        $pref = Preference::create($user, 'pref1', 'value1');

        $this->assertEquals('pref1', $pref->name);
        $this->assertEquals(false, $pref->value);
        $this->assertEquals($user->id, $pref->User->id);

    }
}
