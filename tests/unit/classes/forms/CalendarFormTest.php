<?php
/**
 *     Moment - CalendarFormTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// include_once __DIR__ . '/../../src/includes/core/init.php'

use \AspectMock\Test as mock;

class CalendarFormTest extends \Codeception\Test\Unit {


    protected function _before() {
        mock::double('UserBase', ['createIfNotExists' => null]);
    }

    protected function _after() {
        mock::clean();
    }

    /**
     * test CalendarForm->getHTML
     */
    public function testGetHtml() {
        $html = (new CalendarForm())->getHTML();

        //Checking there are minimal fields
        $this->assertStringContainsString('name="id"', $html);
        $this->assertStringContainsString('name="url"', $html);
        $this->assertStringContainsString('data-form="calendar_form"', $html);
    }
}
