<?php
/**
 *     Moment - CalendarFormTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// include_once __DIR__ . '/../../src/includes/core/init.php'

use \AspectMock\Test as mock;

class ParticpantFormTest extends \Codeception\Test\Unit {


    protected function _before() {
        MomentTestUtil::startUp();
        MomentTestUtil::initSurvey('survey1');


    }

    protected function _after() {
        mock::clean();
    }

    /**
     * test ParticipantForm->getHTML
     */
    public function testGetHtml() {
        $survey = Survey::fromId('survey1');

        $html = (new ParticipantForm($survey))->getHTML();

        //Checking there are fields when no participant
        $this->assertStringContainsString('name="name"', $html);
        $this->assertStringContainsString('name="email"', $html);
        $this->assertStringContainsString('data-form="vote_create"', $html);
    }

    /**
     * test ParticipantForm->getHTML with participant
     */
    public function testGetHtmlWithParticipant() {
        $survey = Survey::fromId('survey1');
        $participant = MomentTestUtil::initParticipant('survey1');

        $html = (new ParticipantForm($survey, $participant))->getHTML();

        //Checking there are fields when participant is set
        $this->assertStringContainsString('name="name"', $html);
        $this->assertStringNotContainsString('name="email"', $html);
        $this->assertStringContainsString('data-form="vote_create"', $html);
    }
}
