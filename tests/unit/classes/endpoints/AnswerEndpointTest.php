<?php
/**
 *     Moment - AnswerEndpointTest.php
 *
 * Copyright (C) 2021  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use AspectMock\Test as mock;

class AnswerEndpointTest extends \Codeception\Test\Unit {

    private $participant = null;

    private $expected_answer = null;

    protected function _before() {
        MomentTestUtil::startUp();
        MomentTestUtil::authenticate();
        MomentTestUtil::initSurvey('survey1');

        $this->participant = MomentTestUtil::initParticipant('survey1');
        $answers = $this->participant->Answers;

        $this->expected_answer = array_shift($answers);
    }

    protected function _after() {
        mock::clean();
    }

    public function testGetNoId() {
        $this->expectException(RestNotAllowedException::class);
        AnswerEndpoint::get(null);
    }

    public function testGetExistingAnswer() {

        mock::double('Answer', ['fromId' => $this->expected_answer]);

        $answer = AnswerEndpoint::get(1);

        $this->assertEquals($this->expected_answer->id, $answer['answer_id']);
    }

    public function testGetComment() {

        mock::double('Answer', ['fromId' => $this->expected_answer]);

        $comment = AnswerEndpoint::get(1, 'comment');

        $this->assertEquals('This is a comment', $comment['content']);
    }

    public function testPutAnswer() {

        $new_data = [
            'question_id' => $this->expected_answer->Question->id,
            'choices' => [],
            'answer_id' => $this->expected_answer->id
        ];

        mock::double('RestRequest', ['getInput' => (object)$new_data]);

        $answer_mock = mock::double('Answer', ['update' => null]);

        $ret = AnswerEndpoint::put($this->expected_answer->id);

        $this->assertEquals('/survey/'.$this->participant->Survey->id.'/participant/'.$this->participant->id.'/answers', $ret['path']);

        //Answer::update must have been call only once
        $calls = $answer_mock->getCallsForMethod('update');
        $this->assertEquals(1, count($calls));

        foreach($calls as $call) {
            $this->assertEquals($call[0], $new_data);
        }

    }

    public function testPutAnswerEditionDisabled() {
        $new_data = [
            'question_id' => $this->expected_answer->Question->id,
            'choices' => [],
            'answer_id' => $this->expected_answer->id
        ];

        mock::double('Survey', ['__get' => function($property) {
            if('disable_answer_edition' === $property) {
                return true;
            }
            return __AM_CONTINUE__;
        }]);

        mock::double('RestRequest', ['getInput' => (object)$new_data]);

        $this->expectException(RestNotAllowedException::class);
        $ret = AnswerEndpoint::put($this->expected_answer->id);
    }

    public function testPutAnswerNotReplier() {
        $new_data = [
            'question_id' => $this->expected_answer->Question->id,
            'choices' => [],
            'answer_id' => $this->expected_answer->id
        ];

        mock::double('RestRequest', ['getInput' => (object)$new_data]);

        //User cannot answer this survey
        mock::double('SurveyUserUtil', ['getCurrentUser' => ['role' => SurveyUserRoles::NONE]]);

        $this->expectException(RestNotAllowedException::class);
        $ret = AnswerEndpoint::put($this->expected_answer->id);
    }

    public function testPutAnswerOwner() {

        $new_data = [
            'question_id' => $this->expected_answer->Question->id,
            'choices' => [],
            'answer_id' => $this->expected_answer->id
        ];

        mock::double('RestRequest', ['getInput' => (object)$new_data]);

        //User cannot answer this survey
        mock::double('SurveyUserUtil', ['getCurrentUser' => ['role' => SurveyUserRoles::OWNER]]);
        mock::double('Participant', ['hasAnswered' => false]);

        $this->expectException(RestNotAllowedException::class);
        $ret = AnswerEndpoint::put($this->expected_answer->id);
    }

    public function testPutAnswers() {

        $previous_answers_ids = [];
        foreach($this->participant->Answers as $answer) {
            $previous_answers_ids[] = $answer->id;
        }

        //Updating one, creating another
        $new_answers = [
            (object)[
            'question_id' => $this->expected_answer->Question->id,
            'choices' => [],
            'answer_id' => $this->expected_answer->id
            ],
            (object)[
                'question_id' => $this->expected_answer->Question->id,
                'choices' => []
            ]
        ];

        mock::double('Participant', ['__get' => function($property) {
            if($property == 'newly_created') {
                return false;
            }
            if($property == 'sorted_answers') {
                return [];
            }
            return __AM_CONTINUE__;
        }]);
        mock::double('RestRequest', ['getInput' => $new_answers]);

        $ret = AnswerEndpoint::put();

        //Checking structure
        $this->assertTrue(isset($ret['path']));
        $this->assertTrue(isset($ret['answers']));

        $new_ids = [];
        foreach($ret['answers'] as $answer) {
            $new_ids[] = $answer['answer_id'];
        }

        //No answers should have been deleted
        foreach($previous_answers_ids as $id) {
            $this->assertTrue(in_array($id, $new_ids));
        }

        //There should be a new answer
        $this->assertEquals(1, count($new_ids) - count($previous_answers_ids));
    }

    public function testPutAnswersEditionDisabled() {
        $new_data = [
            (object) [
                'question_id' => $this->expected_answer->Question->id,
                'choices' => [],
                'answer_id' => $this->expected_answer->id
            ]
        ];

        mock::double('Survey', ['__get' => function($property) {
            if('disable_answer_edition' === $property) {
                return true;
            }
            return __AM_CONTINUE__;
        }]);

        mock::double('RestRequest', ['getInput' => $new_data]);

        $this->expectException(RestNotAllowedException::class);
        $ret = AnswerEndpoint::put();
    }

    public function testDelete() {
        $this->expectException(RestNotAllowedException::class);
        AnswerEndpoint::delete();
    }

    public function testMaskComment() {
        $answer_mock = mock::double('Answer', ['maskComment' => null]);

        AnswerEndpoint::post($this->expected_answer->id, 'mask_comment');

        $calls = $answer_mock->getCallsForMethod('maskComment');

        //maskComment should have been called once
        $this->assertCount(1, $calls);
    }

    public function testUnmaskComment() {
        $answer_mock = mock::double('Answer', ['unmaskComment' => null]);

        AnswerEndpoint::post($this->expected_answer->id, 'unmask_comment');

        $calls = $answer_mock->getCallsForMethod('unmaskComment');

        //maskComment should have been called once
        $this->assertCount(1, $calls);
    }

    public function testUnknownAction() {
        //An unknown action
        $this->expectException(MomentRestNotAllowedException::class);
        AnswerEndpoint::post($this->expected_answer->id, 'unknown');
    }

    public function testNoAction() {
        //An unknown action
        $this->expectException(MomentRestNotAllowedException::class);
        AnswerEndpoint::post($this->expected_answer->id);
    }

    public function testActionNotOwner() {
        //An valid action
        $this->expectException(MomentRestNotAllowedException::class);

        mock::double('SurveyUserUtil', ['getCurrentUser' => ['role' => SurveyUserRoles::NONE]]);

        $this->expectException(MomentRestNotAllowedException::class);
        AnswerEndpoint::post($this->expected_answer->id, 'mask_comment');
    }

    public function testPost() {

        //Updating one, creating another
        $new_answers = [];

        foreach($this->participant->Survey->Questions as $question) {
            $choices = [];

            foreach($question->propositions as $proposition) {
                $currentChoice = YesNoMaybeValues::YES;

                if(count($choices) > 0 && QuestionAnswerTypes::UNIQUE == $question->answer_type) {
                    $currentChoice = YesNoMaybeValues::NO;
                }

                $choices[$proposition->id] = (object)[
                    'proposition_id' => $proposition->id,
                    'value' => $currentChoice,
                ];
            }

            $new_answers[$question->id] = (object)[
                'choices' => $choices,
                'question_id' => $question->id,
                'participant' => (object)[
                    'email' => 'test@renater.fr',
                    'name' => 'Test User'
                ]
            ];
        }

        $survey_mock = mock::double('Survey', ['sendNewParticipationNotification' => false]);

        mock::double('Auth', ['user' => false]);
        mock::double('Utilities', ['generateUID' => 'expected_token']);
        mock::double('Participant', ['fromSurveyAndDeclarativeEmail' => function() {
              throw new ParticipantNotFoundException(null);
        }]);
        mock::double('RestRequest', ['getInput' => $new_answers]);

        $ret = AnswerEndpoint::post();

        $participant_array = [
            'id' => 0
        ];
        $this->assertArrayHasKey('answers', $ret);
        foreach($ret['answers'] as $answer) {
            $this->assertArrayHasKey('participant', $answer);
            $this->assertArrayHasKey('question_id', $answer);
            $this->assertArrayHasKey('choices', $answer);
            $this->assertArrayHasKey('answer_id', $answer);

            $participant_array = $answer['participant'];

            if(isset($new_answers[$answer['question_id']])) {
                foreach($new_answers[$answer['question_id']]->choices as $proposition_id => $choice) {

                    foreach($answer['choices'] as $saved_choice) {
                        if($proposition_id = $saved_choice->proposition_id) {
                            $this->assertEquals($choice->value, $saved_choice->value);
                        }
                    }
                }
            }
        }

        //Check output consistency
        $this->assertEquals('/survey/'.$this->participant->Survey->id.'/participant/'.$participant_array['id'].'/answers', $ret['path']);

        //Owners should receive newparticipation notification
        $this->assertEquals(1, count($survey_mock->getCallsForMethod('sendNewParticipationNotification')));
    }

    public function testPostUltimateAnswer() {

        //Updating one, creating another
        $new_answers = [];

        //Setting ulatimate answer (choosing the first proposition)
        foreach($this->participant->Survey->Questions as $question) {
            $choices = [];

            foreach($question->propositions as $proposition) {
                $choices[$proposition->id] = (object)[
                    'proposition_id' => $proposition->id,
                    'value' => YesNoMaybeValues::YES
                ];
                break;
            }

            $new_answers[$question->id] = (object)[
                'choices' => $choices,
                'question_id' => $question->id,
                'participant' => (object)[
                    'type' => ParticipantTypes::ULTIMATE
                ]
            ];
        }

        MomentTestUtil::authenticate();

        mock::double('Participant', ['getUltimateParticipant' => function() {
            return static::createUltimateParticipant(MomentTestUtil::fromId('survey1'));
        }]);

        mock::double('RestRequest', ['getInput' => $new_answers]);

        $ret = AnswerEndpoint::post();

        //Survey should now be closed
        $this->assertTrue($this->participant->Survey->isClosed());
    }

    public function testPostUltimateAnswerAfterClose() {
        //Create a new fake survey
        $survey = MomentTestUtil::initSurvey('survey2');

        //Close the survey
        $survey->close();

        //Updating one, creating another
        $new_answers = [];

        //Setting ulatimate answer (choosing the first proposition)
        foreach($survey->Questions as $question) {
            $choices = [];

            foreach($question->propositions as $proposition) {
                $choices[$proposition->id] = (object)[
                    'proposition_id' => $proposition->id,
                    'value' => YesNoMaybeValues::YES
                ];
                break;
            }

            $new_answers[$question->id] = (object)[
                'choices' => $choices,
                'question_id' => $question->id,
                'participant' => (object)[
                    'type' => ParticipantTypes::ULTIMATE
                ]
            ];
        }

        $survey_mock = mock::double('Survey', ['sendFinalAnswersSelectedNotifications' => false]);

        mock::double('Participant', ['getUltimateParticipant' => function() {
            return static::createUltimateParticipant(MomentTestUtil::fromId('survey1'));
        }]);

        mock::double('RestRequest', ['getInput' => $new_answers]);

        $ret = AnswerEndpoint::post();

        //User should receive final answers selected notification
        $this->assertEquals(1, count($survey_mock->getCallsForMethod('sendFinalAnswersSelectedNotifications')));

    }

    public function testPostEmptyData() {

        mock::double('RestRequest', ['getInput' => array()]);


        $this->expectException(MomentRestNotAllowedException::class);
        AnswerEndpoint::post();

    }

    public function testPostSurveyNotFound() {

        //Updating one, creating another
        $new_answers = [];

        //Setting  answer (choosing the first proposition)
        foreach($this->participant->Survey->Questions as $question) {
            $choices = [];

            foreach($question->propositions as $proposition) {
                $choices[$proposition->id] = (object)[
                    'proposition_id' => $proposition->id,
                    'value' => YesNoMaybeValues::YES
                ];
                break;
            }

            //Here we set an unknown question id
            $new_answers[$question->id] = (object)[
                'choices' => $choices,
                'question_id' => $question->id
            ];
        }

        mock::double('RestRequest', ['getInput' => $new_answers]);

        mock::double('Question', ['fromId' => function() {
            throw new NotFoundException(null, null);
        }]);

        $this->expectException(SurveyNotFoundException::class);
        AnswerEndpoint::post();

    }

    public function testPostQuestionAnsweredTwice() {

        //Updating one, creating another
        $new_answers = [];

        //Setting  answer (choosing the first proposition)
        foreach($this->participant->Survey->Questions as $question) {
            $choices = [];

            foreach($question->propositions as $proposition) {
                $choices[$proposition->id] = (object)[
                    'proposition_id' => $proposition->id,
                    'value' => YesNoMaybeValues::YES
                ];
                break;
            }

            //Here we set an unknown question id
            $new_answers[$question->id] = (object)[
                'choices' => $choices,
                'question_id' => $question->id
            ];
        }

        mock::double('RestRequest', ['getInput' => $new_answers]);
        mock::double('Participant', ['fromSurveyAndAuthenticatedUser' => function() {
            throw new ParticipantNotFoundException(null);
        }]);

        mock::double('AnswerEndpoint', ['isAnswered' => true]);

        $this->expectException(ParticipantAlreadyAnsweredException::class);
        AnswerEndpoint::post();

    }

    public function testPostMissingAnswer() {

        //Updating one, creating another
        $new_answers = [];

        //Setting  answer (choosing the first proposition)
        foreach($this->participant->Survey->Questions as $question) {
            $choices = [];

            foreach($question->propositions as $proposition) {
                $choices[$proposition->id] = (object)[
                    'proposition_id' => $proposition->id,
                    'value' => YesNoMaybeValues::YES
                ];
                break;
            }

            //Here we set an unknown question id
            $new_answers[$question->id] = (object)[
                'choices' => $choices,
                'question_id' => $question->id
            ];

            //We set only one answer
            break;
        }

        mock::double('RestRequest', ['getInput' => $new_answers]);

        mock::double('Participant', ['fromSurveyAndAuthenticatedUser' => function() {
            throw new ParticipantNotFoundException(null);
        }]);

        $this->expectException(AnswerMissingException::class);
        AnswerEndpoint::post();

    }
}
