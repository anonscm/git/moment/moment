<?php
/**
 *     Moment - UserPreferencesEndpointTest.php
 *
 * Copyright (C) 2024  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use AspectMock\Test as mock;

class UserPreferencesEndpointTest extends \Codeception\Test\Unit {

    protected function _before() {
        MomentTestUtil::startUp();
    }

    protected function _after() {
        mock::clean();
    }

    public function testPutUnauthenticated() {
        mock::double('Auth', ['user' => false]);

        $this->expectException('RestNotAllowedException');
        UserPreferencesEndpoint::put();
    }

    public function testPut() {
        MomentTestUtil::startUp();
        MomentTestUtil::authenticate('user1');

        $new_data = [
            'settings.pref1' => false,
            'settings.new_pref' => 'test',
            'settings.non_existing' => 'value'
        ];

        mock::double('RestRequest', ['getInput' => (object)$new_data]);

        mock::double('UserPreferences', [
            'getSettings' => [
                'pref1' => [
                    'label' => 'label_pref1',
                    'type' => 'switch',
                    'default' => true
                ],
                'new_pref' => [
                    'label' => 'label_new_pref',
                    'type' => 'text',
                    'default' => ''
                ]
            ]
        ]);
        $user = MomentTestUtil::authenticate('user1');
        $pref = Preference::create($user, 'pref1', true);

        mock::double('Preference', ['save' => null]);

        mock::double('User', ['getPreference' => function($name) use($pref) {
            if($name === 'pref1')
                return $pref;

            return null;
        }]);

        UserPreferencesEndpoint::put();
    }

}
