<?php
/**
 *     Moment - CalendarConflictEndpointTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// include_once __DIR__ . '/../../src/includes/core/init.php';

use AspectMock\Test as mock;

class CalendarConflictEndpointTest extends \Codeception\Test\Unit {

    public static $testUser = null;

    public static $survey = null;

    public static $calendarEntity = null;

    protected function _before() {

        MomentTestUtil::startUp();
        MomentTestUtil::authenticate();
        MomentTestUtil::initSurvey('survey1');

        mock::double('Calendar', ['all' => [CalendarConflictEndpointTest::$calendarEntity]]);
        mock::double('CalendarManager', ['storeURLStatus' => true]);
        mock::double('CalendarManager', ['checkUrl' => ['code' => CalendarStatusCodes::OK, 'message' => CalendarStatusMessages::OK]]);


        CalendarConflictEndpointTest::$calendarEntity = Calendar::create(MomentTestUtil::fromId('user1'),
            [
                'url' => 'https://url.de.test',
                'name' => 'Test Calendar Name',
                'settings' => [
                    'color' => '#FFF'
                ],
                'status' => [],
                'checked' => null
            ]
        );
        CalendarConflictEndpointTest::$calendarEntity->__set('status', ['code' => CalendarStatusCodes::OK, 'message' => CalendarStatusMessages::OK]);
    }

    protected function _after() {
        mock::clean();
    }


    /**
     * testGetUnauthenticated test GET /CalendarConflict/<survey_id> while Unauthenticated
     */
    public function testGetUnauthenticated() {
        mock::double('Auth', ['user' => false]);

        $this->expectException('RestNotAllowedException');
        CalendarConflictEndpoint::get('survey1');
    }

    /**
     * testGetExistingSurvey test GET /CalendarConflict/<survey_id> while authenticated with a conflict in my cal
     */
    public function testGetExistingSurveyMyCal() {
        mock::double('Proposition', ['fromQuestion' => MomentTestUtil::all('DateProposition')]);

        mock::double('CalendarManager', ['getMyCalEvents' => [
            [
                'title' => 'Moment - question',
                'start_ts' => 1485324800,
                'end_ts' => 1485411200
            ]]]);
        mock::double('CalendarManager', ['getEvents' => [
            [
                'title' => '',
                'start_ts' => 1487324800,
                'end_ts' => 1487411200
            ],[
                'title' => '',
                'start_ts' => 1567503900,
                'end_ts' => 1567504800,
            ]]]);

        $conflicts = CalendarConflictEndpoint::get('survey1');

        $propositions = MomentTestUtil::all('DateProposition');
        $conflicting_pid = '';
        $conflicting_qid = '';
        foreach ($propositions as $proposition_id => $proposition) {
            if($proposition instanceof DateProposition) {
                if($proposition->base_day = 1485324800) {
                    $conflicting_qid = $proposition->Question->id;
                    $conflicting_pid = $proposition_id;
                    break;
                }
            }
        }

        $this->assertEquals([[
            'title' => 'Moment - question',
            'start_ts' => 1485324800,
            'end_ts' => 1485411200,
            'calendar' => [
                'id' => '@MyCal',
                'name' => Config::get('application_name'),
                'hash' => 'calendar_hash',
                'url' => MyCalUtil::getMyCalUrl(Auth::user()),
                'settings' => [
                    'color' => Config::get('calendar.mycal_color')
                ]
            ]
        ]], $conflicts[$conflicting_qid][$conflicting_pid]);
    }

    /**
     * testGetExistingSurvey test GET /CalendarConflict/<survey_id> while authenticated
     */
    public function testGetExistingSurvey() {
        mock::double('Proposition', ['fromQuestion' => MomentTestUtil::all('DateProposition')]);

        mock::double('CalendarManager', ['getMyCalEvents' => [
            [
                'title' => 'Moment - question',
                'start_ts' => 1487324800,
                'end_ts' => 1487411200
            ]]]);
        mock::double('CalendarManager', ['getEvents' => [
            [
                'title' => '',
                'start_ts' => 1485324800,
                'end_ts' => 1485411200
            ],[
                'title' => '',
                'start_ts' => 1567503900,
                'end_ts' => 1567504800,
            ]]]);

        $conflicts = CalendarConflictEndpoint::get('survey1');

        $propositions = MomentTestUtil::all('DateProposition');
        $conflicting_pid = '';
        $conflicting_qid = '';
        foreach ($propositions as $proposition_id => $proposition) {
            if($proposition instanceof DateProposition) {
                if($proposition->base_day = 1485324800) {
                    $conflicting_qid = $proposition->Question->id;
                    $conflicting_pid = $proposition_id;
                    break;
                }
            }
        }

        $this->assertEquals([[
            'title' => '',
            'start_ts' => 1485324800,
            'end_ts' => 1485411200,
            'calendar' => CalendarEndpoint::castLight(CalendarConflictEndpointTest::$calendarEntity)
        ]], $conflicts[$conflicting_qid][$conflicting_pid]);
    }

    /**
     * testGetExistingSurveyError test GET /CalendarConflict/<survey_id> while authenticated and no Calendar available
     */
    public function testGetExistingSurveyError() {
        mock::double('Proposition', ['fromQuestion' => MomentTestUtil::all('DateProposition')]);

        mock::double('CalendarManager', ['getMyCalEvents' => []]);
        mock::double('CalendarManager', ['getEvents' => []]);

        $cal = Calendar::create(MomentTestUtil::fromId('user1'),
            [
                'url' => 'https://url.de.test',
                'name' => 'Test Calendar Name',
                'settings' => [
                    'color' => '#FFF'
                ],
                'status' => ['code' => CalendarStatusCodes::ERROR],
                'checked' => null
            ]
        );

        $conflicts = CalendarConflictEndpoint::get('survey1');

        $this->assertEquals(1, count($conflicts));
    }

    /**
     * testGetConflictsOnNoRoleSurvey test GET /CalendarConflict/<survey_id> while not authorized to answer survey
     */
    public function testGetConflictsOnNoRoleSurvey() {
        mock::double('SurveyUserUtil', ['getCurrentUser' => ['role' => SurveyUserRoles::NONE]]);
        $this->expectException('RestNotAllowedException');
        CalendarConflictEndpoint::get('survey1');
    }

    /**
     * testGetNoSurveyId test GET /CalendarConflict/
     */
    public function testGetNoSurveyId() {
        $this->expectException('RestNotAllowedException');
        CalendarConflictEndpoint::get(null);
    }
}
