<?php
/**
 *     Moment - CalendarEndpointTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// include_once __DIR__ . '/../../src/includes/core/init.php';

use AspectMock\Test as mock;

class CalendarEndpointTest extends \Codeception\Test\Unit {

    public static $testUser = null;

    public static $calendarEntity = null;

    protected function _before() {
        MomentTestUtil::startUp();

        mock::double('CalendarManager', ['checkUrl' => ['code' => CalendarStatusCodes::OK, 'message' => CalendarStatusMessages::OK]]);

        CalendarEndpointTest::$testUser = User::create('toto@renater.fr');

        mock::double('Auth', ['user' => CalendarEndpointTest::$testUser]);

        CalendarEndpointTest::$calendarEntity = Calendar::create(CalendarEndpointTest::$testUser,
            [
                'url' => 'https://url.de.test',
                'name' => 'Test Calendar Name',
                'settings' => [
                    'color' => '#FFF'
                ],
                'status' => [],
                'checked' => null
            ]
        );

    }

    protected function _after() {
        mock::clean();
    }

    /**
     * Test for cast() : property
     */
    public function testCast() {

        $casted = CalendarEndpoint::cast(CalendarEndpointTest::$calendarEntity);
        $this->assertEquals($casted,
            [
                'id' => CalendarEndpointTest::$calendarEntity->id, //Calendar is not saved
                'url' => 'https://url.de.test',
                'name' => 'Test Calendar Name',
                'settings' => [
                    'color' => '#FFF'
                ],
                'status' => ['code' => CalendarStatusCodes::UNKNOWN, 'message' => CalendarStatusMessages::UNKNOWN],
                'checked' => null
            ]
        );
    }

    /**
     * test get /Calendar/@me that retrieves calendars of current user
     */
    public function testGetAtMe() {
        mock::double('UserBase',
            [
                '__get' => function($property) {
                    if($property === 'Calendars')
                        return [CalendarEndpointTest::$calendarEntity];

                    return __AM_CONTINUE__;
                }
            ]);

        $cal2 = CalendarEndpoint::get('@me');

        $this->assertEquals(CalendarEndpoint::cast(CalendarEndpointTest::$calendarEntity), array_shift($cal2));
    }

    /**
     * test get /Calendar/@MyCal that retrieves calendars of current user
     */
    public function testGetAtMyCal() {

        mock::double('CalendarManager', ['getMyCalEvents' => [
            [
                'title' => 'Moment - question',
                'start_ts' => 1487324800,
                'end_ts' => 1487411200
            ]]]);

        $cal2 = CalendarEndpoint::get('@MyCal');

        $this->assertEquals([
            'title' => 'Moment - question',
            'start_ts' => 1487324800,
            'end_ts' => 1487411200
        ], array_shift($cal2));
    }


    /**
     * test /Calendar/<id> that must retreive calender with Id
     */
    public function testGetById() {

        mock::double('Calendar',
            [
                'fromId' => CalendarEndpointTest::$calendarEntity,
            ]);

        mock::double('UserBase',
            [
                '__get' => [CalendarEndpointTest::$calendarEntity]
            ]);

        $cal2 = CalendarEndpoint::get(1);

        $this->assertEquals(CalendarEndpoint::cast(CalendarEndpointTest::$calendarEntity), $cal2);
    }

    /**
     * test /Calendar/<id>/events that retrieves events of specified calendar
     */
    public function testGetByIdEvents() {
        mock::double('Calendar',
            [
                '__get' => function($property) {
                    switch($property) {
                        case 'User' :
                            return CalendarEndpointTest::$testUser;
                        case 'status' :
                            return ['code' => CalendarStatusCodes::OK, 'message' => CalendarStatusMessages::OK];
                    }

                }
            ]);

        mock::double('Entity',
            [
                'fromId' => CalendarEndpointTest::$calendarEntity,
            ]);

        $eventsExpected = [
                    [
                    'title' => '',
                    'start_utc' => '2019-09-02T09:45:00',
                    'end_utc' => '2019-09-02T10:00:00',
                    'start' => '2019-09-02T11:45:00',
                    'end' => '2019-09-02T12:00:00',
                ],[
                    'title' => '',
                    'start_utc' => '2019-09-03T09:45:00',
                    'end_utc' => '2019-09-03T10:00:00',
                    'start' => '2019-09-03T11:45:00',
                    'end' => '2019-09-03T12:00:00',
                ]
            ];

        mock::double('CalendarManager',
            [
                'getEvents' => $eventsExpected
            ]);

        mock::double('UserBase',
            [
                '__get' => [CalendarEndpointTest::$calendarEntity]
            ]);

        $eventsActual = CalendarEndpoint::get(1, 'events');

        $this->assertEquals($eventsActual, $eventsExpected);
    }

    /**
     * test /Calendar/<id>/events that retrieves events of specified calendar but this calendar is not OK
     */
    public function testGetByIdEventsOnAKOCalendar() {
        mock::double('Calendar',
            [
                '__get' => function($property) {
                    switch($property) {
                        case 'User' :
                            return CalendarEndpointTest::$testUser;
                        case 'status' :
                            return ['code' => CalendarStatusCodes::ERROR, 'message' => CalendarStatusMessages::ERROR];
                    }

                }
            ]);

        mock::double('Entity',
            [
                'fromId' => CalendarEndpointTest::$calendarEntity,
            ]);

        $eventsActual = CalendarEndpoint::get(1, 'events');

        $this->assertEquals($eventsActual, []);
    }

    /**
     * test /Calendar/<id>/something that retrieves events of specified calendar
     */
    public function testGetByIdWithNonExistantSubResources() {
        mock::double('Entity',
            [
                'fromId' => CalendarEndpointTest::$calendarEntity,
                '__get' => CalendarEndpointTest::$testUser
            ]);

        $cal = CalendarEndpoint::get(1, 'somethingwrong');

        $this->assertEquals($cal, CalendarEndpoint::cast(CalendarEndpointTest::$calendarEntity));
    }

    /**
     * test Get on /Calendar/@me that must givre a default color if no color in settings
     */
    public function testGetAtMeNoColor() {

        mock::double('UserBase',
            [
                '__get' => function($property) {
                    if($property === 'Calendars')
                        return [CalendarEndpointTest::$calendarEntity];

                    return __AM_CONTINUE__;
                }
            ]);

        $cals = CalendarEndpoint::get('@me');

        //Check we get the correct color setting
        foreach($cals as $cal) {
            if(CalendarEndpointTest::$calendarEntity->id == $cal['id'])
                $this->assertEquals($cal['settings']['color'], '#FFF');
        }
    }

    /**
     * test Get on /Calendar that must fail because no id is given
     */
    public function testGetNoId() {
        $this->expectException('RestNotAllowedException');
        CalendarEndpoint::get();
    }

    /**
     * test Post on /Calendar that creates a Calendar Entity
     */
    public function testPost() {
        mock::double('RestRequest',
            [
                'getInput' => [
                    'url' => 'https://url.de.test',
                    'name' => 'Test Calendar Name',
                    'color' => '#FFF'
                ]
            ]);

        $cal_data = CalendarEndpoint::post();

        $this->assertEquals($cal_data['data']['url'], 'https://url.de.test');
        $this->assertEquals($cal_data['data']['name'], 'Test Calendar Name');
        $this->assertEquals($cal_data['data']['settings'], [
            'color' => '#FFF'
        ]);
    }


    /**
     * test Put on /Calendar that creates a Calendar Entity
     */
    public function testPut() {
        mock::double('RestRequest',
            [
                'getInput' => [
                    'url' => 'https://url.de.test2',
                    'name' => 'Test Calendar Name 2',
                    'color' => '#000'
                ]
            ]);

        mock::double('Entity',
            [
                'fromId' => CalendarEndpointTest::$calendarEntity,
                '__get' => CalendarEndpointTest::$testUser,
            ]);

        $cal_data = CalendarEndpoint::put(1);

        $this->assertEquals($cal_data['data']['url'], 'https://url.de.test2');
        $this->assertEquals($cal_data['data']['name'], 'Test Calendar Name 2');
        $this->assertEquals($cal_data['data']['settings'], [
            'color' => '#000'
        ]);
    }

    /**
     * test Put on /Calendar that must fail because no id is given
     */
    public function testPutNoId() {
        $this->expectException('RestNotAllowedException');
        CalendarEndpoint::put();
    }


    /**
     * test Delete on /Calendar that delete a Calendar Entity
     */
    public function testDelete() {
        $et = mock::double('Entity',
            [
                'fromId' => CalendarEndpointTest::$calendarEntity,
                '__get' => CalendarEndpointTest::$testUser,
                'delete' => true
            ]);

        CalendarEndpoint::delete(1);

        $et->verifyInvoked('delete');
    }


    /**
     * test Delete on /Calendar that must fail because no id is given
     */
    public function testDeleteNoId() {
        $this->expectException('RestNotAllowedException');
        CalendarEndpoint::delete();
    }

    /**
     * test /Calendar unauthenticated
     */
    public function testUnauthenticatedGet() {
        $this->mockUnauthenticated();
        $this->expectException('RestNotAllowedException');
        CalendarEndpoint::get(1);
    }

    /**
     * test /Calendar unauthenticated
     */
    public function testUnauthenticatedPost() {
        $this->mockUnauthenticated();
        $this->expectException('RestNotAllowedException');
        CalendarEndpoint::post();
    }

    /**
     * test /Calendar unauthenticated
     */
    public function testUnauthenticatedDelete() {
        $this->mockUnauthenticated();
        $this->expectException('RestNotAllowedException');
        CalendarEndpoint::delete(1);
    }

    /**
     * test /Calendar unauthenticated
     */
    public function testUnauthenticatedPut() {
        $this->mockUnauthenticated();
        $this->expectException('RestNotAllowedException');
        CalendarEndpoint::put(1);
    }

    private function mockUnauthenticated() {
        mock::double('Auth',
            [
                'user' => false,
            ]);
    }
}
