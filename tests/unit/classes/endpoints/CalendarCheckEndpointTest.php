<?php
/**
 *     Moment - CalendarCheckEndpointTest.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// include_once __DIR__ . '/../../src/includes/core/init.php';

use AspectMock\Test as mock;

class CalendarCheckEndpointTest extends \Codeception\Test\Unit {

    protected function _before() {
        mock::double('UserBase', ['createIfNotExists' => null]);

        CalendarEndpointTest::$testUser = User::create('toto@renater.fr');

        mock::double('Auth', ['user' => CalendarEndpointTest::$testUser]);
    }

    protected function _after() {
        mock::clean();
    }

    /**
     * test CalendarEndpointCheck post with an OK url
     */
    public function testPostWithOK() {
        mock::double('CalendarManager', ['checkUrl' => ['code' => CalendarStatusCodes::OK, 'message' => CalendarStatusMessages::OK]]);

        // A correcttly formed URL
        $url = 'https://url.de.test';

        mock::double('RestRequest', [
                'getInput' => ['url' => $url]
            ]);

        $res = CalendarCheckEndpoint::post();

        $this->assertEquals($res, ['code' => CalendarStatusCodes::OK, 'message' => CalendarStatusMessages::OK]);
    }

    /**
     * test CalendarEndpointCheck post with an malformed url
     */
    public function testPostWithInvalidURL() {
        mock::double('CalendarManager', ['checkUrl' => ['code' => CalendarStatusCodes::OK, 'message' => CalendarStatusMessages::OK]]);

        // A correcttly formed URL
        $url = 'url.de.test.incorrecte';

        mock::double('RestRequest', [
            'getInput' => ['url' => $url]
        ]);

        $this->expectException('RestBadParameterException');
        CalendarCheckEndpoint::post();
    }

    /**
     * test CalendarEndpointCheck post with no url
     */
    public function testPostWithoutURL() {
        mock::double('RestRequest', [
            'getInput' => []
        ]);

        $this->expectException('RestMissingParameterException');
        CalendarCheckEndpoint::post();
    }

    /**
     * test CalendarEndpointCheck post with no authenticated user
     */
    public function testPostNotAuthenticated() {
        mock::double('Auth', ['user' => false]);

        $this->expectException('RestNotAllowedException');
        CalendarCheckEndpoint::post();
    }
}
