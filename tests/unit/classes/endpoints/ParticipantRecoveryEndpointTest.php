<?php
/**
 *     Moment - ParticipantRecoveryEndpointTest.php
 *
 * Copyright (C) 2022  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

use AspectMock\Test as mock;

class ParticipantRecoveryEndpointTest extends \Codeception\Test\Unit {

    protected function _before() {
        MomentTestUtil::startUp();
//        MomentTestUtil::authenticate();
        MomentTestUtil::initSurvey('survey1');
    }

    protected function _after() {
        mock::clean();
    }

    public function testGet() {
        $this->expectException('RestNotAllowedException');
        ParticipantRecoveryEndpoint::get();
    }

    public function testPut() {
        $this->expectException('RestNotAllowedException');
        ParticipantRecoveryEndpoint::put();
    }

    public function testDelete() {
        $this->expectException('RestNotAllowedException');
        ParticipantRecoveryEndpoint::delete();
    }

    public function testPostSendCode() {

        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));
        MomentTestUtil::deauthenticate();

        mock::double('RestRequest', ['getInput' => (object)[
            'survey_id' => $survey->id,
            'participant_email' => 'an_email'
        ]]);

        $mock = mock::double('ParticipantRecoveryUtil', [
            'sendCodeForParticipant' => true,
            'hasStoredCode' => true
        ]);

        mock::double('Participant', ['fromSurveyAndEmail' => $participant]);

        try {
            ParticipantRecoveryEndpoint::post(ParticipantRecoveryEndpoint::SEND_CODE);
        } catch(Exception $e) {
            $this->fail('ParticipantRecoveryEndpoint should not fail');
        }

        $mock->verifyInvoked('sendCodeForParticipant');

    }

    public function testPostToken() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        MomentTestUtil::deauthenticate();

        mock::double('RestRequest', ['getInput' => (object)[
            'survey_id' => $survey->id,
            'participant_email' => 'an_email',
            'code' => '123456'
        ]]);

        $mock = mock::double('ParticipantRecoveryUtil', [
            'hasStoredCode' => true,
            'isCodeValid' => true
        ]);

        mock::double('Participant', ['fromSurveyAndEmail' => $participant]);

        try {
            $ret = ParticipantRecoveryEndpoint::post(ParticipantRecoveryEndpoint::PARTICIPANT_TOKEN);

            $this->assertArrayHasKey('participant_token', $ret);
            $this->assertEquals($participant->uid, $ret['participant_token']);
        } catch(Exception $e) {
            $this->fail('ParticipantRecoveryEndpoint should not fail');
        }

        $mock->verifyInvoked('isCodeValid', [$participant, '123456']);
    }

    public function testPostTokenInvalidCode() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        MomentTestUtil::deauthenticate();

        mock::double('RestRequest', ['getInput' => (object)[
            'survey_id' => $survey->id,
            'participant_email' => 'an_email',
            'code' => '123456'
        ]]);

        $randTriesLeft = (string)random_int(0, 999);
        $mock = mock::double('ParticipantRecoveryUtil', [
            'hasStoredCode' => true,
            'isCodeValid' => false,
            'getTriesLeft' => $randTriesLeft
        ]);

        mock::double('Participant', ['fromSurveyAndEmail' => $participant]);

        try {
            $ret = ParticipantRecoveryEndpoint::post(ParticipantRecoveryEndpoint::PARTICIPANT_TOKEN);

            $this->assertArrayHasKey('tries_left', $ret);
            $this->assertEquals($randTriesLeft, $ret['tries_left']);
        } catch(Exception $e) {
            $this->fail('ParticipantRecoveryEndpoint should not fail');
        }

        $mock->verifyInvoked('isCodeValid', [$participant, '123456']);
    }

    public function testPostSendLink() {
        $survey = Survey::fromId('survey1');
        MomentTestUtil::deauthenticate();

        mock::double('RestRequest', ['getInput' => (object)[
            'survey_id' => $survey->id,
            'participant_email' => 'an_email'
        ]]);

        $mock = mock::double('ParticipantRecoveryUtil', [
            'sendLinkToParticipant' => true
        ]);

        try {
            ParticipantRecoveryEndpoint::post(ParticipantRecoveryEndpoint::SEND_LINK);
        } catch(Exception $e) {
            $this->fail('ParticipantRecoveryEndpoint should not fail');
        }

        $mock->verifyInvoked('sendLinkToParticipant');
    }

    public function testPostSendLinkMissingParticipantEmail() {
        $survey = Survey::fromId('survey1');
        MomentTestUtil::deauthenticate();

        mock::double('RestRequest', ['getInput' => (object)[
            'survey_id' => $survey->id,
        ]]);

        $this->expectException('ParticipantMissingPropertyException');
        ParticipantRecoveryEndpoint::post(ParticipantRecoveryEndpoint::SEND_LINK);

    }

    public function testPostSendLinkMissingSurveyId() {
        $survey = Survey::fromId('survey1');
        MomentTestUtil::deauthenticate();

        mock::double('RestRequest', ['getInput' => (object)[
            'participant_email' => $survey->id,
        ]]);

        $this->expectException('ParticipantMissingPropertyException');
        ParticipantRecoveryEndpoint::post(ParticipantRecoveryEndpoint::SEND_LINK);

    }

    public function testPostTokenMissingCode() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        MomentTestUtil::deauthenticate();

        mock::double('RestRequest', ['getInput' => (object)[
            'survey_id' => $survey->id,
            'participant_email' => 'an_email',
        ]]);

        mock::double('ParticipantRecoveryUtil', [
            'hasStoredCode' => true,
            'isCodeValid' => true
        ]);

        mock::double('Participant', ['fromSurveyAndEmail' => $participant]);

        $this->expectException('ParticipantMissingPropertyException');
        ParticipantRecoveryEndpoint::post(ParticipantRecoveryEndpoint::PARTICIPANT_TOKEN);
    }


    public function testPostTokenAuthenticated() {

        MomentTestUtil::authenticate();

        //An authenticated user should not be allowed to recover is participation has it should already have it
        $this->expectException('RestNotAllowedException');
        ParticipantRecoveryEndpoint::post(ParticipantRecoveryEndpoint::PARTICIPANT_TOKEN);
    }

    public function testPostSendCodeAuthenticatedParticipant() {
        $survey = Survey::fromId('survey1');
        $participant = Participant::getParticipant($survey, array('participant' => (object)['name' => 'participant1', 'email' => 'participant_email']));

        MomentTestUtil::deauthenticate();

        mock::double('RestRequest', ['getInput' => (object)[
            'survey_id' => $survey->id,
            'participant_email' => 'an_email',
        ]]);

        mock::double('Participant', [
            'fromSurveyAndEmail' => $participant,
            '__get' => function($property) {
                if('type' == $property) {
                    return ParticipantTypes::AUTHENTICATED;
                }

                return __AM_CONTINUE__;
            }
        ]);

        $this->expectException('RestNotAllowedException');
        ParticipantRecoveryEndpoint::post(ParticipantRecoveryEndpoint::SEND_CODE);
    }
}
