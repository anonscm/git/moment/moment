<?php
/**
 *     Moment - _bootstrap.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
include_once __DIR__.'/../../vendor/autoload.php'; // composer autoload
define('EKKO_ROOT', realpath(dirname(__FILE__).'/../..'));

//For test purpose we set the default timezone to Europe/Paris
date_default_timezone_set('Europe/Paris');

$kernel = \AspectMock\Kernel::getInstance();
$kernel->init([
    'debug' => true,
    'cacheDir'  => __DIR__.'/cache',
    'includePaths' => [__DIR__.'/../..'],
    'excludePaths' => [
       __DIR__.'/../../classes/core/model/User.class.php',
       __DIR__.'/../../classes/core/endpoints/UserEndpoint.class.php',
        'vendor'
    ]
]);

$kernel->loadFile(__DIR__.'/../../classes/core/autoload.php'); // path to your autoloader
