<?php

/**
 *     Moment - lang.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* ---------------------------------
 * fr_FR Language File
 * Maintained by RENATER
 * ---------------------------------
 * 
 */

// Header
$lang['home_page'] = 'Accueil';
$lang['logon'] = 'Connexion';
$lang['logoff'] = 'Déconnexion';
$lang['menu'] = 'Menu';
$lang['dropdown_menu'] = 'Menu déroulant';
$lang['language'] = 'Langue';
$lang['select_language'] = 'Choisir la langue';
$lang['back'] = 'Back';

// Footer
$lang['resources'] = 'Ressources';
$lang['user_guide'] = 'Guide utilisateur';
$lang['faq'] = 'FAQ';
$lang['feedback'] = 'Retour utilisateur';
$lang['feedback_title'] = 'Faites-nous part de vos remarques !';
$lang['contact'] = 'Contactez-nous';
$lang['all_rights_reserved'] = 'Tous droits réservés';

// standard date display format
$lang['date_format'] = 'd/m/Y'; // Format for displaying date, use PHP date() format string syntax
$lang['datetime_format'] = 'd/m/Y H:i:s'; // Format for displaying datetime, use PHP date() format string syntax
$lang['time_format'] = 'H:i'; // Format for displaying time, use PHP date() format string syntax
$lang['relative_time_format'] = '{h:H\h} {i:i\m\i\n} {s:s\s}'; // Format for displaying time (elapsed), use PHP date()'s h, i and s components, surrounding parts with {component:...} allow to not display them if zero

// datepicker localization
$lang['dp_close_text'] = 'OK';
$lang['dp_prev_text'] = 'Préc';
$lang['dp_next_text'] = 'Suiv';
$lang['dp_current_text'] = 'Aujourd\'hui';
$lang['dp_month_names'] = 'Janvier,Février,Mars,Avril,Mai,Juin,Juillet,Août,Septembre,Octobre,Novembre,Décembre';
$lang['dp_month_names_short'] = 'Jan,Fev,Mar,Avr,Mai,Jun,Jul,Aou,Sep,Oct,Nov,Dec';
$lang['dp_day_names'] = 'Dimanche,Lundi,Mardi,Mercredi,Jeudi,Vendredi,Samedi';
$lang['dp_day_names_short'] = 'Dim,Lun,Mar,Mer,Jeu,Ven,Sam';
$lang['dp_day_names_min'] = 'Di,Lu,Ma,Me,Je,Ve,Sa';
$lang['dp_week_header'] = 'Sem';
$lang['dp_date_format'] = 'd/m/Y';
$lang['dp_date_format_hint'] = 'Format jj/mm/aaaa, max. {max} jours';
$lang['dp_datetime_format'] = 'd/m/Y H:i';
$lang['dp_time_format'] = 'H:i';
$lang['dp_first_day'] = '1';
$lang['dp_is_rtl'] = 'false';
$lang['dp_show_month_after_year'] = 'false';
$lang['dp_year_suffix'] = '';

$lang['moment_date_format'] = 'DD/MM/YYYY';
$lang['moment_datetime_format'] = 'DD/MM/YYYY HH:mm';
$lang['moment_time_format'] = 'HH:mm';


// File size
$lang['size_unit'] = 'o';

// Common
$lang['expand_all'] = 'Développer tout';
$lang['expires'] = 'Expire';
$lang['options'] = 'Options';
$lang['see_all'] = 'Voir tout';
$lang['stop'] = 'Stop';
$lang['n_more'] = '{n} autres';
$lang['save'] = 'Sauvegarder';
$lang['actions'] = 'Actions';
$lang['done'] = 'Fait';
$lang['retry'] = 'Ré-essayer';
$lang['ignore'] = 'Ignorer';
$lang['never'] = 'jamais';
$lang['none'] = 'aucun';
$lang['cancel'] = 'Annuler';
$lang['close'] = 'Fermer';
$lang['ok'] = 'OK';
$lang['send'] = 'Envoyer';
$lang['delete'] = 'Supprimer';
$lang['add'] = 'Ajouter';
$lang['create'] = 'Créer';
$lang['update'] = 'Mettre à jour';
$lang['export'] = 'Exporter';
$lang['yes'] = 'Oui';
$lang['no'] = 'Non';
$lang['clear_all'] = 'Supprimer tout';
$lang['to'] = 'A';
$lang['from'] = 'De';
$lang['size'] = 'Taille';
$lang['created'] = 'Créé';
$lang['subject'] = 'Sujet';
$lang['message'] = 'Message';
$lang['details'] = 'Details';
$lang['showhide'] = 'Afficher/Cacher';
$lang['optional'] = 'optionnel';
$lang['noscript'] = 'Cette application utilise Javascript massivement, vous devez l\'activer afin de pouvoir commencer.';
$lang['send_reminder'] = 'Envoyer un rappel';
$lang['confirm_dialog'] = 'Confirmation';
$lang['error_dialog'] = 'Erreur';
$lang['info_dialog'] = 'Information';
$lang['success_dialog'] = 'Succès';
$lang['error_type'] = 'Type d\'erreur';
$lang['error_date'] = 'Date';
$lang['error_details'] = 'Détails techniques';
$lang['forward'] = 'Faire suivre';
$lang['email_sent'] = 'Message envoyé';
$lang['copy_text'] = 'Copier le texte ci-dessous';
$lang['reason'] = 'Raison';
$lang['method'] = 'Méthode';
$lang['endpoint'] = 'Point d\'entrée';
$lang['please_wait'] = 'Veuillez patienter';
$lang['invalid_fields'] = 'Tous les champs ne sont pas correctement remplis.';
$lang['url'] = 'URL';
$lang['caption'] = 'Nom';
$lang['email'] = 'Email';
$lang['other'] = 'autre';
$lang['create'] = 'Créer';
$lang['load_more'] = 'Voir plus';
$lang['enabled'] = 'Actif';
$lang['disabled'] = 'Désactivé';
$lang['date'] = 'Date';
$lang['ip'] = 'Adresse IP';
$lang['optional'] = '(optionnel)';
$lang['search'] = 'Rechercher';

/**
 * Form specific
 */
$lang['constraint_pattern'] = '{about}';
$lang['constraints_separator'] = ', ';
$lang['constraint_mandatory'] = 'obligatoire';
$lang['constraint_maxlength'] = 'maximum {value} caractères';
$lang['constraint_min'] = 'minimum {value}';
$lang['constraint_max'] = 'maximum {value}';
$lang['constraint_date_min'] = 'minimum {date:value}';
$lang['constraint_date_max'] = 'maximum {date:value}';
$lang['constraint_datetime_min'] = 'minimum {datetime:value}';
$lang['constraint_datetime_max'] = 'maximum {datetime:value}';
$lang['constraint_time_min'] = 'minimum {time:value}';
$lang['constraint_time_max'] = 'maximum {time:value}';

$lang['not_enough_entries'] = 'Pas assez d\'entrées';
$lang['too_many_entries'] = 'Trop d\'entrées';
$lang['not_long_enough'] = 'Pas assez long';
$lang['value_required'] = 'Valeur requise';
$lang['bad_text'] = 'Mauvais format de texte';
$lang['bad_number'] = 'Mauvais format de nombre';
$lang['bad_email'] = 'Mauvais format d\'adresse email';
$lang['bad_url'] = 'Mauvais format d\'URL';
$lang['value_too_high'] = 'Valeur trop élevée';
$lang['value_too_low'] = 'Valeur trop faible';
$lang['delete_entity'] = 'Supprimer l\'objet : {entity} ?';
$lang['create_success'] = 'La création a réussi';
$lang['create_failure'] = 'La création a échoué';
$lang['update_success'] = 'La mise à jour a réussi';
$lang['update_failure'] = 'La mise à jour a échoué';
$lang['delete_success'] = 'La suppression a réussi';
$lang['delete_failure'] = 'La suppression a échoué';

/**
 * User profile specifics
 */
$lang['user_page'] = 'Mon profil';
$lang['user_preferences'] = 'Préférences';
$lang['user_lang'] = 'Langue préférée';
$lang['user_remote_authentication'] = 'Authentification distante';
$lang['user_auth_secret'] = 'Secret';
$lang['user_additionnal'] = 'Informations additionnelles';
$lang['user_id'] = 'Identité';
$lang['user_created'] = 'Première connexion';
$lang['get_full_user_remote_config'] = 'Obtenir la configuration complète pour l\'authentification distante';
$lang['preferences_updated'] = 'Préférences utilisateur sauvegardées';
$lang['remote_auth_sync_request'] = '<p><strong>{remote}</strong> requiert vos informations pour l\'authentification distante.</p><p>Pour autoriser l\'accès veuiller donner le code suivant à <strong>{remote}</strong> : <strong>{code}</strong> (ce code est utilisable dans les prochaines 2 minutes seulement).</p><p>Si vous n\'êtes pas à l\'origine de cette demande merci d\'ignorer de message.</p>';

/**
 * Admin
 */
$lang['admin_page'] = 'Admin';
$lang['user_management'] = 'Gestion des utilisateurs';
$lang['search_for_user'] = 'Rechercher un utilisateur ...';
$lang['impersonate'] = 'Impersonifier';
$lang['impersonating'] = 'Vous êtes actuellement en train d\'impersonifier un autre utilisateur, cliquez ici pour arrêter l\'impersonification';

/**
 * Exceptions and errors
 */
$lang['undergoing_maintenance'] = 'Cette application est en maintenance';

$lang['authentication_required'] = 'Authentification requise';
$lang['authentication_required_explanation'] = 'Vous devez être authentifié pour effectuer cette opération. Votre session a peut être expiré ? Merci de vous ré-authentifier.';

$lang['access_forbidden'] = 'Vous ne disposez pas des droits nécessaires pour accéder à cette page';

$lang['encountered_exception'] = 'L\'application a rencontré une erreur lors du traitement de votre requête';
$lang['you_can_report_exception'] = 'En rapportant cette erreur merci de mentionner le code suivant afin de faciliter la recherche du problème';
$lang['you_can_report_exception_by_email'] = 'Vous pouvez rapporter cette erreur par email';
$lang['report_exception'] = 'Envoyer un rapport';

// Auth related exceptions
$lang['auth_authentication_not_found'] = 'Méthode d\'authentification introuvable';
$lang['auth_user_not_allowed'] = 'Vous n\'êtes pas autorisé à utiliser cette application';

// AuthRemote related exceptions
$lang['auth_remote_unknown_application'] = 'Application distante inconnue';
$lang['auth_remote_too_late'] = 'La requête est arrivée trop tard';
$lang['auth_remote_signature_check_failed'] = 'Signature fournie incorrecte';
$lang['auth_remote_user_rejected'] = 'Cet utilisateur n\'accèpte pas d\'authentification distante';

// AuthSP related exceptions
$lang['auth_sp_missing_delegation_class'] = 'Classe de délégation d\'authentification de type "Fournisseur de Service" manquante';
$lang['auth_sp_authentication_not_found'] = 'Classe de délégation d\'authentification de type "Fournisseur de Service" introuvable';
$lang['auth_sp_missing_attribute'] = 'Attribut manquant pour l\'authentification de type "Fournisseur de Service"';
$lang['auth_sp_bad_attribute'] = 'Attribut erroné pour l\'authentification de type "Fournisseur de Service"';

// Bad exceptions
$lang['bad_email'] = 'Format d\'adresse email erroné';
$lang['bad_size_format'] = 'Taille incorrecte';
$lang['bad_lang_code'] = 'Code de language incerrect';

// Config related exceptions
$lang['config_file_missing'] = 'Fichier de configuration introuvable';
$lang['config_bad_parameter'] = 'Paramètre de configuration incorrect';
$lang['config_missing_parameter'] = 'Paramètre de configuration introuvable';
$lang['config_override_disabled'] = 'Surcharge de la configuration désactivée';
$lang['config_override_validation_failed'] = 'La validation de la configuration a échoué';
$lang['config_override_not_allowed'] = 'Surcharge de la configuration non-autorisée';
$lang['config_override_cannot_save'] = 'Impossible de sauvegarder la configuration';

// Core related exceptions
$lang['core_file_not_found'] = 'Fichier système introuvable';
$lang['core_class_not_found'] = 'Classe système introuvable';

// DBI related exceptions
$lang['failed_to_connect_to_database'] = 'Impossible de se connecter à la base de données';
$lang['dbi_missing_parameter'] = 'Paramètre de connexion à la base de données manquant';
$lang['database_access_failure'] = 'Erreur lors de l\'accès à la base de données';

// DBO related exceptions
$lang['no_such_property'] = 'Propriété inexistante';

// Rest related exceptions
$lang['rest_authentication_required'] = 'Authentification REST requise';
$lang['rest_admin_required'] = 'Droits administrateur requis';
$lang['rest_ownership_required'] = 'possession de la ressource REST requise';
$lang['rest_missing_parameter'] = 'Paramètre REST manquant';
$lang['rest_bad_parameter'] = 'Paramètre REST erroné';
$lang['rest_method_not_allowed'] = 'Le serveur REST n\'accepte pas cette méthode';
$lang['rest_endpoint_missing'] = 'Le serveur REST n\'a pas pu déduire le point d\'entrée de l\'URL';
$lang['rest_access_forbidden'] = 'Le serveur REST a refusé l\'accès';
$lang['rest_jsonp_get_only'] = 'Le serveur REST n\'accepte que la méthode GET pour les requêtes de type JSONP';
$lang['rest_updatedsince_bad_format'] = 'Le paramètre REST updatedSince est erroné';
$lang['rest_endpoint_not_implemented'] = 'Point d\'entrée REST introuvable';
$lang['rest_method_not_implemented'] = 'Méthode REST introuvable dans le point d\'entrée';
$lang['rest_sanity_check_failed'] = 'La vérification des données REST a échoué';
$lang['rest_xsrf_token_did_not_match'] = 'Le code de sécurité ne correspond pas';
$lang['rest_undergoing_maintenance'] = 'Maintenance en cours';
$lang['rest_not_allowed'] = 'Non autorisé';
$lang['rest_jsonp_bad_method'] = 'Mauvaise JSONP méthode';
$lang['rest_jsonp_not_allowed'] = 'Méthode non autorisé pour JSONP';
$lang['rest_option_not_found'] = 'Option introuvable';
$lang['rest_option_mal_formed'] = 'Option malformée';
$lang['rest_option_not_allowed'] = 'Option non autorisée';
$lang['rest_options_not_allowed'] = 'Options non autorisées';
$lang['rest_option_not_supported'] = 'Option non supportée';
$lang['rest_options_not_supported'] = 'Options non supportées';
$lang['rest_output_format_not_supported'] = 'Format de sortie non supporté';
$lang['rest_output_conversion_failed'] = 'Echec de la conversion du format de sortie';

// Template related exceptions
$lang['template_not_found'] = 'Modèle introuvable';

// User related exceptions
$lang['user_not_found'] = 'Utilisateur introuvable';
$lang['user_missing_uid'] = 'Identifiant unique d\'utilisateur manquant';

// Utilities related exceptions
$lang['utilities_uid_generator_bad_unicity_checker'] = 'Vérificateur d\'unicité du générateur d\'identifiants uniques erroné';
$lang['utilities_uid_generator_tried_too_much'] = 'Nombre maximal d\'essais pour le générateur d\'identifiants uniques dépassé';

// Multiple select
$lang['mlt_select_placeholder'] = 'Sélectionner une option';
$lang['mlt_select_search'] = 'Rechercher';
$lang['mlt_select_selected'] = ' sélectionné';
$lang['mlt_select_select_all'] = 'Sélectionner tout';
$lang['mlt_select_unselect_all'] = 'Désélectionner tout';
$lang['mlt_select_none_selected'] = 'Aucune sélection';