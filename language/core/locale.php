<?php

/**
 *     Moment - locale.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

//The below array maps locales provided by the browser to your local translation.
//Put your custom translated file in the "config" subdirectory.
//Add relevant entries for browser locales here, and your translation will automagically be recognized and used....

$locales  =  array(
    'en'=>  array('name' => 'English', 'path' => 'en_EN'),
    'fr' => array('name' => 'Français', 'path' => 'fr_FR'),
);
