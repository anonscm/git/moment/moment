<?php

/**
 *     Moment - lang.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* ---------------------------------
 * en_AU Language File
 * Maintained by RENATER
 * ---------------------------------
 * 
 */

// Header
$lang['home_page'] = 'Home';
$lang['logon'] = 'Log-on';
$lang['logoff'] = 'Log-off';
$lang['menu'] = 'Menu';
$lang['dropdown_menu'] = 'Dropdown menu';
$lang['language'] = 'Language';
$lang['select_language'] = 'Select language';
$lang['back'] = 'Back';

// Footer
$lang['resources'] = 'Resources';
$lang['user_guide'] = 'User guide';
$lang['faq'] = 'FAQ';
$lang['feedback'] = 'Feedback';
$lang['feedback_title'] = 'Give us feedback !';
$lang['contact'] = 'Contact us';
$lang['all_rights_reserved'] = 'All rights reserved';

// standard date display format
$lang['date_format'] = 'm/d/Y'; // Format for displaying date, use PHP date() format string syntax 
$lang['datetime_format'] = 'm/d/Y H:i:s'; // Format for displaying datetime, use PHP date() format string syntax 
$lang['time_format'] = 'H:i'; // Format for displaying time, use PHP date() format string syntax 
$lang['relative_time_format'] = '{h:H\h} {i:i\m\i\n} {s:s\s}'; // Format for displaying time (elapsed), use PHP date()'s h, i and s components, surrounding parts with {component:...} allow to not display them if zero

// datepicker localization
$lang['dp_close_text'] = 'Done'; // Done
$lang['dp_prev_text'] = 'Prev'; //Prev
$lang['dp_next_text'] = 'Next'; // Next
$lang['dp_current_text'] = 'Today'; // Today
$lang['dp_month_names'] = 'January,February,March,April,May,June,July,August,September,October,November,December'; // Comma separated, w/o whitespaces
$lang['dp_month_names_short'] = 'Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec'; // Comma separated, w/o whitespaces
$lang['dp_day_names'] = 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday'; // Comma separated, w/o whitespaces
$lang['dp_day_names_short'] = 'Sun,Mon,Tue,Wed,Thu,Fri,Sat'; // Comma separated, w/o whitespaces
$lang['dp_day_names_min'] = 'Su,Mo,Tu,We,Th,Fr,Sa'; // Comma separated, w/o whitespaces
$lang['dp_week_header'] = 'Wk';
$lang['dp_date_format'] = 'm/d/Y';
$lang['dp_date_format_hint'] = 'Format mm/dd/yyyy, max. {max} days';
$lang['dp_datetime_format'] = 'm/d/Y H:i';
$lang['dp_time_format'] = 'H:i';
$lang['dp_first_day'] = '1';
$lang['dp_is_rtl'] = 'false'; // "true" or "false"
$lang['dp_show_month_after_year'] = 'false'; // "true" or "false"
$lang['dp_year_suffix'] = '';

$lang['moment_date_format'] = 'MM/DD/YYYY';
$lang['moment_datetime_format'] = 'MM/DD/YYYY HH:mm';
$lang['moment_time_format'] = 'HH:mm';


// File size
$lang['size_unit'] = 'b';

// Common
$lang['expand_all'] = 'Expand all';
$lang['expires'] = 'Expires';
$lang['options'] = 'Options';
$lang['see_all'] = 'See all';
$lang['stop'] = 'Stop';
$lang['n_more'] = '{n} more';
$lang['save'] = 'Save';
$lang['actions'] = 'Actions';
$lang['done'] = 'Done';
$lang['retry'] = 'Try again';
$lang['ignore'] = 'Ignore';
$lang['never'] = 'never';
$lang['none'] = 'none';
$lang['cancel'] = 'Cancel';
$lang['close'] = 'Close';
$lang['ok'] = 'OK';
$lang['send'] = 'Send';
$lang['add'] = 'Add';
$lang['delete'] = 'Delete';
$lang['create'] = 'Create';
$lang['update'] = 'Update';
$lang['export'] = 'Export';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['clear_all'] = 'Clear all';
$lang['to'] = 'To';
$lang['from'] = 'From';
$lang['size'] = 'Size';
$lang['created'] = 'Created';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['details'] = 'Details';
$lang['showhide'] = 'Show/Hide';
$lang['optional'] = 'optional';
$lang['noscript'] = 'This application heavily relies on Javascript, you must enable it to be able to start.';
$lang['confirm_dialog'] = 'Confirmation';
$lang['error_dialog'] = 'Error';
$lang['info_dialog'] = 'Information';
$lang['success_dialog'] = 'Success';
$lang['error_type'] = 'Error type';
$lang['error_date'] = 'Error date';
$lang['error_details'] = 'Error technical details';
$lang['recipient_error_bounce'] = 'email delivery failed';
$lang['email_sent'] = 'Message Sent';
$lang['copy_text'] = 'Copy the text bellow';
$lang['reason'] = 'Reason';
$lang['method'] = 'Method';
$lang['endpoint'] = 'Endpoint';
$lang['please_wait'] = 'Please wait';
$lang['invalid_fields'] = 'All fields are not well filled.';
$lang['url'] = 'URL';
$lang['caption'] = 'Caption';
$lang['email'] = 'Email';
$lang['other'] = 'other';
$lang['create'] = 'Create';
$lang['load_more'] = 'Load more';
$lang['enabled'] = 'Enabled';
$lang['disabled'] = 'Disabled';
$lang['date'] = 'Date';
$lang['ip'] = 'IP address';
$lang['optional'] = '(optionnal)';
$lang['search'] = 'Search';

/**
 * Form specific
 */
$lang['constraints_separator'] = ', ';
$lang['constraint_mandatory'] = 'mandatory';
$lang['constraint_maxlength'] = 'maximum length {value}';
$lang['constraint_min'] = 'minimum {value}';
$lang['constraint_max'] = 'maximum {value}';
$lang['constraint_date_min'] = 'minimum {date:value}';
$lang['constraint_date_max'] = 'maximum {date:value}';
$lang['constraint_datetime_min'] = 'minimum {datetime:value}';
$lang['constraint_datetime_max'] = 'maximum {datetime:value}';
$lang['constraint_time_min'] = 'minimum {time:value}';
$lang['constraint_time_max'] = 'maximum {time:value}';

$lang['not_enough_entries'] = 'Not enough entries';
$lang['too_many_entries'] = 'Too many entries';
$lang['not_long_enough'] = 'Not long enough';
$lang['value_required'] = 'Value required';
$lang['bad_text'] = 'Bad text (size ?)';
$lang['bad_number'] = 'Bad number';
$lang['bad_email'] = 'Bad email';
$lang['bad_url'] = 'Bad URL';
$lang['value_too_high'] = 'Value too high';
$lang['value_too_low'] = 'Value too low';
$lang['delete_entity'] = 'Delete {entity} ?';
$lang['create_success'] = 'Creation succeeded';
$lang['create_failure'] = 'Creation failed';
$lang['update_success'] = 'Update succeeded';
$lang['update_failure'] = 'Update failed';
$lang['delete_success'] = 'Deletion succeeded';
$lang['delete_failure'] = 'Deletion failed';

/**
 * User profile specifics
 */
$lang['user_page'] = 'My profile';
$lang['user_preferences'] = 'Preferences';
$lang['user_lang'] = 'Prefered language';
$lang['user_remote_authentication'] = 'Remote authentication';
$lang['user_auth_secret'] = 'Secret';
$lang['user_additionnal'] = 'Additionnal information';
$lang['user_id'] = 'Identifiant';
$lang['user_created'] = 'First login';
$lang['get_full_user_remote_config'] = 'Get full remote configuration';
$lang['preferences_updated'] = 'User preferences updated';
$lang['remote_auth_sync_request'] = '<p><strong>{remote}</strong> requested your remote authentication details.</p><p>To allow access please give the following code to <strong>{remote}</strong> : <strong>{code}</strong> (code is valid for the next 2 minutes only).</p><p>If you don\'t know what this is about just disregard this message.</p>';

/**
 * Admin
 */
$lang['admin_page'] = 'Admin';
$lang['user_management'] = 'User management';
$lang['search_for_user'] = 'Search for user ...';
$lang['impersonate'] = 'Impersonate';
$lang['impersonating'] = 'You are now impersonating another user, click here to cancel the impersonation';

/**
 * Exceptions and errors
 */
$lang['undergoing_maintenance'] = 'This application is under maintenance';

$lang['authentication_required'] = 'Authentication required';
$lang['authentication_required_explanation'] = 'You need to be authentified to do that. Maybe your session expired ? Please click Ok and authenticate again.';

$lang['access_forbidden'] = 'You are not allowed to access this page';

$lang['encountered_exception'] = 'The application encountered an error while processing your request';
$lang['you_can_report_exception'] = 'When reporting this error please give the following code to help the support finding out details';
$lang['you_can_report_exception_by_email'] = 'You can report this error by email';
$lang['report_exception'] = 'report error';

// Auth related exceptions
$lang['auth_authentication_not_found'] = 'Authentification system not found';
$lang['auth_user_not_allowed'] = 'You are not allowed to use this application';

// AuthRemote related exceptions
$lang['auth_remote_unknown_application'] = 'Unknow remote application';
$lang['auth_remote_too_late'] = 'Authentification too late';
$lang['auth_remote_signature_check_failed'] = 'Remote signature check failed';
$lang['auth_remote_user_rejected'] = 'User does not accept remote authentication';

// AuthSP related exceptions
$lang['auth_sp_missing_delegation_class'] = 'SP authentification delegation class not found';
$lang['auth_sp_authentication_not_found'] = 'SP authentification class not found';
$lang['auth_sp_missing_attribute'] = 'SP authentification attribute not found';
$lang['auth_sp_bad_attribute'] = 'SP authentification bad attribute';

// Bad exceptions
$lang['bad_email'] = 'Invalid email format';
$lang['bad_size_format'] = 'Invalid size format';
$lang['bad_lang_code'] = 'Invalid lang code';

// Config related exceptions
$lang['config_file_missing'] = 'Configuration file not found';
$lang['config_bad_parameter'] = 'Invalid configuration parameter';
$lang['config_missing_parameter'] = 'Configuration parameter not found';
$lang['config_override_disabled'] = 'Overriding configuration is disabled';
$lang['config_override_validation_failed'] = 'Overriding configuration validation failed';
$lang['config_override_not_allowed'] = 'Overriding configuration is not allowed';
$lang['config_override_cannot_save'] = 'Cannot save the new configuration';

// Core related exceptions
$lang['core_file_not_found'] = 'Core file not found';
$lang['core_class_not_found'] = 'Core class not found';

// DBI related exceptions
$lang['failed_to_connect_to_database'] = 'Fail to connect database';
$lang['dbi_missing_parameter'] = 'Missing DBI configuration parameter';
$lang['database_access_failure'] = 'Fail to access database';

// DBO related exceptions
$lang['no_such_property'] = 'Property not found';

// Rest related exceptions
$lang['rest_authentication_required'] = 'REST authentification required';
$lang['rest_admin_required'] = 'Admin rights required';
$lang['rest_ownership_required'] = 'REST resource ownership required';
$lang['rest_missing_parameter'] = 'Missing REST parameter';
$lang['rest_bad_parameter'] = 'Bad REST parameter';
$lang['rest_method_not_allowed'] = 'REST server does not accept this method';
$lang['rest_endpoint_not_found'] = 'REST server could not find endpoint in URL';
$lang['rest_access_forbidden'] = 'REST server denied access';
$lang['rest_jsonp_get_only'] = 'REST server only accepts GET requests for JSONP output';
$lang['rest_updatedsince_bad_format'] = 'REST updatedSince parameter is badly formatted';
$lang['rest_endpoint_not_implemented'] = 'REST endpoint not implemented';
$lang['rest_method_not_implemented'] = 'REST method not implemented in endpoint';
$lang['rest_sanity_check_failed'] = 'REST sanity check failed';
$lang['rest_xsrf_token_invalid'] = 'Security token did not match';
$lang['rest_undergoing_maintenance'] = 'Undergoing maintenance';
$lang['rest_not_allowed'] = 'Not allowed';
$lang['rest_jsonp_bad_method'] = 'Bad JSONP method';
$lang['rest_jsonp_not_allowed'] = 'Method not allowed for JSONP';
$lang['rest_option_not_found'] = 'Option not found';
$lang['rest_option_mal_formed'] = 'Malformed option';
$lang['rest_option_not_allowed'] = 'Option not allowed';
$lang['rest_options_not_allowed'] = 'Options not allowed';
$lang['rest_option_not_supported'] = 'Option not supported';
$lang['rest_options_not_supported'] = 'Options not supported';
$lang['rest_output_format_not_supported'] = 'Output format not supported';
$lang['rest_output_conversion_failed'] = 'Output conversion failed';

// Template related exceptions
$lang['template_not_found'] = 'Template not found';

// User related exceptions
$lang['user_not_found'] = 'User not found';
$lang['user_missing_uid'] = 'User UID not found';

// Utilities related exceptions
$lang['utilities_uid_generator_bad_unicity_checker'] = 'Invalid unicity check for UID generator';
$lang['utilities_uid_generator_tried_too_much'] = 'Too much use of UID generator';

// Multiple select
$lang['mlt_select_placeholder'] = 'Select options';
$lang['mlt_select_search'] = 'Search';
$lang['mlt_select_selected'] = ' selected';
$lang['mlt_select_select_all'] = 'Select all';
$lang['mlt_select_unselect_all'] = 'Unselect all';
$lang['mlt_select_none_selected'] = 'None selected';