<?php
/**
 *     Moment - lang.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* ---------------------------------
 * en_EN Language File
 * Maintained by Renater
 * ---------------------------------
*/

// classes/constants/ParticipantNotificationReasons.class.php
$lang['comment_created'] = 'Comment added';

// classes/constants/ParticipantNotificationReasons.class.php
$lang['answer_created'] = 'Answer added';

// classes/constants/ParticipantNotificationReasons.class.php
$lang['reminder'] = 'Reminder';

// classes/constants/QuestionOptions.class.php
$lang['enable_multiple_choices'] = 'Enable Multiple choices';

// classes/constants/QuestionOptions.class.php
// templates/question/question_form.php
$lang['force_unique_choice'] = 'Unique Choice';

// classes/constants/QuestionOptions.class.php
// templates/question/question_form.php
$lang['enable_maybe_choices'] = 'Enable Maybe answer';

// classes/constants/QuestionOptions.class.php
$lang['mandatory_answer'] = 'Mandatory Answer';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
// templates/survey/accordion/accordion.php
$lang['auto_close'] = 'Closing date';
$lang['settings_auto_close'] = '<b>Closing date:</b> the survey will not be available from the selected date<br><b>Deletion date:</b> the survey will be deleted %d months after the closing date';


// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['enable_anonymous_answer'] = 'Enable anonymous answers';

// classes/constants/SurveySettings.class.php
$lang['enable_answer_edition'] = 'Enable editing answers';

// classes/constants/SurveySettings.class.php
$lang['enable_update_notification'] = 'Enable update notifications';

// classes/constants/SurveySettings.class.php
// templates/question/propositions_text_form.php
// templates/survey/wizard/step_general.php
$lang['limit_participants'] = 'Limit number of participants';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['limit_participants_nb'] = 'Maximum number of participants';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['reply_access'] = 'Who can answer the survey?';
$lang['reply_access_info'] = '<b>Everyone :</b> All the users, authenticated or not, can answer, the questionnaire is public.'
    . '<br /><b>Authenticated users :</b> Only authenticated users on the platform can answer.'
    . '<br /><b>Authenticated guests :</b> Only authenticated users which email has been added as guest on the questionnaire can answer';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['hide_answers'] = 'Hide the answers of other participants';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['hide_comments'] = 'Hide comments of other particpants';

// classes/constants/SurveySettings.class.php
$lang['show_participant_name'] = 'Display name of participants';

// classes/constants/SurveySettings.class.php
$lang['show_participant_email'] = 'Display email of participants';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['dont_notify_on_reply'] = 'Don\'t be notified when a participant answers';

// classes/constants/YesNoMaybeValues.class.php
$lang['selected_value_yes'] = 'Yes';

// classes/constants/YesNoMaybeValues.class.php
$lang['selected_value_no'] = 'No';

// classes/constants/YesNoMaybeValues.class.php
$lang['selected_value_maybe'] = 'Maybe';

// classes/model/Answer.class.php
// classes/model/DateProposition.class.php
// classes/model/Survey.class.php
// classes/model/TextProposition.class.php
$lang['Question'] = 'Question';

// classes/model/Comment.class.php
// classes/model/EventLog.class.php
// classes/model/Survey.class.php
// templates/survey/accordion/accordion.php
$lang['created'] = 'Created';

// classes/model/Comment.class.php
// classes/model/Survey.class.php
$lang['Participant'] = 'Participant';

// classes/model/Guest.class.php
// classes/model/Participant.class.php
// classes/model/Survey.class.php
// templates/survey/wizard/step_guests.php
$lang['email'] = 'Email';

// classes/model/Guest.class.php
$lang['invitation_sent'] = 'Invitation sent';

// classes/model/Guest.class.php
// classes/model/Participant.class.php
// classes/model/Question.class.php
// classes/model/Survey.class.php
$lang['Survey'] = 'Survey';

// classes/model/Participant.class.php
// templates/survey/reply.php
// view/js/components/survey_guests_controller.js
// view/js/components/survey_reply.js
// view/js/components/survey_wizard.js
// view/cache/script.js
// view/lib/select2/js/select2/core.js
$lang['name'] = 'Name';

// classes/model/Participant.class.php
$lang['answers_created'] = 'Saved Answers';

// classes/model/Participant.class.php
$lang['answers_updated'] = 'Updated answers';

// classes/model/Participant.class.php
$lang['Comment'] = 'Comment';

// classes/model/Participant.class.php
// classes/model/Question.class.php
$lang['Answer'] = 'Answer';

// classes/model/Participant.class.php
// classes/model/Survey.class.php
$lang['comments'] = 'Comments';

// classes/model/Participant.class.php
$lang['anonymous'] = 'Anonymous';

// classes/model/Question.class.php
// classes/model/Survey.class.php
// templates/question/question_form.php
// templates/question/question_form.php
// templates/survey/wizard/step_general.php
// view/cache/script.js
// view/lib/select2/js/select2/selection/base.js
$lang['title'] = 'Title';

// classes/model/Question.class.php
// templates/question/question_form.php
$lang['options'] = 'Options';

// classes/model/Question.class.php
// templates/question/propositions_date_form.php
// templates/question/propositions_text_form.php
// templates/question/question_form.php
$lang['propositions'] = 'Suggestions';

// classes/model/Survey.class.php
$lang['opened_to_everyone'] = 'Everyone';

// classes/model/Survey.class.php
$lang['opened_to_guests'] = 'Authenticated guests';

// classes/model/Survey.class.php
$lang['opened_to_authenticated'] = 'Authenticated users';

// classes/model/Survey.class.php
$lang['opened_to_noone'] = 'None';

// classes/model/Survey.class.php
$lang['answer_access_repliers'] = 'The participants';

// classes/model/Survey.class.php
$lang['answer_access_repliers_with_answers'] = 'The particpants that have answered';

// classes/model/Survey.class.php
$lang['answer_access_owner'] = 'The owner';

// classes/model/Survey.class.php
$lang['comment_access_repliers'] = 'The participants';

// classes/model/Survey.class.php
$lang['comment_access_repliers_with_answers'] = 'The participants that have answered';

// classes/model/Survey.class.php
$lang['comment_access_owner'] = 'The owner';

// classes/model/Survey.class.php
$lang['Surveys'] = 'Surveys';

// classes/model/Survey.class.php
$lang['place'] = 'Place';

// classes/model/Survey.class.php
// templates/survey/wizard/step_general.php
$lang['description'] = 'Description';

// classes/model/Survey.class.php
$lang['owner_name'] = 'Owner Name';

// classes/model/Survey.class.php
$lang['owner_email'] = 'Owner Email';

// classes/model/Survey.class.php
$lang['updated'] = 'Updated';

// classes/model/Survey.class.php
// templates/survey/manage.php
// templates/survey/accordion/accordion.php
// templates/survey/accordion/accordion.php
$lang['closed'] = 'Closed';

// classes/model/Survey.class.php
$lang['settings'] = 'Settings';

// classes/model/Survey.class.php
$lang['Guest'] = 'Guest';

// classes/model/Survey.class.php
// templates/survey/create.php
// templates/survey/update.php
$lang['guests'] = 'Guests';

$lang['guests_step_header'] = $lang['guests'];

// classes/model/Survey.class.php
// templates/survey/create.php
// templates/survey/update.php
// templates/survey/wizard/step_questions.php
$lang['questions'] = 'Questions';

// classes/model/Survey.class.php
$lang['expires'] = 'Expires';

// classes/model/TextProposition.class.php
// templates/question/question_form.php
$lang['header'] = 'Header';

// templates/menu.php
$lang['connected_user'] = 'Connected user';
$lang['disconnected_user'] = 'Disconnected user';

// templates/page_menu.php
$lang['create_a_survey'] = 'Create a survey';

// templates/page_menu.php
$lang['manage_surveys'] = 'Manage surveys';

// templates/survey_page.php
$lang['results'] = 'Results';

// templates/answer/answers.php
// templates/question/reply_form.php
// templates/comment/participants_table.php
// templates/survey/results.php
// templates/survey/wizard/step_questions.php
// templates/participant/participants_table.php
$lang['question'] = 'Question';

// templates/answer/sum_row.php
// templates/participant/participants_table.php
$lang['sum'] = 'Total';

$lang['sum_yes'] = 'The "Yes"';

$lang['sum_maybe'] = 'The "Maybe"';

// templates/question/reply_form.php
// templates/survey/create.php
// templates/survey/update.php
// templates/survey/wizard/step_guests.php
$lang['validate'] = 'Validate';

$lang['preview_the_survey'] = 'Preview the survey';

$lang['create_the_survey'] = 'Create the survey';

$lang['save_modifications'] = 'Save the modifications';

// templates/question/reply_form.php
$lang['leave_a_comment'] = 'Leave a comment';

// templates/question/reply_form.php
$lang['your_comment'] = 'Your comment';

// templates/question/propositions_text_form.php
// view/js/components/survey_questions_controller.js
// view/cache/script.js
$lang['proposition'] = 'Suggestion';

// templates/question/propositions_text_form.php
$lang['variant'] = 'Variant';

// templates/question/propositions_text_form.php
$lang['duplicate_variants'] = 'Duplicate the variants';

// templates/question/propositions_text_form.php
$lang['add_text_question_proposition'] = 'Add a suggestion';

// templates/question/question_form.php
$lang['date_question'] = 'Question Date';
$lang['text_question'] = 'Question Text';
$lang['delete_question'] = 'Delete question';

// templates/survey/create.php
$lang['survey_create'] = 'create a survey';

// templates/survey/create.php
// templates/survey/update.php
$lang['previous'] = 'Previous';

// templates/survey/create.php
// templates/survey/update.php
$lang['save_draft'] = 'Save draft';

// templates/survey/create.php
// templates/survey/update.php
$lang['next'] = 'Next';

// templates/survey/manage.php
// templates/survey/manage.php
$lang['edit'] = 'Edit';

// templates/survey/manage.php
// templates/survey/manage.php
// templates/survey/reply.php
$lang['show_results'] = 'Check the survey results';

// templates/survey/manage.php
// templates/survey/manage.php
$lang['send_invitations_reminder'] = 'Send invitations reminder';

$lang['confirm_sending_invitations_reminder'] = 'Are you sure you want to send invitations reminder';

// templates/survey/manage.php
// templates/survey/manage.php
$lang['duplicate'] = 'Duplicate';

// templates/survey/manage.php
// templates/survey/manage.php
$lang['end_survey'] = 'End survey';

$lang['no_survey'] = 'No survey';

$lang['no_matching_survey'] = 'No matching survey.';

// templates/survey/manage.php
// templates/survey/manage.php
$lang['reply'] = 'Answer the survey';

// templates/survey/manage.php
$lang['search_in_surveys'] = 'Search in surveys';

// templates/survey/manage.php
$lang['owned_by'] = 'Organiser';

// templates/survey/manage.php
$lang['invited_to'] = 'Guest';

// templates/survey/manage.php
// templates/survey/accordion/accordion.php
$lang['opened'] = 'In progress';

// templates/survey/manage.php
// templates/survey/accordion/accordion.php
// templates/survey/accordion/accordion.php
$lang['draft'] = 'Draft';

// templates/survey/manage.php
$lang['surveys'] = 'Surveys';

// templates/survey/manage.php
$lang['filters'] = 'Filters';

// templates/survey/manage.php
// templates/survey/accordion/accordion.php
$lang['open'] = 'Open';

// templates/survey/reply.php
$lang['draft_message'] = 'This is a draft survey, you only (as owner) can preview this page ';

// templates/survey/reply.php
$lang['reply_to_survey'] = 'Answer the survey';

// templates/survey/reply.php
// templates/survey/update.php
// templates/survey/tools/short_authorization.php
$lang['survey'] = 'Survey';

// templates/survey/reply.php
//$lang['now_replying_as'] = 'Answer the survey as';
$lang['now_replying_as'] = 'as';

// templates/survey/reply.php
$lang['show_others_answers'] = 'Check the participants answers';

// templates/survey/results.php
$lang['results_of_survey'] = 'Survey results';

// templates/survey/update.php
$lang['general'] = 'General';

// templates/survey/wizard/step_general.php
$lang['parameters'] = 'Settings';

// templates/survey/wizard/step_general.php
$lang['surveys_title'] = 'Your Survey title';

// templates/survey/wizard/step_general.php
$lang['advanced_options'] = 'Advanced options';
$lang['no_delete_survey_owner'] = 'You can\'t edit or delete your own email';

// templates/survey/wizard/step_general.php
$lang['in_results_display'] = 'In results display';
$lang['in_input_of_participants_answers'] = 'During entry of participants answers';

// templates/survey/wizard/step_guests.php
$lang['invite_repliers'] = 'People that receive invitation to answer this survey';
$lang['email_with_link'] = 'Once the survey is created, an email with the access link will be sent to all the guests (emails entries) via this tool. 
the access link to this survey will be visible to the organiser on "manage the survey".';

// templates/survey/wizard/step_guests.php
$lang['add_guest'] = 'Add guest';

$lang['add_owner'] = 'Add owner';

// templates/survey/wizard/step_guests.php
$lang['share_survey'] = 'Share the survey';

// templates/survey/wizard/step_guests.php
$lang['survey_share_link_not_ready'] = 'The share link is not available yet';

// templates/survey/tools/short_authorization.php
$lang['tip_opened_to_everyone'] = 'Open to everyone';

// templates/survey/tools/short_authorization.php
$lang['tip_opened_to_authenticated'] = 'Open to authenticated users';

// templates/survey/tools/short_authorization.php
$lang['tip_opened_to_guests'] = 'Open to guests only';

// templates/survey/tools/short_authorization.php
$lang['tip_opened_to_noone'] = 'Open to none';

// templates/survey/accordion/accordion.php
$lang['all'] = 'Everyone';

// templates/survey/accordion/accordion.php
$lang['last_update'] = 'Last update of the organiser';

// templates/survey/accordion/accordion.php
$lang['nb_questions'] = 'Number of questions';

// templates/survey/accordion/accordion.php
$lang['nb_participants'] = 'Number of participants';

// templates/survey/accordion/accordion.php
$lang['link_to_survey'] = 'Link to this survey';

// templates/survey/accordion/accordion.php
$lang['show_more'] = 'Show more surveys';

// templates/participant/participants_table.php
$lang['your_choice'] = 'Your choice';

// view/js/components/survey_general_controller.js
// view/js/components/survey_questions_controller.js
// view/js/components/survey_wizard.js
// view/cache/script.js
$lang['please_set_title'] = 'Please set a title';

// view/js/components/survey_manage.js
$lang['confirm_survey_duplication'] = 'Are you sure you want to duplicate the survey?';

// view/js/components/survey_manage.js
$lang['survey_duplicated'] = 'Survey duplicated';

// view/js/components/survey_manage.js
// view/cache/script.js
$lang['confirm_survey_deletion'] = 'Are you sure you want to permanently delete the survey';

// view/js/components/survey_manage.js
// view/cache/script.js
$lang['survey_deleted'] = 'Survey deleted';

// view/js/components/survey_manage.js
// view/cache/script.js
$lang['confirm_survey_ending'] = 'Are you sure you want to end the survey';

// view/js/components/survey_questions_controller.js
// view/cache/script.js
$lang['title_empty_error'] = 'No title set.';

// view/js/components/survey_questions_controller.js
// view/cache/script.js
$lang['proposition_empty_error'] = 'Empty suggestion.';

// view/js/components/survey_questions_controller.js
// view/cache/script.js
$lang['no_proposition_found'] = 'No suggestion found';

// view/js/components/survey_reply.js
$lang['answer_saved'] = 'Saved answer';

// view/js/components/survey_reply.js
$lang['answers_saved'] = 'Saved answers';

// view/js/components/survey_reply.js
$lang['answers_saved_info'] = 'Your answers have been saved. Thank you for your participation.';

$lang['answers_saved_declarative_complement'] = 'You will receive a message with an access link to edit your answers.';

$lang['you_can_edit_your_answers'] = 'You can edit your answers';

$lang['you_will_be_redirected_to_edition_page_in'] = 'you will be redirected to the page for editing your answers ';

$lang['you_will_be_redirected_to_manage_page_in'] = 'You will be redirected to the survey management page in';

$lang['time_seconds'] = 'seconds';

$lang['here'] = 'here';

// view/js/components/survey_wizard.js
// view/cache/script.js
$lang['draft_saved'] = 'Draft saved';

$lang['comeback_to_edition'] = 'Back to edition';

// view/js/components/survey_wizard.js
// view/cache/script.js
$lang['survey_saved'] = 'Saved Survey';


$lang['survey_saved_info'] = 'The survey is saved, it is open now. <br /> You can edit, close and delete it from the management interface.';

$lang['saving_survey'] = 'Saving the survey';

$lang['saving_update'] = 'Saving changes';

// view/cache/script.js
$lang['dp_date_format'] = 'DD/MM/YYYY';

// view/cache/script.js
$lang['dp_datetime_format'] = 'DD/MM/YYYY HH:mm';

// view/cache/script.js
$lang['dp_time_format'] = 'HH:mm';

// view/cache/script.js
$lang['readable_date_format'] = 'DD/MM/YYYY';

// view/cache/script.js
$lang['readable_datetime_format'] = 'DD/MM/YYYY HH:MM';

// view/cache/script.js
$lang['readable_time_format'] = 'HH:MM';

// view/cache/script.js
$lang['no_results'] = 'No answer';

// view/cache/script.js
$lang['reproduce_times_everyday'] = 'Duplicate time to all dates';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['disable_answer_edition'] = 'Disable answer edition';

// templates/survey/wizard/step_questions.php
$lang['add_other_question'] = 'Add TEXT question';
$lang['add_date_question'] = 'Add DATE question';
$lang['add_other_question_legend'] = 'Activities, places, etc.';
$lang['add_date_question_legend'] = 'Schedule, days';
$lang['compress'] = 'Display a compressed view';
$lang['expand'] = 'Display a detailed view';
$lang['compress_legend'] = 'Reorganize the questionnaire';
$lang['compress_expand_view_info'] = 'With the compressed view of your questionnaire, you can reorganize the questions more simply by moving them up or down';

// templates/question/propositions_date_form.php
$lang['slot'] = 'Time slot';
$lang['whole_day'] = 'Whole day';
$lang['add_date_propositions'] = 'Add date suggestions';
$lang['duplicate_times'] = 'Replicate times for all dates';
$lang['add_date'] = 'Add date';

// templates/question/propositions_text_form.php
$lang['add_text_propositions'] = 'Add now your text suggestion';

// view/js/components/survey_answers.js
$lang['comment_unmasked'] = 'Unmasked comment';

// view/js/components/survey_answers.js
$lang['comment_masked'] = 'Hidden comment';

// view/js/components/survey_manage.js
$lang['survey_closed'] = 'Closed survey';

$lang['to_hour'] = '-';

$lang['to_day'] = 'to';

$lang['please_login'] = 'To create or manage your surveys, you must be authenticated.';

$lang['please_login_to_access_user_page'] = 'To manage your profile, you must be authenticated.';

$lang['access_denied'] = 'Access denied';

$lang['requires_authentication'] = 'To answer this survey, you should login.';

$lang['requires_invitation'] = 'To answer this survey, you must be invited.';

$lang['survey_not_found'] = 'This survey is not available.<br/> It may not exist, has been deleted, or there is an error in the link you used.';

$lang['your_name'] = 'First and LAST NAME';

$lang['your_email'] = 'Email';

$lang['several_possible_choices'] = 'Several choices are possible{if:count>1} among {count} suggestions{endif}';

$lang['one_possible_choice'] = 'One possible choice {if:count>1} among {count} suggestions{endif}';

$lang['now_replying_anonymously'] = 'You answer the survey anonymously.';

$lang['participant_name'] = 'Participant name';

$lang['participant_count'] = '{count} Participant(s)';

$lang['toggle_decision_making_tool'] = 'Refine your results';

$lang['toggle_decision_making_tool_info'] = 'Hide optional participants answers to update totals and refine your results. Sharing the survey will not hide optional participants';

$lang['mask_comment'] = 'Hide comment';

$lang['unmask_comment'] = 'show comment';

$lang['add_a_constraint'] = 'Add a constraint';

$lang['constraint'] = 'Constraint';

$lang['all_propositions'] = 'All suggestions';

$lang['limit_choice'] = 'Limit number of selection';

$lang['value'] = 'Value';

$lang['survey_is_closed_since'] = 'The survey is closed since';

$lang['survey_closes_soon'] = 'The survey will close soon';

$lang['limited_participant_number'] = 'The number of participants is limited';

$lang['limited_participant_number_example'] = '« Example : You invite 100 people to a conference in a room that can
Contain 50 people. You limit answers to the first 50 participants. »';

$lang['number_left'] = '%d left';

$lang['disable_answer_edition_example'] = '« Example : Participants may only answer one time to the survey. Once the answers are submitted, they cannot be edited »';

$lang['answer_edition_is_disabled'] = 'Your choices cannot be edited once they are submitted';

$lang['owned_by_tip'] = "The surveys that you have created";

$lang['invited_to_tip'] = "The surveys that you have been invited to";

$lang['download_csv'] = "Export results to CSV";

$lang['auto_close_reason'] = "Automatic close";

$lang['participant_limit_reached_reason'] = "The limit number of participants has been reached";

$lang['requested_by_owner_reason'] = "Closing requested by the owner";

$lang['you_answered'] = "You have participated.";

$lang['you_have_not_answered'] = "You haven't participated yet.";

$lang['answered'] = "Answered";

$lang['not_answered'] = "Unanswered";

$lang['organized_by'] = "Organized by";

$lang['yourself'] = "yourself";

$lang['survey_is_closed'] = "The survey is closed";

$lang['send_token_email_notice'] = "Send an email with token access to this participant.";

$lang['validate_final_answers'] = "Validate final answers";

$lang['validate_answers'] = "Validate the answers";

$lang['selected_final_answer'] = "Selected final answer";

$lang['show_answers'] = 'Show participants answers';

$lang['final_answer'] = "Final answer";

$lang['satisfaction_rate'] = "Satisfaction rate";

$lang['copy_to_clipboard'] = "Copy to clipboard";

$lang['copy_link_to_clipboard'] = "Copy link to clipboard";

$lang['copied_to_clipboard_success'] = "Copied to clipboard successfully";

$lang['clipboard_not_supported'] = "Clipboard not supported";

$lang['send_invitation_to_new_guests'] = "Send invitation to new guests?";

$lang['time_zone'] = "Time zone";

$lang['add_time_picker'] = "Add a field";

$lang['guest_email_input_notice'] = "Enter one or more valid emails";

$lang['constraints'] = "Constraints";

$lang['undefined'] = "Undefined";

$lang['participant_invalid_token'] = "Participation invalid token.";

$lang['dont_receive_invitation_copy'] = "Don't receive a copy of the invitation.";

$lang['left_arrow_notice'] = "Double click to go to first suggestion";

$lang['right_arrow_notice'] = "Double click to go to last suggestion";

$lang['selected_value_yes_text_notice'] = $lang['selected_value_yes'];

$lang['selected_value_maybe_text_notice'] = $lang['selected_value_maybe'];

$lang['selected_value_no_text_notice'] = $lang['selected_value_no'];

$lang['selected_value_yes_date_notice'] = "I am available on this date";

$lang['selected_value_maybe_date_notice'] = "I can be present on this date";

$lang['selected_value_no_date_notice'] = "I cannot be present on this date";

$lang['select_for_all'] = "Select for all";

$lang['owners'] = "Owners";

$lang['owner_name_input_notice'] = "Enter a name";

$lang['owner_email_input_notice'] = "Enter the owner email that you wish to add";

$lang['invitation_sent'] = 'Invitation sent';

$lang['comment_of'] = 'Comment of';

$lang['on_date'] = 'On';

$lang['masked_not_shared'] = 'Hiding the comment allows you not to spread it in the shared results.';

$lang['administration_of'] = 'Administration of';

$lang['administration_actions'] = 'Administration tools';

$lang['survey_management'] = 'Surveys management';

$lang['usage_stats'] = 'Usage stats';

$lang['opened_survey_count'] = 'Number of opened surveys';

$lang['total_survey_count'] = 'Total number of created surveys ';

$lang['draft_survey_count'] = 'Number of draft surveys';

$lang['closed_survey_count'] = 'Number of closed surveys';

$lang['all_surveys'] = 'All surveys';

$lang['uneditable_answer'] = 'You cannot edit this answer anymore';

$lang['unavailable_answer'] = 'You cannot select this suggestion anymore.';

$lang['invalid_question'] = 'There are missing data for this question';

$lang['token_sent'] = 'Access link sent';

$lang['sending_recovery_email'] = 'Sending the recovery email in progress.';

$lang['invitations_sent'] = 'The invitations reminder have been sent.';

$lang['help'] = 'Help';

$lang['about'] = 'About';

$lang['give_us_feedback'] = 'User feedback';

$lang['connected_user'] = 'Connected user';

$lang['guests_added'] = 'Added guests';

$lang['constraint_value_placeholder'] = 'Ex. : 100';

$lang['no_known_constraints'] = 'No constraint';

$lang['participant_already_exists_for_survey'] = 'A participant with this email address has already participated to this survey.';

$lang['please_select_value_for_question'] = 'Please select a suggestion for this question';

$lang['add_guest_notice'] = '<p>Click on "'.$lang['add_guest'].'" to add a guest.</p>'
    . '<p>Registered guests will automatically receive an email inviting them to answer.</p> '
    . '<p class="italic">Note: In the Email field you can enter one or several addresses separated by a \';\'</p> ';
    
$lang['add_proposition_variants'] = 'Add variants';

$lang['confirm_question_deletion'] = 'Are you sure you want to delete the question';

$lang['answers_cannot_be_edited'] = 'Your answers cannot be edited anymore.';

$lang['end_date_prior_to_base_date'] = 'End date is prior to start date.';

$lang['end_time_prior_to_base_time'] = 'End time is prior to start time.';

$lang['answer_proposition_not_available'] = 'A suggestion is no more available.';

$lang['survey_action_not_allowed'] = 'You are not allowed to perform this action on the survey. Only survey owners are allowed.';

$lang['closed_survey_with_final_answer_count'] = 'Number of closed surveys with final answer';

$lang['participants_count'] = 'Number of participation to this survey';

$lang['authenticated_participants_count'] = 'Number of authenticated participants';

$lang['declarative_participants_count'] = 'Declarated participation number';

$lang['answer_survey_closed'] = 'The survey is closed, your answers cannot be saved.';

$lang['answer_draft_survey'] = 'You cannot answer a survey draft.';

$lang['participant_not_allowed'] = 'You are not allowed to participate to this survey.';

$lang['no_participant_found'] = 'Your participation cannot be saved.';

$lang['no_way_to_identify'] = 'You cannot be identified';

$lang['headers'] = 'Titles';

$lang['bold'] = 'Bold';

$lang['italics'] = 'Italic';

$lang['ul'] = 'Bullets list';

$lang['ol'] = 'Ordered list';

$lang['table'] = 'Table';
$lang['title_1'] = 'Title 1';
$lang['title_2'] = 'Title 2';
$lang['title_3'] = 'Title 3';
$lang['title_4'] = 'Title 4';
$lang['title_5'] = 'Title 5';
$lang['title_6'] = 'Title 6';

$lang['link'] = 'Hyperlink';
$lang['img'] = 'Image';
$lang['blockquote'] = 'Quote';
$lang['codeblock'] = 'Codeblock';
$lang['code'] = 'Code';
$lang['footnote'] = 'Footnote';
$lang['horizontal_rule'] = 'Horizontal rule';

$lang['more_details'] = 'More details';

$lang['recovery_email_could_not_be_sent_try_later'] = 'Access link could not be sent for the moment';

$lang['no_question_found'] = 'Please add a question by clicking on one of the 3 buttons below';

$lang['participant_has_already_answered_to_survey'] = 'You have already answered this survey. Try to refresh your page to check your answers.';

$lang['constraint_value_info'] = '« Example : You only have 10 available seats for a schedule. You limit the choice of this suggestion to 10. »';

$lang['key'] = 'key';

$lang['avg_count'] = 'Average number of surveys per owner';

$lang['max_count'] = 'Maximum number of surveys per owner';

$lang['support'] = 'Support';

$lang['user_guide_unreachable_title'] = 'User guide not found';
$lang['user_guide_unreachable_content'] = 'An error happened when trying to get the user guide.';
$lang['user_guide_unreachable_download'] = 'By the way, you can download the PDF version ';

$lang['legal_notice'] = 'Legal Notice';

$lang['date_format'] = 'd/m/Y'; // Format for displaying date, use PHP date() format string syntax
$lang['datetime_format'] = 'd/m/Y H:i:s';

$lang['moment_date_format'] = 'DD/MM/YYYY';

$lang['moment_datetime_format'] = 'DD/MM/YYYY HH:mm';

$lang['moment_date_format_readable'] = 'ddd.<br/> DD <br/>MMM.';

$lang['moment_datetime_format_readable'] = 'ddd. DD MMM. YYYY HH:mm';

$lang['updating_survey'] = 'Mise à jour du sondage';

$lang['close_modal'] = 'Close';

$lang['page_not_found'] = 'Requested page cannot be found.';

$lang['send_update_notification'] = 'You updated your survey.<br/> Do you wish to send an update notification ?';

$lang['send_update_notification_and_invitation_to_new_guests'] = 'Vous avez modifié votre sondage.<br/> Souhaitez vous en informer les participants du sondage <br/> et envoyer une invitation aux nouveaux invités ?';

$lang['confirm_leave_page'] = 'Are you sure you want to leave this page ?';

$lang['delete_proposition'] = 'Delete proposition';

$lang['delete_date'] = 'Delete this date';

$lang['delete_time_picker'] = 'Delete this field';

$lang['proposition_header'] = 'Proposition title';

// templates/logout_page.php
$lang['logout_disclamer'] = 'You are disconnected of {cfg:application_name}.';

$lang['time_zone_info_hours_large'] = 'All dates/times are displayed in your time zone';
$lang['time_zone_info_hours_small'] = 'Your time zone';
$lang['time_zone_info_moments_large'] = 'All day moments are displayed in survey creation time zone';
$lang['time_zone_info_moments_small'] = 'Survey time zone';
$lang['time_zone_info_moments_info'] = 'Participants that are not in this timezone will see times in their own time zone.';

$lang['faq_title'] = 'Frequently asked questions';

$lang['all_surveys_tip'] = 'All surveys';

$lang['renater_logo'] = 'Renater logo';

$lang['download_csv_info'] = 'CSV export is encoded in UTF-8 with a {separators} separator.';
$lang['commas'] = 'comma';
$lang['semicolons'] = 'semicolon';

$lang['expand_reduce_view'] = 'Expand/Compress the view';

$lang['comment'] = 'Comment';

$lang['day_slot'] = 'Date range';

$lang['path'] = 'Path';

$lang['requires_authentication'] = 'Authentication is required';

$lang['pending_survey_detected'] = 'We detected a pending survey edition.<br> Do you want to continue ?';

$lang['connection_lost_try_to_reconnect'] = 'It appears that you lost your connection.<br> No worries, your survey is saved.<br> You will be able to continue when you are reconnected.<br> Reconnect ?';

$lang['survey_recovery_dialog'] = 'Survey recovery';

$lang['delete_pending_survey'] = 'Ignore this pending survey';

$lang['later'] = 'Later';

$lang['form_invalid_data'] = 'Invalid data';

$lang['participants_emails_dialog'] = 'Participants emails';

$lang['no_participant_email_found'] = 'No participant email found.';

$lang['participants_emails_with_yes_for_this_proposition'] = 'Participants emails of those who answered "yes" for this proposition';

$lang['display_participants_who_answered_yes_to'] = 'Show participants emails of those who answered yes to';

$lang['toggle_participants_emails_option'] = 'Show participants emails options';

$lang['participants_emails'] = 'Emails';

$lang['toggle_participants_emails_option_info'] = 'Display participants emails';

$lang['display_participants_emails'] = 'Display all participants emails';

$lang['not_allowed_to_see_results'] = 'You are not allowed to see this survey results.';

$lang['distinct_users_count'] = 'Users count';

$lang['loading_answers'] = 'Loading answers';

$lang['avg_date_proposition_count'] = 'Average date proposition count';

$lang['avg_text_proposition_count'] = 'Average text proposition count';

$lang['max_date_proposition_count'] = 'Maximum date proposition count';

$lang['max_text_proposition_count'] = 'Maximum text proposition count';

$lang['survey_usage'] = 'Survey usage';

$lang['avg_participant_count'] = 'Average number of participants';

$lang['max_participant_count'] = 'Maximum number of participants';

$lang['edugain_idps'] = 'eduGAIN IDPs';

$lang['other_idps'] = 'Other IDPs';

// MOTD
$lang['i_got_it'] = "I got it";
$lang['motd_hint'] = 'If you click on "I got it", this message will not be displayed to you again and this information will be stored in your cookies.';

$lang['moment_project'] = "Moment Project on SourceSup";

$lang['terms'] = 'Terms';

$lang['accept_terms'] = 'I accept the <a href="{url:terms}" target="_blank">terms.</a>';

$lang['accept_terms_required'] = 'You must accept the terms';

$lang['log_in_tip'] = 'Log in';

$lang['my_calendars'] = 'My calendars';

$lang['connect_my_calendars'] = 'Connect my calendars';

$lang['connect_my_calendars_info'] = 'You can add your calendars in Moment. You will then be able to view your unavailabilities during your answers or your survey creations.';

$lang['connect_my_moment_calendar'] = 'Connect my Moment calendar';

$lang['connect_my_moment_calendar_info'] = 'Add this URL to your calendar application to see your Moment responses as events.';

$lang['no_calendar_added_yet'] = 'No calendar have been added yet';

$lang['extension_allowed'] = 'Moment supports links to ics (iCalendar) and ifb (Free/Busy) files.' ;

$lang['calendar_usage'] = 'Only calendar URLs are stored. Calendars are downloaded only when showing conflicts/inavailability when answering/managing your surveys. Calendar content is not stored. You can delete calendar URL at any time';

$lang['add_calendar'] = 'Add a calendar';

$lang['access_to_calendar_timed_out'] = 'Calendar access timed out';

$lang['access_to_calendar_forbidden'] = 'Calendar access is not allowed';

$lang['access_to_calendar_failed'] = 'Calendar access failed';

$lang['calendar_unavailable'] = 'Calendar unavailable';

$lang['calendar_not_found'] = 'This calendat cannot be found';

$lang['calendar_is_oversized'] = 'Size of this calendar exceeds the size limit ('.round(Config::get('calendar.max_size') / 1024 / 1024,1).'MB)';

$lang['something_went_wrong_when_checking_calendar'] = 'Something went wrong when checking calendar';

$lang['calendar_url_placeholder'] = 'https://...ics';

$lang['confirm_calendar_deletion'] = 'Are you sure you want to delete this calendar %s from your list&nbsp;?';

$lang['calendar_removed_with_success'] = 'Calendar removed with success';

$lang['calendar_added_with_success'] = 'Calendar added with success';

$lang['calendar_updated_with_success'] = 'Calendar updated with success';

$lang['calendar_removal_failed'] = 'Calendar removal failed';

$lang['calendar_adding_failed'] = 'Calendar adding failed';

$lang['calendar_updating_failed'] = 'Calendar updating failed';

$lang['saving_calendar'] = 'Saving calendar';

$lang['updating_calendar'] = 'Updating calendar';

$lang['did_you_know'] = 'Did you know ?';

$lang['view_calendars'] = 'View calendars';

$lang['view_calendars_info'] = 'When I create or when I answer a survey, I can see the reserved slots or my answers in my calendar. ';

$lang['loading_calendars'] = 'Loading calendars';

$lang['calendar_is_not_compatible'] = 'Calendar not compatible';

$lang['leaving_survey_creation_form'] = 'Leaving survey creation form';

$lang['survey_data_will_be_saved_in_draft'] = 'Survey data will be saved as a draft.<br/><br/>'
    .'You can then find this draft in “Manage surveys”.';

$lang['all_day'] = 'All day';

$lang['from_hour'] = 'From';

$lang['from_day'] = 'From';

$lang['conflict'] = 'Detected conflict';

$lang['conflicts'] = 'Detected conflicts';

$lang['hide_conflicts'] = 'Hide date conflicts';

$lang['hide_conflicts_info'] = 'Conflicts are shown with a <span class=\'calendar-info neutral fa fa-exclamation-triangle\'></span>. Hovering conflicts will show you events conflicting.';

$lang['conflicts_are_now_loaded'] = 'Your calendars have correctly been loaded, possible conflicts are shown';

$lang['conflicts_loading_failed'] = 'Calendars loading failed, please try again later';

$lang['loading_calendars_unavailability_failed'] = 'Calendars loading failed.';

$lang['my_profile'] = 'My profile';

$lang['unavailability'] = 'Unavailabilities';

$lang['learn_more_about_diff_ics_ifb'] = 'Learn more about the difference between ics and ifb...';

$lang['learn_more_about_getting_ics_ifb'] = 'Learn more about how to get ics or ifb URL of your calendar...';

$lang['learn_more'] = 'Learn more...';

$lang['access_user_guide_moment_calendar'] = 'Access the user guide to see how to import the Moment calendar into your agenda';

$lang['regenerate_link'] = 'Regenerate calendar link (Warning previous link will not be valid anymore)';

$lang['calendar_name'] = 'Calendar name';

$lang['calendar_name_placeholder'] = 'Fill in calendar name';

$lang['calendar_color'] = 'Customize calendar color';

$lang['filter_my_calendars'] = 'Filter my calendars';

$lang['filter_my_calendars_tip'] = 'Filter your calendars allows you to manage your unavailability display.<br/> You can select one ore more calendar to display only corresponding events.<br/> You can disable this functionality by unselecting all calendars.';

$lang['all_calendars'] = 'All';

$lang['calendar_url_constraint'] = 'Mandatory, URL must be accessible and point to an ics (iCalendar) or ifb (Free/Busy) calendar';

$lang['calendar_link_regenerated'] = 'Calendar link has been regenerated.';

$lang['calendar_link_regenerate_failed'] = 'Calendar link regeneration failed.';

$lang['confirm_refresh_calendar_link'] = 'Are you sure you want to refresh your calendar link ?<br/> (Warning previous link will not be valid anymore)';

$lang['please_add_at_least_one_proposition'] = 'You must add at least one proposition';

$lang['no_date_selected'] = 'No dates selected.<br/>You can choose them from the date picker, you can also specify times or time slots if needed.';

$lang['no_date_slot_selected'] = 'No date slot selected.<br/>You can choose them from the date picker. To do this, click on the start date, and then on the end date.';


$lang['reopen_survey'] = 'Reopen the survey';

$lang['survey_reopening_title'] = 'Reopening of the survey';

$lang['select_new_auto_close_date'] = 'Please select a new close date :';

$lang['select_new_participant_limit_nb'] = 'The limit number of participant have been reached (%nb_participants%) to that survey have been reached. To reopen the survey, you should increase the limit :';

$lang['invalid_new_auto_close_date'] = 'The new date is not valid';

$lang['limit_participants_nb_too_low'] = 'The new limit for the number of participants is too low';

$lang['reopen_survey_disabled'] = 'It is not possible to reopen this survey. Either it is not possible to postpone the closing date or there is a final answer selected.';

$lang['survey_reopened'] = 'The survey has been reopened';

$lang['please_select_value_for_all_questions'] = 'Please select a suggestion for all questions';

$lang['start_date'] = 'Start date';

$lang['end_date'] = 'End date';

$lang['same_hours_for_all'] = 'Same hours for all days';

$lang['input_mode'] = 'Input mode';
$lang['input_mode_hours'] = 'Hours';
$lang['input_mode_moments'] = 'Day moments';
$lang['same_moments_for_all'] = 'Same times for all days';
$lang['input_mode_info_hours'] = 'The &quot;Hours&quot; entry allows you to specify custom schedules or time slots.';
$lang['input_mode_info_moments'] = 'The &quot;Day moments&quot; entry allows you to specify predefined time slots :';
$lang['input_mode_info_moments_row'] = 'A proposition {name} corresponds to a time slot &quot;{bt} to {et}&quot;';
$lang['moments_of_day_info'] = 'Response proposals';
$lang['morning'] = 'Morning';
$lang['noon'] = 'Noon';
$lang['afternoon'] = 'Afternoon';
$lang['evening'] = 'Evening';

$lang['please_select_dates'] = 'Please select dates';

$lang['changing_to_day_slots_will_delete_your_propositions'] = 'Changing to day slots your propositions will be deleted';

$lang['days'] = 'days';

$lang['changing_from_day_slots_will_delete_your_propositions'] = 'Changing to hour slots your propositions will be deleted';

$lang['question_statement'] = 'Statement of the question';

$lang['optional'] = 'optional';

$lang['please_set_question_statement'] = 'Please set a statement';

$lang['force_unique_choice_explanation'] = 'If this option is toggled, partcipants can only select one of the propositions for this question';

$lang['invalid_range_start_date'] = 'Invalid start date';

$lang['invalid_range_end_date'] = 'Invalid end date';

$lang['add_date_range_question'] = 'Add a DATE RANGE question';

$lang['add_date_range_question_legend'] = 'Multi-day period';

$lang['your_date_range_propositions'] = 'Your date range propositions';

$lang['your_date_range_propositions_info'] = 'You can choose your date ranges on the calendar. You may click on the start date and then the end date.';

$lang['your_date_propositions'] = 'Your date propositions';

$lang['your_date_propositions_info'] = 'You can choose your days on the calendar. You may click on the desired date';

$lang['your_text_propositions'] = 'Your text propositions';

$lang['add_proposition_variant'] = 'Add a variant';

$lang['same_variants_for_all'] = 'Same variants for all propositions';

$lang['question_has_invalid_inputs'] = 'There are invalid data for this question';

$lang['you_must_select_at_least_one_proposition'] = 'You must select at least one proposition';

$lang['monthly'] = 'monthly';

$lang['yearly'] = 'yearly';

$lang['daily'] = 'daily';

$lang['you_already_answered_this_survey'] = 'Participant duplicate detected';

$lang['declarative_participant_already_exists'] = 'We send an email to this address to confirm your identity.';

$lang['authenticated_participant_already_exists'] ='Please authenticate to confirm the modification.';

$lang['participant_with_this_email_already_exists'] = 'An answer linked to this email address has already been given to this survey.';

$lang['update_answer'] = 'Update my answers';

$lang['please_fill_in_the_code'] = 'Please fill in the code that you received by mail to validate the modification :';

$lang['enter_the_code_here'] = 'Enter the code';

$lang['send_me_another_recovery_code'] = 'Send another code';

$lang['n_tries_left'] = 'Invalid code provided (%d trie(s) left)';

$lang['too_much_tries'] = 'You made too much tries. You should reload the page.';

$lang['answers_modification_impossible'] = 'Answers modification impossible';

$lang['participant_with_this_email_already_exists_edition_disabled'] = 'Answers with this email address already exists.'
    .' Unfortunatly, the owner disabled answer edition. Thus we can\'t take your request into account.';

$lang['declarative_participant_already_exists_edition_disabled'] = 'You can recover your previous answers by clicking the following link :';

$lang['authenticated_participant_already_exists_edition_disabled'] = 'You can authenticate to view your previous answers.';

$lang['send_link_to_previous_answers'] = 'Send a mail with a link to previous answers';

$lang['your_recovery_email_is_sent'] = 'An email with the link to your previous answers has been sent';

$lang['i_already_answered_to_this_survey'] = 'I already answered and i want to recover my previous answers';

$lang['i_already_answered_to_this_survey_explanation'] = 'If you already answered this survey, you should use the link'
    .' that has been sent to update or delete your answers.'
    .' If you lost this mail, you can ask for a re-send by clicking the following link.';

$lang['please_fill_in_the_email_so_that_we_can_send_recovery_email'] = 'Please fill in the address used with your'
    .' previous participation so we can resend a modification link.';

$lang['recovery_email_has_been_sent'] = 'An email has been sent to the entered address (I may take a few minutes).';

$lang['recovery_email_has_been_sent_explanation'] = 'If you already answered this survey, you will have to click on the link in the mail.'
    .'You will access your answers directly.'
    .'If the mail says no participation have been found for this email address, it\'s because you haven\'t answered with this one';

$lang['sending_recovery_code'] = 'Sending recovery code';

$lang['recovery_code_sent'] = 'Recovery code sent';

$lang['delete_answers'] = 'Delete my answers';

$lang['confirm_answers_deletion'] = 'Are you sure you want to delete your answers to the survey';

$lang['your_answers_have_been_deleted'] = 'Your answers have been deleted';

$lang['deleting'] = 'Deleting';

$lang['answers_cannot_be_deleted'] = 'Your answers cannot be deleted anymore.';

$lang['if_dont_receive_code'] = 'If you did not receive the email with the code, check your spam, otherwise you can send a new code by clicking on the link';

$lang['no_code'] = 'Inactive because no code entered';

$lang['validating'] = 'Validating';

$lang['logon'] = 'Log-on';

$lang['accessibility'] = 'Accessibility';

$lang['sum_yes_plus_maybe'] = 'The "Yes" + "Maybe"';

$lang['maybe_answers_disabled'] = '<span class="maybe-answer-disabled-label-maybe">"Maybe"</span> answer is not allowed';

$lang['maybe_answers_disabled_info'] = 'The survey owner does not want &#34;Maybe&#34; answers';

$lang['confirm_survey_deletion_warning'] = 'Deletion is permanent. The survey cannot be restored.';

$lang['my_preferences'] = 'My preferences';

$lang['access_link_to_the_survey'] = 'Access link to the survey';

$lang['question_limit_choice'] = 'Limit of selections of a proposal';

$lang['question_limit_choice_nb'] = 'Number of selections of a proposal';

$lang['question_limit_choice_example'] = 'Example : You want to schedule individual appointments. You limit 1 participant per proposal.';

$lang['manage_constraints'] = 'Manage constraints';
$lang['see_constraints'] = 'See constraints';
$lang['some_constraints_are_defined'] = 'Some constraints have been defined for this question.';
$lang['to_define_constraint'] = 'To define limits for each proposal:';