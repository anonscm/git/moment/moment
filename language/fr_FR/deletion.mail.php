subject: Suppression du sondage : {survey.title}

{alternative:plain}

Suppression du sondage : {survey.title}

Le sondage "{survey.title}" a été supprimé.

Cordialement,
L'équipe {cfg:application_name}

=====================================================================

Deletion of the survey: {survey.title}

The survey "{survey.title}" has been deleted.

Regards,
The {cfg:application_name} Team

{alternative:html}

<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Suppression du sondage : "{survey.title}"</h1>
    <p>
        Le sondage "{survey.title}" a été supprimé.
    </p>
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Deletion of the survey: "{survey.title}"</h1>
    <p>
        The survey "{survey.title}" has been deleted.
    </p>
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>
