subject: {if:reason == 'reminder'}[Rappel] {endif}Invitation au sondage : {survey.title}

{alternative:plain}

Invitation au sondage : {survey.title}


{if:reason == 'reminder'}
Pour rappel :
{endif}
Vous avez été invité à répondre au sondage "{survey.title}"{if:survey.owner_names != ''} par {survey.owner_names}{endif}.

Lien d'accès : {survey_direct_link}

Cordialement,
L'équipe {cfg:application_name}

=====================================================================

Invitation to survey: {survey.title}

{if:reason == 'reminder'}
Reminder:
{endif}
You've been invited to answer the survey "{survey.title}"{if:survey.owner_names != ''} by {survey.owner_names}{endif}.

Access link: {survey_direct_link}

Regards,
The {cfg:application_name} Team

{alternative:html}

<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Invitation au sondage : {survey.title}</h1>
    <p>
        {if:reason == 'reminder'}
        Pour rappel : <br />
        {endif}
        Vous avez été invité à répondre au sondage "{survey.title}"{if:survey.owner_names != ''} par {survey.owner_names}{endif}.
    </p>
    <p>
        <a href="{survey_direct_link}" style="{cfg:email.style.green_button}">Accéder au sondage</a>
    </p>
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
   <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Invitation to survey: {survey.title}</h1>
    <p>
        {if:reason == 'reminder'}
        Reminder: <br />
        {endif}
        You've been invited to answer the survey "{survey.title}"{if:survey.owner_names != ''} by {survey.owner_names}{endif}.
    </p>
    <p>
        <a href="{survey_direct_link}" style="{cfg:email.style.green_button}">Access the survey</a>
    </p>
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>
