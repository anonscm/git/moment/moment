subject: Rappel de suppression prochaine du sondage : "{survey.title}"

{alternative:plain}

Rappel de suppression prochaine du sondage : "{survey.title}"

Le sondage "{survey.title}" sera supprimé automatiquement dans {cfg:removal_reminder_delay} jours.

Cordialement,
L'équipe {cfg:application_name}

=====================================================================

Reminder of the deletion of the survey: "{survey.title}"

The survey "{survey.title}" will be deleted in {cfg:removal_reminder_delay} days.

Regards,
The {cfg:application_name} Team

{alternative:html}

<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Rappel de suppression prochaine du sondage : "{survey.title}"</h1>
    <p>
        Le sondage "{survey.title}" sera supprimé automatiquement dans {cfg:removal_reminder_delay} jours.
    </p>
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Reminder of the deletion of the survey: "{survey.title}"</h1>
    <p>
        The survey "{survey.title}" will be deleted in {cfg:removal_reminder_delay} days.
    </p>
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>
