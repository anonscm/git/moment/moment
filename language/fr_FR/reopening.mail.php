subject: Réouverture du sondage : "{survey.title}"

{alternative:plain}

Réouverture du sondage : "{survey.title}"

Le sondage "{survey.title}" a été réouvert. Vous pouvez de nouveau y répondre.

Lien d'accès : {survey.path}

Cordialement,
L'équipe {cfg:application_name}

=====================================================================

Reopening of the survey : "{survey.title}"

The survey "{survey.title}" has been reopened. You can answer it again.

Access link: {survey.path}

Regards,
The {cfg:application_name} Team

{alternative:html}

<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Réouverture du sondage : "{survey.title}"</h1>
    <p>
        Le sondage "{survey.title}" a été réouvert. Vous pouvez de nouveau y répondre.<br />
    </p>
    <p>
        <a href="{survey.path}" style="{cfg:email.style.green_button}">Accéder au sondage</a>
    </p>
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Reopening of the survey: "{survey.title}"</h1>
    <p>
        The survey "{survey.title}" has been reopened. You can answer it again.<br />
    </p>
    <p>
        <a href="{survey.path}" style="{cfg:email.style.green_button}">Access the survey</a>
    </p>
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>