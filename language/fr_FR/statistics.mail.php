subject: Statistiques {periodicity} de {cfg:application_name}

{alternative:plain}

Statistiques {periodicity}

Service {cfg:application_url}


Cordialement,
L'équipe {cfg:application_name}

{alternative:html}

<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">
        Statistiques {periodicity} (Service {cfg:application_url})
    </h1>
    <table style="border:1px solid #304c6f; border-collapse: collapse; background-color:#fff;color:#304c6f;width:90%; margin:0 auto;padding:1rem;">
        <tr><th style="padding:0.5rem 0;border:1px solid #304c6f; border-collapse: collapse;" colspan="{column_count}">Du {periodicity_start_date} au {periodicity_end_date}</th></tr>
        <tr>
            {each: keys as key}
            <th style="text-align:left; padding:0.5rem 0.5rem 0.5rem 1rem; border:1px solid #304c6f; border-collapse: collapse;">
                {key}
            </th>
            {endeach}
        </tr>
        {each: stats as stat_row}
        <tr>
            {each: stat_row as stat_column}
            <td style="text-align:left; padding:0.5rem 0.5rem 0.5rem 1rem;border:1px solid #304c6f; border-collapse: collapse;">
                {stat_column}
            </td>
            {endeach}
        </tr>
        {endeach}
    </table>

</div>