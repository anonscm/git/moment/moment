subject: Réponses finales choisies : "{survey.title}"

{alternative:plain}

Réponses finales choisies pour : "{survey.title}" {if: ultimate_participant && ultimate_participant.name != ''} par {ultimate_participant.name} {endif}

{if: ultimate_participant && survey.has_date_questions}
Vous avez la possibilité de les intégrer dans votre agenda grâce au fichier joint "invite.ics".
{endif}

{if: survey.hide_answers == false && ultimate_participant }
Choix finaux :
    {each: ultimate_participant.Answers as answer}
    {answer.Question.statement} : {answer.raw_labels}

    {endeach}
{endif}

{if: survey.hide_answers == false}
Voir les résultats : {survey.results_path}
{endif}

Cordialement,
L'équipe {cfg:application_name}

=====================================================================

Ultimate answers chosen for : "{survey.title}" {if: ultimate_participant && ultimate_participant.name != ''} by {ultimate_participant.name} {endif}

{if: ultimate_participant && survey.has_date_questions}
You can add them to your agenda with the attachment "invite.ics".
{endif}

{if: survey.hide_answers == false && ultimate_participant }
Final choices:
{each: ultimate_participant.Answers as answer}
{answer.Question.statement} : {answer.raw_labels}

{endeach}
{endif}

{if: survey.hide_answers == false}
View results : {survey.results_path}
{endif}

Regards,
The {cfg:application_name} Team

{alternative:html}

<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Réponses finales choisies pour : "{survey.title}" {if: ultimate_participant && ultimate_participant.name != ''} par {ultimate_participant.name} {endif}</h1>
    {if: ultimate_participant && survey.has_date_questions}
    Vous avez la possibilité de les intégrer dans votre agenda grâce au fichier joint "invite.ics".
    {endif}
    {if: survey.hide_answers == false && ultimate_participant }
    <table style="{cfg:email.style.table}">
        <tr><th style="padding:0.5rem 0">Choix finaux :</th></tr>
        {each: ultimate_participant.Answers as answer}
        <tr><td style="text-align:left; padding:0.5rem 0.5rem 0.5rem 1rem;"><li>{answer.Question.statement} : <b>{answer.raw_labels}</b></li></td></tr>
        {endeach}
    </table>
    {endif}
    {if: survey.hide_answers == false}
    <p>
        <a href="{survey.results_path}" style="{cfg:email.style.green_button}">Voir les résultats</a>
    </p>
    {endif}
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Ultimate answers chosen for: "{survey.title}" {if: ultimate_participant && ultimate_participant.name != ''} by <b>{ultimate_participant.name}</b> {endif}</h1>
    {if: ultimate_participant && survey.has_date_questions}
    You can add them to your agenda with the attachment "invite.ics".
    {endif}
    {if: survey.hide_answers == false && ultimate_participant }
    <table style="{cfg:email.style.table}">
        <tr><th style="padding:0.5rem 0">Final choices:</th></tr>
        {each: ultimate_participant.Answers as answer}
        <tr><td style="text-align:left;padding:0.5rem 0.5rem 0.5rem 1rem;"><li>{answer.Question.statement} : <b>{answer.raw_labels}</b></li></td></tr>
        {endeach}
    </table>
    {endif}
    {if: survey.hide_answers == false}
    <p>
        <a href="{survey.results_path}" style="{cfg:email.style.green_button}">View results</a>
    </p>
    {endif}
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>