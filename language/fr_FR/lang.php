<?php
/**
 *     Moment - lang.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* ---------------------------------
 * fr_FR Language File
 * Maintained by Renater
 * ---------------------------------
*/

// classes/constants/ParticipantNotificationReasons.class.php
$lang['comment_created'] = 'Commentaire ajouté';

// classes/constants/ParticipantNotificationReasons.class.php
$lang['answer_created'] = 'Réponse ajoutée';

// classes/constants/ParticipantNotificationReasons.class.php
$lang['reminder'] = 'Rappel';

// classes/constants/QuestionOptions.class.php
$lang['enable_multiple_choices'] = 'Autoriser plusieurs choix';

// classes/constants/QuestionOptions.class.php
// templates/question/question_form.php
$lang['force_unique_choice'] = 'Choix unique';

// classes/constants/QuestionOptions.class.php
// templates/question/question_form.php
$lang['enable_maybe_choices'] = 'Autoriser la réponse peut-être';

// classes/constants/QuestionOptions.class.php
$lang['mandatory_answer'] = 'Réponse obligatoire';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
// templates/survey/accordion/accordion.php
$lang['auto_close'] = 'Date de clôture';
$lang['settings_auto_close'] = '<b>Date de clôture :</b> le sondage ne sera plus disponible à partir de la date sélectionnée<br><b>Date de suppression :</b> le sondage sera supprimé %d mois après la date de clôture';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['enable_anonymous_answer'] = 'Autoriser les réponses anonymes';

// classes/constants/SurveySettings.class.php
$lang['enable_answer_edition'] = 'Autoriser l\'édition des réponses';

// classes/constants/SurveySettings.class.php
$lang['enable_update_notification'] = 'Activer les notifications de mise à jour';

// classes/constants/SurveySettings.class.php
// templates/question/propositions_text_form.php
// templates/survey/wizard/step_general.php
$lang['limit_participants'] = 'Limiter le nombre de participants';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['limit_participants_nb'] = 'Nombre de participants maximum';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['reply_access'] = 'Qui peut répondre au sondage ?';
$lang['reply_access_info'] = '<b>Tous :</b> tous les utilisateurs, authentifiés ou non, peuvent répondre, le questionnaire est public.'
        . '<br /><b>Les utilisateurs authentifiés :</b> seuls les utilisateurs authentifiés sur la plate-forme peuvent répondre.'
        . '<br /><b>Les invités authentifiés :</b> seuls les utilisateurs authentifiés dont l\'e-mail a été ajouté en tant qu\'invité dans le questionnaire peuvent répondre';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['hide_answers'] = 'Masquer les résultats des autres participants';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['hide_comments'] = 'Masquer les commentaires des autres participants';

// classes/constants/SurveySettings.class.php
$lang['show_participant_name'] = 'Afficher le nom des participants';

// classes/constants/SurveySettings.class.php
$lang['show_participant_email'] = 'Afficher l\'e-mail des participants';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['dont_notify_on_reply'] = 'Ne pas être notifié lorsqu\'un participant répond';

// classes/constants/YesNoMaybeValues.class.php
$lang['selected_value_yes'] = 'Oui';

// classes/constants/YesNoMaybeValues.class.php
$lang['selected_value_no'] = 'Non';

// classes/constants/YesNoMaybeValues.class.php
$lang['selected_value_maybe'] = 'Peut-être';

// classes/model/Answer.class.php
// classes/model/DateProposition.class.php
// classes/model/Survey.class.php
// classes/model/TextProposition.class.php
$lang['Question'] = 'Question';

// classes/model/Comment.class.php
// classes/model/EventLog.class.php
// classes/model/Survey.class.php
// templates/survey/accordion/accordion.php
$lang['created'] = 'Créé';

// classes/model/Comment.class.php
// classes/model/Survey.class.php
$lang['Participant'] = 'Participant';

// classes/model/Guest.class.php
// classes/model/Participant.class.php
// classes/model/Survey.class.php
// templates/survey/wizard/step_guests.php
$lang['email'] = 'E-mail';

// classes/model/Guest.class.php
$lang['invitation_sent'] = 'Invitation envoyée';

// classes/model/Guest.class.php
// classes/model/Participant.class.php
// classes/model/Question.class.php
// classes/model/Survey.class.php
$lang['Survey'] = 'Survey';

// classes/model/Participant.class.php
// templates/survey/reply.php
// view/js/components/survey_guests_controller.js
// view/js/components/survey_reply.js
// view/js/components/survey_wizard.js
// view/cache/script.js
// view/lib/select2/js/select2/core.js
$lang['name'] = 'Nom';

// classes/model/Participant.class.php
$lang['answers_created'] = 'Réponses enregistrées';

// classes/model/Participant.class.php
$lang['answers_updated'] = 'Réponses mises à jour';

// classes/model/Participant.class.php
$lang['Comment'] = 'Commentaire';

// classes/model/Participant.class.php
// classes/model/Question.class.php
$lang['Answer'] = 'Réponse';

// classes/model/Participant.class.php
// classes/model/Survey.class.php
$lang['comments'] = 'Commentaire';

// classes/model/Participant.class.php
$lang['anonymous'] = 'Anonyme';

// classes/model/Question.class.php
// classes/model/Survey.class.php
// templates/question/question_form.php
// templates/question/question_form.php
// templates/survey/wizard/step_general.php
// view/cache/script.js
// view/lib/select2/js/select2/selection/base.js
$lang['title'] = 'Titre';

// classes/model/Question.class.php
// templates/question/question_form.php
$lang['options'] = 'Options';

// classes/model/Question.class.php
// templates/question/propositions_date_form.php
// templates/question/propositions_text_form.php
// templates/question/question_form.php
$lang['propositions'] = 'Propositions';

// classes/model/Survey.class.php
$lang['opened_to_everyone'] = 'Tous';

// classes/model/Survey.class.php
$lang['opened_to_guests'] = 'Les invités authentifiés';

// classes/model/Survey.class.php
$lang['opened_to_authenticated'] = 'Les utilisateurs authentifiés';

// classes/model/Survey.class.php
$lang['opened_to_noone'] = 'Personne';

// classes/model/Survey.class.php
$lang['answer_access_repliers'] = 'Les participants';

// classes/model/Survey.class.php
$lang['answer_access_repliers_with_answers'] = 'Les participants qui ont répondus';

// classes/model/Survey.class.php
$lang['answer_access_owner'] = 'Le propriétaire';

// classes/model/Survey.class.php
$lang['comment_access_repliers'] = 'Les participants';

// classes/model/Survey.class.php
$lang['comment_access_repliers_with_answers'] = 'Les participants qui ont répondus';

// classes/model/Survey.class.php
$lang['comment_access_owner'] = 'Le propriétaire';

// classes/model/Survey.class.php
$lang['Surveys'] = 'Sondages';

// classes/model/Survey.class.php
$lang['place'] = 'Lieu';

// classes/model/Survey.class.php
// templates/survey/wizard/step_general.php
$lang['description'] = 'Description';

// classes/model/Survey.class.php
$lang['owner_name'] = 'Nom du propriétaire';

// classes/model/Survey.class.php
$lang['owner_email'] = 'E-mail du propriétaire';

// classes/model/Survey.class.php
$lang['updated'] = 'Mis à jour';

// classes/model/Survey.class.php
// templates/survey/manage.php
// templates/survey/accordion/accordion.php
// templates/survey/accordion/accordion.php
$lang['closed'] = 'Clos';

// classes/model/Survey.class.php
$lang['settings'] = 'Paramètres';

// classes/model/Survey.class.php
$lang['Guest'] = 'Invité';

// classes/model/Survey.class.php
// templates/survey/create.php
// templates/survey/update.php
$lang['guests'] = 'Invités';

$lang['guests_step_header'] = $lang['guests'];

// classes/model/Survey.class.php
// templates/survey/create.php
// templates/survey/update.php
// templates/survey/wizard/step_questions.php
$lang['questions'] = 'Questions';

// classes/model/Survey.class.php
$lang['expires'] = 'Expire';

// classes/model/TextProposition.class.php
// templates/question/question_form.php
$lang['header'] = 'En-tête';

// templates/menu.php
$lang['connected_user'] = 'Utilisateur connecté';
$lang['disconnected_user'] = 'Utilisateur déconnecté';

// templates/page_menu.php
$lang['create_a_survey'] = 'Créer un sondage';

// templates/page_menu.php
$lang['manage_surveys'] = 'Gérer les sondages';

// templates/survey_page.php
$lang['results'] = 'Résultats';

// templates/answer/answers.php
// templates/question/reply_form.php
// templates/comment/participants_table.php
// templates/survey/results.php
// templates/survey/wizard/step_questions.php
// templates/participant/participants_table.php
$lang['question'] = 'Question';

// templates/answer/sum_row.php
// templates/participant/participants_table.php
$lang['sum'] = 'Somme';

$lang['sum_yes'] = 'Les "Oui"';

$lang['sum_maybe'] = 'Les "Peut-être"';

// templates/question/reply_form.php
// templates/survey/create.php
// templates/survey/update.php
// templates/survey/wizard/step_guests.php
$lang['validate'] = 'Valider';

$lang['preview_the_survey'] = 'Prévisualiser le sondage';

$lang['create_the_survey'] = 'Créer le sondage';

$lang['save_modifications'] = 'Enregistrer les modifications';

// templates/question/reply_form.php
$lang['leave_a_comment'] = 'Laisser un commentaire';

// templates/question/reply_form.php
$lang['your_comment'] = 'Votre commentaire';

// templates/question/propositions_text_form.php
// view/js/components/survey_questions_controller.js
// view/cache/script.js
$lang['proposition'] = 'Proposition';

// templates/question/propositions_text_form.php
$lang['variant'] = 'Variante';

// templates/question/propositions_text_form.php
$lang['duplicate_variants'] = 'Répliquer ces variantes';

// templates/question/propositions_text_form.php
$lang['add_text_question_proposition'] = 'Ajouter une proposition';

// templates/question/question_form.php
$lang['date_question'] = 'Question Date';
$lang['text_question'] = 'Question Texte';
$lang['delete_question'] = 'Supprimer la question';

// templates/survey/create.php
$lang['survey_create'] = 'Création d\'un sondage';

// templates/survey/create.php
// templates/survey/update.php
$lang['previous'] = 'Précédent';

// templates/survey/create.php
// templates/survey/update.php
$lang['save_draft'] = 'Enregistrer comme brouillon';

// templates/survey/create.php
// templates/survey/update.php
$lang['next'] = 'Suivant';

// templates/survey/manage.php
// templates/survey/manage.php
$lang['edit'] = 'Modifier';

// templates/survey/manage.php
// templates/survey/manage.php
// templates/survey/reply.php
$lang['show_results'] = 'Voir les résultats du sondage';

// templates/survey/manage.php
// templates/survey/manage.php
$lang['send_invitations_reminder'] = 'Envoyer un rappel des invitations';

$lang['confirm_sending_invitations_reminder'] = 'Êtes-vous sûr de vouloir envoyer un rappel des invitations';

// templates/survey/manage.php
// templates/survey/manage.php
$lang['duplicate'] = 'Dupliquer';

// templates/survey/manage.php
// templates/survey/manage.php
$lang['end_survey'] = 'Clore le sondage';

$lang['no_survey'] = 'Aucun sondage';

$lang['no_matching_survey'] = 'Aucun sondage correspondant.';

// templates/survey/manage.php
// templates/survey/manage.php
$lang['reply'] = 'Répondre au sondage';

// templates/survey/manage.php
$lang['search_in_surveys'] = 'Rechercher dans les sondages';

// templates/survey/manage.php
$lang['owned_by'] = 'Organisateur';

// templates/survey/manage.php
$lang['invited_to'] = 'Invité';

// templates/survey/manage.php
// templates/survey/accordion/accordion.php
$lang['opened'] = 'En cours';

// templates/survey/manage.php
// templates/survey/accordion/accordion.php
// templates/survey/accordion/accordion.php
$lang['draft'] = 'Brouillons';

// templates/survey/manage.php
$lang['surveys'] = 'Sondages';

// templates/survey/manage.php
$lang['filters'] = 'Filtres';

// templates/survey/manage.php
// templates/survey/accordion/accordion.php
$lang['open'] = 'Ouvert';

// templates/survey/reply.php
$lang['draft_message'] = 'Ceci est un brouillon de sondage, vous êtes le seul (en tant que propriétaire) à pouvoir visionner cette page';

// templates/survey/reply.php
$lang['reply_to_survey'] = 'Répondre au sondage';

// templates/survey/reply.php
// templates/survey/update.php
// templates/survey/tools/short_authorization.php
$lang['survey'] = 'Sondage';

// templates/survey/reply.php
//$lang['now_replying_as'] = 'Vous répondez au sondage en tant que';
$lang['now_replying_as'] = 'en tant que';

// templates/survey/reply.php
$lang['show_others_answers'] = 'Voir les réponses des autres participants';

// templates/survey/results.php
$lang['results_of_survey'] = 'Résultats du sondage';

// templates/survey/update.php
$lang['general'] = 'Général';

// templates/survey/wizard/step_general.php
$lang['parameters'] = 'Paramètres';

// templates/survey/wizard/step_general.php
$lang['surveys_title'] = 'Titre de votre sondage';

// templates/survey/wizard/step_general.php
$lang['advanced_options'] = 'Options avancées';
$lang['no_delete_survey_owner'] = 'Vous ne pouvez pas éditer ou supprimer votre propre adresse mail';

// templates/survey/wizard/step_general.php
$lang['in_results_display'] = 'Dans l\'affichage des résultats';
$lang['in_input_of_participants_answers'] = 'Lors de la saisie des réponses des participants';

// templates/survey/wizard/step_guests.php
$lang['invite_repliers'] = 'Personnes qui recevront une invitation à répondre à ce sondage';
$lang['email_with_link'] = 'Une fois le sondage créé, un e-mail contenant le lien d\'accès au sondage sera envoyé à l\'ensemble des invités (e-mails saisis) via cet outil.
Le lien d\'accès au sondage sera visible pour l\'organisateur dans "Gérer mes sondages".';

// templates/survey/wizard/step_guests.php
$lang['add_guest'] = 'Ajouter un invité';

$lang['add_owner'] = 'Ajouter un propriétaire';

// templates/survey/wizard/step_guests.php
$lang['share_survey'] = 'Partager le sondage';

// templates/survey/wizard/step_guests.php
$lang['survey_share_link_not_ready'] = 'Le lien de partage n\'est pas encore disponible';

// templates/survey/tools/short_authorization.php
$lang['tip_opened_to_everyone'] = 'Ouvert à tous';

// templates/survey/tools/short_authorization.php
$lang['tip_opened_to_authenticated'] = 'Ouvert aux utilisateurs authentifiés';

// templates/survey/tools/short_authorization.php
$lang['tip_opened_to_guests'] = 'Ouvert aux invités seulement';

// templates/survey/tools/short_authorization.php
$lang['tip_opened_to_noone'] = 'Ouvert à personne';

// templates/survey/accordion/accordion.php
$lang['all'] = 'Tous';

// templates/survey/accordion/accordion.php
$lang['last_update'] = 'Dernière mise à jour de l\'organisateur';

// templates/survey/accordion/accordion.php
$lang['nb_questions'] = 'Nombre de questions';

// templates/survey/accordion/accordion.php
$lang['nb_participants'] = 'Nombre de participants';

// templates/survey/accordion/accordion.php
$lang['link_to_survey'] = 'Lien vers ce sondage';

// templates/survey/accordion/accordion.php
$lang['show_more'] = 'Voir plus de sondages';

// templates/participant/participants_table.php
$lang['your_choice'] = 'Votre choix';

// view/js/components/survey_general_controller.js
// view/js/components/survey_questions_controller.js
// view/js/components/survey_wizard.js
// view/cache/script.js
$lang['please_set_title'] = 'Veuillez saisir un titre';

// view/js/components/survey_manage.js
$lang['confirm_survey_duplication'] = 'Êtes-vous certain de vouloir dupliquer le sondage ?';

// view/js/components/survey_manage.js
$lang['survey_duplicated'] = 'Sondage dupliqué';

// view/js/components/survey_manage.js
// view/cache/script.js
$lang['confirm_survey_deletion'] = 'Êtes-vous sûr de vouloir supprimer définitivement le sondage';

// view/js/components/survey_manage.js
// view/cache/script.js
$lang['survey_deleted'] = 'Sondage supprimé';

// view/js/components/survey_manage.js
// view/cache/script.js
$lang['confirm_survey_ending'] = 'Êtes-vous sûr de vouloir clore le sondage';

// view/js/components/survey_questions_controller.js
// view/cache/script.js
$lang['title_empty_error'] = 'Aucun titre saisi.';

// view/js/components/survey_questions_controller.js
// view/cache/script.js
$lang['proposition_empty_error'] = 'Proposition vide';

// view/js/components/survey_questions_controller.js
// view/cache/script.js
$lang['no_proposition_found'] = 'Aucune proposition trouvée';

// view/js/components/survey_reply.js
$lang['answer_saved'] = 'Réponse enregistrée';

// view/js/components/survey_reply.js
$lang['answers_saved'] = 'Réponses enregistrées';

// view/js/components/survey_reply.js
$lang['answers_saved_info'] = 'Vos réponses ont correctement été prises en compte. Merci d\'avoir participé.';

$lang['answers_saved_declarative_complement'] = 'Vous allez recevoir un message vous donnant un lien pour accéder à la modification de votre réponse.';

$lang['you_can_edit_your_answers'] = 'Vous pouvez modifier vos réponses';

$lang['you_will_be_redirected_to_edition_page_in'] = 'Vous serez redirigé vers la page d\'édition des réponses dans';

$lang['you_will_be_redirected_to_manage_page_in'] = 'Vous serez redirigé vers la page de gestion des sondages dans';

$lang['time_seconds'] = 'secondes';

$lang['here'] = 'ici';

// view/js/components/survey_wizard.js
// view/cache/script.js
$lang['draft_saved'] = 'Brouillon enregistré';

$lang['comeback_to_edition'] = 'Retourner à l\'édition';

// view/js/components/survey_wizard.js
// view/cache/script.js
$lang['survey_saved'] = 'Sondage sauvegardé';


$lang['survey_saved_info'] = 'Le sondage a bien été enregistré, il est maintenant ouvert. <br />Vous pouvez le modifier, clore et supprimer depuis l\'interface de gestion.';

$lang['saving_survey'] = 'Enregistrement du sondage';

$lang['saving_update'] = 'Enregistrement des modifications';

// view/cache/script.js
$lang['dp_date_format'] = 'DD/MM/YYYY';

// view/cache/script.js
$lang['dp_datetime_format'] = 'DD/MM/YYYY HH:mm';

// view/cache/script.js
$lang['dp_time_format'] = 'HH:mm';

// view/cache/script.js
$lang['readable_date_format'] = 'JJ/MM/AAAA';

// view/cache/script.js
$lang['readable_datetime_format'] = 'JJ/MM/AAAA HH:MM';

// view/cache/script.js
$lang['readable_time_format'] = 'HH:MM';

// view/cache/script.js
$lang['no_results'] = 'Aucune réponse';

// view/cache/script.js
$lang['reproduce_times_everyday'] = 'Dupliquer ces heures pour toutes les dates';

// classes/constants/SurveySettings.class.php
// templates/survey/wizard/step_general.php
$lang['disable_answer_edition'] = 'Désactiver l\'édition des réponses';

// templates/survey/wizard/step_questions.php
$lang['add_other_question'] = 'Ajouter une question TEXTE';
$lang['add_date_question'] = 'Ajouter une question DATE';
$lang['add_other_question_legend'] = 'Activités, lieux, etc.';
$lang['add_date_question_legend'] = 'Journées, créneaux horaires';
$lang['compress'] = 'Afficher la vue synthétique';
$lang['expand'] = 'Afficher la vue détaillée';
$lang['compress_legend'] = 'Réorganiser le questionnaire';
$lang['compress_expand_view_info'] = 'Avec la vue synthétique de votre questionnaire, vous pourrez réorganiser les questions plus simplemment en les déplaçant vers le haut ou le bas';

// templates/question/propositions_date_form.php
$lang['slot'] = 'Créneaux horaires';
$lang['whole_day'] = 'Journée entière';
$lang['add_date_propositions'] = 'Ajoutez maintenant vos propositions de date';
$lang['duplicate_times'] = 'Répliquer ces heures pour toutes les dates';
$lang['add_date'] = 'Ajouter une date';

// templates/question/propositions_text_form.php
$lang['add_text_propositions'] = 'Ajoutez maintenant vos propositions texte';

// view/js/components/survey_answers.js
$lang['comment_unmasked'] = 'Commentaire révélé';

// view/js/components/survey_answers.js
$lang['comment_masked'] = 'Commentaire masqué';

// view/js/components/survey_manage.js
$lang['survey_closed'] = 'Sondage clos';

$lang['to_hour'] = 'à';

$lang['to_day'] = 'au';

$lang['please_login'] = 'Pour créer ou gérer vos sondages, vous devez être authentifié.';

$lang['please_login_to_access_user_page'] = 'Pour gérer votre profil, vous devez être authentifié.';

$lang['access_denied'] = 'Accès refusé';

$lang['requires_authentication'] = 'Pour répondre à ce sondage, vous devez être authentifié.';

$lang['requires_invitation'] = 'Pour répondre à ce sondage, vous devez être invité.';

$lang['survey_not_found'] = 'Ce sondage n\'est pas disponible.<br/> Il n\'existe pas, a été supprimé, ou le lien que vous avez utilisé est erroné.';

$lang['your_name'] = 'Prénom et NOM';

$lang['your_email'] = 'Adresse mail';

$lang['several_possible_choices'] = 'Plusieurs choix possibles{if:count>1} parmi {count} propositions{endif}';

$lang['one_possible_choice'] = 'Un seul choix possible{if:count>1} parmi {count} propositions{endif}';

$lang['now_replying_anonymously'] = 'Vous répondez au sondage anonymement.';

$lang['participant_name'] = 'Nom du participant';

$lang['participant_count'] = '{count} Participant(s)';

$lang['toggle_decision_making_tool'] = 'Affiner vos résultats';

$lang['toggle_decision_making_tool_info'] = 'Masquer les réponses des participants facultatifs pour mettre à jour les totaux et affiner vos résultats. Le partage du sondage ne masquera pas les participants facultatifs';

$lang['mask_comment'] = 'Masquer le commentaire';

$lang['unmask_comment'] = 'Afficher le commentaire';

$lang['add_a_constraint'] = 'Ajouter une contrainte';

$lang['constraint'] = 'Contrainte';

$lang['all_propositions'] = 'Toutes les propositions';

$lang['limit_choice'] = 'Limiter le nombre de sélections';

$lang['value'] = 'Valeur';

$lang['survey_is_closed_since'] = 'Le sondage est clos depuis le';

$lang['survey_closes_soon'] = 'Le sondage sera bientôt clos';

$lang['limited_participant_number'] = 'Le nombre de participants est limité';

$lang['limited_participant_number_example'] = '« Exemple : Vous invitez 100 personnes à une conférence dans une salle pouvant
contenir 50 personnes. Vous limitez les réponses aux 50 premiers participants. »';

$lang['number_left'] = '%d restant(s)';

$lang['disable_answer_edition_example'] = '« Exemple : Les participants ne peuvent répondre qu’une seule fois au sondage. Une fois les réponses soumises, ils ne peuvent pas éditer leurs réponses »';

$lang['answer_edition_is_disabled'] = 'Vos choix ne sont plus modifiables lorsqu\'ils sont validés';

$lang['owned_by_tip'] = "Les sondages que vous avez créés";

$lang['invited_to_tip'] = "Les sondages auxquels vous avez été invité";

$lang['download_csv'] = "Exporter les résultats en CSV";

$lang['auto_close_reason'] = "Clôture automatique";

$lang['participant_limit_reached_reason'] = "La limite du nombre de participants a été atteinte";

$lang['requested_by_owner_reason'] = "Clôture demandée par le propriétaire";

$lang['you_answered'] = "Vous avez participé.";

$lang['you_have_not_answered'] = "Vous n'avez pas encore participé.";

$lang['answered'] = "Répondus";

$lang['not_answered'] = "Non répondus";

$lang['organized_by'] = "Organisé par";

$lang['yourself'] = "vous même";

$lang['survey_is_closed'] = "Le sondage est clos";

$lang['send_token_email_notice'] = "Envoyer un e-mail contenant le jeton d'accès à ce participant.";

$lang['validate_final_answers'] = "Valider les réponses finales";

$lang['validate_answers'] = "Valider les réponses";

$lang['selected_final_answer'] = "Réponse finale sélectionnée";

$lang['show_answers'] = 'Voir les réponses des participants';

$lang['final_answer'] = "Réponse finale";

$lang['satisfaction_rate'] = "Taux de satisfaction";

$lang['copy_to_clipboard'] = "Copier dans le presse-papier";

$lang['copy_link_to_clipboard'] = "Copier le lien dans le presse-papier";

$lang['copied_to_clipboard_success'] = "Copié dans le presse-papier";

$lang['clipboard_not_supported'] = "Presse-papier non supporté";

$lang['send_invitation_to_new_guests'] = "Envoyer une invitation aux nouveaux invités ?";

$lang['time_zone'] = "Fuseau horaire";

$lang['add_time_picker'] = "Ajouter un horaire";

$lang['guest_email_input_notice'] = "Saisissez un ou plusieurs e-mails valides";

$lang['constraints'] = "Contraintes";

$lang['undefined'] = "Indéfini";

$lang['participant_invalid_token'] = "Le jeton de participation est invalide.";

$lang['dont_receive_invitation_copy'] = "Ne pas recevoir une copie de l'invitation.";

$lang['left_arrow_notice'] = "Double cliquez pour aller à la première proposition";

$lang['right_arrow_notice'] = "Double cliquez pour aller à la dernière proposition";

$lang['selected_value_yes_text_notice'] = $lang['selected_value_yes'];

$lang['selected_value_maybe_text_notice'] = $lang['selected_value_maybe'];

$lang['selected_value_no_text_notice'] = $lang['selected_value_no'];

$lang['selected_value_yes_date_notice'] = "Je suis disponible à cette date";

$lang['selected_value_maybe_date_notice'] = "Je peux être présent(e) à cette date";

$lang['selected_value_no_date_notice'] = "Je ne peux pas être présent(e) à cette date";

$lang['select_for_all'] = "Sélectionner pour tous";

$lang['owners'] = "Propriétaires";

$lang['owner_name_input_notice'] = "Saisissez un nom";

$lang['owner_email_input_notice'] = "Saisissez l'e-mail du propriétaire que vous souhaitez ajouter";

$lang['invitation_sent'] = 'Invitation envoyée';

$lang['comment_of'] = 'Commentaire de';

$lang['on_date'] = 'Le';

$lang['masked_not_shared'] = 'Masquer le commentaire permet de ne pas le diffuser lors de vos partages de résultats.';

$lang['administration_of'] = 'Administration de';

$lang['administration_actions'] = 'Outils d\'administration';

$lang['survey_management'] = 'Gestion des sondages';

$lang['usage_stats'] = 'Statistiques d\'utilisation';

$lang['opened_survey_count'] = 'Nombre de sondages ouverts';

$lang['total_survey_count'] = 'Nombre de total de sondages créés';

$lang['draft_survey_count'] = 'Nombre de brouillons de sondage';

$lang['closed_survey_count'] = 'Nombre de sondages clos';

$lang['all_surveys'] = 'Tous les sondages';

$lang['uneditable_answer'] = 'Vous ne pouvez plus modifier cette réponse';

$lang['unavailable_answer'] = 'Vous ne pouvez plus séléctionner cette proposition.';

$lang['invalid_question'] = 'Il y a des données manquantes pour cette question';

$lang['token_sent'] = 'Lien d\'accès envoyé';

$lang['sending_recovery_email'] = 'Envoi du lien d\'accès par e-mail en cours.';

$lang['invitations_sent'] = 'Le rappel des invitations a bien été envoyé.';

$lang['help'] = 'Aide';

$lang['about'] = 'À propos';

$lang['give_us_feedback'] = 'Retour utilisateur';

$lang['connected_user'] = 'Utilisateur connecté';

$lang['guests_added'] = 'invités ajoutés';

$lang['constraint_value_placeholder'] = 'Ex. : 100';

$lang['no_known_constraints'] = 'Aucune contrainte';

$lang['participant_already_exists_for_survey'] = 'Un participant avec cette adresse e-mail a déjà participé à ce sondage.';

$lang['please_select_value_for_question'] = 'Veuillez sélectionner une proposition pour cette question';

$lang['add_guest_notice'] = '<p>Cliquez sur "'.$lang['add_guest'].'" pour ajouter un invité.</p>'
                            . '<p>Les invités saisis recevront automatiquement un email d\'invitation à répondre </p> '
                            . '<p class="italic">Note : dans le champ Email vous pouvez saisir une adresse ou plusieurs séparées par un ";"</p> ';

$lang['add_proposition_variants'] = 'Ajouter des variantes';

$lang['confirm_question_deletion'] = 'Êtes-vous certain de vouloir supprimer la question';

$lang['answers_cannot_be_edited'] = 'Vos réponses ne peuvent plus être éditées.';

$lang['end_date_prior_to_base_date'] = 'La date de fin est antérieure à la date de début.';

$lang['end_time_prior_to_base_time'] = 'L\'heure de fin est antérieure à l\'heure de début.';

$lang['answer_proposition_not_available'] = 'Une proposition n\'est plus disponible.';

$lang['survey_action_not_allowed'] = 'Vous n\'êtes pas autorisé à réaliser cette action sur le sondage. Seuls les propriétaires du sondage y sont autorisés.';

$lang['closed_survey_with_final_answer_count'] = 'Nombre de sondages clos avec une réponse finale';

$lang['participants_count'] = 'Nombre de participations aux sondages';

$lang['authenticated_participants_count'] = 'Nombre de participations authentifiées';

$lang['declarative_participants_count'] = 'Nombre de participations déclaratives';

$lang['answer_survey_closed'] = 'Le sondage est clos, vos réponses ne peuvent pas être enregistrées.';

$lang['answer_draft_survey'] = 'Vous ne pouvez pas répondre à un brouillon de sondage.';

$lang['participant_not_allowed'] = 'Vous ne pouvez pas participer au sondage.';

$lang['no_participant_found'] = 'Votre participation ne peut pas être enregistrée.';

$lang['no_way_to_identify'] = 'Impossible de vous identifier';

$lang['headers'] = 'Titres';

$lang['bold'] = 'Gras';

$lang['italics'] = 'Italique';

$lang['ul'] = 'Liste à puces';

$lang['ol'] = 'Liste ordonnée';

$lang['table'] = 'Tableau';
$lang['title_1'] = 'Titre 1';
$lang['title_2'] = 'Titre 2';
$lang['title_3'] = 'Titre 3';
$lang['title_4'] = 'Titre 4';
$lang['title_5'] = 'Titre 5';
$lang['title_6'] = 'Titre 6';

$lang['link'] = 'Hyperlien';
$lang['img'] = 'Image';
$lang['blockquote'] = 'Citation';
$lang['codeblock'] = 'Bloc de code';
$lang['code'] = 'Code';
$lang['footnote'] = 'Note de bas de page';
$lang['horizontal_rule'] = 'Ligne horizontale';

$lang['more_details'] = 'Plus de détails';

$lang['recovery_email_could_not_be_sent_try_later'] = 'Le lien d\'accès ne peut pas être envoyé pour le moment';

$lang['no_question_found'] = 'Veuillez ajouter une question en cliquant sur un des 3 boutons ci-dessous';

$lang['participant_has_already_answered_to_survey'] = 'Vous avez déjà répondu à ce sondage. Essayez d\'actualiser la page pour voir vos réponses.';

$lang['constraint_value_info'] = '« Exemple : Vous avez seulement 10 places disponibles pour un créneau. Vous limitez le nombre de sélection de ce créneau à 10. »';

$lang['key'] = 'Clé';

$lang['avg_count'] = 'Nombre moyen de sondages par propriétaires';

$lang['max_count'] = 'Nombre maximum de sondages par propriétaires';

$lang['support'] = 'Support';

$lang['user_guide_unreachable_title'] = 'Guide utilisateur introuvable';
$lang['user_guide_unreachable_content'] = 'Une erreur s\'est produite lors de la récupération du guide utilisateur.';
$lang['user_guide_unreachable_download'] = 'Vous pouvez néanmoins télécharger la version PDF ';

$lang['institution'] = 'Etablissement';
$lang['sugar_id'] = 'Identifiant PASS';
$lang['nb_distinct_user'] = 'Nombre d\'utilisateurs distincts';
$lang['nb_survey_created'] = 'Nombre de sondages créés';
$lang['nb_participations'] = 'Nombre de participations';
$lang['nb_anonymous_participations'] = 'Nombre de participations anonymes';
$lang['total'] = 'Total';
$lang['weekly'] = 'hebdomadaires';

$lang['legal_notice'] = 'Mentions Légales';

$lang['moment_date_format_readable'] = 'ddd<br/> DD <br/>MMM';

$lang['moment_datetime_format_readable'] = 'ddd DD MMM YYYY HH:mm';

$lang['updating_survey'] = 'Mise à jour du sondage';

$lang['close_modal'] = 'Fermer';

$lang['page_not_found'] = 'La page que vous avez demandée n\'existe pas.';

$lang['send_update_notification'] = 'Vous avez modifié votre sondage.<br/> Souhaitez vous en informer les participants du sondage ?';

$lang['send_update_notification_and_invitation_to_new_guests'] = 'Vous avez modifié votre sondage.<br/> Souhaitez vous en informer les participants du sondage <br/> et envoyer une invitation aux nouveaux invités ?';

$lang['confirm_leave_page'] = 'Êtes vous certain de vouloir quitter cette page ?';

$lang['delete_proposition'] = 'Supprimer la proposition';

$lang['delete_date'] = 'Supprimer cette date';

$lang['delete_time_picker'] = 'Supprimer ce champ';

$lang['proposition_header'] = 'Intitulé';

// templates/logout_page.php
$lang['logout_disclamer'] = 'Vous êtes maintenant déconnecté de {cfg:application_name}.';

$lang['time_zone_info_hours_large'] = 'Les dates/heures sont affichées dans votre fuseau horaire';
$lang['time_zone_info_hours_small'] = 'Votre fuseau horaire';
$lang['time_zone_info_moments_large'] = 'Les moments de la journée sont relatifs au fuseau horaire utilisé lors de la création du sondage';
$lang['time_zone_info_moments_small'] = 'Fuseau horaire du sondage';
$lang['time_zone_info_moments_info'] = 'Les participants qui ne sont pas dans ce fuseau verront les créneaux correspondants dans leur propre fuseau, et non les moments de la journée.';

$lang['faq_title'] = 'Foire aux questions';

$lang['all_surveys_tip'] = 'Tous les sondages';

$lang['renater_logo'] = 'Logo Renater';

$lang['download_csv_info'] = 'L\'export CSV est encodé en UTF-8 et séparé par des {separators}.';
$lang['commas'] = 'virgules';
$lang['semicolons'] = 'points-virgules';

$lang['expand_reduce_view'] = 'Agrandir/Réduire la vue';

$lang['comment'] = 'Commentaire';

$lang['day_slot'] = 'Intervalle de dates';

$lang['path'] = 'Chemin';

$lang['requires_authentication'] = 'Authentification requise';

$lang['pending_survey_detected'] = 'Nous avons detecté un sondage en cours d\'édition.<br> Souhaitez vous continuer ?';

$lang['connection_lost_try_to_reconnect'] = 'Il semble que vous ne soyez plus connecté.<br> Pas d\'inquiétude, votre sondage en cours est sauvegardé.<br> Vous pourrez continuer lorsque vous serez reconnecté.<br> Se reconnecter ?';

$lang['survey_recovery_dialog'] = 'Récupération de sondage';

$lang['delete_pending_survey'] = 'Ignorer ce sondage en attente';

$lang['later'] = 'Plus tard';

$lang['form_invalid_data'] = 'Données de formulaire non valides';

$lang['participants_emails_dialog'] = 'E-mails des participants';

$lang['no_participant_email_found'] = 'Aucun e-mail de participant trouvé.';

$lang['participants_emails_with_yes_for_this_proposition'] = 'E-mails des participants qui ont répondu "Oui" à cette proposition';

$lang['display_participants_who_answered_yes_to'] = 'Afficher les e-mails des participants qui ont répondu Oui à';

$lang['toggle_participants_emails_option'] = 'Afficher l’option de récupération des e-mails';

$lang['participants_emails'] = 'E-mails';

$lang['toggle_participants_emails_option_info'] = 'Récupérer les e-mails des participants';

$lang['display_participants_emails'] = 'Afficher tous les e-mails des participants';

$lang['not_allowed_to_see_results'] = 'Vous n\'êtes pas autorisé à voir les résultats de ce sondage.';

$lang['distinct_users_count'] = 'Nombre d\'utilisateurs enregistrés';

$lang['loading_answers'] = 'Chargement des réponses en cours';

$lang['avg_date_proposition_count'] = 'Nombre moyen de propositions de date';

$lang['avg_text_proposition_count'] = 'Nombre moyen de propositions texte';

$lang['max_date_proposition_count'] = 'Nombre maximum de propositions de date';

$lang['max_text_proposition_count'] = 'Nombre maximum de propositions texte';

$lang['survey_usage'] = 'Utilisation des sondages';

$lang['avg_participant_count'] = 'Nombre moyen de participants';

$lang['max_participant_count'] = 'Nombre maximum de participants';

$lang['edugain_idps'] = 'IDP eduGAIN';

$lang['other_idps'] = 'Autres IDP';

// MOTD
$lang['i_got_it'] = "J'ai compris";
$lang['motd_hint'] = 'Si vous cliquez sur "J\'ai compris", ce message ne se réaffichera plus et cette information sera stockée dans vos cookies.';

$lang['moment_project'] = "Projet Moment sur SourceSup";

$lang['terms'] = 'CGU';

$lang['accept_terms'] = 'J\'accepte les <a href="{url:terms}" target="_blank">conditions générales d\'utilisation</a>';

$lang['accept_terms_required'] = 'Vous devez accepter les conditions générales d\'utilisation';

$lang['log_in_tip'] = 'Se connecter';

// My profil - Calendars
$lang['my_calendars'] = 'Mes calendriers';

$lang['connect_my_calendars'] = 'Connecter mes calendriers';

$lang['connect_my_calendars_info'] = 'Vous pouvez ajouter vos calendriers dans Moment. Vous pourrez ainsi visualiser vos indisponibilités lors de vos réponses ou de vos créations de sondages.';

$lang['connect_my_moment_calendar'] = 'Connecter mon calendrier Moment';

$lang['connect_my_moment_calendar_info'] = 'Ajoutez cette adresse URL à votre application d\'agenda pour voir vos réponses Moment en tant qu\'évènements.';

$lang['no_calendar_added_yet'] = 'Aucun calendrier n\'a encore été ajouté';

$lang['extension_allowed'] = 'Moment supporte les liens vers les fichiers ics (iCalendar) et ifb (Free/Busy).' ;

$lang['calendar_usage'] = 'Seule l\'URL de votre calendrier est conservée. Les calendriers ainsi ajoutés sont téléchargés et analysés seulement lorsque vous souhaitez afficher les conflits lors de la réponse à un sondage. Le contenu des calendriers n\'est pas conservé. Vous pouvez à tout moment supprimer l\'URL de votre calendrier.';

$lang['add_calendar'] = 'Ajouter un calendrier';

$lang['access_to_calendar_timed_out'] = 'L\'accès à ce calendrier a dépassé le temps imparti';

$lang['access_to_calendar_forbidden'] = 'L\'accès à ce calendrier n\'est pas autorisé';

$lang['access_to_calendar_failed'] = 'L\'accès à ce calendrier a échoué';

$lang['calendar_unavailable'] = 'Ce calendrier n\'est pas accessible';

$lang['calendar_not_found'] = 'Ce calendrier n\'a pas été trouvé';

$lang['calendar_is_oversized'] = 'La taille de ce calendrier dépasse la taille limite autorisée ('.round(Config::get('calendar.max_size') / 1024 / 1024,1).' Mo)';

$lang['something_went_wrong_when_checking_calendar'] = 'L\'application a rencontré un problème en vérifiant le calendrier';

$lang['calendar_url_placeholder'] = 'https://...ics';

$lang['confirm_calendar_deletion'] = 'Êtes-vous sûr de vouloir supprimer le calendrier %s de votre liste&nbsp;?';

$lang['calendar_removed_with_success'] = 'Calendrier supprimé avec succès';

$lang['calendar_added_with_success'] = 'Calendrier ajouté avec succès';

$lang['calendar_updated_with_success'] = 'Calendrier modifié avec succès';

$lang['calendar_removal_failed'] = 'La suppression du calendrier a échoué';

$lang['calendar_adding_failed'] = 'L\'ajout du calendrier a échoué';

$lang['calendar_updating_failed'] = 'La modification du calendrier a échoué';

$lang['saving_calendar'] = 'Ajout du calendrier en cours';

$lang['updating_calendar'] = 'Modification du calendrier en cours';

$lang['did_you_know'] = 'Le saviez-vous ?';

$lang['view_calendars'] = 'Visualiser les calendriers';

$lang['view_calendars_info'] = 'Lorsque je crée ou lorsque je réponds à un sondage, je peux voir les créneaux réservés ou mes réponses dans mon agenda. ';

$lang['loading_calendars'] = 'Chargement des calendriers en cours';

$lang['calendar_is_not_compatible'] = 'Ce calendrier ne semble pas compatible';

$lang['leaving_survey_creation_form'] = 'Sortie du formulaire de création';

$lang['survey_data_will_be_saved_in_draft'] = 'Les données de votre sondage déjà saisies vont être sauvegardées dans un brouillon.<br/><br/>'
.'Vous pourrez ensuite reprendre votre brouillon depuis la section “Gérer les sondages”.';

$lang['all_day'] = 'Toute la journée';

$lang['from_hour'] = 'De';

$lang['from_day'] = 'Du';

$lang['conflict'] = 'Conflit détecté';

$lang['conflicts'] = 'Conflits détectés';

$lang['hide_conflicts'] = 'Masquer les conflits de date';

$lang['hide_conflicts_info'] = 'Les éventuels conflits sont signalés au moyen d\'un <span class=\'calendar-info neutral fa fa-exclamation-triangle\'></span>. Au survol de la proposition concernée vous pourrez voir le ou les évènements en conflits.';

$lang['conflicts_are_now_loaded'] = 'Vos calendriers ont correctement été chargés et les conflits eventuels sont affichés';

$lang['conflicts_loading_failed'] = 'Le chargement de vos calendriers a échoué, essayez ultérieurement';

$lang['loading_calendars_unavailability_failed'] = 'Le chargement de vos calendriers a échoué.';

$lang['my_profile'] = 'Mon profil';

$lang['unavailability'] = 'Indisponibilités';

$lang['learn_more_about_diff_ics_ifb'] = 'En savoir plus sur la différence entre ics et ifb...';

$lang['learn_more_about_getting_ics_ifb'] = 'En savoir plus sur comment récupérer l\'URL de son calendrier...';

$lang['learn_more'] = 'En savoir plus...';

$lang['access_user_guide_moment_calendar'] = 'Accéder au guide utilisateur pour voir comment importer le calendrier Moment dans votre agenda';

$lang['regenerate_link'] = 'Cliquer pour générer un nouveau lien<br/> (Attention le lien précédent ne sera plus valide)';

$lang['calendar_name'] = 'Nom du calendrier';

$lang['calendar_name_placeholder'] = 'Saisissez le nom du calendrier';

$lang['calendar_color'] = 'Personnaliser la couleur du calendrier';

$lang['filter_my_calendars'] = 'Filtrer mes calendriers';

$lang['filter_my_calendars_tip'] = 'Filtrer vos calendriers permet de paramétrer l\'affichage de vos indisponibilités.<br/> Vous pouvez sélectionner le ou les calendriers voulus pour n\'afficher que les évènements correspondants.<br/> Vous pouvez désactiver la fonctionnalité en désélectionnant tous les calendriers.';

$lang['all_calendars'] = 'Tous';

$lang['calendar_url_constraint'] = 'obligatoire, l\'URL doit être accessible et pointer vers un calendrier ics (iCalendar) ou ifb (Free/Busy)';

$lang['calendar_link_regenerated'] = 'Le lien de calendrier a été regénéré.';

$lang['calendar_link_regenerate_failed'] = 'La regénération du lien a échoué.';

$lang['confirm_refresh_calendar_link'] = 'Êtes-vous sûr de vouloir générer un nouveau lien de calendrier ?<br/> (Attention le lien précédent ne sera plus valide)';

$lang['please_add_at_least_one_proposition'] = 'Vous devez ajouter au moins une proposition';

$lang['no_date_selected'] = 'Aucune date sélectionnée.<br/>Vous pouvez les choisir sur le calendrier, vous pourrez ensuite spécifier des horaires ou créneaux horaires si besoin.';

$lang['no_date_slot_selected'] = 'Aucune période sélectionnée.<br/>Vous pouvez les choisir sur le calendrier. Pour cela, cliquez sur la date de début, et ensuite sur la date de fin.';

$lang['reopen_survey'] = 'Réouvrir le sondage';

$lang['survey_reopening_title'] = 'Réouverture du sondage';

$lang['select_new_auto_close_date'] = 'Veuillez sélectionner la nouvelle date de clôture pour le sondage :';

$lang['select_new_participant_limit_nb'] = 'Le nombre maximum de participations (%nb_participants%) au sondage est déjà atteint. Afin de pouvoir réouvrir le sondage, vous devez augmenter le nombre de participations autorisées :';

$lang['invalid_new_auto_close_date'] = 'La nouvelle date saisie n\'est pas valide';

$lang['limit_participants_nb_too_low'] = 'La nouvelle limite du nombre de participants est trop basse';

$lang['reopen_survey_disabled'] = 'Il n\'est pas possible de réouvrir ce sondage.<br/> Soit il n\'est pas possible de repousser la date de clôture, soit une réponse finale a été définie.';

$lang['survey_reopened'] = 'Le sondage a été réouvert';

$lang['please_select_value_for_all_questions'] = 'Veuillez sélectionner une proposition pour chacune des questions';

$lang['start_date'] = 'Date de début';

$lang['end_date'] = 'Date de fin';

$lang['same_hours_for_all'] = 'Mêmes horaires tous les jours';

$lang['input_mode'] = 'Mode de saisie';
$lang['input_mode_hours'] = 'Horaires';
$lang['input_mode_moments'] = 'Moments de la journée';
$lang['same_moments_for_all'] = 'Mêmes moments tous les jours';
$lang['input_mode_info_hours'] = 'La saisie &quot;Horaires&quot; vous permet de spécifier des horaires ou des créneaux horaires personnalisés.';
$lang['input_mode_info_moments'] = 'La saisie &quot;Moments de la journée&quot; de sélectionner des créneaux prédéfinis de la journée:';
$lang['input_mode_info_moments_row'] = 'Une proposition {name} correspond à un créneau &quot;{bt}-{et}&quot;';
$lang['moments_of_day_info'] = 'Propositions de réponse';
$lang['morning'] = 'Matin';
$lang['noon'] = 'Midi';
$lang['afternoon'] = 'Après-midi';
$lang['evening'] = 'Soir';

$lang['need_hours'] = 'Besoin d\'horaires ?';

$lang['please_select_dates'] = 'Veuillez sélectionner des dates';

$lang['changing_to_day_slots_will_delete_your_propositions'] = 'En changeant pour des intervalles de dates vos créneaux horaires seront effacés';

$lang['changing_from_day_slots_will_delete_your_propositions'] = 'En changeant pour des créneaux horaires vos intervalles de dates seront effacés';

$lang['days'] = 'jour(s)';

$lang['question_statement'] = 'Enoncé de la question';

$lang['optional'] = 'facultatif';

$lang['please_set_question_statement'] = 'Veuillez saisir un énoncé';

$lang['force_unique_choice_explanation'] = 'Si cette option est positionnée, les participants ne peuvent sélectionner qu’une seule réponse parmi les propositions de cette question';

$lang['invalid_range_start_date'] = 'La date de début n\'est pas valide';

$lang['invalid_range_end_date'] = 'La date de fin n\'est pas valide';

$lang['add_date_range_question'] = 'Ajouter une question PERIODE';

$lang['add_date_range_question_legend'] = 'Périodes de plusieurs jours';

$lang['your_date_range_propositions'] = 'Vos propositions de périodes';

$lang['your_date_range_propositions_info'] = 'Vous pouvez choisir les périodes sur le calendrier. Pour cela, cliquez sur la date de début et ensuite sur la date de fin.';

$lang['your_date_propositions'] = 'Vos propositions dates';

$lang['your_date_propositions_info'] = 'Vous pouvez choisir les dates sur le calendrier. Pour cela, cliquez sur la date souhaitée.';

$lang['your_text_propositions'] = 'Vos propositions texte';

$lang['add_proposition_variant'] = 'Ajouter une variante';

$lang['same_variants_for_all'] = 'Mêmes variantes pour toutes les propositions';

$lang['question_has_invalid_inputs'] = 'Il y\'a des données invalides pour cette question';

$lang['you_must_select_at_least_one_proposition'] = 'Vous devez sélectionner ou saisir au moins une proposition';

$lang['monthly'] = 'mensuelles';

$lang['yearly'] = 'annuelles';

$lang['daily'] = 'quotidiennes';

$lang['you_already_answered_this_survey'] = 'Notification de doublon de réponse au sondage';

$lang['declarative_participant_already_exists'] = 'Nous avons envoyé un mail à cette adresse pour confirmer qu\'il s\'agit bien de vous.';

$lang['authenticated_participant_already_exists'] ='Veuillez vous connecter pour confirmer la modification de vos réponses précédentes.';

$lang['participant_with_this_email_already_exists'] = 'Des réponses associées à l\'adresse mail déclarée ont déjà été données pour ce sondage .';

$lang['update_answer'] = 'Modifier mes réponses';

$lang['please_fill_in_the_code'] = 'Merci de saisir le code reçu par mail pour valider la modification de vos réponses précédentes :';

$lang['enter_the_code_here'] = 'Saisissez le code';

$lang['send_me_another_recovery_code'] = 'Renvoyer un nouveau code';

$lang['n_tries_left'] = 'Le code saisi n\'est pas valide (%d essai(s) restant)';

$lang['too_much_tries'] = 'Vous avez fait trop d\'essais. Veuillez recharger la page.';

$lang['answers_modification_impossible'] = 'Modification des réponses impossible';

$lang['participant_with_this_email_already_exists_edition_disabled'] = 'Des réponses associées à l\'adresse mail'
    .' déclarée ont déjà été données pour ce sondage. Malheureusement, le propriétaire du sondage n\'a pas autorisé'
    .' l\'édition des réponses. Aussi, nous ne pouvons prendre en compte votre demande.';

$lang['declarative_participant_already_exists_edition_disabled'] = 'Vous pouvez récupérer vos réponses précédentes en cliquant sur ce lien :';

$lang['authenticated_participant_already_exists_edition_disabled'] = 'Vous pouvez vous connecter pour visualiser vos réponses précédentes.';

$lang['send_link_to_previous_answers'] = 'Envoyer un mail avec le lien vers mes réponses précédentes';

$lang['your_recovery_email_is_sent'] = 'Un mail avec le lien vers vos réponses a été envoyé';

$lang['i_already_answered_to_this_survey'] = 'J\'ai déjà répondu et je souhaite accéder à mes réponses précédentes';

$lang['i_already_answered_to_this_survey_explanation'] = 'Si vous avez déjà répondu à ce sondage, merci d\'utiliser le'
    .' lien qui vous a été transmis par mail pour modifier ou supprimer vos réponses.'
    .' Si vous avez perdu ce mail, vous pouvez en demander le renvoi en cliquant sur ce lien.';

$lang['please_fill_in_the_email_so_that_we_can_send_recovery_email'] = 'Merci de renseigner l\'adresse mail utilisée '
    .'lors de votre précédente participation afin que nous puissions vous renvoyer le lien de modification de vos réponses.';

$lang['recovery_email_has_been_sent'] = 'Un mail a été envoyé à l\'adresse renseignée (cela peut prendre quelques minutes).';

$lang['recovery_email_has_been_sent_explanation'] = 'Si vous aviez déjà répondu au sondage, vous n\'aurez qu\'à cliquer sur le lien contenu dans le mail et vous accèderez directement à vos réponses précédentes.
<br/>Si le mail reçu stipule qu\'aucune participation n\'a été trouvée pour cette adresse mail, c\'est que vous n\'aviez pas répondu à ce sondage avec celle-ci.';

$lang['sending_recovery_code'] = 'Envoi du code de récupération en cours';

$lang['recovery_code_sent'] = 'Un mail avec un nouveau code vous a été envoyé';

$lang['delete_answers'] = 'Supprimer mes réponses';

$lang['confirm_answers_deletion'] = 'Êtes-vous sûr de vouloir supprimer vos réponses au sondage';

$lang['your_answers_have_been_deleted'] = 'Vos réponses ont bien été supprimées';

$lang['deleting'] = 'Suppression en cours';

$lang['answers_cannot_be_deleted'] = 'Vos réponses ne peuvent plus être supprimées.';

$lang['if_dont_receive_code'] = 'Si vous n\'avez pas reçu de mail avec le code, vérifiez d\'abord dans vos spams. Vous pouvez demander le renvoi d\'un nouveau code en cliquant sur le lien (le code précédent ne sera plus valide)';

$lang['no_code'] = 'Inactif car aucun code saisi';

$lang['validating'] = 'Validation en cours';

$lang['logon'] = 'Se connecter';

$lang['accessibility'] = 'Accessibilité';

$lang['sum_yes_plus_maybe'] = 'Les "Oui" + "Peut-être"';

$lang['maybe_answers_disabled'] = 'Réponse <span class="maybe-answer-disabled-label-maybe">"Peut-être"</span> non autorisée';

$lang['maybe_answers_disabled_info'] = 'Le créateur du sondage ne souhaite pas de réponse &#34;Peut-être&#34; à cette question';

$lang['confirm_survey_deletion_warning'] = 'La suppression est définitive. Le sondage ne pourra pas être restauré.';

$lang['my_preferences'] = 'Mes préférences';

$lang['access_link_to_the_survey'] = 'Lien d\'accès au sondage';

$lang['question_limit_choice'] = 'Limite de sélections d\'une proposition';

$lang['question_limit_choice_nb'] = 'Nombre de sélections d\'une proposition';

$lang['question_limit_choice_example'] = 'Exemple : Vous voulez programmer des rendez-vous individuels. Vous limitez à 1 participant par proposition.';

$lang['manage_constraints'] = 'Gérer les contraintes';
$lang['see_constraints'] = 'Voir les contraintes';
$lang['some_constraints_are_defined'] = 'Des contraintes ont été positionnées pour cette question.';
$lang['to_define_constraint'] = 'Pour définir des limites différentes par proposition :';
