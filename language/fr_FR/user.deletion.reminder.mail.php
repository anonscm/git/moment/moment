subject: Suppression prochaine de votre compte

{alternative:plain}

Suppression prochaine de votre compte

Bonjour,

Nous constatons que votre compte sur le service {cfg:application_name} ne présente aucune activité depuis un certain temps.
Dans le respect de la réglementation sur la protection des données à caractère personnel, nous vous informons que, sans activité d'ici {noticePeriod} mois, votre compte et toutes les données s'y rattachant seront supprimées définitivement.
Si vous souhaitez conserver votre compte {cfg:application_name}, vous pouvez simplement vous connecter au service {cfg:application_name} dans {if: noticePeriod === 1}le mois qui vient{else}les {noticePeriod} mois qui viennent{endif}.
Si vous ne souhaitez pas conserver votre compte {cfg:application_name}, vous n'avez rien à faire. Celui-ci sera supprimé automatiquement dans {noticePeriod} mois.

Vous remerciant de votre compréhension.
L'équipe {cfg:application_name}.

=====================================================================

Future deletion of your account

Hello,
We notice that your account on {cfg:application_name} has been inactive for some time.
In accordance with the regulations governing the protection of personal data, we would like to inform you that if there is no activity within {noticePeriod} month, your account and all related data will be permanently deleted.
If you want to keep your {cfg:application_name} account, you simply need to log into {cfg:application_name} in the {if: noticePeriod != 1}{noticePeriod}{endif} following month{if: noticePeriod != 1}s{endif}.
If you do not want to keep your {cfg:application_name} account, you have nothing to do. It will be deleted in {noticePeriod} month{if: noticePeriod != 1}s{endif}.

Regards,
The {cfg:application_name} Team

{alternative:html}

<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Suppression prochaine de votre compte</h1>
    <p style="margin-left:1rem; text-align: left;">
        Bonjour,<br/>
        Nous constatons que votre compte sur le service {cfg:application_name} ne présente aucune activité depuis un certain temps.<br/>
        Dans le respect de la réglementation sur la protection des données à caractère personnel, nous vous informons que, sans activité d'ici {noticePeriod} mois, votre compte et toutes les données s'y rattachant seront supprimées définitivement.<br/>
        Si vous souhaitez conserver votre compte {cfg:application_name}, vous pouvez simplement vous connecter au service {cfg:application_name} dans {if: noticePeriod == 1}le mois qui vient{else}les {noticePeriod} mois qui viennent{endif}.<br/>
        Si vous ne souhaitez pas conserver votre compte {cfg:application_name}, vous n'avez rien à faire. Celui-ci sera supprimé automatiquement dans {noticePeriod} mois.<br/>
    </p>
    <p style="margin-left:1rem; text-align: left;">
        Vous remerciant de votre compréhension.<br/>
        L'équipe {cfg:application_name}.
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Future deletion of your account</h1>
    <p style="margin-left:1rem; text-align: left;">
        Hello,<br/>
        We notice that your account on {cfg:application_name} has been inactive for some time.<br/>
        In accordance with the regulations governing the protection of personal data, we would like to inform you that if there is no activity within {noticePeriod} month, your account and all related data will be permanently deleted.<br/>
        If you want to keep your {cfg:application_name} account, you simply need to log into {cfg:application_name} in the {if: noticePeriod != 1}{noticePeriod}{endif} following month{if: noticePeriod != 1}s{endif}.<br/>
        If you do not want to keep your {cfg:application_name} account, you have nothing to do. It will be deleted in {noticePeriod} month{if: noticePeriod != 1}s{endif}.<br/>
    </p>
    <p style="margin-left:1rem; text-align: left;">
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>
