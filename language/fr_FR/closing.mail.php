subject: Clôture du sondage : "{survey.title}"

{alternative:plain}

Clôture du sondage : "{survey.title}"

Le sondage "{survey.title}" a été clos.
{reason}

{if: ultimate_participant && ultimate_participant.name != ''}
{ultimate_participant.name} a sélectionné les réponses finales.
{endif}
{if: ultimate_participant && survey.has_date_questions}
Vous avez la possibilité de les intégrer dans votre agenda grâce au fichier joint "invite.ics".
{endif}

{if: survey.hide_answers == false && ultimate_participant }
Choix finaux :
    {each: ultimate_participant.Answers as answer}
    {answer.Question.statement} : {answer.raw_labels}

    {endeach}
{endif}

{if: survey.hide_answers == false}
Voir les résultats : {survey.results_path}
{endif}

{if: ultimate_participant == false}
Si vous êtes propriétaire du sondage, vous pouvez le réouvrir. Pour cela, allez dans “Gérer les sondages”, puis cliquez sur l'icône de réouverture du sondage.
{endif}

Cordialement,
L'équipe {cfg:application_name}

=====================================================================

Closure of the survey : "{survey.title}"

The survey "{survey.title}" has been closed.
{reason_en}

{if: ultimate_participant && ultimate_participant.name != ''}
{ultimate_participant.name} selected final choices.
{endif}
{if: ultimate_participant && survey.has_date_questions}
You can add them to your agenda with the attachment "invite.ics".
{endif}

{if: survey.hide_answers == false && ultimate_participant }
Final choices :
    {each: ultimate_participant.Answers as answer}
    {answer.Question.statement} : {answer.raw_labels}

    {endeach}
{endif}

{if: survey.hide_answers == false}
View results : {survey.results_path}
{endif}

{if: ultimate_participant == false}
If you are an owner of this survey you can reopen it. To do so, go to "Manage survey", and then click on the reopen icon.
{endif}

Regards,
The {cfg:application_name} Team

{alternative:html}

<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Clôture du sondage : "{survey.title}"</h1>
    <p>
        Le sondage "{survey.title}" a été clos. <br />
        {reason}<br />
        {if: ultimate_participant && ultimate_participant.name != ''}
        {ultimate_participant.name} a sélectionné les réponses finales.
        {endif}
        {if: ultimate_participant && survey.has_date_questions}
        <br />
        Vous avez la possibilité de les intégrer dans votre agenda grâce au fichier joint "invite.ics".
        {endif}
    </p>
    {if: survey.hide_answers == false && ultimate_participant }
    <table style="{cfg:email.style.table}">
        <tr><th style="padding:0.5rem 0">Choix finaux :</th></tr>
        {each: ultimate_participant.Answers as answer}
        <tr><td style="text-align:left; padding:0.5rem 0.5rem 0.5rem 1rem;"><li>{answer.Question.statement} : <b>{answer.raw_labels}</b></li></td></tr>
        {endeach}
    </table>
    {endif}
    {if: survey.hide_answers == false}
    <p>
        <a href="{survey.results_path}" style="{cfg:email.style.green_button}">Voir les résultats</a>
    </p>
    {endif}
    {if: ultimate_participant == false}
    <p>
        Si vous êtes propriétaire du sondage, vous pouvez le réouvrir.<br/>
        Pour cela, allez dans “Gérer les sondages”, puis cliquez sur l'icône de réouverture du sondage.
    </p>
    {endif}
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Closure of the survey: "{survey.title}"</h1>
    <p>
        The survey "{survey.title}" has been closed. <br />
        {reason_en}<br />
        {if: ultimate_participant && ultimate_participant.name != ''}
        {ultimate_participant.name} selected final choices.
        {endif}
        {if: ultimate_participant && survey.has_date_questions}
        <br />
        You can add them to your agenda with the attachment "invite.ics".
        {endif}
    </p>
    {if: survey.hide_answers == false && ultimate_participant }
    <table style="{cfg:email.style.table}">
        <tr><th style="padding:0.5rem 0">Final choices:</th></tr>
        {each: ultimate_participant.Answers as answer}
        <tr><td style="text-align:left; padding:0.5rem 0.5rem 0.5rem 1rem;"><li>{answer.Question.statement} : <b>{answer.raw_labels}</b></li></td></tr>
        {endeach}
    </table>
    {endif}
    {if: survey.hide_answers == false}
    <p>
        <a href="{survey.results_path}" style="{cfg:email.style.green_button}">View results</a>
    </p>
    {endif}
    {if: ultimate_participant == false}
    <p>
        If you are an owner of this survey you can reopen it.<br/>
        To do so, go to "Manage survey", and then click on the reopen icon.
    </p>
    {endif}
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>