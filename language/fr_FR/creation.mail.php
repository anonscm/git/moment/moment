subject: Création du sondage : {survey.title}

{alternative:plain}

Création du sondage : {survey.title}

Vous avez créé le sondage "{survey.title}".

Lien d'accès : {survey.path}
Lien pour la mise à jour : {survey.update_path}

Cordialement,
L'équipe {cfg:application_name}

=====================================================================

Creation of the survey: {survey.title}

You've created the survey "{survey.title}".

Access link: {survey.path}
Update link: {survey.update_path}

Regards,
The {cfg:application_name} Team

{alternative:html}
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Création du sondage : "{survey.title}"</h1>
    <p>
        Vous avez créé le sondage "{survey.title}".
    </p>
    <p>
        <a href="{survey.path}" style="{cfg:email.style.green_button}">Accéder au sondage</a>
    </p>
    <p>
        <a href="{survey.update_path}" style="{cfg:email.style.red_button}">Modifier le sondage</a>
    </p>
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Creation of the survey: "{survey.title}"</h1>
    <p>
        You've created the survey "{survey.title}".
    </p>
    <p>
        <a href="{survey.path}" style="{cfg:email.style.green_button}">Access the survey</a>
    </p>
    <p>
        <a href="{survey.update_path}" style="{cfg:email.style.red_button}">Update the survey</a>
    </p>
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>