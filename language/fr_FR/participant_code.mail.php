subject: Tentative de modification de vos réponses au sondage : {survey.title}

{alternative:plain}

Bonjour,

Nous avons détecté une tentative de modification de votre réponse au sondage {survey.title}.

S'il ne s'agit pas de vous, vous pouvez ignorer ce message. La modification ne sera pas prise en compte.

Si vous êtes bien à l'origine de cette demande, merci de saisir le code suivant dans votre navigateur. Cela validera la modification de vos réponses.

Code à saisir : {code}

Cordialement,
L'équipe {cfg:application_name}

=====================================================================

Hello,

We detected a modification attempt on your answer to {survey.title}.

If it is not you, you can ignore this message. No modification will be taken into account.

If you are the requester, please put the following code in the popup input. This will confirm your modification.

Regards,
The {cfg:application_name} Team

{alternative:html}
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Tentative de modification de vos réponses au sondage : "{survey.title}"</h1>
    <p>
        Bonjour,<br/>
        Nous avons détecté une tentative de modification de votre réponse au sondage {survey.title}.<br/>
        S'il ne s'agit pas de vous, vous pouvez ignorer ce message. La modification ne sera pas prise en compte.<br/>
        Si vous êtes bien à l'origine de cette demande, merci de saisir le code suivant dans votre navigateur. Cela validera la modification de vos réponses.<br/>
        <br/>
        Code à saisir : <b>{code}</b><br/>
    </p>
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Modification attempt on your answers to : "{survey.title}"</h1>
    <p>
        Hello,<br/>
        We detected a modification attempt on your answer to {survey.title}.<br/>
        If it is not you, you can ignore this message. No modification will be taken into account.<br/>
        If you are the requester, please put the following code in the popup input. This will confirm your modification.<br>
        <br/>
        Code to use :  <b>{code}</b><br/>
    </p>
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>