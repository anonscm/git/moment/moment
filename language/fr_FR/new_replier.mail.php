subject: Nouvelle participation au sondage : "{survey.title}"

{alternative:plain}

Nouvelle participation au sondage : "{survey.title}"

Nom du participant : {participant.ensure_name}

Pour voir les résultats : {survey.results_path}

Cordialement,
L'équipe {cfg:application_name}

=====================================================================

New participation to the survey : "{survey.title}"

Participant name : {participant.ensure_name}

Results link : {survey.results_path}

Regards,
The {cfg:application_name} Team

{alternative:html}
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Nouvelle participation au sondage : "{survey.title}"</h1>
    <p>
        Nom du participant : {participant.ensure_name}
    </p>
    <p>
        <a href="{survey.results_path}" style="{cfg:email.style.green_button}">Voir les résultats</a>
    </p>
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">New participation to the survey : "{survey.title}"</h1>
    <p>
        Participant name : {participant.ensure_name}
    </p>
    <p>
        <a href="{survey.results_path}" style="{cfg:email.style.green_button}">View results</a>
    </p>
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>