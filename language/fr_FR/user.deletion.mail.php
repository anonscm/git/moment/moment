subject: Suppression de votre compte

{alternative:plain}

Suppression de votre compte

Bonjour,

En l'absence d'activité sur votre compte depuis plus de {cfg:user_cleaning.inactive_months} mois, nous sommes donc au regret de vous informer que, conformément au règlement de protection des données à caractère personnel, votre compte {cfg:application_name} et toutes les données s'y rattachant ont été supprimés.
Si vous souhaitez ré-utiliser {cfg:application_name} dans le futur, il vous suffira de vous y connecter pour qu'un nouveau compte soit automatiquement créé. Veuillez noter que tous les sondages ou calendriers que vous auriez pu renseigner sur votre précédent compte ne seront plus présents.

Cordialement,
L'équipe {cfg:application_name}.

=====================================================================

Deletion of your account

Hello,

There has been no activity on your account for more than {cfg:user_cleaning.inactive_months} months, we regret to inform you that, in accordance with the Data Protection Regulation, your {cfg:application_name} account and all related data have been deleted.
If you wish to re-use {cfg:application_name} in the future, simply log in and a new account will automatically be created. Please note that any surveys or calendars you may have entered on your previous account will no longer be present.

Regards,
The {cfg:application_name} Team

{alternative:html}

<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Suppression de votre compte</h1>
    <p style="margin-left:1rem; text-align: left;">
        Bonjour,<br/>
        En l'absence d'activité sur votre compte depuis plus de {cfg:user_cleaning.inactive_months} mois, nous sommes donc au regret de vous informer que, conformément au règlement de protection des données à caractère personnel, votre compte {cfg:application_name} et toutes les données s'y rattachant ont été supprimés.<br/>
        Si vous souhaitez ré-utiliser {cfg:application_name} dans le futur, il vous suffira de vous y connecter pour qu'un nouveau compte soit automatiquement créé. Veuillez noter que tous les sondages ou calendriers que vous auriez pu renseigner sur votre précédent compte ne seront plus présents.<br/>
    </p>
    <p style="margin-left:1rem; text-align: left;">
        Cordialement,<br/>
        L'équipe {cfg:application_name}.
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Deletion of your account</h1>
    <p style="margin-left:1rem; text-align: left;">
        Hello,<br/>
        There has been no activity on your account for more than {cfg:user_cleaning.inactive_months} months, we regret to inform you that, in accordance with the Data Protection Regulation, your {cfg:application_name} account and all related data have been deleted.<br/>
        If you wish to re-use {cfg:application_name} in the future, simply log in and a new account will automatically be created. Please note that any surveys or calendars you may have entered on your previous account will no longer be present.<br/>
    </p>
    <p style="margin-left:1rem; text-align: left;">
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>
