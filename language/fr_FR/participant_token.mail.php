subject: Lien d'accès à vos réponses pour le sondage : {survey.title}

{alternative:plain}

Lien d'accès à vos réponses pour le sondage : {survey.title}

{if:reason == 'answer_created'}
Merci d'avoir répondu au sondage.
Le lien suivant vous permettra d'accéder à la modification de vos réponses :
{endif}

{if:reason == 'comment_created'}
Merci d'avoir laissé un commentaire sur le sondage.
Le lien suivant vous permettra d'accéder à la modification de vos commentaires :
{endif}

{if:reason == 'reminder'}
Pour rappel :
Le lien suivant vous permettra d'accéder à la modification de vos réponses et/ou commentaires :
{endif}
{survey_direct_link_with_token}

Cordialement,
L'équipe {cfg:application_name}

=====================================================================

{if:reason == 'answer_created'}
Thank you for having responded to the survey.
The following link will give you access to modify your answers :
{endif}

{if:reason == 'comment_created'}
Thank you for leaving a comment on the survey.
The following link will give you access to modify your comments :
{endif}

{if:reason == 'reminder'}
Reminder :
The following link will give you access to modify your answers and/or comments :
{endif}
{survey_direct_link_with_token}

Regards,
The {cfg:application_name} Team

{alternative:html}
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Lien d'accès à vos réponses pour le sondage : "{survey.title}"</h1>
    <p>
        {if:reason == 'answer_created'}
        Merci d'avoir répondu au sondage. <br/>
        Le lien suivant vous permettra d'accéder à la modification de vos réponses :
        {endif}

        {if:reason == 'comment_created'}
        Merci d'avoir laissé un commentaire sur le sondage. <br/>
        Le lien suivant vous permettra d'accéder à la modification de vos commentaires :
        {endif}

        {if:reason == 'reminder'}
        Pour rappel :<br/>
        Le lien suivant vous permettra d'accéder à la modification de vos réponses et/ou commentaires :
        {endif}
    </p>
    <p>
        <a href="{survey_direct_link_with_token}" style="{cfg:email.style.green_button}">Accéder à la modification</a>
    </p>
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Survey access token for : "{survey.title}"</h1>
    <p>
        {if:reason == 'answer_created'}
        Thank you for having responded to the survey. <br/>
        The following link will give you access to modify your answers :
        {endif}

        {if:reason == 'comment_created'}
        Thank you for leaving a comment on the survey. <br/>
        The following link will give you access to modify your comments :
        {endif}

        {if:reason == 'reminder'}
        Reminder : <br/>
        The following link will give you access to modify your answers and/or comments :
        {endif}
    </p>
    <p>
        <a href="{survey_direct_link_with_token}" style="{cfg:email.style.green_button}">Access to the modification</a>
    </p>
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>