subject: Modification du sondage : {survey.title}

{alternative:plain}

Modification du sondage : {survey.title}

Le sondage a été modifié par {owner_requestor}.
Dans le cadre de la modification ou la suppression de choix de questions,
il est possible que certaines de vos réponses aient été supprimées.

Vous êtes invité à retourner sur le sondage pour compléter vos réponses si nécessaire.

Lien d'accès : {survey_path}

Cordialement,
L'équipe {cfg:application_name}

=====================================================================

Modification of the survey: {survey.title}

The survey was modified by {owner_requestor}.
In case of choices modification or removal,
some of your answers may have been deleted.

You are invited to come back to the survey and complete your answers if needed.

Access link: {survey_path}

Regards,
The {cfg:application_name} Team

{alternative:html}
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Modification du sondage : "{survey.title}"</h1>
    <p>
        Le sondage a été modifié par {owner_requestor}. <br/>Dans le cadre de la modification ou la suppression de choix de questions,
        il est possible que certaines de vos réponses aient été supprimées.
    </p>
    <p>
        Vous êtes invité à retourner sur le sondage pour compléter vos réponses si nécessaire.
    </p>
    <p>
        <a href="{survey_path}" style="{cfg:email.style.green_button}">Accéder au sondage</a>
    </p>
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Modification of the survey: "{survey.title}"</h1>
    <p>
        The survey was modified by {owner_requestor}. <br/>In case of choices modification or removal,
        some of your answers may have been deleted.
    </p>
    <p>
        You are invited to come back to the survey and complete your answers if needed.
    </p>
    <p>
        <a href="{survey_path}" style="{cfg:email.style.green_button}">Access the survey</a>
    </p>
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>