subject: Récupération de votre participation au sondage : {survey.title}

{alternative:plain}

Bonjour,

Une demande de récupération de vos réponses au sondage {survey.title} a été formulée.
{if: participant && survey.disable_answer_edition }
Vous pouvez visualiser vos réponses en suivant le lien ci-contre :
{endif}
{if: participant && survey.disable_answer_edition == false}
Vous pouvez modifier vos réponses en suivant le lien ci-contre :
{endif}
{if: participant == false}
Nous n'avons trouvé aucune participation à ce sondage correspondant à cette adresse mail.
Si vous aviez répondu anonymement à ce sondage, nous ne sommes pas en mesure d'identifier votre participation et de vous renvoyer un lien de modification.
Vous pouvez répondre au sondage en suivant le lien ci-contre :
{endif}
{survey_participant_link}.
Si vous n'êtes pas l'auteur de cette demande, vous pouvez ignorer ce message.

Cordialement,
L'équipe {cfg:application_name}.

=====================================================================

Hello,

A recovery request for your answers to {survey.title} have been made.
{if: participant && survey.disable_answer_edition}
The following link will give you access to view your answers : {survey_participant_link}.
{endif}
{if: participant && survey.disable_answer_edition == false}
The following link will give you access to edit your answers : {survey_participant_link}.
{endif}
It you are not the requester, you can ignore this message.
{if: participant == false}
We did not find any participant to this survey with this email.
If you answered anonymously, we can't identity your participation.
You can answer to this survey here :
{endif}

Regards,
The {cfg:application_name} Team

{alternative:html}

<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Récupération de votre participation au sondage : "{survey.title}"</h1>
    <p>Bonjour,</p>
    <p>Une demande de récupération de vos réponses au sondage {survey.title} a été formulée.<br/>
        {if: participant && survey.disable_answer_edition}
        Vous pouvez visualiser vos réponses en suivant le lien ci-contre :
        {endif}
        {if: participant && survey.disable_answer_edition == false}
        Vous pouvez modifier vos réponses en suivant le lien ci-contre :
        {endif}
        {if: participant == false}
        Nous n'avons trouvé aucune participation à ce sondage correspondant à cette adresse mail.<br/>
        Si vous aviez répondu anonymement à ce sondage, nous ne sommes pas en mesure d'identifier votre participation et de vous renvoyer un lien de modification.<br/>
        Vous pouvez répondre au sondage en suivant le lien ci-contre :
        {endif}
    </p>
    <p>
        {if: participant == false}
        <a href="{survey_participant_link}" style="{cfg:email.style.green_button}">Accéder au sondage</a>
        {else}
        <a href="{survey_participant_link}" style="{cfg:email.style.green_button}">Accéder à mes réponses</a>
        {endif}
    </p>
    <p>Si vous n'êtes pas l'auteur de cette demande, vous pouvez ignorer ce message.</p>
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
<hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Recovery of your answers to : "{survey.title}"</h1>
    <p>Hello,</p>
    <p>A recovery request for your answers to {survey.title} have been made.<br/>
        {if: participant && survey.disable_answer_edition}
        The following link will give you access to view your answers :
        {endif}
        {if: participant && survey.disable_answer_edition == false}
        The following link will give you access to edit your answers :
        {endif}
        {if: participant == false}
        We did not find any participant to this survey with this email.
        If you answered anonymously, we can't identity your participation.
        You can answer to this survey here :
        {endif}
    </p>
    <p>
        {if: participant == false}
        <a href="{survey_participant_link}" style="{cfg:email.style.green_button}">Access the survey</a>
        {else}
        <a href="{survey_participant_link}" style="{cfg:email.style.green_button}">Access to your answer</a>
        {endif}
    </p>
    <p>It you are not the requester, you can ignore this message.</p>
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>
