subject: Confirmation de la suppression de vos réponses au sondage : "{survey.title}"

{alternative:plain}

Suppression de réponses au sondage "{survey.title}"

Vos réponses au sondage "{survey.title}" ont bien été supprimées.
Le lien de modification qui vous a été transmis précédemment n'est plus valide.
Vous pouvez de nouveau répondre à ce sondage si vous le souhaitez en utilisant le lien ci-dessous.

Lien d'accès : {survey.path}

Cordialement,
L'équipe {cfg:application_name}

=====================================================================

Deletion of your answers for the survey : "{survey.title}"

Your answers to "{survey.title}" have been deleted.
Your access token is no longer valid.
You can answer the survey by using the following link.

Access link : {survey.path}

Regards,
The {cfg:application_name} Team

{alternative:html}
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Suppression de réponses au sondage : "{survey.title}"</h1>
    <p>
        Vos réponses au sondage "{survey.title}" ont bien été supprimées.
        Le lien de modification qui vous a été transmis précédemment n'est plus valide.
        Vous pouvez de nouveau répondre à ce sondage si vous le souhaitez en utilisant le lien ci-dessous.
    </p>
    <p>
        <a href="{survey.path}" style="{cfg:email.style.green_button}">Accéder au sondage</a>
    </p>
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Participant answers deleted for the survey : "{survey.title}"</h1>
    <p>
        Your answers to "{survey.title}" have been deleted.
        Your access token is no longer valid.
        You can answer the survey by using the following link.
    </p>
    <p>
        <a href="{survey.path}" style="{cfg:email.style.green_button}">Access the survey</a>
    </p>
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>