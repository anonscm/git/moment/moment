subject: Rappel de clôture prochaine du sondage : "{survey.title}"

{alternative:plain}

Rappel de clôture prochaine du sondage : "{survey.title}"

Le sondage "{survey.title}" sera clos automatiquement dans {cfg:closing_reminder_delay} jours.
Pensez à relancer les personnes qui n'ont pas encore répondu.
Vous pouvez également repousser la date de clôture si vous avez besoin d'un délai supplémentaire.

{if: survey.hide_answers == false}
Voir les résultats : {survey.results_path}
{endif}

Cordialement,
L'équipe {cfg:application_name}

=====================================================================

Reminder of the closure of the survey : "{survey.title}"

The survey "{survey.title}" will be closed automatically in {cfg:closing_reminder_delay} days.
You can revive people who did not answer.
You can also modify the closing date if you need any delay.

{if: survey.hide_answers == false}
View results : {survey.results_path}
{endif}

Regards,
The {cfg:application_name} Team

{alternative:html}

<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Rappel de clôture prochaine du sondage : "{survey.title}"</h1>
    <p>
        Le sondage "{survey.title}" sera clos automatiquement dans {cfg:closing_reminder_delay} jours. <br />
        Pensez à relancer les personnes qui n'ont pas encore répondu. <br />
        Vous pouvez également repousser la date de clôture si vous avez besoin d'un délai supplémentaire. <br />
    </p>
    {if: survey.hide_answers == false}
    <p>
        <a href="{survey.results_path}" style="{cfg:email.style.green_button}">Voir les résultats</a>
    </p>
    {endif}
    <p>
        Cordialement,<br/>
        L'équipe {cfg:application_name}
    </p>
</div>
    <hr/>
<div style="{cfg:email.style.main}">
    <h1 style="{cfg:email.style.h1}">Reminder of the closure of the survey : "{survey.title}"</h1>
    <p>
        The survey "{survey.title}" will be closed automatically in {cfg:closing_reminder_delay} days.<br />
        You can revive people who did not answer.<br />
        You can also modify the closing date if you need any delay.<br />
    </p>
    {if: survey.hide_answers == false}
    <p>
        <a href="{survey.results_path}" style="{cfg:email.style.green_button}">View results</a>
    </p>
    {endif}
    <p>
        Regards,<br/>
        The {cfg:application_name} Team
    </p>
</div>