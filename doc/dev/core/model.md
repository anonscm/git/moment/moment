# Model

The ApplicationBase provides a simple way to define your data model with automatic database structure update.

## Defining entities

### Concept

An entity class describe one entity from your model, provides methods to deal with the entity life cycle and various model related utilities.

It also maps the entity at the PHP api level to its database representation.

Finally it handles performance related improvements like object, relations and collection caching.


### Common ancestor

All defined entities should inherit from the [`Entity` class](@php/Entity), it provides them with the data mapping, handling and caching utilities.

The [`Entity` class](@php/Entity) defines a lot of default static and non-static properties as well as a number of shared methods, you should avoid tampering with them to keep the ApplicationBase's integrity.


### Datamap

The datamap describes the properties/attributes of your entity, their formats, default values and optionnal transformations (for storage).

It has 2 parts for each property :

  * A data field definition
  * A property registration


#### Property definitions

Property definitions are bundled into the `$dataMap` static property of your entity. It is an array whose keys are the property names and values are the data field definition :

    protected static $dataMap = array(
        'id' => array(
            'type' => 'string',
            'size' => 255,
            'primary' => true
        ),
        'attributes' => array(
            'type' => 'text',
            'transform' => 'json'
        ),
        'name' => array(
            'type' => 'string',
            'size' => 8,
            'null' => true
        ),
        'created' => array(
            'type' => 'datetime'
        )
    );

Possible field definition entries are (each data field has at least a `type` entry) :


##### Types

| Type identifier     | Data type                              | Options                                                                                                                                                                 |
|---------------------|----------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `int`               | Signed integer                         | `size` either `tiny`, `small`, `medium` or `big` (default is `medium`), see [MySQL Integer Types](http://dev.mysql.com/doc/refman/5.7/en/integer-types.html) for ranges |
| `uint`              | Unsigned integer                       | see `int`                                                                                                                                                               |
| `float`             | Single precision floating point number | none                                                                                                                                                                    |
| `double`            | Double precision floating point number | none                                                                                                                                                                    |
| `decimal`           | Fixed precision number                 | `size` as the number of significant digits and `precision` as the decimal part length (both mandatory)                                                                  |
| `date`              | Date of day                            | none                                                                                                                                                                    |
| `datetime`          | Date and time of day                   | none                                                                                                                                                                    |
| `time`              | Time of day                            | none                                                                                                                                                                    |
| `bool` or `boolean` | Boolean                                | none                                                                                                                                                                    |
| `string`            | Short string (up to 255 characters)    | `size` as the string length (mandatory)                                                                                                                                 |
| `text`              | Long string / text                     | none                                                                                                                                                                    |
| `enum`              | Fixed set of value alternatives        | `values` as array of possible values                                                                                                                                    |


##### Additional options

| Option      | Use                                                                                              | Value                                      |
|-------------|--------------------------------------------------------------------------------------------------|--------------------------------------------|
| `primary`   | Flag a field as primary key                                                                      | `true`                                     |
| `index`     | Flag the field as an index (or part of)                                                          | `true` or string identifier if part of set |
| `unique`    | Add an unicity constraint to the field (or set it as part of an unicity constraint)              | `true` or string identifier if part of set |
| `autoinc`   | Set the field to be automatically incremented upon new entity save (requires `type` to be `int`) | `true`                                     |
| `null`      | Allow the field to have a `null` value (not allowed with all type)                               | `true`                                     |
| `default`   | Set the field default value                                                                      | varies depending on `type`                 |
| `transform` | Encode / decode the field value automatically                                                    | See [below](#about_transformations)        |


##### About transformations

If `transform` is set to `json` PHP's [`json_encode`](http://php.net/manual/en/function.json-encode.php) and [`json_decode`](http://php.net/manual/en/function.json-decode.php) will be used to encode and decode the value upon saving and loading instances.

Other values will be passed to the [`ucfirst` function](http://php.net/manual/en/function.ucfirst.php) and a corresponding static method will be expected, it has to take 2 arguments :

  * a `boolean`, `false` if converting from database (loading), `true` if converting to database (saving)
  * the raw data to convert


#### Property registration

Properties are registered within the PHP api using standard object properties :

    protected $id = null;
    protected $attributes = null;
    protected $name = null;
    protected $created = 0;

It is advised that you ALWAYS set a default value for the PHP api properties, unless you need something specific here are standard values :

  * Nullable properties should have a default of `null`
  * Non-nullable date-related properties should have a defalut value of `0` (zero)


### Relations

TODO


## Usefull shared methods

Scope refers to use cases :

  * `pub` many you are free to use this method
  * `int` means this method mainly exists to be used by other method and that there shoud be a better suitted, simpler to use method around


### Entity definition

| Method name                  | Scope | Description                                                           | Arguments                                        | Returns             |
|------------------------------|-------|-----------------------------------------------------------------------|--------------------------------------------------|---------------------|
| `Entity::name`               | pub   | Get the entity name                                                   | `boolean`, if `true` the plural name is returned | `string`            |
| `Entity::getClassName`       | pub   | Alias of `Entity::name(false)`                                        | none                                             |                     |
| `Entity::getPluralClassName` | pub   | Alias of `Entity::name(true)`                                         | none                                             |                     |
| `Entity::getPluralName`      | pub   | Get a plural name from a single name using basic english plural rules | `string`, the single name                        | `string`            |
| `Entity::getSingleName`      | pub   | Get a single name from a plural name using basic english plural rules | `string`, the plural name                        | `string`            |
| `Entity::getPrimaryKeys`     | int   | Get the list of the entity's primary keys                             | none                                             | `array` of `string` |


### Entity relations state

| Method name                    | Scope | Description                                                            | Arguments                       | Returns                            |
|--------------------------------|-------|------------------------------------------------------------------------|---------------------------------|------------------------------------|
| `Entity::getHasMany`           | int   | Get the entity names the current entity has a `has many` relation with | none                            | `array` of `string`                |
| `Entity::getHasOne`            | int   | Get the entity names the current entity has a `has one` relation with  | none                            | `array` of `string`                |
| `Entity::getHasManFromPlural`  | int   | Get single related class name from plural form                         | `string`, the plural name       | `string` or `null`                 |
| `Entity::getRelationWith`      | pub   | Get relation state between current entity and other entity             | `string`, the other entity name | [relation state](#relation_states) |


#### Relation states

Relation states are represented using `int` whose value can be :

  * `0` or `Entity::RELATION_NONE` : no relation
  * `1` or `Entity::RELATION_ONE_TO_ONE` : each current entity instances may be related to many instances of other entity
  * `1` or `Entity::RELATION_ONE_TO_MANY` : each current entity instances may be related to many instances of other entity
  * `2` or `Entity::RELATION_MANY_TO_ONE` : each other entity instances may be related to many instances of current entity
  * `3` or `Entity::RELATION_MANY_TO_MANY` : each instance of both current and other entities may be related both ways


### Data mapping

| Method name                    | Scope | Description                                                                                                                                                                                                                     | Arguments                                                                                       | Returns                                                    |
|--------------------------------|-------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------|------------------------------------------------------------|
| `Entity::getDataMap`           | pub   | Get the analysed and merged datamap (that is the final entity datamap), includes the [fields you defined](#datamap), fields [inherited from parent entities](#inheritance) and fields related to [entity relations](#relations) | `boolean`, if `true` the returned datamap does not include relation fields (default is `false`) | `array` with the same structure as the [datamap](#datamap) |
| `Entity::buildRelationDatamap` | int   | Get the fields for the many-to-many relation table                                                                                                                                                                              | `string`, the other entity name                                                                 | `array` with the same structure as the [datamap](#datamap) |


### Database related

| Method name                        | Scope | Description                                                                | Arguments                                                                               | Returns  |
|------------------------------------|-------|----------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|----------|
| `Entity::getDBTable`               | int   | Get the database table name for the current entity                         | `boolean`, if `true` the configured database table prefix is returned as well (default) | `string` |
| `Entity::getRelationDBTable`       | int   | Get the relation table name with other entity (for many-to-many relations) | `string`, the other entity name                                                         | `string` |
| `Entity::updateStructure`          | int   | Update database table structure                                            | none                                                                                    | none     |
| `Entity::updateRelationsStructure` | int   | Update relation tables structure                                           | none                                                                                    | none     |


### Instance identification and property access

| Method name              | Scope | Description                                                                | Arguments                                                                                                       | Returns             |
|--------------------------|-------|----------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|---------------------|
| `Entity::serializeUID`   | int   | Serialize a unique identifier                                              | Unique identifier as `array`                                                                                    | `string`            |
| `Entity::buildUID`       | int   | Get unique identifier from given                                           | Thing to get unique identifier from (`Entity`, `array`), `boolean` to get unique identifier as scalar (default) | `string` or `array` |
| `$entity->getUID`        | pub   | Get unique instance identifier                                             | `boolean` to get unique identifier as scalar (default)                                                          | `string` or `array` |
| `$entity->getRelatedUID` | pub   | Get unique instance identifier of related entity for one-to-many relations | The other entity name as `string` and a `boolean` to get unique identifier as scalar (default)                  | `string` or `array` |
| `$entity->__toString`    | pub   | Instance stringifier                                                       | none                                                                                                            | `string`            |
| `$entity->is`            | pub   | Instance comparator                                                        | The other instance                                                                                              | `boolean`           |
| `$entity->__get`         | pub   | Property getter                                                            | Related entity name                                                                                             | `array`             |
| `$entity->__set`         | pub   | Property setter                                                            | Related entity name, (`array` of) instance                                                                      | none                |


### Instance loading

| Method name                 | Scope | Description                                                                       | Arguments                                                                                                               | Returns                                                               |
|-----------------------------|-------|-----------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------|
| `Entity::__construct`       | int   | Load instance from database and / or fill with data                               | Instance identifier (`string` or `array`), `array` of data to fill with                                                 | instance                                                              |
| `Entity::fromId`            | pub   | Load instance from unique identifier                                              | Instance identifier (`string` or `array`)                                                                               | instance (honors caching)                                             |
| `Entity::fromData`          | pub   | Load instance from already fetched data                                           | Data as `array`                                                                                                         | instance (honors caching)                                             |
| `Entity::all`               | pub   | Get a collection of instances that match a criteria                               | crtieria (SQL WHERE clause) as `string`, `array` of placeholders, optional `callable` to call for each created instance | `array` of instances (honors instance caching and collection caching) |
| `Entity::from`              | pub   | Load instance (or instance collection) from related entities instances            | `array` of other entities instances                                                                                     | `array` of instances                                                  |
| `$instance->fillFromDBData` | int   | Fill instance with data applying datamap conversions                              | Data as `array`                                                                                                         | none                                                                  |


### Instance saving

| Method name               | Scope | Description                                                                       | Arguments                                     | Returns                                              |
|---------------------------|-------|-----------------------------------------------------------------------------------|-----------------------------------------------|------------------------------------------------------|
| `$instance->toDBData`     | int   | Get database-ready data from the instance properties applying datamap conversions | none                                          | `array`                                              |
| `$instance->insertRecord` | int   | Insert instance data as a new database record                                     | Data as `array`                               | `array` of new incremental primary key values if any |
| `$instance->updateRecord` | int   | Update a database record from data                                                | Data as `array`, SQL WHERE clause as `string` | none                                                 |
| `$instance->save`         | pub   | Save instance (be it a new one or a loaded one)                                   | none                                          | none                                                 |
| `$instance->delete`       | pub   | Delete instance                                                                   | none                                          | none                                                 |


### Instance relations handling

| Method name              | Scope | Description                                                    | Arguments                                                                           | Returns                                                     |
|--------------------------|-------|----------------------------------------------------------------|-------------------------------------------------------------------------------------|-------------------------------------------------------------|
| `$instance->getRelated`  | pub   | Get instances of other entity related to the current one       | `string`, the other entity name                                                     | `array` or single instance of other entity (honors caching) |
| `$instance->addRelated`  | pub   | Add other entity instance to current instance related entities | Instance or `array` of instances                                                    | none                                                        |
| `$instance->dropRelated` | pub   | Drop relation between current entity and related entities      | Single instance or entity name (drop all) or `array` of mixed                       | none                                                        |
| `Entity::preloadFrom`    | pub   | Preload instances base on related entities (bulk loading)      | `array` of entities, `boolean` if grouping by related is required (default `false`) | `array`                                                     |


## Custom methods

You must be careful when overloading all the previously mentionned methods because a big part of them are used internally.

Here are some methods you can define or overload freely :

  * `$entity->__get` : property getter, call `parent::__get` to keep related getter support
  * `$entity->__set` : property setter, call `parent::__set` to keep related setter support
  * `$entity->customSave` : run your own saving process instead of the default `insertRecord`/`updateRecord` one
  * `$entity->beforeDelete` : do something before the entity instance is deleted from database, usefull to remove related or such


## Default entities

### User

The default `User` entity provides basic user handling with standard attributes support (name, emails ...).

You can chage the `User` entity behaviour by overriding the provided [`User` class](@php/User) (your class MUST extend the [`UserBase` class](@php/UserBase)).

You can add support for other attributes by overriding the original [`UserAttributes` class](@php/UserAttributes) (your class MUST extend the [`UserAttributesBase` class](@php/UserAttributesBase)).


### TranslatableEmail

The provided `TranslatableEmail` entity helps handling translatable emails (that contains a link to a public user interface allowing to translate the email contents).

You can override it but you must at least have the same methods and scopes as the original [`TranslatableEmail` class](@php/TranslatableEmail) (which you cannot inherit from because of the autoloading pattern).
