# Authentication system

The provided authentication system supports the following modes : [federated authentication](./authentication/federated.md), [remote authentication](./authentication/remote.md) and [local authentication](./authentication/local.md).

The system first checks for existing authentication in this order : local, then remote and finally federated.


## Tweaking the authentication process

In order to change the authentication lookup process, add new modes, remove existing modes (check if you can disable them first !) you have to copy the `classes/core/auth/Auth.class.php` file to `classes/auth/` first so your changes won't be deleted by future updates.


## Writing your authentication mode

If you want to add another federated authentication mode see [federated authentication](./authentication/federated.md) instead.

An authentication mode is basically represented by a PHP class whose name starts with `Auth` and which is stored in `classes/auth/AuthYourChoice.class.php`.

This class MUST implement the [AuthInterface interface](phpdoc/AuthInterface).

It have 2 mandatory `public static` methods :

  * `isAuthenticated` returns a `boolean` indicating whether there is an authenticated user or not ;
  * `attributes` returns the authenticated user's attributes as an `associative array` with the following data :
    * `uid` user unique identifier (`string`, mandatory)
    * `email` user's email addresses (`array`)
    * `name` user's name (`string`)
    * `additional` user's additional attributes for logging (`associative array`)

Example :

    class AuthBasic implements AuthInterface {
        public static function isAuthenticated() {
            return $_SERVER['REMOTE_USER'] != '';
        }
        
        public static function attributes() {
            return array(
                'uid' => $_SERVER['REMOTE_USER'],
                'name' => $_SERVER['REMOTE_USER'],
                'email' => array(
                    $_SERVER['REMOTE_USER'].'@localhost'
                )
            );
        }
    }
