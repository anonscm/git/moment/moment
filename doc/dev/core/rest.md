# REST server

The ApplicationBase provides a fully-featured REST server with support for several request and response formats.

You can easily implement your own endpoints without thinking too much about data conversion and encoding.


## Requests

The built-in REST server complies with [common REST guidelines](https://en.wikipedia.org/wiki/Representational_state_transfer) and also includes several features to make it flexible.


### Resource path dispatching

The REST server automatically extracts the resource path from the request URI and split it on the `/` character.

The first token from the splitted path is considered to be the endpoint name, it should correspond to a `*Endpoint` class, its value is available in endpoints by using `RestRequest::getEndpoint()`, see [adding endpoints](./rest/endpoints.md#adding_endpoints).

The [HTTP method](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods) tells which method of the endpoint class will be called, its value is available in endpoints by using `RestRequest::getMethod()`.

All other path tokens will passed to the called method as arguments, its value is available in endpoints by using `RestRequest::getPath()`.


### Request data

The REST server is able to process various input data encodings, that is [HTTP request bodies](https://en.wikipedia.org/wiki/HTTP_message_body), based on the sent `Content-Type` :

  * if `application/json` the body will be parsed as [JSON](https://en.wikipedia.org/wiki/JSON)
  * if `text/xml` the body will be parsed as [XML](https://en.wikipedia.org/wiki/XML), expected format is the same as [XML output format](./rest/output_formats.md#xml)
  * if `application/x-www-form-urlencoded` the body will be parsed as [web form data](https://en.wikipedia.org/wiki/POST_%28HTTP%29#Use_for_submitting_web_forms)
  * if `text/plain` the body will only be sanitized fot script injection
  * if `application/octet-stream` the body is left untouched

You will be able to access the (parsed) data in your [endpoints](./rest/endpoints.md) by calling `RestRequest::getInput()`.


### Parsed/reserved request arguments

Several [query string arguments](https://en.wikipedia.org/wiki/Uniform_Resource_Locator#Syntax) names are reserved for specific uses :

  * `callback` : used in [JSONP requests](https://en.wikipedia.org/wiki/JSONP), subject to restrictions, see [JSONP](./rest/jsonp.md)
  * `frame_callback` : used in [post JSONP requests](https://en.wikipedia.org/wiki/JSONP), subject to restrictions, see [JSONP](./rest/jsonp.md)
  * `count` : used to ask the endpoint to limit the returned collection's size, only accepts positive integers, it is up to the endpoint to apply it
  * `startIndex` : used to ask the endpoint to offset the returned collection's start index, only accepts positive integers, it is up to the endpoint to apply it
  * `format` : used to ask for a specific [output format](./rest/output_formats.md), one can also use one of the following file extensions at the end of the resource path to get the same effect for standard formats : `json`, `xml`, `csv`, `plain` (`txt`), `html` (`htm`), `png`, `jpeg` (`jpg`), `gif` or `pdf`
  * `filterOp` : simple output filter, see [output filtering](./rest/output_filtering.md)
  * `filter` : output filter allowing logical operators, see [output filtering](./rest/output_filtering.md) (prefered to `filterOp`)
  * `fields` : response fields set selector, comma separated list of fields names, it is up to the endpoint to apply it, see [output fields](./rest/output_fields.md)
  * `sortOrder` : natural sort order, expects `asc`, `ascending`, `desc` or `descending`, atomatically applied if [REST entity collection getter](./rest/utilities.md#entity_collection_getter) is used by the endpoint
  * `order` : fine sort order, expects JSON array of pairs of fields and sort orders (`asc`, `ascending`, `desc` or `descending`), in priority order, atomatically applied if [REST entity collection getter](./rest/utilities.md#entity_collection_getter) is used by the endpoint
  * `updatedSince` : used to ask the endpoint for data more recent than a specific date, expects either an [ISO date](https://en.wikipedia.org/wiki/ISO_8601), an [unix timestamp](https://en.wikipedia.org/wiki/Unix_time) or a relative date interval (a number followed by one of `hours`, `days`, `weeks`, `months` or `years`), it is up to the endpoint to apply it

All of these arguments can be fetched in your endpoints by using `RestRequest::getOutputProperty('name')`.


### Parsed request properties

Several HTTP headers are analysed and their values can be obtained from your endpoints using `RestRequest::getProperty('name')` :

  * the `Content-Type` header is split and its main value is available as `type`, all sub-types (like the `charset`) are available by using their names


## Processing

You can easily [add endpoints](./rest/endpoints.md#adding_endpoints) to fit your needs.

Several basic endpoints are provided :

  * `/lang` : returns the user interface translations in the language that best fits the user, override it by creating `classes/endpoints/LangEndpoint.class.php`
  * `/user` : allows to get the authentication state and, if admin, list known users, override it by creating `classes/endpoints/UserEndpoint.class.php` (best to extend `UserEndpointBase`)


## Responses

In most cases you won't have to deal with response encoding as it is done automatically depending on what the requestor asked for by using the `format` query argument.

For structured formats (`json`, `xml` or `csv`) your endpoint only needs to return native types (any `scalar`, `array`s ...).

Plain formats (`plain`, `txt`, `html` or `htm`) expects you to return `scalar` data.

Image formats (`png`, `jpeg` (`jpg`) or `gif`) expects you to return a [GD resource](http://php.net/manual/fr/function.imagecreate.php).

`pdf` format can use either already rendered html, through [templates](ui.md#templating) or manually generated, or native types (any `scalar`, `array`s ...) which will be flattened as for `csv` and rendered as a data dump.

To decide what you have to return you can check the following properties using `RestRequest::getOutputProperty('name')` :

  * `format` analysed format : `json`, `xml`, `csv`, `plain`, `html`, `png`, `jpeg`, `gif`, `pdf` or any custom formats
  * `format_type` format type :
    * `structured` for `json` and `xml`
    * `table` for `csv`
    * `plain` for `plain` and `html`
    * `image` for `png`, `jpeg` and `gif`
    * `pdf` for `pdf` since it is a bit peculiar
  * `download` set to true if requestor asked for a file download

If the requestor asks for a download the file name will be automatically generated from the resource path and the appropriate HTTP headers sent.

Your endpoint can force any response format by returning any instance of the `RestResponse*` classes.

Additionnaly you can force a download by returning and instance of `RestFileDownload`.

The two can be combined to force a download of a certain format :

    $graph = imagecreate(100, 100);
    // Draw stuff
    $res = new RestResponsePng($graph);
    return new RestFileDownload($res, 'your_graph.png'); // File name optionnal

Check out [response formats](./rest/output_formats.md).
