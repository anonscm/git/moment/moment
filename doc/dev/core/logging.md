# Logging

The ApplicationBase is provided with a versatile logger module which you can use in your application code.


## Configuring logging

The logger is configured through the [`logging_facilities` config parameter](./config.md#logging_facilities).

This parameter must be an array of facilities which are described using a list of key-value pairs (see below).

All facilities support the following parameters :

  * `level` only log messages over the given level :
    * `error` will only log error messages
    * `warn` will log error and warning messages
    * `info` will log error, warning and informative messages
    * `debug` will log error, warning, informative and debug messages
  * `process` only log messages from specific processes, whitespace, comma or pipe separated (wildcard matches all) set of :
    * `misc` unidentified process (during init)
    * `web` web process (not knowing if webservice or UI yet)
    * `gui` web UI
    * `rest` webservice
    * `cli` command line execution
    * `cron` scheduled task (even if run by hand)
    * `feedback` mail feedback handling
    * `upgrade` upgrader
    * full list may be found in [ProcessTypes constant list](phpdoc/classes/core/constants/ProcessTypes.class.php)

When logging debug message be aware that the log output may (and will) grow large as lots of low level operations (like individual database queries) are logged !


### File facility

Defined at least as such :

    array(
        'type' => 'file',
        'path' => 'path/to/logging/folder/or/file/with/or/without/extension'
    )

If `path` points to a file log will be appended to it, if it points to a directory files with names beginning with `application` and using the `.log` extension will be used.

You can add a `separate_processes` key (value doesn't matter) to get process (web, ui, rest, cli, cron ...) based files (process identifier will be appended to the file name).

You can add a `rotate` key with value being either `hourly`, `daily`, `weekly`, `monthly` or `yearly` to automatically rotate the log file (date will be appended to the file name).

If a file extension is provided it will be appended after any of the above options.

You can add a `file_mode` key with desired file permissions as an [octal number](http://php.net/manual/en/language.types.integer.php) (must be at least 0600).

This facility uses the [`http_uid` config parameter](./config.md#http_uid) to set the file owner.

If provided it also uses the [`http_gid` config parameter](./config.md#http_gid) to set the file group.


### SysLog facility

Defined as such :

    array(
        'type' => 'syslog'
    )

You can add any of the following keys :

  * `ident` string attached to the message
  * `option` combination of `LOG_*` constants
  * `facility` syslog facility to use

See [Php's openlog function](http://php.net/manual/en/function.openlog.php) for more info.


### ErrorLog facility

Defined as such :

    array(
        'type' => 'error_log'
    )

Uses Php's internal logger (goes to Apache's error log if in use, STDERR if used in command line).


### Callable facility

Defined as such :

    array(
        'type' => 'callable',
        'open' => function() { // Optional, called upon logger init
            // Your code
        },
        'log' => function($process, $level, $message) {
            // Your code
        },
        'close' => function() { // Optional, called upon thread closing
            // Your code
        }
    )

Allows for custom logging.


### Writing your own

You can write your own logging facility by extending the [AbstractLogger](phpdoc/classes/core/utils/loggers/AbstractLogger.class.php).


## Logging

Logging can be achieved using any of the method calls below :

  * `Logger::error('Oops')` logs an error message
  * `Logger::warn('Hum')` logs a warning message
  * `Logger::info('Hey')` logs an informative message
  * `Logger::debug('There !')` logs a debug message

Each of those methods take a single argument that can be of any type, message as string, scalar, array, object ... Non-scalar messages are printed through [Php's `print_r` function](http://php.net/manual/en/function.print-r.php).
