# Event system doc

## How to listen to events

Registering to an event :

    Event::register($order, $event_id, $callable);

Where :

  * `$order` (string) must be one of `"before"` or `"after"` (or `Event::BEFORE` or `Event::AFTER`) :
    * `"before"` will run the handler before the default handler runs, your handler will then be allowed to prevent the execution of the default handler
    * `"after"` wiil run the handler after the default handler
  * `$event_id` (string) is the identifier of the event ([see below](#standard_events))
  * `$callable` is your handler, it can be a closure (or lambda/anonymous function), a class method, an object method call ... (see [PHP doc on callable type](http://php.net/manual/en/language.types.callable.php))

When being run your handler will receive a single argument, which is the `Event` object.

This `Event` has an important read/write `data` property (array) which holds all data that will be propagated to next handlers and/or default handler.

The `Event` also has a `result` property holding data returned (if any) by the default handler.

An event handler can take action depending on the data/result an even alter it.

The `Event` object also has two important methods :

  * `preventDefault` : only usefull for `"before"` order, used to prevent the default handler from running
  * `stopPropagation` : used to prevent other registered handlers to run


Example of intercepting a custom REST request :

    Event::register('before', 'rest_request', function($event) {
                
        if(RestRequest::getEndpoint() != 'foo') return; // Only care about our custom endpoint
        
        $event->stopPropagation(); // Forbid any remaining handlers to run
        $event->preventDefault(); // prevent default endpoint lookup and execution
        
        if(RestRequest::getMethod() != 'get')
            throw new RestException('rest_method_not_implemented', 501);
        
        $event->result = array('bar' => RestRequest::getPath()); // Set our own data to be sent to the client after json encoding
    });



## Standard events


### Authentication and user activity


#### `auth_check`

Triggered when checking for authentication status.

Data : none.

Result (in `after`) : none.


#### `auth_sp_trigger`

Triggered when log-in process starts.

Data : none.

Result (in `after`) : none.


#### `auth_sp_logon_url`

Triggered when generating the log-on url.

Data :

  * target url to go back to after authentication

Result (in `after`) : target url the user will be redirected to.


#### `auth_sp_logoff_url`

Triggered when generating the log-off url.

Data :

  * target url to go back to after disconnecting

Result (in `after`) : target url the user will be redirected to.


#### `user_activity`

Triggered when a user accesses the UI.

Data :

  * user object

Result (in `after`) : none.

#### `user_authenticated`

Triggered when a user is authenticated

Data :

  * user object

Result (in `after`) : none.



### User interface

#### `resource_items`

Triggered when rendering items under the "Resources" column in the footer.

Data : none.

Result (in `after`) : array of html strings to be appended to the footer part (in `li`s).


### REST server


#### `rest_request`

Triggered when the REST server receives a request.

Data : none.

Result (in `after`) : the response that will be sent back.



### Utilities


#### `init_done`

Triggered when application initialization ends.

Data : none.

Result (in `after`) : none.


#### `client_config`

Triggered when getting client config.

Data : none.

Result (in `after`) : config associative array.


#### `cron_started`

Triggered after application cronjob ends.

Data : none.

Result (in `after`) : none.


#### `mail_send`

Triggered when sending an email.

Data :

  * the message to be sent as an array with keys (ready to be given to PP's mail function) :
    * `to`
    * `subject`
    * `headers`
    * `body`

Result (in `after`) : boolean indicating success.


#### `mail_sender_lookup`

Triggered when looking for an email sender if either `email.from`, `email.reply_to` or `email.return_path` is configured to `sender` and no main entity the object is about (not set or no `getEmailSender` method or falsish sender returned).

Data :

  * the email translation identifier
  * the object the email is mainly about (if provided)
  * the additionnal translation variables

Result (in `after`) : sender's email address as a `string`.


#### `template_resolve`

Triggered when resolving a template identifier to a path.

Data :

  * the template identifier

Result (in `after`) : template path (relative to application root).


#### `template_process`

Triggered when a template (from path) is processed against a set of variables.

Data :

  * the template id
  * the template path
  * array of variables by their names
  * boolean indicating wether it is ok to wrap the output with comments indicating the template start and end

Result (in `after`) : template output.


### Model


#### `entity_save`

Triggered when saving an object.

Data :

  * the object to be saved

Result (in `after`) : none.


#### `entity_delete`

Triggered when deleting an object.

Data :

  * the object to be deleted

Result (in `after`) : none.


#### `entity_insert`

Triggered when creating a new record.

Data :

  * the class name
  * key-value pairs array of data to be inserted

Result (in `after`) : array of key-value pairs of inserted primary keys.


#### `entity_update`

Triggered when creating a new record.

Data :

  * the class name
  * key-value pairs array of data to be updated
  * array of primary key names (or single name as string)
  * where clause (if provided)

Result (in `after`) : none.
