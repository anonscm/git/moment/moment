# Translating

The ApplicationBase is provided with a key-based translating engine.

## Locales

Available languages are defined in the `/language/locale.php` file (defaults in the `/languages/core/locale.php` file), this files MUST contain a `$locales` [array](http://php.net/manual/en/language.types.array.php) whose keys are the language codes (use of [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) codes is encouraged) and whose values give the language name and path :

    $locales  =  array(
        'en'=>  array('name' => 'English', 'path' => 'en_EN'), // English is located in /languages/en_EN/
        'fr' => array('name' => 'Français', 'path' => 'fr_FR'), // French is located in /languages/fr_FR/
        'de' => array('name' => 'Deutsch', 'path' => 'de_custom'), // German is located in /languages/de_custom/
    );


## Translations

Regular (UI) translations can be expressed in several ways but always under the specific language path :

  * in a `lang.php` file as a `$lang` array entry whose key is the translation identifier and whose value is the actual translation
  * in individual files with names like (no differences between extensions, last found wins, `lang.php` will be skipped) :
    * `translation_identifier.php`
    * `translation_identifier.txt`
    * `translation_identifier.text`
    * `translation_identifier.htm`
    * `translation_identifier.html`
    * `translation_identifier.txt.php`
    * `translation_identifier.text.php`
    * `translation_identifier.htm.php`
    * `translation_identifier.html.php`

Individual files are a bit more resource expensive but usefull to write big chunks of text.


## Email translations

Email can be translated in `translation_identifier.mail` or `translation_identifier.mail.php` files (last one wins).

Lines before first double line feed will be considered as headers.

You can define `alternatives` by using `{alternative:plain}` and `{alternative:html}` syntaxes.

Example :

    // hello.mail.php
    subject: hello !
    
    {alternative:plain}
    
    Hello !
    
    {alternative:html}
    
    <h1>Hello !</h1>


## Translation variables

A translation may be context dependent (plurar forms, figures), the translation engine includes a simple language for variable rendering, tests and iteration :

### Variable rendering

`{variable_name}` renders the variable content (scalar).

`variable_name` can be a `.` (dot) separated path to access sub-keys or public properties, keywords `first`, `last` and `nth(N)` may be used to access numerically indexed array values.

Converters are also available :
    * `{date:variable_name}` render internal date to localized date
    * `{datetime:variable_name}` render internal date to localized date with time
    * `{time:variable_name}` render internal time to localized time
    * `{size:variable_name}` render as size in bytes
    * `{auth_url:variable_name}` render as authentication triggering URL
    * `{raw:variable_name}` do not convert output to get scalar (may be combined with any of the above)

If final value after path resolution is not scalar following transformations will apply (unless the `raw` converter was used) :

  * `array`s will be transformed to their entries count
  * `object`s will be transformed to `boolean true`

Example :

    echo Lang::tr('foo', array($foo_instance));
    
    $lang['foo'] = 'Foo ({size:foo.size})';


### Conditionnals

`{if: test}...{else}...{endif}` conditions rendering, `{else}` part is optional, variable rendering inside statements allowed, nesting not supported.

The `test` can use `|` (`or`) and `&` (`and`) logical operators, `and` takes precedence, parenthesing not supported.

The condition(s) can use regular comparison operators (`==`, `!=`, `<`, `<=`, `>`, and `>=`) on [variable rendering like syntaxes](#variable_rendering) :

    // file_list being an array
    {file_list} {if:file_list>1}files{else}file{endif}


### Loops

`{each: variable_name}...{endeach}` allows to iterate over arrays / collections, inner part may contain [conditionnals](#conditionnals) but no other loops.

By default the current item will be accessible under the name `item`, if you'd like to use another name use the `{each: variable_name as foo}` syntax.

Example :

    {each: foo_array as foo}
        {foo.name} ({size:foo.size})
    {endeach}


## Translation lookup

The translation lookup is a bit complex, the translations are loaded in this order :

  * look into `/config/languages/` if it exists
  * look into `/languages/`
  * look into `/languages/core/`

For the main language then for any remaining available and user accepted language.

So when looking to translate `foo` if your user language is `fr` and application default is `en` the lookup order will be :

    /config/languages/$locales['fr']['path']/foo.*
    /config/languages/$locales['fr']['path']/lang.php
    /languages/$locales['fr']['path']/foo.*
    /languages/$locales['fr']['path']/lang.php
    /languages/core/$locales['fr']['path']/foo.*
    /languages/core/$locales['fr']['path']/lang.php
    /config/languages/$locales['en']['path']/foo.*
    /config/languages/$locales['en']['path']/lang.php
    /languages/$locales['en']['path']/foo.*
    /languages/$locales['en']['path']/lang.php
    /languages/core/$locales['en']['path']/foo.*
    /languages/core/$locales['en']['path']/lang.php


## Using the translation engine

You can use the `{tr:translation_identifier}` to get translations in templates.

If you need to pass variables to the translation engine you must do it like so :

    echo Lang::translate('translation_identifier')->replace($associative_array_of_variables, $object, ...);
    // shorter version
    echo Lang::tr('translation_identifier')->r($associative_array_of_variables, $object, ...);

Any object passed as a single argument of the `replace` method will be accessible under its lowercased class name.
