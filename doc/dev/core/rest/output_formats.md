# REST response formats

Requestor can request an output format by :

  * using the [`format` query argument](../rest.md#parsed_reserved_request_arguments) query argument, the value specification is `format = ["download:"] type [":" any_char]` where :
    * the `download:` part requests a file download
    * the `type` is the wanted output format (`json`, `xml` ...)
    * the `any_char` depends on the requested format
  * adding a file extension at the end of the resource path (like `/user.xml?fields=id,name`), in this case download triggering and format options are not supported


## Build-in formats

### JSON

Convert data to JSON.

Format argument :

  * `/foo?format=json`
  * `/foo?format=download:json`
  * `/foo.json`

Expects structured data.

Output example :

    {
      "id": "foo@bar.tld",
      "attributes": {
        "name": "Foo",
        "emails": ["foo@bar.tld"]
      },
      "created": {
        "raw": 1464266776,
        "formatted": "05/26/2016"
      },
      "last_activity": null,
      "lang": null
    }

Applies `fields`.


### XML

Convert data to XML.

Format argument :

  * `/foo?format=xml`
  * `/foo?format=download:xml`
  * `/foo.xml`

Expects structured data.

Returns hierarchical data, possible `type` attributes for nodes :

  * `object` : all sub-nodes are properties based on their names (converted to `stdClass` on the request side)
  * `array` : has `<item />` as array entries, `index` integer attribute specifies order
  * `boolean` : node content will be casted as a boolean
  * `integer` : node content will be casted as an integer
  * `float` : node content will be casted as a float
  * `string` : node content will be casted as a string, `CDATA` sections will be decoded
  * `null` : null value

Output example :

    <data type="object">
      <id type="string">foo@bar.tld</id>
      <attributes type="object">
        <name type="string">Foo</name>
        <emails type="array">
          <item type="string" index="0">foo@bar.tld</item>
        </emails>
      </attributes>
      <created type="object">
        <raw type="integer">1464266776</raw>
        <formatted type="string">05/26/2016</formatted>
      </created>
      <last_activity type="null"/>
      <lang type="null"/>
    </data>

Applies `fields`.


### CSV

Convert data to CSV, flattens sub-structures.

Format argument :

  * `/foo?format=csv`
  * `/foo?format=csv:;`
  * `/foo?format=download:csv`
  * `/foo?format=download:csv:;`
  * `/foo.csv`

Expects structured data.

Value of `format_options` is the field separator (single char), default is `,` (comma).

Output example :

    id,attributes.name,attributes.emails.0,created.raw,created.formatted,last_activity,lang
    foo@bar.tld,Foo,foo@bar.tld,1464266776,05/26/2016,"",""

Applies `fields`.


### Pdf

Convert html or native data to a [pdf document](https://en.wikipedia.org/wiki/Portable_Document_Format).

Format argument :

  * `/foo?format=pdf`
  * `/foo?format=pdf:json`
  * `/foo?format=download:pdf`
  * `/foo?format=download:pdf:json`
  * `/foo.pdf`

Expects either an html fragment as string or native data (to generate a data dump).

In case of a data dump value of `format_options` can be `json` to get a JSON style dump instead of a table.

Outputs a PDF document.


### Plain

Returns plain text.

Format argument :

  * `/foo?format=plain`
  * `/foo?format=txt`
  * `/foo?format=download:plain`
  * `/foo?format=download:txt`
  * `/foo.plain`
  * `/foo.txt`

Expects string.


### Html

Returns raw html.

Format argument :

  * `/foo?format=html`
  * `/foo?format=htm`
  * `/foo?format=download:html`
  * `/foo?format=download:htm`
  * `/foo.html`
  * `/foo.htm`

Expects string.


### Png

Returns a [PNG image](https://en.wikipedia.org/wiki/Portable_Network_Graphics).

Format argument :

  * `/foo?format=png`
  * `/foo?format=download:png`
  * `/foo.png`

Expects [GD resource](http://php.net/manual/fr/function.imagecreate.php).


### Jpeg

Returns a [JPEG image](https://en.wikipedia.org/wiki/JPEG).

Format argument :

  * `/foo?format=jpeg`
  * `/foo?format=jpg`
  * `/foo?format=download:jpeg`
  * `/foo?format=download:jpg`
  * `/foo.jpeg`
  * `/foo.jpg`

Expects [GD resource](http://php.net/manual/fr/function.imagecreate.php).


### Gif

Returns a [GIF image](https://en.wikipedia.org/wiki/GIF).

Format argument :

  * `/foo?format=gif`
  * `/foo?format=download:gif`
  * `/foo.gif`

Expects [GD resource](http://php.net/manual/fr/function.imagecreate.php).


### Raw

Returns binary data.

Format argument :

  * `/foo?format=raw`
  * `/foo?format=download:raw`
  * `/foo.raw`

Expects binary data as string.

This format is mainly intended to be a base for other formats.


### Buffered raw

Returns binary data in a chunked manner to ease up big downloads.

Format argument :

  * `/foo?format=bufferedRaw`
  * `/foo?format=download:bufferedRaw`

Expects a callable data chunk getter and an optionnal result size.

This format is mainly intended to be a base for other formats.
