# JSONP requests

[JSONP requests](https://en.wikipedia.org/wiki/JSONP) is used to run cross-domain requests.

Since it potentially, depending on how secured your endpoints are, introduce a security breach, it is not enabled by default.

It can be enabled by writing a `isJSONPAllowed` static method in you endpoint which will receive the method and resource path as arguments and has to return a boolean telling wether the requestor is allowed to call your endpoint using JSONP.

Additionnaly the `rest.jsonp_rules` config parameter can list a set of matcher and corresponding verdicts. The matchers are applied on the full resource path (including endpoint) and can target a specific method. Last matching rule wins :

    $config['rest']['jsonp_rules'] = array(
        'post ^/foo' => false,      // Forbid posts to /foo or any sub-resources
        '^/foo/[0-9]+$' => true,    // Allows any method on /foo/some_number (overrides previous rule) but not on sub-resources
        '^/foo' => false            // Forbids any other kind of request on /foo
    );
