# REST response fields selection

The [`field` query argument](../rest.md#parsed_reserved_request_arguments) allows a requestor to ask for only a specific subset of fields to be present in the response.

The given value is a comma separated list of field names, the names can include dots to specify sub-data fields :

    fields=id,name,creation_date.raw

The structured output formats (mostly `json`, `xml` and `csv`) are automatically filtered should they return a single entity or an entity collection, your endpoint can set mandatory returned fields by giving the `RestResponse::addMandatoryFields` method a list of field names :

    RestResponse::addMandatoryFields(array('id', 'foo.bar'));

The other formats field exclusion must be handled by your endpoint, the list of requested fields can be obtainer through a call to `RestResponse::getMandatoryFields()`.
