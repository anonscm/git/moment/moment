# Output filtering

The ApplicationBase provides a standard way to express and apply output filters on entity collections.

The filter is given through the `filter` query argument.


## Filter expression

The filter is expressed as a JSON encoded tree and may contain test expressions combined using logical operators.

Example :

    filter={"and":[{"contains":{"name":"foo"}},{"dateAfter":{"created":1464351563}}]}


Each filter level is an object that has an operator (logical or test) and related operands :

    filter = "{" operator ":" operands "}"

Depending on the operator the operands can be :

  * a test's arguments : `operand = test_arguments`
  * a list of sub-filters : `operand = "[" filter *("," filter) "]"`
  * a single sub-filter : `operand = filter`


### Test operators

The `day` and `time` types are both [PHP's strtotime function compatible string](http://php.net/manual/fr/function.strtotime.php) or [unix timestamp](https://en.wikipedia.org/wiki/Unix_time).

`day` type will ignore time component.


| Type                                               | Operator          | Operands                                   | Example                                                                                        |
|----------------------------------------------------|-------------------|--------------------------------------------|------------------------------------------------------------------------------------------------|
| Non-empty property                                 | `present`         | `"{" field ":" bool "}"`                   | `filter={"present":{"name":true}}}` |
| Substring match                                    | `contains`        | `"{" field ":" string "}"`                 | `filter={"contains":{"name":"foo"}}}` |
| String start match                                 | `startWith`       | `"{" field ":" string "}"`                 | `filter={"startWith":{"name":"foo"}}}` |
| String end match                                   | `endWith`         | `"{" field ":" string "}"`                 | `filter={"endWith":{"name":"foo"}}}` |
| Value equality                                     | `equals`          | `"{" field ":" string "}"`                 | `filter={"equals":{"name":"foo"}}}` |
| Value in set                                       | `in`              | `"{" field ":[" string *("," string) "]}"` | `filter={"in":{"name":["foo","bar"]}}}` |
| Regexp match                                       | `matches`         | `"{" field ":" string "}"`                 | `filter={"matches":{"name":"^foo.+"}}}` |
| Case sensitive regexp match                        | `caseMatches`     | `"{" field ":" string "}"`                 | `filter={"caseMatches":{"name":"^Foo.+"}}}` |
| Number less than                                   | `lessThan`        | `"{" field ":" number "}"`                 | `filter={"lessThan":{"age":42}}}` |
| Number more than                                   | `moreThan`        | `"{" field ":" number "}"`                 | `filter={"moreThan":{"age":42}}}` |
| Date equal to `day`                                | `dateEquals`      | `"{" field ":" day "}"`                    | `filter={"dateEquals":{"created":1464362696}}}` |
| Date before `day`                                  | `dateBefore`      | `"{" field ":" day "}"`                    | `filter={"dateBefore":{"created":1464362696}}}` |
| Date after `day`                                   | `dateAfter`       | `"{" field ":" day "}"`                    | `filter={"dateAfter":{"created":1464362696}}}` |
| Date between `day` boundaries (included)           | `dateBetween`     | `"{" field ":[" day "," day "]}"`          | `filter={"dateBetween":{"created":[1464362696,1464367569]}}}` |
| Date and time equal to `time`                      | `dateTimeEquals`  | `"{" field ":" time "}"`                   | `filter={"dateTimeEquals":{"created":1464362696}}}` |
| Date and time before `time`                        | `dateTimeBefore`  | `"{" field ":" time "}"`                   | `filter={"dateTimeBefore":{"created":1464362696}}}` |
| Date and time after `time`                         | `dateTimeAfter`   | `"{" field ":" time "}"`                   | `filter={"dateTimeAfter":{"created":1464362696}}}` |
| Date and time between `time` boundaries (included) | `dateTimeBetween` | `"{" field ":[" time "," time "]}"`        | `filter={"dateTimeBetween":{"created":[1464362696,1464367569]}}}` |


### Logical operators

| Type                 | Operator                   | Operands         | Example                                                                                        |
|----------------------|----------------------------|------------------|------------------------------------------------------------------------------------------------|
| Negation             | `not`                      | `filter`         | `filter={"not":{"contains":{"name":"foo"}}}`                                                   |
| And                  | `and`                      | list of `filter` | `filter={"and":[{"contains":{"name":"foo"}},{"dateAfter":{"created":1464351563}}]}`            |
| Negated and          | `notAnd` or `nand`         | list of `filter` | `filter={"notAnd":[{"contains":{"name":"foo"}},{"dateAfter":{"created":1464351563}}]}`         |
| Or                   | `or`                       | list of `filter` | `filter={"or":[{"contains":{"name":"foo"}},{"dateAfter":{"created":1464351563}}]}`             |
| Negated or           | `notOr` or `nor`           | list of `filter` | `filter={"notOr":[{"contains":{"name":"foo"}},{"dateAfter":{"created":1464351563}}]}`          |
| Exclusive or         | `exclusiveOr` or `xor`     | list of `filter` | `filter={"exclusiveOr":[{"contains":{"name":"foo"}},{"dateAfter":{"created":1464351563}}]}`    |
| Negated exclusive or | `notExclusiveOr` or `nxor` | list of `filter` | `filter={"notExclusiveOr":[{"contains":{"name":"foo"}},{"dateAfter":{"created":1464351563}}]}` |


### Custom tests

You can add custom tests from any context in which a [`RestFilter`](@php/RestFilter) may be used by calling the `RestFilter::addCustomTest` method.

A custom test will be accessible like any other test at the exception that its `test_arguments` may be anything (that is maybe not even related to a specific entity field) :

    filter={"foo":42}
    filter={"foo":["bar","barbar",42]}
    filter={"foo":{"bar":true,"barbar":42}}

This method takes either a name and a callable or an array of tests named by their keys :

    RestFilter::addCustomTest('foo', function($arguments, $entity) {
        // $arguments is the parsed arguments
        // $entity is the model entity to be checked for match
        
        return (time() - $entity->created) > ($argument * 24 * 3600);
    });
    
    // Adding a set of tests
    RestFilter::addCustomTest(array(
        'foo' => function($arguments, $entity) { ... },
        'bar' => function($arguments, $entity) { ... }
    ));


## Applying filters

### Using provided entity getter

You can simply load an entity collection already filtered (also sorted, offseted and count-limited) by using the provided `RestUtilities::getEntities` utility :

    $entities = RestUtilities::getEntities('entity_name', array( allowed filtering / sorting fields ));

The first parameter can be :

  * an entity name, the `all` method of the related class will be used to fetch entities
  * a string like `entity_name::fetch_method` to use an alternative method
  * an instance of [`EntityGetter`](@php/EntityGetter) whose `callable` will receive pre-filtering criteria and placeholders
  * an array of already fetched entities

The second parameter may contain the list of fields the request can be filtered and sorted onto. It is an array of field names or virtual field name and related getter pairs :

    array(
        // Normal fields
        'id', 'name', 'foo',
        
        // Virtual field
        'bar' => function($entity) {
            return $entity->name.'>'.$entity->foo;
        }
    )

All non-virtual fields known to the [datamap](../model.md#datamap) will be set for pre-filtering, except if an already fetched entity set is provided.

Finally the following [request arguments](../rest.md#parsed_reserved_request_arguments) will be applied :

  * `order`
  * `sortOrder` : `compareWith` method must exist on entities
  * `count`
  * `startIndex`
  * `updatedSince` : `wasUpdatedSince` method must exist on entities


#### Pre-filtering

Upon fetching entities any test that can be applied at the database level will be set for pre-filtering, this allows to reduce the amount of data to process.

The ability for a filter fragment to be used for pre-filtering depends on its nature and combination, not all filters can be applied at database level :

  * `nxor`
  * `in`
  * `matches`
  * `caseMatches`
  * all tests based on virtual fields
  * all [custom tests](#custom_tests)


#### Post-filtering

Any remaining filters will be applied after entity fetching.


### Apply filter by hand

To filter an `array` of entities with a request provided filter you must do it like :

    $filter = RestRequest::getOutputProperty('filter'); // Get parsed filter
    $filter->addPostfilteringField(array( allowed filtering fields, see previous section ));
    $filtered_entities = array_filter($entities, function($entity) use($filter) {
        return $filter->matches($entity);
    });

If filtering by hand only post-filtering can be applied.
