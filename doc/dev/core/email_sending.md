# Sending emails

The ApplicationBase is provided with a simple way to send translated, contextual emails.

## Translating emails

See [email translations](./translating.md#email_translations).


## Sending emails to users / anybody

Here is how to quickly send emails to somebody :

    ApplicationMail::quickSend('email_translation_identifier', $recipient, $context, $other_variables);

`$recipient` can be :

  * A `User` instance
  * An object with at least a public `email` property and optional public `name` and/or `lang` properties
  * An array with at least `email` key and optional `name` and/or `lang` keys
  * An email address as a string

`$context` should be the main [model entity](./model.md), it will be used to se a mail header telling which context the email was sent under. This header may help the [email bounce handling](#bounce_handling) mecanism to find who to notice the bounce to.

`$other_variables`, if provided, will be passed, along with `$context`, to the translation engine.


With attachments :

    $email = new ApplicationMail('foo_report', $rcpt, $foo);
    
    $attachment = new MailAttachment('report.log');
    $attachment->path = EKKO_ROOT.'/tmp/report.log';
    
    $email->attach($attachment);
    
    $email->send();


## Sending emails to admins

Here is how to quickly send emails to [configuration defined admins](./config.md#admin_email) :

    SystemMail::quickSend('email_translation_identifier');

If variable replacement is needed :

    SystemMail::quickSend(Lang::translateEmail('email_translation_identifier')->replace($variables));

And with attachments :

    $email = new SystemMail(Lang::translateEmail('email_translation_identifier')->replace($variables));
    
    $attachment = new MailAttachment('report.log');
    $attachment->path = EKKO_ROOT.'/tmp/report.log';
    
    $email->attach($attachment);
    
    $email->send();


## Bounce handling

TODO


## Instant translation

You can send emails that the recipient will be able to translate if he/she doesn't understand the email language :

    $email = TranslatableEmail::prepare('email_translation_identifier', $recipient, $context, $other_variables);
    
    // Attachments if needed
        
    $email->send();

This way the email will be sent with a footer (defined by `/languages/core/en_EN/translate_email_footer.mail.php`) that gives an online translation link.
