# Documentation

The application base allows to automatically generate most of your project's documentation.

It also allows to upload generated html documentation to a [WebDav](https://en.wikipedia.org/wiki/WebDAV) server (such as [SourceSup](https://sourcesup.renater.fr)'s) in a simple way.

## Get apidoc.json data


At any moment, you can use some tags to access the <code>apidoc.json</code> data using the <code>@api:</code> tag.

Examples :

    @api:url -> get the API URL  
    @api:title -> get the API title


## Developper documentation

You are expected to write your developper documentation in the `doc/dev/` folder using the [Markdown extra syntax](https://michelf.ca/projects/php-markdown/extra/), to be parsed your files MUST have the `.md` extension.

The directory structure is up to you, the main index will be generated automatically so don't create `doc/dev/index.md`.

You can use anchors in your links to redirect to specific titles, to get the anchor string apply these steps :

  * Take the title
  * Lower case it
  * Replace everything that is not a letter, digit, underscore or dash by an underscore
  * Deduplicate and trim underscores

Example :

    ## My nice title !!!
    My nice title !!!
    my nice title !!!
    my_nice_title____
    my_nice_title
    [some text](my_page.md#my_nice_title)


## REST API documentation

The REST API documentation is generated using [apidoc](http://apidocjs.com/), in order for your REST API to be documented you MUST use [apidoc](http://apidocjs.com/) specific comments in any of these locations :

  * `.class.php` endpoint files under `classes/endpoints/`
  * `.doc.php` files under `doc/api/`


## PHP API documentation

The PHP API documentation is generated using [phpDocumentor](https://www.phpdoc.org/), you should use it's syntax when documenting your code.


## Cross-linking

You can refer to a page / anchor of any type of documentation from any other by using one of the following syntaxes :

    [some text](ref)
    [some text](ref#anchor)
    <a href="ref">some text</a>
    <a href="ref#anchor">some text</a>

Where `ref` can be :

  * `@api/group` for a REST API call group
  * `@api/group/call` for a specific REST API call
  * `@dev/path/to/page.md` for a developper documentation reference
  * `@php/FooBar` for a PHP API class reference
  * `@php/path/to/file.php` for a PHP API file reference
  * `@wiki/path/to/page` to refer to a page of your [SourceSup](https://sourcesup.renater.fr)'s project wiki


## Generating

Generating the documentation is done through a call to the `doc/tools/generate.php` script, possible options are :

  * -v : verbose mode
  * -q : quiet mode
  * -p | --path : set target directory (will be created unless it exists)
  * -d | --dav : settings file for webdav push, see [WebDav upload](#webdav_upload)
  * -t | --type : type of documentation to be generated (comma separated set of "api", "php", "dev" or "all"), defaults to "all"
  * --phpdoc : path to phpdoc binary if `phpdoc` is not in your path
  * --apidoc : path to apidoc binary if `apidoc` is not in your path


## Customization

The output of the documentation generator can be altered to fit your needs through several customization files :

### `doc/tools/generate/api/apidoc.json`

Custom apidoc configuration, see [apidoc.json](http://apidocjs.com/#configuration)


### `doc/tools/generate/api/template/`

Custom template for apidoc, see [apidoc templating](http://apidocjs.com/#template)


### `doc/tools/generate/php/args.php`

Additional arguments for phpdoc, see [running phpDocumentor](https://www.phpdoc.org/docs/latest/guides/running-phpdocumentor.html)

Must contain an $args array with switch and value pairs (switches must include hyphens).


### `doc/tools/generate/dev/index.html`

Html index file for the developper documentation.

The `body` tag MUST have a `data-root` attribute set to `./`


### `doc/tools/generate/dev/template.html`

Html page template file for the developper documentation.

The `body` tag MUST have a `data-root` attribute set to `{root}` and a `data-id` attribute set to `{id}`.

`{content}` will be replaced with the parsed page content.


### `doc/tools/generate/index.html`

Html index file for the documentation root


## WebDav upload

In order to automatically upload the generated documentation through [WebDav](https://en.wikipedia.org/wiki/WebDAV) you must create a PHP settings file.

This file must contain a `$dav` array, it will be passed to the [WebDav client](@php/WebDavClient).

Settings example using [SourceSup](https://sourcesup.renater.fr)'s project built-in [WebDav](https://en.wikipedia.org/wiki/WebDAV) server :

    <?php
    
    $dav = array(
        'base_url' => 'https://sourcesup.renater.fr/my_project',
        'username' => 'foo',
        'password' => 'bar',
        'auth_type' => WebDavClient::AUTH_BASIC,
    );

Other settings related to authentification and proxies are available, see [WebDav client API](@php/WebDavClient).
