# Configuration

## The configuration system

### Configuration loading logic

Configuration is loaded in this order :

  * `/includes/core/ConfigDefaults.php` : core defaults
  * `/includes/ConfigDefaults.php` : your application defaults
  * `/config/config.php` : instance config

After loading the configuration is validated using rules expressed in the following files :

  * `/includes/core/ConfigValidation.php` : core parameters validation
  * `/includes/ConfigValidation.php` : your application configuration validation rules


### Configuration expressions

Configuration is written as a [PHP array](http://php.net/manual/en/language.types.array.php) whose name is `$config`.

Values can be [scalars](http://php.net/manual/en/language.types.intro.php) or [callables](http://php.net/manual/en/language.types.callable.php).

In case the value is a [callable](http://php.net/manual/en/language.types.callable.php) it will be executed upon first use and the returned result will be cached, this allows to have dynamically defined parameters (user / environment dependent) :

    // Only allow users from a specific external group
    $config['auth_user_filter'] = function() {
        $user_groups = Sympa::getGroupsOf(Auth::getAttributes()['email']); // Auth::user() not available at this point
        
        return !in_array('allowed_users@sympa.server.tld', $user_groups); // Do not filter if user in allowed group
    };


## Configuration parameters

### Global

#### admin

`string` or `array of string`, mandatory, no default.

The [UID(s)](./authentication.md) of the application admin(s).


#### admin_email

`string` or `array of string`, mandatory, no default.

The emails to which to send the system notifications to.


#### application_gid

`integer`, optional.

Group the files created by the application should belong to. MUST be an UNIX group identifier.


#### application_logouturl

`string`, defaults to the application root.

URL to go to after logout.


#### application_name

`string`, mandatory, no default.

Name of the application, displayed in the web UI, included in the sent emails subjects ...


#### application_uid

`integer`, mandatory, no default.

User the files created by the application should belong to. MUST be an UNIX user identifier.


#### application_url

`string`, mandatory, no default.

Base URL of the application.


#### debug

`boolean`, defaults to `false`.

Setting `debug` to `true` turns off [CSS and Javascript web UI resources bundling](./ui.md#resources_bundling) and turns on database queries logging (very verbose).


#### feedback_url

`string`, no default.

Email address to give to users for feedback enquiries. If not set feedback link is not displayed.


#### landing_page

`string`, defaults to the root page.

Page to go to after authentication if no target page was specified.


#### maintenance

`boolean`, defaults to `false`.

If set to true the web UI will present the [maintenance](./ui.md#default_templates) page and the webservice will return a specific message.


#### max_cookie_duration

`integer`, no default.

Duration, in days, the application cookies should have.


#### session_cookie_path

`string`, defaults to the application path.

Path of the session cookie.


#### support_email

`string`, no default.

Email address to give to users for support enquiries. If not set support contact is not displayed.


#### timezone

`string`, defaults to `Europe/Paris`.

Timezone the application operates in, [Full list here](http://php.net/manual/fr/timezones.php).



### Database related

#### db

`array` with keys :

  * Connection :
    * Using DSN :
      * `dsn` : [database DSN](https://en.wikipedia.org/wiki/Data_source_name), `string`, mandatory, no default, currently only the `mysql` and `posgresql` drivers are supported
    * Specifing as separated parameters :
      * `type` : database type, `string`, mandatory, no default, currently only `mysql` and `posgresql` are supported
      * `host` : database host, `string`, mandatory, no default
      * `port` : database port, `string`, defaults to `3306`
      * `database` : database name, `string`, mandatory, no default
  * Authentication :
    * `username` : user name, `string`, optional
    * `password` : user password, `string`, optional
  * Data exchange :
    * `charset` : charset to use, `string`, optional, no default
    * `collation` : collation to use, `string`, optional, no default
  * Options :
    * `driver_options` : [driver options](http://php.net/manual/fr/pdo.construct.php), `array`, optional
    * `table_prefix` : prefix for the application's database tables, `string`, optional
    * `timeout` : time in second after which the connexion should be pinged because there is a risk of it being in timeout, `integer`, default to `900`

If no [`db_admin` parameter](#db_admin) is defined the database user MUST have `SELECT, INSERT, UPDATE, DELETE, CREATE, ALTER, DROP` privileges.

Examples :

    $config['db'] = array(
        'dsn' => 'mysql://foo:s3cr3t@bar.tld/my_app',
        'charset' => 'utf8'
    );
    
    // is the same as
    
    $config['db'] = array(
        'type' => 'mysql',
        'host' => 'bar.tld',
        'database' => 'my_app',
        'username' => 'foo',
        'password' => 's3cr3t',
        'charset' => 'utf8'
    );


#### db_admin

Allows to define dedicated database user for administrative tasks (structure update and such).

`array` with keys :

  * `username` : user name, `string`
  * `password` : user password, `string`

The related database user MUST have `SELECT, INSERT, UPDATE, DELETE, CREATE, ALTER, DROP` privileges.

If defined then privileges of the user defined by [the `db` parameter](#db) can be restricted to `SELECT, INSERT, UPDATE, DELETE`.


#### remote_db

Allows to define remote databases.

`array` with remote databases identifiers as keys and [`db` config parameter like](#db) as values.

Example :

    $config['remote_db'] = array(
        'remote_database_identifier' => array(
            'dsn' => 'posgresql://foo:s3cr3t@192.168.0.227/service'
        )
    );

Remote databases can be queried by using :

    DBI::remote('remote_database_identifier')->pdo_method



### Authentication related

#### auth_remote.application.enabled

Enable [remote application authentication](./authentication/remote.md#remote_application).

`boolean`, defaults to `false`.


#### auth_remote.application.applications

List of known applications for the [remote application authentication](./authentication/remote.md#remote_application).

See [registering a remote application](./authentication/remote.md#registering_a_remote_application).


#### auth_remote.timeout

Timeout for [remote authentication](./authentication/remote.md) requests.

`integer` in seconds, defaults to 15s.


#### auth_remote.user.autogenerate_secret

Sets the application have to automatically generate a random secret for the [remote user authentication](./authentication/remote.md#remote_user) for each user.

`boolean`, defaults to `true`.


#### auth_remote.user.enabled

Enable [remote user authentication](./authentication/remote.md#remote_user).

`boolean`, defaults to `false`.


#### auth_remote.user.sync_request_timeout

Timeout for [remote user authentication synchronization](./authentication/remote.md#getting_the_user_s_remote_access_credentials) requests.

`integer` in seconds, defaults to 60s.


#### auth_remote.signature_algorithm

Signature algorithm for [remote authentication](./authentication/remote.md) requests.

`string` in seconds, defaults to `sha1`.

See [PHP's `hash_hmac` function](http://php.net/manual/fr/function.hash-hmac.php).

To get the list of available algorithms on your server you can run `php -r 'print_r(hash_algos());'` from it's command line.


#### auth_sp

Contains the [federated authentication](./authentication/federated.md) configuration.

`array` with `type` entry and other entries related to SPs specifics.

Default is :

    $config['auth_sp'] = array(
        'type' => 'shibboleth',  // Authentification type
        'uid_attribute' => 'eduPersonTargetId', // Get uid attribute from authentification service
        'email_attribute' => 'mail', // Get email attribute from authentification service
        'name_attribute' => 'cn', // Get name attribute from authentification service
        
        'save_additional_attributes' => true
    );


#### auth_sp.additional_attributes

Additionnal attributes to get from the SP.

`array` of either attribute names or individual `callable` attribute getters, if a non-numerical key is used it will override the attribute name.

In the case of the [fake SP authentication](./authentication/federated.md#fake_authentication) it contains name-values pairs of additionnal user attributes.


#### auth_sp.save_user_additional_attributes

Sets the application to automatically save the user's additionnal attributes in his/her profile.

`boolean`, defaults to `true`.


#### auth_user_filter

Filtering rule for authenticated users.

If `boolean` and `true` all users will be denied access.

If `string` it MUST be in the format `attribute:regexp`, `attribute` designs the user attribute to match against (`uid`, `email` or `name`), `regexp` will be evaluated using [PHP's `preg_match` function](http://php.net/manual/fr/function.preg-match.php).


### Web UI

#### css_libraries

List of additionnal CSS files to load.

`array` of paths relative to `/view`, MUST be under `/view` otherwise they won't be loaded in [debug mode](./debug.md).


#### js_libraries

List of additionnal Javascript files to load.

`array` of paths relative to `/view`, MUST be under `/view` otherwise they won't be loaded in [debug mode](./debug.md).



### Email sending

#### email.from

Sent email `From` header.

Can be a `string` containing an email address.

Can be an `array` with `email` and `name` properties.

Can be a `string` equal to `sender`, see [email sending](./email_sending.md).

Can be array of the above to search for sender if any else use default :

    $config['email']['from'] = array('sender', 'foo@bar.tld');

If not set or set to falsish value no `From` header will be sent.


#### email.newline

New line style for sent emails.

`string`, defaults to `\r\n`.


#### email.reply_to

Sent email `Reply-To` header.

Can be a `string` containing an email address.

Can be an `array` with `email` and `name` properties.

Can be a `string` equal to `sender`, see [email sending](./email_sending.md).

Can be array of the above to search for sender if any else use default :

    $config['email']['reply_to'] = array('sender', 'foo@bar.tld');

If not set or set to falsish value no `Reply-To` header will be sent.


#### email.return_path

Sent email `Return-Path` header.

Can be a `string` containing an email address.

Can be a `string` equal to `sender`, see [email sending](./email_sending.md).

Can be array of the above to search for sender if any else use default :

    $config['email']['return_path'] = array('sender', 'foo@bar.tld');

If not set or set to falsish value no `Return-Path` header will be sent.


#### email.subject_prefix

Prefix to add to sent emails subjects.

May contain `{cfg:XXX}` like syntax to use configuration parameters.

`string`, defaults to `[{cfg:application_name}]`.


#### email.use_html

Send multipart emails with HTML part.

`boolean`, defaults to `true`.


### Translation related

#### lang.default

Default language to use when translating.

Possible values are defined in [the locale file](./translating.md#locales).

`string`, defaults to `fr`.


#### lang.save_user_pref

Save user selected language in his/her profile for later (like cron email sending).

`boolean`, defaults to `true`.


#### lang.selector_enabled

Display language selector in the web interface.

Needs `lang.use_url` to be set to `true`.

`boolean`, defaults to `true`.


#### lang.use_browser

Use the `Accept-Language` header if provided by the browser to discover the user's language.

`boolean`, defaults to `true`.


#### lang.use_url

Use the `lang` URL argument if provided to switch language.

Language switch is saved in the current session, if `lang.save_user_pref` is set to `true` and there is a logged-in user the change will be saved in his/her profile.

`boolean`, defaults to `true`.


#### lang.use_user_pref

If there is a logged-in user use his/her `lang` property, if set, as language.

The `lang` user property may be set on language switch if `lang.use_url` and `lang.save_user_pref` are set to `true`.

`boolean`, defaults to `true`.


### Logging related

#### log_facilities

See [logging](./logging.md).

`array`, defaults to :

    $config['log_facilities'] = array(
        array(
            'type' => 'file',
            'path' => EKKO_ROOT.'/logs/',
            'rotate' => 'daily'
        )
    );


### Misc

#### user_active_days

Number of days during which a user with no activity is still considered to be active.

`integer`, defaults to `30`.


#### user_inactive_days

Number of days after which a user with no activity is considered to be definitely inactive and will be remove if a `User::removeInactive` procedure is started.

If set to a falsish value no cleaning will be done.

`integer`, defaults to `false`.
