# Debug

## Server side 

The ApplicationBase includes a `debug` [logging mode](./logging.md).

You can set your logging facilities to accept it using the following configuration :

    $config['logging_facilities'] = array(
        array(
            'type' => 'file',
            'level' => 'debug',
            'rotate' => 'hourly',
            'path' => EKKO_ROOT.'/logs/debug'
        )
    );

This mode is very verbose (all database queries gets logged) so only turn it on when needed !


## Client side

The ApplicationBase provides a JS/CSS bundling utility that makes it harder to use standard browser debugging tools, to turn it off you have to set the [`debug` configuration parameter](./config.md#debug) to `true`.

This way all Javascript and CSS files will be separated (resource consuming).
