# Writting custom REST clients

Providing a dedicated REST client for your application can really ease-up the integration process.

To facilitate client creation the ApplicationBase provides :

  * A basic client supporting standard HTTP verbs, body types and header handling
  * Authentication facilities for [remote authentication](./auth/remote.md)
  * A tool to bundle your custom client into a [PHAR archive](http://php.net/manual/en/intro.phar.php) for easy distribution


## Client creation

You can create your onw client with specialized methods by extending the [ApplicationRestClient class](@php/ApplicationRestClient) like so :

    // Start of MANDATORY part, do not alter !
    
    if(defined('EKKO_ROOT'))
        define('EKKO_CLIENT_ROOT', EKKO_ROOT.'/lib/client');
    
    // @dependency DO NOT ALTER
    require_once EKKO_CLIENT_ROOT.'/core/ApplicationRestClient.class.php';
    
    // End of MANDATORY part, do not alter !
    
    class MyCLient extends ApplicationRestClient {
        public function getUserFoo() {
            return $this->get('/user/@me/foo');
        }
    }


## Distribution

You can distribute your client as a set of files (do not forget to include files from `lib/client/core/`).

An easier way is to bundle your client as a [PHAR archive](http://php.net/manual/en/intro.phar.php), this way you will end-up having a single `.phar` file to give to other developpers.

To generate a [PHAR archive](http://php.net/manual/en/intro.phar.php) just call the provided tool with the name of the client you want to bundle :

    php lib/client/core/phar_builder.php MyClient

or

    php lib/client/core/phar_builder.php MyClient.class.php

After the build is over you will find the created [PHAR archive](http://php.net/manual/en/intro.phar.php) under `lib/client/phar/` with the same name as your client :

    lib/client/phar/MyClient.phar

Other developpers will be able to use your client like this :

    include 'phar://MyClient.phar';
    
    $client = new MyClient('https://my.application.tld');
    
    $client->authentication = new ApplicationRestAuthApplication('foo', 'bar');
    $client->authentication->user = 'me@domain.tld';
    
    print_r($client->getUserFoo());
