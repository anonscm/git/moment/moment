# Folders' roles

First of all, anything under folders named `core`, wherever would it be located, is not to be tampered with as it will be overwritten when updating the application base.is from the common application base, you musn't tamper with it since it will be overwritten when updating.

## `classes`

This folder contains all the application's PHP classes.


### `classes/auth`

This is the folder you must put your own authentication classes in ([see authentication](authentication.md)).


### `classes/endpoints`

Your own REST endpoints or overrides of existing ones ([see REST](rest.md)).


### `classes/exceptions`

Your own exceptions ([see Exceptions](exceptions.md)).


### `classes/model`

Your own model entities or overrides of existing ones ([see Model](model.md)).


### `classes/utils`

Your own utilities or overrides of existing ones ([see Utilities](utilities.md)).


## `config`

This folder contains the `config.php` in which your application config resides.

You can create a `language` folder here to add/override translations ([see translating](translating.md)).

You can create a `templates` folder here to add/override templates ([see templating](ui.md#templating)).


## `doc`

You can place your application documentation under this folder.


## `includes`

This folder contains several files related to config defaults, config validation and application startup.

You can create an `init.php` file to extend application init code.

You can create a `ConfigDefaults.php` file to set additionnal config defaults (just append to the `$defaults` array).

You can create an `ConfigValidation.php` file to add configuration checks (make calls to `ConfigValidator::addCheck`, [see config validation](config.md)).


## `language`

This folder contains all base translations.

You can add folders for every language you want your application to be translatable into here ([see translating](translating.md)).

You can also add a `locale.php` file in order to define another set of available locales.


## `lib`

You can put here all the PHP librairies you may use in your application (it is up to you to `require` them when needed).


### `lib/client`

You can put here any custom REST client you may write ([see custom REST clients](client/rest.md)).


#### `lib/client/core`

This is where the core utilities use can use as a base when writting custom REST clients are located.


#### `lib/client/phar`

This is where the [PHAR](http://php.net/manual/en/intro.phar.php) bundled clients will end-up in.


## `logs`

This is the default storage location for the application logs.


## `plugins`

This is where you can put your application plugins ([see Plugins](plugins.md)).


## `scripts`

This is the place where utility scripts are stored.


### `scripts/client`

You can place you application's dedicated client here, the `core` sub-folder contains the `ApplicationClient.class.php` base you can build upon.


### `scripts/task`

You can place any current task related script here.

The `core` sub-folder contains the `cron.php` cronjob script.


## `templates`

This is where the application's base templates reside ([see templating](ui.md#templating)).


## `tmp`

This folder is used to store more or less temporary data.


## `view`

This folder is the web root of the application.

You will find some files here :

  * `index.php` : you ui entry point
  * `plugin.php` : used in [debug mode](debug.md)
  * `rest.php` : the REST server entry point
  * `app-data.js.php` : the config file for JS purpose

You musn't tamper with those files since they will be overwritten when updating.


### `view/cache`

This folder contains compiled javascript and CSS for faster loading (automatic).


### `view/common`

This folder contains some common files like the server to client config and a sample WAYF.

You musn't tamper with those files since they will be overwritten when updating.


### `view/css`

This folder contains the application's CSS.

You can create an `application.css` file to store your application's CSS.

Any other file you may create must be added to loaded resources by registering to either the `css_libraries` or the `css_resources` [Event](events.md).


### `view/css/components`

Intended to host reusable components styles.

Any CSS file you put in this folder will be automatically loaded.


### `view/css/pages`

Intended to host dedicated page styles.

Any CSS file you put in this folder will be automatically loaded.


### `view/images`

This folder contains the application's images.

Several files will be automatically loaded :

  * `favicon.ico`, `favicon.gif` or `favicon.png` will be used as the application's favicon
  * `logo.svg`, `logo.jpg` or `logo.png` will be used as the application's logo in the banner

Any other file you put here may be used anywhere in the [templates](ui.md#templating) with `{img:filename}`.


### `view/js`

This folder contains the application's javascript code.

You can create a `application.js` file which will be automatically loaded.

Any file you may add must be added to loaded resources by registering to either the `js_libraries` or the `js_resources` [Event](events.md).


### `view/js/components`

Intended to host reusable components scripts.

Any javascript file you put in this folder will be automatically loaded.


### `view/js/pages`

Intended to host pages dedicated scripts.

Any javascript file you put in this folder will be automatically loaded.


### `view/lib`

You can add your application's external libraries here.

Any javascript file must be added to loaded resources by setting the [`js_libraries` config parameter](./config.md#js_libraries) or by registering to either the `js_libraries` or the `js_resources` [Event](events.md).

Any CSS file must be added to loaded resources by setting the [`css_libraries` config parameter](./config.md#css_libraries) or by registering to either the `css_libraries` or the `css_resources` [Event](events.md).
