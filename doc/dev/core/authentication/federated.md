# Federated authentication

The ApplicationBase is provided along with support for federated authentication.

Supported SPs are [Shibboleth](#shibboleth_authentication), and [SimpleSAMLPhp](#simplesamlphp_authentication).

[Fake SP authentication](#fake_authentication) is also available for testing purposes.

Disabling federated authentication is done by setting the [`auth_sp` config parameter](../config.md#auth_sp) to a falsish value (or not setting it at all).

Federated authentication can provide additional user attributes to the [Logger](../logging.md) through the use of the [`auth_sp.additional_attributes`](../config.md#auth_sp).


## Shibboleth authenticatication

This mode is enabled by setting the [`auth_sp.type` config parameter](../config.md#auth_sp) to `shibboleth`.

Required config required parameters are :

  * `auth_sp.uid_attribute` attribute name (or array of attributes names, first found wins) to use as user identifier
  * `auth_sp.email_attribute` attribute name (or array of attributes names, first found wins) to use as user email addresses
  * `auth_sp.name_attribute` attribute name (or array of attributes names, first found wins) to use as user name
  * `auth_sp.login_url` URL for triggering log-in, `{target}` will be replace with targetted page
  * `auth_sp.logout_url` URL for triggering log-out, `{target}` will be replace with targetted page


## SimpleSAMLPhp authentication

This mode is enabled by setting the [`auth_sp.type` config parameter](../config.md#auth_sp) to `saml`.

Required config parameters are :

  * `auth_sp.uid_attribute` attribute name (or array of attributes names, first found wins) to use as user identifier
  * `auth_sp.email_attribute` attribute name (or array of attributes names, first found wins) to use as user email addresses
  * `auth_sp.name_attribute` attribute name (or array of attributes names, first found wins) to use as user name
  * `auth_sp.authentication_source` name of the SAML source to use
  * `auth_sp.simplesamlphp_location` path of [SimpleSAMLphp library](https://simplesamlphp.org/)
  * `auth_sp.simplesamlphp_url` URL of the [SimpleSAMLphp SP](https://simplesamlphp.org/)


## Fake authentication

This mode is enabled by setting the [`auth_sp.type` config parameter](../config.md#auth_sp) to `fake`.

It allows to configure an identity of choice in order to test without having a real SP.

Required config parameters are :

  * `auth_sp.authenticated` boolean telling wether there is a session
  * `auth_sp.uid` user identifier to use
  * `auth_sp.email` user email (or array of user emails) to use
  * `auth_sp.name` user name to use


## Adding your own

A federated authentication mode is basically represented by a PHP class whose name starts with `AuthSP` and which is stored in `classes/auth/AuthSPYourChoice.class.php`.

This class MUST implement the [AuthSPInterface interface](phpdoc/AuthSPInterface).

It have 4 mandatory `public static` methods :

  * `isAuthenticated` returns a `boolean` indicating whether there is an authenticated user or not ;
  * `attributes` returns the authenticated user's attributes as an `associative array` with the following data :
    * `uid` user unique identifier (`string`, mandatory)
    * `email` user's email addresses (`array`)
    * `name` user's name (`string`)
    * `additional` user's additional attributes for logging (`associative array`)
  * `logonURL` returns the logon URL, takes an optional argument `target` page to go to once authentication succeeds
  * `logoffURL` returns the logoff URL, takes an optional argument `target` page to go to once deauthentication succeeds

Example :

    include 'OpenID';
    
    class AuthSPOpenID implements AuthSPInterface {
        public static function isAuthenticated() {
            return OpenID::hasSession();
        }
        
        public static function attributes() {
            return array(
                'uid' => OpenID::getAttribute('user_id'),
                'name' => OpenID::getAttribute('user_name'),
                'email' => OpenID::getAttribute('user_email')
            );
        }
        
        public static function logonURL($target = null) {
            return OpenID::loginURL($target);
        }
        
        public static function logoffURL($target = null) {
            return OpenID::logoutURL($target);
        }
    }
