# Remote authentication

The system can allow a remote user or application to access its webservice in a non-web way by using signed requests instead of a SP session.

There are 2 remote access modes : [remote user](#remote_user) and [remote application](#remote_application).


## Remote user

Any remote service can act on behalf of any user by requesting the user's remote access secret and building signed requests with it.

This feature must be enabled through the [`auth_remote_user_enabled` config parameter](../config.md#auth_remote_user_enabled).

### Getting the user's remote access credentials

The service must send the user to the `/remote_auth_sync_request/service_name` application page (example : `https://domain.tld/application/remote_auth_sync_request/foobar`).

The user is then presented a synchronization code which he has to give to your service. The code lives only for a short time (defiend by the [`remote_auth_sync_request_timeout` config parameter](../config.md#remote_auth_sync_request_timeout)).

Your service has to request the user's remote access credentials by fetching from the [user endpoint](../rest/endpoints/user.md) like so :

    GET https://domain.tld/application/rest.php/user/@me/remote_auth_config/the_synchronization_code

If the fetch is accepted the user's remote authentication data is returned, example (JSON response) :

    {
        "remote_config": "application_url|user_id|user_secret"
    }

Your service can then save the remote access credentials in a place of its choice in order to use it in the future.


### Requesting

Having a set of user's remote access credentials your service can request from the application's webservice on behalf of the user.

To do so it must build a request (URL and body), add the user identifier (`user_id`) to the [query string](https://en.wikipedia.org/wiki/Query_string) as the `remote_user` argument and [sign it](#building_signed_requests) using the user's secret (`user_secret`).


## Remote application

A service can be identified as a trusted remote application and allowed to perform REST requests.

This feature must be enabled through the [`auth_remote_application_enabled` config parameter](../config.md#auth_remote_application_enabled).


### Registering a remote application

To register a trusted remote application it must be added in the [`auth_remote[application][applications]` config array](../config.md#auth_remote) like such :

    $config['auth_remote']['application']['applications']['application_identifier'] = array(
        'secret' => 'application_secret',   // String, length at will (the longer the better !)
        
        'isAdmin' => false, // Boolean telling if the remote application has admin level privileges
        
        'acl' => array(         // Access control list, array of endpoint identifiers and access level pairs (wildcard can be used to match all endpoints),
            'user' => array(    // level can be a boolean telling if access is allowed or an array of method and accessibility pairs (wildcard can be used to match all methods).
                'get' => true,  //
                '*' => false    // This example allows get access only to the user endpoint and allows any method on all other endpoints
            )
            '*' => true
        )
    );


### Requesting

If registered as a trusted remote application your service can request from the application's webservice (on behalf of any user).

To do so it must build a request (URL and body), add its application identifier (`application_id`) to the [query string](https://en.wikipedia.org/wiki/Query_string) as the `remote_application` argument, add the user identifier as the query string `remote_user` if necessary and [sign it](#building_signed_requests) using its application secret.


## Building signed requests

To prepare a signed request for calling the service must follow the steps below :

  * start the signed data with the lowercased [HTTP method](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods)
  * append a "&" sign
  * append the full resource path in the webservice, including the domain but not the [scheme](https://en.wikipedia.org/wiki/Uniform_Resource_Locator#Syntax)
  * build the set of [query string](https://en.wikipedia.org/wiki/Query_string) arguments
  * add a `timestamp` [query string](https://en.wikipedia.org/wiki/Query_string) argument containing the current [Unix time](https://en.wikipedia.org/wiki/Unix_time)
  * sort the [query string](https://en.wikipedia.org/wiki/Query_string) by argument name in alphabetic order
  * append the [query string](https://en.wikipedia.org/wiki/Query_string) the the signed data after a "?" sign (skip if no [query string](https://en.wikipedia.org/wiki/Query_string) arguments)
  * append the request body (if any) after a "&" sign
  * sign the built data with the [Hmac](https://en.wikipedia.org/wiki/Hash-based_message_authentication_code) algorithm defined by the [`auth_remote_signature_algorithm` config parameter](../config.md#auth_remote_signature_algorithm) (hexadecimal signature output)
  * append the signature as the `signature` [query string](https://en.wikipedia.org/wiki/Query_string) argument

From the moment the `timestamp` is generated the service only has a few seconds, defined by the [`auth_remote_timeout` config parameter](../config.md#auth_remote_timeout), to call the webservice.
