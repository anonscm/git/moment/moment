# Local authentication

Local authentication is to be used in scripts that need to act on behalf of a user.

Openning an user session in your scripts is done like this :

    AuthLocal::setUser($uid, $emails, $name);

`$uid` MUST be a `string` containing the user identifier.

`$emails` MAY be an `array` containing the user's email addresses (may not be used depending on what your script is doing).

`$name` MAY be the user's name in case it is needed by what your script is doing.

To cancel a session just call the same method with a `null` user identifier :

    AuthLocal::setUser(null);
