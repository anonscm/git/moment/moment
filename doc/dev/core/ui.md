# User interface

## Templating

The user interface make use of templates. Templates are located in the `/templates/` directory with several default templates in `/templates/core/`.

Creating a template in `/templates/` with the same name as one in `/templates/core/` will override the default template.

The template lookup is done in the following order :

  * look in `/config/templates/` if it exists
  * look in `/templates/`
  * look in `/templates/core/`
  * dies if no match

If you want to add content in a default template without having to redefine it all you can listen to the [`template_process` event](./events.md#template_process) like so :

    Event::register(Event::AFTER, 'template_process', function($event) {
        // Chack if template is the wanted one
        $id = $event->data[0];
        if($id != 'wanted_template') return;
        
        // Replace stuff, $event->result is the template output as an HTML string
        $event->result = preg_replace('`foo`', 'bar', $event->result);
    });


### Template writting

A template is just a PHP file that will be included in a restricted scope.

The templating system provides quick means to access the translating engine, local variables, the configuration ...

#### Translating

You can use the following syntaxes to get [translated text](./translating.md) rendered in you template's output :

    {translate:lang_string_id}
    {tr:lang_string_id}
    {loc:lang_string_id}

All these syntaxes are the equivalent of :

    <?php echo Lang::translate('lang_string_id') ?>


#### Variables

Most of the time a template is rendered for a set of variables :

    Template::display('the_template', array(
        'foo' => 3,
        'bar' => 5
        'items' => FooBar::all('foo = :foo AND bar = :bar', array(':foo' => 3, ':bar' => 5))
    ));

In this case you can use the given variables in your template :

    <div class="<?php echo ($foo > 2) ? 'lotsof' : 'few' ?>_foos">
        <?php foreach($items as $item) { ?>
            <div><?php echo $item->id ?></div>
        <?php } ?>
    </div>


#### Configuration

Rendering configuration variables if easy :

    {config:application_name}
    {conf:application_name}
    {cfg:application_name}

All these syntaxes are the equivalent of :

    <?php echo Config::get('application_name') ?>


#### Images

Rendering the path of an images stored under `/view/images/` is done like so :

    <img src="{image:foobar.png}" />
    <img src="{img:foobar.png}" />

All these syntaxes are the equivalent of :

    <img src="<?php echo GUI::path('images/foobar.png') ?>" />


#### Path

Rendering a path is done like so :

    {path:js/foobar.js}

It is the equivalent of :

    <?php echo GUI::path('js/foobar.js') ?>


#### URLs

Rendering an URL relative to the application is done like so :

    <a href="{url:foo/bar?id=3}">Foo</a>

It will produce something like :

    <a href="https://your.application.tld/path/to/the/application/foo/bar?id=3">Foo</a>


#### Authentication URLs

Rendering an authentication triggering URL is done like so :

    <a href="{auth_url:foo/bar?id=3}">Foo (auth)</a>

It will produce something like (using Shibboleth SP) :

    <a href="https://your.application.tld/Shibboleth.sso/Login?target=https%3A%2F%2Fyour.application.tld%2Fpath%2Fto%2Fthe%2Fapplication%2Ffoo%2Fbar%3Fid%3D3">Foo</a>


### Rendering a template

A template can be rendered like so :

    Template::display('template_identifier', $associative_array_of_variables);

If you want to manipulate the template output :

    $out = Template::process('template_identifier', $associative_array_of_variables);


## Pages

The ApplicationBase includes a simple page mecanism, any template whose name ends with the `_page` suffix will be recognized as a page and will be accessible through `https://your.application.tld/path/to/the/application/index.php/page_name` (and `https://your.application.tld/path/to/the/application/page_name` if [rewriting](#rewriting) is enabled.

Any additionnal path tokens (after the `/page_name` one) will be given to the page as a `$path` array, for example accesses to :

    https://your.application.tld/path/to/the/application/foo/bar/3

Will have the `foo_page.php` template looked for and the propagated path will be :

    $path = array('bar', 3);



## Default templates

  * `exception.php` : renders an application exception along with support contact info if configured
  * `faq_page.php` : placeholder for the application FAQ
  * `footer.php` : UI footer (bottom banner)
  * `header.php` : UI header (banner, top menu)
  * `home_page.php` : home page
  * `logout_page.php` : "you've been logged out" page
  * `maintenance.php` : maintenance message
  * `menu.php` : application top menu
  * `page.php` : page dispatcher
  * `page_menu.php` : page menu (big on home page, smaller on other)
  * `remote_auth_sync_request_page.php` : [user remote authentication](./authentication/remote.md#remote_user) autorisation page
  * `translate_email_page.php` : [sent emails translation](./email_sending.md#instant_translation) page
  * `user_guide_page.php` : user guide placeholder
  * `wayf.php` : federation WAYF


## Resources bundling

By default UI resources are automatically bundled into 2 files (Javascript and CSS) to lighten the bandwith use, this can be disabled by setting the [`debug` configuration parameter](./config.md#debug) to `true`.

Refreshing the bundled files cache is done by [`touch`ing](https://en.wikipedia.org/wiki/Touch_%28Unix%29) the `config/config.php` file.
