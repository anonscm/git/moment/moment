<?php

// -----------------------------------------------------------------------------
// GET
// -----------------------------------------------------------------------------

/**
 * @api {get} /rest.php/survey/:survey_id GET Survey
 * @apiName GetSurvey
 * @apiGroup Survey
 * @apiVersion 1.0.0
 * @apiDescription Get a specific survey owned by the currently authenticated user.
 *
 * @apiParam {String} :survey_id The survey unique identifier.
 *
 * @apiUse SurveyReturn
 *
 * @apiUse surveyEndpointThrows
 */

/**
 * @api {get} /rest.php/survey/ GET Surveys
 * @apiName GetSurveys
 * @apiGroup Survey
 * @apiVersion 1.0.0
 * @apiDescription Get a all surveys owned by the currently authenticated user.
 *
 * @apiSuccess {Array} Array of surveys
 *
 * @apiUse surveyEndpointThrows
 * @apiError TODO
 */

/**
 * @api {get} /rest.php/survey/@answered GET Answered Surveys
 * @apiName GetAnsweredSurveys
 * @apiGroup Survey
 * @apiVersion 1.0.0
 * @apiDescription Get a all surveys answered by the currently authenticated user.
 *
 * @apiSuccess {Array} Array of surveys
 *
 * @apiUse surveyEndpointThrows
 * @apiError TODO
 */

/**
 * @api {get} /rest.php/survey/@guest GET Invited Surveys
 * @apiName GetInvitedSurveys
 * @apiGroup Survey
 * @apiVersion 1.0.0
 * @apiDescription Get a all surveys where the currently authenticated user is invited.
 *
 * @apiSuccess {Array} Array of surveys
 *
 * @apiUse surveyEndpointThrows
 * @apiError TODO
 */

/**
 * @api {get} /rest.php/survey/@feed GET Surveys Feed
 * @apiName GetSurveysFeed
 * @apiGroup Survey
 * @apiVersion 1.0.0
 * @apiDescription Get a all surveys where the currently authenticated user's attention is needed.
 *
 * @apiSuccess {Array} Array of surveys
 *
 * @apiUse surveyEndpointThrows
 * @apiError TODO
 */


// -----------------------------------------------------------------------------
// POST
// -----------------------------------------------------------------------------
/**
 * @api {post} /rest.php/survey/ POST Survey
 * @apiName Post Survey
 * @apiGroup Survey
 * @apiVersion 1.0.0
 * @apiDescription Post a survey, this survey will be owned by the currently authenticated user.
 *
 * @apiUse SurveyPost
 *
 * @apiUse surveyEndpointThrows
 * @apiError TODO
 */

// -----------------------------------------------------------------------------
// PUT
// -----------------------------------------------------------------------------
/**
 * @api {put} /rest.php/survey/:survey_id PUT Survey
 * @apiName Put Survey
 * @apiGroup Survey
 * @apiVersion 1.0.0
 * @apiDescription Update a survey identified by <code>:survey_id</code>. This survey must be owned by the currently authenticated user.
 *
 * @apiParam {String} :survey_id The survey unique identifier.
 * @apiUse SurveyPost
 *
 * @apiUse surveyEndpointThrows
 * @apiError TODO
 */

// -----------------------------------------------------------------------------
// DELETE
// -----------------------------------------------------------------------------
/**
 * @api {delete} /rest.php/survey/:survey_id DELETE Survey
 * @apiName Delete Survey
 * @apiGroup Survey
 * @apiVersion 1.0.0
 * @apiDescription Delete a survey identified by <code>:survey_id</code>. This survey must be owned by the currently authenticated user.
 *
 * @apiParam {String} :survey_id The survey unique identifier.
 *
 * @apiUse surveyEndpointThrows
 * @apiError TODO
 */

// -----------------------------------------------------------------------------
// Returned fields
// -----------------------------------------------------------------------------

/**
 * @apiDefine SurveyReturn
 *
 * @apiSuccess {string} survey_id The survey unique identifier.
 * @apiSuccess {String} title The survey title.
 * @apiSuccess {String} path The survey path.
 * @apiSuccess {String} place The survey place.
 * @apiSuccess {String} description The survey description.
 * @apiSuccess {Object} created :
 * @apiSuccess {Integer} created.row a unix timestamp of the creation of the survey
 * @apiSuccess {String} created.formatted a formatted representation of the creation date (Format depends on user language preference)
 * @apiSuccess {Object} updated :
 * @apiSuccess {Integer} updated.row a unix timestamp of the creation of the survey
 * @apiSuccess {String} updated.formatted a formatted representation of the update date (Format depends on user language preference)
 * @apiSuccess {Object} closed :
 * @apiSuccess {Integer} closed.row a unix timestamp of the closing of the survey
 * @apiSuccess {String} closed.formatted a formatted representation of the close date (Format depends on user language preference)
 * @apiSuccess {Object} settings :
 * @apiSuccess {Integer} settings.auto_close The expire date of the survey
 * @apiSuccess {Boolean} settings.limit_participants True if survey answers are limited in number
 * @apiSuccess {Integer} settings.limit_participants_nb Number of answers which will trigger the closing of the survey
 * @apiSuccess {Boolean} settings.enable_anonymous_answer True if identity (name/email) is not required for this survey
 * @apiSuccess {Boolean} settings.disable_answer_edition True if participant cannot edit is answer
 * @apiSuccess {Boolean} settings.dont_notify_on_reply True if owner doesn't want to be notified on reply
 * @apiSuccess {Boolean} settings.hide_answers True if participant must not see other participants answers
 * @apiSuccess {Boolean} settings.hide_comments True if participant must not see other participants comments
 * @apiSuccess {String} settings.time_zone The survey owner/updater timezone
 * @apiSuccess {String} settings.reply_access A string identifying who can reply to the survey <br/>
 * either ("opened_to_noone","opened_to_guests", "opened_to_authenticated", "opened_to_everyone"). From left to right less restrictive.
 * @apiSuccess {String} settings.answers_view_access A string identifying who can see the answers of the survey <br/>
 * either ("answer_access_owner","answer_access_repliers", "answer_access_repliers_with_answers"). From left to right less restrictive.
 * @apiSuccess {Boolean} settings.enable_anonymous_answer True if participant can be anonymous
 * @apiSuccess {Boolean} settings.dont_receive_invitation_copy True if owner doesn't want to receive invitation
 * @apiSuccess {Object[]} questions Array of the questions in the survey
 * @apiSuccess {Integer} question.id Question unique identifier
 * @apiSuccess {String} question.title Question title (Ex. "Where is Bryan?")
 * @apiSuccess {String} question.type Question type, either "text" or "date"
 * @apiSuccess {Integer} question.position the question position (If 0, question is the first to show)
 * @apiSuccess {Object} question.options Options for the question
 * @apiSuccess {Boolean} question.options.force_unique_choice If true Participant can choose only one proposition
 * @apiSuccess {Boolean} question.options.enable_maybe_choices If true Participant can choose the "Maybe" answer for a proposition
 * @apiSuccess {Object[]} question.propositions Array of propositions for the Question
 * @apiSuccess {Integer} question.proposition.id Proposition unique identifier
 * @apiSuccess {String} question.proposition.type Proposition type
 * either "text", "day", "day_hour", "range_of_days", "range_of_days_hours", "range_of_hours"
 * @apiSuccess {Integer} question.proposition.base_day
 * (When type is "day" or "day_hour" or "range_of_days" or "range_of_days_hours" or "range_of_hours") Unix timestamp of the proposition date
 * @apiSuccess {Integer} question.proposition.base_time
 * (When type is "day_hour" or "range_of_days_hours" or "range_of_hours") Number of seconds from midnight
 * @apiSuccess {Integer} question.proposition.end_day
 * (When type is "range_of_days" or "range_of_days_hours") Unix timestamp of the proposition date end
 * @apiSuccess {Integer} question.proposition.end_time
 * (When type is "range_of_days_hours" or "range_of_hours") Number of seconds from midnight
 * @apiSuccess {Boolean} is_draft True if survey is a draft of survey
 * @apiSuccess {Object[]} owners Array of the guests in the survey
 * @apiSuccess {String} owner.name Name of the owner
 * @apiSuccess {String} owner.email Email of the owner
 * @apiSuccess {Object[]} guests Array of the guests in the survey
 * @apiSuccess {String} guest.email Email of the guest
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *	    "id": "86zvyzz8",
 *	    "title": "Survey Title",
 *      "path": "http://.../survey/titre-fwynkdst"
 *	    "place": "",
 *	    "description": "",
 *	    "created": {
 *	        "raw": 1473252785,
 *	        "formatted": "07/09/2016"
 *      },
 *	    "updated": null,
 *	    "closed": null,
 *	    "settings": {
 *	        "auto_close": 1507075200,
 *          "limit_participants": 1,
 *          "limit_participants_nb": "1",
 *          "disable_answer_edition": 1,
 *          "dont_notify_on_reply": 0,
 *          "hide_answers": 0,
 *          "hide_comments": 0,
 *          "time_zone": "Europe/Paris"
 *          "reply_access": "opened_to_authenticated",
 *          "enable_anonymous_answer": 0,
 *          "dont_receive_invitation_copy": 0
 *      },
 *	    "questions": [
 *          {
 *	            "id": 30,
 *	            "title": "Question 1",
 *	            "type": "date",
 *	            "position": 0,
 *	            "options": {
 *	                "force_unique_choice": 1,
 *                  "enable_maybe_choices": 0
 *              },
 *	            "propositions": [
 *                  {
 *	                    "id": 8,
 *	                    "type": "day",
 *	                    "base_day": 1472601600
 *                  }
 *              ]
 *          },
 *
 *      ],
 *      "is_draft": false,
 *	    "owners": [
 *          {
 *              "name": "John Doe";
 *              "email": "john.doe@email.com"
 *          }
 *      ],
 *	    "guests": []
 * }
 */

// -----------------------------------------------------------------------------
// Posted fields
// -----------------------------------------------------------------------------

/**
 * @apiDefine SurveyPost
 *
 * @apiParam {String} title The survey title.
 * @apiParam {String} place The survey place.
 * @apiParam {String} description The survey description.
 * @apiParam {Object} settings :
 * @apiParam {Integer} settings.auto_close The expire date of the survey
 * @apiParam {Boolean} settings.limit_participants True if survey answers are limited in number
 * @apiParam {Integer} settings.limit_participants_nb Number of answers which will trigger the closing of the survey
 * @apiParam {Boolean} settings.enable_anonymous_answer True if identity (name/email) is not required for this survey
 * @apiParam {Boolean} settings.disable_answer_edition True if participant cannot edit is answer
 * @apiParam {Boolean} settings.dont_notify_on_reply True if owner doesn't want to be notified on reply
 * @apiParam {Boolean} settings.hide_answers True if participant must not see other participants answers
 * @apiParam {Boolean} settings.hide_comments True if participant must not see other participants comments
 * @apiParam {String} settings.time_zone The survey owner/updater timezone
 * @apiParam {String} settings.reply_access A string identifying who can reply to the survey <br/>
 * either ("opened_to_noone","opened_to_guests", "opened_to_authenticated", "opened_to_everyone"). From left to right less restrictive.
 * @apiParam {String} settings.answers_view_access A string identifying who can see the answers of the survey <br/>
 * either ("answer_access_owner","answer_access_repliers", "answer_access_repliers_with_answers"). From left to right less restrictive.
 * @apiParam {Boolean} settings.enable_anonymous_answer True if participant can be anonymous
 * @apiParam {Boolean} settings.dont_receive_invitation_copy True if owner doesn't want to receive invitation
 * @apiParam {Object[]} questions Array of the questions in the survey
 * @apiParam {Integer} question.id Question unique identifier
 * @apiParam {String} question.title Question title (Ex. "Where is Bryan?")
 * @apiParam {String} question.type Question type, either "text" or "date"
 * @apiParam {Integer} question.position the question position (If 0, question is the first to show)
 * @apiParam {Object} question.options Options for the question
 * @apiParam {Boolean} question.options.force_unique_choice If true Participant can choose only one proposition
 * @apiParam {Boolean} question.options.enable_maybe_choices If true Participant can choose the "Maybe" answer for a proposition
 * @apiParam {Object[]} question.propositions Array of propositions for the Question
 * @apiParam {Integer} question.proposition.id Proposition unique identifier
 * @apiParam {String} question.proposition.type Proposition type
 * either "text", "day", "day_hour", "range_of_days", "range_of_days_hours", "range_of_hours"
 * @apiParam {Integer} question.proposition.base_day
 * (When type is "day" or "day_hour" or "range_of_days" or "range_of_days_hours" or "range_of_hours") Unix timestamp of the proposition date
 * @apiParam {Integer} question.proposition.base_time
 * (When type is "day_hour" or "range_of_days_hours" or "range_of_hours") Number of seconds from midnight
 * @apiParam {Integer} question.proposition.end_day
 * (When type is "range_of_days" or "range_of_days_hours") Unix timestamp of the proposition date end
 * @apiParam {Integer} question.proposition.end_time
 * (When type is "range_of_days_hours" or "range_of_hours") Number of seconds from midnight
 * @apiParam {Object[]} guests Array of the guests in the survey
 * @apiParam {String} guest.email Email of the guest
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *	    "title": "Survey Title",
 *	    "place": "",
 *	    "description": "",
 *	    "settings": {
 *	        "auto_close": 1507075200,
 *          "limit_participants": 1,
 *          "limit_participants_nb": "1",
 *          "disable_answer_edition": 1,
 *          "dont_notify_on_reply": 0,
 *          "hide_answers": 0,
 *          "hide_comments": 0,
 *          "time_zone": "Europe/Paris",
 *          "reply_access": "opened_to_authenticated",
 *          "enable_anonymous_answer": 0,
 *          "dont_receive_invitation_copy": 0
 *      },
 *	    "questions": [
 *         {
 *	        "id": 30,
 *	        "title": "Question 1",
 *	        "type": "date",
 *	        "position": 0,
 *	        "options": {
 *	            "force_unique_choice": 1,
 *              "enable_maybe_choices": 0
 *          },
 *	        "propositions": [
 *              {
 *	                "id": 8,
 *	                "type": "day",
 *	                "base_day": 1472601600
 *              }
 *          ]
 *      }
 *      ],
 *      "owners": [
 *          {
 *              "name": "John Doe",
 *              "email": "john.doe@email.com"
 *          }
 *      ]
 *	    "guests": [
 *          {
 *              "email": "foo.bar@test.com"
 *          }
 *      ]
 * }
 */

// -----------------------------------------------------------------------------
// Errors
// -----------------------------------------------------------------------------

/**
 * @apiDefine surveyEndpointThrows
 *
 * @apiError {400} rest_bad_parameter If any parameter set is not conform.
 * @apiError {403} rest_authentication_required No authentication was found.
 * @apiError {404} not_found Survey <code>survey_id</code> not found.
 */