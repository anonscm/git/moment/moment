<?php

// -----------------------------------------------------------------------------
// POST
// -----------------------------------------------------------------------------
/**
 * @api {post} /rest.php/comment/ POST Comment
 * @apiName Post Comment
 * @apiGroup Comment
 * @apiVersion 1.0.0
 * @apiDescription Post a comment on a survey. It if necessary creates a new participant.
 *
 * @apiParam {String} survey_id The survey unique identifier.
 * @apiParam {String} content The content of the comment.
 * @apiParam {Object} [participant=<authenticated user participant>] The participant which comment.
 * @apiParam {String} [participant.participant_token] The participant token. This token is given by the app to identify non authenticated users.
 * @apiParam {String} [participant.email] The new participant email. (If no token, a new participant will be created with this email and name)
 * @apiParam {String} [participant.name] The new participant name. (If no token, a new participant will be created with this email and name)
 *
 * @apiSuccess {string} path the path to get this participant comments
 * @apiSuccess {Object[]} comments This participant comments
 * @apiSuccess {Integer} comment.id The comment id
 * @apiSuccess {String} comment.participant_hash A participant identifier for this request
 * @apiSuccess {Object} comment.created :
 * @apiSuccess {Integer} comment.created.row a unix timestamp of the creation of the comment
 * @apiSuccess {String} comment.created.formatted a formatted representation of the creation date (Format depends on user language preference)
 * @apiSuccess {String} comment.content The content of the comment
 *
 * @apiUse surveyEndpointThrows
 * @apiError TODO
 */