<?php

// -----------------------------------------------------------------------------
// POST
// -----------------------------------------------------------------------------
/**
 * @api {post} /rest.php/answer/ POST Answer
 * @apiName Post Answer
 * @apiGroup Answer
 * @apiVersion 1.0.0
 * @apiDescription Post a answer for a survey. It if necessary creates a new participant (<b>Participant data are mandatory in the first answer</b>).

 * @apiParam {Object[]} answers An array of answers, and answer is the reply of an user to a question
 * @apiParam {Object} [answer.participant] The participant which answers.
 * @apiParam {String} [answer.participant.participant_token] The participant token. This token is given by the app to identify non authenticated users.
 * @apiParam {String} [answer.participant.email] The new participant email. (If no token, a new participant will be created with this email and name)
 * @apiParam {String} [answer.participant.name] The new participant name. (If no token, a new participant will be created with this email and name)
 * @apiParam {String} answer.question_id The identifier of the question.
 * This identifier permits to get the survey we are answering to; all answers of this survey will be processed, others will be ignored.
 * <b>The first answer is the reference</b>
 * @apiParam {Object[]} answer.choices an array of choices. At most one choice by question.propositions.
 * @apiParam {Integer} answer.choice.proposition_id Identifier of the proposition
 * @apiParam {String} answer.choice.value="select_value_no" Current value for this proposition either "select_value_yes", "select_value_maybe", "select_value_no"
 *
 * @apiSuccess {string} path the path to get this participant answers
 * @apiSuccess {Object[]} answers This participant comments
 * @apiSuccess {Integer} answer.id The answer id
 * @apiSuccess {Integer} answer.question_id The answer id
 * @apiSuccess {String} answer.participant_hash A participant identifier for this request
 * @apiSuccess {Object[]} answer.choices Array of choice
 * @apiSuccess {Integer} answer.choice.proposition_id Identifier of the proposition
 * @apiSuccess {String} answer.choice.value Current value for this proposition either "select_value_yes", "select_value_maybe", "select_value_no"
 *
 * @apiUse surveyEndpointThrows
 * @apiError TODO
 */

/**
 * @api {put} /rest.php/answer/ PUT Answer
 * @apiName Put Answer
 * @apiGroup Answer
 * @apiVersion 1.0.0
 * @apiDescription Update answers for a survey. (<b>Participant data are mandatory in the first answer</b>).
 
 * @apiParam {Object[]} answers An array of answers, and answer is the reply of an user to a question
 * @apiParam {Object} [answer.participant] The participant which answers.
 * @apiParam {String} [answer.participant.participant_token] The participant token. This token is given by the app to identify non authenticated users.
 * @apiParam {String} answer.answer_id The identifier of the answer to update.
 * @apiParam {String} answer.question_id The identifier of the question.
 * This identifier permits to get the survey we are answering to; all answers of this survey will be processed, others will be ignored.
 * <b>The first answer is the reference</b>
 * @apiParam {Object[]} answer.choices an array of choices. At most one choice by question.propositions.
 * @apiParam {Integer} answer.choice.proposition_id Identifier of the proposition
 * @apiParam {String} answer.choice.value="select_value_no" Current value for this proposition either "select_value_yes", "select_value_maybe", "select_value_no"
 *
 * @apiSuccess {string} path the path to get this participant answers
 * @apiSuccess {Object[]} answers This participant comments
 * @apiSuccess {Integer} answer.id The answer id
 * @apiSuccess {Integer} answer.question_id The answer id
 * @apiSuccess {String} answer.participant_hash A participant identifier for this request
 * @apiSuccess {Object[]} answer.choices Array of choice
 * @apiSuccess {Integer} answer.choice.proposition_id Identifier of the proposition
 * @apiSuccess {String} answer.choice.value Current value for this proposition either "select_value_yes", "select_value_maybe", "select_value_no"
 *
 * @apiUse surveyEndpointThrows
 * @apiError TODO
 */