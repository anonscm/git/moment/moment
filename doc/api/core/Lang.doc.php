<?php

// -----------------------------------------------------------------------------
// API DOC
// -----------------------------------------------------------------------------

/**
 * @api {get} /lang Get translations
 * @apiName GetLang
 * @apiGroup Lang
 * @apiVersion 0.0.1
 * 
 * @apiSuccess {Object} response pairs of translation keys and translated terms
 */
