<?php

// -----------------------------------------------------------------------------
// Common definitions
// -----------------------------------------------------------------------------

/**
 * @apiDefine UserEndpointThrows
 * @apiError {403} rest_authentication_required Authentication required
 * @apiError {403} auth_remote_user_rejected Remote auth sync rejected because it is disabled or user do not have a secret yet
 * @apiError {403} rest_ownership_required Not enough privileges
 * @apiError {403} rest_admin_required Not enough privileges
 */

/**
 * @apiDefine UserIdentifierParam
 * @apiParam {String} [:id] User id
 */

/**
 * @apiDefine UserList
 * @apiSuccess {Array} response List of [Users](@api/User/GetUser)
 */

/**
 * @apiDefine UserDescription
 * @apiSuccess {String} id Unique user identifier
 * @apiSuccess {Object} additional_attributes Pairs of additional attributes keys and values
 * @apiSuccess {Object} created Date of creation
 * @apiSuccess {Number} created.raw UNIX timestamp representation
 * @apiSuccess {String} created.formated Formated representation (format: Y-m-d H:i:s)
 * @apiSuccess {Object} last_activity Date of last recorded activity
 * @apiSuccess {Number} last_activity.raw UNIX timestamp representation
 * @apiSuccess {String} last_activity.formated Formated representation (format: Y-m-d H:i:s)
 * @apiSuccess {String} lang User prefered language
 * @apiSuccess {String} main_email User main email address (only provided if requestor is admin)
 */

/**
 * @apiDefine UserRemoteConfigDescription
 * @apiSuccess {String} remote_config Pipe separated list of application URL, user identifier and user secret
 */


// -----------------------------------------------------------------------------
// API DOC
// -----------------------------------------------------------------------------


/**
 * @api {get} /user/@me Get current user
 * @apiName GetCurrentUser
 * @apiGroup User
 * @apiVersion 0.0.1
 * 
 * @apiUse UserDescription
 * 
 * @apiUse UserEndpointThrows
 */


/**
 * @api {get} /user/:id Get user
 * @apiName GetUser
 * @apiGroup User
 * @apiVersion 0.0.1
 * @apiDescription Restricted to admin or user himself.
 * 
 * @apiUse UserIdentifierParam
 * 
 * @apiUse UserDescription
 * 
 * @apiUse UserEndpointThrows
 */


/**
 * @api {get} /user/ Get all users
 * @apiName GetUsers
 * @apiGroup User
 * @apiVersion 0.0.1
 * @apiDescription Restricted to admin.
 * 
 * @apiUse UserList
 * 
 * @apiUse UserEndpointThrows
 */


/**
 * @api {get} /user/@me/remote_auth_config Get current user remote config
 * @apiName GetUserRemoteConfig
 * @apiGroup User
 * @apiVersion 0.0.1
 * @apiDescription Only works if already properly negociated, [see remote authentication](@dev/core/authentication/remote.md#remote_user).
 * 
 * @apiUse UserRemoteConfigDescription
 * 
 * @apiUse UserEndpointThrows
 */


/**
 * @api {get} /user/:id/remote_auth_config Get user remote config
 * @apiName GetUserRemoteConfig
 * @apiGroup User
 * @apiVersion 0.0.1
 * @apiDescription Restricted to admin or user himself, only works if already properly negociated, [see remote authentication](@dev/core/authentication/remote.md#remote_user).
 * 
 * @apiUse UserIdentifierParam
 * 
 * @apiUse UserRemoteConfigDescription
 * 
 * @apiUse UserEndpointThrows
 */
