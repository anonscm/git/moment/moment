<?php

/**
 * This file is part of the BaseProject project.
 * 2015
 * Copyright (c) RENATER
 */

define('EKKO_ROOT', realpath(dirname(__FILE__).'/../../'));

/**
 * Documentation generator
 */

// Parse arguments / env

$options = getopt('hvqp:t:d:', array('help', 'path:', 'target:', 'dav:', 'phpdoc:', 'apidoc:'));

if(array_key_exists('h', $options) || array_key_exists('help', $options)) {
    echo 'Generate browsable documentation'."\n";
    echo "\t\n";
    echo 'Usage :'."\n";
    echo "\t".' php generate.php [-h|--help] [-v] [-q] [-p <path>|--path=<path>] [-d <settings>|--dav=<settings>] [-t <type>|--type=<type>] [--phpdoc=<path>] [--apidoc=<path>]'."\n";
    echo "\t\n";
    echo "\t".' -h | --help : this help'."\n";
    echo "\t".' -v : verbose mode'."\n";
    echo "\t".' -q : quiet mode'."\n";
    echo "\t".' -p | --path : set target directory (will be created unless it exists)'."\n";
    echo "\t".' -d | --dav : settings file for webdav push'."\n";
    echo "\t".' -t | --type : type of documentation to be generated (comma separated set of "api", "php", "dev" or "all"), defaults to "all"'."\n";
    echo "\t".' --phpdoc : path to phpdoc binary'."\n";
    echo "\t".' --apidoc : path to apidoc binary'."\n";
    echo "\n";
    echo 'Additionnal config files/directories :'."\n";
    echo "\t".'doc/tools/generate/api/apidoc.json : config for apidoc'."\n";
    echo "\t".'doc/tools/generate/api/template/ : custom template for apidoc'."\n";
    echo "\t".'doc/tools/generate/php/args.php : additional arguments for phpdoc as an $args array with switch and value pairs (switches must include hyphens)'."\n";
    echo "\t".'doc/tools/generate/dev/index.html : html index file for the developper documentation'."\n";
    echo "\t".'doc/tools/generate/dev/template.html : html page template file for the developper documentation'."\n";
    echo "\t".'doc/tools/generate/index.html : html index file for the documentation root'."\n";
    exit;
}

global $quiet;
$quiet = array_key_exists('q', $options);

global $verbose;
$verbose = !$quiet && array_key_exists('v', $options);

/**
 * Displays a message
 *
 * @param $msg
 */
function msg($msg) {
    global $quiet;
    if($quiet) return;
    
    echo $msg."\n";
}

/**
 * Displays a verbose message
 *
 * @param $msg
 */
function verbose($msg) {
    global $verbose;
    if(!$verbose) return;
    
    msg($msg);
}

/**
 * Displays an error and die
 *
 * @param $msg
 */
function error($msg) {
    die($msg.', exiting'."\n");
}

/**
 * Run a system command get the result if needed
 *
 * @param string $command
 * @param bool $get_output
 *
 * @return array
 */
function run($command, $get_output = false) {
    global $verbose;
    
    $ret = null;
    $output = array();
    
    verbose('Run '.$command);
    
    if($get_output || !$verbose) {
        exec($command, $output, $ret);
        
    } else {
        system($command, $ret);
    }
    
    if($ret) error($command.' execution failed');
    
    return $output;
}

/**
 * Check that a system command exists
 *
 * @param $what
 *
 * @return bool
 */
function shell_exists($what) {
    return (bool)trim(shell_exec(escapeshellcmd('which '.$what)));
}

/**
 * Check wether utility is available
 *
 * @param string $name
 * @param array $options
 * @param mixed $default
 *
 * @return mixed
 */
function checkUtil($name, $options, $default = null) {
    if(array_key_exists($name, $options)) {
        if(file_exists($options[$name]))
            return $options[$name];
        
        error($options[$name].' not found');
    }
    
    if(is_null($default))
        $default = $name;
    
    if(shell_exists($default))
        return $default;
    
    error($default.' not found');
}

/**
 * Make path (recursively)
 *
 * @param $path
 */
function mkpath($path) {
    clearstatcache(true, $path);
    
    if(is_dir($path)) return;
    
    if(!mkdir($path, 0777, true))
        error('Could not create '.$path);
}

/**
 * Find files under path with optionnal pattern matching
 *
 * @param string $path
 * @param string $matcher
 *
 * @return array
 */
function findFiles($path, $matcher = null) {
    $md = array();
    
    foreach(scandir($path) as $item) {
        if(in_array($item, array('.', '..'))) continue;
        
        $item = $path.'/'.$item;
        
        if(is_dir($item)) {
            foreach(findFiles($item, $matcher) as $file)
                $md[] = $file;
        }
        
        if(is_file($item) && (!$matcher || preg_match('`'.$matcher.'`', $item)))
            $md[] = $item;
    }
    
    sort($md);
    
    return $md;
}

/**
 * Links APIDoc temporary files
 *
 * @param array $pathList
 * @param string $filename
 * @param string $target
 *
 * @return null|string
 */
function linkApidocTmpfiles($pathList, $filename, $target){
    foreach ($pathList as $path){
        if (is_file($path.'/'.$filename)){
            if (!symlink($path.'/'.$filename, $target.'/'.$filename))
                error ('Cannot create symlink: '.$target.'/'.$filename);
            
            return $path.'/'.$filename;
        }
    }
    return null;
}

$path = null;

if(array_key_exists('p', $options))
    $path = $options['p'];

if(array_key_exists('path', $options))
    $path = $options['path'];

if(substr($path, -1) == '/')
    $path = substr($path, 0, -1);

if($path && substr($path, 0, 1) !== '/')
    $path = realpath($path);

$dav = null;

if(array_key_exists('d', $options))
    $dav = $options['d'];

if(array_key_exists('dav', $options))
    $dav = $options['dav'];

if($dav) {
    $dav_config_file = $dav;
    if(!file_exists($dav_config_file))
        error('Dav settings not found');
    
    include EKKO_ROOT.'/lib/core/webdav/WebDavClient.inc.php'; // To make constants available
    
    $dav = array();
    include $dav_config_file;
    
    if(!is_array($dav) || !count($dav) || !array_key_exists('base_url', $dav))
        error('Dav settings missing base_url');
}

if(!$path && !$dav)
    error('Must either output to path or push through webdav');

$tmp_path = false;
if(!$path) {
    $path = realpath(sys_get_temp_dir()).'/applicationbase_docgen_'.uniqid();
    $tmp_path = true;
}

$type = null;

if(array_key_exists('t', $options))
    $type = $options['t'];

if(array_key_exists('type', $options))
    $type = $options['type'];

if(!$type)
    $type = 'all';

if(!preg_match('`^(api|php|dev|all)(,(api|php|dev|all))*$`', $type))
    error('Bad type');

$type = explode(',', $type);
if(in_array('all', $type))
    $type = array('api', 'php', 'dev');

// Check utilities

$apidoc = in_array('api', $type) ? checkUtil('apidoc', $options) : null;

$phpdoc = in_array('php', $type) ? checkUtil('phpdoc', $options) : null;


if($tmp_path) {
    msg('Temp directory : '.$path);
} else {
    msg('Target directory : '.$path);
}

if($dav)
    msg('Webdav upload : '.$dav['base_url']);

if(!is_dir($path)) {
    verbose('Directory '.$path.' does not exists, creating');
    mkpath($path);
}

if(in_array('api', $type)) {
    msg('Generating API documentation ...');
    
    // Creating tmp folder
    $tmpFolder = EKKO_ROOT.'/tmp/apidocfiles_'.uniqid();
    if (!mkdir($tmpFolder))
        error('Cannot create '.$tmpFolder);
    
    $target = $path.'/api';
    if(is_dir($target))
        run('rm -fr "'.$target.'"');
    
    mkpath($target);
    
    $cmd = $apidoc.' -f ".class.php" -f ".doc.php" -o "'.$target.'"';
    
    $content = false;
    foreach(array(
                EKKO_ROOT.'/classes/core/endpoints/',
                EKKO_ROOT.'/classes/endpoints/',
                EKKO_ROOT.'/doc/api/core/',
                EKKO_ROOT.'/doc/api/',
            ) as $input) {
        if(count(findFiles($input, '\.(class|doc)\.php$'))) {
            $cmd .= ' -i "'.$input.'"';
            $content = true;
        }
    }
    
    $files = array();
    $sources = array(
        EKKO_ROOT.'/doc/tools/generate/api',
        EKKO_ROOT.'/doc/tools/generate/core/api',
    );
    
    $configFile = linkApidocTmpfiles($sources, 'apidoc.json', $tmpFolder);
    if ($configFile){
        $files[] = $tmpFolder.'/apidoc.json';
        
        $cmd .= ' -c "'.$tmpFolder.'"';
        
        $config = json_decode(file_get_contents($configFile));
        
        if (!$config || !is_object($config)) error ('Cannot use file apidoc.json');
        
        foreach (array('header','footer') as $f){
            if (property_exists($config, $f) && property_exists($config->$f,'filename')){
                if (strpos($config->$f->filename, '/') !== false) error('Filename cannot be a path.');
                
                $tpl = linkApidocTmpfiles($sources, $config->$f->filename, $tmpFolder);
                if (!$tpl)
                    error('File '.$config->$f->filename.' not found.');
                
                $files[] = $tmpFolder.'/'.$config->$f->filename;
            }
        }
    }
    
    $tpl = EKKO_ROOT.'/doc/tools/generate/api/template/';
    if(!is_dir($tpl))
        $tpl = EKKO_ROOT.'/doc/tools/generate/core/api/template/';
    
    if(is_dir($tpl))
        $cmd .= ' -t "'.$tpl.'"';
    
    if($content) {
        run('cd '.  escapeshellarg($tmpFolder).' && '.$cmd);
        
        // Cleaning tmp files
        foreach ($files as $f) unlink($f);
        if (!rmdir($tmpFolder)) error('Cannot remove directory: '.$tmpFolder);
        
        msg('... done');
        
    } else {
        msg('... couldn\'t find API description files, didn\'t generate');
    }
}

if(in_array('php', $type)) {
    msg('Generating PHP documentation ...');
    
    $target = $path.'/php';
    if(is_dir($target))
        run('rm -fr "'.$target.'"');
    
    mkpath($target);
    
    $dirs = array('classes', 'includes', 'lib', 'plugins', 'scripts', 'templates', 'view');
    foreach($dirs as &$dir)
        $dir = EKKO_ROOT.'/'.$dir;
    
    $cmd = $phpdoc.' -d "'.implode(',', $dirs).'" -t "'.$target.'"';
    
    $args = array();
    foreach(array(
                EKKO_ROOT.'/doc/tools/generate/core/php/args.php',
                EKKO_ROOT.'/doc/tools/generate/php/args.php',
            ) as $args_file)
        if(file_exists($args_file))
            include $args_file;
    
    foreach($args as $k => $v) {
        if(substr($k, 0, 1) != '-')
            $k = '-'.$k;
        
        $k .= (substr($k, 0, 2) == '--') ? '=' : ' ';
        
        $cmd .= ' '.$k.'"'.str_replace('"', '\\"', $v).'"';
    }
    
    run($cmd);
    
    msg('... done');
}

if(in_array('dev', $type)) {
    msg('Generating developper documentation ...');
    
    $target = $path.'/dev';
    if(is_dir($target))
        run('rm -fr "'.$target.'"');
    
    mkpath($target);
    
    $base = EKKO_ROOT.'/doc/dev';
    $files = findFiles($base, '\.md$');
    
    require_once EKKO_ROOT.'/lib/core/markdown/Michelf/MarkdownExtra.inc.php';
    
    $tpl = null;
    foreach(array(
                EKKO_ROOT.'/doc/tools/generate/dev/template.html',
                EKKO_ROOT.'/doc/tools/generate/core/dev/template.html',
            ) as $file) {
        if(file_exists($file)) {
            $tpl = file_get_contents($file);
            break;
        }
    }
    
    if(!$tpl)
        error('Developper documentation template not found');
    
    $index = array();
    
    foreach($files as $file) {
        verbose('Converting '.$file.' to html');
        
        $id = substr($file, strlen($base) + 1, -3);
        
        $md = file_get_contents($file);
        if($md === false)
            error('Could not read '.$file);
        
        if(preg_match('`todo`i', $md))
            msg($file.' has a "todo" tag, still rendering but you should check it');
        
        $out = $target.substr($file, strlen($base), -3).'.html';
        $fp = substr(dirname($file), strlen($base) + 1);
        $depth = $fp ? count(explode('/', $fp)) : 0;
        $fp = $depth ? str_repeat('../', $depth) : './';
        
        mkpath(dirname($out));
        
        try {
            $html = \Michelf\MarkdownExtra::defaultTransform($md);
            if(!$html)
                error('Could not convert to html');
            
            $title = substr(basename($file), 0, -3);
            if(preg_match('`<h1>([^<]+)</h1>`', $html, $m))
                $title = $m[1];
            
            $index[substr($out, strlen($target) + 1)] = $title;
            
            $html = str_replace(array('{root}', '{id}', '{content}'), array($fp, $id, $html), $tpl);
            
        } catch(Exception $e) {
            error('Could not convert to html ('.get_class($e).') : '.$e->getMessage());
        }
        
        // Transform own links, collect missing pages
        $referenced = array();
        
        $html = preg_replace_callback('`href="([^"]+)\.md"`msU', function($m) use($file, &$referenced) {
            $referenced[] = realpath(dirname($file).'/'.$m[1].'.md');
            
            return 'href="'.$m[1].'.html"';
        }, $html);
        
        $html = preg_replace_callback('`href="([^#]+)\.md#([^"]+)"`msU', function($m) use($file, &$referenced) {
            $referenced[] = realpath(dirname($file).'/'.$m[1].'.md');
            
            return 'href="'.$m[1].'.html#"'.$m[2];
        }, $html);
        
        // Set anchors
        $html = preg_replace_callback('`<h([1-6])>([^<]+)</h[1-6]>`msU', function($m) {
            $id = preg_replace('`[^a-z0-9_-]`i', '_', strtolower($m[2]));
            $id = preg_replace('`_+`', '_', $id);
            $id = preg_replace('`^_+`', '', $id);
            $id = preg_replace('`_+$`', '', $id);
            return '<h'.$m[1].' id="'.$id.'">'.$m[2].'</h'.$m[1].'>';
        }, $html);
        
        if(!file_put_contents($out, $html))
            error('Could not write to '.$file);
    }
    
    $referenced = array_unique($referenced);
    sort($referenced);
    
    if(count($referenced)) {
        msg('Found '.count($referenced).' referenced files');
        
        $missing = array();
        foreach($referenced as $file) {
            if(!file_exists($file))
                $missing[] = $file;
        }
        
        if(count($missing)) {
            msg('Some seems to be missing :');
            
            foreach($missing as $file)
                msg("\t".$file);
            
        } else {
            msg('None seems to be missing');
        }
    }
    
    // Build index tree
    ksort($index);
    
    $tree = array();
    foreach($index as $file => $title) {
        $keys = explode('/', $file);
        
        $tg = &$tree;
        $p = '';
        while($key = array_shift($keys)) {
            $p .= ($p ? '/' : '').$key;
            
            if(substr($key, -5) == '.html')
                $key = substr($key, 0, -5);
            
            if(!array_key_exists($key, $tg)) {
                $tg[$key] = array('title' => ucfirst($key));
                
                if(substr($p, -5) == '.html')
                    $tg[$key]['path'] = $p;
            }
            
            $tg = &$tg[$key];
            
            if(count($keys)) {
                if(!array_key_exists('sub', $tg))
                    $tg['sub'] = array();
                
                $tg = &$tg['sub'];
                
            } else {
                $tg['id'] = substr($file, 0, -5);
            }
        }
        
        $tg['title'] = $title;
    }
    
    $file = $target.'/tree.json';
    if(!file_put_contents($file, json_encode($tree, JSON_PRETTY_PRINT)))
        error('Could not write to '.$file);
    
    
    // Add index
    $idx_src = null;
    foreach(array(
                EKKO_ROOT.'/doc/tools/generate/index.html',
                EKKO_ROOT.'/doc/tools/generate/dev/index.html',
                EKKO_ROOT.'/doc/tools/generate/core/dev/index.html',
            ) as $file) {
        if(file_exists($file)) {
            $idx_src = $file;
            break;
        }
    }
    
    if(!$idx_src)
        error('Index file not found');
    
    if(!copy($idx_src, $target.'/index.html'))
        error('Could not copy '.$idx_src.' to '.$target);
    
    msg('... done');
}

// Search and replace custom syntax
msg('Setting up crossdoc ...');
foreach($type as $t) {
    msg('Copy resources');
    // Copy common resources
    $res_srcs = array(
        EKKO_ROOT.'/doc/tools/generate/core/html_res/',
        EKKO_ROOT.'/doc/tools/generate/core/html_res/'.$t.'/',
        EKKO_ROOT.'/doc/tools/generate/html_res/',
        EKKO_ROOT.'/doc/tools/generate/html_res/'.$t.'/',
    );
    $res = array('js' => array(), 'css' => array());
    foreach($res_srcs as $res_src) {
        if(!is_dir($res_src)) continue;
        
        foreach(scandir($res_src) as $file) {
            if(!is_file($res_src.$file)) continue;
            
            $parts = explode('.', $file);
            $ext = array_pop($parts);
            $name = implode('.', $parts);
            
            if(!copy($res_src.$file, $path.'/'.$t.'/'.$file))
                error('Could not copy '.$res_src.$file.' to '.$path.'/'.$t.'/');
            
            if(array_key_exists($ext, $res))
                $res[$ext][] = $file;
        }
    }
    
    foreach(array_keys($res) as $k)
        sort($res[$k]);
    
    $files = findFiles($path.'/'.$t, '\.html?$');
    msg('Customize '.count($files).' files');
    
    foreach($files as $file) {
        $fp = substr(dirname($file), strlen($path.'/'.$t) + 1);
        $fp = $fp ? str_repeat('../', count(explode('/', $fp))) : './';
        
        $html = file_get_contents($file);
        if($html === false)
            error('Could not read '.$file);
        
        // Prepare resources
        $html_res = array();
        foreach($res['css'] as $f)
            $html_res[] = '<link rel="stylesheet" type="text/css" href="'.$fp.basename($f).'" />';
        
        // Add head if missing
        if(!preg_match('`</head>`', $html))
            $html = preg_replace('`(<body(?:\s[^>]*)?>)`msU', '<head></head>$1', $html);
        
        // Add resources
        $html = preg_replace('`</head>`', implode('', $html_res).'</head>', $html);
        
        // Replace custom syntax
        $html = preg_replace_callback('`\[([^]]+)\]\(@([^\)]+)\)`', function($m) use($fp) {
            $title = $m[1];
            $path = explode('/', $m[2], 2);
            $tg = array_shift($path);
            
            return '<a href="@'.$tg.'/'.implode('/', $path).'">'.htmlentities($title).'</a>';
        }, $html);
        
        // Prepare resources
        $html_res = array();
        foreach($res['js'] as $f)
            $html_res[] = '<script type="text/javascript" charset="utf-8" src="'.$fp.basename($f).'"></script>';
        
        // Add resources
        $html = preg_replace('`</body>`', implode('', $html_res).'</body>', $html);
        
        if(!file_put_contents($file, $html))
            error('Could not write to '.$file);
    }
}
msg('... done');

if($dav) {
    msg('Pushing through webdav ...');
    try {
        $client = new WebDavClient($dav);
        $client->debug = $verbose;
        
        foreach($type as $t) {
            // Delete target
            msg('Clean /'.$t);
            try {
                $client->delete('/'.$t);
            } catch(WebDavErrorException $e) {
                if($e->getCode() != 404)
                    throw $e;
            }
            
            // Send all files
            $files = findFiles($path.'/'.$t);
            $cnt = count($files);
            
            msg('Push '.$cnt.' files to /'.$t);
            
            $slice = max(1, floor($cnt / 20));
            
            $epths = array();
            
            $n = 0;
            foreach($files as $file) {
                $rel = substr($file, strlen($path));
                
                verbose('Push '.$file.' to '.$rel);
                
                $rp = explode('/', substr(dirname($rel), 1));
                $rrp = '';
                while($prt = array_shift($rp)) {
                    $rrp .= '/'.$prt;
                    
                    if(!in_array($rrp, $epths)) {
                        try {
                            $client->get($rrp);
                        } catch(WebDavErrorException $e) {
                            if($e->getCode() != 404)
                                throw $e;
                            
                            $client->mkcol($rrp);
                        }
                        
                        $epths[] = $rrp;
                    }
                }
                
                $client->put($file, $rel);
                $n++;
                
                if($n % $slice == 0)
                    msg('Pushed '.$n.'/'.$cnt);
            }
        }
        
        // Add global index
        $idx_src = null;
        foreach(array(
                    EKKO_ROOT.'/doc/tools/generate/index.html',
                    EKKO_ROOT.'/doc/tools/generate/core/index.html',
                ) as $file) {
            if(file_exists($file)) {
                $idx_src = $file;
                break;
            }
        }
        
        if(!$idx_src)
            error('Index file not found');
        
        $client->put($idx_src, '/index.html');
        
    } catch(Exception $e) {
        error($e->getMessage());
    }
    
    msg('... done');
}

if($tmp_path) {
    verbose('Remove temp directory');
    run('rm -fr '.$path);
}
