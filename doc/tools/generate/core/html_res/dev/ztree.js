jQuery(function() {
    var body = jQuery('body');
    var root = body.attr('data-root');
    var id = body.attr('data-id');
    
    var ctn = jQuery('<div id="crossdoc_tree" />').prependTo(body);
    
    if(id) {
        jQuery('<h1 />').text('Table of contents').appendTo(ctn);
        
        var level = 2;
        var ul = jQuery('<ul />').appendTo(ctn);
        jQuery('h2, h3, h4, h5, h6').each(function() {
            var h = $(this);
            var lvl = parseInt(h.prop('tagName').substr(1));
            var txt = h.text();
            
            if(lvl > level) {
                ul = jQuery('<ul />').appendTo(ul.find('li:last'));
            } else if(lvl < level) {
                ul = ul.parent().closest('ul');
            }
            
            level = lvl;
            
            var li = jQuery('<li />').appendTo(ul);
            jQuery('<a />').attr({
                href: '#' + h.attr('id')
            }).text(h.text()).appendTo(li);
        });
        
        jQuery('<hr />').appendTo(ctn);
    }
    
    jQuery('<h1 />').text('Developper documentation index').appendTo(ctn);
    
    var explore = function(tree, ctn) {
        var ul = jQuery('<ul />').appendTo(ctn);
        
        for(var k in tree) {
            var item = tree[k];
            
            var li = jQuery('<li />').appendTo(ul);
            
            if(item.path) {
                jQuery('<a />').attr({
                    href: root + item.path,
                    'data-id': item.id
                }).text(item.title).appendTo(li);
            } else {
                jQuery('<div />').text(item.title).appendTo(li);
            }
            
            if(item.sub)
                explore(item.sub, li);
        }
    };
    
    jQuery.getJSON(root + 'tree.json', function(tree) {
        explore(tree, ctn);
    });
    
    jQuery('a[href^="http"]').attr({target: '_blank'});
});
