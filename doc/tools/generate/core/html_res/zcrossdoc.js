(function() {
    var loader = function(jQuery) {
        if(!jQuery) return;

        var load = this;
        var callb = function(){
            var resolver = function(path){
                path = path.substr(5).split('.');

                var value = load.apiproject;

                while (value && path.length) {
                    var p = path.shift();
                    if (typeof value != 'object' || !(p in value)) {
                        return null;
                    } else {
                        value = value[p];
                    }
                }

                if (value && (typeof value != 'object'))  return value;
                return null;
            };


            $('body :contains("@api:")').contents().filter(function () {
                return this.nodeType == 3 && this.nodeValue.match(/@api:/);

            }).each(function () {
                this.nodeValue = this.nodeValue.replace(/@api:([a-z0-9_]+(?:\.[a-z0-9_]+)*)/gi, function (m) {
                    var value = resolver(m);

                    return value ? value : m.substr(1);
                });
            });

            $('a[href^="@api:"]').each(function () {
                var value = resolver(decodeURIComponent($(this).attr('href')));
                if (value)
                    $(this).attr({href: value});
            });

            $('[src^="@api:"]').each(function () {
                var value = resolver(decodeURIComponent($(this).attr('src')));
                if (value)
                    $(this).attr({src: value});
            });
        };

        if (this.apiproject) {
            callb();
        }else {
            $.getJSON('api_project.json', function (data) {
                load.apiproject = data;
                callb();
            });
        }

        // Setup crosslinks
        jQuery('a[href^="@"]').each(function() {
            var link = $(this);

            var m = link.attr('href').match(/^@([^\/]+)(\/(.*))?$/);

            var type = m[1];
            if (type.match(/^api:/)) return;

            var path = m[3];

            var here = window.location.pathname.substr(1).split('/');

            var project = here.shift();
            if(project == 'wiki')
                project = here.shift();

            var goto = null;

            if(type == 'api') {
                goto = '/' + project + '/api/#api-' + path.replace('/', '-');

            } else if(type == 'php') {
                goto = '/' + project + '/php/';

                var anchor = null;
                var m = path.match(/^([^#]+)#(.*)$/);
                if(m) {
                    path = m[1];
                    anchor = m[2];
                }

                if(path.match(/\.php$/)) {
                    // File
                    goto += 'files/' + path.replace('/', '.').replace(/\.php$/, '') + '.html';

                } else {
                    // Class
                    goto += 'classes/' + path + '.html';
                }

                if(anchor)
                    path += '#' + anchor;

            } else if(type == 'dev') {
                var anchor = null;
                var m = path.match(/^([^#]+)#(.*)$/);
                if(m) {
                    path = m[1];
                    anchor = m[2];
                }

                path = path.replace(/\.(md|htm)$/, '.html');
                if(!path.match(/\.html$/))
                    path += '.html';

                if(anchor)
                    path += '#' + anchor;

                goto = '/' + project + '/dev/' + path;

            } else if(type == 'wiki') {
                goto = '/wiki/' + project + '/' + path;

            } else {
                // Bogus link
                $(this).css({
                    'color': '#d30',
                    'text-decoration': 'none',
                    'border-bottom': '1px dashed',
                });
            }

            if(goto)
                link.attr({href: goto});
        });
    };

    if(typeof require !== 'undefined') {
        window.setInterval(function() {
            require(['jquery'], loader);
        }, 1000);

    } else if(jQuery) {
        loader(jQuery);
    }
})();
