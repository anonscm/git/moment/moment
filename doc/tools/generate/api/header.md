# Moment - Webservice

This is the REST webservice to manage surveys through Moment application.

----------


This webservice let you pass GET, POST PUT and DELETE calls.


## RestResponse


### Format

By default, every response is sent as a json tree.

You can specify the response format with the ** format ** query parameter:

    https://.../rest.php/survey?format=xml

At the moment, only **json** and **xml** format are supported.
