<?php

/**
 *     Moment - CorsPlugin.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * CORS plugin
 *
 * Allow to send CORS related headers.
 *
 * Configuration expects an array of origin-headers pairs like
 * $config['plugin']['cors'] = array(
 *     'https://foo.bar' => array(
 *         'methods' => array('GET', 'POST', 'PUT'),
 *         'headers' => array('X-MyApp-Something')
 *     ),
 *     '*' => array(
 *         'methods' => array('GET')
 *     )
 * );
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
 */
class CorsPlugin extends Plugin {
    /**
     * @var array Request headers cache
     */
    private static $headers = null;

    /**
     * Initialization method
     */
    public static function initialize() {
        Event::register(Event::BEFORE, 'rest_request', function() {
            // Get origin
            $origin = self::getHeader('Origin');
            if(!$origin) return;

            $cfg = self::getConfig();
            if(!is_array($cfg)) return; // No config, cannot act

            if(array_key_exists($origin, $cfg)) {
                // Specific config
                $cfg = $cfg[$origin];

            } else if(array_key_exists('*', $cfg)) {
                // Wildcard match
                $cfg = $cfg['*'];
                $origin = '*';

            } else {
                // Not handled
                return;
            }

            // Origin is allowed, tell about it
            header('Access-Control-Allow-Origin: '.$origin);

            if(RestRequest::getMethod() === 'options') {
                // Preflight request, add allowed methods and headers

                $methods = array_key_exists('methods', $cfg) ? implode(', ', array_map('strtoupper', $cfg['methods'])) : '';
                if($methods !== '')
                    header('Access-Control-Allow-Methods: '.$methods);

                $headers = array_key_exists('headers', $cfg) ? implode(', ', array_map('strtoupper', $cfg['headers'])) : '';
                if($headers !== '')
                    header('Access-Control-Allow-Headers: '.$headers);
            }
        });
    }

    /**
     * Get request header value
     *
     * @param string $name
     * @return string|null
     */
    private static function getHeader($name) {
        if(is_null(self::$headers)) {
            if(function_exists('getallheaders')) {
                self::$headers = getallheaders();

            } else {
                self::$headers = array();
                foreach ($_SERVER as $k => $v) {
                    if (substr($k, 0, 5) !== 'HTTP_') continue;

                    $k = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($k, 5)))));

                    self::$headers[$k] = $v;
                }
            }
        }

        return array_key_exists($name, self::$headers) ? self::$headers[$name] : null;
    }
}
