<?php $id = GUI::getPage(null, 2) ?>

<section class="row" data-plugin-tracer>
    <select onchange="app.ui.redirect('plugin/tracer/' + $(this).val())">
        <option value="">--- Choose ---</option>
        
        <?php foreach(TracerPlugin::getTraces() as $k => $d) { ?>
            <option value="<?php echo $k ?>" <?php if($k == $id) echo 'selected="selected"' ?>>
                <?php echo htmlentities($d->context) ?> <?php echo Utilities::formatDate($d->time, true) ?>
            </option>
        <?php } ?>
    </select>
</section>

<?php if($id) { ?>
<script type="text/javascript">var trace = <?php echo TracerPlugin::getTrace($id) ?>;</script>
<?php } ?>

<script type="text/javascript">
$(function() {
    if(!trace) return;
    
    var ctn = $('<div />').appendTo($('[data-plugin-tracer]').css({padding: '1em'}));
    
    var crawl = function(what, ctn) {
        ctn = $('<div class="call" />').css({margin: '0.2em'}).appendTo(ctn);
        
        $('<strong />').text(what.call + '(' + what.args.join(', ') + ')').attr({
            title: what.file
        }).appendTo(ctn);
        
        $('<small />').css({'margin-left': '0.5em'}).text((what.time * 1000).toFixed(1) + 'ms / ' + (what.memory / 1024).toFixed(2) + 'kb').appendTo(ctn);
        
        var s = false, k;
        for(k in what.sub) s = true;
        if(!s) return;
        
        ctn.css({cursor: 'pointer'});
        
        ctn = $('<div class="sub" />').css({'margin-left': '2em'}).hide().appendTo(ctn);
        
        for(k in what.sub)
            crawl(what.sub[k], ctn);
    };
    
    crawl(trace.trace, ctn);
    
    ctn.find('.call').on('click', function(e) {
        $(this).find('> .sub').toggle();
        e.stopPropagation();
    });
});
</script>
