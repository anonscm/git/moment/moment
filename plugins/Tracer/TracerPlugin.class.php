<?php

/**
 *     Moment - TracerPlugin.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Tracer plugin
 */
class TracerPlugin extends Plugin {
    /**
     * @var array known traces
     */
    private static $traces = null;
    
    /**
     * Install method
     */
    public static function install() {}
    
    /**
     * Update method
     */
    public static function update() {}
    
    /**
     * Uninstall method
     */
    public static function uninstall() {}
    
    /**
     * Initialization method
     */
    public static function initialize() {
        $path = self::getConfig('path');
        
        if(!$path) {
            self::log(LogLevels::WARN, 'Tracer plugin enabled but path not configured');
            return;
        }
        
        FileIO::makePath($path);
        
        $view = (GUI::getPage(null, 0) == 'plugin') && (GUI::getPage(null, 1) == 'tracer');
        
        // Trace calls
        if(!$view) Event::register(Event::AFTER, 'init_done', function($event) use($path) {
            require_once EKKO_ROOT.'/lib/core/tracer/Tracer.class.php';
            
            Tracer::setup(array(
                'path' => $path,
                'skip' => self::getConfig('skip')
            ));
        });
    }
    
    /**
     * Get traces
     * 
     * @return array
     */
    public static function getTraces() {
        if(is_null(self::$traces)) {
            $idx = self::getConfig('path').'/index.json';
            if(!file_exists($idx))
                return array();
            
            self::$traces = array_reverse((array)json_decode(file_get_contents($idx)));
        }
        
        return self::$traces;
    }
    
    /**
     * Get JSON trace from id
     * 
     * @param string $id
     * 
     * @return string
     */
    public static function getTrace($id) {
        if(!array_key_exists($id, self::getTraces()))
            return 'null';
        
        return file_get_contents(self::getConfig('path').'/'.$id.'.json');
    }
}
