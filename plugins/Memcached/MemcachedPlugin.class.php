<?php

/**
 *     Moment - MemcachedPlugin.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Memcached plugin
 */
class MemcachedPlugin extends Plugin {
    /**
     * Install method
     */
    public static function install() {}
    
    /**
     * Update method
     */
    public static function update() {}
    
    /**
     * Uninstall method
     */
    public static function uninstall() {}
    
    /**
     * Initialization method
     */
    public static function initialize() {
        require_once EKKO_ROOT.'/lib/core/memcached/MemcachedCache.class.php';
        
        MemcachedCache::open(self::getConfig());
        
        Event::register(Event::BEFORE, 'cache_exists', function(Event $event) {
            $event->stopPropagation();
            $event->preventDefault();
            
            $event->result = MemcachedCache::exists($event->data[0]);
        });
        
        Event::register(Event::BEFORE, 'cache_index', function(Event $event) {
            $event->stopPropagation();
            $event->preventDefault();
            
            $event->result = MemcachedCache::index($event->data[0]);
        });
        
        Event::register(Event::BEFORE, 'cache_get', function(Event $event) {
            $event->stopPropagation();
            $event->preventDefault();
            
            $event->result = MemcachedCache::get($event->data[0]);
        });
        
        Event::register(Event::BEFORE, 'cache_match', function(Event $event) {
            $event->stopPropagation();
            $event->preventDefault();
            
            $event->result = MemcachedCache::match($event->data[0]);
        });
        
        Event::register(Event::BEFORE, 'cache_set', function(Event $event) {
            $event->stopPropagation();
            $event->preventDefault();
            
            MemcachedCache::set($event->data[0], $event->data[1]);
        });
        
        Event::register(Event::BEFORE, 'cache_drop', function(Event $event) {
            $event->stopPropagation();
            $event->preventDefault();
            
            MemcachedCache::drop($event->data[0]);
        });
    }
}
