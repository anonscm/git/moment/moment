<?php

$config['version'] = '<%= $version %>';

// ---------------------------------------------------------------------------
// DB config
// ---------------------------------------------------------------------------
$config['db'] = array(
    'type' => 'mysql',
    'host' => '<%= $host %>',
    'port' => '<%= $port %>',
    'database' => '<%= $db_name %>',
    'username' => '<%= $db_user %>',
    'password' => '<%= $db_pass %>',
    'charset' => 'utf8',
);


// ---------------------------------------------------------------------------
// APP config
// ---------------------------------------------------------------------------

$config['application_name'] = 'Evento';
$config['application_url'] = '<%= $app_url %>';
$config['landing_page'] = function () {
    return isset($_SERVER['PATH_INFO']) && !strpos($_SERVER['PATH_INFO'], 'logout')?implode('/', (array) $_SERVER['PATH_INFO']):'home';
};

// Unix uid of user apache (@see /etc/passwd)
$config['application_uid'] = 33;
$config['application_gid'] = 33;

//UID of the administrator
$config['admin'] = array();

//Emails of the administrators (separated by commas)
$config['admin_email'] = '<admin-email>';

$config['debug'] = false;


// ---------------------------------------------------------------------------
// Authentication settings (see doc/core/authentication.md)
// ---------------------------------------------------------------------------

$config['auth_sp'] = array(
    'type' => 'shibboleth',
    'login_url' => $config['application_url'].'/Shibboleth.sso/Login?target={target}',
    'logout_url' => $config['application_url'].'/Shibboleth.sso/Logout?return={target}',
    'attributes' =>
        array(
            'uid' => 'HTTP_EPPN',
            'emails' => 'HTTP_MAIL',
            'name' => array('HTTP_DISPLAYNAME', 'HTTP_CN'),
            'idp' => 'HTTP_SHIB_IDENTITY_PROVIDER'
        ),
);

$config['use_wayf'] = false;
$config['wayf_url'] = '<%= $wayf_url %>';

// ---------------------------------------------------------------------------
// Business configs
// ---------------------------------------------------------------------------

// Conservation of survey duration (in month)
$config['conservation_duration'] = 6;

// Max duration of opened state (in month)
$config['max_opened_duration'] = 12;

// Key for referentiel SI
$config['referentielsi'] = array(
    'url' => '<%= $referentielsi_url %>',
    'app' => '<%= $referentielsi_app %>',
    'secret' => '<%= $referentielsi_key %>'
);

// List of weekly stats recipients
$config['stats'] = array(
    'recipients' => array('<%= $stats_recipients %>')
);

//User guide
$config['user_guide'] = array(
    'enabled' => true,
    'type' => 'web_scrapping',
    'path' => '<%= $user_guide_path %>',
    'guide_path' => '/groupware/evento/guide_utilisateur/',
    'home' => 'index',
    'toc' => 'sommaire',
    'pages' => array(
        'index',
        'sommaire',
        'se_connecter_a_evento',
        'creer_un_evento',
        'gerer_les_eventos',
        'repondre_a_un_evento',
        'resultats_d_un_evento',
        'calendars'
    ),
    'use_cache' => true
);

// MOTD
$config['motd'] = array(
    'enabled' => true,
    'url' => '<%= $motd_url %>',
    'endpoint' => 'evento',
);

$config['must_accept_terms'] = true;

// Calendar management
$config['calendar'] = array(
    'timeout' => 5, // Timeout in seconds
    'max_size' => 2000000, // 2MB,
    'status_validity_duration' => 60 // In minutes
);

$config['timezone_pivot'] = 1578564396;