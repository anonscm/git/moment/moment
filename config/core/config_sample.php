<?php
/**
 *     Moment - config_sample.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * This is a sample configuration file for the ApplicationBase
 * 
 * Want to provide your own sample configuration file ? Just create it under /config/config_sample.php
 * 
 */

// For an ApplicationBase based application to work you will need at least the parameters below

// General settings

// Full application root URL, including scheme and domain
$config['application_url'] = 'http://localhost/applicationbase/';

// Application name
$config['application_name'] = 'App Base';

// UIDs (see doc/core/authentication.md) of admin users
$config['admin'] = 'foo@bar.tld';

// Address(es) the system notifications will be sent to
$config['admin_email'] = array('foo@bar.tld', 'support@mydomain.tld');

// Address to use as Reply-To when sending emails to anybody
$config['email']['reply_to'] = 'noreply@renater.fr';

// Database config (see doc/core/database.md)
$config['db'] = array(
    'type' => 'mysql',
    'host' => 'localhost',
    'database' => 'applicationbase',
    //'port' => '3306',
    'username' => 'applicationbase',
    'password' => 'applicationbase'
);

// Authentication settings (see doc/core/authentication.md)
$config['auth_sp'] = array(
    'type' => 'shibboleth',
    'uid_attribute' => 'mail',
    'email_attribute' => 'mail',
    'name_attribute' => 'cn',
    'login_url' => '/Shibboleth.sso/Login?target={target}',
    'logout_url' => '/Shibboleth.sso/Logout?return={target}'
);

// Logging (see doc/core/logging.md)
$config['log_facilities'] = array(
    array(
        'type' => 'file',
        'path' => EKKO_ROOT.'/logs/',
        'rotate' => 'daily',
        'level' => 'info'
    )
);

// UID under which the application will run, most likely your webserver's
$config['application_uid'] = 0;

// GID under which the application will run, most likely your webserver's (optional)
//$config['application_gid'] = 0;
