<?php
/**
 *     Moment - CoreUtilitiesTest.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Test core utilities features
 */
class CoreUtilitiesTest extends UnittestCase {
    /**
     * Test is isValidUID is behaving as expected
     */
    public function testIsValidUID() {
        $this->assertFalse(Utilities::isValidUID(null));
        $this->assertFalse(Utilities::isValidUID(true));
        $this->assertFalse(Utilities::isValidUID(array()));
        $this->assertFalse(Utilities::isValidUID((object)array()));
        $this->assertFalse(Utilities::isValidUID('a1b2c3d4-e5f6-g7h8-i9j0-a1b2c3d4e5f'));

        $this->assertTrue(Utilities::isValidUID('a1b2c3d4-e5f6-g7h8-i9j0-a1b2c3d4e5f6'));
    }

    /**
     * Test byte size parser
     */
    public function testSizeToBytes() {
        $bytes = Utilities::sizeToBytes('3');
        $this->assertEquals($bytes, 3);

        $bytes = Utilities::sizeToBytes('1k');
        $this->assertEquals($bytes, 1024);

        $bytes = Utilities::sizeToBytes('2M');
        $this->assertEquals($bytes, 2 * 1024 * 1024);

        $bytes = Utilities::sizeToBytes('5G');
        $this->assertEquals($bytes, 5 * 1024 * 1024 * 1024);
    }
}