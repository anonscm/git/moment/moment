<?php
/**
 *     Moment - config.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
$config['email']['enabled'] = false;

/**
 * Defining a fake DB in memory sqlite
 *
 * Note: It's possible to use a file
 */
if (defined('FAKE_DB')) {
    // Database config (see doc/core/database.md)
    $config['db'] = array(
        'dsn' => 'sqlite::memory:',
        'type' => 'test',
        'host' => '127.0.0.1',
        'database' => 'moment_dev',
        'port' => '3306',
        'username' => 'root',
        'password' => 'root',
        'charset' => '' // We can't set it to utf8 in SQLite for now because
        // core class DBI set charset is not compatible with
    );

};

