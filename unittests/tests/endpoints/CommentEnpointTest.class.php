<?php
/**
 *     Moment - CommentEnpointTest.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
require_once EKKO_ROOT . '/lib/client/core/ApplicationRestClient.class.php';

/**
 * Class CommentEndpointTest
 *
 * Testing endpoint from client side, app should be launched.
 *
 * Simulates a client remote application
 */
class CommentEndpointTest extends UnittestCase {

    private static $comment = null;

    private static $participant = null;

    private static $client = null;
    private static $creds = null;

    public static function setUpBeforeClass() {

        SurveyTest::setUpBeforeClass();

        self::$client =
            new ApplicationRestClient(
                Config::get('application_url'),
                new ApplicationRestClientAuth(array('remote_application' => 'moment'), 'a367e604914e9f12584598462c9bdd31')
            );
        //$user = Auth::user();
        self::$creds = array('XDEBUG_SESSION_START' => 17109, 'remote_user' => array('uid' => 'test@test.com', 'email' => array('test@test.com')));


        //Initializing comment
        self::$comment = array(
            'survey_id' => SurveyTest::$surveyTest->id,
            'participant' => array(
                "email" => "florian.bruneau-voisine@renater.fr",
                "name" => "test"
            ),
            'content' => 'This is a test comment'
        );

    }

    public static function tearDownAfterClass() {

        SurveyTest::tearDownAfterClass();
    }


    public function testGet() {

        //GET is not allowed, so we should have a "rest_not_allowed" message
        try {
            self::$client->get('comment');
        } catch (ApplicationRestClientException $e) {
            if (property_exists($e, 'message')) {
                $this->assertEquals("rest_not_allowed", $e->getMessage());

                //Test is finished
                return;
            }

        }
        $this->fail('We should not be here, no exception ApplicationRestClientException has been thrown');
    }


    public function testPost() {

        try {
            $response = self::$client->post('comment', self::$creds, self::$comment);

            //Body should at least contain one comment (The one we just created)
            $this->assertNotEquals(reset($response->body->comments), FALSE);

            //Storing participant for further use (e.g PUT)
            self::$participant = reset($response->body->comments)->participant;

            //We must get the token for further interaction with this survey
            $this->assertNotNull(self::$participant->participant_token);

            //Comparing content of the comment
            $this->assertEquals(self::$comment['content'], reset($response->body->comments)->content);

            $this->displayInfo(__FUNCTION__, self::$participant->participant_token);
        } catch (ApplicationRestClientException $e) {
            $this->fail($e);
        }

    }

    public function testPut() {

        //PUT is not allowed, so we should have a "rest_not_allowed" message
        try {
            self::$client->put('comment', self::$creds, self::$comment);
        } catch (ApplicationRestClientException $e) {

            if (property_exists($e, 'message')) {
                $this->assertEquals("rest_not_allowed", $e->getMessage());

                //Test is finished
                return;
            }

        }
        $this->fail('We should not be here, no exception ApplicationRestClientException has been thrown');

    }

    public function testDelete() {

        //Note we connect as a remote application to be trusted (App must be launched)
        //DELETE is not allowed, so we should have a "rest_not_allowed" message
        try {
            self::$client->delete('answer', self::$creds);
        } catch (ApplicationRestClientException $e) {

            if (property_exists($e, 'message')) {
                $this->assertEquals('rest_not_allowed', $e->getMessage());

                //Test is finished
                return;
            }
            //Exception can't be parsed
            $this->displayError(__FUNCTION__, $e->getDetails());
            throw $e;
        }
        $this->fail('We should not be here, no exception ApplicationRestClientException has been thrown');
    }


}