<?php
/**
 *     Moment - AnswerEnpointTest.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
require_once EKKO_ROOT . '/lib/client/core/ApplicationRestClient.class.php';

/**
 * Class AnswerEndpointTest
 *
 * Testing endpoint from client side, app should be launched.
 *
 * Simulates a client remote application
 */
class AnswerEndpointTest extends UnittestCase {

    private static $answers = null;

    private static $participant = null;

    private static $client = null;
    private static $creds = null;

    public static function setUpBeforeClass() {

        SurveyTest::setUpBeforeClass();

        self::$client =
            new ApplicationRestClient(
                Config::get('application_url'),
                new ApplicationRestClientAuth(array('remote_application' => 'moment'), 'a367e604914e9f12584598462c9bdd31')
            );
        //$user = Auth::user();
        self::$creds = array('XDEBUG_SESSION_START' => 16499, 'remote_user' => array('uid' => 'test@test.com', 'emails' => array('test@test.com'), 'name' => 'test'));


        //Initializing answers
        self::$answers = array();

        //We get a question of the survey to prepare an answer
        $questions = SurveyTest::$surveyTest->Questions;

        //Even if we are a remote application authenticated as a real
        foreach ($questions as $question) {
            $answer = array(
                "participant" => array(
                    "email" => "florian.bruneau-voisine@renater.fr",
                    "name" => "test"
                ),
                'question_id' => $question->id,
                'choices' => array()
            );

            $chosen = false;
            foreach ($question->propositions as $proposition) {
                $answer['choices'][] = array(
                    'proposition_id' => $proposition->id,
                    'value' => ($chosen) ? YesNoMaybeValues::NO : YesNoMaybeValues::YES
                );
                $chosen = true;
            }

            self::$answers[] = $answer;
        }

    }

    public static function tearDownAfterClass() {

        SurveyTest::tearDownAfterClass();
    }


    public function testGet() {

        //GET is not allowed, so we should have a "rest_not_allowed" message
        try {
            self::$client->get('answer');
        } catch (ApplicationRestClientException $e) {

            if (property_exists($e, 'message')) {
                $this->assertEquals("rest_not_allowed", $e->getMessage());

                //Test is finished
                return;
            }

        }
        $this->fail('We should not be here, no exception ApplicationRestClientException has been thrown');
    }


    public function testPost() {

        try {
            $response = self::$client->post('answer', self::$creds, self::$answers);

            //Body should at least contain one answer
            $this->assertNotEquals(reset($response->body->answers), FALSE);

            //Storing participant token for further use (e.g PUT)
            self::$participant = reset($response->body->answers)->participant;

            $this->assertNotNull(self::$participant->participant_token);

            self::$answers = json_decode(json_encode($response->body->answers), true);

            $this->displayInfo(__FUNCTION__, self::$participant->participant_token);
        } catch (ApplicationRestClientException $e) {
            $this->fail($e);
        }

    }

    public function testPut() {

        //Updating an answer (Setting the last proposition to yes)
        foreach (self::$answers as $key => &$answer) {
            foreach ($answer['choices'] as $choice_key => &$choice) {
                $choice['value'] = YesNoMaybeValues::NO;
            }

            //We set the last choice
            end($answer['choices']);
            $answer['choices'][key($answer['choices'])]['value'] = YesNoMaybeValues::YES;
        }
        //We set the participant token in the first answer
        reset(self::$answers);
        self::$answers[key(self::$answers)]['participant'] = array(
            'participant_token' => self::$participant->participant_token
        );


        try {
            $response = self::$client->put('answer/', self::$creds, self::$answers);

            $this->assertNotNull($response->body->answers);

            //Comparing $answers
            foreach (self::$answers as $answer) {
                //Searching matching answer in received $answer
                foreach ($response->body->answers as $recieved_answer) {
                    if ($answer['question_id'] == $recieved_answer->question_id) {
                        foreach ($answer['choices'] as $choice_key => $choice) {
                            //Searching matching choice
                            foreach ($recieved_answer->choices as $recieved_choice) {
                                if ($choice['proposition_id'] == $recieved_choice->proposition_id) {
                                    $this->assertEquals($choice['value'], $recieved_choice->value);
                                }
                            }

                        }
                    }
                }

            }


            $this->displayInfo(__FUNCTION__, self::$participant->participant_token);
        } catch (ApplicationRestClientException $e) {
            $this->fail($e->getDetails());
        }


    }

    public function testDelete() {

        //Note we connect as a remote application to be trusted (App must be launched)
        //DELETE is not allowed, so we should have a "rest_not_allowed" message
        try {
            self::$client->delete('answer', self::$creds);
        } catch (ApplicationRestClientException $e) {
            if ($e && property_exists($e, 'message')) {
                $this->assertEquals('rest_not_allowed', $e->getMessage());

                //Test is finished
                return;
            }
            //Exception can't be parsed
            $this->displayError(__FUNCTION__, $e->getDetails());
            throw $e;
        }
        $this->fail('We should not be here, no exception ApplicationRestClientException has been thrown');
    }


}