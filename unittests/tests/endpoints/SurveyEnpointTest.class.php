<?php
/**
 *     Moment - SurveyEnpointTest.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
require_once EKKO_ROOT . '/lib/client/core/ApplicationRestClient.class.php';

/**
 * Class SurveyEndpointTest
 *
 * Testing endpoint from client side, app should be launched.
 *
 * Simulates a client remote application
 */
class SurveyEndpointTest extends UnittestCase {

    private static $client = null;
    private static $creds = null;

    private static $survey = null;

    public static function setUpBeforeClass() {

        //We create a survey (For GET and PUT method)
        SurveyTest::setUpBeforeClass();

        self::$client =
            new ApplicationRestClient(
                Config::get('application_url'),
                new ApplicationRestClientAuth(array('remote_application' => 'moment'), 'a367e604914e9f12584598462c9bdd31')
            );
        //$user = Auth::user();
        self::$creds = array('XDEBUG_SESSION_START' => 18615, 'remote_user' => array('uid' => 'test_owner@test.com', 'emails' => array('test_owner@test.com'), 'name' => 'test'));

    }

    public static function tearDownAfterClass() {
        SurveyTest::tearDownAfterClass();
    }

    /**
     * Test GET one survey by its id
     */
    public function testGetOne() {

        try {
            $response = self::$client->get('survey/' . SurveyTest::$surveyTest->id, self::$creds);

            $this->assertNotNull($response->body);

            $this->assertTrue(is_array($response->body));

            //We should only have at most one survey
            $this->assertEquals(1, count($response->body));

            $survey = reset($response->body);

            //Comparing values
            $this->assertEquals($survey->title, SurveyTest::$goodSurveyData['title']);
            $this->assertEquals($survey->place, SurveyTest::$goodSurveyData['place']);
            $this->assertEquals($survey->description, SurveyTest::$goodSurveyData['description']);
            $this->assertEquals(count((array)$survey->settings), count(SurveyTest::$goodSurveyData['settings']));
            foreach ((array)$survey->settings as $key => $value) {
                $this->assertEquals($value, SurveyTest::$goodSurveyData['settings'][$key]);
            }
        } catch (ApplicationRestClientException $e) {
            $this->fail($e->getDetails());
        }
    }

    /**
     * Test GET all survey owned by the test user
     */
    public function testGetSurveysOfUser() {

        //In thoses tests the owner is test_owner@test.com
        try {
            $response = self::$client->get('survey', self::$creds);

            $this->assertNotNull($response->body);

            $this->assertTrue(is_array($response->body));

            $found = false;
            foreach ($response->body as $survey) {
                $owns = false;
                foreach ($survey->owners as $owner) {
                    $owns = $owns || $owner->email == 'test_owner@test.com';
                }
                $this->assertTrue($owns);

                //We should at least find the one we just created in setUp
                if ($survey->id === SurveyTest::$surveyTest->id)
                    $found = true;
            }

            //Survey test must be in the list
            $this->assertTrue($found);

            $this->displayInfo(__FUNCTION__, null);

        } catch (ApplicationRestClientException $e) {
            $this->fail($e->getDetails());
        }
    }

    /**
     * Test GET all survey where user is invited
     */
    public function testGetSurveysUserIsInvited() {

        //In thoses tests the owner is test_owner@test.com
        //He is also guest of the survey
        try {
            $response = self::$client->get('survey/@guest', self::$creds);

            $this->assertNotNull($response->body);

            $this->assertTrue(is_array($response->body));

            $found = false;
            foreach ($response->body as $survey) {

                //Note only owner can see guests, for tests that's why owner is also guest
                $this->assertTrue(is_array($survey->guests));
                $this->assertTrue(count($survey->guests) > 0);

                $guest_found = false;
                foreach ($survey->guests as $guest) {
                    if ($guest->email === 'test_owner@test.com') {
                        $guest_found = true;
                        //We have found the matching mail in guest list
                        break;
                    }
                }
                //Guest mail must be in the list
                $this->assertTrue($guest_found);


                //We should at least find the one we just created in setUp
                if ($survey->id === SurveyTest::$surveyTest->id)
                    $found = true;
            }

            //Survey test must be in the list
            $this->assertTrue($found);

            $this->displayInfo(__FUNCTION__, null);

        } catch (ApplicationRestClientException $e) {
            $this->fail($e);
        }
    }

    /**
     * Test GET all survey where user is invited
     */
    public function testGetSurveysUserHasAnswered() {

        //In thoses tests the owner is test_owner@test.com
        //He is also guest of the survey
        try {
            $response = self::$client->get('survey/@answered', self::$creds);

            $this->assertNotNull($response->body);

            $this->assertTrue(is_array($response->body));

            //We can't be sure there are surveys where user have answered
            //TODO insert an answer

            $this->displayInfo(__FUNCTION__, null);

        } catch (ApplicationRestClientException $e) {
            $this->fail($e);
        }
    }

    /**
     * Test GET all survey where user's attention is required (Not implemented yet)
     */
    public function testGetSurveysUsersAttentionIsRequired() {

        //@feed is not implemented yet
        try {
            self::$client->get('survey/@feed', self::$creds);
        } catch (ApplicationRestClientException $e) {

            if (property_exists($e, 'message')) {
                $this->assertEquals("rest_method_not_implemented", $e->getMessage());

                $this->displayInfo(__FUNCTION__, null);
                //Test is finished
                return;
            }
        }
        $this->fail('We should not be here, no exception ApplicationRestClientException has been thrown');
    }

    /**
     * Test POST method
     *
     */
    public function testPost() {

        try {
            $response = self::$client->post('survey', self::$creds, SurveyTest::$goodSurveyData);

            $this->assertNotNull($response->body->data);

            $survey = $response->body->data;

            $this->assertNotNull($survey->id);

            $this->assertEquals($survey->title, SurveyTest::$goodSurveyData['title']);
            $this->assertEquals($survey->place, SurveyTest::$goodSurveyData['place']);
            $this->assertEquals($survey->description, SurveyTest::$goodSurveyData['description']);
            $this->assertEquals(count((array)$survey->settings), count(SurveyTest::$goodSurveyData['settings']));
            foreach ((array)$survey->settings as $key => $value) {
                $this->assertEquals($value, SurveyTest::$goodSurveyData['settings'][$key]);
            }
            $this->assertEquals(count($survey->guests), 1);
            $this->assertEquals(count($survey->questions), 1);

            self::$survey = $survey;

            $this->displayInfo(__FUNCTION__, $survey->id);
        } catch (ApplicationRestClientException $e) {
            $this->fail($e);
        }

    }

    /**
     * Test PUT Edition of a survey
     */
    public function testPut() {

        $newSurveyData = json_decode(json_encode(self::$survey), true);

        if (empty($newSurveyData))
            return;

        //Changing title
        $newSurveyData['title'] = 'This is the new title';

        //Changing guest
        $newSurveyData['guests'] = array();
        $newSurveyData['guests'][] = array('email' => 'test_email@test.com');

        try {
            $response = self::$client->put('survey/' . $newSurveyData['id'], self::$creds, $newSurveyData);

            $this->assertNotNull($response->body->data);

            $survey = $response->body->data;

            $this->assertNotNull($survey->id);

            $this->assertEquals($survey->title, $newSurveyData['title']);
            $this->assertEquals($survey->place, $newSurveyData['place']);
            $this->assertEquals($survey->description, $newSurveyData['description']);
            $this->assertEquals(count((array)$survey->settings), count($newSurveyData['settings']));
            foreach ((array)$survey->settings as $key => $value) {
                $this->assertEquals($value, $newSurveyData['settings'][$key]);
            }
            //Check guests as been changed
            $this->assertEquals(count($survey->guests), 1);

            $guest = reset($survey->guests);
            $this->assertEquals('test_email@test.com', $guest->email);

            $this->assertEquals(count($survey->questions), 1);

            $this->displayInfo(__FUNCTION__, $survey->id);
        } catch (ApplicationRestClientException $e) {
            $this->fail($e);
        }

    }

    /**
     * Test DELETE of survey
     */
    public function testDelete() {

        try {
            $response = self::$client->delete('survey/' . SurveyTest::$surveyTest->id, self::$creds);

            $this->assertTrue($response->body);

            $this->displayInfo(__FUNCTION__, self::$survey->id);

        } catch (ApplicationRestClientException $e) {
            $this->fail($e);
        }

    }

}