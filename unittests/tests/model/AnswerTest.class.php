<?php
/**
 *     Moment - AnswerTest.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
class AnswerTest extends UnittestCase {

    private static $participant = null;

    private static $answer1 = null;

    public static function setUpBeforeClass() {
        SurveyTest::setUpBeforeClass();

        self::$participant = Participant::getParticipant(SurveyTest::$surveyTest,
            (array)json_decode(json_encode(array(
                "participant" => array(
                    "email" => "florian.bruneau-voisine@renater.fr",
                    "name" => "test"
                )
            ))));
    }

    public static function tearDownAfterClass() {
        if (self::$participant) {
            self::$participant->delete();
        }

        SurveyTest::tearDownAfterClass();
    }

    public function testCreate() {

        if (is_null(self::$answer1)) {
            //We get a question of the survey to prepare an answer
            $questions = SurveyTest::$surveyTest->Questions;
            $question = reset($questions);
            $propositions = $question->propositions;
            $proposition_1 = reset($propositions);

            //An answer is a set of choices for a question
            $answer_data = array(
                'question_id' => $question->id,
                'choices' => array(
                    array(
                        'proposition_id' => $proposition_1->id,
                        'value' => YesNoMaybeValues::YES
                    )
                )
            );

            //We create the answer
            self::$answer1 = Answer::create(self::$participant, $question, (array)json_decode(json_encode($answer_data)));


            //Checking answer has been correctly created
            $choiceFound = false;
            //Looking for choice made in answer
            foreach (self::$answer1->choices as $choice) {
                //We use $choice->proposition->id in place of $choice->proposition_id,
                // just to run the lazy loading of the proposition
                $choiceFound = $choiceFound || ($choice->proposition->id === $proposition_1->id);

                $this->assertEquals(self::$answer1->id, $choice->answer->id);
            }
            $this->assertTrue($choiceFound);
        }

        $this->displayInfo(__FUNCTION__, self::$answer1);

    }

    public function testUpdate() {

        if (is_null(self::$answer1)) {
            $this->testCreate();
        }

        //We get a question of the survey to prepare an answer
        $questions = SurveyTest::$surveyTest->Questions;
        $question = reset($questions);
        $propositions = $question->propositions;
        $proposition_1 = array_shift($propositions);
        $proposition_2 = array_shift($propositions);

        //An answer is a set of choices for a question
        $answer_data = array(
            'question_id' => $question->id,
            'choices' => array(
                array(
                    'proposition_id' => $proposition_1->id,
                    'value' => YesNoMaybeValues::NO
                ),
                array(
                    'proposition_id' => $proposition_2->id,
                    'value' => YesNoMaybeValues::YES
                )
            )
        );

        self::$answer1->update((array)json_decode(json_encode($answer_data)));

        //Checking new values of answer
        $this->assertEquals($answer_data['question_id'], self::$answer1->Question->id);
        $this->assertEquals(2, count(self::$answer1->choices));

        $answer1_data = Answer::cast(self::$answer1);
//        Logger::info($answer1_data);
//        Logger::info($answer_data);
        foreach ($answer_data as $key => $values) {
            if ($key === 'question_id')
                continue;

            $index = 0;

            foreach ($values as $value) {
                $this->assertEquals($answer1_data['choices'][$index]->proposition_id, $value['proposition_id']);
                $this->assertEquals($answer1_data['choices'][$index]->value, $value['value']);

                $index++;
            }
        }


        $this->displayInfo(__FUNCTION__, self::$answer1);

    }
}