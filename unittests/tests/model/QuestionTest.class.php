<?php
/**
 *     Moment - QuestionTest.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
class QuestionTest extends UnittestCase {

    private static $question1 = null;

    private static $question2 = null;

    private static $goodQuestionTextData = array(
        "title" => "Question 1",
        "type" => "text",
        "position" => 0,
        "options" => array(
            "force_unique_choice" => 0,
            "enable_maybe_choices" => 0
        ),
        "propositions" => array(
            array(
                "type" => "text",
                "position" => 0,
                "header" => "",
                "text" => "Proposition 1"
            ),
            array(
                "type" => "text",
                "position" => 1,
                "header" => "",
                "text" => "Proposition 2"
            )
        )
    );

    private static $goodQuestionDateData = array(
        "title" => "Question 2",
        "type" => "date",
        "position" => 1,
        "options" => array(
            "force_unique_choice" => 0,
            "enable_maybe_choices" => 0
        ),
        "propositions" => array(
            array(
                "type" => "date",
                "base_day" => 1472688000,
                "base_time" => 36000
            ),
            array(
                "type" => "date",
                "base_day" => 1477688000,
                "base_time" => 39600
            )
        )
    );

    public static function setUpBeforeClass() {
        SurveyTest::setUpBeforeClass();

        self::$question2 = Question::create(SurveyTest::$surveyTest,
            (array)json_decode(json_encode(self::$goodQuestionDateData)));

        self::$question1 = Question::create(SurveyTest::$surveyTest,
            (array)json_decode(json_encode(self::$goodQuestionTextData)));
    }

    public static function tearDownAfterClass() {
        if (self::$question2) {
            self::$question2->delete();
        }

        if (self::$question1) {
            self::$question1->delete();
        }


        SurveyTest::tearDownAfterClass();
    }

    public function testCreate() {
        $tmp1 = Question::create(SurveyTest::$surveyTest,
            (array)json_decode(json_encode(self::$goodQuestionDateData)));

        $this->assertNotNull($tmp1->id);
        $this->assertTrue($tmp1->storedInDatabase);

        $tmp1->dropRelated(SurveyTest::$surveyTest);

        $tmp2 = Question::create(SurveyTest::$surveyTest,
            (array)json_decode(json_encode(self::$goodQuestionTextData)));

        $this->assertNotNull($tmp2->id);
        $this->assertTrue($tmp2->storedInDatabase);

        $tmp2->dropRelated(SurveyTest::$surveyTest);

        $this->displayInfo(__FUNCTION__, $tmp1 . ' ' . $tmp2);

        $tmp1->delete();
        $tmp2->delete();

        $badQuestionData = self::$goodQuestionDateData;

        $this->setExpectedException(FormDataValidationException::class);
        $badQuestionData['options']['force_unique_choice'] = 'this is not valid for boolean value';
        Question::create(SurveyTest::$surveyTest,
            (array)json_decode(json_encode($badQuestionData)));

        $this->setExpectedException(FormDataValidationException::class);
        $badQuestionData['options']['force_unique_choice'] = 0;
        $badQuestionData['options']['enable_maybe_choices'] = 5;
        Question::create(SurveyTest::$surveyTest,
            (array)json_decode(json_encode($badQuestionData)));

    }

    public function testUpdate() {
        $tmp1 = Question::create(SurveyTest::$surveyTest,
            (array)json_decode(json_encode(self::$goodQuestionDateData)));

        $this->assertNotNull($tmp1->id);
        $this->assertTrue($tmp1->storedInDatabase);

        $newData = Question::cast($tmp1, false);
        $newData['title'] = "Question 2 (Mise à jour)";

        $newData['propositions'][] = array(
            "type" => "date",
            "base_day" => 1478688000,
            "base_time" => 39600,
            "options" => array()
        );

        $tmp1->update($tmp1->Survey, $newData);

        $this->assertTrue($tmp1->title === $newData['title']);

        //Checking the third propositions has really been added
        $this->assertEquals(3, count($tmp1->propositions));

        $this->displayInfo(__FUNCTION__, $tmp1);

        $tmp1->delete();

    }

    public function testFromSurvey() {

        $questions = Question::fromSurvey(SurveyTest::$surveyTest);

        $this->assertNotEmpty($questions);

        $question2Found = false;
        $question1Found = false;

        foreach ($questions as $question) {
            //Checking the survey linked to the question is the good one
            $this->assertEquals($question->Survey . '', SurveyTest::$surveyTest . '');

            $question1Found = ($question1Found || $question->id === self::$question1->id);
            $question2Found = ($question2Found || $question->id === self::$question2->id);
        }

        //Test questions must be present
        $this->assertTrue($question1Found);
        $this->assertTrue($question2Found);

        $this->displayInfo(__FUNCTION__, null);

    }

    /**
     * Tests the compare methods, comparison is only made on question position
     */
    public function testCompare() {

        //As question entity
        $question3Data = self::$goodQuestionDateData;
        $question3Data['position'] = 1;//Same as Q2

        $question3 = Question::create(SurveyTest::$surveyTest,
            (array)json_decode(json_encode($question3Data)));
        //Q3 is in the same position than Q2


        //question (Q1) is in position 0 so compare(Q1, Q2) should return -1 Q1 < Q2
        $this->assertEquals(-1, Question::compare(self::$question1, self::$question2));

        //question (Q1) is in position 0 so compare(Q2, Q1) should return 1 Q2 > Q1
        $this->assertEquals(1, Question::compare(self::$question2, self::$question1));

        //question (Q2) is in position 1 and Q3 too so compare(Q2, Q3) should return 0 Q2 == Q3
        $this->assertEquals(0, Question::compare(self::$question2, $question3));


        //As arrays
        $this->assertEquals(-1, Question::compare(Question::cast(self::$question1), Question::cast(self::$question2)));

        //question (Q1) is in position 0 so compare(Q2, Q1) should return 1 Q2 > Q1
        $this->assertEquals(1, Question::compare(Question::cast(self::$question2), Question::cast(self::$question1)));

        //question (Q2) is in position 1 and Q3 too so compare(Q2, Q3) should return 0 Q2 == Q3
        $this->assertEquals(0, Question::compare(Question::cast(self::$question2), Question::cast($question3)));

        $question3->delete();

        $this->displayInfo(__FUNCTION__, null);
    }
}