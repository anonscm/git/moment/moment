<?php
/**
 *     Moment - ParticipantTest.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
class ParticipantTest extends UnittestCase {

    private static $participantDeclarative = null;
    private static $participantAuthenticated = null;

    private static $participant1Data = array(
        "participant" => array(
            "email" => "test@test.com",
            "name" => "test"
        )
    );

    public static function setUpBeforeClass() {
        //We need a survey for tests
        SurveyTest::setUpBeforeClass();
    }

    public static function tearDownAfterClass() {
        if (self::$participantDeclarative) {
            self::$participantDeclarative->delete();
        }

        if (self::$participantAuthenticated) {
            self::$participantAuthenticated->delete();
        }

        SurveyTest::tearDownAfterClass();
    }

    public function testGetDeclarativeParticipant() {

        //Get declarative participant with a name and email
        if (is_null(self::$participantDeclarative)) {
            self::$participantDeclarative = Participant::getParticipant(SurveyTest::$surveyTest,
                (array)json_decode(json_encode(self::$participant1Data)));
        }

        $this->assertEquals(self::$participant1Data['participant']['email'], self::$participantDeclarative->email);
        $this->assertEquals(ParticipantTypes::DECLARATIVE, self::$participantDeclarative->type);
        $this->assertNotNull(self::$participantDeclarative->uid);

        //Now the same participant must be returned with the specific uid as "participant_token"
        $participant = Participant::getParticipant(SurveyTest::$surveyTest,
            array("participant" => json_decode(json_encode(array(
                "participant_token" => self::$participantDeclarative->uid
            )))));

        $this->assertEquals(self::$participantDeclarative->id, $participant->id);
        $this->assertEquals(self::$participantDeclarative->uid, $participant->uid);
        $this->assertEquals(self::$participantDeclarative->name, $participant->name);
        $this->assertEquals(self::$participantDeclarative->email, $participant->email);

        //For a participant and a request hash must be the same
        $this->assertEquals(self::$participantDeclarative->hash, $participant->hash);
        $this->assertEquals(ParticipantTypes::DECLARATIVE, $participant->type);

        //Now trying to get participant with a fake uid, it should not return a participant
        $this->setExpectedException(ParticipantInvalidTokenException::class);
        Participant::getParticipant(SurveyTest::$surveyTest,
            array("participant" => json_decode(json_encode(array(
                "participant_token" => "this-is-a-fake-uid"
            )))));

        $this->displayInfo(__FUNCTION__, self::$participantDeclarative->uid);
    }

    public function testGetAuthenticatedParticipant() {

        //Get an authenticated participant (No participant data given)
        if (is_null(self::$participantAuthenticated)) {
            self::$participantAuthenticated = Participant::getParticipant(SurveyTest::$surveyTest, array());
        }

        $this->assertEquals(Auth::user()->id, self::$participantAuthenticated->uid);
        $this->assertEquals(Auth::user()->email, self::$participantAuthenticated->email);
        $this->assertEquals(Auth::user()->name, self::$participantAuthenticated->name);
        $this->assertEquals(ParticipantTypes::AUTHENTICATED, self::$participantAuthenticated->type);

        //Now the same participant must be returned with the same authenticated user
        $participant = Participant::getParticipant(SurveyTest::$surveyTest, array());

        $this->assertEquals(self::$participantAuthenticated->id, $participant->id);
        $this->assertEquals(self::$participantAuthenticated->uid, $participant->uid);
        $this->assertEquals(self::$participantAuthenticated->name, $participant->name);
        $this->assertEquals(self::$participantAuthenticated->email, $participant->email);

        //For a participant and a request hash must be the same
        $this->assertEquals(self::$participantAuthenticated->hash, $participant->hash);
        $this->assertEquals(ParticipantTypes::AUTHENTICATED, $participant->type);

        $this->displayInfo(__FUNCTION__, self::$participantAuthenticated->uid);
    }

    public function testHasAnswered() {

        //Test order is not known, we create participant if necessary
        if (is_null(self::$participantAuthenticated)) {
            self::$participantAuthenticated = Participant::getParticipant(SurveyTest::$surveyTest, array());
        }
        //Normally, this participant should not have answered
        $this->assertFalse(Participant::hasAnswered(SurveyTest::$surveyTest, array("participant" => json_decode(json_encode(array(
            "participant_token" => self::$participantDeclarative->uid
        ))))));

        //We get a question of the survey to prepare an answer
        $questions = SurveyTest::$surveyTest->Questions;
        $question = reset($questions);
        $propositions = $question->propositions;
        $proposition_1 = reset($propositions);

        //An answer is a set of choices for a question
        $answer = array(
            'question_id' => $question->id,
            'choices' => array(
                array(
                    'proposition_id' => $proposition_1->id,
                    'value' => YesNoMaybeValues::YES
                )
            )
        );


        //We create the answer
        $answer = Answer::create(self::$participantDeclarative, $question, (array)json_decode(json_encode($answer)));

        //Normally, this participant should  have answered
        $this->assertTrue(Participant::hasAnswered(SurveyTest::$surveyTest, array("participant" => json_decode(json_encode(array(
            "participant_token" => self::$participantDeclarative->uid
        ))))));

        //Testing with a user who isn't linked to a participant => has not answered
        $this->assertFalse(Participant::hasAnswered(SurveyTest::$surveyTest,
            (array)json_decode(json_encode(self::$participant1Data))));

        $this->displayInfo(__FUNCTION__, self::$participantDeclarative->uid);
    }

    public function testCast() {
        $participant_array = Participant::cast(self::$participantDeclarative,
            array('role' => SurveyUserRoles::OWNER,
                'can' => array(
                    'view_answers' => true,
                    'view_comments' => true
                )
            ));

        $this->assertNotNull($participant_array['answers']);
        $this->assertEquals(self::$participantDeclarative->name, $participant_array['name']);
        $this->assertEquals(self::$participantDeclarative->email, $participant_array['email']);
        $this->assertEquals(self::$participantDeclarative->hash, $participant_array['participant_hash']);

        $participant_array = Participant::cast(self::$participantDeclarative,
            array('role' => SurveyUserRoles::OWNER,
                'can' => array(
                    'view_answers' => false,
                    'view_comments' => false
                )
            ));

        $this->assertFalse(array_key_exists('answers', $participant_array));
        $this->assertFalse(array_key_exists('comments', $participant_array));
        $this->assertEquals(self::$participantDeclarative->name, $participant_array['name']);
        $this->assertEquals(self::$participantDeclarative->email, $participant_array['email']);
        $this->assertEquals(self::$participantDeclarative->hash, $participant_array['participant_hash']);


        $this->displayInfo(__FUNCTION__, self::$participantDeclarative->uid);
    }

    public function testCastCollection() {

        $participants = array(
            self::$participantDeclarative,
            self::$participantAuthenticated
        );

        $participants_array = Participant::castCollection($participants, array('role' => SurveyUserRoles::OWNER,
            'can' => array(
                'view_answers' => true,
                'view_comments' => true
            )
        ));

        foreach ($participants_array as $participant_array) {
            $this->assertNotNull($participant_array['answers']);
            $this->assertTrue(in_array($participant_array['name'],
                array(self::$participantDeclarative->name, self::$participantAuthenticated->name)));
            $this->assertTrue(in_array($participant_array['email'],
                array(self::$participantDeclarative->email, self::$participantAuthenticated->email)));
            $this->assertTrue(in_array($participant_array['participant_hash'],
                array(self::$participantDeclarative->hash, self::$participantAuthenticated->hash)));
        }

    }

}