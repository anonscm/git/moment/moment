<?php
/**
 *     Moment - SurveyTest.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
class SurveyTest extends UnittestCase {

    public static $surveyTest = null;

    public static $goodSurveyData = array(
        "title" => "Test survey",
        "place" => "It will be there",
        "description" => "This is the description",
        "settings" => array(
            "auto_close" => "",
            "enable_anonymous_answer" => 0,
            "enable_answer_edition" => 0,
            "enable_update_notification" => 0,
            "limit_participants" => 0,
            "limit_participants_nb" => "1",
            "reply_access" => "opened_to_everyone",
            "hide_answers" => 0,
            "hide_comments" => 0,
            "show_participant_name" => 1,
            "show_participant_email" => 0
        ),
        "questions" => array(
            array(
                "title" => "Question 1",
                "type" => "text",
                "position" => 0,
                "options" => array(
                    "force_unique_choice" => 0,
                    "enable_maybe_choices" => 0
                ),
                "propositions" => array(
                    array(
                        "type" => "text",
                        "position" => 0,
                        "header" => "Proposition 1",
                        "text" => ""
                    ),
                    array(
                        "type" => "text",
                        "position" => 1,
                        "header" => "Proposition 2",
                        "text" => ""
                    )
                )
            ),
            array(
                "title" => "Question 2",
                "type" => "date",
                "position" => 0,
                "options" => array(
                    "force_unique_choice" => 0,
                    "enable_maybe_choices" => 1
                ),
                "propositions" => array(
                    array(
                        "type" => "date",
                        "base_day" => 1485324800
                    ),
                    array(
                        "type" => "date",
                        "base_day" => 1475324800,
                        "base_time" => 28800
                    )
                )
            )
        ),
        "guests" => array(
            array(
                "email" => "test@test.com"
            )
        )
    );

    public static function setUpBeforeClass() {

        if (!is_object(Auth::user()))
            throw new AuthSPAuthenticationNotFoundException();

        self::$goodSurveyData['settings']['auto_close'] = time()+86400;

        $survey_data = self::$goodSurveyData;
        //Survey owner will be the currently authenticated user (see Auth::user())
        //For tests authenticated user will also be invited to the survey
//        $survey_data['guests'][] = Auth::user()->email;

        $survey_data['guests'][] = array(
            "email" => Auth::user()->email
        );
        self::$surveyTest = Survey::create((array)json_decode(json_encode($survey_data)));

    }

    public static function tearDownAfterClass() {
        if (self::$surveyTest) {
            self::$surveyTest->delete();
        }
    }

    /**
     * Test create survey
     */
    public function testCreate() {

        $survey = Survey::create((array)json_decode(json_encode(self::$goodSurveyData)));

        $this->assertTrue($survey->storedInDatabase);
        $this->assertTrue($survey->id !== null);

        $this->assertEquals($survey->title, self::$goodSurveyData['title']);
        $this->assertEquals($survey->place, self::$goodSurveyData['place']);
        $this->assertEquals($survey->raw_description, self::$goodSurveyData['description']);
        $this->assertEquals(count((array)$survey->settings), count(self::$goodSurveyData['settings']));
        foreach ((array)$survey->settings as $key => $value) {
            $this->assertEquals($value, self::$goodSurveyData['settings'][$key]);
        }
        $this->assertEquals(count($survey->Guests), 1);
        $this->assertEquals(count($survey->Questions), 2);

        $this->displayInfo(__FUNCTION__, 'Survey#' . $survey->id);

        $survey->delete();

        $surveyBadData = self::$goodSurveyData;

        $surveyBadData['questions'] = true;
        $this->setExpectedException(SurveyBadPropertyException::class);
        Survey::create((array)json_decode(json_encode($surveyBadData)));

    }

    /**
     * Test create survey
     */
    public function testUpdate() {


        $survey = self::$surveyTest;

        $survey_array = SurveyEndpoint::cast($survey, array('role' => SurveyUserRoles::OWNER));
        //Changing title
        $old_title = $survey_array['title'];
        $survey_array['title'] = 'This is the new title';

        //Deleting guests
        $old_guests = $survey_array['guests'];
        $survey_array['guests'] = array();

        //Updating question
        $old_questions = $survey_array['questions'];
        $survey_array['questions'] = json_decode(json_encode(array(
            array(
                "title" => "Nouvelle question",
                "type" => "date",
                "options" => array(
                    "force_unique_choice" => 0,
                    "enable_maybe_choices" => 0
                ),
                "propositions" => array(
                    array(
                        "base_day" => 1472688000
                    )
                )
            )
        )));

        $survey->update($survey_array);

        $this->assertTrue($survey->storedInDatabase);
        $this->assertTrue($survey->id !== null);

        $this->assertEquals($survey->title, $survey_array['title']);
        $this->assertEquals(count($survey->Guests), count($survey_array['guests']));
        $this->assertEquals($survey->Questions[0]->title, $survey_array['questions'][0]->title);

        //Reseting survey
        $survey_array['title'] = $old_title;
        $old_guests[0]['email'] = 'test1@test.com';
        $old_guests[] = array('email' => 'test2@test.com');
        $survey_array['guests'] = $old_guests;
        unset($old_questions[0]['id']);
        unset($old_questions[1]['id']);
        $survey_array['questions'] = json_decode(json_encode($old_questions));

        $survey->update($survey_array);

        $this->assertEquals(count($survey->Guests), count($survey_array['guests']));

        $this->displayInfo(__FUNCTION__, $survey);
    }

    public function testFromId() {

        //id to get
        $requested_survey_id = self::$surveyTest->id;

        $survey = Survey::fromId($requested_survey_id);

        //Comparing values
        $this->assertEquals($survey->title, self::$goodSurveyData['title']);
        $this->assertEquals($survey->place, self::$goodSurveyData['place']);
        $this->assertEquals($survey->raw_description, self::$goodSurveyData['description']);
        $this->assertEquals(count((array)$survey->settings), count(self::$goodSurveyData['settings']));
        foreach ((array)$survey->settings as $key => $value) {
            $this->assertEquals($key.$value, $key.self::$goodSurveyData['settings'][$key]);
        }

        $this->displayInfo(__FUNCTION__, $survey);
    }

    public function testOwnedBy() {

        //user
        $user = Auth::user();

        $surveys = Survey::ownedBy($user);

        $found = false;
        foreach ($surveys as $survey) {
            $this->assertTrue($survey->isOwner($user->email));

            if ($survey->id === self::$surveyTest->id)
                $found = true;
        }

        //Survey test must be in the list
        $this->assertTrue($found);

        $this->displayInfo(__FUNCTION__, null);
    }

    public function testFromHumanReadableId() {

        //id to get
        $requested_survey_id = self::$surveyTest->id;

        $survey = Survey::fromId($requested_survey_id);

        //Good human readable id
        $human_readable_id = $survey->human_readable_id;

        $new_survey = Survey::fromHumanReadableId($human_readable_id);

        $this->assertNotNull($new_survey);

        //We should have found a survey
        $this->assertEquals($new_survey->id, $survey->id);

        //Bad human readable id
        $human_readable_id = 'transformed-id-' . $survey->human_readable_id . '-this-is-the-bad-part';

        //The bad HRI must not give a result
        $this->setExpectedException(NotFoundException::class);
        Survey::fromHumanReadableId($human_readable_id);

    }

    public function testFromGuestUser() {

        $surveys = Survey::fromGuestUser(Auth::user());

        foreach ($surveys as $survey) {
            $this->assertTrue($survey->isInvited(Auth::user()->email));
        }

        $this->displayInfo(__FUNCTION__, null);
    }

    public function testAnsweredBy() {

        //user
        $user = Auth::user();

        $surveys = Survey::answeredBy($user);

        $this->displayInfo(__FUNCTION__, null);
    }

}