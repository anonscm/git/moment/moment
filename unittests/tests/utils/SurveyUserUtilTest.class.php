<?php
/**
 *     Moment - SurveyUserUtilTest.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
class SurveyUserUtilTest extends UnittestCase {

    public static function setUpBeforeClass() {
        SurveyTest::setUpBeforeClass();
    }

    public static function tearDownAfterClass() {
        SurveyTest::tearDownAfterClass();
    }

    public function testGetCurrentUser() {

        //In tests there is a fake user authenticated, this user is the owner of surveyTest
        $current_user = SurveyUserUtil::getCurrentUser(SurveyTest::$surveyTest);

        $this->assertEquals(SurveyUserRoles::OWNER, $current_user['role']);

        //When survey does not exists, user should have the role of creator ("Able to create")
        $current_user = SurveyUserUtil::getCurrentUser(null);

        $this->assertEquals(SurveyUserRoles::CREATOR, $current_user['role']);

        $this->displayInfo(__FUNCTION__, null);
    }
}