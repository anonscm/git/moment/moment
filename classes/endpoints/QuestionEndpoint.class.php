<?php

/**
 *     Moment - QuestionEndpoint.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class QuestionEndpoint
 *
 * Rest Endpoint for question
 *
 */
class QuestionEndpoint extends RestEndpoint {


    /**
     * Get question details
     *
     * @throws RestNotAllowedException
     */
    public static function get($question_id, $sub_resource = null) {


        $question = Question::fromId($question_id);

        //Get current user info on this survey
        $current_user = SurveyUserUtil::getCurrentUser($question->Survey);

        if($sub_resource === 'answer'
                && isset($current_user['can']['view_answers'])
                && $current_user['can']['view_answers']
            ) {
            //Checking selectors
            $selectors = RestRequest::getOutputProperties();
            if($selectors['filter']) { // It's possible to get participant email for a proposition and a value
                $answers = static::getFilteredAnswers($question, $selectors);
            } else {
                $answers = static::getAnswers($question, $selectors);
            }

            return array_map(function($answer) use ($current_user) {
                return Answer::cast($answer, false, $current_user);
            }, $answers);
        }

        // Legitimate user should not get here
        throw new RestNotAllowedException();
    }

    /**
     * Get answers (paginated)
     *
     * @param $question
     * @param $selectors
     * @return array
     * @throws ModelViolationException
     * @throws RestBadParameterException
     */
    private static function getAnswers($question, $selectors) {
        //We use count and startIndex in prefilter
        $count = null;
        if(is_int($selectors['count']) && ($selectors['count'] > 0))
            $count = (int)$selectors['count'];

        $start_index = null;
        if(is_int($selectors['startIndex']) && ($selectors['startIndex'] >= 0))
            $start_index = (int)$selectors['startIndex'];

        //Resetting startIndex and count (to prevent postfilter)
        $selectors['startIndex'] = null;
        $selectors['count'] = null;

        return RestUtilities::getEntities(new EntityGetter('Answer', function() use($question, $count, $start_index) {
            //We get answers for a specific question
            $pre = 'question_id = :question_id AND participant_type != :participant_type';
            $ph[':question_id'] = $question->id;
            $ph[':participant_type'] = ParticipantTypes::ULTIMATE;

            //If count and startIndex are defined we can add those in our prefilter query part
            if(!is_null($count)) {
                $pre .= ' LIMIT '.$count;
                if(!is_null($start_index)) {
                    $pre .= ' OFFSET '.$start_index;
                }
            } else if(!is_null($start_index)) {
                // See MySQL LIMIT about this wierdness
                $pre .= ' LIMIT 18446744073709551614 OFFSET '.$start_index;
            }

            return Answer::all($pre, $ph);
        }), array(), $selectors);

    }

    /**
     * Get answers with a filter
     * @param $question
     * @param $selectors
     * @return array
     * @throws ModelViolationException
     * @throws RestBadParameterException
     */
    private static function getFilteredAnswers($question, $selectors) {
        return RestUtilities::getEntities(new EntityGetter('Answer', function($pre, $ph) use($question) {
            $pre_exploded = explode('LIMIT', $pre);
            $pre = 'question_id = :question_id AND participant_type != :participant_type AND (' . $pre_exploded[0] . ') ' . ((count($pre_exploded) > 1) ? 'LIMIT ' . $pre_exploded[1] : '');
            $ph[':question_id'] = $question->id;
            $ph[':participant_type'] = ParticipantTypes::ULTIMATE;
            return Answer::all($pre, $ph);
        }), array('participant_id'), $selectors);
    }

}
