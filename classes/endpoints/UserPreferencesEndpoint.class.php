<?php

/**
 *     Moment - UserPreferencesEndpoint.class.php
 *
 * Copyright (C) 2024  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST user preferences endpoint
 */
class UserPreferencesEndpoint extends RestEndpoint {

    /**
     * Update user preferences
     *
     * @param int $id
     * @return array
     * @throws FormDataValidationException
     * @throws RestNotAllowedException
     */
    public static function put() {
        $user = Auth::user();

        if (!$user)
            throw new RestNotAllowedException();

        $data = (array)RestRequest::getInput();

        //data validation (throws exception)
        $form = new PreferencesForm($user);
        $form->validateData($data);

        $res = [];

        $userSettings = UserPreferences::getSettings();

        foreach($data as $name => $value) {
            $name = str_replace('settings.', '', $name);

            if(!isset($userSettings[$name]))
                continue;

            $pref = $user->getPreference($name);
            if(!is_null($user->getPreference($name))) {
                $pref->value = $value;
                $pref->save();
            } else {
                $pref = Preference::create($user, $name, $value);
            }
            $res[] = [
                $name => $pref->value
            ];
        }

        return array(
            'path' => '/UserPreferences',
            'data' => $res
        );
    }
}
