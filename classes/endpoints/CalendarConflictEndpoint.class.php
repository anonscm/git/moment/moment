<?php

/**
 *     Moment - CalendarConflictEndpoint.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST calendar conflict endpoint
 */
class CalendarConflictEndpoint extends RestEndpoint {

    /**
     * Get calendar conflicts
     *
     * @param null $survey_id
     * @return array
     * @throws RestNotAllowedException
     */
    public static function get($survey_id = null) {
        if (!Auth::user())
            throw new RestNotAllowedException('Calendar');

        if(isset($survey_id)) {
            $survey = Survey::fromId($survey_id);

            //Get current user info on this survey
            $current_user = SurveyUserUtil::getCurrentUser($survey);

            //User must at least can reply to survey to get conflicts
            if (SurveyUserRoles::REPLIER > $current_user['role'])
                throw new RestNotAllowedException();

            return CalendarManager::getConflicts(Auth::user(), $survey);
        }
        throw new RestNotAllowedException();
    }

}
