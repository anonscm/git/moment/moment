<?php

/**
 *     Moment - CalendarCheckEndpoint.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST calendarcheck endpoint
 */
class CalendarCheckEndpoint extends RestEndpoint {

    /**
     * creates calendar
     *
     * Request body must be an object with properties
     *
     * @return array
     * @throws SurveyActionNotAllowedException
     */
    public static function post() {
        if (!Auth::user())
            throw new RestNotAllowedException('CalendarCheck');

        $data = (array)RestRequest::getInput();

        if(!isset($data['url']))
            throw new RestMissingParameterException('url');

        $url = filter_var(urldecode($data['url']), FILTER_SANITIZE_URL);

        if($url && !filter_var($url, FILTER_VALIDATE_URL))
            throw new RestBadParameterException('not_an_url');

        return CalendarManager::checkUrl($url);
    }
}
