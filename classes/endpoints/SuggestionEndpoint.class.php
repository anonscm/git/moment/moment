<?php

/**
 *     Moment - SuggestionEndpoint.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class SuggestionEndpoint
 *
 * Rest Endpoint for suggestion
 *
 * GET /suggestion/:sub_entity/:nature (ex. /suggestion/survey/title)
 */
class SuggestionEndpoint extends RestEndpoint {


    /**
     * Get suggestions
     *
     * @param string $sub_entity
     * @param string $nature
     * @return array
     * @throws RestNotAllowedException
     */
    public static function get($sub_entity = '', $nature = '') {

        if (!Auth::user() || empty($sub_entity) || empty($nature))
            throw new RestNotAllowedException();

        $suggestions = array();
        switch ($sub_entity) {
            case 'survey' :
                switch ($nature) {
                    case 'title' :
                        foreach (Survey::ownedBy(Auth::user()) as $survey) {
                            $suggestions[] = $survey->title;
                        }
                        break;
                    case 'guest_email' :
                        $suggestions = Guest::getGuestEmailsFormOwnerEmail(Auth::user()->email);
                        break;
                }
                break;
        }
        return array_values(array_unique($suggestions));
    }

    /**
     * put not allowed
     */
    public static function put($id = null) {
        throw new RestNotAllowedException();
    }

    /**
     * Post not allowed
     */
    public static function post() {
        throw new RestNotAllowedException();
    }

    /**
     * Deletes
     *
     * @param type $id
     * @return boolean
     * @throws RestNotAllowedException
     */
    public static function delete($id = null) {
        throw new RestNotAllowedException();
    }
}
