<?php

/**
 *     Moment - AnswerEndpoint.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class AnswerEndpoint
 *
 * Rest Endpoint for answers
 *
 * POST /answer  to create an answer
 * PUT /answer/:id to update an answer
 */
class AnswerEndpoint extends RestEndpoint {


    /**
     * Get answers
     *
     * @param null $id, id of answer
     * @param null $sub_resource, sub_resource to get ('comment')
     * @return array
     * @throws RestNotAllowedException
     */
    public static function get($id = null, $sub_resource = null) {

        // No id => not allowed
        if(!isset($id)) {
            throw new RestNotAllowedException();
        }

        $answer = Answer::fromId($id);

        //Getting survey from answer
        $survey = $answer->Question->Survey;

        //Get current user info (He may not be the participant
        $current_user = SurveyUserUtil::getCurrentUser($survey);

        if (!isset($current_user['can']['view_answers'])
            || !$current_user['can']['view_answers']) {
            throw new RestNotAllowedException();
        }

        $can_view_comment = isset($current_user['can']['view_comments'])
            && $current_user['can']['view_comments']
            && (!$answer->hide_comment
                || ($answer->hide_comment && SurveyUserRoles::OWNER === $current_user['role']));

        if (is_null($sub_resource)) {
            return Answer::cast($answer, $can_view_comment);
        } elseif ($sub_resource === 'comment' && $can_view_comment) {
            return array(
                'date' => ($answer->Participant->answers_updated ?
                    $answer->Participant->answers_updated
                    : $answer->Participant->answers_created),
                'participant_name' => $answer->Participant->ensure_name,
                'can_perform_action' => SurveyUserRoles::OWNER == $current_user['role'],
                'content' => $answer->comment
            );
        } else {
            throw new RestNotAllowedException();
        }
    }

    /**
     * Update answer or answers
     *
     * @param int $id answer id
     *
     * Request body must be an object with properties to update
     *
     * @return array
     * @throws AnswerBadPropertyException
     * @throws RestNotAllowedException
     */
    public static function put($id = null, $access_token = null) {

        // Getting request inputs
        $data = RestRequest::getInput();

        if (is_null($id)) { //Updating a collection of answers
            return static::updateAnswersCollection($data);
        } else { //Updating an answer
            //Validating data (Check there are a question_id, choices and answer_id)
            AnswerUtil::validate((array)$data, true);

            //Getting question
            $question = Question::fromId($data->question_id);

            //Getting survey from question
            $survey = $question->Survey;

            //Get current user info (He may not be the participant
            $current_user = SurveyUserUtil::getCurrentUser($survey);

            // Check permissions : user must be at least replier and edition must be enabled
            if ($survey->disable_answer_edition || $current_user['role'] < SurveyUserRoles::REPLIER)
                throw new RestNotAllowedException('Update answer for survey ' . $survey->id);

            //Getting the answer
            $answer = Answer::fromId($data->answer_id);

            // Check if participant has answered
            if (Participant::hasAnswered($survey, (array)$data)) {

                //Getting the real participant for this User and survey
                $participant = Participant::getParticipant($survey, (array)$data);

                //Only answer participant can edit
                if ($answer->Participant->id == $participant->id) {
                    $answer->update((array)$data);
                }
            } else {
                throw new RestNotAllowedException();
            }
        }

        //Transforming newly created answers to an array of answers as array
        $answers = static::prepareAnswersReturn($participant);

        return array(
            'path' => '/survey/' . $survey->id . '/participant/' . $participant->id . '/answers',
            'answers' => $answers
        );
    }

    /**
     * Creates a answers for a survey
     *
     * Participant (identified by participant id) must exist
     * otherwise Survey (identified by survey id) must exist
     * @param null $id
     * @param null $action
     * @return array
     * @throws Exception
     * @throws ParticipantAlreadyAnsweredException
     * @throws RestNotAllowedException
     */
    public static function post($id = null, $action = null) {

        //If id is set, an action may be required on answer by owner
        if (!is_null($id)) {
            return static::performAction($id, $action);
        }

        // Getting request inputs
        $data = RestRequest::getInput();

        // data nature must be ok
        if (!is_array($data) || empty($data)) {
            throw new MomentRestNotAllowedException('Operation not permitted');
        }

        //Validating data
        AnswerUtil::validateAnswersData($data);

        //First element must contain participant data
        $first_answer_data = array_slice($data, 0, 1)[0];

        try {
            //Getting question
            $question = Question::fromId($first_answer_data->question_id);

            //Getting survey from question
            $survey = $question->Survey;
        } catch (NotFoundException $nfe) {
            throw new SurveyNotFoundException($nfe);
        }

        // Check if survey is not a draft; we should not register an answer for a draft
        if ($survey->is_draft) {
            throw new AnswerDraftSurveyException('Create answer for draft ' . $survey->id);
        }

        try {
            //IMPORTANT call isClosed before getParticipant (because of limit_participants_nb can be reached)
            $isClosed = $survey->isClosed();

            //Getting the real participant for this User and survey
            $participant = Participant::getParticipant($survey, (array)$first_answer_data);

            //Only ultimate answers can be set after close
            if($isClosed && ParticipantTypes::ULTIMATE !== $participant->type) {
                throw new AnswerSurveyClosedException('Create answer for closed survey ' . $survey->id);
            }

            //Check participant has already replied to every question
            if (count($participant->Answers) == count($survey->Questions)) {
                throw new ParticipantAlreadyAnsweredException($participant);
            }

            //Get current user info
            $current_user = SurveyUserUtil::getCurrentUser($survey);

            // Check permissions : user must be at least replier
            if ($current_user['role'] < SurveyUserRoles::REPLIER) {
                throw new ParticipantNotAllowedException('Create answer for survey ' . $survey->id);
            }

            //Create the answers for this participant
            static::createAnswersFromData($survey, $participant, $data);

            //Updating answers info in participant
            $participant->answers_created = time();
            $participant->save();

            //Transforming newly created answers to an array of answers as array
            $array = array(
                'path' => '/survey/' . $survey->id . '/participant/' . $participant->id . '/answers',
                'answers' => static::prepareAnswersReturn($participant)
            );

            static::sendAnswersCreationNotification($survey, $participant);

            return $array;
        } catch (Exception $e) {
            //If participant has just been created and something went wrong we must delete it
            if (isset($participant) && $participant->newly_created)
                $participant->delete();

            throw $e;
        }

        throw new MomentRestNotAllowedException('Operation not permitted');

    }

    /**
     * Deletes answer
     *
     * @param type $id
     * @return boolean
     * @throws RestNotAllowedException
     */
    public static function delete($id = null) {
        throw new RestNotAllowedException();
    }

    /**
     * Check if the $question as an answer in the $answer_collection
     *
     * @param array $answer_collection
     * @param Question $question
     * @return bool
     */
    private static function isAnswered(array $answer_collection, Question $question) {
        foreach ($answer_collection as $answer) {
            if ($answer instanceof Answer) {
                return ($answer->Question->id == $question->id);
            } else {
                if (property_exists($answer, 'question_id')) {
                    return ($answer->question_id == $question->id);
                }
            }
        }
        return false;
    }

    /**
     * Creates Answers Entity for this $survey and $participant with $data
     * @param Survey $survey
     * @param Participant $participant
     * @param array $data
     * @throws AnswerMissingException
     * @throws AnswerMissingPropertyException
     * @throws ParticipantAlreadyAnsweredException
     */
    private static function createAnswersFromData(Survey $survey, Participant $participant, array $data) {
        foreach ($survey->Questions as $question) {
            $question_answered = self::isAnswered($participant->Answers, $question);

            //Getting question answer in the new answers
            foreach ($data as $index => $answer_data) {
                //Answer exists in list for this question
                if ($answer_data->question_id == $question->id) {
                    //We check if participant haven't already answered this question
                    if ($question_answered) {
                        throw new ParticipantAlreadyAnsweredException($participant);
                    }

                    //We create the answer
                    Answer::create($participant, $question, (array)$answer_data);
                    $question_answered = true;
                    unset($data[$index]);
                    break;
                }
            }

            //If question of the survey must have an answer
            if (!$question_answered) {
                throw new AnswerMissingException($question, $data);
            }
        }
    }

    /**
     * Send notification of answer creation (new participation and token or final answers)
     * @param Survey $survey
     * @param Participant $participant
     * @throws EventHandlerIsNotCallableException
     * @throws MailNoAddressesFoundException
     */
    private static function sendAnswersCreationNotification(Survey $survey, Participant $participant) {
        //If ultimate answers then closing survey
        if (ParticipantTypes::ULTIMATE === $participant->type) {
            if (!$survey->isClosed()) {
                //We have to close the survey
                //TODO set reason
                $survey->close(null);
            } else {
                $survey->sendFinalAnswersSelectedNotifications();
            }

            //Triggering event
            (new Event('ultimate_answers_selected', $survey, $participant))->trigger(function ($survey, $participant) {
                EventLog::push($participant, EventLogTypes::CLOSED, 'ultimate answers selected');
            });

        } else {
            //It's a "normal" participation

            //Sending participant token mail if needed (even if participant has commented before, we re-send the token)
            Participant::sendNotification($participant,
                ParticipantNotificationTypes::TOKEN,
                ParticipantNotificationReasons::ANSWER_CREATED);

            //If owner doesn't want to be notified
            if (!$survey->dont_notify_on_reply) {
                $survey->sendNewParticipationNotification($participant);
            }
        }
    }

    /**
     * Update a collection of answers by data
     * @param $data
     * @return array the concerned participant
     * @throws AnswerBadPropertyException
     * @throws AnswerMissingPropertyException
     * @throws ParticipantNotFoundException
     * @throws RestNotAllowedException
     */
    private static function updateAnswersCollection($data) {

        $participant = null;

        if(!is_array($data) || empty($data))
            throw new AnswerBadPropertyException('data', $data);

        //Validating data (Check there are a question_id, choices)
        AnswerUtil::validateAnswersData($data);

        //First element must contain participant data
        $first_answer_data = array_slice($data, 0, 1)[0];

        //Getting question
        $question = Question::fromId($first_answer_data->question_id);

        //Getting survey from question
        $survey = $question->Survey;

        //Get current user info (He may not be the participant
        $current_user = SurveyUserUtil::getCurrentUser($survey);

        // Check permissions : answer edition must not be disabled and user must be at least replier
        if ($survey->disable_answer_edition
            || $current_user['role'] < SurveyUserRoles::REPLIER) {
            throw new RestNotAllowedException('Update answer for survey ' . $survey->id);
        }

        //Getting the real participant for this User and survey
        $participant = Participant::getParticipant($survey, (array)$first_answer_data);

        //In case of declarative participant it's possible to update name
        if (ParticipantTypes::DECLARATIVE === $participant->type && property_exists($first_answer_data, 'participant')) {
            //Updating participant name
            $participant->update((array)$first_answer_data->participant);
        }

        //If participant is newly created => he can't already have answers
        // Check if participant has answered
        if ($participant->newly_created || !$participant->answers_created) {
            //Participant is newly created or has not answered.
            //Forbidden to update answer
            throw new RestNotAllowedException();
        }

        foreach ($data as $answer_data) {
            static::updateOrCreateAnswerFromData($answer_data, $participant, $current_user);
        }

        //If Partcipant is declarative and has updated his answers with recovery code we send him his token
        if (ParticipantTypes::DECLARATIVE === $participant->type
            && isset($_SERVER['HTTP_X_PARTICIPANT_RECOVERY'])
            && $_SERVER['HTTP_X_PARTICIPANT_RECOVERY']) {
            //Sending participant token mail if needed
            Participant::sendNotification($participant,
                ParticipantNotificationTypes::TOKEN,
                ParticipantNotificationReasons::ANSWER_UPDATED);
        }

        return array(
            'path' => 'survey/' . $survey->id . '/participant/' . $participant->id . '/answers',
            'answers' => static::prepareAnswersReturn($participant)
        );
    }

    private static function updateOrCreateAnswerFromData($answer_data, Participant $participant, $current_user) {

        //Getting question (question_id is mandatory)
        $question = Question::fromId($answer_data->question_id);

        //Getting the answer
        if (property_exists($answer_data, 'answer_id')) {
            $answer = Answer::fromId($answer_data->answer_id);
        } else if (isset($participant->sorted_answers[$question->id])) {
            $answer = $participant->sorted_answers[$question->id];
        } else {
            $answer = null;
        }

        //Answer for this question already exists
        if ($answer) {
            //Only survey owner and answer owner can edit
            if (SurveyUserRoles::OWNER == $current_user['role']
                || $answer->Participant->id == $participant->id
            ) {
                $answer->update((array)$answer_data);
            }
        } else {
            //Answer doesn't exist yet
            Answer::create($participant, $question, (array)$answer_data);
        }
    }

    private static function performAction($answer_id, $action = null) {

        if(!isset($action)) {
            throw new MomentRestNotAllowedException('Answer');
        }

        //Id is set
        $current_answer = Answer::fromId($answer_id);

        //current survey
        $current_survey = $current_answer->Question->Survey;

        //Get current user role
        $current_user = SurveyUserUtil::getCurrentUser($current_survey);

        //Only owner can access actions on survey
        if (SurveyUserRoles::OWNER !== $current_user['role']) {
            throw new MomentRestNotAllowedException('Answer');
        }

        switch ($action) {
            case 'mask_comment' :
                return $current_answer->maskComment();
                break;
            case 'unmask_comment' :
                return $current_answer->unmaskComment();
                break;
            default :
                throw new MomentRestNotAllowedException($action . ' answer');
        }
    }

    private static function prepareAnswersReturn(Participant $participant) {
        return array_map(function ($answer) use ($participant) {
            $answer_cast = Answer::cast($answer);

            //When identity is declarative we must send back the participant token (needed for any further action)
            if (ParticipantTypes::DECLARATIVE === $participant->type) {
                if (!isset($answer_cast['participant']))
                    $answer_cast['participant'] = array();

                $answer_cast['participant']['participant_token'] = $participant->uid;
            }

            return $answer_cast;
        }, $participant->Answers);
    }
}
