<?php

/**
 *     Moment - MyCalEndpoint.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * REST my calendar endpoint
 */
class MyCalEndpoint extends RestEndpoint {

    /**
     * Get user calendar
     *
     * @param null $calendar_hash
     * @return array
     * @throws RestNotAllowedException
     */
    public static function get($calendar_hash = null) {
        if(isset($calendar_hash)) {
            header('Content-Type: text/calendar');
            echo MyCalUtil::getIcsFromHash($calendar_hash);
            exit;
        }
        throw new RestNotAllowedException();
    }

    /**
     * Actions of calendar
     *
     * @param null $calendar_hash
     * @param null $action
     * @return array
     * @throws RestNotAllowedException
     */
    public static function post($calendar_hash = null, $action = null) {
        $auth_user = Auth::user();
        if(!Auth::user())
            throw new RestNotAllowedException();

        if(isset($calendar_hash) && isset($action)) {
            $user = User::fromCalendarHash($calendar_hash);

            if($user == $auth_user) {
                switch ($action) {
                    case 'regenerate' :
                        //Revoke hash
                        $user->calendar_hash = null;
                        return CalendarManager::getMyCalItem(false);
                }
            }
        }
        throw new RestNotAllowedException();
    }

}
