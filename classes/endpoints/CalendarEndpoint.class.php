<?php

/**
 *     Moment - CalendarEndpoint.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST calendar endpoint
 */
class CalendarEndpoint extends RestEndpoint {

    /**
     * Cast a Calendar to an array for response
     *
     * @param Calendar $cal
     *
     * @return array
     */
    public static function cast(Calendar $cal) {
        $data = self::castLight($cal);
        $data['checked'] = RestUtilities::formatDate($cal->checked);
        $data['status'] = $cal->status;

        return $data;
    }

    /**
     * Cast a Calendar to an light array for response (conflicts...°
     *
     * @param Calendar $cal
     *
     * @return array
     */
    public static function castLight(Calendar $cal) {
        return array(
            'id' => $cal->id,
            'url' => $cal->url,
            'name' => $cal->name,
            'settings' => $cal->settings
        );
    }

    /**
     * Get calendar
     *
     * If $id is @me it retrieves all user's calendars entity
     * Else it retrieves only requested calendar entity
     *
     * If $sub_resources is 'events' it retrieves formatted event of calendar with $id
     * @param null $id
     * @param null $sub_resources
     * @return array
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws RestNotAllowedException
     */
    public static function get($id = null, $sub_resources = null) {
        if (!Auth::user() || !isset($id))
            throw new RestNotAllowedException();

        if ($id == '@me') {
            $cals = Auth::user()->Calendars;
            $res = array();

            foreach ($cals as $cal) {
                $res[] = self::cast($cal);
            }

            //Include internal calendar MyCal
            $res[] = CalendarManager::getMyCalItem();

            return $res;
        } else if ($id == '@MyCal') {
            return CalendarManager::getMyCalEvents();
        } else {
            $cal = Calendar::fromId($id);

            if ($cal && $cal->User->id === Auth::user()->id) {

                //get events of calendar
                if (isset($sub_resources) && 'events' === $sub_resources) {
                    if ($cal->status['code'] == CalendarStatusCodes::OK) {
                        return CalendarManager::getEvents($cal->url);
                    }
                    //No events  can be found when calendar status is not ok
                    return array();
                }

                return self::cast($cal);
            }
        }
    }

    /**
     * creates calendar
     *
     * Request body must be an object with properties
     *
     * @return array
     * @throws FormDataValidationException
     * @throws MissingArgumentException
     * @throws RestNotAllowedException
     */
    public static function post() {
        if (!Auth::user())
            throw new RestNotAllowedException();

        $data = (array)RestRequest::getInput();

        //data validation (throws exception)
        $form = new CalendarForm();
        $form->validateData($data);

        $cal = Calendar::create(Auth::user(), $data);

        return array(
            'path' => '/calendar/' . $cal->id,
            'data' => self::cast($cal)
        );
    }

    /**
     * Update calendar properties
     *
     * @param int $id
     * @return array
     * @throws FormDataValidationException
     * @throws RestNotAllowedException
     */
    public static function put($id = null) {
        if (!Auth::user())
            throw new RestNotAllowedException();

        if (isset($id)) {
            $data = (array)RestRequest::getInput();

            //data validation (throws exception)
            $form = new CalendarForm();
            $form->validateData($data);

            $cal = Calendar::fromId($id);
            //Only calendar user can update his calendars
            if ($cal && $cal->User->id === Auth::user()->id) {
                $cal->update(Auth::user(), $data);

                return array(
                    'path' => '/calendar/' . $cal->id,
                    'data' => self::cast($cal)
                );
            }
        }
        throw new RestNotAllowedException();
    }

    /**
     * Deletes participant
     *
     * @param int $id
     * @return bool true if participant as been deleted
     * @throws RestNotAllowedException
     */
    public static function delete($id = null) {
        if (!Auth::user())
            throw new RestNotAllowedException();

        if (isset($id)) {
            $cal = Calendar::fromId($id);
            //Only calendar user can delete his calendars
            if ($cal && $cal->User->id === Auth::user()->id)
                $cal->delete();

            return true;
        }
        throw new RestNotAllowedException();
    }
}
