<?php

/**
 *     Moment - ParticipantEndpoint.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class ParticipantEndpoint
 *
 * Rest Endpoint for participants
 *
 * POST /answer  to create an answer
 * PUT /answer/:id to update an answer
 */
class ParticipantEndpoint extends RestEndpoint {


    /**
     * Get participants
     *
     * Not allowed to list participants
     *
     * @throws RestNotAllowedException
     */
    public static function get() {
        throw new RestNotAllowedException();
    }

    /**
     * Update participant
     *
     * @param int $id
     * @throws RestNotAllowedException
     */
    public static function put($id = null) {
        throw new RestNotAllowedException();
    }

    /**
     * Deletes participant
     *
     * @param int $id
     * @throws RestNotAllowedException
     */
    public static function delete($id = null) {

        if(isset($id)) {
            $participant_to_delete = Participant::fromIdOnly($id);
            $survey = $participant_to_delete->Survey;

            $current_user = SurveyUserUtil::getCurrentUser($survey);

            // Check permissions : only users with role replier can delete survey
            if (SurveyUserRoles::REPLIER <= $current_user['role']) {

                //We try to get a participant (without creating it)
                $current_participant = Participant::getParticipant($survey, array(), false);

                //Current participant is trying to delete is own participation
                if($current_participant->id === $participant_to_delete->id) {
                    //Notify Participant that his answers have been deleted
                    if(ParticipantTypes::DECLARATIVE === $participant_to_delete->type) {
                        Participant::sendNotification($participant_to_delete, ParticipantNotificationTypes::DELETION_CONFIRMATION);
                    }

                    //Notify Owners if requested
                    if (!$survey->dont_notify_on_reply) {
                        MomentApplicationMail::prepareParticipantDeletedMail($survey, $participant_to_delete)->send();
                    }

                    //Participant can delete is own participation
                    return $participant_to_delete->delete();
                }
            }
        }

        throw new RestNotAllowedException();
    }

    /**
     * Sends a email containing the participant custom link
     * for the survey to the participant
     *
     * Participant (identified by participant id) must exist
     *
     * @param string $sub_resource ('token')
     * @return array
     * @throws ParticipantMissingPropertyException
     * @throws RestNotAllowedException
     */
    public static function post($sub_resource) {

        // Getting request inputs
        $data = RestRequest::getInput();

        if (ParticipantNotificationTypes::isValid($sub_resource)
            && Auth::user()
            && is_object($data)
        ) {

            if (!property_exists($data, 'participant_id'))
                throw new ParticipantMissingPropertyException('participant_id', $data);

            //Getting declarative participant
            $participant = Participant::fromIdOnly($data->participant_id, ParticipantTypes::DECLARATIVE);

            if ($participant) {
                //Getting survey
                $survey = $participant->Survey;

                $current_user = SurveyUserUtil::getCurrentUser($survey);

                //Only owner can communicate to participant
                if ($current_user['role'] >= SurveyUserRoles::OWNER) {
                    return Participant::sendNotification($participant, $sub_resource);
                }
            }
        }
        throw new RestNotAllowedException('Operation not permitted');
    }
}
