<?php

/**
 *     Moment - UserEndpoint.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST user endpoint
 */
class UserEndpoint extends UserEndpointBase {

    /**
     * Cast a User to an array for response
     *
     * @param User user
     *
     * @return array
     */
    public static function cast(User $user) {
        $data = array(
            'id' => $user->id,
            'attributes' => $user->attributes->getAll(),
            'last_motd_time' => $user->last_motd_time,
            'created' => RestUtilities::formatDate($user->created),
            'last_activity' => RestUtilities::formatDate($user->last_activity),
            'lang' => $user->lang
        );

        if(Auth::isAdmin()) {
            $data['email'] = $user->email;
        }

        return $data;
    }

    /**
     * Update user properties
     *
     * Call examples :
     *  /user/@me : update current user
     *
     * @param mixed $ids
     * @return RestResponseJson
     * @throws RestNotAllowedException
     */
    public static function put($ids) {

        $input = RestRequest::getInput();

        if(property_exists($input, 'last_motd_time')) {
            $user = Auth::user();
            $user->last_motd_time = $input->last_motd_time;
            $user->save();

            return new RestResponseJson(static::cast($user));
        }

        return parent::put($ids);
    }
}
