<?php

/**
 *     Moment - SurveyEndpoint.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class SurveyEndpoint
 *
 * Rest Endpoint for survet
 *
 * GET /survey  to get surveys
 * GET /survey/:id  to get survey
 * POST /survey  to create a survey
 * PUT /survey/:id to update a survey
 * DELETE /survey/:id to delete a survey
 *
 */
class SurveyEndpoint extends RestEndpoint {

    /**
     * Get surveys
     *
     * Call examples :
     *  /survey : get all surveys where user is owner
     *  /survey/@guest : get all surveys where user is invited
     *  /survey/@answered : get all surveys where user answered.
     *  /survey/@feed : get all surveys where user's attention is needed
     *  /survey/<id> : get survey
     *
     * @param int $id Survey id to get info about
     *
     * @param string $sub_resource can be 'participant', 'participants'
     * @param string $sub_resource_id identifier of the sub_resource
     * @param string $sub_resource_modifier modifier in 'comments' or 'answers'
     * @return mixed
     * @throws RestBadParameterException
     * @throws RestMethodNotImplementedException
     * @throws RestNotAllowedException
     */
    public static function get($id = null, $sub_resource = null, $sub_resource_id = null, $sub_resource_modifier = null) {


        //Check if id is not a macro param
        $macro_param = '';
        if ('@' == substr($id, 0, 1) && ($id == '@guest'
                || $id == '@answered'
                || $id == '@feed')
        ) {
            $macro_param = $id;
        }

        if (is_null($id) || !empty($macro_param)) {
            //User must be authenticated
            if(!Auth::user())
                throw new RestNotAllowedException();

            $surveys = static::getSurveys($macro_param);
        } else {
            // Trying to get specific survey
            if (!is_string($id) && strlen($id) != Survey::UID_LENGTH)
                throw new RestBadParameterException('id');

            $survey = Survey::fromId($id);

            //Get current user info on this survey
            $current_user = SurveyUserUtil::getCurrentUser($survey);

            if (isset($sub_resource)) {
                return self::getSubResource($survey, $current_user, $sub_resource, $sub_resource_id, $sub_resource_modifier);
            }

            //No roles -> Not allowed
            if (SurveyUserRoles::NONE == $current_user['role'])
                throw new RestNotAllowedException();

            //Apply data restriction with roles
            $restricted_survey = self::cast($survey, $current_user);

            $surveys = array($restricted_survey);
        }
        return $surveys;
    }

    /**
     * Update survey
     *
     * @param int $id survey id
     *
     * Request body must be an object with properties to update
     *
     * @return array
     * @throws RestMissingParameterException
     * @throws SurveyActionNotAllowedException
     * @throws SurveyClosedException
     */
    public static function put($id = null) {
        if (!$id)
            throw new RestMissingParameterException('id');

        $data = RestRequest::getInput();

        // Get survey and update data
        $survey = Survey::fromId($id);

        //Get current user role
        $current_user = SurveyUserUtil::getCurrentUser($survey);

        // Check permissions : only users with role creator can create survey
        if (SurveyUserRoles::OWNER !== $current_user['role']) {
            //If not owner because not authenticated, we know the reason
            if(!Auth::user())
                $current_user['reason'] = 'requires_authentication';

            throw new SurveyActionNotAllowedException('update survey', $current_user['reason']);
        }

        // Closed survey should not be update
        if ($survey->is_closed)
            throw new SurveyClosedException($survey, $current_user);

        // Check if survey is still tagged as draft in data
        if (!array_key_exists('is_draft', (array)$data)) {
            $data->is_draft = false;
        }

        $survey->update((array)$data);

        return array(
            'path' => '/survey/' . $survey->id,
            'data' => self::cast($survey, $current_user)
        );
    }

    /**
     * creates survey
     *
     * Request body must be an object with properties
     *
     * @param null $id (optionnal) when posting an action
     * @param null $action (optionnal) within (duplicate|close)
     * @return array
     * @throws SurveyActionNotAllowedException
     */
    public static function post($id = null, $action = null) {

        //If id is set, there may be an action required on survey
        if (!is_null($id)) {
            //Id is set
            $current_survey = Survey::fromId($id);

            //Get current user role
            $current_user = SurveyUserUtil::getCurrentUser($current_survey);

            //There must be an action
            if (!isset($action))
                throw new SurveyActionNotAllowedException('Survey');

            switch ($action) {
                case 'duplicate' :
                    //Only more than replier can duplicate
                    if ($current_user['role'] < SurveyUserRoles::REPLIER)
                        throw new SurveyActionNotAllowedException('Survey');

                    //We duplicate data
                    $data = (array)json_decode(json_encode(SurveyEndpoint::cast($current_survey, $current_user)));
                    //Newly created survey will be a draft
                    $data['id'] = '';
                    $data['is_draft'] = true;
                    //Reiniting auto_close date
                    $data['settings']->auto_close = null;

                    break;
                case 'send_invitations_reminder' :
                    //Only more than Owner can send reminder
                    if ($current_user['role'] < SurveyUserRoles::OWNER)
                        throw new SurveyActionNotAllowedException('Survey');

                    //We send a reminder to all guest (+ if asked to owner)
                    return $current_survey->sendInvitations(true, true, ParticipantNotificationReasons::REMINDER);
                    break;
                case 'close' :
                    //Only more than Owner can close the survey
                    if ($current_user['role'] < SurveyUserRoles::OWNER)
                        throw new SurveyActionNotAllowedException('Survey');

                    return $current_survey->close(null, 'requested_by_owner_reason');
                    break;
                case 'reopen' :
                    //Only more than Owner can close the survey
                    if ($current_user['role'] < SurveyUserRoles::OWNER
                        || !$current_survey->isClosed())
                        throw new SurveyActionNotAllowedException('Survey');

                    return $current_survey->reopen((array)RestRequest::getInput());
                    break;
                default :
                    throw new SurveyActionNotAllowedException($action . ' survey');
            }

        } else {
            $data = (array)RestRequest::getInput();
        }

        //Get current user role
        $current_user = SurveyUserUtil::getCurrentUser();

        // Check permissions : only users with role creator can create survey
        if (SurveyUserRoles::CREATOR !== $current_user['role'])
            throw new SurveyActionNotAllowedException('create survey', $current_user['reason']);

        // Check it can already exists if it's a draft
        if (array_key_exists('id', $data) && $data['id']) {

            try {
                $survey = Survey::fromId($data['id']);

                //Update current user, it must be the owner to continue
                $current_user = SurveyUserUtil::getCurrentUser($survey);

                //We can only update drafts by this method
                if (SurveyUserRoles::OWNER !== $current_user['role']
                    || !$survey->is_draft
                )
                    throw new SurveyActionNotAllowedException('update draft');

                $survey->update($data);
            } catch (NotFoundException $e) {
                //the given id doesn't match an existing survey

                $survey = Survey::create($data);
            }
        } else {
            $survey = Survey::create($data);
        }

        return array(
            'path' => '/survey/' . $survey->id,
            'data' => self::cast($survey, $current_user)
        );
    }

    /**
     * Delete survey
     *
     * @param type $id
     * @return boolean
     * @throws RestMissingParameterException
     * @throws RestNotAllowedException
     */
    public static function delete($id = null) {

        if (!$id)
            throw new RestMissingParameterException('id');

        // Get survey 
        $survey = Survey::fromId($id);

        //Get current user info
        $current_user = SurveyUserUtil::getCurrentUser($survey);

        // Check permissions
        if (SurveyUserRoles::OWNER !== $current_user['role'])
            throw new RestNotAllowedException('delete survey ' . $survey->id);

        $survey->delete();

        return true;
    }

    /**
     * Casts a Survey into to an array for response
     *
     * @param Survey $survey
     * @param array $current_user
     * @return array
     * @throws RestNotAllowedException
     */
    public static function cast(Survey $survey, $current_user) {


        // Do not add survey_id to vote response unless vote id is ungessable, it would otherwise allow to easily discover surveys
        $data = array(
            'id' => $survey->id,
            'title' => $survey->title,
            'path' => $survey->path,
            'place' => $survey->place,
            'description' => $survey->raw_description,
            'created' => RestUtilities::formatDate($survey->created),
            'updated' => RestUtilities::formatDate($survey->updated),
            'closed' => RestUtilities::formatDate($survey->closed),
            'settings' => (array)$survey->settings,
            'questions' => array(),
            'is_draft' => $survey->is_draft,
            'is_closed' => $survey->is_closed
        );

        //We get all questions
        //Then we cast every question to array
        foreach ($survey->Questions as $question) {
            $data['questions'][] = Question::cast($question, false);
        }

        //If user is the owner or creator (or admin)
        //Owners
        $data['owners'] = array();
        foreach ($survey->Owners as $owner) {
            $data['owners'][] = array(
                'name' => $owner->name,
                'email' => $owner->email
            );
        }

        //If user is the owner or creator (or admin)
        if (SurveyUserRoles::OWNER <= $current_user['role']) {
            //Guests
            $data['guests'] = array();
            foreach ($survey->Guests as $guest) {
                $data['guests'][] = array('email' => $guest->email);
            }

            //Number of participants
            $data['nb_participants'] = count($survey->Participants);

            //Max auto_close date
            $settings = SurveySettings::getSettings($survey);

            //Max and min should be midnight utc
            $data['settings']['auto_close_max'] = ($settings['auto_close']['properties']['max'] - ($settings['auto_close']['properties']['max'] % 86400)) ;
            $data['settings']['auto_close_min'] = ($settings['auto_close']['properties']['min'] - ($settings['auto_close']['properties']['min'] % 86400)) ;

        }

        return $data;
    }

    /**
     * Get subresource of a Survey
     * @param Survey $survey the survey
     * @param array $current_user
     * @param string $sub_resource in 'participant', 'participants'
     * @param string $sub_resource_id
     * @param string $sub_resource_modifier 'comments', 'answers'
     * @return array
     * @throws RestMethodNotImplementedException
     * @throws RestNotAllowedException
     */
    private static function getSubResource(Survey $survey, array $current_user, $sub_resource, $sub_resource_id, $sub_resource_modifier) {
        switch (strtolower($sub_resource)) {
            case 'participant' :
            case 'participants' :
                return static::getSubResourceParticipant($survey, $current_user, $sub_resource_id, $sub_resource_modifier);
                break;
            case 'results' :
                if (isset($current_user['can']['view_answers'])
                    && $current_user['can']['view_answers']) {
                    return $survey->getResults();
                }
                // Legitimate user should not get here
                throw new RestNotAllowedException();

            default :
                throw new RestMethodNotImplementedException();
        }
    }

    /**
     * Get Participants as a sub resource of Survey
     * @param Survey $survey
     * @param array $current_user
     * @param $sub_resource_id
     * @param $sub_resource_modifier
     * @return array
     * @throws ModelViolationException
     * @throws ParticipantNotFoundException
     * @throws RestBadParameterException
     * @throws RestMethodNotImplementedException
     * @throws RestNotAllowedException
     */
    private static function getSubResourceParticipant(Survey $survey, array $current_user, $sub_resource_id, $sub_resource_modifier) {
        if (isset($sub_resource_id)) { //A participant
            //Check if sub_resource_id is not a macro param
            if ($sub_resource_id == '@emails' || $sub_resource_id == '@gecos') {
                if(SurveyUserRoles::OWNER != $current_user['role'])
                    throw new RestNotAllowedException();

                // User wants to get emails of participants
                $emails = array();

                //Checking selectors
                $selectors = RestRequest::getOutputProperties();
                if($selectors['filter']) { // It's possible to get participant email for a proposition and a value
                    $participants = static::getSubResourceParticipantFiltered($selectors);
                } else { // No recognized filtering => all participants
                    $participants = $survey->Participants;
                }

                //We each participant email or gecos
                foreach($participants as $participant) {
                    //Ultimate participant should not be processed
                    if (ParticipantTypes::ULTIMATE === $participant->type)
                        continue;

                    if($participant->Survey->id === $survey->id && $participant->email) {
                        $email = $participant->email;
                        if($sub_resource_id == '@gecos') {
                            $email = $participant->name.' <'.$participant->email.'>';
                        }
                        $emails[] = $email;
                    }
                }
                return $emails;

            } else { // sub_resource_id is an id of participant
                $participant = Participant::getParticipant($survey, array());

                $declared_participant = Participant::fromId(array('id' => $sub_resource_id, 'uid' => $participant->uid, 'type' => $participant->type));

                //Only current participant or survey owner can access participant
                if(SurveyUserRoles::OWNER != $current_user['role']
                    && $declared_participant->id != $participant->id)
                    throw new RestNotAllowedException();

                if (isset($sub_resource_modifier)) {
                    if('answers' === $sub_resource_modifier) {
                        return array_map(array(Answer::name(), 'cast'), $participant->Answers);
                    } else {
                        throw new RestMethodNotImplementedException();
                    }
                } else {
                    return Participant::cast($participant);
                }
            }

        } else if (isset($current_user['can']['view_answers'])
            && $current_user['can']['view_answers']) {
            //All participants
            return Participant::castCollection($survey->Participants, $current_user);
        }

        // Legitimate user should not get here
        throw new RestNotAllowedException();
    }

    /**
     * Get filtered Participants (hasAnswered yes to proposition X)
     * @param $selectors
     * @return array
     * @throws ModelViolationException
     * @throws RestBadParameterException
     */
    private static function getSubResourceParticipantFiltered($selectors) {
        $selectors['filter']->addCustomTest('hasAnswered',
            function($arguments) {
                $arguments = (array) $arguments;

                if(!array_key_exists('value', $arguments)
                    || !array_key_exists('proposition_id', $arguments)
                    || !array_key_exists('question_id', $arguments))
                    throw new RestBadParameterException('filter : hasAnswered');

                $short_value = $arguments['value'];
                $proposition_id = $arguments['proposition_id'];
                $question_id = $arguments['question_id'];

                $value = 'selected_value_'.$short_value;
                $restricted_question = ' AND question_id='.$question_id.' AND participant_type != "'.ParticipantTypes::ULTIMATE.'"';


                //Compatibility query (proposition_id as string and is now int)
                $query = '(JSON_CONTAINS(choices, \'{\"proposition_id\":\"'.$proposition_id.'\",\"value\":\"'.$value.'\"}\') OR JSON_CONTAINS(choices, \'{\"proposition_id\":'.$proposition_id.',\"value\":\"'.$value.'\"}\'))';
                return $query.$restricted_question;
            },
            true
        );
        //Getting matching answers
        $answers = RestUtilities::getEntities('Answer', array(), $selectors);
        //Getting corresponding participants
        return array_map(function ($answer) {
            return $answer->Participant;
        }, $answers);
    }

    /**
     * Get surveys corresponding to macro_param
     * @param string $macro_param (empty for all surveys, '@answered' for answered surveys, '@guest' for surveys where user is guest ...
     * @return array
     * @throws RestMethodNotImplementedException
     * @throws RestNotAllowedException
     */
    private static function getSurveys($macro_param = '') {
        $surveys = array();
        $current_user = array(
            'role' => SurveyUserRoles::OWNER
        );

        switch ($macro_param) {
            case '':
                //An admin can get all the surveys
                if (Auth::isAdmin()) {
                    foreach (Survey::all() as $survey) {
                        $surveys[] = self::cast($survey, $current_user);
                    }
                } else {
                    foreach (Survey::ownedBy(Auth::user()) as $survey) {
                        $surveys[] = self::cast($survey, $current_user);
                    }
                }
                break;
            case '@guest':
                foreach (Survey::fromGuestUser(Auth::user()) as $survey) {
                    $surveys[] = self::cast($survey, $current_user);
                }
                break;
            case '@answered':
                foreach (Survey::answeredBy(Auth::user()) as $survey) {
                    $surveys[] = self::cast($survey, $current_user);
                }
                break;
            case '@feed':
            default :
                throw new RestMethodNotImplementedException();
        }
        return $surveys;
    }

}
