<?php

/**
 *     Moment - ParticipantRecoveryEndpoint.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class ParticipantRecoveryEndpoint
 *
 * Rest Endpoint for participants
 *
 * POST /answer  to create an answer
 * PUT /answer/:id to update an answer
 */
class ParticipantRecoveryEndpoint extends RestEndpoint {

    const SEND_CODE = 'send_code';
    const PARTICIPANT_TOKEN = 'token';
    const SEND_LINK = 'send_link';

    /**
     * Get participants
     *
     * Not allowed to list participants
     *
     * @throws RestNotAllowedException
     */
    public static function get() {
        throw new RestNotAllowedException();
    }

    /**
     * Update participant
     *
     * @param int $id
     * @throws RestNotAllowedException
     */
    public static function put($id = null) {
        throw new RestNotAllowedException();
    }

    /**
     * Deletes participant
     *
     * @param int $id
     * @throws RestNotAllowedException
     */
    public static function delete($id = null) {
        throw new RestNotAllowedException();
    }

    /**
     * Sends a email containing the participant custom link
     * for the survey to the participant
     *
     * Participant (identified by participant id) must exist
     *
     * @param string $sub_resource ('token')
     * @return array
     * @throws ParticipantMissingPropertyException
     * @throws RestNotAllowedException
     */
    public static function post($sub_resource) {

        // Getting request inputs
        $data = RestRequest::getInput();

        if (!Auth::user()
            && (static::SEND_CODE == $sub_resource
             || static::PARTICIPANT_TOKEN == $sub_resource
             || static::SEND_LINK)) {

            if (!property_exists($data, 'survey_id'))
                throw new ParticipantMissingPropertyException('survey_id', $data);

            if (!property_exists($data, 'participant_email'))
                throw new ParticipantMissingPropertyException('participant_email', $data);

            try {
                $survey = Survey::fromId($data->survey_id);

                //To ask for token email $survey should not allow answer edition (no other case identified yet)
                if(static::SEND_LINK === $sub_resource) {
                    return ParticipantRecoveryUtil::sendLinkToParticipant($survey, $data->participant_email);
                }

                $participant = Participant::fromSurveyAndEmail($survey, $data->participant_email);

                //From here participant must be declarative
                if(ParticipantTypes::DECLARATIVE !== $participant->type)
                    throw new RestNotAllowedException('invalid_participant');

                //Asking for a new code
                if(static::SEND_CODE === $sub_resource) {
                    return ParticipantRecoveryUtil::sendCodeForParticipant($participant);
                }

                //To get a token by code you must have a code
                if(static::PARTICIPANT_TOKEN === $sub_resource && ParticipantRecoveryUtil::hasStoredCode($participant)) {
                    if (!property_exists($data, 'code'))
                        throw new ParticipantMissingPropertyException('code', $data);

                    if(ParticipantRecoveryUtil::isCodeValid($participant, $data->code)) {
                        return array(
                            'participant_token' => $participant->uid
                        );
                    } else {
                        return array(
                            'tries_left' => ParticipantRecoveryUtil::getTriesLeft($participant)
                        );
                    }
                }
            } catch (ParticipantNotFoundException $e) {
                //Participant has not been found
            } catch (SurveyNotFoundException $e) {
                //Nothing to do here
            }
        }

        throw new RestNotAllowedException();
    }
}
