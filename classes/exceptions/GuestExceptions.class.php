<?php

/**
 *     Moment - GuestExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * No id found for survey, therefore guest cannot be created
 */
class GuestSurveyNotSavedException extends DetailedException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('guest_survey_not_saved');
    }
}

/**
 * Guest email is not a valid email
 */
class GuestInvalidEmailException extends DetailedException {
    /**
     * Constructor
     */
    public function __construct($guest_email) {
        parent::__construct('guest_invalid_email',
            array('property' => 'email', 'data' => $guest_email));
    }
}
