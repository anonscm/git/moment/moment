<?php

/**
 *     Moment - AnswerExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


/**
 * Missing property
 */
class AnswerMissingPropertyException extends DetailedException {

    /**
     * Constructor
     * @param string $property
     * @param array $data
     */
    public function __construct($property, $data) {
        parent::__construct(
            'missing_mandatory_property',
            array('property' => $property, 'data' => $data)
        );
    }
}

/**
 * Bad property
 */
class AnswerBadPropertyException extends DetailedException {
    /**
     * Constructor
     * @param string $property
     * @param array $data
     */
    public function __construct($property, $data) {
        parent::__construct(
            'bad_property',
            array('property' => $property, 'data' => $data)
        );
    }
}

/**
 * Missing answer
 */
class AnswerMissingException extends DetailedException {
    /**
     * Constructor
     * @param string $question
     * @param array $data
     * @internal param string $message
     */
    public function __construct($question, $data) {
        parent::__construct(
            'missing_answer',
            array('question' => $question, 'data' => $data)
        );
    }
}

/**
 * Choices made in an answer don't match the question
 */
class AnswerBadChoicesException extends DetailedException {
    /**
     * Constructor
     * @param string $message
     * @param array $data
     */
    public function __construct($question, $data) {
        parent::__construct(
            'answer_bad_choices',
            array('question' => $question, 'data' => $data)
        );
    }
}

/**
 * Choice is not available
 */
class AnswerPropositionNotAvailableException extends DetailedException {
    /**
     * Constructor
     * @param string $message
     * @param array $data
     */
    public function __construct($proposition, $data) {
        parent::__construct(
            'answer_proposition_not_available',
            array('proposition' => $proposition, 'data' => $data)
        );
    }
}

/**
 * Class AnswerSurveyClosedException
 *
 * Thrown when answering to a closed survey
 */
class AnswerSurveyClosedException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('answer_survey_closed', 403);
    }
}

/**
 * Class AnswerDraftSurveyException
 *
 * Thrown when answering to a draft survey
 */
class AnswerDraftSurveyException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('answer_draft_survey', 403);
    }
}