<?php

/**
 *     Moment - MomentExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Missing property
 */
class MomentRestNotAllowedException extends RestException {

    /**
     * Constructor
     */
    public function __construct($details = '') {
        parent::__construct('rest_not_allowed', 403, $details);
    }
}

class MomentHttpException extends Exception {
    /**
     * Constructor
     *
     * @param string $message message id to return to the interface
     * @param int $code http error code
     * @param string $page the page where it happened (for logging)
     */
    public function __construct($message, $page, $code) {
        if(404 !== $code) {
            $uid = ApplicationContext::getUID();
            Logger::error('['.get_class($this).':'.$uid.':exception] '.$message);
            Logger::error('['.get_class($this).':'.$uid.':details] page = '. $page);
        }

        parent::__construct(
            $message,
            $code
        );
        $this->code = $code;
    }
}

class MomentPageNotFoundException extends MomentHttpException {
    /**
     * Constructor
     *
     * @param string $page the page where it happened (for logging)
     */
    public function __construct($page) {
        parent::__construct(
            'page_not_found',
            $page,
            404
        );
    }
}

class MomentForbiddenException extends MomentHttpException {
    /**
     * Constructor
     *
     * @param string $page the page where it happened (for logging)
     */
    public function __construct($page) {
        parent::__construct(
            'access_denied',
            $page,
            403
        );
    }
}

class MomentPleaseLoginException extends Exception {
    /**
     * Constructor
     *
     * @param string $page the page where it happened (for logging)
     */
    public function __construct($page) {
        parent::__construct(
            'please_login',
            403
        );
    }
}


