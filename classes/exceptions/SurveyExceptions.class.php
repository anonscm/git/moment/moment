<?php

/**
 *     Moment - SurveyExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');


/**
 * Bad choicegroup
 */
class SurveyBadChoiceGroupException extends DetailedException {
    /**
     * Constructor
     *
     * @param mixed $choicegroup
     */
    public function __construct($choicegroup) {
        parent::__construct(
            'survey_bad_choicegroup',
            $choicegroup
        );
    }
}

/**
 * Class SurveyBadPropertyException
 */
class SurveyBadPropertyException extends DetailedException {
    /**
     * Constructor
     *
     * @param mixed $choice
     */
    public function __construct($property, $data) {
        parent::__construct(
            'bad_property',
            array('property' => $property, 'data' => $data)
        );
    }
}

/**
 * Class SurveyMissingPropertyException
 */
class SurveyMissingPropertyException extends DetailedException {
    /**
     * Constructor
     *
     * @param mixed $choice
     */
    public function __construct($property, $data) {
        parent::__construct(
            'missing_mandatory_property',
            array('property' => $property, 'data' => $data)
        );
    }
}

/**
 * Class SurveyMissingSettingPropertyException
 */
class SurveyMissingSettingPropertyException extends DetailedException {
    /**
     * Constructor
     *
     * @param mixed $choice
     */
    public function __construct($property) {
        parent::__construct(
            'missing_mandatory_setting_property',
            array('property' => $property)
        );
    }
}

/**
 * Class SurveyBaseSettingOverrideNotAllowedException
 */
class SurveyBaseSettingOverrideNotAllowedException extends DetailedException {
    /**
     * Constructor
     *
     * @param string $setting
     */
    public function __construct($setting) {
        parent::__construct(
            'overriding_base_setting_not_allowed',
            array('setting' => $setting)
        );
    }
}

/**
 * Class SurveyActionNotAllowedException
 */
class SurveyActionNotAllowedException extends DetailedException {
    /**
     * Constructor
     * @param string $what
     * @param string $why
     */
    public function __construct($what = '', $why = '') {

        $details = array();
        if($what)
            $details['action'] = $what;
        if($what)
            $details['reason'] = $why;

        $context = RestRequest::getContext();
        if($why != '')
            $context['reason'] = $why;

        parent::__construct(
            'survey_action_not_allowed',
            $details,
            $context
        );
        $this->code = 403;
    }

}

/**
 * Class SurveyAccessDeniedException
 */
class SurveyAccessDeniedException extends DetailedException {

    private $survey;
    private $current_user;

    /**
     * Constructor
     *
     * @param Survey $survey
     * @internal param string $setting
     */
    public function __construct(Survey $survey, array $current_user) {
        $this->survey = $survey;
        $this->current_user = $current_user;
    }

    /**
     * Getter
     * @param $property
     * @return mixed
     */
    public function __get($property) {
        if (in_array($property, array('survey', 'current_user'))) {
            return $this->$property;
        }
        return parent::__get($property);
    }
}

/**
 * Class SurveyClosedException
 */
class SurveyClosedException extends Exception {

    private $survey = null;
    private $current_user;

    /**
     * Constructor
     *
     * @param Survey $survey
     * @param array $current_user
     * @internal param string $setting
     */
    public function __construct(Survey $survey, array $current_user) {
        $this->survey = $survey;
        $this->current_user = $current_user;

        Logger::error('['.get_class($this).':'.ApplicationContext::getUID().'] [survey:'.$survey->id.'] survey_is_closed');
    }

    /**
     * Getter
     * @param $property
     * @return mixed
     */
    public function __get($property) {
        if (in_array($property, array('survey', 'current_user'))) {
            return $this->$property;
        }
    }
}

/**
 * Class SurveyNoOwnerException
 */
class SurveyNoOwnerException extends DetailedException {
    /**
     * Constructor
     *
     * @param mixed $choice
     */
    public function __construct($data) {
        parent::__construct(
            'missing_owner',
            array('data' => $data)
        );
    }
}

/**
 * Class SurveyNotFoundException
 */
class SurveyNotFoundException extends LoggingException {
    /**
     * Constructor
     *
     * @param string $setting
     */
    public function __construct($data) {

        $details = print_r($data, true);

        if(is_array($data) && array_key_exists('id', $data))
            $details = '[survey:' . $data['id'] . '] ';

        parent::__construct(
            'survey_not_found',
            array('details' => $details)
        );
    }

}