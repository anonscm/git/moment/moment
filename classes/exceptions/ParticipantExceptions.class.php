<?php

/**
 *     Moment - ParticipantExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
class ParticipantNotFoundException extends Exception {
    /**
     * Constructor
     * @param string $survey
     * @param string $data
     */
    public function __construct($survey, $data = '') {
        parent::__construct('no_participant_found');
    }
}

/**
 * Missing property
 */
class ParticipantMissingPropertyException extends DetailedException {

    /**
     * Constructor
     * @param string $property
     * @param array $data
     */
    public function __construct($property, $data) {
        parent::__construct(
            'missing_mandatory_property',
            array('property' => $property, 'data' => $data)
        );
    }
}

/**
 * Class ParticipantBadPropertyException
 */
class ParticipantBadPropertyException extends DetailedException {
    /**
     * Constructor
     * @param string $property
     * @param array $data
     */
    public function __construct($property, $data) {
        parent::__construct(
            'bad_property',
            array('property' => $property, 'data' => $data)
        );
    }
}

class ParticipantAlreadyAnsweredException extends DetailedException {
    /**
     * Constructor
     * @param Participant $participant
     */
    public function __construct(Participant $participant = null) {
        if ($participant) {
            parent::__construct('participant_has_already_answered_to_survey',
                array('participant_id' => $participant->id, 'survey_id' => $participant->Survey->id));
        } else {
            parent::__construct('participant_has_already_answered_to_survey');
        }
    }
}

/**
 * Partipant already exists with this declarative identity
 */
class ParticipantAlreadyExistsException extends DetailedException {
    /**
     * Constructor
     */
    public function __construct($type, $editable_answers = true) {
        parent::__construct('participant_already_exists_for_survey', null, array('participant_type' => $type, 'editable_answers' => $editable_answers), 409);
    }
}

/**
 * Participant token is invalid
 */
class ParticipantInvalidTokenException extends LoggingException {
    /**
     * Constructor
     * @param $survey
     * @param $token
     */
    public function __construct($survey, $token) {
        parent::__construct('participant_invalid_token',
            array('message' => 'participant_invalid_token, survey_id:'.$survey->id.', token:'.$token.''));
    }
}

/**
 * Class ParticipantNotAllowedException
 *
 * Thrown when answering to a survey without replier role
 */
class ParticipantNotAllowedException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('participant_not_allowed', 403);
    }
}