<?php

/**
 *     Moment - QuestionExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Bad Type for question
 */
class QuestionBadTypeException extends DetailedException {
    /**
     * Constructor
     */
    public function __construct($type) {
        parent::__construct('bad_type_for_question',
            array('type' => $type));
    }
}

class QuestionBadPropertyException extends DetailedException {
    /**
     * Constructor
     *
     * @param string $property
     * @param mixed|null $data
     * @internal param mixed $choice
     */
    public function __construct($property, $data) {
        parent::__construct(
            'bad_property',
            array('property' => $property, 'data' => $data)
        );
    }
}

class QuestionMissingPropertyException extends DetailedException {
    /**
     * Constructor
     *
     * @param string $property
     * @param mixed|null $data
     * @internal param mixed $choice
     */
    public function __construct($property, $data) {
        parent::__construct(
            'missing_mandatory_property',
            array('property' => $property, 'data' => $data)
        );
    }
}

/**
 * No id found for survey, therefore question cannot be created
 */
class QuestionSurveyNotSavedException extends DetailedException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('question_survey_not_saved');
    }
}

/**
 * Trying to access question through a forbidden method
 */
class QuestionCrossAccessException extends DetailedException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('question_cross_access');
    }
}
