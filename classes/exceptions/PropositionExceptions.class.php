<?php

/**
 *     Moment - PropositionExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * No id found for question, therefore proposition cannot be created
 */
class PropositionQuestionNotSavedException extends DetailedException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('proposition_question_not_saved');
    }
}

/**
 * A mandatory property is missing for a proposition
 */
class PropositionMissingPropertyException extends DetailedException {
    /**
     * Constructor
     *
     * @param string $property
     * @param mixed|null $data
     * @internal param mixed $choice
     */
    public function __construct($property, $data) {
        parent::__construct(
            'missing_mandatory_property',
            array('property' => $property, 'data' => $data)
        );
    }
}

/**
 * Trying to access Proposition through a forbidden method
 */
class PropositionCrossAccessException extends DetailedException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('proposition_cross_access');
    }
}

/**
 * Trying to get an unknown proposition (no logging)
 */
class PropositionNotFoundException extends Exception {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('proposition_not_found');
    }
}
