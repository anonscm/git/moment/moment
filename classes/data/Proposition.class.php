<?php

/**
 *     Moment - Proposition.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class Proposition
 */
abstract class Proposition extends Entity {

    /**
     * Database map
     */
    protected static $dataMap = array(
        'id' => array(
            'type' => 'uint',
            'size' => ' ', // This is a tricks to use INT (and not medium or big)
            'primary' => true,
            'autoinc' => true
        ),
        'options' => array(
            'type' => 'text',
            'default' => '[]',
            'transform' => 'json'
        )
    );

    /**
     * Properties
     */
    protected $id = null;
    protected $options = array();
    protected $is_available = null;

    private $constraints_applied = array();

    /**
     * Get all propositions from a question
     * @param Question $question
     * @return array
     */
    public static function fromQuestion(Question $question) {
        return static::all('question_id = :question_id', array(':question_id' => $question->id));
    }

    /**
     * Create a new TextProposition or DateProposition
     *
     * @param Question $question
     * @return static
     * @throws PropositionQuestionNotSavedException
     */
    protected static function create(Question $question, array $proposition_data = array()) {

        //Check if question has an id
        if (!$question->__get('storedInDatabase'))
            throw new PropositionQuestionNotSavedException();

        $proposition = new static();

        $proposition->Question = $question;

        $proposition->storeConstraints($proposition_data);

        return $proposition;
    }

    /**
     * Create Proposition if not exists
     *
     * @param Question $question
     * @param array $proposition_data
     * @return Proposition
     * @throws PropositionQuestionNotSavedException
     */
    public static function createIfNotExists(Question $question, array $proposition_data = array()): Proposition {
        try {
            return static::fromValues($question, $proposition_data);
        } catch(PropositionNotFoundException $nfe) {
            //Proposition does not exist, Creates it and add it to question
            return static::create($question, $proposition_data);
        }
    }

    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return property value
     */
    public function __get($property) {
        if (in_array($property, array(
            'id', 'question_id', 'options'
        ))) {
            return $this->$property;
        }

        if ($property === 'is_available') {
            if (is_null($this->is_available)) {
                //Applying constraints to check availability
                $this->applyConstraints();

                //
                if (is_null($this->is_available)) {
                    $this->is_available = true;
                }
            }
            return $this->is_available;
        }

        return parent::__get($property);
    }

    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if (in_array($property, array(
            'id', 'question_id', 'options'
        ))) {
            $this->$property = $value;
        } else {
            parent::__set($property, $value);
        }
    }

    /**
     * Update proposition.
     * (Actually only checks that we are updating something we are allowed to)
     * @param Question $question
     * @param array $proposition_data
     * @throws ParticipantBadPropertyException
     * @throws PropositionCrossAccessException
     */
    public function update(Question $question, array $proposition_data = array()) {
        if ($question->id
            !== $this->Question->id
        )
            throw new PropositionCrossAccessException();

        $this->storeConstraints($proposition_data);
    }

    /**
     * Get constraint by its name (FALSE otherwise)
     * @param string $constraint_type
     * @return bool
     */
    public function getConstraint($constraint_type = '') {
        if (count($this->options)
            && count($this->options['constraints'])
        ) {
            foreach ($this->options['constraints'] as $constraint) {
                if ($constraint_type === $constraint['type']) {
                    $constraint_classname = str_replace(' ', '', mb_convert_case(str_replace('_', ' ', strtolower($constraint['type'])), MB_CASE_TITLE)) . 'Constraint';

                    if (array_key_exists($constraint_classname, $this->constraints_applied)) {
                        return $this->constraints_applied[$constraint_classname];
                    }

                    if (class_exists($constraint_classname)) {
                        return new $constraint_classname($constraint);
                    }
                }
            }
        }
        return false;
    }

    /**
     * Apply constraints on question
     */
    public function applyConstraints() {
        if (count($this->options)
            && count($this->options['constraints'])
        ) {
            foreach ($this->options['constraints'] as $constraint) {
                $constraint_classname = str_replace(' ', '', mb_convert_case(str_replace('_', ' ', strtolower($constraint['type'])), MB_CASE_TITLE)) . 'Constraint';

                //We apply a constraint only once (if two constraint of the same type the first wins)
                if (!array_key_exists($constraint_classname, $this->constraints_applied)
                    && class_exists($constraint_classname)
                    && method_exists($constraint_classname, 'isValidated')
                ) {

                    $constraint_class = new $constraint_classname($constraint);

                    $this->is_available = $constraint_class->isValidated($this);

                    //Storing applied constraint
                    $this->constraints_applied[$constraint_classname] = $constraint_class;
                }
            }
        }
    }

    /**
     * Store constraints contained in data
     * @param array $proposition_data
     * @throws ParticipantBadPropertyException
     */
    private function storeConstraints(array $proposition_data) {

        //Reinit constraints
        $this->options['constraints'] = array();

        $new_constraints = array();

        //No option => no constraint
        if (!array_key_exists('options', $proposition_data)
            || empty($proposition_data['options']))
            return;

        // To object
        $proposition_data['options'] = (object)$proposition_data['options'];

        if (!property_exists($proposition_data['options'], 'constraints') ||
            empty($proposition_data['options']->constraints))
            return;

        foreach ($proposition_data['options']->constraints as $constraint_data) {
            $constraint_data = (object)$constraint_data;
            if (!property_exists($constraint_data, 'type') || empty($constraint_data->type))
                throw new ParticipantBadPropertyException('options.constraints.type');

            $constraint_classname = str_replace(' ', '',
                    mb_convert_case(str_replace('_', ' ',
                        strtolower($constraint_data->type)), MB_CASE_TITLE)) . 'Constraint';

            if (!class_exists($constraint_classname))
                throw new ParticipantBadPropertyException('options.constraints.type');

            //One constraint of one type can be stored on proposition, not more
            if (array_key_exists($constraint_classname, $new_constraints))
                continue;

            //Class exists
            $constraint = new $constraint_classname((array)$constraint_data);

            //Checking constraint consistency
            $constraint->isValid();

            $this->options['constraints'][] = $constraint->toArray();

            //Saving that we have this constraint
            $new_constraints[$constraint_classname] = $constraint;
        }

    }

}
