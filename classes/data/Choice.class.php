<?php

/**
 *     Moment - Choice.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class Choice
 *
 * @property-read Proposition $proposition
 * @property-read string $value
 */
class Choice implements JsonSerializable {

    /**
     * Count cache to avoid computing count twice
     * @var array
     */
    private static $countCache = array();

    /**
     * Properties
     */
    protected $proposition_id = null;
    protected $answer = null;
    protected $value = null;

    /**
     * Constructor
     * @param Answer $answer the related answer
     * @param mixed $raw_data [proposition_id => ..., value => ...] or choice
     */
    public function __construct(Answer $answer, $raw_data) {
        $this->answer = $answer;

        if (is_array($raw_data)) {
            $this->proposition_id = (int) $raw_data['proposition_id'];
            $this->value = $raw_data['value'];
        }

        if (is_object($raw_data)) {
            $this->proposition_id = (int) $raw_data->proposition_id;
            $this->value = $raw_data->value;
        }
    }

    /**
     * Get count for $value on question/proposition
     * Ex : How many participants answered 'selected_value_yes' on proposition X from question Y
     *
     * @param $question_id
     * @param $proposition_id
     * @param $value
     * @return mixed
     */
    public static function getValueCount($question_id, $proposition_id, $value) {

        /**
         * There may exists answers with older and bad format {..., "proposition_id":"<xxx>",...}
         * Good format is {..., "proposition_id":<xxx>,...}
         * We should count if anyway
         *
         * IMPORTANT : We keep both until all stored proposition_id are integers
         */
        //No need to instantiate participants nor answers for it
        if(!isset(self::$countCache[$question_id.$proposition_id.$value])) {
            self::$countCache[$question_id.$proposition_id.$value] = 0;
            self::initCountCache($question_id);
        }

        return self::$countCache[$question_id.$proposition_id.$value];
    }

    /**
     * Serialize Choice
     */
    public function jsonSerialize() {
        return array(
            'proposition_id' => $this->proposition_id,
            'value' => $this->value
        );
    }

    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return property value
     */
    public function __get($property) {
        if (in_array($property, array(
            'answer', 'proposition_id', 'value'
        ))) {
            return $this->$property;
        }

        if ($property === 'proposition') {
            $class = Question::getPropositionClass($this->answer->Question);

            return $class::fromId($this->proposition_id);
        }
    }

    /**
     * Init counts
     * @param $question_id
     */
    private static function initCountCache($question_id) {
        try {
            //Init counts
            $question = Question::fromId($question_id);
            $check = DBI::prepare('SELECT id FROM '.Question::getPropositionClass($question).'s WHERE question_id = :question_id');
            $check->execute(array(':question_id' => $question_id));
            foreach ($check->fetchAll(PDO::FETCH_COLUMN) as $proposition_id) {
                self::$countCache[$question_id . $proposition_id .YesNoMaybeValues::YES] = 0;
                self::$countCache[$question_id . $proposition_id .YesNoMaybeValues::NO] = 0;
                self::$countCache[$question_id . $proposition_id .YesNoMaybeValues::MAYBE] = 0;
            }

            //Actually count
            $check = DBI::prepare('SELECT choices FROM Answers WHERE question_id = :question_id AND participant_type != "'.ParticipantTypes::ULTIMATE.'"');
            $check->execute(array(':question_id' => $question_id));
            while ($row = $check->fetch(PDO::FETCH_NUM)) {
                foreach (json_decode($row[0]) as $choice) {
                    if(isset(self::$countCache[$question_id.$choice->proposition_id.$choice->value]))
                        self::$countCache[$question_id.$choice->proposition_id.$choice->value]++;
                }
            }
        } catch(NotFoundException $nfe) {
            //Nothing to do
        }

    }

}
