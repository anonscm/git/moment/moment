<?php

/**
 *     Moment - SurveyConstraint.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class SurveyConstraint
 */
abstract class SurveyConstraint {

    const LEVEL_WARNING = 'warning';
    const LEVEL_ALERT = 'alert';

    /**
     * Should apply the constraint on the object
     * @param $objectOfTheConstraint
     * @return bool
     */
    abstract public function isValidated($objectOfTheConstraint);

    /**
     * Should give an array representing constraint
     * @return bool
     */
    abstract public function toArray();

    /**
     * Check constraint consistency
     * @return bool
     */
    abstract public function isValid();
}