<?php

/**
 *     Moment - LimitChoiceConstraint.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class LimitChoiceConstraint
 */
class LimitChoiceConstraint extends SurveyConstraint {

    const WARNING_THRESHOLD = 25;

    private $limit;
    private $current;

    /**
     * Writes a custom sum text '($current/$limit)'
     *
     * @param $event
     * @return mixed|string
     */
    public static function initSumText($event) {
        $sum = array_shift($event->data);
        $proposition = array_shift($event->data);
        $constraint = $proposition->getConstraint('limit_choice');
        if ($constraint) {
            $sum = '<span class="' . $constraint->getLevel() . '">( <span class="real-sum">' . $sum . '</span> / ' . $constraint->limit . ' )</span>';
        }

        return $sum;
    }

    /**
     * LimitChoiceConstraint constructor.
     * @param array $constraint_data
     * @throws MissingArgumentException
     */
    public function __construct(array $constraint_data) {
        if (!array_key_exists('value', $constraint_data))
            throw new MissingArgumentException('constraint.value');

        $this->limit = $constraint_data['value'];
    }

    /**
     * Getter
     * @param $property
     * @return mixed
     */
    public function __get($property) {
        if (in_array($property, array(
            'limit', 'current'
        ))) {
            return $this->$property;
        }

        return parent::__get($property);
    }

    /**
     * Checks if proposition is still "chooseable"
     * @param $objectOfTheConstraint
     * @return bool
     */
    public function isValidated($objectOfTheConstraint) {
        if (($objectOfTheConstraint instanceof DateProposition
                || $objectOfTheConstraint instanceof TextProposition)
            && is_numeric($this->limit)
        ) {
            $choicesCount = Choice::getValueCount($objectOfTheConstraint->Question->id, $objectOfTheConstraint->id, YesNoMaybeValues::YES);

            $this->current = $choicesCount;
            return ($choicesCount < $this->limit);

        } else { //Not concerned by the constraint
            return true;
        }
    }

    /**
     * Compute usage level of this proposition
     * @return string LEVEL_ALERT, LEVEL_WARNING, ''
     */
    public function getLevel() {

        if ($this->current >= $this->limit) {
            return self::LEVEL_ALERT;
        }
        $left = $this->limit - $this->current;
        $percentageLeft = ($left * 100 / $this->limit);
        if ($percentageLeft <= self::WARNING_THRESHOLD) {
            return self::LEVEL_WARNING;
        }
        return '';
    }

    /**
     * Transforms this constraint into array
     * @return array
     */
    public function toArray() {
        return array(
            'type' => 'limit_choice',
            'value' => $this->limit
        );
    }

    /**
     * Checking constraint consistency
     */
    public function isValid() {
        if (!is_numeric($this->limit) || 0 > $this->limit)
            throw new ParticipantBadPropertyException('options.constraints[limit_choice_constraint] value too low', $this->limit);

    }
}