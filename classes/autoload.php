<?php

/**
 *     Moment - autoload.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Data classes
 */
Autoloader::addMapper('Choice', 'data/');
Autoloader::addMapper('*Constraint', 'data/');

/**
 * Model classes
 */
Autoloader::addMapper('Answer', 'model/');
Autoloader::addMapper('Comment', 'model/');
Autoloader::addMapper('Guest', 'model/');
Autoloader::addMapper('Owner', 'model/');
Autoloader::addMapper('EventLog', 'model/');
Autoloader::addMapper('Participant', 'model/');
Autoloader::addMapper('Question', 'model/');
Autoloader::addMapper('Survey', 'model/');
Autoloader::addMapper('*Proposition', 'model/');
Autoloader::addMapper('User', 'model/');
Autoloader::addMapper('Calendar', 'model/');
Autoloader::addMapper('Preference', 'model/');

Autoloader::addMapper('Proposition', 'data/');

/**
 * Constant classes
 */
Autoloader::addMapper('ParticipantTypes', 'constants/');
Autoloader::addMapper('ParticipantNotificationTypes', 'constants/');
Autoloader::addMapper('ParticipantNotificationReasons', 'constants/');
Autoloader::addMapper('QuestionTypes', 'constants/');
Autoloader::addMapper('QuestionAnswerTypes', 'constants/');
Autoloader::addMapper('QuestionSettings', 'constants/');
Autoloader::addMapper('PropositionTypes', 'constants/');
Autoloader::addMapper('EventLogTypes', 'constants/');

Autoloader::addMapper('Settings', 'constants/');
Autoloader::addMapper('ChoiceFieldTypes', 'constants/');
Autoloader::addMapper('SurveySettings', 'constants/');
Autoloader::addMapper('SurveyUserRoles', 'constants/');
Autoloader::addMapper('YesNoMaybeValues', 'constants/');
Autoloader::addMapper('SurveyFilters', 'constants/');
Autoloader::addMapper('CalendarStatusCodes', 'constants/');
Autoloader::addMapper('CalendarStatusMessages', 'constants/');
Autoloader::addMapper('IcsConstants', 'constants/');
Autoloader::addMapper('UserPreferences', 'constants/');

/**
 * Utility classes
 */
Autoloader::addMapper('KeyGenerator', 'utils/');
Autoloader::addMapper('*Util', 'utils/');
Autoloader::addMapper('MomentApplicationMail', 'utils/');
Autoloader::addMapper('SurveyStats', 'utils/');
Autoloader::addMapper('UserGuide', 'utils/');
Autoloader::addMapper('MomentUtilities', 'utils/');
Autoloader::addMapper('Faq', 'utils/');
Autoloader::addMapper('CalendarManager', 'utils/');
Autoloader::addMapper('CalendarUtil', 'utils/');
Autoloader::addMapper('HTTPClient', 'utils/');
Autoloader::addMapper('Ics', 'utils/');
Autoloader::addMapper('IcsEvent', 'utils/');
Autoloader::addMapper('EntityIterator', 'utils/');

/**
 * Form utility classes
 */
Autoloader::addMapper('Form*', 'utils/form/');
Autoloader::addMapper('FormFieldToggle', 'utils/form/');

/**
 * Form classes
 */
Autoloader::addMapper('ParticipantForm', 'forms/participant/');
Autoloader::addMapper('CalendarForm', 'forms/calendar/');
Autoloader::addMapper('PreferencesForm', 'forms/preferences/');

