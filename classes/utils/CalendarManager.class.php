<?php

/**
 *     Moment - CalendarManager.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Class CalendarManager
 */
class CalendarManager {

    private static $allowedTypes = array();

    private static $cacheContent = array();

    private static $cacheEvents = array();

    /**
     * Gte events of the external calendar
     * @param $url url of this calendar
     * @return mixed
     */
    public static function getEvents($url) {
        if(isset(self::$cacheEvents[$url]))
            return self::$cacheEvents[$url];

        self::$cacheEvents[$url] = array();
        $content = self::loadUrl($url);

        if(strlen($content)  > 0) {
            self::$cacheEvents[$url] = CalendarUtil::parse($content);
        }

        return self::$cacheEvents[$url];
    }

    /**
     * Get events of my Moment calendar
     * @param Survey|null $survey
     * @return mixed
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     */
    public static function getMyCalEvents(Survey $survey = null) {
        $myCalUrl = MyCalUtil::getMyCalUrl(Auth::user());
        if(isset(self::$cacheEvents[$myCalUrl]))
            return self::$cacheEvents[$myCalUrl];

        self::$cacheEvents[$myCalUrl] = CalendarUtil::parse(MyCalUtil::getIcsFromUser(Auth::user(), $survey));

        return self::$cacheEvents[$myCalUrl];
    }

    /**
     * Get my Moment calendar
     * @return mixed
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     */
    public static function getMyCalItem($complete = true) {
        $item = array(
            'id' => '@MyCal',
            'url' => MyCalUtil::getMyCalUrl(Auth::user()),
            'name' => Config::get('application_name'),
            'hash' => Auth::user()->calendar_hash,
            'settings' => array(
                'color' => Config::get('calendar.mycal_color')
            ),
        );

        if($complete) {
            $item['status'] = array(
                'code' => CalendarStatusCodes::OK,
                'message' => CalendarStatusMessages::OK
            );
        }

        return $item;
    }


    /**
     * Loads calendar at URL
     * @param $url string $url the requested url
     * @return mixed|string
     */
    public static function loadUrl($url) {
        if(isset(self::$cacheContent[$url]))
            return self::$cacheContent[$url];

        $ch = self::prepareCURLLoader($url);
        self::$cacheContent[$url] = $ch->execute();
        $status = self::getStatus($ch);
        $ch->close();

        //Storing results
        self::storeURLStatus($url, $status);

        if(CalendarStatusCodes::OK == $status['code']) {
            return self::$cacheContent[$url];
        } else { //If error on load then no content
            return '';
        }
    }

    /**
     * Check calendar URL status
     * @param string $url the requested url
     * @return array of the results as {code:<code>, message:<message>}
     * @throws Exception
     */
    public static function checkUrl($url) {
        $ch = self::prepareCURLLoader($url);
        $ch->execute();
        $status = self::getStatus($ch);
        $ch->close();

        Logger::debug('Checking calendar at URL : '.$url);
        Logger::debug('Result : '.json_encode($status));

        //Storing results
        self::storeURLStatus($url, $status);

        return $status;
    }

    /**
     * Get conflicts between $user calendars ans $survey propositions
     *
     * @param User $user
     * @param Survey $survey
     * @return array
     */
    public static function getConflicts(User $user, Survey $survey) {
        $conflicts = array();

        $externalCalendars = $user->Calendars;

        //If survey has no date question or user has no calendar there can't be conflicts
        if($survey->has_date_questions) {
            foreach ($survey->Questions as $question) {

                if(QuestionTypes::DATE != $question->type)
                    continue;

                if(!isset($conflicts[$question->id]))
                    $conflicts[$question->id] = array();

                foreach ($question->propositions as $proposition) {
                    $conflicts[$question->id][$proposition->id] = self::getConflictsForProposition($proposition, $externalCalendars);
                }
            }
        }

        return $conflicts;
    }

    /**
     * Get conflict for a $proposition in $calendars
     *
     * @param DateProposition $proposition
     * @param array $externalCalendars
     * @return array
     */
    private static function getConflictsForProposition(DateProposition $proposition, array $externalCalendars) {
        $conflicts = array();

        $propositionAsSlot = $proposition->asSlot();
        //External calendar
        foreach($externalCalendars as $calendar) {
            if($calendar->status['code'] != CalendarStatusCodes::OK)
                continue;

            foreach(self::getEvents($calendar->url) as $event) {
                if(self::isConflicting($event, $propositionAsSlot)) {
                    $event['calendar'] = CalendarEndpoint::castLight($calendar);
                    $conflicts[] = $event;
                }
            }
        }
        //Internal calendar
        foreach(self::getMyCalEvents($proposition->Question->Survey) as $event) {
            if(self::isConflicting($event, $propositionAsSlot)) {
                $event['calendar'] = self::getMyCalItem(false);
                $conflicts[] = $event;
            }
        }

        return $conflicts;
    }

    /**
     * Prepares a cURL handle to load an URL
     * @param $url
     * @return resource
     * @throws Exception
     */
    private static function prepareCURLLoader($url) {
        $ch = new HTTPClient();

        $ch->setOption(CURLOPT_RETURNTRANSFER, 1);
        $ch->setOption(CURLOPT_URL, $url);
        $ch->setOption(CURLOPT_CONNECTTIMEOUT, Config::get('calendar.timeout'));
        $ch->setOption(CURLOPT_TIMEOUT, Config::get('calendar.timeout'));
        $ch->setOption(CURLOPT_FOLLOWLOCATION, true);
        $ch->setOption(CURLOPT_USERAGENT, Config::get('application_name').'/'.Config::get('version'));

        // We need progress updates to break the connection mid-way
        $ch->setOption(CURLOPT_BUFFERSIZE, 128); // more progress info
        $ch->setOption(CURLOPT_NOPROGRESS, false);
        $ch->setOption(CURLOPT_PROGRESSFUNCTION, function (
            $resource, $downloadSize, $downloaded, $uploadSize, $uploaded
        ) {
            // If $downloaded exceeds calendar.max_size, returning non-0 breaks the connection!
            return ($downloaded > Config::get('calendar.max_size')) ? 1 : 0;
        });

        return $ch;
    }

    /**
     * Get Status form a cURL Handle
     * @param $ch a cURL handle
     * @return array
     */
    private static function getStatus($ch) {
        if (count(self::$allowedTypes) == 0)
            self::$allowedTypes = Config::get('calendar.allowed_types');

        //Default status
        $res = array(
            'code' => CalendarStatusCodes::UNKNOWN,
            'message' => CalendarStatusMessages::UNKNOWN
        );

        // Check if there are errors (example progress callback aborted for file size)
        if ($ch->getErrNo()) {
            switch($ch->getErrNo()) {
                case CURLE_ABORTED_BY_CALLBACK :
                    $res['code'] = CalendarStatusCodes::OVERSIZED;
                    $res['message'] = CalendarStatusMessages::OVERSIZED;
                    break;
                case CURLE_OPERATION_TIMEOUTED :
                    $res['code'] = CalendarStatusCodes::DOWN;
                    $res['message'] = CalendarStatusMessages::DOWN;
                    break;
                default:
                    $res['code'] = CalendarStatusCodes::ERROR;
                    $res['message'] = CalendarStatusMessages::ERROR;
                    break;
            }
            $res['details'] = array(
                'code' => $ch->getErrNo(),
                'msg' => $ch->getError()
            );
        } else {
            $httpStatusCode = $ch->getInfo(CURLINFO_HTTP_CODE);

            if (!in_array(substr($httpStatusCode, 0, 1), array('2', '3'))) {
                $res['code'] = CalendarStatusCodes::ERROR;
                $res['details'] = array(
                    'code' => $httpStatusCode
                );

                //If status code is known we can add more details to message
                switch ($httpStatusCode) {
                    case 401:
                    case 403:
                        $res['message'] = CalendarStatusMessages::FORBIDDEN;
                        break;
                    case 404:
                        $res['message'] = CalendarStatusMessages::NOT_FOUND;
                        break;
                    default:
                        $res['message'] = CalendarStatusMessages::ERROR;
                        break;
                }

            } else {
                $contentType = $ch->getInfo(CURLINFO_CONTENT_TYPE);

                $allowed = false;
                foreach(self::$allowedTypes as $type) {
                    if(strpos($contentType, $type) !== false) {
                        $allowed = true;
                        break;
                    }
                }
                if($allowed) {
                    $res['code'] = CalendarStatusCodes::OK;
                    $res['message'] = CalendarStatusMessages::OK;
                } else {
                    $res['code'] = CalendarStatusCodes::INCOMPATIBLE;
                    $res['message'] = CalendarStatusMessages::INCOMPATIBLE;
                }
            }
        }

        return $res;
    }

    /**
     * Store status for Calendar matching this $url
     * @param string $url the url
     * @param array $status
     */
    private static function storeURLStatus($url, $status) {
        //Storing result for every time this url matches a Calendar
        $calendars = Calendar::all('url = :url', array(':url' => $url));
        foreach($calendars as $calendar) {
            $calendar->status = $status;
            $calendar->checked = time();
            $calendar->save();
        }

    }

    private static function isConflicting($event, $propositionAsSlot) {
        $st = $event["start_ts"];
        $end = $event["end_ts"];

        // There are four possibilities to have a conflict
        // Event ends in proposition
        // Event starts in proposition
        // Event starts in proposition and ends in proposition (this case in covered by the two above)
        // Event starts before and ends after proposition
        return (($propositionAsSlot['start'] < $end && $end <= $propositionAsSlot['end'])
            || ($propositionAsSlot['start'] <= $st && $st < $propositionAsSlot['end'])
            || ($propositionAsSlot['start'] > $st && $end > $propositionAsSlot['end']));
    }
}
