<?php

/**
 *     Moment - UserGuide.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Class UserGuide
 */
class UserGuide {

    /**
     * @var boolean If UserGuide is enabled in configuration
     */
    private $enabled = false;

    /**
     * @var string UserGuide type (csv,...)
     */
    private $type;

    /**
     * @var string UserGuide path
     */
    private $path;

    /**
     * @var string UserGuide guide path
     */
    private $guidePath;

    /**
     * @var string UserGuide pages
     */
    private $pages;

    /**
     * @var string UserGuide guide path
     */
    private $pagesContent;

    /**
     * @var mixed use cache file
     */
    private $useCache = false;

    /**
     * @var string UserGuide URL
     */
    private $url;


    /**
     * Constructor
     */
    public function __construct() {
        $confDocUser = Config::get('user_guide');

        if ($confDocUser
            && isset($confDocUser['enabled']) && $confDocUser['enabled'] === true
            && isset($confDocUser['type']) && $confDocUser['type'] === 'web_scrapping'
            && isset($confDocUser['path'])) {

            $this->enabled = true;
            $this->type = 'web_scrapping';
            $this->path = $confDocUser['path'];
            $this->guidePath = $confDocUser['guide_path'];
            $this->pages = $confDocUser['pages'];
            $this->home = $confDocUser['home'];
            if (isset($confDocUser['use_cache']))
                $this->useCache = $confDocUser['use_cache'];

            $this->initContent();
        }
    }

    /**
     * Allows to init FAQ content (question and answers)
     */
    private function initContent() {
        foreach ($this->pages as $page) {
            $this->load($page);
        }
    }

    /**
     * Loads wiki pages
     *
     * @param $page
     */
    private function load($page) {
        if (!$this->enabled)
            return;

        $lang = Lang::getCode();

        switch ($this->type) {
            case 'web_scrapping':
                $cache_file = EKKO_ROOT . "/tmp/user_guide." . $lang . md5($page) . ".cache";
                $this->url = $this->path . $this->guidePath . $page;

                $newURL = Config::get('application_url') . 'user_guide/';

                if ($lang === 'en') {
                    $this->url .= "/en";
                }

                if ($this->useCache && file_exists($cache_file) && filemtime($cache_file) > (time() - 15 * 60)) {
                    $this->pagesContent[$page] = file_get_contents($cache_file);
                } else {

                    $content = @call_user_func_array('file_get_contents', array($this->url . '?do=export_html'));

                    if ($content === false) {
                        $this->pagesContent[$page] = false;
                    } else {
                        //Replacing subpages references
                        $content = str_ireplace('href="' . $this->guidePath, 'href="' . $newURL, $content);

                        $content = str_ireplace("/_media", $this->path . "/_media", $content);
                        $content = str_ireplace("/_details", $this->path . "/_media", $content);
                        $content = str_ireplace("/_detail", $this->path . "/_media", $content);
                        $content = str_ireplace("/lib", $this->path . "/lib", $content);


                        $this->pagesContent[$page] = preg_replace("#^.*<body[^>]*>(.+)</body>.*$#Umsi", '$1', $content);

                        if ($this->useCache)
                            file_put_contents($cache_file, $this->pagesContent[$page]);
                    }
                }
                break;
        }
    }

    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return mixed value
     */
    public function __get($property) {
        if (in_array($property, array('enabled', 'type', 'path', 'home', 'pages', 'pagesContent', 'use_cache', 'url')))
            return $this->$property;

        if ('homePage' === $property) {
            return $this->pagesContent[$this->home];
        }

        throw new PropertyAccessException($this, $property);
    }

}
