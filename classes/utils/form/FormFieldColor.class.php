<?php

/**
 *     Moment - FormFieldColor.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Text form field generator / validator
 *
 * Accepts properties :
 *   - required boolean
 */
class FormFieldColor extends FormFieldText {
    /**
     * HTML type
     */
    const HTML_TYPE = 'hidden';
    
    
    /**
     * Get HTML input node
     *
     * @return string
     */
    public function getHTMLInput() {
        $html = '<input data-color-input type="'.static::HTML_TYPE.'"';
    
        $html .= ' name="'.htmlspecialchars($this->name).'"';
        
        if($this->required)
            $html .= ' required';

        if(!is_null($this->value))
            $html .= ' value="'.htmlspecialchars($this->value).'"';
        
        return $html .' />'."\n";
    }
}
