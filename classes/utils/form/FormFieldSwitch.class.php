<?php

/**
 *     Moment - FormFieldSwitch.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class FormFieldSwitch
 */
class FormFieldSwitch extends FormFieldCheckbox {

    /**
     * @var string constraints alternative text
     */
    protected $disabled = false;

    /**
     * Constructor
     *
     * @param string $name
     * @param array $properties
     * @param mixed $value
     */
    public function __construct($name, array $properties = array(), $value = null) {

        if (isset($properties['disabled']))
            $this->disabled = $properties['disabled'];

        //Parent won't accept uknown property
        unset($properties['disabled']);

        parent::__construct($name, $properties, $value);

        $this->value = $value;
    }

    /**
     * Get HTML input node
     *
     * @return string
     */
    public function getHTMLInput() {

        $switch_id = 'switch' . uniqid();

        $input = '<span class="switch tinier">';
        $input .= '<input type="checkbox"';
        $input .= ' id="' . $switch_id . '"';
        $input .= ' name="' . htmlspecialchars($this->name) . '"';
        $input .= ' class="switch-input"';

        if ($this->required)
            $input .= ' required';

        if ($this->value)
            $input .= ' checked="checked"';

        if ($this->disabled)
            $input .= ' disabled';

        $input .= ' tabindex="-1"/>';

        $input .= '<label class="switch-paddle" for="' . $switch_id . '">'
            . '<span class="show-for-sr">' . Lang::tr($this->label) . '</span>'
            . '</label>';

        return $input . '</span>';
    }

    /**
     * Get HTML representation
     *
     * @return string
     */
    public function getHTML() {
        $input = $this->getHTMLInput();

        $label = '<span class="switch-label">' . Lang::tr($this->label) . '</span>';

        return '<label class="' . (($this->disabled) ? 'disabled' : '') . ' switch-container" tabindex="0">' . $input . $label . '</label>';
    }

    /**
     * Validate data
     *
     * @param mixed $data
     *
     * @throws FormDataValidationException
     */
    public function validateData($data) {
        parent::validateData($data);

        if (!in_array($data, array(0, 1), true)) {
            throw new FormDataValidationException($this, 'invalid');
        }
    }

}