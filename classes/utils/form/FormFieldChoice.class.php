<?php

/**
 *     Moment - FormFieldChoice.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class FormFieldChoice
 */
class FormFieldChoice extends FormField {

    /**
     * @var array values
     */
    protected $values = array();

    protected $groupedValues = array();

    protected $question = null;

    /**
     * current answer
     */
    protected $answer = null;

    protected $fields = array();

    protected $formFieldType = QuestionAnswerTypes::UNIQUE;

    protected $choiceType = QuestionTypes::TEXT;

    /**
     * FormFieldChoice constructor.
     * @param $id
     * @param Question $question
     * @param Answer|null $answer
     * @throws FormFieldBadNameException
     */
    public function __construct($id, Question $question, Answer $answer = null) {
        parent::__construct($id);

        foreach ($question->propositions as $proposition) {
            $this->values[$proposition->id] = $proposition;
        }

        $this->groupedValues = $question->grouped_propositions;

        $this->formFieldType = $question->answer_type;

        $this->choiceType = $question->type;

        $this->question = $question;

        //There is an answer for this choice
        $this->answer = $answer;

        // Create fields as needed
        $this->prepareFields();

    }

    /**
     * Get table row representing suggestion answers
     *
     * @return string
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    public function getHTML() {

        $input = '<tr class="your_choice">';
        //one columns is for voters name
        $input .= '<td class="replier_name first">' . Lang::tr('your_choice') . '</td>';

        $disable_answer_edition = (isset($this->answer) && $this->answer->Question->Survey->disable_answer_edition);

        foreach ($this->fields as $proposition_id => $field) {
            //When field is array, no input is required
            if (is_array($field)) {
                $value = $field['value'];
            } elseif ($field instanceof FormFieldSingleRadio) {
                $value = ($field->checked_value === $field->value) ? YesNoMaybeValues::YES : YesNoMaybeValues::NO;
            } else/*if($field instanceof FormFieldYesNoMaybeClick)*/ {
                $value = $field->value;
            }

            $input .= '<td class="proposition" data-proposition-id="' . $proposition_id . '" data-proposition-type="' . $this->question->type . '" data-value="' . $value . '">';

            $input .= '<label for="p'.$proposition_id.'" class="label-for-small show-for-small-only text-center">' . $this->values[$proposition_id]->label . '</label>';

            if ($disable_answer_edition || is_array($field)) {

                $title = Lang::tr('uneditable_answer');
                //If answer edition is not disabled and current value is not 'yes'
                // and proposition is not available
                if (!$disable_answer_edition
                    && $value !== YesNoMaybeValues::YES
                    && !$this->values[$proposition_id]->is_available
                ) {
                    $title = Lang::tr('unavailable_answer');

                    if ($value === Answer::UNDEFINED) {
                        $value = 'forbidden';
                    }
                }

                $input .= '<span class="' . YesNoMaybeValues::$ICONS[$value]['icon'] . ' uneditable"
                                    data-tooltip aria-haspopup="true" data-disable-hover="false"
                                   title="' . $title . '">'
                    . '</span>';
            } else {
                $input .= $field->getHTMLInput();
            }
            $input .= '</td>' . "\n";
        }

        return $input . '</tr>' . "\n";
    }

    /**
     * No specficic constraint in field
     */
    public function getConstraints() {
        //No constraints
    }

    /**
     * @param mixed $data
     * @throws FormDataValidationException
     */
    public function validateData($data) {
        // Data shoud be part of values
        if ($data && !array_key_exists($data, $this->values))
            throw new FormDataValidationException($this, 'bad value');
    }

    /**
     * Prepares each fields (either yesNoMaybe or radio)
     */
    private function prepareFields() {

        $radio_uid = uniqid('proposition_');

        foreach ($this->groupedValues as $groupName => $groupMembers) {
            foreach ($groupMembers as $proposition) {
                $current_value = Answer::UNDEFINED;
                //Getting current value
                if (!is_null($this->answer)) {
                    $current_value = $this->answer->getValue($proposition->id);
                }
                //If proposition is not available, and current is not yes => it is not possible to modify this answer
                if (!$proposition->is_available && ($current_value !== YesNoMaybeValues::YES)) {
                    $this->fields[$proposition->id] = array(
                        'value' => is_null($current_value) ? Answer::UNDEFINED : $current_value
                    );
                    continue;
                }
                switch ($this->formFieldType) {
                    case QuestionAnswerTypes::MULTIPLE :
                        $this->fields[$proposition->id] = new FormFieldYesNoMaybe('proposition_' . $proposition->id, array(), $current_value, false);
                        break;
                    case QuestionAnswerTypes::YESNOMAYBE :
                        $this->fields[$proposition->id] = new FormFieldYesNoMaybe('proposition_' . $proposition->id, array(), $current_value);
                        break;
                    default :
                        //If current value is 1 then radio must be checked
                        $checked_id = ($current_value == YesNoMaybeValues::YES) ? $proposition->id : '';

                        $this->fields[$proposition->id] = new FormFieldSingleRadio($radio_uid, array('checked_value' => $checked_id, 'required' => true), $proposition->id);
                        break;
                }
            }
        }

    }


}
