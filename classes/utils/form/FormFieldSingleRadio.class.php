<?php

/**
 *     Moment - FormFieldSingleRadio.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Radio form field generator / validator without label
 */
class FormFieldSingleRadio extends FormFieldSingle {

    protected $checked_value = '';

    /**
     * Get HTML input node
     *
     * @return string
     */
    public function getHTMLInput() {
        $input = '<input type="radio" id="p' .$this->value. '" class="single_choice" name="' . htmlspecialchars($this->name) . '" value="' . htmlspecialchars($this->value) . '"';

        if ($this->required)
            $input .= ' required';

        if ($this->value && property_exists($this, 'checked_value') && ($this->checked_value == $this->value))
            $input .= ' checked="checked"';

        return $input . ' />' . "\n";
    }

    /**
     * Getter
     *
     * @param string $property
     *
     * @throws PropertyAccessException
     *
     * @return mixed
     */
    public function __get($property) {
        if (in_array($property, array(
            'checked_value'
        ))) return $this->$property;

        return parent::__get($property);
    }

    /**
     * Setter
     *
     * @param string $property
     * @param mixed $value
     *
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if ($property == 'checked_value') {
            $this->checked_value = $value;
        } else {
            parent::__set($property, $value);
        }
    }
}
