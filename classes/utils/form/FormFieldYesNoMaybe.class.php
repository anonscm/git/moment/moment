<?php

/**
 *     Moment - FormFieldYesNoMaybe.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Click form field generator / validator
 *
 */
class FormFieldYesNoMaybe extends FormFieldHidden {

    protected $maybe = true;

    /**
     * Constructs a select Yes No Maybe
     *
     * @param type $name
     * @param array|type $properties
     * @param type $value
     * @param bool $maybe
     */
    public function __construct($name, $properties = array(), $value = null, $maybe = true) {
        $this->maybe = $maybe;

        parent::__construct($name, $properties, $value);
    }

    /**
     * Get HTML input node
     *
     * @return string
     */
    public function getHTMLInput() {

        $values = array('success' => YesNoMaybeValues::YES);
        if ($this->maybe) {
            $values['warning'] = YesNoMaybeValues::MAYBE;
        }
        $values['alert'] = YesNoMaybeValues::NO;

        $value_selector = '<span class="button-group text-center field-yes-no-maybe-radio">';
        foreach ($values as $foundationClass => $value) {
            $uid = uniqid($value);
            $value_selector .= '<label tabindex="0" for="' . $uid . '" class="button ' . $foundationClass
                . ' ' . htmlspecialchars($value)
                . ' ' . (($this->value && ($this->value == $value)) ? 'selected' : 'hollow') . '" '
                . '>';
            $value_selector .= '<input id="' . $uid . '" class="yes_no_maybe_radio hide" type="radio" name="'
                . htmlspecialchars($this->name) . '" value="' . htmlspecialchars($value) . '"';

            if ($this->value && ($this->value == $value))
                $value_selector .= ' checked="checked"';

            $value_selector .= ' />' . Lang::tr($value) . '</label>' . "\n";
        }


        $value_selector .= '</span>';

        return parent::getHTMLInput() . $value_selector;
    }
}
