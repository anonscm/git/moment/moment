<?php

/**
 *     Moment - IcsEvent.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This class contains all the information related to an ICS
 * (ICalendar file description)
 *
 * @property string $id
 * @property string $uid
 * @property string $summary
 * @property string $description
 * @property string $description_html
 * @property string $dtstart
 * @property string $dtend
 * @property string $duration
 * @property string $location
 * @property string $lastmodified
 * @property string $created
 * @property string $dtstamp
 * @property string $sequence
 * @property string $status
 * @property string $transp
 * @property string $organizer
 * @property string[] $attendees
 *
 */
class IcsEvent {
    public $id = -1;
    public $uid = -1;
    public $summary = '';
    protected $description = '';
    protected $description_html = '';
    public $dtstart = '';
    public $dtend = '';
    public $duration = '';
    public $location = '';
    public $lastmodified = '';
    public $created = '';
    public $dtstamp = '';
    public $sequence = 1;
    public $status = '';
    public $transp = '';
    public $organizer = '';
    public $attendees = array();


    /**
     * Getter (cleans some properties before returning)
     * @param $property
     * @return mixed
     */
    public function __get($property) {
        if('description' == $property
            || 'description_html' == $property) {
            return str_replace(array("\n", ";"), array("\\n", "\;"), $this->$property);
        }
        return $this->$property;
    }

    /**
     * Setter
     * @param $property
     * @param $value
     */
    public function __set($property, $value) {
        $this->$property = $value;
    }
}
