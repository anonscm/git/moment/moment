<?php

/**
 *     Moment - DateUtil.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class DateUtil
 */
class DateUtil {

    /**
     * Convert timestamp to date string in the correct format and timezone
     *
     * @param $timestamp
     * @param string $time_zone
     * @param string $format
     * @param null $custom_format
     * @return string
     */
    public static function toLocale($timestamp, $time_zone = 'UTC', $format = 'datetime', $custom_format = null) {

        $dt = new DateTime();
        $dt->setTimestamp($timestamp);

        $dt->setTimezone(new DateTimeZone($time_zone));
        if (isset($custom_format)) {
            $is = $dt->format($custom_format);
        } else {
            $is = $dt->format(Lang::tr($format . '_format'));
        }
        return $is;
    }

    /**
     * Convert a string date in a timezone into a string date in an other format and other timezone
     *
     * @param $string_date input date string
     * @param $input_timezone input timezone
     * @param $input_format input format
     * @param $output_timezone output timezone
     * @param $output_format output format
     * @return string date as string
     */
    public static function convertTimezone($string_date, $input_timezone, $input_format, $output_timezone, $output_format) {
        return DateTime::createFromFormat($input_format, $string_date, new DateTimeZone($input_timezone))
            ->setTimezone(new DateTimeZone($output_timezone))
            ->format($output_format);
    }

    /**
     * Convert a @DateTime into the correct timezone in the desired format
     *
     * @param DateTime $date date to convert
     * @param $output_timezone timezone
     * @param $output_format format (eg: Y/m/d ..)
     * @return string
     */
    public static function convertDateToTimezone(DateTime $date, $output_timezone, $output_format) {
        return $date
            ->setTimezone(new DateTimeZone($output_timezone))
            ->format($output_format);
    }

    /**
     * Get offset in seconds between GMT and $timezone
     * @param string $timezone
     * @param null $timestamp
     * @return int offset in seconds
     */
    public static function getTimezoneOffset($timezone = 'UTC', $timestamp = null) {
        $dt = new DateTime();
        $dt->setTimezone(new DateTimeZone($timezone));
        if(isset($timestamp)) {
            $dt->setTimestamp($timestamp);
        }

        return $dt->getOffset();
    }
}
