<?php

/**
 *     Moment - SurveyStats.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class SurveyStats
 */
class SurveyStats {

    const DAY = 0;
    const WEEK = 1;
    const MONTH = 2;
    const YEAR = 3;
    const OVERALL = 4;

    private static $stored_stats = null;

    private static $idps = array();
    
    private static $tr_cache = array();

    /**
     * Get indicators current value
     * @return null
     */
    public static function get() {
        if (!Auth::isAdmin())
            return null;

        if (isset(self::$stored_stats)) {
            return self::$stored_stats;
        }

        //Counting all surveys
        $check = DBI::prepare('SELECT count(*) AS count FROM ' . Survey::getDBTable());
        $check->execute();
        self::$stored_stats['general']['total_survey_count'] = $check->fetchColumn(0);

        //Counting opened surveys
        $check = DBI::prepare('SELECT count(*) AS count FROM ' . Survey::getDBTable() . ' WHERE closed IS NULL AND is_draft = 0');
        $check->execute();
        self::$stored_stats['general']['opened_survey_count'] = $check->fetchColumn(0);

        //Counting draft surveys
        $check = DBI::prepare('SELECT count(*) AS count FROM ' . Survey::getDBTable() . ' WHERE is_draft = 1');
        $check->execute();
        self::$stored_stats['general']['draft_survey_count'] = $check->fetchColumn(0);

        //Counting closed surveys
        $check = DBI::prepare('SELECT count(*) AS count FROM ' . Survey::getDBTable() . ' WHERE closed IS NOT NULL AND is_draft = 0');
        $check->execute();
        self::$stored_stats['general']['closed_survey_count'] = $check->fetchColumn(0);

        //Counting closed surveys with final answer
        $check = DBI::prepare('SELECT count(*) AS count FROM ' . Participant::getDBTable() . ' WHERE type = "ULTIMATE"');
        $check->execute();
        self::$stored_stats['general']['closed_survey_with_final_answer_count'] = $check->fetchColumn(0);

        //Counting average number and max number of survey by owner
        $check = DBI::prepare('SELECT AVG(a.scount) AS avg_count, MAX(a.scount) AS max_count FROM (SELECT count(*) as scount FROM ' . Survey::getDBTable() . ' s JOIN ' . Owner::getDBTable() . ' o ON o.survey_id = s.id  GROUP BY o.email) a;');
        $check->execute();
        $res = $check->fetch(PDO::FETCH_ASSOC);
        self::$stored_stats['general']['avg_count'] = $res['avg_count'];
        self::$stored_stats['general']['max_count'] = $res['max_count'];

        //Counting participants
        $check = DBI::prepare('SELECT count(*) AS count FROM ' . Participant::getDBTable() . ' WHERE type <> "ULTIMATE"');
        $check->execute();
        self::$stored_stats['general']['participants_count'] = $check->fetchColumn(0);

        //Counting authenticated participants
        $check = DBI::prepare('SELECT count(*) AS count FROM ' . Participant::getDBTable() . ' WHERE type = "AUTHENTICATED"');
        $check->execute();
        self::$stored_stats['general']['authenticated_participants_count'] = $check->fetchColumn(0);

        //Counting declarative participants
        $check = DBI::prepare('SELECT count(*) AS count FROM ' . Participant::getDBTable() . ' WHERE type = "DECLARATIVE"');
        $check->execute();
        self::$stored_stats['general']['declarative_participants_count'] = $check->fetchColumn(0);

        //Counting users
        $check = DBI::prepare('SELECT count(id) AS count FROM ' . User::getDBTable());
        $check->execute();
        self::$stored_stats['general']['distinct_users_count'] = $check->fetchColumn(0);

        //Counting average number of participants by survey
        $check = DBI::prepare('SELECT AVG(counts.pcount) as avg_count, MAX(counts.pcount) as max_count FROM (SELECT count(*) as pcount FROM '. Participant::getDBTable() .' GROUP BY survey_id) counts ;');
        $check->execute();
        $res = $check->fetch(PDO::FETCH_ASSOC);
        self::$stored_stats['survey_usage']['avg_participant_count'] = $res['avg_count'];
        self::$stored_stats['survey_usage']['max_participant_count'] = $res['max_count'];

        //Counting average number of proposition by question
        $check = DBI::prepare('SELECT AVG(counts.pcount) as avg_count, MAX(counts.pcount) as max_count FROM (SELECT count(*) as pcount FROM '. DateProposition::getDBTable() .' GROUP BY question_id) counts ;');
        $check->execute();
        $res = $check->fetch(PDO::FETCH_ASSOC);
        self::$stored_stats['survey_usage']['avg_date_proposition_count'] = $res['avg_count'];
        self::$stored_stats['survey_usage']['max_date_proposition_count'] = $res['max_count'];

        $check = DBI::prepare('SELECT AVG(counts.pcount) as avg_count, MAX(counts.pcount) as max_count FROM (SELECT count(*) as pcount FROM '. TextProposition::getDBTable() .' GROUP BY question_id) counts ;');
        $check->execute();
        $res = $check->fetch(PDO::FETCH_ASSOC);
        self::$stored_stats['survey_usage']['avg_text_proposition_count'] = $res['avg_count'];
        self::$stored_stats['survey_usage']['max_text_proposition_count'] = $res['max_count'];



        Logger::info("Stats");
        Logger::info(json_encode(self::$stored_stats));
        return self::$stored_stats;
    }

    /**
     * Generate a stat report
     * @param int $period
     * @param null $refDate
     * @param bool $withIDP
     * @return array|array[]
     * @throws ApplicationRestClientException
     * @throws BadDateException
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    public static function generateReport($period = self::DAY, $refDate = null, $withIDP = true) {
        if(isset($refDate)) {
            $d = DateTime::createFromFormat('Y-m-d', $refDate);
            if(!$d || $d->format('Y-m-d') != $refDate) {
                throw new BadDateException($refDate);
            }
        }

        //Processing base stats
        self::get();

        //Whether we include stat by IDP or not
        if($withIDP) {
            return static::generateReportWithIDP($period, $refDate);
        } else {
            return static::generateReportTotal($period, $refDate);
        }
    }

    /**
     * Generate full report including indicators by idps
     * @param int $period
     * @param null $refDate
     * @return array
     * @throws ApplicationRestClientException
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    private static function generateReportWithIDP($period = self::DAY, $refDate = null) {
        //Get all idps
        self::loadIDPs();

        $report = array();

        //Get all used idps
        $result = self::statRequest('SELECT DISTINCT idp FROM ' . EventLog::getDBTable() . ' WHERE idp IS NOT NULL AND idp != ""');
        foreach ($result as $row) {
            $report[$row['idp']] = array();
        }
        $report['total'] = array(
            'nb_distinct_user' => 0,
            'nb_survey_created' => 0,
            'nb_participations' => 0,
            'nb_anonymous_participations' => 0
        );

        //Distinct user
        $result = self::statRequest('SELECT substring_index(substring(attributes, instr(attributes, \'"idp":"\') + length(\'"idp":"\')),\'"\', 1) AS idp, COUNT(u.id) AS counter FROM User AS u WHERE ' . self::delayCriteria(' u.last_activity', $period, $refDate) . ' GROUP BY substring_index(substring(attributes, instr(attributes, \'"idp":"\') + length(\'"idp":"\')),\'"\', 1);');
        foreach ($result as $row) {
            $report[stripslashes($row['idp'])]['nb_distinct_user'] = $row['counter'];
            $report['total']['nb_distinct_user'] += $row['counter'];
        }
        Logger::info("Total number of distinct users : ".$report['total']['nb_distinct_user']);
        //Survey created
        $result = self::statRequest('SELECT idp, count(*) AS counter FROM ' . EventLog::getDBTable() . ' WHERE type = "CREATED" AND SUBSTR(subject,1,1) = "S" AND ' . self::delayCriteria('created', $period, $refDate) . ' GROUP BY idp;');
        foreach ($result as $row) {
            $report[$row['idp']]['nb_survey_created'] = $row['counter'];
            $report['total']['nb_survey_created'] += $row['counter'];
        }
        Logger::info("Total number of created survey : ".$report['total']['nb_survey_created']);
        //Participations created in period
        $result = self::statRequest('SELECT e1.idp, count(DISTINCT e2.id) AS counter FROM ' . EventLog::getDBTable() . ' e1 INNER JOIN ' . EventLog::getDBTable() . ' e2 ON e1.value = e2.value WHERE e1.type = "CREATED" AND SUBSTR(e1.subject,1,1) = "S" AND e2.type = "CREATED" AND SUBSTR(e2.subject,1,1) = "P" AND ' . self::delayCriteria('e2.created', $period, $refDate) . ' GROUP BY e1.idp;');
        foreach ($result as $row) {
            $report[$row['idp']]['nb_participations'] = $row['counter'];
            $report['total']['nb_participations'] += $row['counter'];
        }
        Logger::info("Total number of participants : ".$report['total']['nb_participations']);

        //Anonymous participations created in period
        $result = self::statRequest('SELECT e1.idp, count(DISTINCT e2.id) AS counter FROM ' . EventLog::getDBTable() . ' e1 INNER JOIN ' . EventLog::getDBTable() . ' e2 ON e1.value = e2.value WHERE e1.type = "CREATED" AND SUBSTR(e1.subject,1,1) = "S" AND e2.type = "CREATED" AND SUBSTR(e2.subject,1,1) = "P" AND e2.idp IS NULL AND ' . self::delayCriteria('e1.created', $period, $refDate) . ' GROUP BY e1.idp;');
        foreach ($result as $row) {
            $report[$row['idp']]['nb_anonymous_participations'] = $row['counter'];
            $report['total']['nb_anonymous_participations'] += $row['counter'];
        }
        Logger::info("Total number of anonymous participants : ".$report['total']['nb_anonymous_participations']);

        // Matching IDP with institutions
        $reportInstitutions = array();

        // IDP without institution
        $otherIDPstats = array();

        //Init empty row
        $otherIDPstats['other_idps'] = static::toReportRow( Lang::tr('other_idps')->out());
        // IDP eduGAIN without institutions
        $otherIDPstats['edugain_idps'] = $otherIDPstats['other_idps'];
        $otherIDPstats['edugain_idps'][static::trKey('institution')] = Lang::tr('edugain_idps')->out();

        Logger::info('Report has '.count($report).' entries.');
        foreach ($report as $idp => $idpStats) {
            if ($idp === 'fake')
                $idp = 'https://idp.renater.fr/idp/shibboleth';

            //If there is no value for this idp -> no line in report
            if(count($idpStats) == 0) {
                Logger::info('IDP "'.$idp.'" has no values for this periodicity');
                continue;
            }

            if($idp == 'total') {
                continue;
            }
            //We know this IDP
            if (isset(self::$idps[$idp])) {
                $reportInstitutions[] = static::toReportRow(self::$idps[$idp]['name'], self::$idps[$idp]['sugar_id'], $idpStats);
            } else {
                //We don't know this IDP
                Logger::info('IDP "'.$idp.'" has no matching institution');

                // What is the "kind" of IDP
                $kind = self::isEduGAINIDP($idp)?'edugain_idps':'other_idps';

                //If this idp has values and is not known but considered as eduGAIN -> add values to eduGAIN idp row
                //If this idp has values and is not known and not considered as eduGAIN -> add values to other idp row

                Logger::info('IDP "'.$idp.'" is considered as '.$kind);
                $otherIDPstats[$kind][static::trKey('nb_distinct_user')] += isset($idpStats['nb_distinct_user']) ? $idpStats['nb_distinct_user'] : 0;
                $otherIDPstats[$kind][static::trKey('nb_survey_created')] += isset($idpStats['nb_survey_created']) ? $idpStats['nb_survey_created'] : 0;
                $otherIDPstats[$kind][static::trKey('nb_participations')] += isset($idpStats['nb_participations']) ? $idpStats['nb_participations'] : 0;
                $otherIDPstats[$kind][static::trKey('nb_anonymous_participations')] += isset($idpStats['nb_anonymous_participations']) ? $idpStats['nb_anonymous_participations'] : 0;
            }
        }

        // Adding EduGAIN Row
        $reportInstitutions[] = $otherIDPstats['edugain_idps'];

        // Adding other IDP Row
        $reportInstitutions[] = $otherIDPstats['other_idps'];

        // Adding total Row
        $reportInstitutions[] = static::toReportRow(static::trKey('total'), '', $report['total']);

        return $reportInstitutions;
    }

    /**
     * Generate total report for period
     * @param int $period
     * @param null $refDate
     * @return array[]
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    private static function generateReportTotal($period = self::DAY, $refDate = null) {
        $total = array();
        $total['nb_distinct_user'] = self::statRequestCount('SELECT COUNT(u.id) AS counter FROM User AS u WHERE ' . self::delayCriteria(' u.last_activity', $period, $refDate) . ';');
        $total['nb_survey_created'] = self::statRequestCount('SELECT count(*) AS counter FROM ' . EventLog::getDBTable() . ' WHERE type = "CREATED" AND SUBSTR(subject,1,1) = "S" AND ' . self::delayCriteria('created', $period, $refDate) . ';');
        $total['nb_participations'] = self::statRequestCount('SELECT count(*) AS counter FROM ' . EventLog::getDBTable() . ' WHERE type = "CREATED" AND SUBSTR(subject,1,1) = "P" AND ' . self::delayCriteria('created', $period, $refDate) . ';');
        $total['nb_anonymous_participations'] = self::statRequestCount('SELECT count(*) AS counter FROM ' . EventLog::getDBTable() . ' WHERE type = "CREATED" AND SUBSTR(subject,1,1) = "P" AND idp IS NULL AND ' . self::delayCriteria('created', $period, $refDate) . ';');

        Logger::info("Total number of distinct users : ".$total['nb_distinct_user'] );
        Logger::info("Total number of created survey : ". $total['nb_survey_created']);
        Logger::info("Total number of participants : ".$total['nb_participations']);
        Logger::info("Total number of anonymous participants : ".$total['nb_anonymous_participations']);

        return array(
            static::toReportRow(static::trKey('total'), '', $total)
        );
    }

    /**
     * Load all IDPs institutions names  / sugar_ids
     * @throws ApplicationRestClientException
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     */
    private static function loadIDPs() {
        //We need the rest client to get IDPs
        require_once EKKO_ROOT . '/lib/client/core/ApplicationRestClient.class.php';

        //Getting all known IDPs through referentielsi
        $client =
            new ApplicationRestClient(
                Config::get('referentielsi.url'),
                new ApplicationRestClientAuth(array('remote_application' => Config::get('referentielsi.app')), Config::get('referentielsi.secret'))
            );

        $response = $client->get('idp+with+institution', array());

        if (is_array($response->body)) {
            foreach ($response->body as $idpObject) {

                if (isset($idpObject->_entity_name) && 'idp' === $idpObject->_entity_name
                    && isset($idpObject->idp_hostname)
                    && isset($idpObject->idp_id)
                    && isset($idpObject->institutions) && count($idpObject->institutions) > 0
                ) {
                    self::$idps[$idpObject->idp_hostname] = array(
                        'name' => self::institutionNames($idpObject->institutions),
                        'sugar_id' => self::institutionPassId($idpObject->institutions)
                    );
                }
            }
            Logger::info('Loaded '.count(self::$idps).' Identity Providers');
        } else {
            Logger::error('Failed loading idp+with+institution :'.json_encode($response->body));
        }
    }

    /**
     * Is $idp_hostname an IDP we consider as eduGAIN (outside .fr)
     * @param $idp_hostname
     * @return bool
     */
    private static function isEduGAINIDP($idp_hostname) {
        //If top-level domain is not .fr we consider it as eduGAIN IDP
        return (preg_match('`^https?:\/\/?(?:\w*\.)*(fr)\/.*?$`',$idp_hostname) == 0);
    }

    /**
     * Get a delay SQL criteria for column and given period
     * @param $columnName
     * @param $period
     * @param null $date
     * @return string
     */
    private static function delayCriteria($columnName, $period, $date = null) {
        $delay = '';

        //Setting current reference date
        $reference_date = 'DATE(NOW())';

        if($date) {
            $reference_date = "'".$date."'";
        }

        switch ($period) {
            case self::DAY :
                $delay = ' DATE(' . $columnName . ') = ' . $reference_date . ' ';
                break;

            case self::WEEK :
                $delay = ' ' . $columnName . ' >= ' . $reference_date . ' - INTERVAL 1 WEEK AND '. $columnName . ' < ' . $reference_date;
                break;

            case self::MONTH :
                $delay = ' ' . $columnName . ' >= ' . $reference_date . ' - INTERVAL 1 MONTH AND  '. $columnName . ' < ' . $reference_date;
                break;

            case self::YEAR :
                $delay = ' ' . $columnName . ' >= ' . $reference_date . ' - INTERVAL 1 YEAR AND  '. $columnName . ' < ' . $reference_date;
                break;
        }
        return $delay;
    }

    /**
     * Get a string of all institutions names (separated by |)
     * @param $institutions
     * @return string
     */
    private static function institutionNames($institutions) {

        $institutionNames = array();
        foreach ($institutions as $institution) {
            $institutionNames[] = $institution->name;
        }

        return implode('|', $institutionNames);
    }

    /**
     * Get a string of all institutions sugar_id (separated by |)
     * @param $institutions
     * @return string
     */
    private static function institutionPassId($institutions) {

        $institutionId = array();
        foreach ($institutions as $institution) {
            $institutionId[] = $institution->sugar_id;
        }

        return implode('|', $institutionId);
    }

    /**
     * Get result of a $request
     * @param $request
     * @return array
     */
    private static function statRequest($request) {

        $check = DBI::prepare($request);
        $check->execute();

        $result = $check->fetchAll();

        if ($result) {
            return $result;
        } else {
            return array();
        }
    }

    /**
     * Get a count by a SQL $request
     * @param $request
     * @return int|mixed
     */
    private static function statRequestCount($request) {

        $result = static::statRequest($request);
        $count = 0;
        if(count($result) == 1) {
            $result = array_shift($result);
            if(count($result) == 1) {
                $count = array_shift($result);
            }
        }
        return $count;
    }

    /**
     * Caching translation for keys
     * @param $key
     * @return mixed
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    private static function trKey($key) {
        if(!isset(static::$tr_cache[$key])) {
            static::$tr_cache[$key] = Lang::tr($key)->out();
        }
        return static::$tr_cache[$key];
    }

    /**
     * Cast to a report row
     * @param $institution
     * @param string $sugarId
     * @param array $idpStats
     * @return array
     */
    private static function toReportRow($institution, $sugarId = '', $idpStats = array()) {
        return array(
            static::trKey('institution') => $institution,
            static::trKey('sugar_id') => $sugarId,
            static::trKey('nb_distinct_user') => isset($idpStats['nb_distinct_user']) ? $idpStats['nb_distinct_user'] : 0,
            static::trKey('nb_survey_created') => isset($idpStats['nb_survey_created']) ? $idpStats['nb_survey_created'] : 0,
            static::trKey('nb_participations') => isset($idpStats['nb_participations']) ? $idpStats['nb_participations'] : 0,
            static::trKey('nb_anonymous_participations') => isset($idpStats['nb_anonymous_participations']) ? $idpStats['nb_anonymous_participations'] : 0
        );
    }

}
