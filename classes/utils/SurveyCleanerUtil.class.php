<?php

/**
 *     Moment - SurveyCleanerUtil.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class SurveyCleanerUtil
 */
class SurveyCleanerUtil {

    /**
     * Deletes older than cfg.conservation_duration months old surveys
     *
     * @throws BadFormatException
     */
    public static function cleanObsoletesSurveys() {

        Logger::info('** Cleaning old surveys - START**');

        //Conservation duration is in monthes
        $conservationDuration = Config::get('conservation_duration') ? Config::get('conservation_duration') : 6;

        if (!is_numeric($conservationDuration))
            throw new BadFormatException($conservationDuration, 'INTEGER');

        $obsolescenceDate = date('Y-m-d', strtotime('-' . $conservationDuration . ' month', time()));

        //Selecting all surveys closed before
        $obsoleteSurveys = Survey::all('closed < :obsolescenceDate',
            array(':obsolescenceDate' => $obsolescenceDate)
        );

        Logger::info('We will delete ' . count($obsoleteSurveys) . ' survey(s) closed before ' . $obsolescenceDate);
        Logger::info('There are ' . count($obsoleteSurveys) . ' survey(s) to be deleted');

        $totalCount = 0;
        $successCount = 0;
        $failedCount = 0;
        foreach ($obsoleteSurveys as $survey) {
            $totalCount++;
            //To trace $survey has been deleted
            Logger::info('Deleting "' . $survey->id . '".');
            try {
                $survey->delete();
                $successCount++;
                Logger::info('"' . $survey->id . '" deleted.');
            } catch (Exception $e) {
                $failedCount++;
                Logger::info('Deleting "' . $survey->id . '" FAILED. (' . $e->getMessage() . ')');
            }
        }

        Logger::info($successCount . '/' . $totalCount . ' survey(s) deleted');
        if ($failedCount)
            Logger::info($failedCount . ' failures');

        Logger::info('** Cleaning old surveys - END**');
    }

    /**
     * Closes survey that must be closed (auto_close)
     */
    public static function autoCloseSurveys() {

        Logger::info('** Closing surveys arrived to due date - START**');

        //Selecting all surveys created before
        $openedSurveys = Survey::all('closed IS null AND is_draft = 0');

        Logger::info('There are currently ' . count($openedSurveys) . ' opened survey(s)');

        $totalCount = 0;
        $closedCount = 0;
        foreach ($openedSurveys as $survey) {
            $totalCount++;

            if($survey->isClosed()) {
                $closedCount++;
            }
        }

        Logger::info($closedCount . ' survey(s) closed');
        Logger::info('** Closing surveys arrived to due date - END**');
    }

    /**
     * Send closing reminder to survey owners
     */
    public static function sendClosingReminders() {

        Logger::info('** Reminding closing of surveys - START**');

        //Selecting all surveys created before
        $openedSurveys = Survey::all('closed IS null AND is_draft = 0');

        $closingReminderCount = 0;
        foreach ($openedSurveys as $survey) {
            if(self::needClosingReminder($survey)) {

                $closingReminderCount++;

                $recipients = array();

                foreach ($survey->Owners as $owner) {
                    $recipients[$owner->email] = $owner->email;
                }

                foreach ($recipients as $recipient_email) {
                    MomentApplicationMail::prepareClosingReminderEmail($survey, $recipient_email)->send();
                }
            }
        }

        Logger::info($closingReminderCount . ' survey(s) needed a closing reminder');
        Logger::info('** Reminding closing of surveys - END**');
    }

    /**
     * Send removal reminder to survey owners
     */
    public static function sendDeletionReminders() {

        Logger::info('** Reminding deletion of surveys - START**');

        //Conservation duration is in monthes
        $conservationDuration = Config::get('conservation_duration') ? Config::get('conservation_duration') : 6;

        if (!is_numeric($conservationDuration))
            throw new BadFormatException($conservationDuration, 'INTEGER');

        //The date of closed + n days (Survey will be deleted in n days)
        $obsolescenceDatePlusDelay = date('Y-m-d', strtotime('-' . $conservationDuration . ' month', time()) + (86400 * Config::get('removal_reminder_delay')));

        //Selecting all surveys that will be deleted in removal_reminder_delay days
        //Survey is closed before (now - conservation_duration month) + removal_reminder_delay + 1 day so it will be deleted in removal_reminder_delay days
        $toBeReminded = Survey::all('closed < :obsolescenceDatePlusDelay + INTERVAL 1 DAY',
            array(':obsolescenceDatePlusDelay' => $obsolescenceDatePlusDelay)
        );

        Logger::info('There are ' . count($toBeReminded) . ' survey(s) closed before (included) ' . $obsolescenceDatePlusDelay);

        $removalReminderCount = 0;
        foreach ($toBeReminded as $survey) {

            // If closed date is not exactly current obsolescenceDatePlusDelay we have already reminded
            if($obsolescenceDatePlusDelay != date('Y-m-d',$survey->closed))
                continue;

            $removalReminderCount++;
            Logger::info("[survey:{$survey->id}] This survey will be deleted in ".date('Y-m-d', time() + (86400 * Config::get('removal_reminder_delay'))));

            MomentApplicationMail::prepareDeletionReminderEmail($survey)->send();
        }

        Logger::info($removalReminderCount . ' survey(s) needed a deletion reminder');
        Logger::info('** Reminding deletion of surveys - END**');
    }

    /**
     * Check weither survey needs a closing reminder
     * @return bool
     */
    private static function needClosingReminder(Survey $survey) {
        $current_date = date('Y-m-d', time());

        $survey_lifetime = $survey->auto_close - $survey->created;

        //survey should be opened and not reopened and have alifetime > one month
        if ($survey->is_draft == 1 || $survey->closed != null || $survey->reopened != null || $survey_lifetime < (86400 * 31))
            return false;


        //If current time is > auto_close date
        if (is_numeric($survey->auto_close)) {

            $date_of_closing_reminder = date('Y-m-d',$survey->auto_close - (86400 * Config::get('closing_reminder_delay')));

            // Current date is auto_close minus <closing_reminder_delay> days
            if($current_date == $date_of_closing_reminder) {
                return true;
            }
        }

        return false;
    }

}
