<?php

/**
 *     Moment - CSVUtil.class.php
 *
 * Copyright (C) 2021  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class CSVUtil
 */
class CSVUtil {
    const ESCAPED_CHARACTERS = array('=','+','@','-');

    public static function sanitize($text) {
        if(in_array(substr($text, 0, 1), self::ESCAPED_CHARACTERS)) {
            $text = "'".$text;
        }
        return $text;
    }
}
