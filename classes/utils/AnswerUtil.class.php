<?php

/**
 *     Moment - AnswerUtil.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class AnswerUtil
 */
class AnswerUtil {

    /**
     * Complete answer data, add default choices if needed
     * @param Question $question
     * @param array $answer_data
     * @param int $default_value
     */
    public static function completeChoices(Question $question, array $answer_data = array(), $default_value = YesNoMaybeValues::NO) {

        $new_answer_data = array();
        $new_answer_data['choices'] = array();

        $proposition_class = Question::getPropositionClass($question);
        $proposition_class_plural = $proposition_class . 's';
        //Each proposition must have a value in the answer
        foreach ($question->$proposition_class_plural as $proposition) {
            $proposition_answer_found = false;
            if (isset($answer_data['choices'])) {
                foreach ($answer_data['choices'] as $choice) {
                    //Value must be valid
                    if ($choice->proposition_id == $proposition->id
                        && YesNoMaybeValues::isValid($choice->value)
                    ) {
                        $proposition_answer_found = true;
                        $new_answer_data['choices'][] = $choice;
                        break;
                    }
                }
            }
            //No answer found for proposition => setting default
            if (!$proposition_answer_found) {
                // We need to add default choice
                $new_answer_data['choices'][] = array(
                    'proposition_id' => $proposition->id,
                    'value' => $default_value
                );
            }
            if (isset($answer_data['comment'])) {
                $new_answer_data['comment'] = $answer_data['comment'];
            }
        }
        return $new_answer_data;
    }

    /**
     * Validate a collection of answer_data
     * @param array $data
     * @param bool $checkAnswerId
     * @throws AnswerMissingPropertyException
     */
    public static function validateAnswersData(array $data, $checkAnswerId = false) {
        //Validating data
        foreach ($data as $answer_data) {
            static::validate((array)$answer_data, $checkAnswerId);
        }
    }

    /**
     * Validate data for an answer
     * @param array $answer_data
     * @param bool $checkAnswerId
     * @return bool
     * @throws AnswerMissingPropertyException
     */
    public static function validate(array $answer_data, $checkAnswerId = false) {

        if (!array_key_exists('question_id', $answer_data))
            throw new AnswerMissingPropertyException('question_id', $answer_data);

        if (!array_key_exists('choices', $answer_data))
            throw new AnswerMissingPropertyException('choices', $answer_data);

        if ($checkAnswerId && !array_key_exists('answer_id', $answer_data))
            throw new AnswerMissingPropertyException('answer_id', $answer_data);

        return true;
    }

    /**
     * Validate choices in an array of answer data
     * @param Participant $participant
     * @param Question $question
     * @param array $answer_data
     * @param array $former_choices
     * @throws AnswerBadChoicesException when user made choices that cant be done for this question
     * @throws AnswerBadPropertyException when choices are not valid
     * @throws AnswerMissingException when user should make a unique choice and no choice is selected
     * @throws AnswerMissingPropertyException when  no choices are made
     * @throws AnswerPropositionNotAvailableException
     */
    public static function validateChoices(Participant $participant, Question $question, array $answer_data, array $former_choices) {

        if (!array_key_exists('choices', $answer_data))
            throw new AnswerMissingPropertyException('choices', $answer_data);

        if (!is_array($answer_data['choices']))
            throw new AnswerBadPropertyException('choices', $answer_data);

        $proposition_class = Question::getPropositionClass($question);
        $proposition_class_plural = $proposition_class . 's';

        //If question answer type is unique only one proposition must have a positive value
        if (QuestionAnswerTypes::UNIQUE == $question->answer_type) {
            $question_answered = false;
            //Each proposition must have a value in the answer
            foreach ($question->$proposition_class_plural as $proposition) {
                foreach ($answer_data['choices'] as $choice) {
                    if ($choice->proposition_id == $proposition->id) {
                        //We can't answer maybe when maybe is disabled
                        if ($choice->value == YesNoMaybeValues::MAYBE)
                            throw new AnswerBadChoicesException($question, $answer_data);

                        if ($choice->value == YesNoMaybeValues::YES) {
                            if ($question_answered)
                                throw new AnswerBadChoicesException($question, $answer_data);

                            //Checking if proposition is still available if new value is 'yes' (a former yes must not throw the exception)
                            if (ParticipantTypes::ULTIMATE !== $participant->type
                                && (empty($former_choices)
                                    || !array_key_exists($choice->proposition_id, $former_choices)
                                    || $former_choices[$choice->proposition_id]->value != YesNoMaybeValues::YES)
                                && !$proposition->is_available)
                                throw new AnswerPropositionNotAvailableException($proposition, $answer_data);

                            $question_answered = true;
                        }
                        break;
                    }
                }
            }
            //We haven't found any positive choice for this question 
            if (!$question_answered)
                throw new AnswerMissingException($question, $answer_data);
        }

        //If question answer type is multiple and maybe is disable, answer can have maybes
        if (QuestionAnswerTypes::MULTIPLE == $question->answer_type
            && !$question->enable_maybe_choices
        ) {
            foreach ($question->$proposition_class_plural as $proposition) {
                foreach ($answer_data['choices'] as $choice) {
                    if ($choice->proposition_id == $proposition->id) {
                        //We can't answer maybe when maybe is disabled
                        if ($choice->value == YesNoMaybeValues::MAYBE) {
                            throw new AnswerBadChoicesException($question, $answer_data);
                        }
                        //Checking if proposition is still available if new value is 'yes' (a former yes must not throw the exception)
                        if (ParticipantTypes::ULTIMATE !== $participant->type
                            && $choice->value == YesNoMaybeValues::YES
                            && (empty($former_choices)
                                || !array_key_exists($choice->proposition_id, $former_choices)
                                || $former_choices[$choice->proposition_id]->value != YesNoMaybeValues::YES)
                            && !$proposition->is_available
                        ) {
                            throw new AnswerPropositionNotAvailableException($proposition, $answer_data);
                        }

                        break;
                    }
                }
            }
        }
    }

    /**
     * Count selected_value_yes for each proposition
     *
     * @param Question $question
     * @return array
     * @internal param array $answers
     */
    public static function countPropositions(Question $question) {

        $propositions_counting = array();

        //Getting count of yes foreach proposition
        foreach ($question->propositions as $proposition) {
            $propositions_counting[$proposition->id] = array(
                YesNoMaybeValues::YES => Choice::getValueCount($question->id, $proposition->id, YesNoMaybeValues::YES),
                YesNoMaybeValues::MAYBE => Choice::getValueCount($question->id, $proposition->id, YesNoMaybeValues::MAYBE)
            );

            if($propositions_counting[$proposition->id][YesNoMaybeValues::MAYBE])
                $question->has_maybe_answers = true;
        }

        //Sorting count descending (Winners first)
        uasort($propositions_counting, function($a, $b) {
            return $a[YesNoMaybeValues::YES] <= $b[YesNoMaybeValues::YES];
        });

        return $propositions_counting;
    }

}
