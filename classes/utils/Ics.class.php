<?php

/**
 *     Moment - Ics.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This class contains all the information related to an ICS
 * (ICalendar file description)
 *
 * @property int uid
 * @property IcsEvent[] $events
 *
 */
class Ics {

    private $events;           // array of IcsEvents
    private $options;

    public function __construct() {
        $this->events = array();

        $this->options = array(
            'method' => IcsConstants::METHOD_REQUEST
        );
    }

    /* ------------------------------------------------------------
     * Businesses functions
     * ------------------------------------------------------------ */
    
    /**
     *
     * This function allows to get the ICS formated with the standards
     * according to the RFC 2445 (Internet Calendaring and Scheduling Core
     * Object Specification - iCalendar)
     *
     * @param $data : custom data
     * @param array $options : additional options
     *
     * @return string : the ICS formated
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     */
    public function getFormatedICS() {
        $ics = array();
        //Each line should not be longer than 75 characters
        foreach(explode(PHP_EOL, Template::process('!ics_template', array('ics' => $this))) as  $line) {
            $ics[] =  wordwrap($line, 75, PHP_EOL."  ");
        }
        return implode(PHP_EOL, $ics);
    }

    /**
     * Add an event to the Ics
     * @param IcsEvent $event
     */
    public function addEvent(IcsEvent $event) {
        //Adding event only once
        if(!isset($this->events[$event->id]))
            $this->events[$event->id] = $event;
    }

    /**
     * Set option value
     * @param $key
     * @param $value
     */
    public function setOption($key, $value) {
        $this->options[$key] = $value;
    }

    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return mixed value
     */
    public function __get($property) {
        if(in_array($property, array(
            'events','options'

        ))) return $this->$property;

        throw new PropertyAccessException($this, $property);
    }

    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if(in_array($property, array(
            'events','options'
        ))){
            $this->$property = $value;

        } else {
            throw new PropertyAccessException($this, $property);
        }
    }

}
