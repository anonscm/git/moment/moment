<?php

/**
 *     Moment - LangUtil.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Class LangUtil - Utility class to translate a token in any available language (prefer using Lang if translating in users context/lang)
 */
class LangUtil {
    /**
     * Translations (lang_id to translated version)
     */
    private static $translations = null;

    /**
     * Loader lock
     */
    private static $loading = false;

    /**
     * Translate $id in requested $lang
     *
     * @param $id
     * @param null $lang
     * @return Translation
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    public static function tr($id, $lang = null) {
        if(!isset($lang))
            $lang = Config::get('lang.default');

        static::loadDictionaries($lang);

        $ret = '{'.$id.'}';
        $t = array();

        if(isset(static::$translations[$lang][$id])) {
            $t = static::$translations[$lang][$id];
        } elseif(isset(static::$translations[Config::get('lang.default')][$id])) {
            $t = static::$translations[Config::get('lang.default')][$id];
        }

        if(isset($t['text'])) {
            $ret = $t['text'];
        }

        // File based ? Then loads it up and cache contents
        if(array_key_exists('text', $t) && is_null($t['text']) && array_key_exists('file', $t)) {
            ob_start(); // Allows for php inside translations
            include $t['file'];
            $s = ob_get_clean();

            $ret = $s;
        }

        return new Translation($ret);
    }

    /**
     * Load dictionaries
     *
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    private static function loadDictionaries($lang = null) {
        if(isset(static::$translations[$lang]) || static::$loading) return;

        static::$loading = true;

        // Get list of available languages
        $available = Lang::getAvailableLanguages();
        if(!array_key_exists('en', $available)) $available['en'] = array('path' => 'en_AU');

        //If $lang is set we only this lang
        if(isset($lang) && isset($available[$lang])) {
            $available = array($lang => $available[$lang]);
        }

        foreach($available as $code => $lang) {
            static::$translations[$code] = static::loadDictionary($available[$code]['path']);
        }

        static::$loading = false;
    }

    /**
     * Load dictionary
     *
     * @param string $rel_path translations relative path
     *
     * @return array
     *
     * @throws ConfigBadParameterException
     * @throws ConfigMissingParameterException
     */
    private static function loadDictionary($rel_path) {
        Logger::debug($rel_path);

        $dictionary = array();

        // Translations locations
        $locations = array(
            'language/core',
            'language',
            'config/language'
        );

        // Lookup locations for translation files
        foreach($locations as $location) {
            $path = EKKO_ROOT.'/'.$location.'/'.$rel_path;
            Logger::debug($path);

            if(!is_dir($path)) continue;

            // Main translation file
            if(file_exists($path.'/lang.php')) {
                $lang = array();
                include $path.'/lang.php';
                foreach($lang as $id => $s)
                    $dictionary[$id] = array('text' => $s);
            }

            // Extended file name based translations
            foreach(scandir($path) as $i) {
                if(!is_file($path.'/'.$i)) continue;

                if(preg_match('`^([^.]+)\.(te?xt(\.php)?|html?(\.php)?|php)$`', $i, $m)) {
                    if($m[1] == 'lang') continue;
                    if(!array_key_exists($m[1], $dictionary))
                        $dictionary[$m[1]] = array('text' => null);
                    $dictionary[$m[1]]['file'] = $path.'/'.$i;
                }
            }
        }

        return $dictionary;
    }

}
