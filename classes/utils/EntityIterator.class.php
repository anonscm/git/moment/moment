<?php

/**
 *     Moment - EntityIterator.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class EntityIterator
 */
class EntityIterator implements Iterator {

    private $stmt = null;
    private $classname = null;
    private $where = null;
    private $ph = array();
    private $currentEntity = null;

    /**
     * EntityIterator constructor. Initialize the iterator over $o
     * @param $classname
     * @param null $filter
     * @param array $ph
     * @throws Exception
     */
    public function __construct($classname, $filter = null, $ph = array()) {

        if(!is_subclass_of($classname, Entity::name()))
            throw new DetailedException('Trying iterate over non-entity');

        $this->classname = $classname;
        $this->where  = '';
        if($filter)
            $this->where .= ' WHERE '.$filter;

        $this->ph = $ph;
    }

    /**
     * Move forward to next element
     * @link https://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next() {
        if($this->stmt) {
            $this->currentEntity = $this->stmt->fetch(PDO::FETCH_ASSOC);

            if(is_array($this->currentEntity)) {

                //If it is possible to transform into an entity we proceed
                if(method_exists($this->classname, 'fillFromEntityIteratorDBData')) {
                    $this->currentEntity = ($this->classname)::fillFromEntityIteratorDBData($this->currentEntity);
                }
            } else {
                //If we currentEntity is not an entity we finished
                $this->currentEntity = false;
            }
        }

    }


    /**
     * Return the current element
     * @link https://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current() {
        return $this->currentEntity;
    }

    /**
     * Return the key of the current element
     * @link https://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key() {
        if($this->currentEntity) {
            return $this->currentEntity->getUID();
        }
        return null;
    }

    /**
     * Checks if current position is valid
     * @link https://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid() {
        return !is_null($this->currentEntity) && $this->currentEntity;
    }

    /**
     * Rewind the Iterator to the first element
     * @link https://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind() {
        $this->stmt = DBI::prepare('SELECT * FROM ' . ($this->classname)::getDBTable() . $this->where);
        $this->stmt->execute($this->ph);

        $this->next();
    }

}
