<?php

/**
 *     Moment - Faq.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Class Faq
 */
class Faq {
    
    /**
     * @var boolean If FAQ is enabled in configuration
     */
    private $enabled = false;
    
    /**
     * @var string FAQ type (csv,...) 
     */
    private $type;
    
    /**
     * @var string Path to access FAQ datas
     */
    private $path;
    
    /**
     * @var mixed FAQ content
     */
    private $content = array();
    
    
    /**
     * Constructor
     */
    public function __construct() {
        $conFaq = Config::get('faq');
        
        if ($conFaq && isset($conFaq['enabled']) && $conFaq['enabled'] === true && isset($conFaq['type'])) {
            switch ($conFaq['type']){
                case 'csv':
                case 'json':
                    if (isset($conFaq['path']) && is_file($conFaq['path'])){
                        $this->enabled= true;
                        $this->type = $conFaq['type'];
                        $this->path = $conFaq['path'];

                        $this->initContent();
                    }
                break;
            }
        }
    }
    
    /**
     * Allows to init FAQ content (question and answers)
     */
    private function initContent(){
        if (!$this->enabled)
            return;

        try {
            switch ($this->type) {
                case 'csv':
                    $this->initFromCSV();
                    break;
                case 'json' :
                    $this->initFromJSON();
                    break;
            }
        } catch (FileIOException $e) {
            Logger::error('FAQ file unavailable');
        } catch (DetailedException $e) {
            Logger::error('FAQ cannot be initialized');
        }
    }

    /**
     * Initailize FAQ from CSV file
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    private function initFromCSV() {
        $lang = Lang::getCode();
        if ($lang === 'fr') {
            $q = 0;
            $a = 2;
        }else{
            $q = 1;
            $a = 3;
        }
        $fileIOCSV = new FileIOCSV($this->path, false, true, ';');

        $id = 0;
        while ($data = $fileIOCSV->read()) {
            if (!empty($data)  ){
                $question = isset($data[$q])?$data[$q]:'';
                $answer = isset($data[$a])?$data[$a]:'';

                $encoding = "UTF-8";

                if ($question !== '' && $answer !== ''){
                    $this->content[$id] = array(
                        'question' => iconv( $encoding, "UTF-8", $question ),
                        'answer' => iconv( $encoding, "UTF-8", $answer )
                    );
                    $id++;
                }else if ($answer !== ''){
                    $this->content[$id-1]['answer'] .= "<br/>".iconv( $encoding, "UTF-8", $answer );
                }
            }
        }
    }

    /**
     * Initialize FAQ from json file
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     * @throws FileIOCannotReadFileException
     * @throws FileIONotFoundException
     */
    private function initFromJSON() {
        $lang = strtolower(Lang::getCode());

        $question_id = 'question_'.$lang;
        $answer_id = 'answer_'.$lang;

        $faq_entries = FileIOJSON::read($this->path);

        if(is_array($faq_entries)) {
            foreach ($faq_entries as $faq_entry) {
                $answer = is_array($faq_entry->$answer_id)?implode('<br/>', $faq_entry->$answer_id):$faq_entry->$answer_id;

                $this->content[] = array(
                    'question' => $faq_entry->$question_id,
                    'answer' => $answer
                );
            }
        }
    }
    
     /**
     * Getter
     * 
     * @param string $property property to get
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed value
     */
    public function __get($property) {
        if(in_array($property, array(
            'enabled','type','path', 'content'
        ))) return $this->$property;
        
        throw new PropertyAccessException($this, $property);
    }
    
}
