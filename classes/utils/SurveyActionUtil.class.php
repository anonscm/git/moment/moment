<?php

/**
 *     Moment - SurveyActionUtil.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class SurveyActionUtil
 */
class SurveyActionUtil {

    public static function initActionButtonsForTypes(array $actions, array $types) {
        $action_icons_for_type = array();
        foreach ($actions as $action_key => $action) {
            foreach ($types as $type) {
                if (!isset($action_icons_for_type[$type])) {
                    $action_icons_for_type[$type] = array();
                }

                if(in_array($type, $action['for'])) {
                    $action_icons_for_type[$type][$action_key]
                        = '<button data-action="' . $action_key . '" data-tooltip aria-haspopup="true" title="' . Lang::tr($action_key)->out() . '" tabindex="0">'
                        . '<i class="fa ' . $action['icon'] . '" aria-hidden="true"></i>'
                        . '<span class="show-for-sr">' . Lang::tr($action_key) . '</span>'
                        . '</button>';
                } else {
                    $action_icons_for_type[$type][$action_key] = '<i class="fa ' . $action['icon'] . ' fa-invisible" aria-hidden="true"></i>';
                }
            }
        }
        return $action_icons_for_type;
    }

    public static function getForSurvey($survey, $action_icons, $is_guest) {
        //Get max open duration
        $maxOpenedDuration = Config::get('max_opened_duration') ? Config::get('max_opened_duration') : 6;

        //Computing types
        $survey_type = ($survey->is_draft) ? 'draft' : (($survey->is_closed) ? 'closed' : 'opened');

        if ($is_guest && $survey->hide_answers) {
            return implode('', $action_icons[$survey_type . '_wo_results']);
        } else {
            $actions = $action_icons[$survey_type];

            //Removing useless action between closing or reopening
            if('closed' == $survey_type) {

                //We should disable 'reopen_survey' when action is now allowed
                //If survey is closed after initial max date => it has already been reopened => it can't be reopened
                //If survey is closed with a final answer => it can't be reopened
                if($survey->closed > strtotime('+' . $maxOpenedDuration . ' month', $survey->created) || $survey->ultimate_participant != null) {
                    unset($actions['reopen_survey']);
                } else {
                    unset($actions['reopen_survey_disabled']);
                }

                // Survey is already closed no  need for 'end_survey' action
                unset($actions['end_survey']);
            } else {
                unset($actions['reopen_survey']);
                unset($actions['reopen_survey_disabled']);
            }

            return implode('', $actions);
        }
    }

}
