<?php

/**
 *     Moment - UserCleanerUtil.class.php
 *
 * Copyright (C) 2024  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class UserCleanerUtil
 */
class UserCleanerUtil {

    /**
     * We match inactive users (some users have no last_activity)
     */
    private const INACTIVE_CRITERIA = 'DATE(COALESCE(last_activity, \'1970-01-01 00:00:00\')) <= :inactiveDate1 AND DATE(created) <= :inactiveDate2';

    /**
     * Clean inactive users
     * @param bool $forceDryRun
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws DetailedException
     */
    public static function cleanInactiveUsers(bool $forceDryRun = false) {

        //Dry-run mode
        $dryRun = Config::get('user_cleaning.dry_run') || $forceDryRun;

        $startTime = microtime(true);
        Logger::info('** Cleaning of inactive users '.($dryRun?'(Dry run)':'').'- START**');

        //Conservation duration is in monthes
        $conservationDuration = Config::get('user_cleaning.inactive_months') ? Config::get('user_cleaning.inactive_months') : 24;

        //obsolescence date is current - $conservationDuration monthes
        $obsolescenceDate = date('Y-m-d', strtotime('-' . ($conservationDuration) . ' month', time()));

        //We can limit the number of inactive users deleted
        $inactiveUserLimit = Config::get('user_cleaning.limit');

        $criteria = static::INACTIVE_CRITERIA;
        if($inactiveUserLimit && is_int($inactiveUserLimit)) {
            $criteria .= ' ORDER BY last_activity ASC LIMIT '.$inactiveUserLimit;
        }

        //Selecting all users that are inactive
        $toBeDeleted = User::all($criteria,
            array(':inactiveDate1' => $obsolescenceDate, ':inactiveDate2' => $obsolescenceDate)
        );

        $total = static::countInactiveUsers($obsolescenceDate);
        Logger::info("** There are $total user(s) where last activity date is before (or equal) to $obsolescenceDate or NULL");
        $userCounter = 0;
        $surveyCounter = 0;
        $ownerCounter = 0;
        foreach ($toBeDeleted as $user) {
            //User is the only one with this email
            if(count(User::fromUserEmail($user->first_email)) === 1) {
                foreach (Owner::fromEmail($user->first_email) as $owner) {
                    if(count($owner->Survey->Owners) === 1) {
                        $surveyCounter++;
                        if(!$dryRun)
                            $owner->Survey->delete();
                    } else {
                        $ownerCounter++;
                        if(!$dryRun)
                            $owner->delete();
                    }
                }

                if(!$dryRun) {
                    Logger::info("** Deleting '[$user->id]' (email : [$user->first_email])");
                    MomentApplicationMail::prepareUserDeletionEmail($user)->send();
                    $user->delete();
                }
            } else {
                //Other User accounts have this email, we silently delete this User
                if(!$dryRun) {
                    Logger::info("** Deleting silently '$user->id' (email : $user->first_email)");
                    $user->delete();
                }
            }

            $userCounter++;
        }

        $info = $userCounter;
        if($inactiveUserLimit)
            $info .= "/$total";

        Logger::info("** $surveyCounter surveys with only one inactive owner have been deleted".($dryRun?'(Dry run)':'')."**");
        Logger::info("** $ownerCounter owners have been deleted".($dryRun?'(Dry run)':'')."**");
        Logger::info("** Cleaning of $info inactive users took ".sprintf('%.2f',(microtime(true) - $startTime))." seconds ".($dryRun?'(Dry run)':'')."- END**");
    }

    /**
     * Send removal reminder to inactive users
     */
    public static function sendDeletionReminders() {

        Logger::info('** Reminding deletion of inactive users - START**');

        //Conservation duration is in monthes
        $conservationDuration = Config::get('user_cleaning.inactive_months') ? Config::get('user_cleaning.inactive_months') : 24;

        //There are two reminders one 3 months before, one 1 month before
        static::sendDeletionReminder($conservationDuration, 3);
        static::sendDeletionReminder($conservationDuration, 1);

        Logger::info('** Reminding deletion of inactive users - END**');
    }

    private static function countInactiveUsers($obsolescenceDate) {
        $request = 'SELECT COUNT(u.id) AS counter FROM User AS u WHERE '.static::INACTIVE_CRITERIA;
        $check = DBI::prepare($request);
        $check->execute(array(':inactiveDate1' => $obsolescenceDate, ':inactiveDate2' => $obsolescenceDate));

        return $check->fetchColumn();
    }

    private static function sendDeletionReminder(int $conservationDuration, int $noticePeriod) {
        //obsolescence date $noticePeriod month (Survey will be deleted in $noticePeriod months)
        $obsolescenceDatePlusNoticePeriod = date('Y-m-d', strtotime('-' . ($conservationDuration - $noticePeriod) . ' month', time()));

        //Selecting all users that will be deleted
        $toBeReminded = User::all('DATE(last_activity) = :inactiveDate',
            array(':inactiveDate' => $obsolescenceDatePlusNoticePeriod)
        );

        Logger::info('** There are ' . count($toBeReminded) . ' user(s) where last activity date is  ' . $obsolescenceDatePlusNoticePeriod);
        $removalReminderCount = 0;
        foreach ($toBeReminded as $user) {
            $users = User::fromUserEmail($user->first_email);

            //Only remind if the most recent user is the one we are trying to remind
            if($user->id === array_shift($users)->id) {
                $removalReminderCount++;

                MomentApplicationMail::prepareUserDeletionReminderEmail($user, $noticePeriod)->send();
            }
        }

        Logger::info('** '.$removalReminderCount . " user(s) needed a deletion reminder ($noticePeriod month(s))");
    }
}
