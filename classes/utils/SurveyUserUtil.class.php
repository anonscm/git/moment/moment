<?php

/**
 *     Moment - SurveyUserUtil.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class SurveyUserUtil
 */
class SurveyUserUtil {

    private static $currentSurveyId = null;

    private static $currentUser = null;

    /**
     * Gets current user role, email for the survey
     *
     * If $survey is null and user is authenticated then user is considered future owner
     *
     * @param Survey $survey when null we are checking for creation rights
     * @return array ('role' => SurveyUserRole::..., 'name' => ..., 'email' => ...
     */
    public static function getCurrentUser(Survey $survey = null) {

        if (!is_null(self::$currentUser) && !is_null($survey) && self::$currentSurveyId === $survey->id) {
            return self::$currentUser;
        }

        //Once we got the survey, we need to manage rights on it
        $current_user = array();
        $current_user['role'] = SurveyUserRoles::NONE;
        $current_user['reason'] = 'access_denied';

        // Check authentification
        if (Auth::user()) {
            $current_user['name'] = Auth::user()->name;
            $current_user['email'] = Auth::user()->email;

            if (!is_null($survey)) {
                //Important Admin has the owner role on every project
                if (Auth::isAdmin() || $survey->isOwner(Auth::user()->email)) {
                    //Authenticated user is the owner of the survey, he can get every informations
                    $current_user['role'] = SurveyUserRoles::OWNER;
                } else {
                    //Authenticated user is not the owner of the survey,
                    // he may be either guest or the survey is open to everyone

                    //Check if Survey is opened to everyone or user is invited
                    if ($survey->reply_access === Survey::OPENED_TO_EVERYONE
                        || $survey->reply_access === Survey::OPENED_TO_AUTHENTICATED
                        || $survey->isInvited(Auth::user()->email)
                    ) {
                        $current_user['role'] = SurveyUserRoles::REPLIER;
                    } else {
                        // user is not allowed to complete the survey
                        $current_user['role'] = SurveyUserRoles::NONE;
                        $current_user['reason'] = 'requires_invitation';
                    }
                }
            } else {
                //Authenticated user has the role of creator
                $current_user['role'] = SurveyUserRoles::CREATOR;
            }
        } else {
            // User is not authenticated
            //Check if Survey is opened to everyone
            if ($survey && $survey->reply_access === Survey::OPENED_TO_EVERYONE) {
                $current_user['role'] = SurveyUserRoles::REPLIER;
            } else {
                // user is not allowed to complete the survey
                $current_user['role'] = SurveyUserRoles::NONE;
                $current_user['reason'] = 'requires_authentication';
            }
        }
        //getCurrentUser can be called without $survey
        if ($survey) {
            $current_user['can'] = array(
                'view_answers' =>
                    (SurveyUserRoles::OWNER == $current_user['role']
                        || (SurveyUserRoles::REPLIER == $current_user['role']
                            && !$survey->hide_answers)
                    ),
                'view_comments' =>
                    (SurveyUserRoles::OWNER == $current_user['role']
                        || (SurveyUserRoles::REPLIER == $current_user['role']
                            && !$survey->hide_comments))

            );

            //If it's a draft only owner is authorized
            if ($survey->is_draft && SurveyUserRoles::OWNER !== $current_user['role']) {
                $current_user['role'] = SurveyUserRoles::NONE;
                $current_user['reason'] = 'survey_not_found';
            }
            self::$currentSurveyId = $survey->id;
        } else {
            self::$currentSurveyId = null;
        }

        //"caching" value
        self::$currentUser = $current_user;


        return $current_user;
    }
}
