<?php

/**
 *     Moment - Config.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class Config
 *
 * This class is an override of core class Config
 *
 * <b>Important :</b> ONLY FOR TEST USE (see unittests dir.)
 *
 */
class Config {

    /**
     * Actual parameters' values, raw from config file or evaluated
     *
     * Null if not already loaded, array otherwise
     */
    private static $parameters = null;

    /**
     * Parameters override stack
     */
    private static $override = null;

    /**
     * List of already evaluated parameters' keys (special loaders, lambda functions)
     */
    private static $cached_parameters = array();

    /**
     * Merge down
     *
     * @param array $target
     * @param array $set
     */
    private static function merge(&$target, $set) {
        foreach ($set as $k => $v) {
            if (is_array($v) && array_filter(array_keys($v), function ($sk) {
                    return !is_int($sk);
                }) && array_key_exists($k, $target)
            ) {
                // Sub keys, merge
                self::merge($target[$k], $v);

            } else {
                $target[$k] = $v;
            }
        }
    }

    /**
     * Load config file and merge
     *
     * @param string $path
     */
    private static function loadFile($path) {
        $config = array();
        include_once $path;

        self::merge(self::$parameters, $config);
    }

    /**
     * Main loader, loads defaults, main config and virtualhost config if it exists
     *
     * @param string $virtualhost the name of a particular virtualhost to load
     *
     * @throws ConfigFileMissingException
     * @throws ConfigBadParameterException
     */
    private static function load($virtualhost = null) {
        if (!is_null(self::$parameters) && !$virtualhost)
            return; // Do not load twice, except if switching virtualhost

        self::$parameters = array();

        // Load default configuration

        self::loadFile(EKKO_ROOT . '/includes/core/ConfigDefaults.php');

        $defaults_file = EKKO_ROOT . '/includes/ConfigDefaults.php';
        if (file_exists($defaults_file))
            self::loadFile($defaults_file);

        // Check if main config exists
        $main_config_file = EKKO_ROOT . '/config/config.php';
        if (!file_exists($main_config_file))
            throw new ConfigFileMissingException($main_config_file);

        // Load base config
        self::loadFile($main_config_file);

        // Load test config if exists
        if (defined('RUNNING_UNIT_TESTS')) {
            $tests_config_file = EKKO_ROOT . '/unittests/config/config.php';

            if (file_exists($tests_config_file))
                self::loadFile($tests_config_file);
        }

        // Set virtualhost if provided
        if ($virtualhost != null)
            self::$parameters['virtualhost'] = $virtualhost;

        // Load virtualhost config if used
        if ($virtualhost === null)
            $virtualhost = self::get('virtualhost');

        if ($virtualhost) {
            if (!is_string($virtualhost))
                throw new ConfigBadParameterException('virtualhost');

            $config_file = EKKO_ROOT . '/config/' . $virtualhost . '/config.php';
            if (!file_exists($config_file))
                throw new ConfigFileMissingException($config_file); // Should exist even if empty

            self::loadFile($config_file);
        }

        // Load config overrides if any
        $overrides_cfg = self::get('config_overrides');
        if ($overrides_cfg) {
            $overrides_file =
                EKKO_ROOT . '/config/' .
                ($virtualhost ? $virtualhost . '/' : '') .
                'config_overrides.json';

            $overrides = new stdClass();
            if (file_exists($overrides_file))
                $overrides = json_decode(trim(file_get_contents($overrides_file)));

            self::$override = array('file' => $overrides_file, 'parameters' => array());
            foreach ($overrides_cfg as $key => $dfn) {
                // Casting
                if (is_string($dfn)) {
                    $dfn = array('type' => $dfn);
                } else if (is_array($dfn) && !array_key_exists('type', $dfn)) {
                    $dfn = array('type' => 'enum', 'values' => $dfn);
                } else if (!is_array($dfn))
                    throw new ConfigBadParameterException('config_overrides');

                $dfn['value'] = property_exists($overrides, $key) ? $overrides->$key : null;

                self::$override['parameters'][$key] = $dfn;
            }
        }

    }

    public static function clearCached() {
        self::$cached_parameters = array();
    }

    /**
     * Get virtualhosts list
     *
     * @return array virtualhosts names
     */
    public static function getVirtualhosts() {
        $virtualhosts = array();
        foreach (scandir(EKKO_ROOT . '/config') as $item) {
            if (!preg_match('`^(.+)\.conf\.php$`', $item, $match))
                continue;
            $virtualhosts[] = $match[1];
        }
        return $virtualhosts;
    }

    /**
     * Run code for each virtualhost (allow usage of Config class in sub code)
     *
     * @param callable $callback code called for each virtualhost
     */
    public static function callUponVirtualhosts($callback) {
        $virtualhosts = self::getVirtualhosts();

        if (count($virtualhosts)) { // Using virtualhosts
            foreach ($virtualhosts as $name) {
                self::load($name);
                $callback();
            }
        } else { // Not using virtualhosts
            self::load();
            $callback();
        }
    }

    /**
     * Evaluate runtime configuration parameters (main scope function names are not proccessed)
     *
     * @param $param
     * @param $args
     *
     * @return mixed
     */
    private static function evalParameter($param, $args) {
        $value = self::$parameters[$param];

        if ((is_callable($value) && !is_string($value)) || (is_string($value)
            && preg_match('`^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*::[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$`',
                $value, $m)
            && is_callable($value))) {
            $value = call_user_func_array($value, $args); // Does it exists ?
        }

        return $value;
    }

    /**
     * Get value for a configuration parameter with callables evaluation
     *
     * @param string $key parameter name
     * @param mixed ... arguments to forward to callable if defined
     *
     * @return mixed the parameter value or null if parameter is unknown
     *
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     */
    public static function get($key) {
        // Load config if not already done
        self::load();

        $args = func_get_args();
        array_shift($args);

        // Do we require a family ?
        if (substr($key, -1) == '*') {
            $search = substr($key, 0, -1);
            $set = array();
            array_unshift($args, null); // Prepare place for key for sub-calls
            foreach (array_keys(self::$parameters) as $key)
                if (substr($key, 0, strlen($search)) == $search) {
                    $args[0] = $key;
                    $set[substr($key, strlen($search))] = call_user_func_array(get_class() . '::get', $args);
                }

            return $set;
        }

        // Do we require a path ?
        $path = explode('.', $key);
        $param = array_shift($path);

        // If not already cached
        if (!array_key_exists($key, self::$cached_parameters)) {

            // Undef returns null
            if (!array_key_exists($param, self::$parameters))
                return null;

            // Evaluate otherwise
            $value = self::evalParameter($param, $args);

            // Managing '/' on application_url
            if ($key == 'application_url' && substr($value, -1) != '/')
                $value .= '/';

            // Resolve path if any
            while (count($path)) {
                if (!is_array($value)) {
                    $value = null;
                    break;
                }

                if (!array_key_exists($path[0], $value)) {
                    $value = null;
                    break;
                }

                $value = $value[array_shift($path)];
            }

            // Apply override if any
            if (
                is_array(self::$override) &&
                array_key_exists($key, self::$override['parameters']) &&
                !is_null(self::$override['parameters'][$key]['value'])
            ) {
                self::$override['parameters'][$key]['base'] = $value;
                $value = self::$override['parameters'][$key]['value'];
            }

            // Cache and return
            self::$cached_parameters[$key] = $value;
        }

        return self::$cached_parameters[$key];
    }

    /**
     * Get default value (without override)
     *
     * @param string $key parameter name
     * @param mixed ... arguments to forward to callable if defined
     *
     * @return mixed the parameter value or null if parameter is unknown
     */
    public static function getBaseValue($key) {
        self::load();

        if (
            is_array(self::$override) &&
            array_key_exists($key, self::$override['parameters']) &&
            array_key_exists('base', self::$override['parameters'][$key])
        ) return self::$override['parameters'][$key]['base'];

        return call_user_func_array(get_class() . '::get', func_get_args());
    }

    /**
     * Check if parameter exists
     *
     * @param string $key parameter name
     *
     * @return bool
     */
    public static function exists($key) {
        self::load();

        $path = explode('.', $key);
        if (!array_key_exists($path[0], self::$parameters))
            return false;

        array_shift($path);

        if (count($path)) {
            $value = self::get($key);

            while (count($path)) {
                if (!is_array($value)) return false;

                if (!array_key_exists($path[0], $value)) return false;

                $value = $value[array_shift($path)];
            }
        }

        return true;
    }

    /**
     * Get overrides data
     *
     * @return array
     *
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigOverrideDisabledException
     */
    public static function overrides() {
        self::load();

        if (!self::$override)
            throw new ConfigOverrideDisabledException();

        return self::$override['parameters'];
    }

    /**
     * Set override
     *
     * Null values means go back to default value from config
     *
     * @param array $set array of key-values
     * @param bool $save (optional)
     *
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigOverrideDisabledException
     * @throws ConfigOverrideNotAllowedException
     * @throws ConfigOverrideValidationFailedException
     * @throws CoreCannotDeleteFileException
     * @throws CoreCannotWriteFileException
     */
    public static function override($set, $save = true) {
        // Load if not already done
        self::load();

        // If override allowed ?
        if (!self::$override)
            throw new ConfigOverrideDisabledException();

        // Apply any changes
        foreach ($set as $k => $v) {
            // Is override of this parameter allowed ?
            if (!array_key_exists($k, self::$override['parameters']))
                throw new ConfigOverrideNotAllowedException($k);

            // Apply any defined validators, throw if failure
            if (array_key_exists('validator', self::$override['parameters'][$k])) {
                $validators = self::$override['parameters'][$k]['validator'];
                if (!is_array($validators)) $validators = array($validators);

                if (!is_null($v))
                    foreach ($validators as $n => $validator)
                        if (is_callable($validator) && !$validator($v))
                            throw new ConfigOverrideValidationFailedException($k, is_string($validator) ? $validator : 'custom:' . $n);
            }

            // Cache new value
            self::$override['parameters'][$k]['value'] = $v;
        }

        // Do we need to save something ?
        if ($save) {
            // Gather values without ones that got back to default
            $overrides = array();
            foreach (self::$override['parameters'] as $k => $dfn) {
                if (!is_null($dfn['value']))
                    $overrides[$k] = $dfn['value'];
            }

            $file = self::$override['file'];

            // Save if any overrides, remove overrides file otherwise
            if (count($overrides)) {
                Utilities::storeInFile($file, json_encode($overrides));

            } else {
                Utilities::removeFile($file);
            }
        }
    }
}