<?php

/**
 *     Moment - CalendarUtil.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Import of libraries
 */
require_once EKKO_ROOT . '/lib/ics-parser/ICal.php';
require_once EKKO_ROOT . '/lib/ics-parser/Event.php';
use ICal\ICal;

/**
 * Class CalendarUtil
 */
class CalendarUtil {

    private static $color_index = 0;
    private static $colors = array();

    /**
     * Parse a iCalendar / ifb string
     * @param $content
     * @return array
     */
    public static function parse($content) {
        $ical = new ICal();
        $ical->initString($content);

        $events = self::getIcsEvents($ical);

        if(count($events) == 0)
            $events = self::getFreeBusyEvents($ical);

        return $events;
    }

    /**
     * Give an incremental color code (Used to default color setting of Calendar)
     * @return mixed
     */
    public static function getColor() {
        if (count(self::$colors) == 0)
            self::$colors = Config::get('calendar.colors');

        return self::$colors[self::$color_index++ % count(self::$colors)];
    }

    /**
     * get Events from an ics content
     *
     * @param ICal $ical
     * @return array of events
     */
    private static function getIcsEvents(Ical $ical) {
        $events = $ical->events();

        $converted =  array();
        foreach($events as $event) {
            $dtend = self::toTimestamp($event->dtend);
            $dtend_tz = self::formatDate($event->dtend_tz);
            // If event has no end but a duration we must get a dtend
            if(null == $event->dtend) {
                // Event has a duration we can compute duration
                if (property_exists($event, 'duration_array') && $event->duration_array[2] instanceof DateInterval) {
                    $dtend = self::applyDateInterval($event->dtstart, $event->duration_array[2]);
                    //dtend_tz duration is already applied
                } else if(self::isDate($event->dtstart)) {
                    //Event start is a date and has no end nor duration, the event's duration is taken to be one day. (See RFC 5545 3.6.1.  Event Component)
                    $dtend = self::toTimestamp($event->dtstart) + 86400;
                    $dtend_tz = (new DateTime($event->dtstart_tz))->add(new DateInterval('P1D'))->format('Y-m-d\TH:i:s');
                } else {
                    //Event start is a datetime and has no end nor duration, end must be equal to start. (See RFC 5545 3.6.1.  Event Component)
                    $dtend = self::toTimestamp($event->dtstart);
                    //dtend_tz is already dtstart_tz
                }
            }
            $ev = array(
                'uid' => $event->uid,
                'title' => $event->summary?$event->summary:'',
                'start_ts' => self::toTimestamp($event->dtstart),
                'end_ts' => $dtend,
                'start' => self::formatDate($event->dtstart_tz),
                'end' => $dtend_tz,
            );
            if(self::isAllDayEventArray($ev))
                $ev['allDay'] = true;

            $converted[] = $ev;
        }
        return $converted;
    }

    /**
     * get Events from an ifb content
     * @param ICal $ical
     * @return array
     */
    private static function getFreeBusyEvents(ICal $ical) {
        $events = $ical->freeBusyEvents();

        $converted =  array();
        foreach($events as $event) {
            if(array_key_exists('FREEBUSY', $event) && is_array($event['FREEBUSY'])) {
                foreach ($event['FREEBUSY'] as $e) {
                    //start and end are store in $e[1]
                    if(isset($e[1]) && isset($e[1][0]) && isset($e[1][1])) {
                        $ev = array(
                            'title' => '',
                            'start_ts' => self::toTimestamp($e[1][0]),
                            'end_ts' => self::toTimestamp($e[1][1]),
                            'start' => DateUtil::convertDateToTimezone((new DateTime($e[1][0])), $ical->calendarTimeZone(), 'Y-m-d\TH:i:s'),
                            'end' => DateUtil::convertDateToTimezone((new DateTime($e[1][1])), $ical->calendarTimeZone(),'Y-m-d\TH:i:s'),
                        );

                        if(self::isAllDayEventArray($ev))
                            $ev['allDay'] = true;

                        $converted[] = $ev;
                    }
                }
            }
        }
        return $converted;
    }

    private static function toTimestamp($date) {
        return (new DateTime($date))->getTimestamp();
    }

    private static function formatDate($date, $output_format = 'Y-m-d\TH:i:s') {
        return (new DateTime($date))->format($output_format);
    }

    private static function applyDateInterval($date, DateInterval $interval) {
        return (new DateTime($date))->add($interval)->getTimestamp();
    }

    private static function isDate($date) {
        return (new DateTime($date))->format('Ymd') == $date;
    }

    private static function isAllDayEventArray($event_array) {
        $dt = (new DateTime($event_array['start']));
        $dstart = $dt->modify($event_array['start'])->format('H:i:s');
        $dend = $dt->modify($event_array['end'])->format('H:i:s');

        return ('00:00:00' == $dstart && $dstart == $dend);

    }

}
