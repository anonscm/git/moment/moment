<?php

/**
 *     Moment - ParticipantRecoveryUtil.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class ParticipantRecoveryUtil
 */
class ParticipantRecoveryUtil {

    const RECOVERY_CODE_LENGTH = 6;
    const RECOVERY_CODE_MAX_TRIES = 3;
    const RECOVERY_CODE_VALIDITY = 15;

    private static $current = null;

    /**
     * @param Survey $survey
     * @param $email
     * @return bool
     * @throws BadEmailException
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws DetailedException
     * @throws EventHandlerIsNotCallableException
     */
    public static function sendLinkToParticipant(Survey $survey, $email) {

        try {
            $participant = Participant::fromSurveyAndEmail($survey, $email);

            // Send it
            MomentApplicationMail::prepareParticipantRecoveryMail($participant)->send();
        } catch (ParticipantNotFoundException $e) {
            MomentApplicationMail::prepareParticipantRecoveryMail(null, $survey, $email)->send();
        }

        return true;
    }

    /**
     * @param Participant $participant
     * @return bool
     * @throws BadEmailException
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws DetailedException
     * @throws EventHandlerIsNotCallableException
     */
    public static function sendCodeForParticipant(Participant $participant) {

        // Generate a new code
        $code = static::generateCode();

        // Store it
        static::storeCode($participant, $code, 0);

        // Send it
        MomentApplicationMail::prepareParticipantRecoveryCodeMail($participant, $code)->send();

        return true;
    }

    /**
     * @param Participant $participant
     * @param null $code
     * @return bool
     */
    public static function isCodeValid(Participant $participant, $code = null) {
        $storedCode = static::getStoredCode($participant);

        if($storedCode) {
            if($code && $code === $storedCode) {
                static::revocateCode($participant);

                return true;
            } else {
                static::incrementTries($participant);
            }
        }

        return false;
    }

    /**
     * Check if $participant has a stored code
     * @param $participant
     * @return bool
     */
    public static function hasStoredCode($participant) {
        return !is_null(static::getStoredCode($participant));
    }

    /**
     * Check if $participant has a stored code
     * @param $participant
     * @return bool
     */
    public static function getTriesLeft($participant) {
        $triesLeft = 0;

        if(null == static::$current) {
            static::getStoredCode($participant);
        }

        if(isset(static::$current['tries'])) {
            $triesLeft = static::RECOVERY_CODE_MAX_TRIES - static::$current['tries'];
        }

        return $triesLeft;
    }

    private static function getStoredCode($participant) {

        $filename = static::getCodeFileName($participant);

        try {
            if(static::fileExists($filename)) {
                if(!static::fileIsValid($filename)) {
                    //Code is not valid anymore
                    FileIO::delete($filename);
                }

                $content = unserialize(FileIO::readWhole($filename));

                if(isset($content['code'])
                    && $content['code'] != ''
                    && isset($content['tries'])
                    && $content['tries'] < static::RECOVERY_CODE_MAX_TRIES) {
                    static::$current = $content;
                    return $content['code'];
                } else {
                    //Code is not valid anymore
                    ParticipantRecoveryUtil::revocateCode($participant);
                }
            }
        } catch (FileIOException $e) {
        }

        return null;
    }

    private static function generateCode() {
        $code = '';
        for ($i = 0; $i < static::RECOVERY_CODE_LENGTH; $i++) {
            $code .= (string)random_int(0, 9);
        }
        return $code;
    }

    private static function storeCode($participant, $code, $tries) {
        $filename = static::getCodeFileName($participant);

        try {
            // Store It
            FileIO::writeWhole($filename, serialize(
                array(
                    'code' => $code,
                    'tries' => $tries
                )
            ));
        } catch (FileIOException $f) {
            // Nothing to do
        }
    }

    private static function revocateCode($participant) {
        $filename = static::getCodeFileName($participant);

        try {
            // Delete It
            FileIO::delete($filename);

            static::$current = null;
        } catch (FileIOException $f) {
            // Nothing to do
        }
    }

    private static function incrementTries($participant) {
        if(null == static::$current) {
            static::getStoredCode($participant);
        }

        if(isset(static::$current['code']) && static::$current['code'] != '' && isset(static::$current['tries'])) {
            static::$current['tries']++;

            static::storeCode($participant, static::$current['code'], static::$current['tries']);
        }

        return null;
    }

    private static function getCodeFileName(Participant $participant) {
        if (defined('EKKO_ROOT')) {
            return EKKO_ROOT . "/tmp/recovery/".hash('sha256', $participant->id, false);
        }
    }

    private static function fileExists($filename) {
        return file_exists($filename);
    }

    private static function fileIsValid($filename) {
        return filemtime($filename) >= (time() - static::RECOVERY_CODE_VALIDITY * 60);
    }

}
