<?php

/**
 *     Moment - MomentApplicationMail.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Description of MomentApplicationMail
 *
 * This class is a wrapper around core class Application mail
 *
 * @property ApplicationMail $email
 * @property string $name
 * @property array $context
 */
class MomentApplicationMail {

    private $email = null;

    private static $email_cache = array();

    private $name;

    private $context;

    /**
     * MomentApplicationMail constructor.
     *
     * Can't be used. Use static methods to build one
     * @param string $name
     * @param array $context
     */
    private function __construct($name = 'application email', $context = array()) {
        $this->name = $name;

        $this->context = $context;
    }

    /**
     * Getter
     *
     * @param string $property
     *
     * @return mixed
     *
     * @throws PropertyAccessException
     */
    public function __get($property) {
        if(in_array($property, ['email', 'name', 'context']))
            return $this->$property;

        throw new PropertyAccessException(static::class, $property);
    }

    /**
     * Prepares the "Participant-token" email. Email to notify a declarative participant his access token
     *
     * @param Participant $participant ,  the participant who needs this token
     * @param string $participant_notification_reason weither ANSWER_CREATED, COMMENT_CREATED, REMINDER
     * @return MomentApplicationMail
     */
    public static function prepareTokenMail(Participant $participant,
                                            $participant_notification_reason = ParticipantNotificationReasons::REMINDER) {

        $communication = new MomentApplicationMail('participant_token',
            array('survey' => $participant->Survey->id,
                'participant' => MomentUtilities::hash($participant->email)
                ));

        //Only declarative participants have a token
        if (ParticipantTypes::DECLARATIVE === $participant->type
            && $participant->email != '') {

            //Create custom link for participant
            $survey_direct_link_with_token = $participant->Survey->path . '?participant_token=' . $participant->uid;

            // Translating the mail
            $ctn = Lang::translateEmail('participant_token')->r(array(
                'survey' => $participant->Survey,
                'survey_direct_link_with_token' => $survey_direct_link_with_token,
                'reason' => $participant_notification_reason));

            //Create a mail
            $communication->email = new ApplicationMail($ctn, null, Auth::user());

            // The only possible recipient for this email is the participant himself
            $communication->email->to($participant->email, $participant->name);

        }
        return $communication;
    }

    /**
     * Prepares the participant delete confirmation email.
     *
     * @param Participant $participant ,  the participant who needs this confirmation
     * @return MomentApplicationMail
     */
    public static function prepareParticipantDeletionConfirmationMail(Participant $participant) {
        $communication = new MomentApplicationMail('participant_deletion_confirmation',
            array(
                'participant' => MomentUtilities::hash($participant->email),
            )
        );

        //Only declarative participants have a token
        if (ParticipantTypes::DECLARATIVE === $participant->type
            && $participant->email != '') {

            // Translating the mail
            $ctn = Lang::translateEmail('participant_deletion_confirmation')->r(array('survey' => $participant->Survey));

            //Create a mail
            $communication->email = new ApplicationMail($ctn, null, Auth::user());

            // The only possible recipient for this email is the participant himself
            $communication->email->to($participant->email, $participant->name);

        }
        return $communication;
    }

    /**
     * Prepares the "Participant-token" email. Email to notify a declarative participant his access token
     *
     * @param Participant $participant ,  the participant who needs this code
     * @param $code , the code
     * @return MomentApplicationMail
     * @throws DetailedException
     */
    public static function prepareParticipantRecoveryCodeMail(Participant $participant, $code) {
        $communication = new MomentApplicationMail('participant_code',
            array(
                'participant' => MomentUtilities::hash($participant->email),
            )
        );

        //Only declarative participants have a token
        if (ParticipantTypes::DECLARATIVE === $participant->type
            && $participant->email != '') {

            // Translating the mail
            $ctn = Lang::translateEmail('participant_code')->r(array(
                'survey' => $participant->Survey,
                'code' => $code)
            );

            //Create a mail
            $communication->email = new ApplicationMail($ctn, null, Auth::user());

            // The only possible recipient for this email is the participant himself
            $communication->email->to($participant->email, $participant->name);

        }
        return $communication;
    }

    /**
     * Prepares the "Participant-recovery" email. Email to notify a participant his access
     *
     * @param Participant $participant ,  the participant who needs this code
     * @param Survey|null $survey
     * @param string $email
     * @return MomentApplicationMail
     * @throws DetailedException
     */
    public static function prepareParticipantRecoveryMail(Participant $participant = null, Survey $survey = null, $email = '') {
        $name = null;
        if($participant) {
            $email = $participant->email;
            $name = $participant->name;
            $survey = $participant->Survey;
        } else {
            $participant = false;
        }

        $communication = new MomentApplicationMail('participant_recovery',
            array(
                'participant' => MomentUtilities::hash($email),
            )
        );

        //Only declarative participants have a token
        if ($email != '') {
            $link = $survey->path;
            if($participant) {
                if (ParticipantTypes::DECLARATIVE === $participant->type) {
                    $link .= '?participant_token=' . $participant->uid;
                } else {
                    $link .= '?force_auth=true';
                }
            }

            // Translating the mail
            $ctn = Lang::translateEmail('participant_recovery')->r(array(
                    'survey' => $survey,
                    'participant_exists' => $participant?true:false,
                    'participant' => $participant,
                    'survey_participant_link' => $link
                )
            );

            //Create a mail
            $communication->email = new ApplicationMail($ctn, null, Auth::user());

            // The only possible recipient for this email is the participant himself
            $communication->email->to($email, $name);

        }
        return $communication;
    }

    /**
     * Prepares the email for creation
     *
     * @param Survey $survey
     * @return MomentApplicationMail
     * @throws DetailedException
     */
    public static function prepareCreationEmail(Survey $survey) {
        return static::prepareOwnersEmail('creation', $survey);
    }

    /**
     * Prepares the email for modification
     *
     * @param Survey $survey
     * @param string $recipient_email recipient email
     * @param Entity $entity for more info
     * @return MomentApplicationMail
     * @throws DetailedException
     */
    public static function prepareModificationEmail(Survey $survey, $recipient_email, Entity $entity) {
        $communication = new MomentApplicationMail('modification',
            array('survey' => $survey->id,
                'recipient' => MomentUtilities::hash($recipient_email)));

        if ($recipient_email && !empty($recipient_email)) {

            // Getting path for survey (if declarative participant, then we add token)
            $survey_path = $survey->path;
            if($entity instanceof Participant
                && ParticipantTypes::DECLARATIVE === $entity->type) {
                $survey_path .= '?participant_token=' . $entity->uid;
            }

            if (!array_key_exists(sha1('modification' . $survey->id.$survey_path), self::$email_cache)) {

                // Translating the mail
                $ctn = Lang::translateEmail('modification')->r(array(
                    'survey' => $survey,
                    'survey_path' => $survey_path,
                    'owner_requestor' => Auth::user()->name));

                // We cache this content, it's quite likely that we will reuse this modification
                self::$email_cache[sha1('modification' . $survey->id.$survey_path)] = $ctn;
            } else {
                //We use cached content
                $ctn = self::$email_cache[sha1('modification' . $survey->id.$survey_path)];
            }

            //Create a mail
            $communication->email = new ApplicationMail($ctn, null, Auth::user());

            //Mail will be send for authenticated user
            $communication->email->addHeader('Sender', Auth::user()->name.'<'.Auth::user()->email.'>');
            $communication->email->to($recipient_email);
        }
        return $communication;
    }

    /**
     * Prepares the email for closing reminder
     *
     * @param Survey $survey
     * @param string $recipient_email
     * @return MomentApplicationMail
     * @throws DetailedException
     */
    public static function prepareClosingReminderEmail(Survey $survey, $recipient_email = '') {
        return static::prepareEmailForRecipient('closing.reminder', $survey, $recipient_email);
    }

    /**
     * Prepares the email for closing
     *
     * @param Survey $survey
     * @param string $reason
     * @param string $recipient_email
     * @return MomentApplicationMail
     * @throws DetailedException
     */
    public static function prepareClosedEmail(Survey $survey, $reason = '', $recipient_email = '') {
        $communication = new MomentApplicationMail('closure',
            array('survey' => $survey->id,
                'recipient' => MomentUtilities::hash($recipient_email)));

        if ($recipient_email == '')
            throw new MailNoAddressesFoundException();


        $reason_en = '';
        if ($reason !== '') {
            //English translation for reason
            $reason_en = '(' . LangUtil::tr($reason, 'en') . ')';
            $reason = '(' . Lang::tr($reason)->out() . ')';
        }

        if (!array_key_exists(sha1('closure' . $survey->id), self::$email_cache)) {
            // Translating the mail
            $ctn = Lang::translateEmail('closing')->r(array('survey' => $survey,
                'reason' => $reason,
                'reason_en' => $reason_en,
                'ultimate_participant' => $survey->ultimate_participant));

            // We cache this content, it's quite likely that we will reuse this email
            self::$email_cache[sha1('closure' . $survey->id)] = $ctn;
        } else {
            //We use cached content
            $ctn = self::$email_cache[sha1('closure' . $survey->id)];
        }

        //Create a mail
        $communication->email = new ApplicationMail($ctn, null, Auth::user());

        // Setting the only recipient
        $communication->email->to($recipient_email);

        // Only owner recieves the results as attachment
        if ($survey->isOwner($recipient_email)) {
            // We attach results as CSV
            $communication->email->attach(self::prepareResultAttachment($survey));
        }

        // Ics attachment
        $final_attachment = self::prepareFinalAnswerAttachment($survey, $recipient_email);
        if($final_attachment)
            $communication->email->attach($final_attachment);

        return $communication;
    }

    /**
     * Prepares the email for reopening
     *
     * @param Survey $survey
     * @param string $recipient_email
     * @return MomentApplicationMail
     * @throws DetailedException
     */
    public static function prepareReopenedEmail(Survey $survey, $recipient_email = '') {
        return static::prepareEmailForRecipient('reopening', $survey, $recipient_email);
    }


    /**
     * Prepares the email for final answers selected
     *
     * @param Survey $survey
     * @param string $recipient_email
     * @return MomentApplicationMail
     * @throws DetailedException
     */
    public static function prepareFinalAnswersSelectedEmail(Survey $survey, $recipient_email = '') {
        $communication = new MomentApplicationMail('final_answer_selected',
            array('survey' => $survey->id,
                'recipient' => MomentUtilities::hash($recipient_email)));

        if ($recipient_email == '')
            throw new MailNoAddressesFoundException();

        if (!array_key_exists(sha1('final_answer' . $survey->id), self::$email_cache)) {
            //TODO Fix only one answer is known for the participant
            // Translating the mail
            $ctn = Lang::translateEmail('final_answer_selected')->r(array('survey' => $survey,
                'ultimate_participant' => $survey->ultimate_participant));

            // We cache this content, it's quite likely that we will reuse this email
            self::$email_cache[sha1('final_answer' . $survey->id)] = $ctn;
        } else {
            //We use cached content
            $ctn = self::$email_cache[sha1('final_answer' . $survey->id)];
        }

        //Create a mail
        $communication->email = new ApplicationMail($ctn, null, Auth::user());

        // Setting the only recipient
        $communication->email->to($recipient_email);

        // Only owner recieves the results as attachment
        if ($survey->isOwner($recipient_email)) {
            // We attach results as CSV
            $communication->email->attach(self::prepareResultAttachment($survey));
        }

        $final_attachment = self::prepareFinalAnswerAttachment($survey, $recipient_email);
        if($final_attachment)
            $communication->email->attach($final_attachment);

        return $communication;
    }


    /**
     * Prepares the email for deletion
     *
     * @param Survey $survey
     * @return MomentApplicationMail
     * @throws DetailedException
     */
    public static function prepareDeleteEmail(Survey $survey) {
        return static::prepareOwnersEmail('deletion', $survey, self::prepareResultAttachment($survey));
    }

    /**
     * Prepares the email for deletion reminder
     *
     * @param Survey $survey
     * @return MomentApplicationMail
     * @throws DetailedException
     */
    public static function prepareDeletionReminderEmail(Survey $survey) {
        return static::prepareOwnersEmail('deletion.reminder', $survey);
    }

    /**
     * Prepares email of notifying a reply
     * @param Survey $survey
     * @param Participant $participant
     * @return MomentApplicationMail
     * @throws DetailedException
     * @internal param $guest_email
     */
    public static function prepareNewReplierEmail(Survey $survey, Participant $participant) {
        return static::prepareOwnersEmail('new_replier', $survey, null, $participant);
    }

    /**
     * Prepares email of notifying a reply
     * @param Survey $survey
     * @param Participant $participant
     * @return MomentApplicationMail
     * @throws DetailedException
     * @internal param $guest_email
     */
    public static function prepareParticipantDeletedMail(Survey $survey, Participant $participant) {
        return static::prepareOwnersEmail('participant_deleted', $survey, null, $participant);
    }

    /**
     * Prepares email of invitation
     * @param Survey $survey
     * @param $guest_email
     * @param string $participant_notification_reason
     * @return MomentApplicationMail
     * @throws DetailedException
     */
    public static function prepareInvitationEmail(Survey $survey, $guest_email, $participant_notification_reason = '') {
        $communication = new MomentApplicationMail('invitation',
            array('survey' => $survey->id,
                  'guest' => MomentUtilities::hash($guest_email)));

        //
        if ($guest_email && !empty($guest_email)) {

            //Create link
            $survey_direct_link = $survey->path;

            if (!array_key_exists(sha1('invitation' . $survey->id), self::$email_cache)) {
                // Translating the mail
                $ctn = Lang::translateEmail('invitation')->r(array(
                    'survey' => $survey,
                    'survey_direct_link' => $survey_direct_link,
                    'reason' => $participant_notification_reason));

                // We cache this content, it's quite likely that we will reuse this invitation
                self::$email_cache[sha1('invitation' . $survey->id)] = $ctn;
            } else {
                //We use cached content
                $ctn = self::$email_cache[sha1('invitation' . $survey->id)];
            }


            //Create a mail
            $communication->email = new ApplicationMail($ctn, null, Auth::user());

            //Mail will be send for authenticated user
            $communication->email->addHeader('Sender', Auth::user()->email);

            // The only possible recipient is the guest
            $communication->email->to($guest_email);

        }
        return $communication;
    }

    public static function prepareStatsEmail($stats, $recipient_email = '', $period = SurveyStats::WEEK, $periodicity_start_date = '', $periodicity_end_date = '') {

        if ($recipient_email == '') {
            throw new MailNoAddressesFoundException();
        }
        if (!is_array($recipient_email)) {
            $recipient_email = array($recipient_email);
        }

        $communication = new MomentApplicationMail('statistics');

        $keys = array();
        if (count($stats) > 0) {
            $keys = array_keys($stats[0]);
        }

        $periodicity = Lang::tr('weekly')->out();
        $t_format = 'YmdHis';
        switch ($period) {
            case SurveyStats::DAY:
                $periodicity = Lang::tr('daily')->out();
                $t_format = 'Y_m_d_YmdHis';
                break;
            case SurveyStats::WEEK:
                $periodicity = Lang::tr('weekly')->out();
                $t_format = 'Y_W_YmdHis';
                break;
            case SurveyStats::MONTH:
                $periodicity = Lang::tr('monthly')->out();
                $t_format = 'Ym_YmdHis';
                break;
            case SurveyStats::YEAR:
                $periodicity = Lang::tr('yearly')->out();
                $t_format = 'Y_YmdHis';
                break;
        }

        // Translating the mail
        $ctn = Lang::translateEmail('statistics')->r(array(
            'periodicity' => $periodicity,
            'periodicity_start_date' => $periodicity_start_date,
            'periodicity_end_date' => $periodicity_end_date,
            'keys' => $keys,
            'column_count' => count($keys),
            'stats' => $stats
        ));

        //Create a mail
        $communication->email = new ApplicationMail($ctn, null, Auth::user());
        foreach ($recipient_email as $rpt) {
            $communication->email->to($rpt);
        }

        //Creating attachment
        $copy = new MailAttachment();

        // Minus 1 day because this stat email is sent on the Monday (for the previous week)
        $copy->name = Config::get('application_name').date($t_format, strtotime('-1 day')).'.csv';
        $copy->mime_type = 'text/csv';
        $copy->content = (new RestResponseCsv($stats))->data;

        // We attach results as CSV
        $communication->email->attach($copy);

        return $communication;

    }

    /**
     * Prepares the email for deletion reminder
     *
     * @param User $user
     * @return MomentApplicationMail
     * @throws DetailedException
     */
    public static function prepareUserDeletionReminderEmail(User $user, int $noticePeriod) {
        $context = array(
            'user' => $user
        );
        $mail_template = 'user.deletion.reminder';

        $communication = new MomentApplicationMail($mail_template, $context);

        // Translating the mail
        $cacheKey = sha1($mail_template . $noticePeriod);

        if (!array_key_exists($cacheKey, self::$email_cache)) {
            // Translating the mail
            $ctn = Lang::translateEmail($mail_template)->r(['noticePeriod' => $noticePeriod]);

            // We cache this content, it's quite likely that we will reuse this email
            self::$email_cache[$cacheKey] = $ctn;
        } else {
            //We use cached content
            $ctn = self::$email_cache[$cacheKey];
        }

        //Create a mail
        $communication->email = new ApplicationMail($ctn, $user, null);
        $communication->email->to($user->email, $user->name);

        return $communication;
    }

    /**
     * Prepares the email for user deletion
     *
     * @param User $user
     * @return MomentApplicationMail
     * @throws DetailedException
     */
    public static function prepareUserDeletionEmail(User $user) {
        $context = array(
            'user' => $user
        );
        $mail_template = 'user.deletion';

        $communication = new MomentApplicationMail($mail_template, $context);

        // Translating the mail
        $ctn = Lang::translateEmail($mail_template);

        //Create a mail
        $communication->email = new ApplicationMail($ctn, $user, null);
        $communication->email->to($user->email, $user->name);

        return $communication;
    }

    /**
     * Gets results attachment in CSV for Owners
     * @param Survey $survey
     * @return MailAttachment
     */
    private static function prepareResultAttachment(Survey $survey) {
        //Creating attachment
        $copy = new MailAttachment();

        $copy->name = $survey->cleanTitle() . date('YmdHis') . '.csv';
        $copy->mime_type = 'text/csv';
        $copy->content = (new RestResponseCsv($survey->getResults(true)))->data;

        return $copy;
    }

    /**
     * Prepare final answer attachment (if needed)
     *
     * @param Survey $survey
     *
     * @return MailAttachment|null
     *
     * @throws ConfigFileMissingException|ConfigBadParameterException
     */
    private static function prepareFinalAnswerAttachment(Survey $survey, $recipient_email = null) {
        if(!$survey->ultimate_participant) return null;

        $ics = new Ics();
        $ics->setOption('method', IcsConstants::METHOD_PUBLISH);
        foreach($survey->ultimate_participant->Answers as $answer) {
            foreach($answer->choices as $choice) {
                if($choice->value !== YesNoMaybeValues::YES) continue;

                $proposition = $choice->proposition;
                if(!($proposition instanceof DateProposition)) continue;

                $event = MyCalUtil::datePropositionToIcsEvent($proposition);
                $event->status = IcsConstants::STATUS_CONFIRMED;
                $event->organizer = $survey->ultimate_participant->name.' <'.$survey->ultimate_participant->email.'>';
                if($recipient_email) {
                    $event->attendees[] = array(
                        'email' => $recipient_email,
                        'partstat' => IcsConstants::PARTSTAT_ACCEPTED
                    );
                }
                $ics->addEvent($event);
            }
        }

        if(!$ics->events) return null;

        $attachment = new MailAttachment('invite.ics');
        $attachment->content = $ics->getFormatedICS();

        //Clear user ics cache
        MyCalUtil::invalidateIcsCacheForUser($recipient_email);

        return $attachment;
    }

    /**
     * Prepares an email for Owners of the $survey
     * @param $mail_template template to use
     * @param $survey
     * @param MailAttachment|null $attachment
     * @param Participant|null $participant
     * @return MomentApplicationMail
     * @throws BadEmailException
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws DetailedException
     * @throws EventHandlerIsNotCallableException
     */
    private static function prepareOwnersEmail($mail_template, $survey, MailAttachment $attachment = null, Participant $participant = null) {
        $context = array(
            'survey' => $survey
        );

        if($participant) {
            $context['participant'] = MomentUtilities::hash($participant->email);
        }

        $communication = new MomentApplicationMail($mail_template, $context);

        //Sending this mail only to owners (for the moment)
        if (count($survey->Owners) > 0) {

            //No caching necessary
            $r = array(
                'survey' => $survey
            );

            if($participant) {
                $r['participant'] = $participant;
            }

            // Translating the mail
            $ctn = Lang::translateEmail($mail_template)->r($r);

            //Create a mail
            $communication->email = new ApplicationMail($ctn, null, Auth::user());

            // The only possible recipient are the owners
            foreach ($survey->Owners as $owner) {
                $communication->email->to($owner->email);
            }

            if($attachment) {
                $communication->email->attach($attachment);
            }
        }
        return $communication;
    }

    /**
     * Prepare an email based opn template for $recipient_email
     * @param $mail_template template to use
     * @param $survey
     * @param string $recipient_email
     * @return MomentApplicationMail
     * @throws BadEmailException
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws DetailedException
     * @throws EventHandlerIsNotCallableException
     * @throws MailNoAddressesFoundException
     */
    private static function prepareEmailForRecipient($mail_template, $survey, $recipient_email = '') {
        $communication = new MomentApplicationMail($mail_template,
            array('survey' => $survey->id,
                'recipient' => MomentUtilities::hash($recipient_email)));

        if ($recipient_email == '')
            throw new MailNoAddressesFoundException();

        if (!array_key_exists(sha1($mail_template . $survey->id), self::$email_cache)) {
            // Translating the mail
            $ctn = Lang::translateEmail($mail_template)->r(array('survey' => $survey));

            // We cache this content, it's quite likely that we will reuse this email
            self::$email_cache[sha1($mail_template . $survey->id)] = $ctn;
        } else {
            //We use cached content
            $ctn = self::$email_cache[sha1($mail_template . $survey->id)];
        }

        //Create a mail
        $communication->email = new ApplicationMail($ctn, null, Auth::user());

        // Setting the only recipient
        $communication->email->to($recipient_email);

        return $communication;
    }


    /**
     * Handle to send the prepared email
     * @return bool
     */
    public function send() {

        //If sending email is disabled
        if (Config::get('email.enabled') === false)
            return true;

        $context = '';
        foreach($this->context as $key => $value) {
            $context .= ' ['.$key.':'.$value.']';
        }

        if ($this->email) {
            $msgID = "<".MomentUtilities::hash($context)."-".uniqid()
                ."@".parse_url(Config::get('application_url'), PHP_URL_HOST).">";

            Logger::info('[email:'.$this->name.']' .$context. " (Message-ID: $msgID) sent");

            // Generate Message-Id
            $this->email->msg_id = $msgID;

            return $this->email->send();
        }

        return false;
    }

}
