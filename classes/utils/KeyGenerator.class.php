<?php

/**
 *     Moment - KeyGenerator.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class KeyGenerator
 */
class KeyGenerator {

    /**
     * UID / choice key chars
     */
    const CHARS = '0123456789abcdefghijklmnopqrstuvwxyz';

    /**
     * Generate random key
     *
     * @param int $len
     * @param callable $check
     *
     * @return string
     *
     * @throws KeyGeneratorTriedTooMuchException
     */
    public static function generateKey($len, $check) {
        $chars = self::CHARS;
        $tries = 1000;

        do {
            $tries--;
            $key = '';
            for ($i = 0; $i < $len; $i++)
                $key .= $chars[mt_rand(0, strlen($chars) - 1)];
        } while ($tries && !$check($key));

        if (!$tries) throw new KeyGeneratorTriedTooMuchException();

        return $key;
    }


    /**
     * Validates key
     *
     * @param string $key
     * @param int $len
     *
     * @return boolean TRUE if key is valid (length and chars)
     */
    public static function validateKey($key, $len) {
        return preg_match("/(^[" . self::CHARS . "]{" . $len . "}$)/", $key);
    }
}
