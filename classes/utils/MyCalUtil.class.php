<?php

/**
 *     Moment - MyCalUtil.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class MyCalUtil
 */
class MyCalUtil {

    /**
     * Get "MyCal" URL for a user
     * @param User $user
     * @return string
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     */
    public static function getMyCalUrl(User $user) {
        return Config::get('application_url').'rest/MyCal/'.$user->calendar_hash;
    }

    /**
     * Get ics calendar
     * @param $calendar_hash calendar hash
     * @return string
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     */
    public static function getIcsFromHash($calendar_hash) {
        $user = User::fromCalendarHash($calendar_hash);

        if($user) {
            $content = self::getIcsFromCache($user, $calendar_hash);
            if($content) {
                return $content;
            } else {
                $content = self::getIcsFromUser($user);
                self::cacheIcsContent($user, $calendar_hash, $content);
            }
            return $content;
        }
    }

    /**
     * Get Ics for a User
     * @param User $user
     * @param Survey $excludedSurvey Exclude DateProposition
     * @return string
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     */
    public static function getIcsFromUser(User $user, Survey $excludedSurvey = null) {
        $ics = new Ics();

        $includedChoiceValues = [YesNoMaybeValues::YES, YesNoMaybeValues::MAYBE];
        $pastEventLimit = Config::get('calendar.mycal_past_events_limit');
        foreach (Survey::ownedBy($user) as $survey) {
            if (!$survey->has_date_questions || $survey->is_draft || $excludedSurvey == $survey)
                continue;

            // Get the midnight timestamp in survey timezone
            $today = (new DateTime('now', new DateTimeZone($survey->time_zone)))->setTime(0, 0)->getTimestamp();
            $limit = strtotime($pastEventLimit, $today);

            // Get ultimate participant (if exists)
            $ultimateParticipant = $survey->ultimate_participant;

            // If survey is closed we keep only ultimate participant events
            if($survey->isClosed() && !$ultimateParticipant)
                continue;

            foreach ($survey->Questions as $question) {
                if (QuestionTypes::DATE != $question->type)
                    continue;

                $ultimateAnswer = $ultimateParticipant && isset($ultimateParticipant->sorted_answers[$question->id]) ? $ultimateParticipant->sorted_answers[$question->id] : false;

                foreach ($question->propositions as $proposition) {

                    // If an ultimate choice have been made and proposition is not selected => excluded
                    if($ultimateAnswer && !in_array($ultimateAnswer->getValue($proposition->id), $includedChoiceValues))
                        continue;

                    //Only include events not past
                    if (($proposition->base_day + $proposition->base_time) >= $limit) {
                        $ics->addEvent(self::toOwnedEvent($proposition, $user));
                    }
                }
            }
        }

        foreach (Participant::fromAuthenticatedUser($user) as $participant) {
            if (!$participant->Survey->has_date_questions || $participant->Survey->is_draft || $excludedSurvey == $participant->Survey)
                continue;

            $today = (new DateTime('now', new DateTimeZone($participant->Survey->time_zone)))->setTime(0, 0)->getTimestamp();
            $limit = strtotime($pastEventLimit, $today);

            // Get ultimate participant (if exists)
            $ultimateParticipant = $participant->Survey->ultimate_participant;

            // If survey is closed we keep only ultimate participant events
            if($participant->Survey->isClosed() && !$ultimateParticipant)
                continue;

            foreach ($participant->Answers as $answer) {
                if (QuestionTypes::DATE != $answer->Question->type)
                    continue;

                $ultimateAnswer = $ultimateParticipant ? $ultimateParticipant->sorted_answers[$answer->Question->id] : false;

                foreach ($answer->choices as $choice) {
                    //If user answered is own survey only first added event instance is kept
                    //Only include events not past
                    try {
                        // If an ultimate choice have been made and proposition is not selected => excluded
                        if($ultimateAnswer && !in_array($ultimateAnswer->getValue($choice->proposition->id), $includedChoiceValues))
                            continue;

                        if (in_array($choice->value, $includedChoiceValues)
                            && ($choice->proposition->base_day + $choice->proposition->base_time) >= $limit) {
                            $ics->addEvent(self::toParticipantEvent($choice->proposition, $user));
                        }
                    } catch (NotFoundException $e) {
                        //User answered a non existing proposition (proposition deleted by owner)
                    }
                }
            }
        }

        return $ics->getFormatedICS();
    }

    /**
     * Get Ics from cache by hash
     * @param User $user
     * @param string $calendar_hash
     * @return bool|false|string
     */
    private static function getIcsFromCache(User $user, $calendar_hash) {
        if (defined('EKKO_ROOT')) {
            try {
                $filename = self::getCacheFileName($user->email, $calendar_hash);
                if(is_file($filename)) {
                    return FileIO::readWhole($filename);
                }
            } catch (FileIOException $e) {
                // No worries we just cannot use cache
            }
        }
        return false;
    }

    /**
     * Cache content
     * @param User $user
     * @param $calendar_hash
     * @param $content
     * @return false|int
     */
    private static function cacheIcsContent(User $user, $calendar_hash, $content) {
        if (defined('EKKO_ROOT')) {
            try {
                FileIO::writeWhole(self::getCacheFileName($user->email, $calendar_hash), $content);
            } catch (FileIOException $e) {
                // No worries we just cannot use cache
            }
        }
    }

    /**
     * Removes cached ics content for a user
     * @param string $user_email
     */
    public static function invalidateIcsCacheForUser($user_email) {
        if (defined('EKKO_ROOT')) {
            try {
                $user_dir = self::getUserDir($user_email);
                if(is_dir($user_dir)) {
                    FileIO::delete($user_dir, true);
                }
            } catch (FileIOException $e) {
                // No worries we just cannot use cache
            }
        }
    }

    /**
     * Converts a DateProposition to an IcsEvent for Participant
     * @param DateProposition $proposition
     * @param User $user
     * @return IcsEvent
     */
    private static function toParticipantEvent(DateProposition $proposition, User $user) {
        $event = self::datePropositionToIcsEvent($proposition);
        $event->uid = 'participated_'.$event->uid;

        $owners = $proposition->Question->Survey->Owners;
        if($owners && count($owners)) {
            $first_owner = array_shift($owners);
            $event->organizer = $first_owner->name.' <'.$first_owner->email.'>';
        }

        $event->attendees[] = array(
            'email' => $user->name.' <'.$user->email.'>'
        );

        return $event;

    }

    /**
     * Converts a DateProposition to an IcsEvent for Owner
     * @param DateProposition $proposition
     * @param User $user
     * @return IcsEvent
     */
    private static function toOwnedEvent(DateProposition $proposition, User $user) {
        $event = self::datePropositionToIcsEvent($proposition);
        $event->uid = 'owned_'.$event->uid;
        $event->organizer = $user->name.' <'.$user->email.'>';

        return $event;
    }

    /**
     * Converts a DateProposition to an IcsEvent
     * @param DateProposition $proposition
     * @return IcsEvent
     */
    public static function datePropositionToIcsEvent(DateProposition $proposition) {
        $survey = $proposition->Question->Survey;

        $event = new IcsEvent();
        $event->uid = $survey->id.'_'.sha1($proposition->id.$proposition->Question->id)
            .'@'.parse_url(Config::get('application_url'), PHP_URL_HOST);
        $event->id = $event->uid;

        $event->summary = $survey->title.' - '.$proposition->Question->statement;
        $event->created = self::toIcsDateTime($survey->created);
        $event->lastmodified = self::toIcsDateTime($survey->updated);
        $event->status = IcsConstants::STATUS_TENTATIVE;

        // Adding the link to survey in event description
        $accessLink = ($survey->isClosed()?$survey->results_path:($survey->path.'?force_auth=true'));
        $event->description = $survey->description;
        $event->description .= PHP_EOL.Lang::tr('access_link_to_the_survey').' : '.$accessLink.PHP_EOL;
        $event->description_html =$survey->parsed_description;
        $event->description_html .= sprintf("<p><a href=\"%s\">%s</a></p>", $accessLink, Lang::tr('access_link_to_the_survey'));

        switch($proposition->type) {
            case PropositionTypes::DAY :
                $event->dtstart = self::toIcsDate($proposition->base_day);
                $event->dtend = self::toIcsDate($proposition->base_day + 86400);
                break;
            case PropositionTypes::DAY_HOUR:
                $event->dtstart = self::toIcsDateTime($proposition->base_day + $proposition->base_time);
                $event->dtend = self::toIcsDateTime($proposition->base_day + $proposition->base_time + 3600);
                break;
            case PropositionTypes::RANGE_OF_HOURS:
                $event->dtstart = self::toIcsDateTime($proposition->base_day + $proposition->base_time);
                $event->dtend = self::toIcsDateTime($proposition->base_day + $proposition->end_time);
                break;
            case PropositionTypes::RANGE_OF_DAYS:
                $event->dtstart = self::toIcsDate($proposition->base_day);
                $event->dtend = self::toIcsDate($proposition->end_day + 86400);
                break;
        }
        return $event;
    }

    /**
     * Converts a timestamp to the correct Date format for Ics
     * @param $timestamp
     * @return string
     */
    private static function toIcsDate($timestamp) {
        return DateUtil::toLocale($timestamp, 'UTC', '', IcsConstants::DATE_ONLY_FORMAT);
    }

    /**
     * Converts a timestamp to the correct DateTime format for Ics
     * @param $timestamp
     * @return string
     */
    private static function toIcsDateTime($timestamp) {
        return DateUtil::toLocale($timestamp, 'UTC', '', IcsConstants::DATE_FORMAT_UTC);
    }

    /**
     * Get filename of cached ics
     * @param $user_email
     * @param $calendar_hash
     * @return string
     */
    private static function getCacheFileName($user_email, $calendar_hash) {
        $dir = self::getUserDir($user_email);
        return $dir.'/'.$calendar_hash;
    }

    /**
     * Get a key for user
     * @param $user_email
     * @return string
     */
    private static function getUserDir($user_email) {
        return EKKO_ROOT . "/tmp/calendars/".hash('sha256', $user_email, false);
    }

}
