<?php

/**
 *     Moment - MomentUtilities.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Class MomentUtilities
 */
class MomentUtilities {

    private static $known_pages = null;

    /**
     * Get page title, defaults to {cfg:application_name}
     * @return array|mixed
     */
    public static function getPageTitle() {
        $title = Config::get('application_name');
        if (array_key_exists('PATH_INFO', $_SERVER) && $_SERVER['PATH_INFO']) {
            $path = array_filter(explode('/', $_SERVER['PATH_INFO']));
            $page = array_shift($path);
            if($page === 'survey') {
                $title =  static::getSurveyPageTitle($path);
            }

            if($page === 'user_guide') {
                $title = Lang::tr('user_guide').' - '.Config::get('application_name');
            }

            if($page === 'legal') {
                $title = Lang::tr('legal_notice').' - '.Config::get('application_name');
            }

            if($page === 'faq') {
                $title = Lang::tr('faq_title').' - '.Config::get('application_name');
            }

            if($page === 'admin' && Auth::isAdmin()) {
                $title = Lang::tr('administration_actions').' - '.Config::get('application_name');
            }

            if($page === 'user') {
                $title = Lang::tr('my_profile').' - '.Config::get('application_name');
            }

            if($page === 'accessibility') {
                $title = Lang::tr('accessibility').' - '.Config::get('application_name');
            }
        }

        return $title;
    }

    /**
     * Controlling access to pages,
     * by default pages are 'public' (means that they handle access control by their own)
     * There are two arrays $authenticated and $admin, if a page requires authentication or being admin
     * @return bool TRUE access is granted
     * @throws MomentForbiddenException access is forbidden
     * @throws MomentPageNotFoundException requested page is not found
     * @throws MomentPleaseLoginException|AuthUserNotAllowedException requested page needs to be logged in
     */
    public static function accessControl() {

        // Pages that requires authentication
        $authenticated = array('user');
        // Pages that requires being admin
        $admin = array('admin');

        if (array_key_exists('PATH_INFO', $_SERVER) && $_SERVER['PATH_INFO']) {
            $path = array_filter(explode('/', $_SERVER['PATH_INFO']));
            $page = array_shift($path);

            //Checks if page exists
            if(!self::exists($page)) {
                throw new MomentPageNotFoundException($page);
            }

            //Page is in restricted area
            if(in_array($page, $authenticated) && !Auth::isAuthenticated()) {
                throw new MomentPleaseLoginException($page);
            } elseif(in_array($page, $admin) && !Auth::isAdmin()) {
                throw new MomentForbiddenException($page);
            }
            $access = true;
        } else { // no path -> home
            $access = true;
        }

        return $access;
    }

    /**
     * Generate a one way hash key for hashable
     * @param string $hashable
     * @param int $length
     * @return string
     */
    public static function hash($hashable = '', $length = 16) {
        return substr(md5($hashable), 0, $length);
    }

    /**
     * List all pages (template named xxx_page.php in template dirs)
     * @return array|null
     */
    private static function getKnownPages() {

        if(!is_null(self::$known_pages))
            return self::$known_pages;

        $locations = array(
            'config/templates',
            'templates',
            'templates/core');

        $plugins = PluginManager::getEnabledPlugins();

        foreach($plugins as $plugin) {
            $locations[] = str_replace(EKKO_ROOT, '', $plugin['path'].'/templates');
        }

        self::$known_pages = array();


        // Look in possible locations
        foreach($locations as $location) {
            $location = EKKO_ROOT.'/'.$location;
            if(!is_dir($location)) continue;

            $files = array_map(function($path) {
                $basename = basename($path);
                return substr($basename,0, (strlen($basename)-strlen('_page.php')));
            }, glob($location.'/*_page.php'));

            self::$known_pages= array_merge(self::$known_pages, array_values($files));
        }

        return self::$known_pages;
    }

    /**
     * Checks if this page can be displayed
     * @param $page
     * @return bool
     */
    private static function exists($page) {
        return in_array($page, self::getKnownPages());
    }

    /**
     * Get page title for survey ressources
     * @param $path
     * @return string
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    private static function getSurveyPageTitle($path) {
        $title = array();
        $human_readable_id = false;
        $action = array_shift($path);

        switch($action) {
            case 'create':
                $title[] =  Lang::tr('survey_create');
                break;
            case 'manage':
                $title[] = Lang::tr('manage_surveys');
                break;
            case 'update':
                $title[] = Lang::tr('updating_survey');
                $human_readable_id = array_shift($path);
                break;
            case 'results':
                $title[] = Lang::tr('results_of_survey');
                $human_readable_id = array_shift($path);
                break;
            default :
                $title[] = Lang::tr('reply_to_survey');
                $human_readable_id = $action;
                break;
        }

        if($human_readable_id) {
            try {
                $survey = Survey::fromHumanReadableId($human_readable_id);
                $current_user = SurveyUserUtil::getCurrentUser($survey);

                if(SurveyUserRoles::REPLIER > $current_user['role']) {
                    $title = array(Lang::tr('access_denied'));
                } else {
                    $title[] = $survey->title;
                }
            } catch(SurveyNotFoundException $e) {
                $title = array(Lang::tr('access_denied'));
            } catch(NotFoundException $e) {
                $title = array(Lang::tr('access_denied'));
            }
        }

        $title[] = Config::get('application_name');
        return implode(' - ', $title);
    }

    /**
     * Get infos ul on $momentsOfDay
     * @param array $momentsOfDay
     * @return string
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    public static function getMomentsOfDayInfos(array $momentsOfDay): string {
        $momentsOfDayInfo = '';
        if($momentsOfDay && count($momentsOfDay) > 0) {
            $momentsOfDayInfo = '<ul>';
            foreach($momentsOfDay as $name => $momentCfg) {
                $momentsOfDayInfo .= '<li>'.Lang::tr('input_mode_info_moments_row')->r(['name' => Lang::tr($name)->out(), 'bt' => $momentCfg['bt'], 'et' => $momentCfg['et']])->out().'</li>';
            }
            $momentsOfDayInfo .= '</ul>';
        }
        return $momentsOfDayInfo;
    }

    /**
     * Hook to add specific scripts for specific page
     * @return void
     */
    public static function includePageSpecificScripts():void {

        $scripts = (new Event('page_js_libraries'))->trigger(function() {
            return [];
        });

        foreach(GUI::path($scripts) as $path){
            echo '<script type="text/javascript" src="'.$path.'"></script>'."\n";
        }

    }

    /**
     * Hook to add specific stylesheets for specific page
     * @return void
     */
    public static function includePageSpecificStylesheets():void {

        $stylesheets = (new Event('page_css_libraries'))->trigger(function() {
            return [];
        });

        foreach(GUI::path($stylesheets) as $path){
            echo '<link type="text/css" rel="stylesheet" href="'.$path.'" />'."\n";
        }

    }
}
