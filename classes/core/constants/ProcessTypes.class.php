<?php

/**
 *     Moment - ProcessTypes.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Class containing process types
 */
class ProcessTypes extends Enum{
    /**
     * Log levels
     */
    const MISC      = 'misc';
    
    const WEB       = 'web';
    const CLI       = 'cli';
    
    const GUI       = 'gui';
    const REST      = 'rest';
    
    const CRON      = 'cron';
    const FEEDBACK  = 'feedback';
    
    const INSTALL   = 'install';
    const UPGRADE   = 'upgrade';
    
    const UNITTEST  = 'unittest';
}
