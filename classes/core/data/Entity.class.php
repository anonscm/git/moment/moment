<?php

/**
 *     Moment - Entity.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Base class for database stored objects
 *
 * @property-read bool $storedInDatabase
 * @property-read string $dataTable
 */
abstract class Entity {
    
    /**
     * Defines data in database
     * 
     * Used by fromDBData / toDBData methods
     * Can be used to generate database creation queries
     * 
     * Associative array of <field> => <field_def>
     * 
     * <field_def> associative array of field definition entries in :
     *   - type : int, uint, string, bool, enum, text, date, datetime, time
     *   - size (for int types) : tiny, medium, big (defaults to medium)
     *   - size (for string types) : string length
     *   - values (for enum types) : array of possible values
     *   - null : bool indicating if field can be null
     *   - primary : bool indicating if field is primary key
     *   - autoinc : bool indicating if field is auto-incremented
     *   - unique : bool indicating if field is unique or string pointing unicity column set
     *   - default : default value for this field
     */
    protected static $dataMap = array();
    
    
    /**
     * Constants used for metadata management
     */
    const METADATA_DATAMAP = 'datamap';
    const METADATA_PKS = 'pks';
    const METADATA_PLURALNAME = 'plural_name';
    const METADATA_RELATIONS = 'relations';
    
    
    /**
     * Relation states
     */
    const RELATION_NONE         = 0;
    const RELATION_ONE_TO_ONE   = 1;
    const RELATION_ONE_TO_MANY  = 2;
    const RELATION_MANY_TO_ONE  = 3;
    const RELATION_MANY_TO_MANY = 4;
    
    /**
     * @var array Self to Many dependencies
     */
    protected static $hasMany = array();
    
    /**
     * @var array Self to One dependencies
     */
    protected static $hasOne = array();
    
    /**
     * @var array plural hasManys (shared !)
     */
    protected static $pluralHasMany = array();
    
    /**
     * @var array foreign keys
     */
    protected $relationKeys = array();
    
    /**
     * @var boolean True if object stored in database, false otherwise
     */
    protected $storedInDatabase = false;
    
    /**
     * @var array instance data cache
     */
    protected $dataCache = array();

    /**
     * DataMap getter
     *
     * @param boolean $ignore_relations
     *
     * @return array the class dataMap
     *
     * @throws ModelViolationException
     */
    public static function getDataMap($ignore_relations = false) {
        $datamap = EntityCache::getMetadata(static::name(), self::METADATA_DATAMAP);
        
        if(!$datamap) {
            $datamap = array();
            
            $raw_datamap = array();
            
            // Has parent which is not Entity ?
            $parent = get_parent_class(static::name());
            if($parent != Entity::name()){
                /** @var Entity $parent */
                $raw_datamap = $parent::getDataMap($ignore_relations);
            }
            
            // Add local data map
            $raw_datamap = array_merge($raw_datamap, static::$dataMap);
            
            foreach($raw_datamap as $f => $dfn) {
                $f = strtolower($f);
                if(array_key_exists($f, $datamap))
                    throw new ModelViolationException(static::name().' property '.$f.' conflicts with other (case insensitive)');
                
                $datamap[$f] = $dfn;
            }
            
            foreach(static::getHasOne() as $other) {
                $refers = false;
                if(substr($other, 0, 1) == '@') {
                    $refers = true;
                    $other = substr($other, 1);
                }
                
                $relation = static::getRelationWith($other);
                
                if($relation == self::RELATION_ONE_TO_ONE) {
                    if(!$refers) continue;
                    
                } else if($relation != self::RELATION_MANY_TO_ONE)
                    continue;
                
                foreach($other::getDataMap(true) as $f => $dfn) {
                    if(!array_key_exists('primary', $dfn) || !$dfn['primary']) continue;
                    
                    $rf = strtolower($other.'_'.$f);
                    if(count(preg_grep('`^'.preg_quote($rf, '`').'$`i', array_keys($datamap))))
                        throw new ModelViolationException(static::name().' property '.$rf.' conflicts with relation with '.$other);
                    
                    if(array_key_exists('transform', $dfn))
                        throw new ModelViolationException($other.' relation involved field cannot use transformation');
                    
                    if(array_key_exists('property', $dfn))
                        throw new ModelViolationException($other.' relation involved field cannot use custom property name');
                    
                    unset($dfn['primary']);
                    unset($dfn['autoinc']);
                    $dfn['relation_key'] = array('class' => $other, 'field' => $f);
                    $dfn['index'] = strtolower($other);
                    $datamap[$rf] = $dfn;
                }
            }
            EntityCache::setMetadata(static::name(), self::METADATA_DATAMAP, $datamap);
        }
        
        return $ignore_relations ? array_filter($datamap, function($dfn) {
            return !array_key_exists('relation_key', $dfn);
        }) : $datamap;
    }
    
    /**
     * Get relation column for given field/property
     * 
     * @param string $other
     * @param string $property
     * 
     * @return array or string
     */
    public static function getRelationColumn($other, $property = null) {
        $keys = array();
        foreach(static::getDataMap() as $column => $dfn) {
            if(!array_key_exists('relation_key', $dfn)) continue;
            if($dfn['relation_key']['class'] !== $other) continue;
            
            $keys[$dfn['relation_key']['field']] = $column;
        }
        
        if(is_null($property))
            return $keys;
        
        return array_key_exists($property, $keys) ? $keys[$property] : null;
    }
    
    /**
     * HasMany getter
     * 
     * @return array
     */
    public static function getHasMany() {
        return static::$hasMany;
    }
    
    /**
     * HasOne getter
     * 
     * @return array
     */
    public static function getHasOne() {
        return static::$hasOne;
    }

    /**
     * Constructor
     *
     * @param integer $id identifier of user to load from database (null if loading not wanted)
     * @param array $data data to create the user from (if already fetched from database)
     *
     * @throws ModelViolationException
     * @throws NotFoundException
     */
    protected function __construct($id = null, $data = null) {
        if(!is_null($id)) {
            if (!is_array($id)) $id = array('id' => $id);
            
            $where = array();
            $values = array();
            foreach (static::getPrimaryKeys() as $pk){
                if (!array_key_exists($pk, $id)) 
                    throw new ModelViolationException('Missing primary key '.$pk.' when loading instance of '.  static::name());
                
                $where[] = $pk.' = :'.$pk;
                $values[':'.$pk] = $id[$pk] ;
            }
            if (!count($where))
                throw new ModelViolationException('Instance selector build failed: '.  static::name());
                
            // Load from database if id given
            $statement = DBI::prepare('SELECT * FROM '.static::getDBTable().' WHERE '.  implode(' AND ', $where));
            $statement->execute($values);
            $data = $statement->fetch();
            if(!$data) {
                $ecls = static::name().'NotFoundException';
                if(class_exists($ecls))
                    throw new $ecls($id);
                
                throw new NotFoundException(static::name(), $id);
            }
        }
        
        if($data) {
            $this->fillFromDBData($data);
            $this->storedInDatabase = true;
        }
    }
    
    /**
     * Cached getter relying on id
     * 
     * Only creates the object if it was not cached before, calls constructor with id otherwise
     * 
     * @param mixed $id primary key of the object
     * 
     * @return static
     */
    public static function fromId($id) {
        $object = EntityCache::getEntity(static::name(), $id);
        if($object) return $object;
        
        $object = new static($id);
        EntityCache::setEntity($object);
        
        return $object;
    }
    
    /**
     * Cached getter relying on data
     * 
     * Only creates the object if it was not cached before, calls constructor with data otherwise
     * then updates object properties
     * 
     * @param array $data data to create the object from instead of loading it (used by "get all" queries)
     * 
     * @return self
     */
    public static function fromData($data) {
        $object = EntityCache::getEntity(static::name(), $data);
        
        if($object) {
            $object->fillFromDBData($data);
        } else {
            $object = new static(null, $data);
            EntityCache::setEntity($object);
        }
        
        return $object;
    }

    /**
     * Get instances related to given entities, filling relation cache, may be used for preloading
     *
     * @param Entity[] $others
     *
     * @return array
     *
     * @throws ModelViolationException
     */
    public static function from($others) {
        if(is_object($others))
            $others = array($others);
        
        $classes = array();
        foreach($others as $other) {
            if(!is_object($other) || !($other instanceof Entity))
                throw new ModelViolationException('Trying to load from non-Entity');
            
            $class = get_class($other);
            if(!static::getRelationWith($class)) continue;
            
            $uid = $other->getUID();
            if(is_null($uid))
                throw new ModelViolationException('Trying to load from unsaved '.$class);
            
            $classes[$class][$uid] = $other;
        }
        
        if(!count($classes)) return array();
        
        $entities = array();
        foreach($classes as $other_class => $others) {
            /** @var Entity $other_class */
            $relation = static::getRelationWith($other_class);
            
            $get_one = ($relation == self::RELATION_ONE_TO_ONE);
            $refers = $get_one && in_array('@'.$other_class, static::getHasOne());
            
            if($relation == self::RELATION_ONE_TO_MANY || ($get_one && $refers)) {
                // Collect relation columns
                $keys = array_flip(static::getRelationColumn($other_class));
                
                // Collect other ids
                $where = array();
                $ph = array();
                $idx = 0;
                foreach($others as $other) {
                    /** @var Entity $other */
                    $uid = $other->getUID(false);
                    
                    $and = array();
                    foreach($keys as $local_field => $other_key) {
                        $and[] = $local_field.' = :'.$other_key.'___'.$idx;
                        $ph[':'.$other_key.'___'.$idx] = $uid[$other_key];
                    }
                    $where[] = '('.implode(' AND ', $and).')';
                    
                    $idx++;
                }
                
                $q = static::query(implode(' OR ', $where), $ph);
                $other_uid = null;
                foreach($q['items'] as $entity) {
                    $other_uid = $other_class::buildUID($entity->relationKeys[$other_class]);
                    
                    EntityCache::setRelation($entity, $others[$other_uid], $q['id']);
                    
                    $entities[$other_class][$other_uid][] = $entity;
                }
                
                foreach($entities[$other_class] as $other_uid => $locals)
                    EntityCache::setRelation($others[$other_uid], $locals, $q['id']);
                
                if($get_one)
                    $entities[$other_class][$other_uid] = array_shift($entities[$other_class][$other_uid]);
                
            } else if($relation == self::RELATION_MANY_TO_ONE || ($get_one && !$refers)) {
                
                // Collect local ids
                $where = array();
                $ph = array();
                $idx = 0;
                $others_by_local = array();
                $other_by_id = array();
                $local_by_other = array();
                foreach($others as $other) {
                    $uid = $other->getRelatedUID(static::name(), false);
                    $suid = $other->getRelatedUID(static::name(), true);
                    
                    $other_by_id[$suid] = $other;
                    $others_by_local[$suid][] = $other;
                    $local_by_other[$other->getUID(false)] = $suid;
                    
                    $and = array();
                    foreach($uid as $field => $value) {
                        $and[] = $field.' = :'.$field.'___'.$idx;
                        $ph[':'.$field.'___'.$idx] = $value;
                    }
                    $where[] = '('.implode(' AND ', $and).')';
                    
                    $idx++;
                }
                
                $q = static::query(implode(' OR ', $where), $ph);
                $entities[$other_class] = $q['items'];
                
                $local_by_id = array();
                foreach($entities[$other_class] as $local) {
                    /** @var Entity $local */
                    $uid = $local->getUID();
                    
                    $local_by_id[$uid] = $local;
                    
                    EntityCache::setRelation($local, $others_by_local[$uid], $q['id']);
                }
                
                foreach($local_by_other as $oid => $uid)
                    EntityCache::setRelation($other_by_id[$oid], $local_by_id[$uid], $q['id']);
                
                if($get_one)
                    $entities[$other_class] = array_shift($entities[$other_class]);
                
            } else if($relation == self::RELATION_MANY_TO_MANY) {
                $rtbl = static::getRelationDBTable($other_class);
                
                $where = array();
                $ph = array();
                $idx = 0;
                $others = array();
                foreach($others as $other) {
                    $uid = $other->getRelatedUID(static::name(), false);
                    $others[$uid] = $other;
                    
                    $and = array();
                    foreach($uid as $field => $value) {
                        $and[] = $field.' = :'.$field.'___'.$idx;
                        $ph[':'.$field.'___'.$idx] = $value;
                    }
                    $where[] = '('.implode(' AND ', $and).')';
                    
                    $idx++;
                }
                
                // Init query to get all primary keys
                $query = 'SELECT * FROM '.$rtbl.' WHERE '.implode(' OR ', $where);
                
                $key = EntityCache::getCollectionKey(array($query, $ph));
                
                // Get join name
                $joinName = array(static::name(), $other_class);
                sort($joinName);
                $joinName = implode('_', $joinName);
                
                $join = EntityCache::getCollection($joinName, array($query, $ph));
                
                if(is_null($join)) {
                    $statement = DBI::prepare($query);
                    $statement->execute($ph);
                    
                    foreach($statement->fetchAll() as $r) {
                        $couple = array();
                        
                        foreach($r as $field => $value) {
                            list($class, $field) = explode('_', $field, 2);
                            $couple[$class][$field] = $value;
                        }
                        
                        foreach($couple as $class => $keys)
                            ksort($couple[$class]);
                        
                        $join[] = $couple;
                    }
                    
                    EntityCache::setCollection($joinName, array($query, $ph), $join);
                }
                
                $where = array();
                $ph = array();
                $idx = 0;
                $locals_for_others = array();
                foreach($join as $couple) {
                    $s_local_uid = $couple[strtolower(static::name())];
                    $local_uid = implode('-', $s_local_uid);
                    $s_other_uid = $couple[strtolower($other_class)];
                    $other_uid = implode('-', $s_other_uid);
                    
                    $locals_for_others[$local_uid][] = $other_uid;
                    
                    $and = array();
                    foreach($s_local_uid as $field => $value) {
                        $and[] = $field.' = :'.$field.'___'.$idx;
                        $ph[':'.$field.'___'.$idx] = $value;
                    }
                    $where[] = '('.implode(' AND ', $and).')';
                    
                    $idx++;
                }
                
                $q = static::query(implode(' OR ', $where), $ph);
                $entities[$other_class] = $q['items'];
                
                $locals_by_other = array();
                foreach($entities[$other_class] as $entity) {
                    /** @var Entity $entity */
                    $uid = $entity->getUID();
                    $other_uids = $locals_for_others[$uid];
                    
                    foreach($other_uids as $other_uid)
                        $locals_by_other[$other_uid][] = $entity;
                }
                
                foreach($locals_by_other as $other_uid => $locals)
                    EntityCache::setRelation($others[$other_uid], $locals, array($key, $q['id']));
                
            }
        }
        
        if(count($entities) == 1) // Only one related class
            $entities = reset($entities);
        
        return $entities;
    }

    /**
     * Get a set objects
     *
     * @param string $criteria sql criteria
     * @param array $placeholders
     * @param callable $run will be applied to all objects, return values will replace objects and result will be filtered to remove nulls
     *
     * @return array
     */
    public static function all($criteria = null, $placeholders = array(), $run = null) {
        $q = static::query($criteria, $placeholders);
        $objects = $q['items'];
        
        // Apply callback if provided
        if($run && is_callable($run)) {
            foreach($objects as $id => $o) {
                $objects[$id] = $run($o);
            }
            $objects = array_filter($objects, function($o) {
                return !is_null($o);
            });
        }
        return $objects;
    }

    /**
     * Query entities database
     *
     * @param string $query sql criteria
     * @param array $placeholders
     *
     * @return array
     *
     * @throws BadFormatException
     */
    public static function query($query = null, $placeholders = array()) {
        if($query && !is_string($query))
            throw new BadFormatException($query, 'SQL query or WHERE criteria');

        if(!$query)
            $query = 'SELECT * FROM '.static::getDBTable();

        $query = trim($query);

        if(!preg_match('`^select\s+`i', $query))
            $query = 'SELECT * FROM '.static::getDBTable().' WHERE '.$query;
        
        $id = EntityCache::getCollectionKey(array($query, $placeholders));
        
        $objects = EntityCache::getCollection(static::name(), array($query, $placeholders));
        
        if(is_null($objects)) {
            // Prepare query depending on contents
            if(preg_match('`\s+[^\s]+\s+IN\s+:[^\s]+\b`i', $query)) {
                $statement = DBI::prepareInQuery($query, array_filter($placeholders, 'is_array'));
            } else {
                $statement = DBI::prepare($query);
            }
            
            // run it
            $statement->execute($placeholders);
            
            // Fetch records, register them with id build from primary key(s) value(s)
            $objects = array();
            foreach($statement->fetchAll() as $r) 
                $objects[] = static::fromData($r);
            
            EntityCache::setCollection(static::name(), array($query, $placeholders), $objects);
        }
        
        return array('id' => $id, 'items' => $objects);
    }
    
    /**
     * Get database table name
     * 
     * Database table has the same name as the class, except it plural (class User => table Users).
     * 
     * @param boolean $with_prefix True if prefix asked, false otherwhise
     * 
     * @return string table name
     */
    public static function getDBTable($with_prefix = true) {
        $class = static::name();
        
        $name = property_exists($class, 'dataTable') ? static::$dataTable : static::name(true);
        
        if($with_prefix) $name = Config::get('db.table_prefix').$name;
        
        return $name;
    }

    /**
     * Allows to get the relation database name
     *
     * @param mixed $other
     *
     * @return string
     *
     * @throws ModelViolationException
     */
    public static function getRelationDBTable($other){
        if(is_object($other) && $other instanceof Entity)
            $other = get_class($other);
        
        $relation = static::getRelationWith($other);
        
        if($relation != self::RELATION_MANY_TO_MANY)
            throw new ModelViolationException('No many-to-many relation between '.static::name().' and '.$other);
        
        $tables = array(static::getDBTable(false), $other::getDBTable(false));
        sort($tables);
        
        return Config::get('db.table_prefix').implode('_', $tables);
    }

    /**
     * Insert object as new record
     *
     * @param array $data database compliant data such as returned by toDBData
     *
     * @return array
     */
    public static function insertRecord($data) {
        $event = new Event('entity_insert', static::name(), $data);
        
        $table = static::getDBTable();
        $datamap = static::getDataMap();
        
        return $event->trigger(function($class, $data) use($table, $datamap) {
            
            // Remove autoinc keys
            foreach($datamap as $field_name => $dfn)
                if(array_key_exists('autoinc', $dfn) && $dfn['autoinc'])
                    if(array_key_exists($field_name, $data)) 
                        unset($data[$field_name]);
            
            // Insert data
            $values = array();
            foreach($data as $field_name => $value)
                $values[':'.$field_name] = $value;
            
            $s = DBI::prepare('INSERT INTO '.$table.'('.implode(', ', array_keys($data)).') VALUES(:'.implode(', :', array_keys($data)).')');
            $s->execute($values);
            
            // Get primary key(s) back
            $pks = array();
            foreach($datamap as $field_name => $dfn)
                if(array_key_exists('autoinc', $dfn) && $dfn['autoinc'])
                    $pks[$field_name] = DBI::lastInsertId($table.'_'.$field_name.'_seq');
            
            return $pks;
        });
    }
    
    /**
     * Update object record
     * 
     * @param array $data database compliant data such as returned by toDBData
     * @param string $where where clause extension (optional)
     */
    public static function updateRecord($data, $where = null) {
        $event = new Event('entity_update', static::name(), $data, static::getPrimaryKeys(), $where);
        
        $table = static::getDBTable();
        
        $event->trigger(function($class, $data, $pks, $where) use($table) {
            $placeholders = array();
            $values = array();
            
            // Build update pairs
            foreach($data as $field_name => $value) {
                if(!in_array($field_name, $pks)) $placeholders[] = $field_name.' = :'.$field_name;
                $values[':'.$field_name] = $value;
            }
            
            // Build filter
            $where_parts = array();
            foreach($pks as $key_name)
                $where_parts[] = $key_name.' = :'.$key_name;
            
            if($where) $where_parts[] = $where;
            
            // Run the query
            $s = DBI::prepare('UPDATE '.$table.' SET '.implode(', ', $placeholders).(count($where_parts) ? ' WHERE ('.implode(') AND (', $where_parts).')' : ''));
            $s->execute($values);
        });
    }
    
    /**
     * Get plural name from single name
     * 
     * @param string $name single name
     * 
     * @return string
     */
    protected static function getPluralName($name) {
        if(preg_match('`[sho]$`', $name)) {
            $name .= 'es';
            
        } else if(preg_match('`[^aeiou]y$`', $name)) {
            $name = substr($name, 0, -1).'ies';
            
        } else {
            $name .= 's';
        }
        
        return $name;
    }
    
    /**
     * Get single name from plural name
     * 
     * @param string $name plural name
     * 
     * @return string
     */
    protected static function getSingleName($name) {
        if(preg_match('`[sho]es$`', $name)) {
            $single = substr($name, 0, -2);
            if(static::getPluralName($single) === $name)
                return $single;
        }
        
        if(substr($name, -3) == 'ies') {
            $single = substr($name, 0, -3).'y';
            if(static::getPluralName($single) === $name)
                return $single;
        }
            
        if(substr($name, -1) == 's')
            return substr($name, 0, -1);
        
        return $name;
    }
    
    /**
     * Get the class name
     * 
     * @param boolean $plural
     * 
     * @return string
     */
    public static function name($plural = false) {
        $name = get_called_class();
        if(!$plural) return $name;
        
        $pluralName = EntityCache::getMetadata($name, self::METADATA_PLURALNAME);
        
        if(!$pluralName) {
            $pluralName = static::getPluralName($name);
            EntityCache::setMetadata($name, self::METADATA_PLURALNAME, $pluralName);
        }
        
        return $pluralName;
    }
    
    /**
     * Allows to get the class name
     * 
     * @deprecated
     * 
     * @return string String: the class name
     */
    public static function getClassName(){
        return static::name();
    }
    
    /**
     * Get plural name
     * 
     * @deprecated
     * 
     * @return string
     */
    public static function getPluralClassName() {
        return static::name(true);
    }
    
    /**
     * Update database
     */
    public static function updateStructure() {
        $class = static::name();
        
        Logger::info('Updating '.$class.' database structure');
        
        $related = array_unique(array_merge(
            array_map(function($rel) {
                return (substr($rel, 0, 1) == '@') ? substr($rel, 1) : $rel;
            }, static::getHasOne()),
            static::getHasMany()
        ));
        
        foreach($related as $class) // Check relations
            static::getRelationWith($class);
        
        Database::ensureTableFormat(static::getDBTable(), static::getDataMap());
        
        static::updateRelationsStructure();
        
        Logger::info('Done for '.$class);
    }
    
    /**
     * Update the relations structure
     * 
     * @todo make it so it is not run twice (flag in Database class)
     */
    public static function updateRelationsStructure() {
        foreach(static::getHasMany() as $other) {
            if(!in_array(static::name(), $other::getHasMany())) continue;
            
            Database::ensureTableFormat(
                static::getRelationDBTable($other), 
                static::buildRelationDatamap($other)
            );
        }
    }
    
    /**
     * Build the relation datamap
     * 
     * @param string $other The other class name
     * @return array Datamap
     */
    private static function buildRelationDatamap($other){
        $map = array();
        
        $classes = array(static::name(), $other::name());
        sort($classes);
        
        $idx_prefix = strtolower(implode('_', $classes)).'_';
        $idx = 1;
        
        foreach($classes as $class) {
            foreach($class::getDataMap() as $f => $dfn) {
                if(!array_key_exists('primary', $dfn) || !$dfn['primary']) continue;
                
                foreach(array('autoinc', 'property', 'transform') as $p)
                    if(array_key_exists($p, $dfn)) unset($dfn[$p]);
                
                $dfn['index'] = $idx_prefix.sprintf('%02d', $idx);
                
                $map[strtolower($class).'_'.$f] = $dfn;
            }
            
            $idx++;
        }
        
        return $map;
    }
    
    /**
     * Allows to get primary keys
     * 
     * @return array Array of primary keys
     */
    public static function getPrimaryKeys(){
        $pks = EntityCache::getMetadata(static::name(), self::METADATA_PKS);
        
        if (is_null($pks)){
            $pks = array();
            foreach (static::getDataMap(true) as $f => $dfn){
                if (!array_key_exists('primary', $dfn) || !$dfn['primary']) continue;
                $pks[] = $f;
            }
            
            sort($pks);
            EntityCache::setMetadata(self::name(), static::METADATA_PKS, $pks);
        }
        return $pks;
    }
    
    /**
     * Serialize unique identifier
     * 
     * @param array $uid
     * 
     * @return string
     */
    protected static function serializeUID($uid) {
        if(!$uid) return null;
        
        if(count($uid) == 1)
            return reset($uid);
        
        $serialized = array();
        foreach($uid as $k => $v)
            $serialized[] = $k.'='.urlencode($v);
        
        return implode('&', $serialized);
    }
    
    /**
     * Get unique identifier
     *
     * @param boolean $as_scalar
     *
     * @return mixed
     *
     * @throws ModelViolationException
     */
    public function getUID($as_scalar = true) {
        if($as_scalar) {
            if(!array_key_exists('scalar_uid', $this->dataCache)) {
                $uid = $this->getUID(false);
                if(!$uid) return null;
                
                $this->dataCache['scalar_uid'] = static::serializeUID($uid);
            }
            
            return $this->dataCache['scalar_uid'];
        }
        
        if(!array_key_exists('uid', $this->dataCache)) {
                
            if(!$this->canGetUID())
                throw new ModelViolationException('Cannot build uid of unsaved entity that has autoinc properties');
            
            $this->dataCache['uid'] = static::buildUID($this, false);
        }
        
        return $this->dataCache['uid'];
    }
    
    /**
     * Allows to know if uid can be created
     *
     * @return bool
     */
    public function canGetUID(){
        if($this->storedInDatabase)
            return true;
    
        if(!array_key_exists('has_autoinc', $this->dataCache)) {
            $this->dataCache['has_autoinc'] = false;
            foreach(static::getDataMap(true) as $k => $dfn)
                if(array_key_exists('autoinc', $dfn) && $dfn['autoinc'])
                    $this->dataCache['has_autoinc'] = true;
        }
    
        return !$this->dataCache['has_autoinc'];
    }
    
    
    /**
     * Get one-to-many related identifier
     * 
     * @param string $class
     * @param boolean $as_scalar
     * 
     * @return mixed
     */
    public function getRelatedUID($class, $as_scalar = true) {
        if($as_scalar) {
            $key = 'related_'.$class.'_scalar_uid';
            
            if(!array_key_exists($key, $this->dataCache))
                $this->dataCache[$key] = static::serializeUID($this->getRelatedUID(false));
            
            return $this->dataCache[$key];
        }
        
        $key = 'related_'.$class.'_uid';
        if(!array_key_exists($key, $this->dataCache)) {
            $uid = array_key_exists($class, $this->relationKeys) ? $this->relationKeys[$class] : null;
            
            if($uid) ksort($uid);
            
            $this->dataCache[$key] = $uid;
        }
        
        return $this->dataCache[$key];
    }

    /**
     * Build UID
     *
     * @param array|object $data
     * @param boolean $as_scalar
     *
     * @return mixed
     *
     * @throws ModelViolationException
     */
    public static function buildUID($data, $as_scalar = true){
        $uid = array();
        $pks = static::getPrimaryKeys();
        
        foreach($pks as $pk){
            if(is_object($data) ){
                if(!property_exists($data, $pk)) 
                    throw new ModelViolationException('Property '.$pk.' does not exists in current data.');
                
                $uid[$pk] = $data->$pk;
                
            } else if(is_array ($data)){
                if(!array_key_exists($pk, $data))
                    throw new ModelViolationException('Property '.$pk.' does not exists in current data.');
                
                $uid[$pk] = $data[$pk];
                
            } else {
                throw new ModelViolationException('Object|array excepected.');
            }
        }
        
        if(!count($uid))
            throw new ModelViolationException('Trying to get UID of entity without any primary keys');
        
        ksort($uid);
        
        return $as_scalar ? static::serializeUID($uid) : $uid;
    }
    
    /**
     * String caster for logging purposes
     * 
     * @return string
     */
    public function __toString() {
        $name = static::name().'#';
        if ($this->canGetUID()){
            return $name.$this->getUID(true);
        }else{
            return $name.'unsaved';
        }
    }
    
    /**
     * Object comparison
     * 
     * @param object $other other object or id
     * 
     * @return bool
     */
    public function is($other) {
        if(!is_object($other) || !($other instanceof static))
            return false;
        
        return $this->getUID() === $other->getUID();
    }
    
    /**
     * Save in database
     */
    public function save() {
        (new Event('entity_save', $this))->trigger(function() {
            if(method_exists($this, 'customSave')) {
                // Child class has custom saver, run it
                $this->customSave();
                
            }else{
                // Normal save
                
                // Check references
                foreach(static::getHasOne() as $other) {
                    $refers = false;
                    if(substr($other, 0, 1) == '@') {
                        $refers = true;
                        $other = substr($other, 1);
                    }
                    
                    if($refers || in_array(static::name(), $other::getHasMany())) {
                        if(!array_key_exists($other, $this->relationKeys))
                            throw new ModelViolationException(static::name().' must refer to an instance of '.$other);
                    }
                }
                
                if($this->storedInDatabase) {
                    // Update record if object was loaded from existing one
                    $this->updateRecord($this->toDBData());
                    
                }else{
                    // Create it otherwise and get primary key back
                    $pks = $this->insertRecord($this->toDBData());
                    $datamap = static::getDataMap();
                    foreach ($pks as $pk => $value){
                        switch ($datamap[$pk]['type']){
                            case 'uint':
                            case 'int':
                                $value = (int) $value;
                                break;
                            
                            case 'float':
                                $value = (float) $value;
                                break;
                            
                            case 'date':
                            case 'datetime':
                                $value = (int) (new DateTime($value, new DateTimeZone('UTC')))->format('U');
                                break;
                            
                            case 'time':
                                $value = ((int)(new DateTime($value, new DateTimeZone('UTC')))->format('U') % (24 * 3600)); // Offset since 0h00
                                break;
                                
                        }
                        $this->$pk = $value;
                    }
                }
            }
        });
        
        // Invalidate collections if new instance
        if(!$this->storedInDatabase)
            EntityCache::dropCollection(static::name());
        
        // Stored whatever the way it was done
        $this->storedInDatabase = true;
        
        // Cache object
        EntityCache::setEntity($this);
    }
    
    /**
     * Delete from database
     */
    public function delete() {
        // If child class has things to do before instance is deleted run that
        if(method_exists($this, 'beforeDelete'))
            $this->beforeDelete();
        
        (new Event('entity_delete', $this))->trigger(function() {
            // Remove from database
            $where = array();
            $values = array();
            foreach (static::getPrimaryKeys() as $pk){
                $where[] = $pk.' = :'.$pk;
                $values[':'.$pk] = $this->$pk;
            }
            if (!count($where))
                throw new ModelViolationException('Instance selector build failed: '.  static::name());
                
            
            $s = DBI::prepare('DELETE FROM '.static::getDBTable().' WHERE '.  implode(' AND ', $where));
            $s->execute($values);
        });
        
        $this->storedInDatabase = false;
        
        // Remove from object cache
        EntityCache::dropEntity($this);
    }

    /**
     * Hydrant
     *
     * Fill out object properties from database data, converting types on the fly.
     *
     * @param mixed $data associative array or stdClass instance of data from the database
     *
     * @throws ModelViolationException
     */
    protected function fillFromDBData($data) {
        if(!is_array($data)) $data = (array)$data;
        
        $datamap = static::getDataMap();
        
        // Iterate over data
        foreach($data as $field_name => $value) {
            if(!array_key_exists($field_name, $datamap)) continue; // Ignore non-mapped data
            
            $dfn = $datamap[$field_name];
            
            // Basic types transformations/casting
            if(!is_null($value) || !array_key_exists('null', $dfn) || !$dfn['null']) {
                switch($dfn['type']) {
                    case 'int':
                    case 'uint':
                        $value = (int)$value;
                        break;
                    
                    case 'float':
                    case 'decimal':
                        $value = (float)$value;
                        break;
                    
                    case 'datetime':
                    case 'date':
                        if(!$value && array_key_exists('null', $dfn) && $dfn['null']) {
                            $value = null;
                        } elseif(!$value) {
                            $value = 0;
                        } else {
                            $value = (int) (new DateTime($value, new DateTimeZone('UTC')))->format('U');
                        }
                        break;
                    
                    case 'time':
                        $value = ((int)(new DateTime($value, new DateTimeZone('UTC')))->format('U') % (24 * 3600)); // Offset since 0h00
                        break;
                    
                    case 'bool':
                    case 'boolean':
                        $value = (bool)$value;
                        break;
                }
            }
            
            if(array_key_exists('relation_key', $dfn) && $dfn['relation_key']) {
                $this->relationKeys[$dfn['relation_key']['class']][$dfn['relation_key']['field']] = $value;
                continue;
            }
            
            // On-the-fly transforms
            if(array_key_exists('transform', $dfn)){
                switch($dfn['transform']) {
                    case 'json' :
                        if(($value === '') && array_key_exists('default', $dfn))
                            $value = $dfn['default'];
                        
                        $value = is_null($value) ? null : JSON::decode($value,true);
                        break;
                    
                    default:
                        $class = static::name();
                        $method = 'transform'.ucfirst($dfn['transform']);
                        if(!method_exists($this, $method))
                            throw new ModelViolationException($class.'->'.$method.' not found');
                        
                        $value = $this->$method(false, $value);
                }
            }
            
            $property = array_key_exists('property', $dfn) ? $dfn['property'] : $field_name;
            
            $this->$property = $value;
        }
    }

    /**
     * Turns object into a database compliant data set using dataMap converting types on the fly.
     *
     * @return array database ready data
     *
     * @throws ModelViolationException
     */
    public function toDBData() {
        $datamap = static::getDataMap();
        
        $data = array();
        
        // Iterate over keys
        foreach($datamap as $field_name => $dfn) {
            $property = array_key_exists('property', $dfn) ? $dfn['property'] : $field_name;
            
            if(array_key_exists('relation_key', $dfn) && $dfn['relation_key']) {
                $value = $this->relationKeys[$dfn['relation_key']['class']][$dfn['relation_key']['field']];
                
            } else {
                $value = $this->$property;
            }
            
            // Does the value need transformation ?
            if(array_key_exists('transform', $dfn)) switch($dfn['transform']) {
                case 'json' :
                    if(!is_null($value) || !array_key_exists('null', $dfn) || !$dfn['null'])
                        $value = JSON::encode($value);
                    break;
                
                default:
                    $class = static::name();
                    $method = 'transform'.ucfirst($dfn['transform']);
                    if(!method_exists($this, $method))
                        throw new ModelViolationException($class.'->'.$method.' not found');
                    
                    $value = $this->$method(true, $value);
            }
            
            if(is_null($value)) {
                if (array_key_exists('null', $dfn) && $dfn['null']){
                    $value = null;
                }else if (!array_key_exists('autoinc', $dfn) || !$dfn['autoinc']){
                    throw new ModelViolationException('property '.$property.' cannot be null');
                }
            } else switch($dfn['type']) { // Basic types transformations/casting
                case 'datetime':
                    $value = (new DateTime('@'.$value, new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
                    break;
                
                case 'date':
                    $value = (new DateTime('@'.$value, new DateTimeZone('UTC')))->format('Y-m-d');
                    break;
                
                case 'time':
                    $value = (new DateTime('@'.$value, new DateTimeZone('UTC')))->format('H:i:s');
                    break;
                
                case 'bool':
                case 'boolean':
                    $value = $value ? '1' : '0';
                    break;
            }
            
            $data[$field_name] = $value;
        }
        
        return $data;
    }

    /**
     * Returns relation state between current object and other class
     *
     * @param string $other class name
     * @uses $other::getHasOne()
     *
     * @return bool
     *
     * @throws ModelViolationException
     */
    public static function getRelationWith($other){
        if (is_object($other))
            $other = get_class($other);
        
        $relations = EntityCache::getMetadata(static::name(), self::METADATA_RELATIONS);
        
        if (is_null($relations)) $relations = array();
        
        if(!array_key_exists($other, $relations)) {
            $a_refers_to_b = in_array('@'.$other, static::getHasOne());
            $a_has_one_b = in_array($other, static::getHasOne()) || $a_refers_to_b;
            $a_has_many_b = in_array($other, static::getHasMany());
            
            $b_refers_to_a = in_array('@'.static::name(), $other::getHasOne());
            $b_has_one_a = in_array(static::name(), $other::getHasOne()) || $b_refers_to_a;
            $b_has_many_a = in_array(static::name(), $other::getHasMany());
            
            if($a_has_one_b && $a_has_many_b)
                throw new ModelViolationException(static::name().' has one and many '.$other.' at the same time');
            
            if($b_has_one_a && $b_has_many_a)
                throw new ModelViolationException($other.' has one and many '.static::name().' at the same time');
            
            $relation = self::RELATION_NONE;
            
            if($a_has_one_b) {
                if($b_has_one_a) {
                    if(!$a_refers_to_b && !$b_refers_to_a)
                        throw new ModelViolationException('neither '.static::name().' nor '.$other.' takes upon refering in the one-to-one relation');
                    
                    $relation = self::RELATION_ONE_TO_ONE;
                    
                } else if($b_has_many_a) {
                    $relation = self::RELATION_MANY_TO_ONE;
                    
                } else {
                    throw new ModelViolationException($other.' does not reciprocate relation with '.static::name());
                }
            }
            
            if($a_has_many_b) {
                if($b_has_one_a) {
                    $relation = self::RELATION_ONE_TO_MANY;
                    
                } else if($b_has_many_a) {
                    $relation = self::RELATION_MANY_TO_MANY;
                    
                } else {
                    throw new ModelViolationException($other.' does not reciprocate relation with '.static::name());
                }
            }
            
            $relations[$other] = $relation;
            
            EntityCache::setMetadata(static::name(), self::METADATA_RELATIONS, $relations);
        }
        
        return $relations[$other];
    }

    /**
     * Allows to return all related entities of given type
     *
     * @param string $other The other class
     *
     * @return array|Entity|null
     *
     * @throws ModelViolationException
     */
    public function getRelated($other) {
        $relation = static::getRelationWith($other);
        if(!$relation)
            throw new ModelViolationException(get_class($this).' does not have relation with '.$other);
        
        if(!$this->storedInDatabase)
            throw new ModelViolationException('Trying to get relation of unsaved '.static::name());
        
        $cache = EntityCache::getRelation($this, $other);
        if(!is_null($cache)) {
            if(in_array($relation, array(self::RELATION_ONE_TO_ONE, self::RELATION_MANY_TO_ONE)))
                $cache = array_shift($cache);
            
            return $cache;
        }
        
        if($relation == self::RELATION_ONE_TO_ONE) {
            if(in_array('@'.$other, static::getHasOne())) {
                if(!array_key_exists($other, $this->relationKeys))
                    return null;
                
                $related = $other::fromId($this->relationKeys[$other]);
                
                EntityCache::setRelation($this, $related);
                
                return $related;
                
            } else {
                $ph = array();
                $crt = array();
                foreach($other::getRelationColumn(static::name()) as $field => $rel_column) {
                    $crt[] = $rel_column.' = :'.$rel_column;
                    $ph[':'.$rel_column] = $this->$field;
                }
                
                $q = $other::query(implode(' AND ', $crt), $ph);
                
                if(!count($q['items']))
                    return null;
                
                EntityCache::setRelation($this, $q['items'], $q['id']);
                
                return array_shift($q['items']);
            }
            
        } else if($relation == self::RELATION_ONE_TO_MANY) {
            $ph = array();
            $crt = array();
            foreach($other::getRelationColumn(static::name()) as $field => $rel_column) {
                $crt[] = $rel_column.' = :'.$rel_column;
                $ph[':'.$rel_column] = $this->$field;
            }
            
            $q = $other::query(implode(' AND ', $crt), $ph);
            
            EntityCache::setRelation($this, $q['items'], $q['id']);
            
            return $q['items'];
            
        } else if($relation == self::RELATION_MANY_TO_ONE) {
            if(!array_key_exists($other, $this->relationKeys))
                return null;
            
            $related = $other::fromId($this->relationKeys[$other]);
            
            EntityCache::setRelation($this, $related, true);
            
            return $related;
            
        } else if($relation == self::RELATION_MANY_TO_MANY) {
            $rtbl = static::getRelationDBTable($other);
            $otbl = $other::getDBTable();
            
            // Build join
            $join = array();
            foreach($other::getPrimaryKeys() as $f)
                $join[] = $rtbl.'.'.strtolower($other).'_'.$f.' = '.$otbl.'.'.$f;
            
            // Get self pks
            $values = array();
            $pks = array();
            foreach(static::getPrimaryKeys() as $f) {
                $values[':'.$f] = $this->$f;
                $pks[] = $rtbl.'.'.strtolower(get_class($this)).'_'.$f.' = :'.$f;
            }
    
            // Get join name
            $joinName = array(static::name(), $other);
            sort($joinName);
            $joinName = implode('_', $joinName);
            
            // Init query to get all primary keys
            $query = 'SELECT '.$otbl.'.* FROM '.$rtbl.' INNER JOIN '.$otbl.' ON ('.implode(' AND ', $join).') WHERE '.implode(' AND ', $pks).' GROUP BY '.implode(', ', $other::getPrimaryKeys());
            
            $related = EntityCache::getCollection($joinName, array($query, $values));
            $cid = EntityCache::getCollectionKey(array($query, $values));
            
            if(is_null($related)) {
                $statement = DBI::prepare($query);
                $statement->execute($values);
                
                $related = array();
                foreach($statement->fetchAll() as $r)
                    $related[] = $other::fromData($r);
                
                EntityCache::setCollection($joinName, array($query, $values), $related);
            }
            
            EntityCache::setRelation($this, $related, $cid);
            
            return $related;
        }

        return null;
    }

    /**
     * Add a relation / relations
     *
     * @param mixed $other (array of) Entity
     *
     * @throws ModelViolationException
     */
    public function addRelated($other) {
        if(is_object($other))
            $other = array($other);
        
        if(!is_array($other))
            throw new ModelViolationException('Trying to add relation to non-Entity or collection of Entities');
        
        $entities = array();
        foreach($other as $o) {
            if(!is_object($o) || !($o instanceof Entity))
                throw new ModelViolationException('Trying to add relation to non-Entity');
            
            $entities[get_class($o)][] = $o;
        }
        
        if(!count($entities)) return;
        
        foreach($entities as $class => $set) {
            $relation = static::getRelationWith($class);
            
            if($relation == self::RELATION_ONE_TO_ONE) {
                if(count($set) > 1)
                    throw new ModelViolationException(static::name().' cannot have more than one '.$class.' because of one-to-one relation');
                
                $other = array_shift($set);
                
                if(in_array('@'.$class, static::getHasOne())) {
                    if(!$other->storedInDatabase)
                        throw new ModelViolationException('Cannot reference non-stored '.$class);
                    
                    $this->relationKeys[$class] = $other->getUID(false);
                    
                    if($this->storedInDatabase) {
                        $this->save();
                        
                        EntityCache::setRelation($this, $other);
                        EntityCache::setRelation($other, $this);
                    }
                    
                } else {
                    $other->addRelated($this);
                }
                
            } else if($relation == self::RELATION_ONE_TO_MANY) {
                foreach($set as $entity){
                    /** * @var Entity $entity */
                    $entity->addRelated($this);
                }
                    
                
            } else if($relation == self::RELATION_MANY_TO_ONE) {
                $other = array_shift($set);
                
                if(!$other->storedInDatabase)
                    throw new ModelViolationException('Cannot reference non-stored '.$class);
                
                EntityCache::dropRelation($other, static::name());
                
                $this->relationKeys[$class] = $other->getUID(false);
                
                if($this->storedInDatabase) {
                    $this->save();
                    
                    EntityCache::setRelation($this, $other);
                }
                
            } else if($relation == self::RELATION_MANY_TO_MANY) {
                if(!$this->storedInDatabase)
                    throw new ModelViolationException('Cannot reference non-stored '.static::name());
                
                // Relation table
                $values = array();
                
                $cols = array();
                $cv = array();
                foreach(static::getPrimaryKeys() as $f) {
                    $p = strtolower(get_class($this)).'_'.$f;
                    $cols[] = $p;
                    $cv[$p] = $this->$f;
                }

                $spks = $class::getPrimaryKeys();
                foreach($spks as $f)
                    $cols[] = strtolower($class).'_'.$f;
                
                $i = 0;
                foreach($set as $o) {
                    if(!$o->storedInDatabase)
                        throw new ModelViolationException('Cannot reference non-stored '.$class);
                    
                    $ov = $cv;
                    foreach($spks as $f){
                        $p = strtolower($class).'_'.$f;
                        $ov[$p] = $o->$f;
                    }
                    foreach($ov as $p => $v)
                        $values[':'.$p] = $v;
                    $i++;
                }
                
                $vals = '(:'.implode(', :', $cols).')';
                $vals = implode(', ', array_fill(0, count($set), $vals));
                $query = 'INSERT INTO '.static::getRelationDBTable($class).'('.implode(', ', $cols).') VALUES '.$vals;
                $statement = DBI::prepare($query);
                $statement->execute($values);
                
                EntityCache::dropRelation($this, $class);
                
                foreach($set as $o)
                    EntityCache::dropRelation($o, static::name());
            }
        }
    }

    /**
     * Remove a relation / relations
     *
     * @param mixed $other (array of) Entity and/or class name
     *
     * @throws ModelViolationException
     */
    public function dropRelated($other) {
        if(is_object($other))
            $other = array($other);
        
        if(!is_array($other))
            throw new ModelViolationException('Trying to add drop to non-Entity or collection of Entities');
        
        $entities = array();
        foreach((array)$other as $o) {
            if(is_object($o) && ($o instanceof Entity)) {
                $entities[get_class($o)][] = $o;
                
            } else if(is_string($o) && class_exists($o) && is_subclass_of($o, 'Entity')) {
                $entities[$o] = true;
                
            } else {
                throw new ModelViolationException('Trying to drop relation to non-Entity or Entity child class');
            }
        }
        
        foreach($entities as $class => $set) {
            $relation = static::getRelationWith($class);
            
            if($relation == self::RELATION_ONE_TO_ONE) {
                $other = is_array($set) ? array_shift($set) : $this->$class;
                if(!$other) continue;
                
                if(!$this->$class->is($other))
                    throw new ModelViolationException('Cannot drop non-related '.$class);
                
                if(in_array('@'.$class, static::getHasOne())) {
                    if(!array_key_exists($class, $this->relationKeys)) continue;
                    
                    unset($this->relationKeys[$class]);
                    
                    if($this->storedInDatabase)
                        $this->save();
                    
                    EntityCache::dropRelation($this, $class);
                    EntityCache::dropRelation($other, static::name());
                    
                } else {
                    $other->dropRelated($this);
                }
                
            } else if($relation == self::RELATION_ONE_TO_MANY) {
                foreach(is_array($set) ? $set : $this->getRelated($class) as $entity)
                    $entity->dropRelated($this);
                
            } else if($relation == self::RELATION_MANY_TO_ONE) {
                EntityCache::dropRelation($this->getRelated($class), $this);
                
                unset($this->relationKeys[$class]);
                
                // Cache
                EntityCache::dropRelation($this, $class);
                
            } else if($relation == self::RELATION_MANY_TO_MANY) {
                // Relation table
                $values = array();
                
                $cph = array();
                foreach (static::getPrimaryKeys() as $f){
                    $p = strtolower(get_class($this)).'_'.$f;
                    $cph[] = $p.' = :'.$p;
                    $values[':'.$p] = $this->$f;
                }
                
                $sph = array();
                $spks = $class::getPrimaryKeys();
                if(is_array($set)) {
                    $i = 0;
                    foreach($set as $o) {
                        $oph = array();
                        foreach($spks as $f){
                            $p = strtolower($class).'_'.$f;
                            $oph[] = $p.' = :'.$p.'___'.$i;
                            $values[':'.$p.'___'.$i] = $o->$f;
                        }
                        $i++;
                        $sph[] = implode(' AND ', $oph);
                    }
                }
                
                $query = 'DELETE FROM '.static::getRelationDBTable($class).
                        ' WHERE '.  implode(' AND ', $cph).(count($sph) ? ' AND (('.implode(' ) OR ( ', $sph).'))' : '');
                $statement = DBI::prepare($query);
                $statement->execute($values);
                
                // Cache
                EntityCache::dropRelation($this, is_array($set) ? $set : $class);
                
                if(is_array($set))
                    foreach($set as $entity)
                        EntityCache::dropRelation($entity, $this);
            }
        }
    }

    /**
     * Related set preloader
     *
     * @param array $others
     * @param boolean $group_by_other
     *
     * @return array
     *
     * @throws ModelViolationException
     */
    public static function preloadFrom($others, $group_by_other = false) {
        $class = static::name();
        
        // Group others by classes, check types
        $other_classes = array();
        foreach($others as $other) {
            if(!is_object($other) || !($other instanceof Entity))
                throw new ModelViolationException('Expecting Entity subclass');
            
            $other_classes[get_class($other)][] = $other;
        }
        
        // Mix of others ?
        if(count($other_classes) > 1) {
            $entities = array();
            foreach($other_classes as $other_class => $others)
                $entities[$other_class] = static::preloadFrom($others, $group_by_other);
            
            if($group_by_other) return $entities;
            
            $uniques = array();
            foreach($entities as $c => $set) {
                foreach($set as $entity) {
                    $uid = $entity->getUID();
                    if(is_null($uid))
                        throw new ModelViolationException('Trying to preload from unsaved '.$c);
                    
                    if(!array_key_exists($uid, $uniques))
                        $uniques[$uid] = $entity;
                }
            }
            
            return array_values($uniques);
        }
        
        $others = reset($other_classes);
        $other_class = key($other_classes);
        
        // Get and check relation state
        $relation = static::getRelationWith($other_class);
        if(!$relation)
            throw new ModelViolationException($class.' does not have relation with '.$other_class);
        
        $entities = array();
        
        // Get from cache if already loaded, gather uids for group requesting otherwise
        
        $missing = array();
        foreach($others as $other) {
            $uid = $other->getUID();
            if(is_null($uid))
                throw new ModelViolationException('Trying to preload from unsaved '.$other_class);
            
            $cached = EntityCache::getRelation($other, $class);
            
            if(is_null($cached)) { // Add to missing
                $missing[] = $other;
                
            } else { // Already in cache
                $entities[$uid] = (array)$cached;
                continue;
            }
        }
        
        if(count($missing)) {
            
            if($relation == self::RELATION_ONE_TO_MANY) {
                // other refers to self uid, collect self uid on others
                
                $placeholders = array();
                $where = array();
                $i = 0;
                
                foreach($missing as $other) {
                    $entities[$other->getUID()] = array(); // Init to no related
                    
                    $uid = $other->getRelatedUID($class, false);
                    $suid = static::buildUID($uid);
                    if(array_key_exists($suid, $where)) continue;
                    
                    $selector = array();
                    foreach($uid as $k => $v) {
                        $placeholders[':'.$k.'___'.$i] = $v;
                        $selector[] = $k.' = :'.$k.'___'.$i;
                    }
                    
                    $where[$suid] = '('.implode(' AND ', $selector).')';
                    $i++;
                }
                
                // Grouped get
                $q = static::query(implode(' OR ', $where), $placeholders);
                
                // Group by other
                foreach($q['items'] as $instance)
                    $entities[$instance->getRelatedUID($other_class)][] = $instance;
                
                // Cache
                foreach($missing as $other)
                    EntityCache::setRelation($other, $entities[$other->getUID()], $q['id']);
                
            } else if($relation == self::RELATION_MANY_TO_ONE) {
                // self refers to other uid, collect other uids
                
                $keys = array_filter(static::getDataMap(), function($dfn) use($other_class) {
                    return array_key_exists('relation_key', $dfn) && ($dfn['relation_key']['class'] == $other_class);
                });
                
                $key_equiv = array();
                foreach($keys as $field => $dfn)
                    $key_equiv[$dfn['relation_key']['field']] = $field;
                
                $placeholders = array();
                $where = array();
                $i = 0;
                
                foreach($missing as $other) {
                    $entities[$other->getUID()] = array(); // Init to no related
                    
                    $selector = array();
                    foreach($other->getUID(false) as $k => $v) {
                        $placeholders[':'.$k.'___'.$i] = $v;
                        $selector[] = $key_equiv[$k].' = :'.$k.'___'.$i;
                    }
                    
                    $where[] = '('.implode(' AND ', $selector).')';
                    $i++;
                }
                
                // Grouped get
                $q = static::query(implode(' OR ', $where), $placeholders);
                
                // Group by other
                foreach($q['items'] as $instance)
                    $entities[$instance->getRelatedUID($other_class)][] = $instance;
                
                // Cache
                foreach($missing as $other)
                    EntityCache::setRelation($other, $entities[$other->getUID()], $q['id']);
                
            } else if($relation == self::RELATION_MANY_TO_MANY) {
                // relation table, collect other uids and do a join
                
                $rtbl = static::getRelationDBTable($other_class);

                // Map relations out
                $placeholders = array();
                $where = array();
                $i = 0;
                
                foreach($missing as $other) {
                    $entities[$other->getUID()] = array(); // Init to no related
                    
                    $selector = array();
                    foreach($other->getUID(false) as $k => $v) {
                        $placeholders[':'.$k.'___'.$i] = $v;
                        $selector[] = strtolower($other_class).'_'.$k.' = :'.$k.'___'.$i;
                    }
                    
                    $where[] = '('.implode(' AND ', $selector).')';
                    $i++;
                }
                
                // Query to get all primary keys
                $statement = DBI::prepare('SELECT * FROM '.$rtbl.' WHERE '.implode(' OR ', $where));
                $statement->execute($placeholders);
                
                $instances_others_uids = array();
                $placeholders = array();
                $where = array();
                $i = 0;
                
                foreach($statement->fetchAll() as $r) {
                    $other_uid = array();
                    $instance_uid = array();
                    
                    foreach($other_class::getPrimaryKeys() as $k)
                        $other_uid[$k] = $r[strtolower($other_class).'_'.$k];
                    
                    foreach(static::getPrimaryKeys() as $k)
                        $instance_uid[$k] = $r[strtolower($class).'_'.$k];
                    
                    $suid = static::buildUID($instance_uid);
                    $instances_others_uids[$suid][] = $other_class::buildUID($other_uid);
                    
                    if(array_key_exists($suid, $where)) continue;

                    $selector = array();
                    foreach(static::getPrimaryKeys() as $k) {
                        $placeholders[':'.$k.'___'.$i] = $instance_uid[$k];
                        $selector[] = $k.' = :'.$k.'___'.$i;
                    }
                    
                    $where[$suid] = '('.implode(' AND ', $selector).')';
                    $i++;
                }
                
                // Grouped get
                $q = static::query(implode(' OR ', $where), $placeholders);
                
                // Group by other
                foreach($q['items'] as $instance)
                    foreach($instances_others_uids[$instance->getUID()] as $other_uid)
                        $entities[$other_uid][] = $instance;
                
                // Cache
                foreach($missing as $other)
                    EntityCache::setRelation($other, $entities[$other->getUID()], $q['id']);
            }
        }
        
        if($group_by_other) return $entities;
        
        $uniques = array();
        foreach($entities as $o => $set)
            foreach($set as $entity)
                if(!array_key_exists($entity->getUID(), $uniques))
                    $uniques[$entity->getUID()] = $entity;
        
        return array_values($uniques);
    }
    
    /**
     * Reverse get hasMany from plural
     * 
     * @param string $plural
     * 
     * @return mixed
     */
    protected static function getHasManyFromPlural($plural) {
        $name = static::name();
        
        if(!array_key_exists($name, self::$pluralHasMany)) {
            self::$pluralHasMany[$name] = array();
            
            foreach(static::getHasMany() as $other)
                self::$pluralHasMany[$name][self::getPluralName($other)] = $other;
        }
        
        if(!array_key_exists($plural, self::$pluralHasMany[$name]))
            return null;
        
        return self::$pluralHasMany[$name][$plural];
    }
    
    /**
     * Getter
     * 
     * @param string $property property to get
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if(preg_match('`^([A-Z][^_]*)(?:_(.+))?$`U', $property, $m)) { // Related
            $other = $m[1];
            $key = (count($m) > 2) ? $m[3] : null;
            
            if(in_array($other, static::getHasOne()) || in_array('@'.$other, static::getHasOne())) {
                if($key) { // Relation key value
                    if(!array_key_exists($other, $this->relationKeys))
                        return null;
                        
                    if(!array_key_exists($key, $this->relationKeys[$other]))
                        return null;
                        
                    return $this->relationKeys[$other][$key];
                        
                } else { // Related object
                    return $this->getRelated($other);
                }
                
            } else {
                $other = static::getHasManyFromPlural($other); // If non-null then it is in hasMany
                
                if($other) {
                    $related = $this->getRelated($other);
                    
                    if(!$key) return $related;
                    
                    return array_map(function($rel) use($key) {
                        return $rel->$key;
                    }, $related);
                }
            }
        }
        
        if($property == 'storedInDatabase') return $this->storedInDatabase;
        
        throw new PropertyAccessException($this, $property);
    }

    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws ModelViolationException
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if(preg_match('`^([A-Z][^_]*)$`U', $property, $m)) { // Related ?
            $other = $m[1];
            
            if(in_array($other, static::getHasOne()) || in_array('@'.$other, static::getHasOne())) {
                if(!is_object($value) || !($value instanceof $other))
                    throw new ModelViolationException('Expecting '.$other);
                
                $this->addRelated($value);
                return;
                
            } else {
                $other = static::getHasManyFromPlural($other); // If non-null then it is in hasMany
                
                if($other) {
                    if(!is_array($value))
                        throw new ModelViolationException('Expecting array of '.$other);
                    
                    foreach($value as $entry)
                        if(!is_object($entry) || !($entry instanceof $other))
                            throw new ModelViolationException('Expecting '.$other);
                    
                    $this->dropRelated($other);
                    
                    foreach($value as $entry)
                        $this->addRelated($entry);
                }
            }
        }
        
        throw new PropertyAccessException($this, $property);
    }
}

