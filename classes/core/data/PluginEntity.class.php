<?php

/**
 *     Moment - PluginEntity.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Base class for database stored plugin objects
 */
abstract class PluginEntity extends Entity {
    /**
     * Remove database
     */
    public static function removeStructure() {
        $class = static::name();
        
        Logger::info('Removing '.$class.' database structure');
        
        Database::removeTable(static::getDBTable());
        
        Logger::info('Done for '.$class);
    }
}
