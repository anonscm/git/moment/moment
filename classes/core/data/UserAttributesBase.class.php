<?php

/**
 *     Moment - UserAttributesBase.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * User attributes base
 *
 * @property array $emails
 * @property string $email
 * @property string $idp
 */
abstract class UserAttributesBase implements JsonSerializable {
    /**
     * User instance
     */
    protected $user = null;

    /**
     * Attribute data
     */
    protected $emails = array();
    protected $idp = null;
    protected $additional = array();

    /**
     * Constructor
     *
     * @param UserBase $user
     * @param mixed $data to create from
     *
     * @throws BadEmailException
     * @throws PropertyAccessException
     */
    public function __construct(UserBase $user, $data) {
        $this->user = $user;

        if(!is_array($data)) return;

        foreach($data as $k => $v)
            $this->__set($k, $v); // Run some checks
    }

    /**
     * Get all attributes as an array
     *
     * @return array
     */
    public function getAll() {
        $attributes = $this->additional;
        $attributes['idp'] = $this->idp;
        $attributes['emails'] = $this->emails;

        return $attributes;
    }

    /**
     * Validate user attributes
     */
    public function validate(){
        if (!count($this->emails))
            throw new UserAttributesException('emails', 'empty');

        foreach ($this->emails as $k => $email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                throw new UserAttributesException('emails['.$k.']', 'bad format');
        }
    }

    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array('emails', 'idp', 'additional')))
            return $this->$property;

        if($property == 'email') return count($this->emails) ? $this->emails[0] : null;

        if (array_key_exists($property, $this->additional))
            return $this->additional[$property];

        return null;
    }


    /**
     * Allows to check if is set
     *
     * @param string $property
     * @return bool
     */
    public function __isset($property){
        return $property === 'idp' || $property === 'emails' ||
        array_key_exists($property, $this->additional);
    }

    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws UserAttributesException
     */
    public function __set($property, $value)
    {
        if ($property === 'idp') {
            $this->idp = (string)$value;

        } else if ($property === 'emails') {
            if (!is_array($value))
                throw new UserAttributesException($property, 'not array');

            foreach ($value as $email) {
                if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                    throw new UserAttributesException($email, 'bad format');
            }

            $this->emails = $value;

        } else if ($property === 'additional'){
            $this->additional = (array) $value;

        } else {
            $this->additional[$property] = $value;
        }
    }


    /**
     * String caster for logging purposes
     *
     * @return string
     */
    public function __toString() {
        return $this->user.'>attributes';
    }

    /**
     * Serialize to json
     *
     * @return mixed
     */
    public function jsonSerialize() {
        return $this->getAll();
    }
}
