<?php

/**
 *     Moment - UserBase.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Represents an user in database
 *
 * @property string $id
 * @property string $lang
 * @property string $auth_secret
 * @property string $created
 * @property string $last_activity
 * @property UserAttributesBase $attributes
 * @property string $email
 * @property string $remote_config
 * @property string $identity
 */
abstract class UserBase extends Entity {
    /**
     * Database table
     */
    protected static $dataTable = 'User';
    
    /**
     * Database map
     */
    protected static $dataMap = array(
        'id' => array(
            'type' => 'string',
            'size' => 255,
            'primary' => true
        ),
        'attributes' => array(
            'type' => 'text',
            'transform' => 'attributes'
        ),
        'lang' => array(
            'type' => 'string',
            'size' => 8,
            'null' => true
        ),
        'created' => array(
            'type' => 'datetime'
        ),
        'last_activity' => array(
            'type' => 'datetime',
            'null' => true
        ),
        'auth_secret' => array(
            'type' => 'string',
            'size' => 64,
            'null' => true
        )
    );
    
    /**
     * Properties
     */
    protected $id = null;
    protected $attributes = null;
    protected $lang = null;
    protected $created = 0;
    protected $last_activity = null;
    protected $auth_secret = null;
    
    
    /**
     * Create a new user
     *
     * @param string $id user id, mandatory
     *
     * @return static
     */
    public static function create($id) {
        $user = new static(null);
    
        $user->id = $id;
        $user->created = time();
        $user->attributes = new UserAttributes($user, array());
        
        return $user;
    }
    
    
    /**
     * Create user if not exists
     *
     * @param string $id User ID
     *
     * @return UserBase
     */
    public static function createIfNotExists($id){
        $statement = DBI::prepare('SELECT * FROM '.self::getDBTable().' WHERE id = :id');
        $statement->execute(array(':id' => $id));
        $data = $statement->fetch();
        
        return $data ? static::fromData($data) : static::create($id);
    }
    
    
    /**
     * Record activity
     */
    public function recordActivity() {
        // No updates if active less than 1h ago : reduces db load ...
        if($this->last_activity > time() - 3600) return;
        
        (new Event('user_activity', $this))->trigger(function() {
            $this->last_activity = time();
            $this->save();
        });
    }
    
    /**
     * Updated since prefilter getter
     * 
     * @param string $uid placeholder id
     * 
     * @return string
     */
    public static function getUpdatedSincePrefilter($uid) {
        return 'last_activity >= '.$uid;
    }
    
    /**
     * Updated since check
     * 
     * @param int $date
     * 
     * @return boolean
     */
    public function wasUpdatedSince($date) {
        return $this->last_activity >= $date;
    }
    
    /**
     * Get active users
     * 
     * @return array of User
     */
    public static function getActive() {
        $days = Config::get('user_active_days');
        
        if(!$days || !is_int($days) || $days <= 0) {
            $days = Config::get('user_inactive_days');
            
            if(!$days || !is_int($days) || $days <= 0)
                $days = 30;
        }
        
        return User::all('last_activity >= :date', array(':date' => date('Y-m-d', time() - $days * 24 * 3600)));
    }
    
    /**
     * Remove inactive users preferences
     */
    public static function removeInactive() {
        $days = Config::get('user_inactive_days');
        if(!$days || !is_int($days) || $days <= 0)
            return;
        
        foreach(User::all('last_activity < :date', array(':date' => date('Y-m-d', time() - $days * 24 * 3600))) as $user)
            $user->delete();
    }
    
    
    /**
     * Allows to transform user attributes
     *
     * @param boolean $toDB True if transform to DB, false otherwise
     * @param UserAttributes|array $value
     *
     * @return string|UserAttributes
     */
    public function transformAttributes($toDB, $value){
        if ($toDB){
            return JSON::encode($value);
        }else{
            return new UserAttributes($this, (array) JSON::decode($value));
        }
    }
    
    /**
     * Handle remote auth secret
     *
     * @return null|string
     */
    private function remoteAuthSecret(){
        if ($this->auth_secret)
            return $this->auth_secret;
        
        // Generate user remote auth secret
        if(Config::get('auth_remote.user.autogenerate_secret')) {
            $this->auth_secret = hash('sha256', $this->id.'|'.time().'|'.Utilities::generateUID());
            $this->save();
            return $this->auth_secret;
        }
        
        return null;
    }
    
    /**
     * Getter
     * 
     * @param string $property property to get
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'id', 'lang', 'created', 'last_activity',
        ))) return $this->$property;
        
        if($property == 'attributes') return $this->attributes;
        
        if($property == 'email') return $this->attributes->email;
        
        if($property == 'remote_config') {
            $secret = $this->remoteAuthSecret();
            if (!$secret)
                return '';

            return Config::get('application_url') . '|' . $this->id . '|' . $secret;
        }
        
        if ($property === 'auth_secret'){
            return $this->remoteAuthSecret();
        }
        
        if($property == 'identity') return $this->email;
        
        if(isset($this->attributes->$property))
            return $this->attributes->$property;
        
        return parent::__get($property);
    }

    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws BadLangCodeException
     * @throws ModelViolationException
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if($property == 'lang') {
            if(!array_key_exists($value, Lang::getAvailableLanguages()))
                throw new BadLangCodeException($value);
            $this->lang = (string)$value;
            
        } else if(isset($this->attributes->$property)) {
            $this->attributes->$property = $value;
            
        } else {
            parent::__set($property, $value);
        }
    }
}
