<?php

/**
 *     Moment - DBUpdater.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Event handler (hook)
 */
class DBUpdater {
    /**
     * Find model objects in path
     * 
     * @param string $path
     * 
     * @return array of class names
     */
    private static function findModelObjects($path) {
        if(!is_dir($path)) return array();
        
        $classes = array();
        foreach(scandir($path) as $i) {
            if(substr($i, -10) != '.class.php') continue;
            
            $className= substr($i, 0, -10);
            
            // Continue if class not exists
            if (!class_exists($className)) continue;
            
            // Continue if current class is abstract
            $class = new ReflectionClass($className);
            if ($class->isAbstract()) continue;
            
            $classes[] = $className;
        }
        
        return $classes;
    }
    
    /**
     * Update all model objects at once
     */
    final public static function updateModel() {
        $done = array();
        
        foreach(self::findModelObjects(EKKO_ROOT.'/classes/model') as $class) {
            if(!method_exists($class, 'updateStructure') || !is_a($class, 'Entity', true))
                throw new DBUpdaterNotEntityException($class);
            
            call_user_func(array($class, 'updateStructure'));
            $done[] = $class;
        }
        
        foreach(self::findModelObjects(EKKO_ROOT.'/classes/core/model') as $class) {
            if(in_array($class, $done)) continue;
            
            if(!method_exists($class, 'updateStructure') || !is_a($class, 'Entity', true))
                throw new DBUpdaterNotEntityException($class);
            
            call_user_func(array($class, 'updateStructure'));
        }
    }

    /**
     * Update model objects of plugin
     *
     * @param string $name plugin name
     *
     * @throws DBUpdaterNotPluginEntityException
     * @throws ModelViolationException
     */
    final public static function updatePluginModel($name) {
        $path = PluginManager::getPluginPath($name);
        
        foreach(self::findModelObjects($path.'/model') as $class) {
            if(!preg_match('`^'.preg_quote($name, '`').'Plugin([A-Z][A-Za-z]+)$`', $class))
                throw new ModelViolationException('Entity '.$class.' from plugin '.$name.' has a bad name');
            
            if(!method_exists($class, 'updateStructure') || !is_a($class, 'PluginEntity', true))
                throw new DBUpdaterNotPluginEntityException($class);
            
            call_user_func(array($class, 'updateStructure'));
        }
    }
    
    /**
     * Remove model objects of plugin
     * 
     * @param string $name plugin name
     */
    final public static function removePluginModel($name) {
        $path = PluginManager::getPluginPath($name);
        
        foreach(self::findModelObjects($path.'/model') as $class)
            call_user_func(array($class, 'removeStructure'));
    }
}
