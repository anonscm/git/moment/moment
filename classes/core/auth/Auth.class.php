<?php

/**
 *     Moment - Auth.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Authentication class
 *
 * Handles user authentication logic.
 */
class Auth {

    /**
     * The current user cache.
     */
    private static $user = null;

    /**
     * Current user allowed state
     */
    private static $allowed = true;

    /**
     * Attributes cache.
     */
    private static $attributes = null;

    /**
     * Admin status of the current user.
     */
    private static $isAdmin = null;

    /**
     * Auth provider if any auth found
     */
    private static $type = null;

    /**
     * Return current user if it exists.
     *
     * @return User instance or false
     */
    public static function user() {
        if (is_null(self::$user)) { // Not already cached
            self::$user = false;

            // Authentication logic

            $event = new Event('auth_check');

            $auth = $event->trigger(function() {

                // Check for local authentificaiton (script)
                if (AuthLocal::isAuthenticated())
                    return array('local', AuthLocal::attributes());

                // Check for remote application/user
                if (
                    (Config::get('auth_remote.application.enabled') || Config::get('auth_remote.user.enabled')) && AuthRemote::isAuthenticated() && (
                        (AuthRemote::application() && Config::get('auth_remote.application.enabled')) ||
                        (!AuthRemote::application() && Config::get('auth_remote.user.enabled'))
                    )
                )
                    return array('remote', AuthRemote::attributes(), AuthRemote::application() && AuthRemote::isAdmin());

                // Check for SP autentification
                // No authentification is required by application
                if (!Config::get('auth_sp'))
                    return array();

                if (AuthSP::isAuthenticated())
                    return array('sp', AuthSP::attributes());

                return array();
            });

            self::$type = array_shift($auth);
            self::$attributes = array_shift($auth);

            if (count($auth))
                self::$isAdmin = array_shift($auth);

            if (self::$attributes && array_key_exists('uid', self::$attributes)) {
                // Get user corresponding to uid
                $user = User::createIfNotExists(self::$attributes['uid']);

                // Update other attributes if needed
                $update = false;
                foreach (self::$attributes as $k => $v) {
                    if ($k === 'uid') continue;
                    if ($v == $user->attributes->$k) continue;

                    $user->attributes->$k = $v;
                    $update = true;
                }

                if (self::isRemote()) {
                    try{
                        $user->attributes->validate();

                    }catch (UserAttributesException $e){
                        self::$allowed = false;
                        self::$type = null;
                        return null;
                    }
                }

                $user_filter = Config::get('auth_user_filter');
                if ($user_filter) {
                    self::$allowed = false;

                    if (is_string($user_filter)) {
                        if (preg_match('`^([^:]+):(.+)$`', $user_filter, $p)) {
                            $k = $p[1];
                            $t = '`' . str_replace('`', '\\`', $p[2]) . '`';
                            self::$allowed = !is_null($user->attributes->$k) && preg_match($t, $user->attributes->$k);
                        } else {
                            self::$allowed = !(bool) $user_filter;
                        }

                        if (!self::$allowed) {
                            self::$type = null;
                            return null;
                        }
                    }
                }

                if ($update)
                    $user->save();

                self::$user = $user;
    
                (new Event('user_authenticated'))->trigger();
            }
        }
        
        return self::$user;
    }

    /**
     * Tells if an user is connected.
     *
     * @retrun bool
     */
    public static function isAuthenticated() {
        if (!self::$allowed)
            throw new AuthUserNotAllowedException();

        return (bool) self::user();
    }

    /**
     * Tells if the current user is an admin.
     *
     * @retrun bool
     */
    public static function isAdmin() {
        if (is_null(self::$isAdmin)) {
            self::$isAdmin = false;

            if (self::user()) {
                $admin = Config::get('admin');
                if (!is_array($admin))
                    $admin = array_filter(array_map('trim', preg_split('`[,;\s]+`', (string) $admin)));

                self::$isAdmin = in_array(self::user()->id, $admin);
            }
        }

        return self::$isAdmin && !self::isGuest();
    }

    /**
     * Get auth type
     *
     * @return mixed
     */
    public static function type() {
        return self::$type;
    }

    /**
     * Tells if the user is given by an authenticated remote application.
     *
     * @return bool
     */
    public static function isRemoteApplication() {
        return self::isRemote() && AuthRemote::application();
    }

    /**
     * Tells if the user authenticated in a remote fashion.
     *
     * @return bool
     */
    public static function isRemoteUser() {
        return self::isRemote() && !AuthRemote::application();
    }

    /**
     * Tells if the user authenticated in a remote fashion or is from a remote application.
     *
     * @return bool
     */
    public static function isRemote() {
        return self::$type == 'remote';
    }

    /**
     * Tells if the user authenticated through a service provider.
     *
     * @return bool
     */
    public static function isSP() {
        return self::$type == 'sp';
    }

    /**
     * Tells if the user authenticated through a local service.
     *
     * @return bool
     */
    public static function isLocal() {
        return self::$type == 'local';
    }

    /**
     * Tells if the user authenticated as guest.
     *
     * @return bool
     */
    public static function isGuest() {
        return self::$type == 'guest';
    }
}
