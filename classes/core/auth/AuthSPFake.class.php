<?php

/**
 *     Moment - AuthSPFake.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Fake service provider authentication class
 *
 * Handles fake service provider authentication.
 */
class AuthSPFake implements AuthSPInterface {
    /**
     * Cache config
     */
    private static $config = null;

    /**
     * Cache authentication status
     */
    private static $isAuthenticated = null;

    /**
     * Cache attributes
     */
    private static $attributes = null;

    /**
     * Load data
     */
    private static function load() {
        if(!is_null(self::$config)) return;

        $config = Config::get('auth_sp');
        if(!$config || !array_key_exists('type', $config) || $config['type'] != 'fake') return;

        foreach(array('authenticated', 'attributes') as $p)
            if(!array_key_exists($p, $config))
                throw new ConfigMissingParameterException('auth_sp['.$p.']');

        if (!is_array($config['attributes']))
            throw new ConfigBadParameterException('auth_sp[attributes] not array');

        if (!array_key_exists('uid', $config['attributes']))
            throw new ConfigBadParameterException('auth_sp[attributes][uid] not present');

        if (!array_key_exists('emails', $config['attributes']))
            throw new ConfigBadParameterException('auth_sp[attributes][emails] not present');

        self::$config = $config;
    }

    /**
     * Authentication check.
     *
     * @return bool
     */
    public static function isAuthenticated() {
        self::load();

        if(is_null(self::$isAuthenticated))
            self::$isAuthenticated = self::$config['authenticated'];

        return self::$isAuthenticated;
    }

    /**
     * Retreive user attributes.
     *
     * @retrun array
     */
    public static function attributes() {
        self::load();

        if(is_null(self::$attributes)) {
            if(!self::isAuthenticated()) throw new AuthSPAuthenticationNotFoundException();

            // Wanted attributes
            $attributes = self::$config['attributes'];

            // Check attributes
            if(!$attributes['uid']) throw new AuthSPMissingAttributeException('uid');

            if(!$attributes['emails']) throw new AuthSPMissingAttributeException('emails');

            if(!is_array($attributes['emails'])) $attributes['emails'] = array($attributes['emails']);

            foreach($attributes['emails'] as $email) {
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)) throw new AuthSPBadAttributeException('emails');
            }

            $attributes['idp'] = 'fake';

            self::$attributes = $attributes;
        }

        return self::$attributes;
    }

    /**
     * Generate the logon URL.
     *
     * @param $target
     *
     * @return string
     */
    public static function logonURL($target = null) {
        if(!$target) {
            $landing_page = Config::get('landing_page');
            $target = Config::get('application_url').$landing_page;
        }

        return '#logon-'.urlencode($target);
    }

    /**
     * Generate the logoff URL.
     *
     * @param $target
     *
     * @return string
     */
    public static function logoffURL($target = null) {
        if(!$target)
            $target = Config::get('application_logouturl');

        return '#logoff-'.urlencode($target);
    }
}
