<?php

/**
 *     Moment - AuthRemoteRequest.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Remote authenticated request
 */
class AuthRemoteRequest {

    /**
     * @var string|null Holds the body
     */
    private static $body = null;

    /**
     * @var string|null Holds the application definition
     */
    private static $application = null;

    /**
     * Get the body
     * 
     * @return string The body
     */
    public static function body() {
        if (is_null(self::$body))
            self::$body = file_get_contents('php://input');

        return self::$body;
    }

    /**
     * Get/set the application
     * 
     * @param string $app The application
     * 
     * @return string The application
     */
    public static function application($app = null) {
        if ($app)
            self::$application = $app;

        return self::$application;
    }

}
