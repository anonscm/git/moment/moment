<?php

/**
 *     Moment - AuthSPShibboleth.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Shibboleth service provider authentication class
 *
 * Handles Shibboleth service provider authentication.
 */
class AuthSPShibboleth implements AuthSPInterface {
    /**
     * Cache config
     */
    private static $config = null;

    /**
     * Cache authentication status
     */
    private static $isAuthenticated = null;

    /**
     * Cache attributes
     */
    private static $attributes = null;

    /**
     * Authentication check.
     *
     * @return bool
     */
    public static function isAuthenticated() {
        if(is_null(self::$isAuthenticated)) self::$isAuthenticated = (bool) self::getIDP();

        return self::$isAuthenticated;
    }


    /**
     * Return IDP
     */
    private static function getIDP(){
        self::load();

        $idpAttribute = array_key_exists('idp', self::$config['attributes']) ?
            self::$config['attributes']['idp'] :
            'Shib-Identity-Provider';

        return getenv($idpAttribute);
    }

    /**
     * Retreive user attributes.
     *
     * @retrun array
     */
    public static function attributes() {
        if(is_null(self::$attributes)) {
            if(!self::isAuthenticated()) throw new AuthSPAuthenticationNotFoundException();

            $attributes = array('idp' => self::getIDP());

            // Wanted attributes
            foreach (self::$config['attributes'] as $attribute => $src) {
                if ($attribute === 'idp') continue;

                $value = null;
                if (!is_array($src)) $src = (array) $src;

                while (!$value && count($src)){
                    $value = getenv(array_shift($src));
                }
                if (!$value)
                    throw new AuthSPMissingAttributeException($attribute);

                if ($attribute === 'emails')
                    $value = array_filter(array_map('trim', explode(';', $value)));

                $attributes[$attribute] = $value;
            }

            // Proccess received attributes

            foreach($attributes['emails'] as $email) {
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)) throw new AuthSPBadAttributeException('emails');
            }

            self::$attributes = $attributes;
        }

        return self::$attributes;
    }

    /**
     * Generate the logon URL.
     *
     * @param $target
     *
     * @return string
     */
    public static function logonURL($target = null) {
        self::load();

        if(!$target) {
            $landing_page = Config::get('landing_page');
            $target = Config::get('application_url').$landing_page;
        }

        return str_replace('{target}', urlencode($target), self::$config['login_url']);
    }

    /**
     * Generate the logoff URL.
     *
     * @param $target
     *
     * @return string
     */
    public static function logoffURL($target = null) {
        self::load();

        if(!$target)
            $target = Config::get('application_logouturl');

        return str_replace('{target}', urlencode($target), self::$config['logout_url']);
    }

    /**
     * Load config
     */
    private static function load() {
        if(is_null(self::$config)) {
            self::$config = Config::get('auth_sp');

            foreach(array(
                        'attributes',
                        'login_url',
                        'logout_url'
                    ) as $key) if(!array_key_exists($key, self::$config))
                throw new ConfigMissingParameterException('auth_sp['.$key.']');

            if (!is_array(self::$config['attributes']))
                throw new ConfigBadParameterException('auth_sp[attributes] : not array');

            foreach (array('uid', 'emails') as $item) {
                if (!array_key_exists($item, self::$config['attributes']))
                    throw new ConfigBadParameterException('auth_sp[attributes]['.$item.'] : not present');
            }

        }
    }
}
