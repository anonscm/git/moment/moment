<?php

/**
 *     Moment - AuthLocal.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Guest authentication class
 * 
 * Handles guest authentication.
 */
class AuthLocal implements AuthInterface {
    /**
     * Cache attributes
     */
    private static $attributes = null;
    
    /**
     * Authentication check.
     * 
     * @return bool
     */
    public static function isAuthenticated() {
        return !is_null(self::$attributes);
    }
    
    /**
     * Retreive user attributes.
     * 
     * @retrun array
     */
    public static function attributes() {
        if(is_null(self::$attributes))
            throw new AuthAuthenticationNotFoundException();
        
        return self::$attributes;
    }
    
    /**
     * Set local user
     * 
     * @param string $user_id user id
     * @param string $email user email
     * @param string $name user name
     * 
     */
    public static function setUser($user_id, $email = null, $name = null) {
        if(is_null($user_id)) { // Virtually closes the local session
            self::$attributes = null;
            
        } else {
            self::$attributes = array(
                'uid' => $user_id,
                'email' => $email,
                'name' => $name
            );
        }
    }
}
