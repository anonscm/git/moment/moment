<?php

/**
 *     Moment - AuthSP.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Service provider authentication class
 * 
 * Handles service provider authentication delegation.
 */
class AuthSP implements AuthSPInterface {
    /**
     * Cache if delegation class was loaded.
     */
    private static $delegationClassName = false;
    
    /**
     * Delegates authentication check.
     * 
     * @return bool
     */
    public static function isAuthenticated() {
        $class = self::loadDelegationClass();
        
        if (!$class) return false;
        
        return call_user_func($class.'::isAuthenticated');
    }
    
    /**
     * Retreive user attributes from delegated class.
     * 
     * @retrun array
     */
    public static function attributes() {
        $class = self::loadDelegationClass();
        
        if (!$class) return array();
        
        $attributes = call_user_func($class.'::attributes');
        
        if(
            isset($_SESSION) && 
            array_key_exists('impersonated_user_id', $_SESSION)
            && $_SESSION['impersonated_user_id']
        ) {
            $attributes['uid'] = $_SESSION['impersonated_user_id'];
            $attributes['name'] = (array_key_exists('name', $attributes) ? $attributes['name'].' ' : '').'(impersonating)';
        }
        
        return $attributes;
    }

    /**
     * Impersonate user
     *
     * @param string $uid
     *
     * @throws AuthUserNotAllowedException
     */
    public static function impersonate($uid) {
        if($uid && !Auth::isAdmin())
            throw new AuthUserNotAllowedException();
        
        if($uid && array_key_exists('impersonated_user_id', $_SESSION) && $_SESSION['impersonated_user_id'])
            throw new AuthUserNotAllowedException();
        
        if($uid) {
            $_SESSION['impersonated_user_id'] = $uid;
            
        } else if(array_key_exists('impersonated_user_id', $_SESSION)) {
            unset($_SESSION['impersonated_user_id']);
        }
    }
    
    /**
     * Tells if impersonating is in progress
     * 
     * @return boolean
     */
    public static function isImpersonating() {
        return array_key_exists('impersonated_user_id', $_SESSION);
    }
    
    /**
     * Get the logon URL from delegated class.
     * 
     * @param string $target
     * @param bool $full_url
     * 
     * @return string
     */
    public static function logonURL($target = null, $full_url = false) {
        return Utilities::ensureFullURL((new Event('auth_sp_logon_url', $target))->trigger(function($target) {
            $class = self::loadDelegationClass();
            
            if (!$class) return '';
            
            return call_user_func($class.'::logonURL', $target);
        }), $full_url);
    }
    
    /**
     * Get the logoff URL from delegated class.
     * 
     * @param string $target
     * @param bool $full_url
     * 
     * @return string
     */
    public static function logoffURL($target = null, $full_url = false) {
        return Utilities::ensureFullURL((new Event('auth_sp_logoff_url', $target))->trigger(function($target) {
            $class = self::loadDelegationClass();
            
            if (!$class) return '';
            
            return call_user_func($class.'::logoffURL', $target);
        }), $full_url);
    }
    
    /**
     * Trigger authentication
     * 
     * @param string $target
     *
     * @return mixed
     */
    public static function trigger($target = null) {
        $url = self::logonURL($target, true);
        
        return (new Event('auth_sp_trigger'))->trigger(function() use($url) {
            
            if(!$url) return;
            
            header('Location: '.$url);
            exit;
        });
    }
    
    /**
     * Load selected service provider delegation class and return its class name.
     * 
     * @return string delegation class name
     *
     * @throws ConfigBadParameterException
     * @throws AuthSPMissingDelegationClassException
     */
    private static function loadDelegationClass() {
        if(is_null(self::$delegationClassName)) return self::$delegationClassName;
        
        self::$delegationClassName = false;
        
        $cfg = Config::get('auth_sp');
        
        if(!$cfg)
            return null;
        
        if(!array_key_exists('type', $cfg) || !$cfg['type'])
            throw new ConfigBadParameterException('auth_sp[type]');
        
        $class = 'AuthSP'.ucfirst($cfg['type']);
        
        $file = EKKO_ROOT.'/classes/auth/'.$class.'.class.php';
        if(!file_exists($file)) {
            $file = EKKO_ROOT.'/classes/core/auth/'.$class.'.class.php';
            if(!file_exists($file))
                throw new AuthSPMissingDelegationClassException($class);
        }
        
        require_once $file;
        
        self::$delegationClassName = $class;
        
        return $class;
    }
}
