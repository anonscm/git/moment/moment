<?php

/**
 *     Moment - AuthRemote.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Remote application/user authentication class
 */
class AuthRemote implements AuthInterface {
    /**
     * Cache authentication status
     */
    private static $isAuthenticated = null;
    
    /**
     * Cache admin state
     */
    private static $isAdmin = false;
    
    /**
     * Cache attributes
     */
    private static $attributes = null;
    
    /**
     * Application name
     */
    private static $application = null;

    /**
     * Authentication check.
     *
     * @return bool
     *
     * @throws AuthRemoteSignatureCheckFailedException
     * @throws AuthRemoteTooLateException
     * @throws AuthRemoteUknownApplicationException
     * @throws AuthRemoteUserRejectedException
     */
    public static function isAuthenticated() {
        if(is_null(self::$isAuthenticated)) {
            self::$isAuthenticated = false;
            
            // Do we have remote authentication data in the request ?
            if(!array_key_exists('signature', $_GET)) return false;
            if(!array_key_exists('timestamp', $_GET)) return false;
            
            $application = array_key_exists('remote_application', $_GET) ? $_GET['remote_application'] : null;
            
            $remote_user = array_key_exists('remote_user', $_GET) ? $_GET['remote_user'] : null;
            if(!is_array($remote_user)) $remote_user = array('uid' => $remote_user);
            
            $uid = array_key_exists('uid', $remote_user) ? $remote_user['uid'] : null;
            
            if(!$application && !$uid) return false;
            
            // Get data
            $received_signature = $_GET['signature'];
            $timestamp = (int)$_GET['timestamp'];
            
            if($application) {
                // Check that application is known
                $applications = Config::get('auth_remote.application.applications');
                
                if(!is_array($applications) || !array_key_exists($application, $applications))
                    throw new AuthRemoteUknownApplicationException($application);
                
                $application = new RemoteApplication($application, $applications[$application]);
            }
            
            // Check request time to avoid replays
            $late = time() - $timestamp - Config::get('auth_remote.timeout');
            if($late > 0)
                throw new AuthRemoteTooLateException($late);
            
            // Get method from headers
            $method = null;
            foreach(array('X_HTTP_METHOD_OVERRIDE', 'REQUEST_METHOD') as $k) {
                if(!array_key_exists($k, $_SERVER)) continue;
                $method = strtolower($_SERVER[$k]);
            }
            
            // Build signed data
            $signed = $method.'&'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'].(array_key_exists('PATH_INFO', $_SERVER) ? $_SERVER['PATH_INFO'] : '');
            
            $args = $_GET;
            unset($args['signature']);
            if(count($args)) $signed .= '?'.implode('&', RestUtilities::flatten($args));
            
            $input = HttpRequest::body();
            if($input) $signed .= '&'.$input;
            
            // Check signature
            if($application) {
                $secret = $application->secret;
                
            } else {
                // Get user, fail if unknown or no user secret
                $user = User::fromId($uid);
                if(!$user->storedInDatabase)
                    throw new AuthRemoteUserRejectedException($uid, 'user not found');
                
                if(!$user->auth_secret)
                    throw new AuthRemoteUserRejectedException($user->id, 'no secret set');
                
                $secret = $user->auth_secret;
            }
            $algorithm = Config::get('auth_remote.signature_algorithm');
            if(!$algorithm) $algorithm = 'sha1';
            $signature = hash_hmac($algorithm, $signed, $secret);
            if($received_signature !== $signature)
                throw new AuthRemoteSignatureCheckFailedException($signed, $secret, $received_signature, $signature);
            
            self::$attributes = array();
            
            // Register admin level if asked for and enabled
            if($application) {
                if($uid) {
                    $user = User::fromId($uid);
                    
                    if(!$user->storedInDatabase && !$application->canCreateUser)
                        throw new AuthRemoteUserRejectedException($uid, 'user not found and application not allowed to create');
                    
                    self::$attributes = $remote_user; // Application can virtually create the user
                }
                
                self::$isAdmin = $application->isAdmin;
                
                self::$application = $application;
                
            } else if($uid) { // Remote user
                self::$attributes['uid'] = $uid;
            }
            
            self::$isAuthenticated = true;
        }
        
        return self::$isAuthenticated;
    }

    /**
     * Retreive user attributes.
     *
     * @return array
     *
     * @throws AuthAuthenticationNotFoundException
     * @throws AuthRemoteSignatureCheckFailedException
     * @throws AuthRemoteTooLateException
     * @throws AuthRemoteUknownApplicationException
     * @throws AuthRemoteUserRejectedException
     */
    public static function attributes() {
        if(!self::isAuthenticated()) throw new AuthAuthenticationNotFoundException();
        
        return self::$attributes;
    }
    
    /**
     * Get admin state
     * 
     * @return bool
     */
    public static function isAdmin() {
        return self::$isAdmin;
    }
    
    /**
     * Get application
     * 
     * @return RemoteApplication
     */
    public static function application() {
        return self::$application;
    }
}
