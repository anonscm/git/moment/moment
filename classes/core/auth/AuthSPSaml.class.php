<?php

/**
 *     Moment - AuthSPSaml.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * SAML service provider authentication class
 *
 * Handles SAML service provider authentication.
 */
class AuthSPSaml implements AuthSPInterface {
    /**
     * Cache config
     */
    private static $config = null;

    /**
     * Cache SimpleSamlPHP_Auth_Simple instance
     */
    private static $simplesamlphp_auth_simple = null;

    /**
     * Cache authentication status
     */
    private static $isAuthenticated = null;

    /**
     * Cache attributes
     */
    private static $attributes = null;

    /**
     * Authentication check.
     *
     * @return bool
     */
    public static function isAuthenticated() {
        if(is_null(self::$isAuthenticated)) self::$isAuthenticated = self::loadSimpleSAML()->isAuthenticated();

        return self::$isAuthenticated;
    }

    /**
     * Retrieve user attributes.
     *
     * @retrun array
     */
    public static function attributes() {
        if(is_null(self::$attributes)) {
            if(!self::isAuthenticated()) throw new AuthSPAuthenticationNotFoundException();

            $raw_attributes = self::loadSimpleSAML()->getAttributes();

            $attributes = array();


            // Wanted attributes
            foreach (self::$config['attributes'] as $attribute => $src) {
                if ($attribute === 'idp') continue;

                $value = null;
                if (!is_array($src)) $src = (array) $src;

                while (!$value && count($src)){
                    $attr = array_shift($src);
                    if (array_key_exists($attr, $raw_attributes))
                        $value = $raw_attributes[$attr];
                }

                if (!$value)
                    throw new AuthSPMissingAttributeException($attribute);

                if ($attribute === 'emails')
                    $value = array_filter(array_map('trim', explode(';', $value)));

                $attributes[$attribute] = $value;
            }

            foreach($attributes['emails'] as $email) {
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)) throw new AuthSPBadAttributeException('emails');
            }

            self::$attributes = $attributes;
        }

        return self::$attributes;
    }

    /**
     * Generate the logon URL.
     *
     * @param $target
     *
     * @return string
     */
    public static function logonURL($target = null) {
        self::loadSimpleSAML();

        if(!$target) {
            $landing_page = Config::get('landing_page');
            $target = Config::get('application_url').$landing_page;
        }

        $url = self::$config['simplesamlphp_url'].'module.php/core/as_login.php?';
        $url .= 'AuthId='.self::$config['authentication_source'];
        $url .= '&ReturnTo='.urlencode($target);

        return $url;
    }

    /**
     * Generate the logoff URL.
     *
     * @param $target
     *
     * @return string
     */
    public static function logoffURL($target = null) {
        self::loadSimpleSAML();

        if(!$target)
            $target = Config::get('application_logouturl');

        $url = self::$config['simplesamlphp_url'].'module.php/core/as_logout.php?';
        $url .= 'AuthId='.self::$config['authentication_source'];
        $url .= '&ReturnTo='.urlencode($target);

        return $url;
    }

    /**
     * Load SimpleSAML class
     */
    private static function loadSimpleSAML() {
        if(is_null(self::$config)) {
            self::$config = Config::get('auth_sp');

            foreach(array(
                        'simplesamlphp_location',
                        'simplesamlphp_url',
                        'authentication_source',
                        'attributes'
                    ) as $key) if(!array_key_exists($key, self::$config))
                throw new ConfigMissingParameterException('auth_sp['.$key.']');
        }

        if (!is_array(self::$config['attributes']))
            throw new ConfigBadParameterException('auth_sp[attributes] : not array');

        foreach (array('uid', 'emails') as $item) {
            if (!array_key_exists($item, self::$config['attributes']))
                throw new ConfigBadParameterException('auth_sp[attributes]['.$item.'] : not present');
        }

        if(is_null(self::$simplesamlphp_auth_simple)) {
            require_once(self::$config['simplesamlphp_location'] . 'lib/_autoload.php');

            self::$simplesamlphp_auth_simple = new SimpleSAML_Auth_Simple(self::$config['authentication_source']);
        }

        return self::$simplesamlphp_auth_simple;
    }
}
