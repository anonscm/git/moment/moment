<?php

/**
 *     Moment - autoload.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Autoloading helper
 */
class Autoloader {
    /**
     * Discovered application classes
     */
    private static $application_mappers = array(
        '*Exception' => 'exceptions/@package(Exception)',
        '*Endpoint' => 'endpoints/',
        '*Form' => 'forms/',
        'Auth*' => 'auth/',
    );
    
    /**
     * Class name to path mappers for core classes
     */
    private static $core_mappers = array(
        'PropertyAccessException'  => 'core/exceptions/CommonExceptions',
        'NotFoundException'        => 'core/exceptions/CommonExceptions',
        'MissingArgumentException' => 'core/exceptions/CommonExceptions',
        
        '*Exception'               => 'core/exceptions/@package(Exception)',

        // CONSTANTS
        '*Types'                   => 'core/constants/',
        '*Options'                 => 'core/constants/',
        '*Constants'               => 'core/constants/',
        '*Statuses'                => 'core/constants/',
        'RestMethods'              => 'core/constants/',
        'LogLevels'                => 'core/constants/',
        
        'Entity'                   => 'core/data/',
        'PluginEntity'             => 'core/data/',
        'DBUpdater'                => 'core/data/',
        'UserBase'                 => 'core/data/',
        'UserAttributes'           => 'core/data/',
        'UserAttributesBase'       => 'core/data/',
        
        'User'                     => 'core/model/',
        'TranslatableEmail'        => 'core/model/',
        
        'RestEndpoint'             => 'core/rest/',
        'RestResponse'             => 'core/rest/',
        'RestResponseFields'       => 'core/rest/',
        'RestResponse*'            => 'core/rest/responses/',
        'Rest*'                    => 'core/rest/',
        'UserEndpointBase'         => 'core/rest/',
        
        '*Endpoint'                => 'core/endpoints/',
        
        'Auth*'                    => 'core/auth/',
        
        'Logger'                   => 'core/utils/',
        '*Logger'                  => 'core/utils/loggers/',
        
        'Form*'                    => 'core/utils/form/',
        
        '*'                        => 'core/utils/'
    );
    
    /**
     * Plugin class matcher
     */
    const PLUGIN_CLASS_MATCHER = '`^([A-Z][A-Za-z]+)Plugin((?:[A-Z][A-Za-z]+)?)((?:Endpoint)?)$`U';

    /**
     * Load a class
     *
     * @param $class
     *
     * @throws EventHandlerIsNotCallableException
     */
    public static function load($class) {
        if (file_exists(EKKO_ROOT.'/classes/autoload.php'))
            include_once EKKO_ROOT.'/classes/autoload.php';
        
        if(self::pluginClassLookup($class)) return;
        
        if(self::classLookup($class, array_reverse(self::$application_mappers))) return;
        
        if(self::classLookup($class, self::$core_mappers)) return;
        
        (new Event('autoload_class_not_found', $class))->trigger();
        
        Logger::debug('Could not find class '.$class.', may (or may not) be a problem ...');
    }


    /**
     * Look for class
     *
     * @param string $class
     * @param array $mappers
     *
     * @return bool
     */
    private static function classLookup($class, array $mappers) {
        foreach($mappers as $matcher => $path) {
            $m = uniqid();
            $matcher = str_replace('*', $m, $matcher);
            $matcher = preg_quote($matcher);
            $matcher = str_replace($m, '.*', $matcher);
            $matcher = '`^'.$matcher.'$`';
            
            if(preg_match($matcher, $class)) {
                if(preg_match('`^(.*)@package\((.+)\)$`', $path, $m))
                    $path = self::package($m[1], $class, $m[2]);
                
                if (substr($path, 0,1) === '/')
                    $file = EKKO_ROOT.$path;
                else
                    $file = EKKO_ROOT.'/classes/'.$path;
                
                if(!$path || substr($path, -1) == '/') $file .= $class;
                
                if (substr($path, -10) !== '.class.php') 
                    $file .= '.class.php';
                
                if(!file_exists($file)) {
                    Logger::debug('Looking for class '.$class.', expecting it at '.$file.' but nothing found, may (or may not) be a problem ...');
                    return false;
                }
                
                require_once $file;
                
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Look for plugin class
     * 
     * @param string $class
     * 
     * @return bool
     */
    private static function pluginClassLookup($class) {
        if(!preg_match(self::PLUGIN_CLASS_MATCHER, $class, $m))
            return false;
        
        $path = PluginManager::getPath($m[1].'/');
        
        $file = $path.$class.'.class.php';
        
        // If "sub"-class and Endpoint suffix look it up in model then at root
        if($m[3] == 'Endpoint') {
            $endpoint_file = $path.'endpoints/'.$class.'.class.php';
            if(file_exists($endpoint_file)) $file = $endpoint_file;
        }
        
        // If "sub"-class look it up in model then at root
        if($m[2]) {
            $model_file = $path.'model/'.$class.'.class.php';
            if(file_exists($model_file)) $file = $model_file;
        }
        
        if(!file_exists($file)) return false;
        
        require_once $file;
        
        return true;
    }
    
    /**
     * Resolve tokenized package
     * 
     * @param $path string the search path
     * @param $class string the class name
     * @param $type string the default class
     * 
     * @return string the class path
     */
    private static function package($path, $class, $type) {
        if(substr($path, -1) != '/') $path .= '/';
        
        $tokens = array();
        $bits = preg_split('`([A-Z][a-z0-9]*)`', $class, null, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

        while($bit = array_shift($bits)) {
            if(preg_match('`^[A-Z]$`', $bit)) {
                while(count($bits) && preg_match('`^[A-Z]$`', $bits[0])) {
                    $bit .= array_shift($bits);
                }
            }
            
            $tokens[] = $bit;
        }
        
        array_pop($tokens); // Exception
        
        while(count($tokens) && !file_exists(EKKO_ROOT.'/classes/'.$path.implode('', $tokens).$type.'s.class.php')) {
            array_pop($tokens);
        }
        
        return $path.(count($tokens) ? implode('', $tokens) : '').$type.'s';
    }


    /**
     * Add class mapper
     *
     * @param string $matcher
     * @param string $path
     */
    public static function addMapper($matcher, $path){
        // IF === *, explore then get all
        if ($matcher === '*'){
            $absPath = EKKO_ROOT.'/classes/'.$path.'/';
            if (!is_dir($absPath)) return;
            
            foreach (scandir($absPath) as $item) {
                if (!is_file($absPath.$item)) continue;
                if (!preg_match('`^(.+)\.class\.php$`', $item, $m)) continue;
                self::$application_mappers[$m[1]] = $path.$item;
            }
            return;
        }
        
        // Standard matcher
        self::$application_mappers[$matcher] = $path;
    }
}

/**
 * Register autoload
 */
spl_autoload_register(function($class) {
    Autoloader::load($class);
});
