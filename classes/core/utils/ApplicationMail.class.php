<?php

/**
 *     Moment - ApplicationMail.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');


/**
 * Class ApplicationMail
 */
class ApplicationMail extends Mail {
    /**
     * Constructor
     *
     * @param mixed $translation_or_id Translation object or translation identifier
     * @param mixed $recipient email address, User, array/object with email (and optional name and lang) key/property
     * @param mixed $about optional entity the email is about
     * @param array $vars additionnal translation variables ($recipient if User and $about will be added)
     *
     * @throws BadEmailException
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws DetailedException
     * @throws EventHandlerIsNotCallableException
     */
    public function __construct($translation_or_id, $recipient = null, $about = null, $vars = array()) {
        // Manage recipient if object given, get language if possible
        $lang = null;
        $name = null;
        
        if(is_object($recipient) || is_array($recipient))
            $vars['recipient'] = $recipient;
        
        if (!is_null($recipient)) 
            $recipient = self::rawToAddress($recipient);
        
        $content = null;
        if(is_object($translation_or_id) && $translation_or_id instanceof Translation) {
            $content = $translation_or_id;
            
        } else if(is_string($translation_or_id)) {
            $content = Lang::translateEmail($translation_or_id, $recipient ? $recipient['lang'] : null);
            if($about || count($vars))
                $content = call_user_func_array(array($content, 'replace'), array_merge((array)$about, $vars));
            
        } else if($translation_or_id) {
            throw new BadFormatException($translation_or_id, 'translation_or_id');
        }
        
        $context = null;
        if($about && is_object($about) && !($about instanceof stdClass))
            $context = strtolower(get_class($about)).'-'.$about->getUID();
        
        // Cast content to string if translation object
        $subject = '';
        if($content) {
            $subject = $content->subject->out();
            if(is_array($subject)) {
                $subject = array_filter($subject);
                $subject = (isset($subject['prefix']) ? $subject['prefix'] : '').' '.array_pop($subject);
            }
        }
        
        // Trigger basic mail build
        parent::__construct(null, $subject);
        
        // Get sender from recipient data if needed
        $need_sender = false;
        foreach(array('from', 'reply_to', 'return_path', 'sender') as $p) {
            $p = Config::get('email.'.$p);
            if(!$p) continue;
            
            if(!is_array($p)) $p = array(array('email' => $p));
            
            if(array_key_exists('email', $p)) $p = array($p);
            
            foreach($p as $alt)
                $need_sender |= ($alt['email'] == 'sender');
        }
        
        $sender = null;
        if($need_sender) {
            if(($about) && is_object($about) && method_exists($about, 'getEmailSender'))
                $sender = $about->getEmailSender();
            
            if(!$sender) $sender = (new Event('mail_sender_lookup', $translation_or_id, $about, $vars))->trigger();
        }
        
        $this->addressHeader('from', $sender, 'From');
        
        $this->addressHeader('sender', $sender, 'Sender');
        
        $this->addressHeader('reply_to', $sender, 'Reply-To');
        
        $return_path = $this->addressHeader('return_path', $sender);
        if($return_path) {
            // If verp pattern insert context so that return path alone tells which object the bounce is related to
            if(preg_match('`^(.+)<verp>(.+)$`i', $return_path, $match))
                $return_path = $match[1].($context ? $context : 'no_context').$match[2];
            
            if(!filter_var($return_path, FILTER_VALIDATE_EMAIL))
                throw new BadEmailException($return_path);
            
            $this->return_path = $return_path;
        }
        
        // Set context in headers so that it is returned along with bounces
        if($context) {
            $this->msg_id = '<'.$context.'-'.uniqid().'>';
            $this->addHeader('X-Application-Context', $context);
        }
        
        // Write content if a translation object was given
        if($content) {
            $this->writePlain($content->plain);
            $this->writeHTML($content->html);
        }
    }

    /**
     * Quick translated sending
     *
     * @see __construct
     *
     * @param string|Translation $translation_or_id
     * @param string|User|array|object $recipient
     * @param Entity $about
     * @param array $vars
     */
    public static function quickSend($translation_or_id, $recipient, $about = null, $vars = array()) {
        // Create email and send it right away
        (new self($translation_or_id, $recipient, $about, $vars))->send();
    }
    
    /**
     * Flatten replacements
     * 
     * @param array $data
     * @param string $prefix
     * 
     * @return array
     */
    private static function flatten($data, $prefix = '') {
        $flat = array();
        foreach($data as $k => $v) {
            if(is_scalar($v)) {
                $flat[$prefix.$k] = $v;
                
            } else if(is_array($v)) {
                foreach(self::flatten($v, $k.'.') as $sk => $sv)
                    $flat[$prefix.$sk] = $sv;
            }
        }
        
        return $flat;
    }
    
    /**
     * Replace sender attributes in string
     * 
     * @param string $subject
     * @param mixed $sender
     * 
     * @return string
     */
    private static function customizeForSender($subject, $sender) {
        $data = array();
        
        if(is_object($sender) && ($sender instanceof User)){
            $data = self::flatten($sender->attributes->all);
            
        } else if(is_object($sender) && method_exists($sender, 'toArray')) {
            $data = self::flatten($sender->toArray());
            
        } else if(is_object($sender)) {
            $data = self::flatten(get_object_vars($sender));
            
        } else if(is_array($sender)) {
            $data = $sender;
        }
        
        foreach(self::flatten($data) as $k => $v)
            $subject = str_replace('{'.$k.'}', $v, $subject);
        
        return $subject;
    }

    /**
     * Convert raw to address
     *
     * @param mixed $raw
     * @param bool $ignore_verp
     *
     * @return array
     *
     * @throws BadEmailException
     */
    private static function rawToAddress($raw, $ignore_verp = false) {
        $res = array('address' => $raw, 'gecos' => null, 'lang' => null);

        if(is_object($raw) && ($raw instanceof User)) {
            $res = array(
                'address' => $raw->email,
                'gecos' => $raw->name,
                'lang' => $raw->lang
            );
        
        } else if(is_object($raw) && property_exists($raw, 'email')) {
            $res = array(
                'address' => $raw->email,
                'gecos' => property_exists($raw, 'name') ? $raw->name : null,
                'lang' => property_exists($raw, 'lang') ? $raw->lang : null
            );
        
        } else if(is_array($raw) && array_key_exists('email', $raw)) {
            $res = array(
                'address' => $raw['email'],
                'gecos' => array_key_exists('name', $raw) ? $raw['name'] : null,
                'lang' => array_key_exists('lang', $raw) ? $raw['lang'] : null
            );
        }
        
        $email = $res['address'];
        if($ignore_verp) $email = str_replace('<verp>', 'verp', $email);
        if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            throw new BadEmailException($raw);
        
        return $res;
    }

    /**
     * Set address type header according to config
     *
     * @param string $cfg_param
     * @param mixed $sender
     * @param string $header
     *
     * @return string
     *
     * @throws BadEmailException
     * @throws ConfigBadParameterException
     */
    private function addressHeader($cfg_param, $sender, $header = null) {
        $possibilities = Config::get('email.'.$cfg_param);
        if(!$possibilities)
            return null;
        
        if(
            is_string($possibilities) ||
            (is_array($possibilities) && array_key_exists('email', $possibilities))
        )
            $possibilities = array($possibilities);
        
        if(!is_array($possibilities))
            throw new ConfigBadParameterException('email.'.$cfg_param);
        
        foreach($possibilities as $value) {
            if(is_string($value)) $value = array('email' => $value);
            if(!is_array($value) || !array_key_exists('email', $value)) continue;
            
            if($value['email'] == 'sender') {
                if(!$sender) continue;
                
                $sender_data = self::rawToAddress($sender, ($cfg_param == 'return_path'));
                
                $name = array_key_exists('name', $value) ? $value['name'] : $sender_data['gecos'];
                
                $value = array(
                    'email' => $sender,
                    'name' => self::customizeForSender($name, $sender)
                );
            }
            
            $value = self::rawToAddress($value, ($cfg_param == 'return_path'));
            
            if(!$value['gecos'] || ($cfg_param == 'return_path')) {
                $value = $value['address'];
                
            } else {
                $value = '"'.mb_encode_mimeheader($value['gecos']).'" <'.$value['address'].'>';
            }
            
            if($header) $this->addHeader($header, $value);
            
            return $value;
        }
        
        if($header)
            throw new BadEmailException('email.'.$cfg_param);
        
        return null;
    }
}
