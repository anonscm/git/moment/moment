<?php

/**
 *     Moment - Template.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Template managment class (resolve, parse, render)
 */
class Template {
    /**
     * Resolve template id to path
     *
     * @param string $id template id
     *
     * @return string the path
     */
    private static function resolve($id) {
        return (new Event('template_resolve', $id))->trigger(function($id) {
            $locations = array('config/templates', 'templates', 'templates/core');
            
            if(preg_match('`^plugin:([^:]+):(.+)$`', $id, $m)) {
                $locations = array('config/templates/plugin/'.$m[1], 'templates/plugin/'.$m[1], 'plugins/'.$m[1].'/templates');
                $id = $m[2];
            }
            
            // Look in possible locations
            foreach($locations as $location) {
                $location = EKKO_ROOT.'/'.$location;
                if(!is_dir($location)) continue;
                
                // Return if found
                if(file_exists($location.'/'.$id.'.php'))
                    return $location.'/'.$id.'.php';
            }
            
            // Fail if not found
            throw new TemplateNotFoundException($id);
        });
    }
    
    /**
     * Process a template (catch displayed content)
     *
     * @param string $id template id
     * @param array $vars template variables
     *
     * @return string parsed template content
     */
    public static function process($id, $vars = array()) {
        // Are we asked to not output context related html comments ?
        $addctx = true;
        if(substr($id, 0, 1) == '!') {
            $addctx = false;
            $id = substr($id, 1);
        }
        
        // Resolve template file path
        $path = self::resolve($id);
        
        return (new Event('template_process', $id, $path, $vars, $addctx))->trigger(function($id, $path, $vars, $addctx) {
            // Lambda renderer to isolate context
            $renderer = function($_path, $_vars) {
                foreach($_vars as $_k => $_v) if(substr($_k, 0, 1) != '_') $$_k = $_v;
                include $_path;
            };
            
            // Render 
            $exception = null;
            ob_start();
            try {
                $renderer($path, $vars);
            } catch(Exception $e) {
                $exception = $e;
            }
            $content = ob_get_clean();
            
            // Translation syntax
            while(preg_match('`\{(?:loc|tr|translate):([^}]+)\}`', $content, $m, PREG_OFFSET_CAPTURE)) {
                $content = substr($content, 0, $m[0][1]).Lang::tr($m[1][0]).substr($content, $m[0][1] + strlen($m[0][0]));
            }
            
            // Config syntax
            while(preg_match('`\{(?:cfg|conf|config):([^}]+)\}`', $content, $m, PREG_OFFSET_CAPTURE)) {
                $content = substr($content, 0, $m[0][1]).Config::get($m[1][0]).substr($content, $m[0][1] + strlen($m[0][0]));
            }
            
            // Image syntax
            while(preg_match('`\{(?:img|image):([^}]+)\}`', $content, $m, PREG_OFFSET_CAPTURE)) {
                $content = substr($content, 0, $m[0][1]).GUI::path('images/' .$m[1][0]).substr($content, $m[0][1] + strlen($m[0][0]));
            }
            
            // Path syntax
            while(preg_match('`\{(?:path):([^}]+)\}`', $content, $m, PREG_OFFSET_CAPTURE)) {
                $content = substr($content, 0, $m[0][1]).GUI::path($m[1][0]).substr($content, $m[0][1] + strlen($m[0][0]));
            }
            
            $base_url = preg_replace('`^(https?://)?([^/]+)/`', '/', Config::get('application_url'));
            
            // URL syntax
            while(preg_match('`\{(?:url):([^}]*)\}`', $content, $m, PREG_OFFSET_CAPTURE)) {
                $content = substr($content, 0, $m[0][1]).$base_url.$m[1][0].substr($content, $m[0][1] + strlen($m[0][0]));
            }
            
            while(preg_match('`\{(?:auth_url):([^}]*)\}`', $content, $m, PREG_OFFSET_CAPTURE)) {
                $content = substr($content, 0, $m[0][1]).AuthSP::logonURL($m[1][0]).substr($content, $m[0][1] + strlen($m[0][0]));
            }
            
            // Add context as a html comment if required
            if($addctx) $content = "\n".'<!-- template:'.$id.' start -->'."\n".$content."\n".'<!-- template:'.$id.' end -->'."\n";
            
            // If rendering threw rethrow
            if($exception) throw $exception;
            
            return $content;
        });
    }
    
    /**
     * Sanitize data to avoid tag replacement
     *
     * @param mixed $data
     *
     * @return string
     *
     */
    public static function sanitize($data) {
        return str_replace(array('{', '}'), array('&#123;', '&#125;'), $data);
    }
    
    /**
     * Sanitize data to avoid tag replacement
     *
     * @param mixed $data
     *
     * @return string
     *
     */
    public static function sanitizeOutput($data) {
        return self::sanitize(Utilities::sanitizeOutput($data));
    }
    
    /**
     * Display a template (catch displayed content)
     *
     * @param string $id template id
     * @param array $vars template variables
     *
     * @return string parsed template content
     */
    public static function display($id, $vars = array()) {
        echo self::process($id, $vars);
    }
}