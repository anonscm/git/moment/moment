<?php

/**
 *     Moment - GUI.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * GUI utilities
 * TODO: Améliorer le système de chargement des fichiers JS, css, etc (wut ?)
 */
class GUI {
    /**
     * Base web path
     */
    private static $path = null;
    
    /**
     * Current path cache
     */
    private static $current_path = null;
    
    /**
     * Get stylesheet(s)
     *
     * CSS defaults:
     *  - Foundation
     *  - Font Awesome
     *  - jBox
     *  - others
     * 
     * @return array of http file path
     */
    public static function stylesheets() {
        // Libraries
        $sources = (new Event('css_libraries'))->trigger(function() {
            $libs = array_filter((array)Config::get('css_libraries'));
            return array_merge($libs, array(
                'lib/core/jquery-ui/css/cupertino/jquery-ui-1.10.3.custom.min.css',
                'lib/core/font-awesome/css/font-awesome.min.css',
                'lib/core/jquery-notification/jBox-0.3.2/Source/jBox.css',
                'lib/core/datetimepicker/jquery.datetimepicker.min.css',
                'lib/core/jquery-datatables/1.10.12/css/jquery.dataTables.min.css',
                'lib/core/jquery-multiselect/jquery.multiselect.css',
//                'lib/core/foundation/css/normalize.css',
                'lib/core/foundation/css/foundation.css',
                'lib/core/font/foundation-icons/foundation-icons.css',
            ));
        });
        
        // Basic
        $sources[] = 'css/core/fonts.css';
        $sources[] = 'css/core/view.css';
        $sources[] = 'css/core/form.css';
        //$sources[] = 'css/core/mail.css';
        $sources[] = 'css/core/header.css';
        $sources[] = 'css/core/embedded_wayf.css';
        $sources[] = 'css/core/content.css';
        $sources[] = 'css/core/footer.css';
        
        // Core pages
        $sources = array_merge($sources, self::filesWithExtension('css/core/pages/', 'css'));
        
        // Components
        $sources = array_merge($sources, self::filesWithExtension('css/components/', 'css'));
        
        // Pages
        $sources = array_merge($sources, self::filesWithExtension('css/pages/', 'css'));
        
        // Forms
        $sources = array_merge($sources, self::filesWithExtension('css/forms/', 'css'));
        
        // Plugins
        foreach(PluginManager::getEnabledPlugins() as $name => $p)
            if(file_exists($p['path'].'styles.css'))
                $sources[] = '../plugins/'.$name.'/styles.css';
        
        // General
        $sources = array_merge($sources, self::filesWithExtension('css/', 'css'));
        
        // Skin styles
	if (is_dir(EKKO_ROOT.'/view/skin'))
            $sources[] = 'skin/styles.css';
        
        return (new Event('css_resources', $sources))->trigger(function($sources) {
            return self::filterSources($sources);
        });
    }
    
    /**
     * Include stylesheets
     */
    public static function includeStylesheets() {
        $stylesheets = self::stylesheets();
        
        if(!Config::get('debug')) {
            $stylesheets = array(self::bundleCSS($stylesheets));
            
        } else {
            $stylesheets = str_replace('../plugins/', 'plugin.php/', $stylesheets);
        }
        
        foreach(self::path($stylesheets) as $path)
            echo '<link type="text/css" rel="stylesheet" href="'.$path.'" />'."\n";
    }
    
    /**
     * Get script(s)
     *
     * JS defaults:
     *  - Foundation
     *  - Font Awesome
     *  - jBox
     *  - blockUI
     *  - momentjs
     *  - modernizr
     *  - html5shiv.js
     *  - others
     * 
     * @return array of http file path
     */
    public static function scripts() {
        // Libraries
        $sources = (new Event('js_libraries'))->trigger(function() {
            $libs = array_filter((array)Config::get('js_libraries'));
            return array_merge($libs, array(
                'lib/core/jquery/jquery-1.11.1.min.js',
                'lib/core/jquery-ui/js/jquery-ui-1.11.1.custom.min.js',
                'lib/core/jquery-notification/jBox-0.3.2/Source/jBox.min.js',
                'lib/core/jquery-blockui/jquery.blockUI.js',
                'lib/core/jquery-multiselect/jquery.multiselect.js',
                'lib/core/datetimepicker/jquery.datetimepicker.full.js',
                'lib/core/jquery-datatables/1.10.12/js/jquery.dataTables.min.js',
                'lib/core/foundation/js/vendor/foundation.js',
                'lib/core/foundation/js/vendor/what-input.js',
//                'lib/core/foundation/js/foundation/foundation.topbar.js',
//                'lib/core/foundation/js/foundation/foundation.reveal.js',
//                'lib/core/foundation/js/foundation/foundation.tooltip.js',
//                'lib/core/foundation/js/vendor/modernizr.js',
                'lib/core/moment/moment.min.js',
                'lib/core/moment/moment-with-locales.js',
                'lib/core/moment-timezone/moment-timezone-with-data-2010-2020.js',
                'lib/core/html5shiv/html5shiv.js',
                'lib/core/respondjs/respond.min.js',
//                'lib/core/zurb-responsive-tables/responsive-tables.js',
            ));
        });
        
        // Basic utilities
        $sources[] = 'js/core/event.js';
        $sources[] = 'js/core/promise.js';
        $sources[] = 'js/core/utilities.js';
        $sources[] = 'js/core/client.js';
        $sources[] = 'js/core/session.js';
        $sources[] = 'js/core/lang.js';
        
        // UI utilities
        $sources[] = 'js/core/ui.js';
        $sources[] = 'js/core/popup.js';

        // Form handling
        //$sources[] = 'js/core/form.js';
        $sources[] = 'js/core/form/forms.js';
        $sources[] = 'js/core/form/form.js';
        $sources[] = 'js/core/form/field.js';
        $sources[] = 'js/core/form/fieldset.js';
        $sources[] = 'js/core/form/multiple.js';

        // View handling helper
        $sources[] = 'js/core/template.js';
        $sources[] = 'js/core/view.js';
        
        // Core pages
        $sources = array_merge($sources, self::filesWithExtension('js/core/pages/', 'js'));
        
        // UI components
        $sources = array_merge($sources, self::filesWithExtension('js/components/', 'js'));
        
        // Pages scripts
        $sources = array_merge($sources, self::filesWithExtension('js/pages/', 'js'));
        
        // Form scripts
        $sources = array_merge($sources, self::filesWithExtension('js/forms/', 'js'));
        
        // Plugins
        foreach(PluginManager::getEnabledPlugins() as $name => $p)
            if(file_exists($p['path'].'script.js'))
                $sources[] = '../plugins/'.$name.'/script.js';
        
        // General
        $sources = array_merge($sources, self::filesWithExtension('js/', 'js'));
        
        // Skin script
	if (is_dir(EKKO_ROOT.'/view/skin'))
            $sources[] = 'skin/script.js';
        
        // Startup
        $sources[] = 'js/core/startup.js';
        
        return (new Event('js_resources', $sources))->trigger(function($sources) {
            return self::filterSources($sources);
        });
    }
    
    /**
     * Include scripts
     */
    public static function includeScripts() {
        $scripts = self::scripts();
        
        if(!Config::get('debug')) {
            $scripts = array(self::bundleJS($scripts));
            
        } else {
            $scripts = str_replace('../plugins/', 'plugin.php/', $scripts);
        }
        
        foreach(self::path($scripts) as $path){
            echo '<script type="text/javascript" src="'.$path.'"></script>'."\n";
        }
    }
    
    /**
     * Get favicon
     * 
     * @return string http file path
     */
    public static function favicon() {
        $locations = self::filterSources(array(
            'images/core/favicon.png',
            
            'images/favicon.ico',
            'images/favicon.gif',
            'images/favicon.png',
            
            'skin/favicon.ico',
            'skin/favicon.gif',
            'skin/favicon.png'
        ), false);
        
        return array_pop($locations);
    }
    
    /**
     * Include favicon
     */
    public static function includeFavicon() {
        $location = self::favicon();
        if(!$location) return;
        
        echo '<link type="'.Mime::getFromFile($location).'" rel="icon" href="'.self::path($location).'" />'."\n";
    }
    
    /**
     * Get logo
     * 
     * @return string http file path
     */
    public static function logo() {
        $locations = self::filterSources(array(
            'images/core/logo_application_base.svg',
            
            'images/logo.svg',
            'images/logo.jpg',
            'images/logo.png',
            
            'skin/logo.svg',
            'skin/logo.jpg',
            'skin/logo.png'
        ), false);
        
        return array_pop($locations);
    }
    
    /**
     * Include logo
     */
    public static function includeLogo() {
        $location = self::logo();
        if(!$location) return;
        
        echo '<img src="'.self::path($location).'" alt="'.Config::get('application_name').'" />'."\n";
    }
    
    /**
     * Compute base path
     * 
     * @param string|array|null $location location to append
     * 
     * @return string|array
     */
    public static function path($location = null) {
        if(is_null(self::$path))
            self::$path = preg_replace('`^(https?://)?([^/]+)/`', '/', Config::get('application_url'));
        
        if(is_array($location)) return array_map(function($l) {
            return GUI::path($l);
        }, $location);
        
        if($location && substr($location, -1) != '/' && substr($location, 0, 1) != '?')
            $location .= (strpos($location, '?') ? '&' : '?').'v='.Utilities::runningInstanceUID();
        
        return self::$path.$location;
    }
    
    /**
     * Filter sources based on existance
     * 
     * @param array $sources
     * @param boolean $needAll
     * 
     * @return array
     */
    private static function filterSources($sources, $needAll = true) {
        return array_filter($sources, function($source) use ($needAll) {
            if (!file_exists(EKKO_ROOT.'/view/'.$source)){
                if ($needAll)
                    Logger::warn('Looking for file'.$source.', expecting it at '.EKKO_ROOT.'/view/'.$source.' but nothing found, may (or may not) be a problem ...');
                return false;
            }
            return true;
        });
    }
    
    /**
     * Get all files that have ext
     * 
     * @param string $path
     * @param string $ext
     * 
     * @return array
     */
    public static function filesWithExtension($path, $ext) {
        $real_path = EKKO_ROOT.'/view/'.$path;
        
        if(!is_dir($real_path))
            return array();
        
        $files = array();
        foreach(scandir($real_path) as $i) {
            if(!is_file($real_path.$i)) continue;
            
            if(substr($i, -1 * (strlen($ext) + 1)) != '.'.$ext) continue;
            
            $files[] = $path.$i;
        }
        
        return $files;
    }
    
    /**
     * Bundle files
     * 
     * @param string $target
     * @param array $files
     * @param callable $packer
     * 
     * @return string
     */
    private static function bundleResources($target, $files, $packer = null) {
        $cache = EKKO_ROOT.'/view/cache/'.$target;
        $version = file_exists($cache.'.version') ? trim(file_get_contents($cache.'.version')) : null;
        
        if(file_exists($cache) && ($version == Utilities::runningInstanceUID()))
            return 'cache/'.$target;
        
        $content = array();
        foreach($files as $file) {
            $path = EKKO_ROOT.'/view/'.$file;
            $fctn = file_get_contents($path);
            
            if($packer && is_callable($packer))
                $fctn = $packer($fctn, $file);
            
            $content[] = $fctn;
        }
        
        Utilities::storeInFile($cache, implode("\n\n", $content));
        
        Utilities::storeInFile($cache.'.version', Utilities::runningInstanceUID());
        
        return 'cache/'.$target;
    }
    
    /**
     * Bundle JS
     * 
     * @param array $files
     * 
     * @return string
     */
    private static function bundleJS($files) {
        return self::bundleResources('script.js', $files, function($js, $file) {
            return '/* '.$file.' */'."\n".$js;
        });
    }
    
    /**
     * Bundle CSS
     * 
     * @param array $files
     * 
     * @return string
     */
    private static function bundleCSS($files) {
        return self::bundleResources('styles.css', $files, function($css, $file) {
            $path = dirname($file);
            $css = preg_replace_callback('`url\([\'"]?([^\)\'"]+)[\'"]?\)`', function($m) use($path) {
                return preg_match('`^(https?://|data:)`', $m[1]) ? 'url('.$m[1].')' : 'url("../'.$path.'/'.$m[1].'")';
            }, $css);
            
            return '/* '.$file.' */'."\n".$css;
        });
    }
    
    /**
     * Get current path
     * 
     * @param integer $skip ignore first n tokens
     * 
     * @return array
     */
    public static function getPath($skip = 0) {
        if(is_null(self::$current_path)) {
            $path = '';
            
            if(array_key_exists('PATH_INFO', $_SERVER))
                $path = $_SERVER['PATH_INFO'];
            
            if(array_key_exists('path', $_GET))
                $path = $_GET['path'];
            
            self::$current_path = array_filter(explode('/', $path));
        }
        
        return array_slice(self::$current_path, $skip);
    }
    
    /**
     * Get nth level page from path
     * 
     * @param string $default
     * @param integer $level 0 based level of dispatching
     * 
     * @return string
     */
    public static function getPage($default = 'home', $level = 0) {
        $path = self::getPath($level);
        
        return count($path) ? $path[0] : $default;
    }
    
    /**
     * Redirect browser to another application page
     * 
     * @param string $path
     */
    public static function redirect($path) {
        header('Location: '.Config::get('application_url').$path);
        exit;
    }
}
