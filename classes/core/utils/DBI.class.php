<?php

/**
 *     Moment - DBI.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Database access abstraction class
 * 
 * Handles connexion setup, provides PDO instance methods shorthands and easing methods
 * 
 * @method static bool beginTransaction()
 * @method static bool commit()
 * @method static mixed errorCode()
 * @method static array errorInfo()
 * @method static int exec(string $statement)
 * @method static mixed getAttribute(int $attribute)
 * @method static array getAvailableDrivers()
 * @method static bool inTransaction()
 * @method static string lastInsertId(string $name = null)
 * @method static DBIStatement prepare(string $statement , array $driver_options = array())
 * @method static DBIStatement prepareInQuery(string $statement, array $datasets)
 * @method static DBIStatement query(string $statement)
 * @method static string quote(string $string , int $parameter_type = PDO::PARAM_STR)
 * @method static bool rollBack()
 * @method static bool setAttribute(int $attribute , mixed $value)
 */
class DBI {
    /**
     * Connexion data
     */
    private $config = null;
    
    /**
     * Connection instance
     */
    private $pdo = null;
    
    /**
     * Last request timestamp for ping
     */
    private $last_request = null;
    
    /**
     * Local database instance
     */
    private static $instance = null;
    
    /**
     * Remote instances
     */
    private static $remote_instances = array();
    
    /**
     * Construct new connection
     * 
     * @param boolean $remote true if remote database
     * @param string $identifier "admin"/null if not remote, remote database identifier otherwise
     * 
     * @throws DBIUnknownRemoteException
     * @throws DBIConnexionMissingParameterException
     * @throws DBIConnexionException
     */
    private function __construct($remote = false, $identifier = null) {
        $cfgid = 'db';
        if($remote) {
            $cfgid = 'remote_db';
            
            if(!$identifier)
                throw new DBIUnknownRemoteException('');
            
        } else {
            if(!$identifier) $identifier = 'user';
        }
        
        $config = Config::get($cfgid);
        
        if($identifier == 'admin') {
            $admin = Config::get('db_admin');
            if($admin) {
                $config['username'] = $admin['username'];
                $config['password'] = $admin['password'];
            }
        }
        
        if($remote) {
            if(!array_key_exists($identifier, $config))
                throw new DBIUnknownRemoteException($identifier);
            
            $config = $config[$identifier];
        }
        
        // Check mandatory parameters
        foreach(array(
            'dsn', 'type', 'host', 'database', 'port', 'username',
            'password', 'driver_options', 'charset', 'collation'
        ) as $p)
            if(!array_key_exists($p, $config))
                $config[$p] = null;
        
        // Build dsn from individual components if not defined
        if($config['dsn']) {
            $dsn = explode(':', $config['dsn']);
            $config['type'] = $dsn[0];
            
        } else {
            $params = array();
            
            if(!$config['host']) throw new DBIConnexionMissingParameterException('host');
            $params[] = 'host='.$config['host'];
            
            if(!$config['database']) throw new DBIConnexionMissingParameterException('database');
            $params[] = 'dbname='.$config['database'];
            
            if($config['port']) $params[] = 'port='.$config['port'];
            
            $config['dsn'] = $config['type'].':'.implode(';', $params);
        }
        
        // Check that required parameters are not empty
        if(!$config['username']) throw new DBIConnexionMissingParameterException('username');
        if(!$config['password']) throw new DBIConnexionMissingParameterException('password');
        
        if(!$config['driver_options']) $config['driver_options'] = array();
        
        $config['id'] = ($remote ? 'remote' : 'local').'/'.$identifier;
        
        $this->config = $config;
        
        $this->connect();
    }
    
    /**
     * Connect
     */
    private function connect() {
        // Close any remaining connexion
        $this->pdo = null;
        
        // Try to connect, cast any thrown exception
        try {
            // Connect
            $this->pdo = new PDO(
                $this->config['dsn'],
                $this->config['username'],
                $this->config['password'],
                $this->config['driver_options']
            );
            
            // Set options : throw if error, do not cast returned values
            // to string, fetch as associative array by default
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
            $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            
            $this->last_request = time();
            
            // db_charset given in config ?
            if($this->config['charset']) {
                if($this->config['collation']) {
                    self::prepare('SET NAMES :charset COLLATE :collation')->execute(array(
                        ':charset' => $this->config['charset'],
                        ':collation' => $this->config['collation']
                    ));
                }else{
                    self::prepare('SET NAMES :charset')->execute(array(
                        ':charset' => $this->config['charset']
                    ));
                }
            }
            
        } catch(Exception $e) {
            throw new DBIConnexionException('DBI connect error : '.$e->getMessage());
        }
    }
    
    /**
     * Ping connection
     */
    private function ping() {
        // Do not ping if last request less than Xmin ago
        $timeout = array_key_exists('timeout', $this->config) ? (int)$this->config['timeout'] : null;
        if(!$timeout) $timeout = 900;
        
        if($this->pdo && (time() - $this->last_request < $timeout))
            return;
        
        if($this->pdo) {
            try {
                $this->pdo->query('SELECT 1');
                return;
            } catch(PDOException $e) {}
        }
        
        // Ping failed, reconnect
        $this->connect();
        
        // Only log after as logger may use DBI
        Logger::info('Connection to database '.$this->config['id'].' was lost, restored');
    }
    
    /**
     * Get local connexion
     * 
     * @return self
     */
    public static function local() {
        if(!self::$instance)
            self::$instance = new self(false);
        
        return self::$instance;
    }
    
    /**
     * Get admin connexion
     * 
     * @return self
     */
    public static function admin() {
        if(!self::$instance)
            self::$instance = new self(false, 'admin');
        
        return self::$instance;
    }
    
    /**
     * Get connexion to remote database
     * 
     * @param string $identifier
     * 
     * @return self
     */
    public static function remote($identifier) {
        if(!array_key_exists($identifier, self::$remote_instances))
            self::$remote_instances[$identifier] = new self(true, $identifier);
        
        return self::$remote_instances[$identifier];
    }
    
    /**
     * Magic call handler
     * 
     * Forwards calls to methods to existing PDO instance methods
     * 
     * @param string $name name of the wanted method
     * @param array $args arguments to forward
     * 
     * @throws DBIUsageException
     * 
     * @return mixed value returned by PDO call
     */
    public function __call($name, $args) {
        if($name =='prepareInQuery') {
            $query = array_shift($args);
            $sets = array_shift($args);
            
            foreach($sets as $key => $values) {
                if(is_array($values)) $values = count($values);
                
                // If there is values replace by fitting amount of OR clauses, set falsy clause otherwise
                if(is_int($values) && $values) {
                    $query = preg_replace_callback('`\s+([^\s]+)\s+IN\s+'.$key.'\b`i', function($m) use($key, $values) {
                        $cdn = array();
                        for($i=0; $i<$values; $i++)
                            $cdn[] = $m[1].' = '.$key.'___'.$i;
                        
                        return ' ('.implode(' OR ', $cdn).') ';
                    }, $query);
                } else {
                    $query = preg_replace('`\s+([^\s]+)\s+IN\s+'.$key.'\b`i', ' 1=0', $query);
                }
            }
            
            // Prepare transformed query
            $r = $this->prepare($query);
            
            return $r;
        }
        
        $this->ping();
        
        // Log usual queries
        $this->last_request = time();
        if(in_array($name, array('prepare', 'query', 'exec'))) Logger::debug('DBI call');
        
        // Does the called method exist ?
        if(!method_exists($this->pdo, $name)) throw new DBIUsageException('Calling unknown DBI method '.$name);
        
        // Try to call, cast any thrown exception
        try {
            $r = call_user_func_array(array($this->pdo, $name), $args);
            
            // Cast any returned PDOStatment to a DBIStatment so that fetches and such may be logged
            if(is_object($r) && ($r instanceof PDOStatement))
                $r = new DBIStatement($r);
            
            return $r;
        } catch(Exception $e) {
            throw new DBIUsageException($e->getMessage(), array('name' => $name, 'args' => $args));
        }
    }

    /**
     * Getter
     *
     * @param string $property
     *
     * @return mixed
     *
     * @throws PropertyAccessException
     */
    public function __get($property) {
        if($property == 'type') return $this->config['type'];
        
        throw new PropertyAccessException($this, $property);
    }

    /**
     * Magic call handler
     *
     * Forwards calls to static methods to local instance
     *
     * @see __call
     *
     * @param string $name
     * @param array $arguments
     *
     * @return mixed
     */
    public static function __callStatic($name, $arguments) {
        return call_user_func_array(array(self::local(), '__call'), array($name, $arguments));
    }
}
