<?php

/**
 *     Moment - CoreData.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Application custom data handling
 */
class CoreData {
    /**
     * Storage path
     */
    const STORAGE_PATH = '/data/core/';
    
    /**
     * Cache path
     */
    const CACHE_PATH = 'core_data';
    
    /**
     * @var array Stored data mapping
     */
    private static $store = null;

    /**
     * Check if data exists and return file name (explore store if needed)
     *
     * @param string $key
     *
     * @return string
     */
    private static function exists($key) {
        if(is_null(static::$store)) {
            static::$store = array();
            $storage = EKKO_ROOT . static::STORAGE_PATH;
            
            foreach(scandir($storage) as $file) {
                if(!is_file($storage.$file)) continue;
                if(!preg_match('`(.+)\.json?$`', $file, $match)) continue;
                
                static::$store[$match[1]] = $storage.$file;
            }
        }
        
        return array_key_exists($key, static::$store) ? static::$store[$key] : null;
    }

    /**
     * Get data
     *
     * @param string $key
     *
     * @return mixed
     *
     * @throws CoreCannotReadFileException
     */
    public static function get($key) {
        $file = static::exists($key); // Allows for cache canceling
        if(!$file) return null;
        
        // (Maybe) shared cache
        $cached = ApplicationCache::get(array(static::CACHE_PATH, $key));
        if(!is_null($cached)) return $cached['value'];
        
        $data = JSON::decode(Utilities::readFile($file));
        if(!$data || !is_object($data) || !property_exists($data, 'value'))
            throw new CoreCannotReadFileException($file);
        
        // Cache array to allow for null values without constant reload
        ApplicationCache::set(array(static::CACHE_PATH, $key), array('value' => $data->value));
        
        return $data->value;
    }
    
    /**
     * Set data
     * 
     * @param string $key
     * @param mixed $value
     */
    public static function set($key, $value) {
        $file = EKKO_ROOT . static::STORAGE_PATH.$key.'.json';
        
        Utilities::storeInFile($file, JSON::encode($value));
        
        // Cache array to allow for null values without constant reload
        ApplicationCache::set(array(static::CACHE_PATH, $key), array('value' => $value));
    }
}
