<?php

/**
 *     Moment - Lang.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Language management class (current user language, translations ...)
 */
class Lang {
    /**
     * Translations (lang_id to translated version)
     */
    private static $translations = null;

    /**
     * Loader lock
     */
    private static $loading = false;

    /**
     * Available languages
     */
    private static $available_languages = null;

    /**
     * Current lang code stack
     */
    private static $code_stack = null;

    /**
     * Get available languages
     *
     * @return array
     */
    public static function getAvailableLanguages() {
        // Already cached ?
        if(is_null(self::$available_languages)) {
            self::$available_languages = array();

            // Include first found locale list
            $sources = array('config/language/locale.php', 'language/locale.php', 'language/core/locale.php');

            $locales = array();
            foreach($sources as $file) {
                if(file_exists(EKKO_ROOT.'/'.$file)) {
                    include EKKO_ROOT.'/'.$file;
                    break;
                }
            }

            // Normalize locales
            foreach($locales as $id => $dfn) {
                $name = $id;
                $path = $dfn;

                if(is_array($dfn)) {
                    $path = $dfn['path'];
                    if(array_key_exists('name', $dfn))
                        $name = $dfn['name'];
                }

                self::$available_languages[$id] = array(
                    'name' => $name,
                    'path' => $path
                );
            }
        }

        return self::$available_languages;
    }

    /**
     * Check if a lang code is available (directly or throught aliasing)
     *
     * @param string $raw_code
     *
     * @return mixed real code or null if not found
     */
    public static function realCode($raw_code) {
        $raw_code = str_replace('_', '-', strtolower($raw_code));
        $available = self::getAvailableLanguages();

        // Exists as is ?
        if(array_key_exists($raw_code, $available))
            return $raw_code;

        // Lookup main code
        $parts = explode('-', $raw_code);
        $main = array_shift($parts);

        if(array_key_exists($main, $available))
            return $main;

        return null;
    }

    /**
     * Get current lang code stack
     *
     * @return array
     *
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    private static function getCodeStack() {
        if(is_null(self::$code_stack)) {
            $stack = array();

            $available = self::getAvailableLanguages();

            // Fill stack by order of preference and without duplicates

            if(count($available) > 1) {
                // Auth exception should not stop processing of lang code
                try {
                    // URL/session given language
                    if(Config::get('lang.use_url')) {
                        if(array_key_exists('lang', $_GET) && preg_match('`^[a-z]+(-.+)?$`', $_GET['lang'])) {
                            $code = self::realCode($_GET['lang']);
                            if($code) {
                                if(!in_array($code, $stack))
                                    $stack[] = $code;

                                if(isset($_SESSION)) $_SESSION['lang'] = $code;
                                if(Config::get('lang.save_user_pref') && Auth::isAuthenticated()) {
                                    Auth::user()->lang = $code;
                                    Auth::user()->save();
                                }
                            }
                        }

                        if(isset($_SESSION) && array_key_exists('lang', $_SESSION)) {
                            if(!in_array($_SESSION['lang'], $stack))
                                $stack[] = $_SESSION['lang'];
                        }
                    }

                    // User preference stored language
                    if(Config::get('lang.use_user_pref') && Auth::isAuthenticated()) {
                        $code = Auth::user()->lang;
                        if($code && !in_array($code, $stack))
                            $stack[] = $code;
                    }
                } catch(Exception $e) {}

                // Browser language
                if(Config::get('lang.use_browser') && array_key_exists('HTTP_ACCEPT_LANGUAGE', $_SERVER)) {
                    $codes = array();
                    foreach(array_map('trim', explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE'])) as $part) {
                        $code = $part;
                        $weight = 1;
                        if(strpos($part, ';') !== false) {
                            $part = array_map('trim', explode(';', $part));
                            $code = array_shift($part);
                            foreach($part as $p)
                                if(preg_match('`^q=([0-9]+\.[0-9]+)$`', $p, $m))
                                    $weight = (float)$m[1];
                        }
                        $codes[$code] = $weight;
                    }

                    uasort($codes, function($a, $b) {
                        return ($b > $a) ? 1 : (($b < $a) ? -1 : 0);
                    });

                    foreach($codes as $code => $weight) {
                        $code = self::realCode($code);
                        if($code && !in_array($code, $stack))
                            $stack[] = $code;
                    }
                }
            }

            // Filter provided values
            $stack = array_filter($stack, function($code) use($available) {
                return array_key_exists($code, $available);
            });

            // Config default language
            $code = Config::get('lang.default');
            if($code) {
                $code = self::realCode($code);
                if($code && !in_array($code, $stack))
                    $stack[] = $code;
            }

            // Absolute default
            $stack[] = 'en';

            // Add to cached stack (most significant first)
            $main = array_shift($stack);
            self::$code_stack = array('main' => $main, 'fallback' => $stack);

            Logger::debug(self::$code_stack);
        }

        return self::$code_stack;
    }

    /**
     * Get base code, without any user related getters
     *
     * @return string
     *
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     */
    public static function getBaseCode() {
        // Config default language
        $code = Config::get('lang.default');
        if($code) {
            $code = self::realCode($code);
            if($code) return $code;
        }

        return key(self::getAvailableLanguages()); // Defaults to first available language
    }

    /**
     * Get current lang code
     *
     * @return string
     *
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    public static function getCode() {
        $stack = self::getCodeStack();

        return $stack['main'];
    }

    /**
     * Get current lang stack id
     *
     * @return string
     *
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     * @throws JSONCannotEncodeException
     */
    public static function getCodeStackUID() {
        return substr(sha1(JSON::encode(self::getCodeStack())), -12);
    }


    /**
     * Get current dictionary unique id
     *
     * @return string
     *
     * @throws AuthUserNotAllowedException
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     * @throws JSONCannotEncodeException
     */
    public static function getDictionnaryUID() {
        return substr(sha1(
            JSON::encode(self::getCodeStack())
            .Utilities::userUID()
            .Utilities::runningInstanceUID()
        ), -12);
    }

    /**
     * Clean lang string id
     *
     * @param string $id
     *
     * @return string cleaned id
     */
    public static function cleanId($id) {
        $id = trim($id);
        $id = trim($id, '_');
        $id = strtolower($id);
        return $id;
    }

    /**
     * Load dictionary
     *
     * @param string $rel_path translations relative path
     *
     * @return array
     *
     * @throws ConfigBadParameterException
     * @throws ConfigMissingParameterException
     */
    private static function loadDictionary($rel_path) {
        Logger::debug($rel_path);

        $dictionary = array();

        // Translations locations
        $locations = array(
            'language/core',
            'language',
            'config/language'
        );

        // Lookup locations for translation files
        foreach($locations as $location) {
            $path = EKKO_ROOT.'/'.$location.'/'.$rel_path;
            Logger::debug($path);

            if(!is_dir($path)) continue;

            // Main translation file
            if(file_exists($path.'/lang.php')) {
                $lang = array();
                include $path.'/lang.php';
                foreach($lang as $id => $s)
                    $dictionary[$id] = array('text' => $s);
            }

            // Extended file name based translations
            foreach(scandir($path) as $i) {
                if(!is_file($path.'/'.$i)) continue;

                if(preg_match('`^([^.]+)\.(te?xt(\.php)?|html?(\.php)?|php)$`', $i, $m)) {
                    if($m[1] == 'lang') continue;
                    if(!array_key_exists($m[1], $dictionary))
                        $dictionary[$m[1]] = array('text' => null);
                    $dictionary[$m[1]]['file'] = $path.'/'.$i;
                }
            }
        }

        return $dictionary;
    }

    /**
     * Load dictionaries
     *
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    private static function loadDictionaries() {
        if(!is_null(self::$translations) || self::$loading) return;

        self::$loading = true;

        // Get lang codes stack
        $stack = self::getCodeStack();

        // Get list of available languages
        $available = self::getAvailableLanguages();
        if(!array_key_exists('en', $available)) $available['en'] = array('path' => 'en_AU');

        // Build fallback dictionaries
        $fallback = array();
        foreach($stack['fallback'] as $code) {
            $dictionary = self::loadDictionary($available[$code]['path']);

            foreach($dictionary as $id => $d)
                if(!array_key_exists($id, $fallback))
                    $fallback[$id] = $d;
        }

        // Set dictionaries cache
        self::$translations = array(
            'main' => self::loadDictionary($available[$stack['main']]['path']),
            'fallback' => $fallback
        );

        self::$loading = false;
    }

    /**
     * Translate a string
     *
     * @param string $id identifier of lang string
     *
     * @return Translation
     *
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    public static function translate($id) {
        // Load dictionaries if not already done
        self::loadDictionaries();

        // Translate list of keys
        if (is_array($id))
            return array_combine($id, array_map(function($v){
                return self::tr($v);
            }, $id));


        // Clean given id from unwanted characters
        $id = self::cleanId($id);

        // Need a translation while loading dictionaries, probably
        // encoutering an exception, do not translate to avoid loops
        if(self::$loading) return new Translation('{'.$id.'}', false);

        // Lookup for translation in the main language, warn and look in the fallbacks if not found
        if(array_key_exists($id, self::$translations['main'])) {
            $tr = self::$translations['main'][$id];
            $src = 'main';
        }else{
            $stack = self::getCodeStack();
            Logger::warn('No translation found for '.$id.' in '.$stack['main'].' language');

            if(array_key_exists($id, self::$translations['fallback'])) {
                Logger::warn('No fallback translation found for '.$id.' in '.implode(', ', $stack['fallback']).' languages');

                $tr = self::$translations['fallback'][$id];
                $src = 'fallback';
            }else{
                return new Translation('{'.$id.'}', false);
            }
        }

        // File based ? Then loads it up and cache contents
        if(is_null($tr['text']) && array_key_exists('file', $tr)) {
            ob_start(); // Allows for php inside translations
            include $tr['file'];
            $s = ob_get_clean();

            $tr['text'] = $s;
            self::$translations[$src][$id]['text'] = $s; // Update cache
        }

        // Config syntax replacments
        $tr['text'] = self::replaceConfigValues($tr['text']);

        // Cast to translation object
        return new Translation($tr['text']);
    }

    /**
     * Replace config values (not doing that in Translation::replace avoids config extraction by syntax injection)
     *
     * @param string $text
     *
     * @return string
     */
    private static function replaceConfigValues($text) {
        return preg_replace_callback('`\{(|size:)(cfg|conf|config):([^}]+)\}`', function($m) {
            $value = Config::get($m[3]);
            switch(substr($m[1], 0, -1)) {
                case 'size' : $value = Utilities::formatBytes($value); break;
            }
            return $value;
        }, $text);
    }

    /**
     * Translation shortcut
     *
     * @param string $id identifier of lang string
     *
     * @return Translation
     *
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    public static function tr($id) {
        return self::translate($id);
    }

    /**
     * Translate email
     *
     * @param string $id identifier of email
     * @param string|null $lang
     *
     * @return Translation
     *
     * @throws DetailedException
     */
    public static function translateEmail($id, $lang = null) {
        return (new Event('translate_email', $id, $lang))->trigger(function() use($id, $lang) {
            // Load (user) lang codes stack
            $stack = self::getCodeStack();

            // Merge main and fallbacks into a single stack
            $codes = $stack['fallback'];
            array_unshift($codes, $stack['main']);

            // Add given lang if any
            if ($lang) array_unshift($codes, $lang);

            // Translations locations
            $locations = array(
                'config/language',
                'language',
                'language/core'
            );

            // List available languages
            $available = self::getAvailableLanguages();

            // Look for translation in code stack ...
            foreach ($codes as $code) {
                if (!array_key_exists($code, $available)) continue;

                // ... and for each possible location
                foreach ($locations as $location) {
                    // Translations path
                    $path = EKKO_ROOT . '/' . $location . '/' . $available[$code]['path'];

                    if (!is_dir($path)) continue;

                    // Mail translation file
                    $file = $path . '/' . $id . '.mail';

                    if (file_exists($file . '.php')) $file .= '.php';

                    // No matching file ? Then go to next location / code
                    if (!file_exists($file)) continue;

                    // Load file contents and eval
                    ob_start();
                    include $file;
                    $translation = trim(ob_get_clean());

                    // Separate headers from body
                    $parts = preg_split('`\n\s*\n`', $translation, 2);

                    // Do we have headings
                    $subject = array('prefix' => trim((string)Config::get('email.subject_prefix')), '{cfg:application_name}');
                    if (count($parts) > 1) {
                        $headers = explode("\n", array_shift($parts));
                        foreach ($headers as $line) {
                            // Get subject
                            if (preg_match('`^\s*subject\s*:\s*(.+)$`i', $line, $m))
                                $subject[] = trim($m[1]);
                        }
                    }

                    // Try to split body based on alternatives tags
                    $misc = array();
                    $plain = array();
                    $html = array();
                    $mode = null;
                    foreach (explode("\n", array_shift($parts)) as $line) {
                        if (trim($line) == '{alternative:plain}') {
                            $mode = 'plain';
                        } else if (trim($line) == '{alternative:html}') {
                            $mode = 'html';
                        } else if (trim($line) == '{alternative}') {
                            if ($mode == 'plain') $mode = 'html';
                            else if ($mode == 'html') $mode = 'plain';
                            else $mode = 'html';
                        } else if ($mode == 'html') {
                            $html[] = $line;
                        } else if ($mode == 'plain') {
                            $plain[] = $line;
                        } else $misc[] = $line;
                    }

                    // Trim contents
                    $misc = trim(implode("\n", $misc));
                    $plain = trim(implode("\n", $plain));
                    $html = trim(implode("\n", $html));

                    // Handle defaults
                    if ($misc) {
                        if ($html && !$plain) $plain = $misc;
                        if ($plain && !$html) $html = $misc;

                        if (!$html && !$plain) {
                            if (preg_match('`(</(a|p|table|td|tr)>|<br\s*/?>)`', $misc)) {
                                $html = $misc;
                            } else {
                                $plain = $misc;
                            }
                        }
                    }

                    // Config syntax replacements
                    $subject = self::replaceConfigValues($subject);
                    list($plain, $html) = self::replaceConfigValues(array($plain, $html));

                    // Convert to Translation instance with sub-Translations
                    return new Translation(array(
                        'subject' => new Translation($subject, true, true), // Raw outputting, will be encoded
                        'plain' => new Translation($plain, true, true), // Raw outputting
                        'html' => new Translation($html, true, false)
                    ));
                }
            }

            // No translation found
            throw new DetailedException('mail_translation_not_found', 'id = ' . $id);
        });
    }

    /**
     * Whole dictionary getter
     *
     * Do not get file-translated strings
     *
     * @return array
     *
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    public static function getTranslations() {
        self::loadDictionaries();

        return array_filter(array_map(function($t) {
            if(is_null($t['text']) && array_key_exists('file', $t)) {
                ob_start(); // Allows for php inside translations
                include $t['file'];
                $s = ob_get_clean();

                $t['text'] = $s;
            }

            return $t['text'];
        }, array_merge(self::$translations['fallback'], self::$translations['main'])), function($t) {
            return !is_null($t);
        });
    }
}

