<?php

/**
 *     Moment - ApplicationCache.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Application wide cache
 */
class ApplicationCache {
    /**
     * @var array cached data
     */
    public static $cache = array();
    
    /**
     * Check if "path" exists
     * 
     * @param array $path
     * 
     * @return boolean
     */
    public static function exists($path) {
        return (new Event('cache_exists', $path))->trigger(function() use($path) {
            $key = implode('/', $path);
            
            return array_key_exists($key, self::$cache);
            
        });
    }
    
    /**
     * List sub entries
     * 
     * @param array $path
     * 
     * @return array of paths
     */
    public static function index($path) {
        return (new Event('cache_index', $path))->trigger(function() use($path) {
            $key = implode('/', $path).'/';
            
            $keys = preg_grep('`^'.preg_quote($key, '`').'`', array_keys(self::$cache));
            
            return array_map(function($key) {
                return explode('/', $key);
            }, $keys);
            
        });
    }
    
    /**
     * Get cached data
     * 
     * @param array $path
     * 
     * @return mixed
     */
    public static function get($path) {
        return (new Event('cache_get', $path))->trigger(function() use($path) {
            $key = implode('/', $path);
            
            return array_key_exists($key, self::$cache) ? self::$cache[$key] : null;
            
        });
    }
    
    /**
     * List sub entries whose key matches prefix
     * 
     * @param array $path
     * 
     * @return array of path and data pairs
     */
    public static function match($path) {
        return (new Event('cache_match', $path))->trigger(function() use($path) {
            $key = implode('/', $path).'/';
            
            $keys = preg_grep('`^'.preg_quote($key, '`').'`', array_keys(self::$cache));
            
            $entries = array();
            foreach($keys as $key)
                $entries[] = array('path' => explode('/',$key), 'data' => self::$cache[$key]);
            
            return $entries;
            
        });
    }
    
    /**
     * Set cache entry
     * 
     * @param string $data to store
     * @param array $path
     * 
     * @return mixed
     */
    public static function set($path, $data) {
        return (new Event('cache_set', $path, $data))->trigger(function() use($path, $data) {
            $key = implode('/', $path);
            
            self::$cache[$key] = $data;
            
        });
    }

    /**
     * Drop path
     *
     * @param array $path
     *
     * @return mixed
     */
    public static function drop($path) {
        return (new Event('cache_drop', $path))->trigger(function() use($path) {
            $key = implode('/', $path);
            
            if(substr($key, -1) == '*') {
                $key = substr($key, 0, -1);
                
                $drop = array();
                foreach(array_keys(self::$cache) as $k)
                    if(substr($k, 0, strlen($key)) == $key)
                        $drop[] = $k;
                
            } else {
                $drop = array($key);
            }
            
            foreach($drop as $key)
                unset(self::$cache[$key]);
            
        });
    }
}
