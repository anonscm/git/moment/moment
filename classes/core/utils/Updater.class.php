<?php

/**
 *     Moment - Updater.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Application updater
 */
class Updater {
    /**
     * Update scripts path
     */
    const PATH = '/scripts/update/';

    /**
     * Compare versions
     *
     * @param string $a
     * @param string $b
     *
     * @return integer (see php's version_compare)
     */
    public static function compareVersions($a, $b) {
        return version_compare($a, $b);
    }
    
    /**
     * Gather updates
     * 
     * @param string $from_version
     * @param string $to_version
     * 
     * @return array
     */
    private static function gatherUpdates($from_version, $to_version) {
        $path = EKKO_ROOT . self::PATH;
        
        $dependencies = (array)CoreData::get('version_dependencies');
        
        $updates = array();
        
        // Scan for steps
        foreach(scandir($path) as $file) {
            if(!is_file($path.$file)) continue;
            if(!preg_match('`^([0-9][0-9a-zA-Z\.\+_-]*)_to_([0-9][0-9a-zA-Z\.\+_-]*)\.php`', $file, $m)) continue;
            
            $cmp = self::compareVersions($m[1], $m[2]);
            if(!$cmp) continue; // Same versions ? Really ?
            
            if(self::compareVersions($m[1], $from_version) < 0) continue; // Scripts starts from an earlier version
            if(self::compareVersions($m[2], $to_version) > 0) continue; // Scripts ends at an later version
            
            // In case of version swap
            $from = ($cmp < 0) ? $m[1] : $m[2];
            $to = ($cmp > 0) ? $m[1] : $m[2];
            
            // Core update ?
            if(array_key_exists($from, $dependencies) && array_key_exists($to, $dependencies)) {
                $core_from = $dependencies[$from];
                $core_to = $dependencies[$to];
                
                $core_file = $path.'core/'.$core_from.'_to_'.$core_to.'.php';
                if(file_exists($core_file)) {
                    $updates[] = new Update($from, $to, 0, array('from' => $core_from, 'to' => $core_to)); // Set current update
                    include $core_file; // Register steps
                }
            }
            
            $updates[] = new Update($from, $to, 1); // Set current update
            include $path.$file; // Register steps
        }
        
        return $updates;
    }
    
    /**
     * Run update
     * 
     * @param string $from version
     * @param string $to version
     */
    public static function run($from = null, $to = null) {
        if(is_null($from)) $from = ApplicationVersion::getInstalledVersion();
        if(!$from) $from = '0';
        
        if(is_null($to)) $to = ApplicationVersion::getCodeVersion();
        
        Logger::info('Updating application from version '.$from.' to version '.$to);
        
        $updates = self::gatherUpdates($from, $to);
        
        // Pre-update
        $before = array_filter($updates, function($u) {
            return $u->has_before;
        });
        usort($before, function($a, $b) {
            return $a->compare($b, Update::BEFORE);
        });
        
        Logger::info('Found '.count($before).' pre-update steps, running them ...');
        
        foreach($before as $update)
            $update->run();
        
        Logger::info('... done running pre-update');
        
        // Database
        Logger::info('Updating database structure ...');
        
        DBUpdater::updateModel();
        
        Logger::info('... done updating database');
        
        // Post-update
        $after = array_filter($updates, function($u) {
            return $u->has_after;
        });
        usort($after, function($a, $b) {
            return $a->compare($b, Update::AFTER);
        });
        
        Logger::info('Found '.count($after).' post-update steps, running them ...');
        
        foreach($after as $update)
            $update->run();
        
        Logger::info('... done running post-update');
        
        Logger::info('Done updating application from version '.$from.' to version '.$to);
    }
}
