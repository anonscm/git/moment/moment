<?php

/**
 *     Moment - ConfigValidator.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Config validator helper
 * 
 * Every error leads to php error logging as Logger may not have what it needs to work already
 */
class ConfigValidator {
    /**
     * Checks
     */
    private static $checks = array();
    
    /**
     * Standard checks pattern
     */
    const STD_PATTERN = '(?:is_)?(?:not_empty|null|bool|string|int|float|array|callable|email)';
    
    /**
     * Match and split standard checks
     * 
     * @param string $check
     * 
     * @return mixed array if syntax ok, null otherwise
     */
    private static function parseStandardChecks($check) {
        if(!preg_match('`^\s*'.self::STD_PATTERN.'(?:\s*\|\s*'.self::STD_PATTERN.')*\s*$`', $check)) return null;
        
        return array_map('trim', explode('|', $check));
    }
    
    /**
     * Check database config field
     * 
     * @param array $config database config fragment
     * @param string $key sub-parameter to check
     * @param boolean $mandatory
     * 
     * @return mixed string if error, null otherwise
     */
    public static function validateDBConfigParameter($config, $key, $mandatory = true) {
        if(array_key_exists($key, $config)) {
            if(!is_string($config[$key]))
                return 'string';
        
        } else if($mandatory) {
            return 'set';
        }
        
        return null;
    }

    /**
     * Check database config fragment
     *
     * @param array $config database config fragment
     * @param string $param context
     *
     * @throws ConfigValidationException
     */
    public static function validateDBConfig($config, $param) {
        if(
            !array_key_exists('dsn', $config)
            && !array_key_exists('type', $config)
            && !array_key_exists('database', $config)
        )
            throw new ConfigValidationException('missing either '.$param.'[dsn] or '.$param.'[type] + '.$param.'[database]');
        
        foreach(array(
            'dsn' => !array_key_exists('type', $config),
            'type' => !array_key_exists('dsn', $config),
            'database' => !array_key_exists('dsn', $config),
            'host' => !array_key_exists('dsn', $config),
            'port' => false,
            'username' => true,
            'password' => true,
            'charset' => false,
            'collation' => false
        ) as $key => $mandatory) {
            $expected = self::validateDBConfigParameter($config, $key, $mandatory);
            
            if($expected)
                throw new ConfigValidationException($param.'['.$key.'] is not '.$expected);
        }
        
        if(array_key_exists('driver_options', $config) && !is_array($config['driver_options']))
            throw new ConfigValidationException($param.'[driver_options] must be an array');
    }
    
    /**
     * Check optional database config fragment
     *
     * @param array $config database config fragment
     * @param string $param context
     *
     * @throws ConfigValidationException
     */
    public static function validateOptionalDBConfig($config, $param) {
        if(is_null($config)) return;
    
        foreach(array(
                    'username' => true,
                    'password' => true,
                ) as $key => $mandatory) {
            $expected = self::validateDBConfigParameter($config, $key, $mandatory);
        
            if($expected)
                throw new ConfigValidationException($param.'['.$key.'] is not '.$expected);
        }
    }

    /**
     * Check email sender
     *
     * @param mixed $value
     * @param mixed $param
     *
     * @throws ConfigValidationException
     */
    public static function validateEmailSender($value, $param) {
        if(is_array($value) && !array_key_exists('email', $value)) {
            foreach($value as $idx => $alt)
                self::validateEmailSender($alt, $param.'['.$idx.']');
            
            return;
        }
        
        if(is_string($value)) {
            if($value === 'sender') return;
            
            if(filter_var($value, FILTER_VALIDATE_EMAIL)) return;
            
            throw new ConfigValidationException($param.' must be set to a valid email address or "sender"');
        }
        
        if(is_array($value) && array_key_exists('email', $value)) {
            if(filter_var($value['email'], FILTER_VALIDATE_EMAIL)) return;
            
            throw new ConfigValidationException($param.'.email must be set to a valid email address or "sender"');
        }
        
        throw new ConfigValidationException($param.' is not a valid sender');
    }
    
    /**
     * Check optional email sender
     * 
     * @param mixed $value
     * @param mixed $param
     */
    public static function validateOptionalEmailSender($value, $param) {
        if(is_null($value)) return;
        
        self::validateEmailSender($value, $param);
    }

    /**
     * Add new check(s)
     *
     * @param string $parameter_name
     * @param mixed ... test(s)
     *
     * @throws ConfigValidationException
     */
    public static function addCheck($parameter_name) {
        if(!$parameter_name)
            throw new ConfigValidationException('check needs parameter_name');
        
        if(!array_key_exists($parameter_name, self::$checks))
            self::$checks[$parameter_name] = array();
        
        foreach(array_slice(func_get_args(), 1) as $arg) {
            if(is_string($arg)) {
                $standard = self::parseStandardChecks($arg);
                
                if($standard) {
                    self::$checks[$parameter_name][] = implode('|', $standard);
                    
                } else if(preg_match('`^(is_?)?set$`', $arg)) {
                    self::$checks[$parameter_name][] = 'set';
                    
                } else if(preg_match('`^([a-z][a-z0-9]*)?::([a-z][a-z0-9]*)$`i', $arg, $m)) { // Method
                    $class = $m[1];
                    if(!$class) $class = get_called_class();
                    
                    $method = $m[2];
                    
                    if(!method_exists($class, $method))
                        throw new ConfigValidationException('bad check for "'.$parameter_name.'" (method '.$method.' not found in '.$class.') : '.$arg);
                    
                    self::$checks[$parameter_name][] = function($value, $parameter_name) use($class, $method) {
                        return call_user_func($class.'::'.$method, $value, $parameter_name);
                    };
                    
                } else {
                    throw new ConfigValidationException('bad check for "'.$parameter_name.'" : '.$arg);
                }
                
            } elseif(is_callable($arg)) {
                self::$checks[$parameter_name][] = $arg;
            }
        }
    }
    
    /**
     * Run checks
     */
    public static function run() {
         $all_pass = true;
        
        foreach(self::$checks as $parameter => $checks) {
            if(in_array('set', $checks)) {
                if(!Config::exists($parameter))
                    throw new ConfigValidationException('"'.$parameter.'" must be set');
            }
            
            $value = Config::get($parameter);
            
            try {
                foreach($checks as $check) {
                    if(is_string($check) && ($check == 'set')) continue;
                    
                    if(!Utilities::validateValue($value, $check, $parameter))
                        throw new ConfigValidationException(
                            '"'.$parameter.'" does not match '.
                            (is_callable($check) ? 'expected value' : implode('|', $checks))
                        );
                }
            } catch(ConfigValidationException $e) {
                $all_pass = false;
            }
        }
        
        if(!$all_pass)
            die('Configuration error(s), please check the server logs or ask an admin to do so.');
    }
}
