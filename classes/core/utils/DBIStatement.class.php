<?php

/**
 *     Moment - DBIStatement.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Wrapper around PDOStatement for better exception handling
 *
 * @method bool bindColumn(mixed $column, mixed &$param, int $type = null, int $maxlen = null, mixed $driverdata = null)
 * @method bool bindParam(mixed $parameter, mixed &$variable, int $data_type = PDO::PARAM_STR, int $length = null, mixed $driver_options = null)
 * @method bool bindValue(mixed $parameter, mixed $value, int $data_type = PDO::PARAM_STR)
 * @method bool closeCursor()
 * @method int columnCount()
 * @method void debugDumpParams()
 * @method string errorCode()
 * @method array errorInfo()
 * @method bool execute(array $input_parameters = array())
 * @method mixed fetch(int $fetch_style = null, int $cursor_orientation = PDO::FETCH_ORI_NEXT, int $cursor_offset = 0)
 * @method array fetchAll(int $fetch_style = null, mixed $fetch_argument = null, array $ctor_args = array())
 * @method mixed fetchColumn(int $column_number = 0)
 * @method mixed fetchObject(string $class_name = 'stdClass', array $ctor_args = array())
 * @method mixed getAttribute(int $attribute)
 * @method array getColumnMeta(int $column)
 * @method bool nextRowset()
 * @method int rowCount()
 * @method bool setAttribute(int $attribute, mixed $value)
 * @method bool setFetchMode(int $mode)
 */
class DBIStatement {
    /**
     * Real statement
     */
    private $statement = null;
    
    /**
     * Creates statement
     * 
     * @param PDOStatement $statement
     */
    public function __construct(PDOStatement $statement) {
        $this->statement = $statement;
    }
    
    /**
     * Call forwarder
     * 
     * @param string $method
     * @param array $args
     * 
     * @return mixed
     * 
     * @throws DBIUsageException
     */
    public function __call($method, $args) {
        // Log execute calls
        if($method == 'execute') Logger::debug('DBI call');
        
        // Transform any IN subset into serialized OR values
        if($method == 'execute') {
            if (count($args)){
                foreach($args[0] as $key => $value) {
                    if(is_array($value)) {
                        $values = array_values($value);
                        for($i=0; $i<count($values); $i++)
                            $args[0][$key.'___'.$i] = $values[$i];

                        unset($args[0][$key]);
                    }
                }
            }
        }
        
        // Is the required method valid ?
        if(!method_exists($this->statement, $method)) throw new DBIUsageException('Calling unknown DBIStatement method '.$method);
        
        // Tries to propagate the call, cast any thrown exception
        try {
            return call_user_func_array(array($this->statement, $method), $args);
            
        } catch(Exception $e) {
            throw new DBIUsageException($e->getMessage(), array('method' => $method, 'args' => $args, 'query' => $this->statement->queryString));
        }
    }
}
