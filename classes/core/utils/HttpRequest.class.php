<?php

/**
 *     Moment - HttpRequest.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Request body handler
 * 
 * Needed since php://input can only be read once
 */
class HttpRequest {
    /**
     * Holds the body
     */
    private static $body = null;
    
    /**
     * Get the body
     */
    public static function body() {
        if(is_null(self::$body)) self::$body = file_get_contents('php://input');
        
        return self::$body;
    }
}
