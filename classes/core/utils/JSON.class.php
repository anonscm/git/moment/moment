<?php

/**
 *     Moment - JSON.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

class JSON{
    
    /**
     * Encode to json
     *
     * @param mixed $data
     *
     * @return string
     *
     * @throws JSONCannotEncodeException
     */
    public static function encode($data){
        $encode = json_encode($data);
        if (json_last_error())
            throw new JSONCannotEncodeException(json_last_error_msg());
        
        return $encode;
    }
    
    
    /**
     * Decode from json
     *
     * @param string $data
     * @param boolean $assoc
     *
     * @return mixed
     *
     * @throws JSONCannotDecodeException
     */
    public static function decode($data, $assoc = false){
        $decode = json_decode($data, $assoc);
        if (json_last_error())
            throw new JSONCannotDecodeException(json_last_error_msg());
        
        return $decode;
    }
}