<?php

/**
 *     Moment - Database.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Database managing
 */
class Database {
    /**
     * Cache if delegation class was loaded.
     */
    private static $class = null;
    
    /**
     * Delegates table exists check.
     * 
     * @param string $table table name
     * 
     * @return bool
     */
    public static function tableExists($table) {
        $class = self::getDelegationClass();
        
        return call_user_func($class.'::tableExists', $table);
    }

    /**
     * Delegates table columns format checking.
     *
     * @param string $table table name
     * @param string $definition column definition
     * @param mixed $logger
     *
     * @return bool
     *
     * @throws ConfigBadParameterException
     * @throws CoreFileNotFoundException
     */
    public static function checkTable($table, $definition, $logger = null) {
        $class = self::getDelegationClass();
        
        return call_user_func($class.'::checkTable', $table, $definition, $logger);
    }

    /**
     * Create a table
     *
     * @param string $table the table name
     * @param array $definition dataMap entry
     */
    public static function createTable($table, $definition) {
        $class = self::getDelegationClass();
        
        call_user_func($class.'::createTable', $table, $definition);
    }

    /**
     * Remove a table
     *
     * @param string $table the table name
     */
    public static function removeTable($table) {
        $class = self::getDelegationClass();
        
        call_user_func($class.'::removeTable', $table);
    }

    /**
     * Get selected database delegation class
     *
     * @return string delegation class name
     *
     * @throws ConfigBadParameterException
     * @throws CoreFileNotFoundException
     */
    private static function getDelegationClass() {
        if(is_null(self::$class)) {
            $type = DBI::admin()->type;
            
            if(!$type) throw new ConfigBadParameterException('db|db_admin[type|dsn]');
            $class = 'Database'.ucfirst($type);
            $file = EKKO_ROOT.'/classes/utils/'.$class.'.class.php';
    
            if(!file_exists($file)) {
                $file = EKKO_ROOT.'/classes/core/utils/'.$class.'.class.php';
        
                if(!file_exists($file))
                    throw new CoreFileNotFoundException($file);
            }
            
            self::$class = $class;
        }
        
        return self::$class;
    }
    
    
    /**
     * Ensure table passed has the right format
     * 
     * @param string $table Table name
     * @param array $datamap The datamap
     */
    public static function ensureTableFormat($table, $datamap){
        // Check if table exists
        Logger::info('Look for table '.$table);
        if(self::tableExists($table)) {
            Logger::info('Table found, check columns');
            self::checkTable($table, $datamap);
            
        }else{
            Logger::info('Table is missing, create it');
            self::createTable($table, $datamap);
        }
    }
}
