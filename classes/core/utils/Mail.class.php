<?php

/**
 *     Moment - Mail.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');


class Mail {
    /**
     * Message-Id header value
     */
    protected $msg_id = null;
    
    /**
     * Return path value
     */
    protected $return_path = null;
    
    /**
     * Subject header
     */
    protected $subject = '?';
    
    /**
     * text body parts
     */
    protected $contents = array();
    
    /**
     * Recipients
     */
    protected $rcpt = array('To' => array(), 'Cc' => array(), 'Bcc' => array());
    
    /**
     * Attached files
     */
    protected $attachments = array();
    
    /**
     * Additionnal headers
     */
    protected $headers = array();
    
    /**
     * New line style
     */
    protected $nl = "\r\n";
    
    /**
     * Constructor
     * 
     * @param string $to (optional)
     * @param string $subject (optional)
     */
    public function __construct($to = null, $subject = '') {
        mb_internal_encoding('UTF-8');
        
        if($to) $this->to($to);
        $this->__set('subject', $subject);
        
        $nl = Config::get('email.newline');
        if($nl) $this->nl = $nl;
    }
    
    /**
     * Setter
     * 
     * @param string $property property to get
     * @param mixed $value value to set property to
     * 
     * @throws BadEmailException
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if($property == 'subject') {
            //Zimbra fixed problem with Some Spaces removed in RFC 2047 encoded subject and then this method now works
            $this->subject = mb_encode_mimeheader(trim(str_replace(array("\n", "\r"), ' ', $value)), mb_internal_encoding(), 'Q', $this->nl);

        }else if($property == 'return_path') {
            if(!filter_var($value, FILTER_VALIDATE_EMAIL)) throw new BadEmailException($value);
            $this->return_path = (string)$value;
            
        }else if($property == 'msg_id') {
            $this->msg_id = (string)$value;
            
        }else throw new PropertyAccessException($this, $property);
    }
    
    
    /**
     * Adds recipient
     * 
     * @param string $mode to/cc/bcc
     * @param $email email address
     * @param $name optional name
     */
    public function addRcpt($mode, $email, $name = '') {

        //PATCH : sanitize name
        if($name) {
            $name = mb_encode_mimeheader($name);

            if(preg_match('`[^-A-Za-z0-9!#$%&\'*+/=?^_\`{|}~\s]`', $name))
                $name = '"'.str_replace('"', '\\"', $name).'"';
        }

        $this->rcpt[ucfirst($mode)][] = $name ? $name.' <'.$email.'>' : $email;
    }
    
    /**
     * Adds to
     * 
     * @param string $email email address
     * @param string $name optional name
     */
    public function to($email, $name = '') {
        $this->addRcpt('To', $email, $name);
    }
    
    /**
     * Adds cc
     * 
     * @param string $email email address
     * @param string $name optional name
     */
    public function cc($email, $name = '') {
        $this->addRcpt('Cc', $email, $name);
    }
    
    /**
     * Adds bcc
     * 
     * @param string $email email address
     * @param string $name optional name
     */
    public function bcc($email, $name = '') {
        $this->addRcpt('Bcc', $email, $name);
    }
    
    /**
     * Adds header(s)
     * 
     * @param mixed $header name or array of name=>value
     * @param string $value (optional)
     */
    public function addHeader($header, $value = null) {
        if(!is_array($header)) $header = array($header => $value);
        
        foreach($header as $name => $value)
            $this->headers[$name] = $value;
    }
    
    /**
     * Set mail contents
     * 
     * @param string $content mail contents
     * @param string $mime_type
     */
    public function write($content, $mime_type = 'text/plain') {
        $content = (string) $content;
        if (!$content)
            return;
        
        if ($mime_type === 'text/plain'){
            $priority = MailBodyAlternative::PRIORITY_PLAIN;
                
        } else if ($mime_type === 'text/html') {
            $priority = MailBodyAlternative::PRIORITY_HTML;
            
        }else {
            $priority = MailBodyAlternative::PRIORITY_LOW;
        }
        
        $mba = new MailBodyAlternative($mime_type, $priority);
        $mba->content = $content;        
        $this->contents[] = $mba;
    }
    
    /**
     * Write HTML part
     * 
     * @param string $ctn text data
     */
    public function writeHTML($ctn) {
        $this->write($ctn, 'text/html');
    }
    
    /**
     * Write Plain part
     * 
     * @param string $ctn text data
     */
    public function writePlain($ctn) {
        $this->write($ctn, 'text/plain');
    }
    
    
    
    /**
     * Add an alternative
     * 
     * @param MailBodyAlternative $mba
     */
    public function addAlternative(MailBodyAlternative $mba){
        if (preg_match('`^text/(plain|html)$`', $mba->mime_type)){
            $part = array_filter($this->contents, function($mbalt) use ($mba) {
                return $mba->mime_type === $mbalt->mime_type;
            });
            
            if (count($part)){
                $part[0]->content .= $mba->content;
                $part[0]->priority = max($part[0]->priority, $mba->priority);
                return;
            }
        }
        
        $this->contents[] = $mba;
    }
    
    /**
     * Attach a file
     * 
     * @param MailAttachment $attachment
     * @param bool $related related to content
     */
    public function attach(MailAttachment $attachment) {
        $this->attachments[] = $attachment;
    }
    
    /**
     * Generate code for attachments
     * 
     * @param string $bnd mime boundary
     * 
     * @return string
     */
    private function buildAttachments($bnd, $related = false) {
        $s = '';
        foreach($this->attachments as $attachment) {
            if((bool)$attachment->cid != $related) continue;
            
            $s .= $bnd . $this->nl;
            $s .= $attachment->build();
        }
        
        return $s;
    }
    
    /**
     * Build all the mail source
     * 
     * @param bool $raw if false returns mail function compatible array, string returned otherwise
     * 
     * @return mixed
     */
    public function build($raw = false) {
        // Additionnal headers
        $headers = $this->headers;
        
        // Generate Message-Id if none
        if(!$this->msg_id) $this->msg_id = 'application-'.uniqid();
        
        // Extract "main" recipient
        $to = $raw ? null : array_shift($this->rcpt['To']);
        
        // Add recipients for each reception mode
        foreach($this->rcpt as $mode => $rcpt)
            if(count($rcpt))
                $headers[$mode] = implode(', ', $rcpt);
        
        // Add Message-Id
        $headers['Message-Id'] = $this->msg_id;
        
        // Add date
        $headers['Date'] = date('r');
        
        // Add Return-Path if any
        if($this->return_path)
            $headers['Return-Path'] = '<' . $this->return_path . '>';
        
        // Mailer identification and basic headers
        $headers['X-Mailer'] = 'YMail PHP/' . phpversion();
        $headers['MIME-Version'] = '1.0';
        
        // Add Subject header if raw mail (given to PHP's mail function separately otherwise)
        if($raw) $headers['Subject'] = $this->subject;
        
        $content = '';
        $totalParts = count($this->contents) + count($this->attachments);
        
        // Boundaries generation
        $bndid = time() . rand(999, 9999) . uniqid();
        $bnd_mixed = $this->nl.'--mixed' . $bndid;
        $bnd_alt = $this->nl.'--alternative' . $bndid;
        $bnd_rel = $this->nl.'--related' . $bndid;
        
        $mbaHTML = $this->getMBAFromMimeType('text/html');
        $mbaPlain = $this->getMBAFromMimeType('text/plain');
        
        // sort
        $toSort = array_map(function($idx, $mba){
            return array(
                'idx' => $idx,
                'mba' => $mba
            );
        }, array_keys($this->contents), array_values($this->contents));
        
        usort($toSort, function($a, $b){
           if ($a['mba']->priority !== $b['mba']->priority)
               return $a['mba']->priority - $b['mba']->priority;
            
            return $a['idx'] - $b['idx'];
        });
        
        $this->contents = array_map(function($element){
            return $element['mba'];
        }, $toSort);
         
        
        $related = count(array_filter($this->attachments, function($a) {
            return $a->cid;
        })) && $mbaHTML;
        
        $mixed = count(array_filter($this->attachments, function($a) {
            return !$a->cid;
        }));
        
        $alternative = count($this->contents) > 1;
        
        // Set headers
        if ($mixed){
            $headers['Content-Type'] = 'multipart/mixed; boundary=' . 'mixed' .$bndid ;
            
        }else if ($alternative){
            $headers['Content-Type'] = 'multipart/alternative; boundary=' . 'alternative' .$bndid ;
            
        }else if ($related){
            $headers['Content-Type'] = 'multipart/related; boundary=' . 'related' .$bndid ;
            
        }else if ($mbaPlain){
            $headers = array_merge($headers, $mbaPlain[0]->getHeaders());
            
        }else if ($mbaHTML){
            $headers = array_merge($headers, $mbaHTML[0]->getHeaders());
            
        }else if ($this->contents){
            $headers = array_merge($headers, $this->contents[0]->getHeaders());
        }
        
        // Required by rfc822
        if ($mixed || $related || $alternative){
            $content .= 'This is a multi-part message in MIME format.' . $this->nl.$this->nl;
        }
        
        if ($mixed && $alternative){
            $content .= $bnd_mixed.$this->nl;
            $content .= 'Content-Type: multipart/alternative; boundary='.'alternative' . $bndid. $this->nl.$this->nl;
        }
        
        foreach ($this->contents as $mba) {
            if ($mba->mime_type === 'text/html' && $related){
                
                if ($alternative) {
                    $content .= $bnd_alt . $this->nl;
        
                }else if ($mixed) {
                    $content .= $bnd_mixed . $this->nl;
                }
                
    
                if ($mixed || $alternative){
                    $content .= 'Content-Type: multipart/related; boundary='.'related' . $bndid. $this->nl.$this->nl;
                }
                
                $content .= $bnd_rel.$this->nl;
                $content .= $mba->getSource();
    
                $content .= $this->buildAttachments($bnd_rel, true);
                $content .= $bnd_rel.'--'.$this->nl;
            }else{
                if ($alternative) {
                    $content .= $bnd_alt . $this->nl;
                    
                }else if ($mixed) {
                    $content .= $bnd_mixed . $this->nl;
                }
                
                if ($mixed || $alternative){
                    $content .= $mba->getSource();
                    
                }else{
                    $content .= $mba->getEncodedContent();
                }
            }
        }
    
        if ($alternative) {
            $content .= $bnd_alt . '--' . $this->nl;
        }
        
        // Manage des PJ
        $content .= $this->buildAttachments($bnd_mixed, false);
        
        // Ferme le main
        if ($mixed){
            $content .= $bnd_mixed.'--';
        }
        
        // Build headers
        $headers = implode($this->nl, array_map(function($name, $value) {
            return $name.': '.$value;
        }, array_keys($headers), array_values($headers)));
        
        // Return raw if needed
        if($raw) return $headers . $this->nl.$this->nl . $content;
        
        return array('to' => $to, 'subject' => $this->subject, 'body' => $content, 'headers' => $headers);
    }
    
    
    /**
     * Get all mba from mime_type
     *
     * @param $mime_type
     * @return array
     */
    private function getMBAFromMimeType($mime_type){
        $ret = array();
        foreach ($this->contents as $mba) {
            if ($mba->mime_type === $mime_type)
                $ret[] = $mba;
        }
        return $ret;
    }
    
    
    /**
     * Sends the mail using mail function
     * 
     * @return bool success
     */
    public function send() {
        $source = $this->build();
        
        return (new Event('mail_send', $source))->trigger(function($source) {
            $safemode = ini_get('safe_mode');
            $safemode = ($safemode && !preg_match('`^off$`i', $safemode));

            if (!$safemode && $this->return_path) {
                return mail($source['to'], $source['subject'], $source['body'], $source['headers'], '-f' . $this->return_path);
            } else {
                Logger::warn('Safe mode is on, cannot set the return_path for sent email');
                return mail($source['to'], $source['subject'], $source['body'], $source['headers']);
            }
        });
    }
    
    /**
     * Spools the mail in file named from $this->id under given directory
     * 
     * @param string $dir directory to store the file in, . taken if omitted
     * 
     * @return bool success
     */
    public function spool($dir = null) {
        if(!$dir) $dir = './ymail.spool';
        
        if(!@is_dir($dir))
            if(!@mkdir($dir))
                return false;
        
        $file = preg_replace('`^(.*)/?$`', '$1/' . $this->id, $dir);
        
        Utilities::storeInFile($file, $this->build(true));
        
        return true;
    }
    
    /**
     * Sends the source for debug
     * 
     * @param string $mode target for output (stdout/- => to standard output / download => to download file prompt (in web mode))
     */
    public function debug($mode = '-') {
        $source = $this->build(true);
        if ($mode == 'download') {
            header('Content-Type: application/force-download');
            header('Content-Disposition: attachment; filename=mail_' . $this->id . '.txt');
            echo $source;
        } elseif ($mode == 'raw') {
            echo $source;
        } elseif ($mode == '-' || $mode == 'stdout') {
            print_r(nl2br(htmlspecialchars($source)));
        }
    }
}
