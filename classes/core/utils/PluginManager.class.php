<?php

/**
 *     Moment - PluginManager.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Plugin manager
 */
abstract class PluginManager {
    /**
     * Plugins path
     */
    const PATH = '/plugins/';
    
    /**
     * File under PATH in which installed plugins data is stored
     */
    const INSTALLED_PLUGINS_FILE = 'installed_plugins.json';
    
    /**
     * Plugin list
     */
    private static $plugins = null;
    
    /**
     * Path builder
     * 
     * @param string $of
     * 
     * @return string
     */
    public static function getPath($of = '') {
        return EKKO_ROOT . self::PATH . $of;
    }

    /**
     * Gather installed plugins
     *
     * @return array of Plugin child classes instances
     *
     * @throws PluginNotFoundException
     */
    public static function getInstalledPlugins() {
        if(is_null(self::$plugins)) {
            self::$plugins = array();
            
            $file = self::getPath(self::INSTALLED_PLUGINS_FILE);
            if(!file_exists($file)) return array();
            
            $metadata = JSON::decode(file_get_contents($file));
            
            foreach($metadata as $name => $data) {
                $path = self::getPath($name.'/');
                if(!is_dir($path))
                    throw new PluginNotFoundException($name);
                
                $class = $name.'Plugin';
                if(!class_exists($class)) continue;
                
                self::$plugins[$name] = array(
                    'class' => $class,
                    'path' => $path,
                    'enabled' => call_user_func($class.'::getConfig', 'enabled'),
                    'meta' => (array)$data
                );
            }
        }
        
        return self::$plugins;
    }
    
    /**
     * Save installed plugins data
     */
    private static function saveInstalledPluginsData() {
        $metadata = array();
        foreach(self::$plugins as $name => $pdfn)
            $metadata[$name] = $pdfn['meta'];
        
        $file = self::getPath(self::INSTALLED_PLUGINS_FILE);
        
        $content = json_encode($metadata, JSON_PRETTY_PRINT);
        
        Utilities::storeInFile($file, $content);
    }

    /**
     * Install plugin
     *
     * @param string $name
     *
     * @throws DBUpdaterNotPluginEntityException
     * @throws PluginNotFoundException
     */
    public static function installPlugin($name) {
        if(array_key_exists($name, self::$plugins))
            return;
        
        Logger::info('Installing plugin "'.$name.'"');
        
        $path = self::getPath($name.'/');
        $class = $name.'Plugin'; // Cannot use self::getPluginInfo as the plugin is not installed
        $file = $path.$class.'.class.php';
        
        if(!file_exists($file))
            throw new PluginNotFoundException($name);
        
        require_once $file;
        
        self::$plugins[$name] = array(
            'class' => $class,
            'path' => $path,
            'enabled' => false,
            'meta' => array(
                'installed' => time(),
                'updated' => time()
            )
        );
        
        DBUpdater::updatePluginModel($name);
        
        call_user_func($class.'::install');
        
        self::saveInstalledPluginsData();
    }
    
    /**
     * Update plugin
     * 
     * @param string $name
     */
    public static function updatePlugin($name) {
        Logger::info('Updating plugin "'.$name.'"');
        
        $info = self::getPluginInfo($name);
        
        DBUpdater::updatePluginModel($name);
        
        call_user_func($info['class'].'::update');
        
        self::$plugins[$name]['meta']['updated'] = time();
        
        self::saveInstalledPluginsData();
    }
    
    /**
     * Delete plugin
     * 
     * @param string $name
     */
    public static function uninstallPlugin($name) {
        self::getInstalledPlugins();
        
        if(!array_key_exists($name, self::$plugins))
            return; // No throw
        
        Logger::info('Uninstalling plugin "'.$name.'"');
        
        $info = self::getPluginInfo($name);
        
        call_user_func($info['class'].'::uninstall');
        
        DBUpdater::removePluginModel($name);
        
        unset(self::$plugins[$name]);
        
        self::saveInstalledPluginsData();
    }
    
    /**
     * Get enabled plugins
     * 
     * @return array of Plugin child classes instances
     */
    public static function getEnabledPlugins() {
        return array_filter(self::getInstalledPlugins(), function($p) {
            return $p['enabled'];
        });
    }

    /**
     * Get plugin infos
     *
     * @param string $name
     *
     * @return array
     *
     * @throws PluginNotFoundException
     */
    public static function getPluginInfo($name) {
        self::getInstalledPlugins();
        
        if(!array_key_exists($name, self::$plugins))
            throw new PluginNotFoundException($name);
        
        return self::$plugins[$name];
    }
    
    /**
     * Get plugin path from name
     * 
     * @param string $name
     * 
     * @return string
     */
    public static function getPluginPath($name) {
        return self::getPluginInfo($name)['path'];
    }
    
    /**
     * Check if a plugin is enabled
     * 
     * @param string $name
     * 
     * @return bool
     */
    public static function isPluginEnabled($name) {
        return self::getPluginInfo($name)['enabled'];
    }
    
    /**
     * Initialize all enabled plugins
     */
    public static function initializePlugins() {
        foreach(self::getEnabledPlugins() as $p) {
            call_user_func($p['class'].'::initialize');
        }
    }
    
    /**
     * Initialize the manager (and plugins)
     */
    public static function initialize() {
        self::initializePlugins();
        
        Event::register('before', 'rest_request', get_called_class().'::handlePluginRESTRequest');
    }

    /**
     * Plugin REST requests handler
     *
     * @param Event $event
     *
     * @throws PluginNotFoundException
     * @throws RestPluginEndpointMissingException
     * @throws RestPluginEndpointNotImplementedException
     * @throws RestPluginMethodNotImplementedException
     * @throws RestPluginNameMissingException
     */
    public static function handlePluginRESTRequest($event) {
        if(strtolower(RestRequest::getEndpoint()) != 'plugin') return;
        
        if(!count(RestRequest::getPath()))
            throw new RestPluginNameMissingException();
        
        $name = array_shift(RestRequest::getPath());
        
        if(!count(RestRequest::getPath()))
            throw new RestPluginEndpointMissingException();
        
        $endpoint = array_shift(RestRequest::getEndpoint());
        
        $class = ucfirst($name).'Plugin'.ucfirst($endpoint).'Endpoint';
        if(!class_exists($class))
            throw new RestPluginEndpointNotImplementedException($endpoint);
        
        if(!method_exists($class, RestRequest::getMethod()))
            throw new RestPluginMethodNotImplementedException(RestRequest::getMethod ());
        
        Logger::debug('Forwarding call to '.$class.'::'.RestRequest::getMethod().'() handler');
        
        $event->preventDefault();
        
        $event->result = call_user_func_array($class.'::'.RestRequest::getMethod(), RestRequest::getPath());
    }
}
