<?php
/**
 *     Moment - DatabaseSqlite.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
class DatabaseSqlite {
    
    /**
     * Check if a table exists
     *
     * @param string $table
     *
     * @return bool
     */
    public static function tableExists($table) {

        $result = DBI::admin()->prepare("SELECT name FROM sqlite_master WHERE type='table' AND name='$table';");
        
        $result = $result->fetch();
        
        // Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
        return $result !== FALSE;
    }
    
    /**
     * Create a table
     *
     * @param string $table the table name
     * @param array $definition dataMap entry
     */
    public static function createTable($table, $definition) {
        $columns = array();
        $pks = array();
        foreach($definition as $column => $dfn) {
            $columns[] = $column.' '.self::columnDefinition($dfn);
            
            if(array_key_exists('primary', $dfn) && $dfn['primary'])
                $pks[] = $column;
        }
        
        //$columns[] = 'PRIMARY KEY ('.implode(', ', $pks).')';
        
        DBI::admin()->exec('CREATE TABLE '.$table.' ('.implode(', ', $columns).')');
    }
    
    /**
     * Remove a table
     *
     * @param string $table the table name
     */
    public static function removeTable($table) {
        DBI::admin()->exec('DROP TABLE '.$table);
    }
    
    /**
     * Checks table columns format
     *
     * @param string $table table name
     * @param array $datamap
     */
    public static function checkTable($table, $datamap) {
        return;
    }
    
    /**
     * Set charset and collation
     * @param $charset
     * @param $collation
     */
    public static function setCharsetAndCollation($charset, $collation = null) {
        //TODO find how-to set collation for all connection
        DBI::prepare("PRAGMA encoding='$charset'")->execute();
        
    }
    
    /**
     * Get column definition
     *
     * @param array $definition dataMap entry
     *
     * @return string Mysql definition
     */
    private static function columnDefinition($definition) {
        $mysql = '';
        
        // Build type part
        switch($definition['type']) {
            case 'int':
            case 'uint':
                //$size = array_key_exists('size', $definition) ? $definition['size'] : 'medium';
                //if(!$size) $size = 'medium';
                //$mysql = strtoupper($size).'INT';
                //if($definition['type'] == 'uint') $mysql .= ' UNSIGNED';
                //
                $mysql = 'INTEGER ';
                if(array_key_exists('primary', $definition) && $definition['primary'])
                    $mysql .= ' PRIMARY KEY';
                
                break;
            
            case 'string':
                $size = array_key_exists('size', $definition) ? $definition['size'] : '255';
                $mysql = 'VARCHAR('.$size.')';
                break;
            
            case 'binary':
                $size = array_key_exists('size', $definition) ? $definition['size'] : '255';
                $mysql = 'VARBINARY('.$size.')';
                break;
            
            case 'blob':
                $mysql = (array_key_exists('size', $definition) ? strtoupper($definition['size']) : '') .'BLOB';
                break;
            
            case 'bool':
            case 'boolean':
                $mysql = 'TINYINT UNSIGNED';
                break;
            
            case 'enum':
                $mysql = "ENUM('".implode("','", $definition['values'])."')";
                break;
            
            case 'text':
                $mysql = 'TEXT';
                break;
            
            case 'float':
                $mysql = 'FLOAT';
                break;
            
            case 'decimal':
                $mysql = 'DECIMAL('.$definition['size'].','.$definition['precision'].')';
                break;
            
            case 'double':
                $mysql = 'DOUBLE';
                break;
            
            case 'date':
                $mysql = 'DATE';
                break;
            
            case 'datetime':
                $mysql = 'DATETIME';
                break;
            
            case 'time':
                $mysql = 'TIME';
                break;
        }
        
        // Add nullable
        //$null = 'NOT NULL';
        //if(array_key_exists('null', $definition) && $definition['null']) $null = 'NULL';
        //$mysql .= ' '.$null;
        
        // Add default
        if(array_key_exists('default', $definition) && ($definition['type'] != 'text')) {
            $mysql .= ' DEFAULT ';
            $default = $definition['default'];
            
            if(is_null($default)) {
                $mysql .= 'NULL';
            }else if(is_bool($default)) {
                $mysql .= $default ? '1' : '0';
            }else if(is_numeric($default) && in_array($definition['type'], array('int', 'uint'))) {
                $mysql .= $default;
            }else $mysql .= '"'.str_replace('"', '\\"', $default).'"';
        }
        
        // Add options
        if(array_key_exists('autoinc', $definition) && $definition['autoinc']) $mysql .= ' AUTOINCREMENT';
        
        // Return statment
        return $mysql;
    }
}