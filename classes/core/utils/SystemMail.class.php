<?php

/**
 *     Moment - SystemMail.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Class SystemMail
 */
class SystemMail extends Mail {
    /**
     * Constructor
     *
     * @param mixed $translation_or_id Translation object or translation identifier
     * @param array $vars
     *
     * @throws BadFormatException
     * @throws DetailedException
     */
    public function __construct($translation_or_id = null, $vars = array()) {
        $content = null;
        if(is_object($translation_or_id) && $translation_or_id instanceof Translation) {
            $content = $translation_or_id;
            
        } else if(is_string($translation_or_id)) {
            $content = Lang::translateEmail($translation_or_id);
            
        } else if($translation_or_id) {
            throw new BadFormatException($translation_or_id, 'translation_or_id');
        }
        
        if (count($vars))
            $content = $content->replace ($vars);
        
        // Cast content to string if translation object
        $subject = '';
        if($content) {
            $subject = $content->subject->out();
            if(is_array($subject)) {
                $subject = array_filter($subject);
                $subject = $subject['prefix'].' '.array_pop($subject);
            }
        }
        
        // Trigger basic mail build
        parent::__construct(null, $subject);
        
        // Write content if a translation object was given
        if($content) {
            $this->writePlain($content->plain);
            $this->writeHTML($content->html);
        }
        
        $admins = Config::get('admin_email');
        if(!is_array($admins))
            $admins = array_filter(array_map('trim', explode(',', $admins)));
        
        foreach($admins as $email) $this->to($email);
    }

    /**
     * Quick translated sending
     *
     * @param mixed $translation_or_id Translation object or translation identifier
     * @param array $vars
     *
     * @throws BadFormatException
     */
    public static function quickSend($translation_or_id, $vars = array()) {
        if(!$translation_or_id)
            throw new BadFormatException($translation_or_id, 'translation_or_id');
        
        (new static($translation_or_id, $vars))->send();
    }
}
