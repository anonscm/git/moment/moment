<?php

/**
 *     Moment - Utilities.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Utility functions holder
 */
class Utilities {
    /**
     * CSRF token
     */
    private static $security_token = null;
    
    /**
     * UIDs cache
     */
    private static $uids = array(
        'instance' => null,
        'running' => null,
        'user' => null
    );
    
    /**
     * Generate a unique ID to be used as token
     * 
     * @param callable $unicity_checker callback used to check for uid unicity (takes uid as sole argument, returns bool telling if uid is unique), null if check not needed
     * @param int $max_tries maximum number of tries before giving up and throwing
     * 
     * @return string uid
     * 
     * @throws UtilitiesUidGeneratorBadUnicityCheckerException
     * @throws UtilitiesUidGeneratorTriedTooMuchException
     */
    public static function generateUID($unicity_checker = null, $max_tries = 1000) {
        // Do we need to generate a unicity-checked random UID ?
        if($unicity_checker) {
            // Fail if checker is not a callable
            if(!is_callable($unicity_checker))
                throw new UtilitiesUidGeneratorBadUnicityCheckerException();
            
            // Try to generate until uniquely-checked or max tries reached
            $tries = 0;
            do {
                $uid = self::generateUID();
                $tries++;
            } while(!call_user_func($unicity_checker, $uid) && ($tries <= $max_tries));
            
            // Fail if max tries reached
            if($tries > $max_tries)
                throw new UtilitiesUidGeneratorTriedTooMuchException($tries);
            
            return $uid;
        }
        
        // Generate a simple random UID
        
        $rnd = self::generateRandomHexString();
        
        return substr($rnd, 0, 8).'-'.substr($rnd, 8, 4).'-'.substr($rnd, 12, 4).'-'.substr($rnd, 16, 4).'-'.substr($rnd, 20, 12);
    }
    
    /**
     * Validates unique ID format
     * 
     * @param string $uid
     * 
     * @return bool
     */
    public static function isValidUID($uid) {
        if(!is_string($uid))
            return false;

        return (bool) preg_match('/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/i', $uid);
    }
    
    /**
     * Generate (pseudo) (super-)random hex string
     *
     * @param bool $nearly Simple random is enough
     * 
     * @return string
     */
    public static function generateRandomHexString($nearly = false) {
        // Random length
        $len = mt_rand(16, 32);
        
        // Random data
        $rnd = '';
        for($i=0; $i<$len; $i++) $rnd .= sprintf('%04d', mt_rand(0, 9999));
        
        // No need for an super-random, just hash
        if($nearly) return hash('sha1', $rnd);
        
        // Need for an super-random
        
        // Get secret, generate it if not found
        $sfile = EKKO_ROOT.'/tmp/instance.secret';
        if(file_exists($sfile)) {
            $ctn = array_filter(array_map('trim', explode("\n", file_get_contents($sfile))), function($line) {
                return substr($line, 0, 1) != '#';
            });
            
            $secret = array_shift($ctn);
        } else {
            $secret = self::generateRandomHexString(true);
            
            self::storeInFile($sfile, '# Automatically generated'."\n".$secret);
        }
        
        // return hmac signature of random data with secret => super-random !
        return hash_hmac('sha1', $rnd, $secret);
    }
    
    /**
     * Get instance uid
     * 
     * @return string
     */
    public static function instanceUID() {
        if(!is_null(self::$uids['instance']))
            return self::$uids['instance'];
        
        // Get uid from file, generate it if not found
        $sfile = EKKO_ROOT.'/tmp/instance.uid';
        if(file_exists($sfile)) {
            $ctn = array_filter(array_map('trim', explode("\n", file_get_contents($sfile))), function($line) {
                return substr($line, 0, 1) != '#';
            });
            
            $uid = array_shift($ctn);
        } else {
            $uid = self::generateRandomHexString(true);
            
            self::storeInFile($sfile, '# Automatically generated'."\n".$uid);
        }
        
        self::$uids['instance'] = $uid;
        
        return $uid;
    }
    
    /**
     * Get running instance uid
     * 
     * @return string
     */
    public static function runningInstanceUID() {
        if(!is_null(self::$uids['running']))
            return self::$uids['running'];
        
        $uid = substr(self::instanceUID(), -8).'-'.filemtime(EKKO_ROOT.'/config/config.php');
        
        self::$uids['running'] = $uid;
        return $uid;
    }

    /**
     * Get user specific instance uid
     * @return string
     * @throws AuthUserNotAllowedException
     */
    public static function userUID() {
        if(is_null(self::$uids['user'])) {
            $uid = hash_hmac('sha1', Auth::isAuthenticated() ? Auth::user()->id : 'anonymous', self::instanceUID());
            
            self::$uids['user'] = substr($uid, -12);
        }
        
        return self::$uids['user'];
    }
    
    /**
     * Format a date according to configuration
     * 
     * @param integer $timestamp php timestamp to format to date or null to use current date
     * @param bool $withTime
     * @param bool $dateFormat
     * @param bool $timezone
     * 
     * @return string formatted date
     */
    public static function formatDate($timestamp = null, $withTime = false, $dateFormat = null, $timezone = null) {
        if (!$dateFormat){
            $lid = $withTime ? 'datetime_format' : 'date_format';
            $dateFormat = (string) Lang::tr($lid);
            if ($dateFormat == '{date_format}')
                $dateFormat = 'Y-m-d';
            if ($dateFormat == '{datetime_format}')
                $dateFormat = 'Y-m-d H:i:s';
        }
        
        if (!is_null($timestamp))
            $timestamp = '@'.$timestamp;
        
        $date = new DateTime($timestamp, new DateTimeZone('UTC'));
        if (!is_null($timezone))
            $date->setTimezone(new DateTimeZone($timezone));
        
        return $date->format($dateFormat);
    }

    /**
     * Format a time according to configuration
     *
     * @param integer $time in seconds
     * @param bool $relative Time is a duration instead of a fixed hour
     *
     * @return string formatted time
     */
    public static function formatTime($time = null, $relative = false) {
        if(is_null($time)) $time = time();
        
        if(!$relative)
            return static::formatDate($time, true, (string)Lang::tr('time_format'));
        
        // Get time format
        $time %= 24 * 3600;
        $time_format = Lang::tr('relative_time_format');
        if($time_format == '{time_format}')
            $time_format = '{h:H\h} {i:i\m\i\n} {s:s\s}';
        
        // convert time to time parts
        $bits = array();
        $bits['h'] = floor($time / 3600);
        $time %= 3600;
        $bits['i'] = floor($time / 60);
        $bits['s'] = $time % 60;
        
        // Process and replace bits in format string
        foreach($bits as $k => $v) {
            if($v) {
                $time_format = preg_replace_callback('`\{'.$k.':([^}]+)\}`', function($m) use($k, $v) {
                    return preg_replace_callback('`(?<!\\\\)'.$k.'`i', function() use($v) {
                        return sprintf('%02d', $v);
                    }, $m[1]);
                }, $time_format);
            } else { // Remove part if zero
                $time_format = preg_replace('`\{'.$k.':[^}]+\}`', '', $time_format);
            }
        }
        
        // Remove backslashes
        $time_format = str_replace('\\', '', $time_format);
        
        // Strip leading 0s
        $time_format = preg_replace('`^[\s0]+`', '', $time_format);
        
        return trim($time_format);
    }
    
    /**
     * Turn PHP defined size (ini files) to bytes
     * 
     * @param string $size the size to analyse
     * 
     * @return integer the size in bytes
     * 
     * @throws UtilitiesBadSizeFormatException
     */
    public static function sizeToBytes($size) {
        // Check format
        if(!preg_match('`^([0-9]+)([ptgmk])?$`i', trim($size), $parts))
            throw new UtilitiesBadSizeFormatException($size);
        
        $size = (int)$parts[1];
        
        if(count($parts) > 2) switch(strtoupper($parts[2])) {
            case 'P': $size *= 1024;
            case 'T': $size *= 1024;
            case 'G': $size *= 1024;
            case 'M': $size *= 1024;
            case 'K': $size *= 1024;
        }
        return $size;
    }

    /**
     * Format size
     *
     * @param int $bytes
     * @param int $precision
     *
     * @return string
     */
    public static function formatBytes($bytes, $precision = 1) {
        // Default
        if(!$precision || !is_numeric($precision))
            $precision = 2;
        
        // Variants
        $unit = Lang::tr('size_unit')->out();
        if($unit == '{size_unit}') $unit = 'b';
        
        $multipliers = array('', 'k', 'M', 'G', 'T');
        
        // Compute multiplier
        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($multipliers) - 1);
        
        $bytes /= pow(1024, $pow);
        
        return round($bytes, $precision).' '.$multipliers[$pow].$unit;
    }
    
    /**
     * Get remote client IP (v4 or v6)
     * 
     * @return string
     */
    public static function getClientIP(){
        return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
    }
    
    /**
     * Replace illegal chars with _ character in supplied file names
     * 
     * @param string $filename
     * 
     * @return string
     */
    public static function sanitizeFilename($filename) {
        //return preg_replace('`[^a-z0-9_\-\. ]`i', '_', $filename);
        return preg_replace('`^\.`', '_', (string)$filename);
    }
    
    /**
     * Sanitize input against encoding variations and (a bit) against html injection
     * 
     * @param mixed $input
     * 
     * @return mixed
     */
    public static function sanitizeInput($input) {
        if(is_array($input)) {
            foreach($input as $k => $v) {
                $nk = preg_replace('`[^a-z0-9\._-]`i', '', $k);
                if($k !== $nk) unset($input[$k]);
                $input[$nk] = self::sanitizeInput($v);
            }
            
            return $input;
        }
        
        if(
            is_numeric($input)
            || is_bool($input)
            || is_null($input)
            || is_object($input) // How can that be ?
        )
            return $input;
        
        if(is_string($input)) {
            // Convert to UTF-8
            $input = iconv(mb_detect_encoding($input, mb_detect_order(), true), 'UTF-8', $input);
            
            // Render potential tags useless by putting a space immediatelly after < which does not already have one
            $input = html_entity_decode($input, ENT_QUOTES, 'UTF-8');
            $input = preg_replace('`<([^\s])`', '< $1', $input);
            
            return $input;
        }
        
        // Still here ? Should not ...
        return null;
    }
    
    /**
     * Sanitize output
     * 
     * @param string $output
     * 
     * @return string
     */
    public static function sanitizeOutput($output) {
        return htmlentities($output, ENT_QUOTES, 'UTF-8');
    }
    
    /**
     * Check if HTTPS is in use
     * 
     * @return bool
     */
    public static function httpsInUse() {
        return isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on';
    }
    
    /**
     * Get the security token, refreshing it in the process if needed
     * 
     * @return string
     */
    public static function getSecurityToken() {
        if(!is_null(self::$security_token))
            return self::$security_token['value'];
        
        // Fetch existing token
        $token = array_key_exists('security_token', $_SESSION) ? $_SESSION['security_token'] : null;
        
        // Old token style, cancel it
        if(!is_array($token)) $token = null;
        
        if(!$token) { // First access
            $token = array(
                'value' => Utilities::generateUID(),
                'valid_until' => time() + 3600,
                'old_value' => null
            );
        
        } else if($token['valid_until'] < time()) { // Must renew
            $token['old_value'] = $token['value'];
            $token['value'] = Utilities::generateUID();
            $token['valid_until'] = time() + 3600;
        
        } else { // Still valid, scrape old value from any previous changes
            $token['old_value'] = null;
        }
        
        if($token['old_value']) // Send new value as header if changed
            header('X-Application-Security-Token: '.$token['value']);
        
        // Store in session
        $_SESSION['security_token'] = $token;
        
        // Cache in class
        self::$security_token = $token;
        
        return $token['value'];
    }
    
    /**
     * Check given security token against stored one
     * 
     * @param string $token_to_check
     * 
     * @return bool
     */
    public static function checkSecurityToken($token_to_check) {
        $token = self::getSecurityToken();
        
        // Direct match
        if($token_to_check === $token) return true;
        
        // If no direct match and no previous token value, no match
        if(!self::$security_token['old_value']) return false;
        
        // Previous value matches
        return $token_to_check === self::$security_token['old_value'];
    }
    
    /**
     * Validate value
     * 
     * @param mixed $value
     * @param mixed $validator
     * @param string $context
     * 
     * @return boolean
     *
     * @throws UtilitiesBadValidatorException
     */
    public static function validateValue(&$value, $validator, $context = null) {
        if(is_string($validator)) {
            $pass = false;
            foreach(array_map('trim', explode('|', $validator)) as $check) {
                if(substr($check, 0, 3) == 'is_')
                    $check = substr($check, 3);
                
                switch($check) {
                    // Standard types
                    case 'not_empty':   $pass |= !empty($value);        break;
                    case 'null':        $pass |= ($value === null);     break;
                    case 'bool':        $pass |= is_bool($value);       break;
                    case 'string':      $pass |= is_string($value);     break;
                    case 'array':       $pass |= is_array($value);      break;
                    case 'callable':    $pass |= is_callable($value);   break;
                    
                    case 'int':
                        if (preg_match('`^-?(0|[1-9][0-9]*)$`', $value)){
                            $value = (int) $value;
                            $pass = true;
                        }
                        break;
                        
                    case 'positive_int':
                        if (preg_match('`^(0|[1-9][0-9]*)$`', $value)){
                            $value = (int) $value;
                            $pass = true;
                        }
                        break;
                        
                    case 'negative_int':
                        if (preg_match('`^-(0|[1-9][0-9]*)$`', $value)){
                            $value = (int) $value;
                            $pass = true;
                        }
                        break;
                        
                    case 'float':
                        if (preg_match('`^-?(0|[1-9][0-9]*)(\.[0-9]+)?$`', $value)){
                            $value = (float) $value;
                            $pass = true;
                        }
                        break;
                    
                    // Specific types
                    case 'email':       $pass |= filter_var($value, FILTER_VALIDATE_EMAIL); break;
                    
                    default: throw new UtilitiesBadValidatorException($check);
                }
                    
                if($pass) break; // Stop on first true check
            }
            
            return $pass;
        }
        
        if(is_callable($validator)) {
            try {
                call_user_func($validator, $value, $context);
                return true;
                
            } catch(Exception $e) {
                return false;
            }
        }
        
        if(is_array($validator)) {
            foreach($validator as $sub)
                if(!self::validateValue($value, $sub, $context))
                    return false;
            
            return true;
        }
        
        throw new UtilitiesBadValidatorException(gettype($validator));
    }
    
    /**
     * Get current URL
     * 
     * @return string
     */
    public static function getCurrentURL() {
        $base = preg_replace('`^(https?://[^/]+).*$`', '$1', Config::get('application_url'));
        $uri = $_SERVER['REQUEST_URI'];
        $query = $_SERVER['QUERY_STRING'];
        
        return $base.$uri.$query;
    }
    
    /**
     * Ensure full url if needed
     * 
     * @param string $url
     * @param bool $need_full
     * 
     * @return string
     */
    public static function ensureFullURL($url, $need_full = true) {
        if(!$need_full || preg_match('`^https?://[^/]+`', $url)) return $url;
        
        $base = Config::get('application_url');
        if(substr($url, 0, 1) == '/') // Absolute url
            $base = preg_replace('`^(https?://[^/]+).*$`', '$1', $base);
        
        return $base.$url;
    }
    
    /**
     * Ensure that a file is owned by the application
     * 
     * @param string $file
     * 
     * @throws UtilitiesCannotChangeFileOwnerException
     * @throws UtilitiesCannotChangeFileGroupException
     */
    public static function ensureFileOwnership($file) {
        $application_uid = Config::get('application_uid');
        if(!$application_uid) return; // Should not happen if config is all right
        
        if(fileowner($file) == $application_uid) return;
        
        if(!chown($file, $application_uid))
            throw new UtilitiesCannotChangeFileOwnerException($file, $application_uid);
        
        $application_gid = Config::get('application_gid');
        if($application_gid)
            if(!chgrp($file, $application_gid))
                throw new UtilitiesCannotChangeFileGroupException($file, $application_gid);
    }
    
    /**
     * Read file contents
     * 
     * @param string $file
     * 
     * @return string
     * 
     * @throws CoreCannotReadFileException
     */
    public static function readFile($file) {
        $ctn = file_get_contents($file);
        
        if($ctn === false)
            throw new CoreCannotReadFileException($file);
        
        return $ctn;
    }
    
    /**
     * Put content in file
     * 
     * @param string $file
     * @param mixed $content
     * 
     * @throws CoreCannotWriteFileException
     */
    public static function storeInFile($file, $content) {
        if(!file_put_contents($file, $content))
            throw new CoreCannotWriteFileException($file);
        
        self::ensureFileOwnership($file);
    }
    
    /**
     * Append content to file
     * 
     * @param string $file
     * @param mixed $content
     * 
     * @throws CoreCannotWriteFileException
     */
    public static function appendToFile($file, $content) {
        $fh = fopen($file, 'a');
        if(!$fh)
            throw new CoreCannotWriteFileException($file);
        
        fwrite($fh, $content);
        fclose($fh);
        
        self::ensureFileOwnership($file);
    }
    
    /**
     * Remove file
     * 
     * @param string $file
     * 
     * @throws CoreCannotDeleteFileException
     */
    public static function removeFile($file) {
        if(!file_exists($file)) return;
        
        if(!unlink($file))
            throw new CoreCannotDeleteFileException($file);
    }
}
