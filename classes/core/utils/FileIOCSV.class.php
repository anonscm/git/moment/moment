<?php

/**
 *     Moment - FileIOCSV.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * CSV file reader / writer
 */
class FileIOCSV {
    /**
     * @var FileIO sub FileIO
     */
    private $handle = null;
    
    /**
     * @var array headers
     */
    private $headers = array();
    
    /**
     * @var string delimiter
     */
    private $delimiter = null;
    
    /**
     * @var string enclosure
     */
    private $enclosure = null;
    
    /**
     * @var string escape
     */
    private $escape = null;
    
    /**
     * Constructor
     * 
     * @param string $path
     * @param boolean $writing
     * @param mixed $headers boolean telling wether there is headers if reading or set of headers for writing
     * @param string $delimiter @see str_getcsv
     * @param string $enclosure @see str_getcsv
     * @param string $escape @see str_getcsv
     */
    public function __construct($path, $writing = false, $headers = false, $delimiter = ',', $enclosure = '"', $escape = '\\') {
        $this->delimiter = $delimiter;
        $this->enclosure = $enclosure;
        $this->escape = $escape;
        
        $this->handle = new FileIO($path, $writing ? 'wb' : 'rb');
        
        if($writing && is_array($headers)) {
            $this->headers = $headers;
            
            $this->write(array_values($headers));
        }
        
        if(!$writing && $headers)
            $this->headers = $this->read();
    }

    /**
     * Close handle
     */
    public function close() {
        $this->handle->close();
    }

    /**
     * Destructor
     */
    public function __destuct() {
        $this->close();
    }

    /**
     * Read line
     * 
     * @return array
     */
    public function read() {
        $line = $this->handle->gets();
        if(!$line) return null;
        
        $data = str_getcsv($line, $this->delimiter, $this->enclosure, $this->escape);
        
        return $data;
    }
    
    /**
     * Write line
     * 
     * @param array $data
     */
    public function write($data) {
        $enc = $this->enclosure;
        $esc = $this->escape;
        
        $line = implode($this->delimiter, array_map(function($v) use($enc, $esc) {
            return $enc.str_replace($enc, $esc.$enc, $v).$enc;
        }, $data));
        
        $this->handle->write($line."\n");
    }
    
    /**
     * Read whole file
     * 
     * @param string $path
     * @param mixed $headers boolean telling wether there is headers
     * @param string $delimiter @see str_getcsv
     * @param string $enclosure @see str_getcsv
     * @param string $escape @see str_getcsv
     * 
     * @return object
     */
    public static function readWhole($path, $headers = false, $delimiter = ',', $enclosure = '"', $escape = '\\') {
        $csv = new stdClass();
        
        $input = new self($path, false, $headers, $delimiter, $enclosure, $escape);
        
        $csv->headers = $input->headers;
        
        $csv->rows = array();
        while($row = $input->read())
            $csv->rows[] = $row;
        
        $input->close();
        
        return $csv;
    }
    
    /**
     * Write whole file
     * 
     * @param string $path
     * @param array $data
     * @param mixed $headers set of headers for writing
     * @param string $delimiter @see str_getcsv
     * @param string $enclosure @see str_getcsv
     * @param string $escape @see str_getcsv
     */
    public static function writeWhole($path, array $data, $headers = false, $delimiter = ',', $enclosure = '"', $escape = '\\') {
        $output = new self($path, true, $headers, $delimiter, $enclosure, $escape);
        
        foreach($data as $row)
            $output->write($row);
        
        $output->close();
    }

    /**
     * Getter
     *
     * @param string $property
     *
     * @return mixed
     *
     * @throws PropertyAccessException
     */
    public function __get($property) {
        if($property == 'headers') return $this->headers;
        
        throw new PropertyAccessException($this, $property);
    }
}
