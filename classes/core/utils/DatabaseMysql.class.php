<?php

/**
 *     Moment - DatabaseMysql.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Database managing
 */
class DatabaseMysql {
    /**
     * Check if a table exists
     *
     * @param string $table
     *
     * @return bool
     */
    public static function tableExists($table) {
        $s = DBI::admin()->prepare('SHOW TABLES LIKE :table');
        $s->execute(array(':table' => $table));
        return (bool)$s->fetch();
    }
    
    /**
     * Create a table
     *
     * @param string $table the table name
     * @param array $definition dataMap entry
     */
    public static function createTable($table, $definition) {
        $columns = array();
        $pks = array();
        foreach($definition as $column => $dfn) {
            $columns[] = $column.' '.self::columnDefinition($dfn);
            
            if(array_key_exists('primary', $dfn) && $dfn['primary'])
                $pks[] = $column;
        }
        
        $columns[] = 'PRIMARY KEY ('.implode(', ', $pks).')';
        
        DBI::admin()->exec('CREATE TABLE '.$table.' ('.implode(', ', $columns).')');
    }
    
    /**
     * Remove a table
     *
     * @param string $table the table name
     */
    public static function removeTable($table) {
        DBI::admin()->exec('DROP TABLE '.$table);
    }
    
    /**
     * Checks table columns format
     *
     * @param string $table table name
     * @param array $datamap
     */
    public static function checkTable($table, $datamap) {
        // Gather current columns and primary keys
        $current_columns = array_map(function($c) {
            return $c['Field'];
        }, DBI::admin()->query('SHOW COLUMNS FROM '.$table)->fetchAll());
        
        $current_pks = array_map(function($c) {
            return $c['Column_name'];
        }, DBI::admin()->query('SHOW KEYS FROM '.$table.' WHERE Key_name = "PRIMARY"')->fetchAll());
        
        $pksAttributes  = array();
        foreach ($current_pks as $pk){
            // Get current definition
            $pksAttributes[$pk] = DBI::admin()->query('SHOW COLUMNS FROM '.$table.' LIKE "'.$pk.'"')->fetch();
        }
        
        // Get UNIQUE column definition
        $current_uniques = array();
        foreach (DBI::admin()->query('SHOW KEYS FROM '.$table.' WHERE Key_name != "PRIMARY"')->fetchAll() as $c){
            if (!preg_match('`^unique___`', $c['Key_name'])) continue;
            $current_uniques[$c['Key_name']][] = $c['Column_name'];
        }
        
        // Get Indexes on table
        $current_indexes = array();
        foreach (DBI::admin()->query('SHOW INDEX FROM '.$table.' WHERE Key_name != "PRIMARY"')->fetchAll() as $c){
            if (!preg_match('`^index___`', $c['Key_name'])) continue;
            $current_indexes[$c['Key_name']][] = $c['Column_name'];
        }
        
        // Analyse changes
        $pks = array();
        $pks_changes = false;
        foreach($datamap as $column => $dfn) {
            if(array_key_exists('primary', $dfn) && $dfn['primary']) {
                $pks[] = $column;
                
                if(!in_array($column, $current_pks))
                    $pks_changes = true; // pk added
            }
        }
        
        foreach($current_pks as $column){
            if( !array_key_exists($column, $datamap) ||
                !array_key_exists('primary', $datamap[$column]) ||
                !$datamap[$column]['primary']){
                
                $pks_changes = true; // pk removed
                // Check column 
                
                // Check autoinc
                if (preg_match('`auto_increment`', $pksAttributes[$column]['Extra'])){
                    // Drop autoinc
                    DBI::admin()->exec('ALTER TABLE '.$table.' DROP '.$column);
                    // No more key to drop
                    $pks_changes = false;
                    $current_columns = array_filter($current_columns, function($c) use ($column) {
                        return $c != $column;
                    });
                }
                
            }
        }
        
        if($pks_changes && count($current_pks)){ // Changes on pks, drop all current ones for later rebuild
            $query = 'ALTER TABLE '.$table.' DROP PRIMARY KEY';
            foreach ($pksAttributes as $pk => $attributes){
                if (preg_match('`auto_increment`',$attributes['Extra'])){
                    $query .= ', CHANGE '.$pk.' '.$pk.' '.self::columnDefinition($datamap[$pk]);
                }
            }
            DBI::admin()->exec($query);
        }
        
        
        // Get UNIQUE keys to add
        $uniques = array();
        $uniques_changes = array();
        foreach($datamap as $column => $dfn) {
            if(array_key_exists('unique', $dfn) && $dfn['unique']){
                if (preg_match('`^[a-z][a-z0-9_-]*`i', $dfn['unique']))
                    $key_name = 'unique'.'___'.$table.'___'.$dfn['unique'];
                else $key_name = 'unique'.'___'.$table.'___'.$column;
                
                $uniques[$key_name][] = $column;
                
                if (!array_key_exists($key_name, $current_uniques))
                    $uniques_changes[] = $key_name; // Not unique set
                
                else if (!in_array($column, $current_uniques[$key_name]))
                    $uniques_changes[] = $key_name; // Not unique set
            }
        }
        
        // Get UNIQUE keys to drop
        foreach($current_uniques as $index => $columns){
            foreach ($columns as $column){
                if( !array_key_exists($column, $datamap) ||
                    !array_key_exists('unique', $datamap[$column]) ||
                    !$datamap[$column]['unique']){
                    
                    $uniques_changes[] = $index; // UNIQUE to be managed*
                    
                }else{
                    if (preg_match('`^[a-z][a-z0-9_-]*`i', $datamap[$column]['unique']))
                        $key_name = 'unique'.'___'.$table.'___'.$datamap[$column]['unique'];
                    else $key_name = 'unique'.'___'.$table.'___'.$column;
                    
                    if ($index !== $key_name)
                        $uniques_changes[] = $index; // UNIQUE to be managed*
                }
            }
        }
        $uniques_changes = array_unique($uniques_changes);
        
        if(count($uniques_changes)) // Changes on key, drop all current ones for later rebuild
            foreach ($current_uniques as $index => $columns)
                DBI::admin()->exec('ALTER TABLE '.$table.' DROP KEY `'.$index.'`');
        
        
        // Get Indexes to add
        $indexes = array();
        $indexes_changes = array();
        foreach($datamap as $column => $dfn) {
            if(array_key_exists('index', $dfn) && $dfn['index']){
                if (preg_match('`^[a-z][a-z0-9_-]*`i', $dfn['index']))
                    $index_name = 'index'.'___'.$table.'___'.$dfn['index'];
                else $index_name = 'index'.'___'.$table.'___'.$column;
                
                
                // Init index length
                $index_length = '';
                
                if(array_key_exists('index_length', $dfn) && $dfn['index_length']){
                    if (is_int($dfn['index_length'])) {
                        $index_length = '('.$dfn['index_length'].')';
                    }
                }
                
                $indexes[$index_name][] = $column.$index_length;
                
                if (!array_key_exists($index_name, $current_indexes))
                    $indexes_changes[] = $index_name; // Not unique set
                
                else if (!in_array($column, $current_indexes[$index_name]))
                    $indexes_changes[] = $index_name; // Not unique set
            }
        }
        
        // Get Indexes to drop
        foreach($current_indexes as $index => $columns){
            foreach ($columns as $column){
                if( !array_key_exists($column, $datamap) ||
                    !array_key_exists('index', $datamap[$column]) ||
                    !$datamap[$column]['index']){
                    
                    $indexes_changes[] = $index; // UNIQUE to be managed*
                    
                }else{
                    if (preg_match('`^[a-z][a-z0-9_-]*`i', $datamap[$column]['index']))
                        $key_name = 'index'.'___'.$table.'___'.$datamap[$column]['index'];
                    else $key_name = 'index'.'___'.$table.'___'.$column;
                    
                    if ($index !== $key_name)
                        $indexes_changes[] = $index; // UNIQUE to be managed*
                }
            }
        }
        $indexes_changes = array_unique($indexes_changes);
        
        if(count($indexes_changes)){ // Changes on indexes, drop all current ones for later rebuild
            foreach ($current_indexes as $index => $columns) {
                DBI::admin()->exec('DROP INDEX `'.$index.'` ON '.$table. ' ' );
            }
        }
        
        // Check required columns
        foreach($datamap as $column => $dfn) {
            if(in_array($column, $current_columns)) { // Column exists, check it
                self::checkColumn($table, $column, $dfn);
                
            } else {
                DBI::admin()->exec('ALTER TABLE '.$table.' ADD '.$column.' '.self::columnDefinition($dfn));
            }
        }
        
        // Drop unneeded columns
        foreach($current_columns as $column)
            if(!array_key_exists($column, $datamap))
                DBI::admin()->exec('ALTER TABLE '.$table.' DROP '.$column);
        
        // Rebuild pk if needed
        if($pks_changes && count($pks))
            DBI::admin()->exec('ALTER TABLE '.$table.' ADD PRIMARY KEY ('.implode(', ', $pks).')');
        
        // Rebuild unique if needed
        if($uniques_changes){
            foreach ($uniques as $index => $columns){
                DBI::admin()->exec('ALTER TABLE '.$table.' ADD UNIQUE KEY `'.$index.'` ('.implode(', ', $columns).')');
            }
        }
        
        // Rebuild index if needed
        if($indexes_changes){
            foreach ($indexes as $index => $columns){
                $a = 'CREATE INDEX `'.$index.'` ON '.$table.' ('.implode(', ', $columns).')';
                DBI::admin()->exec($a);
            }
        }
        
    }
    
    /**
     * Check table column, update if needed
     *
     * @param string $table table name
     * @param string $column column name
     * @param string $definition column definition
     */
    private static function checkColumn($table, $column, $definition) {
        $up = false;
        
        // Get current definition
        $column_dfn = DBI::admin()->query('SHOW COLUMNS FROM '.$table.' LIKE "'.$column.'"')->fetch();
        
        // Build type matcher
        $typematcher = '';
        switch($definition['type']) {
            case 'int':
            case 'uint':
                $size = array_key_exists('size', $definition) ? $definition['size'] : 'medium';
                if(!$size) $size = 'medium';
                $typematcher = $size.'int(?:\([0-9]+\))';
                if($definition['type'] == 'uint') $typematcher .= ' unsigned';
                break;
            
            case 'string':
                $typematcher = 'varchar\('.$definition['size'].'\)';
                break;
            
            case 'blob':
                $typematcher = (array_key_exists('size', $definition) ? $definition['size'] : ''). 'blob';
                break;
            
            case 'binary':
                $typematcher = 'varbinary\('.$definition['size'].'\)';
                break;
            
            case 'bool':
            case 'boolean':
                $typematcher = 'tinyint\([0-9]+\) unsigned';
                break;
            
            case 'enum':
                $typematcher = "enum\('".implode("','", $definition['values'])."'\)";
                break;
            
            case 'text':
                $typematcher = 'text';
                break;
            
            case 'float':
                $typematcher = 'float';
                break;
            
            case 'decimal':
                $typematcher = 'decimal\(\s*[1-9][0-9]*\s*,\s*[1-9][0-9]*\s*\)';
                break;
            
            case 'double':
                $typematcher = 'double';
                break;
            
            case 'date':
                $typematcher = 'date';
                break;
            
            case 'datetime':
                $typematcher = 'datetime';
                break;
            
            case 'time':
                $typematcher = 'time';
                break;
        }
        
        // Check type
        if(!preg_match('`'.$typematcher.'`i', $column_dfn['Type'])) {
            Logger::info($column.' type does not match '.$typematcher);
            $up = true;
        }
        
        // Check default
        if(array_key_exists('default', $definition) && ($definition['type'] != 'text')) {
            if(is_null($definition['default'])) {
                if(!is_null($column_dfn['Default'])) {
                    Logger::info($column.' default is not null');
                    $up = true;
                }
            }else if(is_bool($definition['default'])) {
                if((bool)$column_dfn['Default'] != $definition['default']) {
                    Logger::info($column.' default is not '.($definition['default'] ? '1' : '0'));
                    $up = true;
                }
            }else if($column_dfn['Default'] != $definition['default']) {
                Logger::info($column.' default is not "'.$definition['default'].'"');
                $up = true;
            }
        }
        
        // Options defaults
        foreach(array('null', 'unique', 'autoinc') as $k) if(!array_key_exists($k, $definition)) $definition[$k] = false;
        
        // Check nullable
        $is_null = ($column_dfn['Null'] == 'YES');
        if($definition['null'] && !$is_null) {
            Logger::info($column.' is not nullable');
            $up = true;
        } else if(!$definition['null'] && $is_null) {
            Logger::info($column.' should not be nullable');
            $up = true;
        }
        
        // Check autoinc
        $is_autoinc = preg_match('`auto_increment`', $column_dfn['Extra']);
        if($definition['autoinc'] && !$is_autoinc) {
            Logger::info($column.' is not autoinc');
            $up = true;
        } else if(!$definition['autoinc'] && $is_autoinc) {
            Logger::info($column.' should not be autoinc');
            $up = true;
        }
        
        // Update needed ?
        if($up) DBI::admin()->exec('ALTER TABLE '.$table.' MODIFY '.$column.' '.self::columnDefinition($definition));
    }
    
    /**
     * Get column definition
     *
     * @param array $definition dataMap entry
     *
     * @return string Mysql definition
     */
    private static function columnDefinition($definition) {
        $mysql = '';
        
        // Build type part
        switch($definition['type']) {
            case 'int':
            case 'uint':
                $size = array_key_exists('size', $definition) ? $definition['size'] : 'medium';
                if(!$size) $size = 'medium';
                $mysql = strtoupper($size).'INT';
                if($definition['type'] == 'uint') $mysql .= ' UNSIGNED';
                break;
            
            case 'string':
                $size = array_key_exists('size', $definition) ? $definition['size'] : '255';
                $mysql = 'VARCHAR('.$size.')';
                break;
            
            case 'binary':
                $size = array_key_exists('size', $definition) ? $definition['size'] : '255';
                $mysql = 'VARBINARY('.$size.')';
                break;
            
            case 'blob':
                $mysql = (array_key_exists('size', $definition) ? strtoupper($definition['size']) : '') .'BLOB';
                break;
            
            case 'bool':
            case 'boolean':
                $mysql = 'TINYINT UNSIGNED';
                break;
            
            case 'enum':
                $mysql = "ENUM('".implode("','", $definition['values'])."')";
                break;
            
            case 'text':
                $mysql = 'TEXT';
                break;
            
            case 'float':
                $mysql = 'FLOAT';
                break;
            
            case 'decimal':
                $mysql = 'DECIMAL('.$definition['size'].','.$definition['precision'].')';
                break;
            
            case 'double':
                $mysql = 'DOUBLE';
                break;
            
            case 'date':
                $mysql = 'DATE';
                break;
            
            case 'datetime':
                $mysql = 'DATETIME';
                break;
            
            case 'time':
                $mysql = 'TIME';
                break;
        }
        
        // Add nullable
        $null = 'NOT NULL';
        if(array_key_exists('null', $definition) && $definition['null']) $null = 'NULL';
        $mysql .= ' '.$null;
        
        // Add default
        if(array_key_exists('default', $definition) && ($definition['type'] != 'text')) {
            $mysql .= ' DEFAULT ';
            $default = $definition['default'];
            
            if(is_null($default)) {
                $mysql .= 'NULL';
            }else if(is_bool($default)) {
                $mysql .= $default ? '1' : '0';
            }else if(is_numeric($default) && in_array($definition['type'], array('int', 'uint', 'float', 'double'))) {
                $mysql .= $default;
            }else if (in_array($definition['type'], array('date', 'datetime', 'time')) && $default === 'CURRENT_TIMESTAMP') {
                $mysql .= $default;
            }else $mysql .= '"'.str_replace('"', '\\"', $default).'"';
        }
        
        // Add options
        if(array_key_exists('autoinc', $definition) && $definition['autoinc']) $mysql .= ' AUTO_INCREMENT';
        
        // Return statment
        return $mysql;
    }
}
