<?php

/**
 *     Moment - Update.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Update step
 */
class Update {
    /**
     * Positional constants
     */
    const BEFORE = 'before';
    const AFTER = 'after';
    
    /**
     * @var Update current update
     */
    private static $current = null;
    
    /**
     * @var string start version
     */
    private $from = '';
    
    /**
     * @var string end version
     */
    private $to = '';
    
    /**
     * @var integer order
     */
    private $order = 0;
    
    /**
     * @var mixed core infos
     */
    private $core = false;
    
    /**
     * @var array update steps
     */
    private $steps = array('before' => array(), 'after' => array());

    /**
     * Constructor
     *
     * @param string $from
     * @param string $to
     * @param int $order
     * @param bool $core
     */
    public function __construct($from, $to, $order = 0, $core = false) {
        $this->from = $from;
        $this->to = $to;
        $this->order = $order;
        $this->core = (object)$core;
        
        self::$current = $this;
    }
    
    /**
     * Run update
     * 
     * @param string $position
     */
    public function run($position) {
        if(!in_array($position, array(self::BEFORE, self::AFTER)))
            return;
        
        if($this->core) {
            Logger::info('Running core update from version '.$this->core->from.' to version '.$this->core->to.' (required by application version '.$this->to.') ...');
        } else {
            Logger::info('Running application update from version '.$this->from.' to version '.$this->to.' ...');
        }
        
        foreach($this->steps[$position] as $step)
            $step();
        
        Logger::info('... done');
    }

    /**
     * Update data getter
     *
     * @param string $key
     *
     * @return mixed
     *
     * @throws PropertyAccessException
     */
    public function __get($key) {
        if(property_exists($this, $key)) return $this->$key;
        
        if($key == 'has_before') return (bool)count($this->steps[self::BEFORE]);
        
        if($key == 'has_after') return (bool)count($this->steps[self::AFTER]);
        
        throw new PropertyAccessException($this, $key);
    }
    
    /**
     * Register step(s)
     * 
     * @param string $position
     * @param mixed $steps callable or array of callable
     */
    public function addStep($position, $steps) {
        if(!in_array($position, array(self::BEFORE, self::AFTER)))
            return;
        
        foreach((array)$steps as $step) {
            if(!is_callable($step)) continue;
            $this->steps[$position][] = $step;
        }
    }
    
    /**
     * Register step(s) on current update
     * 
     * @param string $position
     * @param mixed $steps callable or array of callable
     */
    public function addStepToCurrent($position, $steps) {
        if(!self::$current) return;
        
        self::$current->addStep($position, $steps);
    }
    
    /**
     * Compare self to other considering position
     * 
     * @param Update $other
     * @param string $position
     * 
     * @return integer (see str_cmp)
     */
    public function compare($other, $position) {
        $from_cmp = Updater::compareVersions($this->from, $other->from);
        $to_cmp = Updater::compareVersions($this->to, $other->to);
        
        if($from_cmp == 0 && $to_cmp == 0)
            return $other->order - $this->order; // Same start and end : order leads
        
        if($from_cmp == 0)
            return $to_cmp; // Same start : end leads
        
        if($to_cmp == 0)
            return $from_cmp; // Same end : start leads
        
        if(!in_array($position, array(self::BEFORE, self::AFTER)))
            return 0;
        
        // Different starts and ends in before : start leads if before, end leads otherwise
        return ($position == self::BEFORE) ? $from_cmp : $to_cmp;
    }
}
