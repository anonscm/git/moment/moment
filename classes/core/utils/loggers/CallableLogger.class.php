<?php

/**
 *     Moment - CallableLogger.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Callable based logger
 */
class CallableLogger extends AbstractLogger {
    /**
     * @var array callbacks
     */
    private $callbacks = array();

    /**
     * Constructor
     *
     * @param array $config
     *
     * @throws ConfigBadParameterException
     * @throws ConfigMissingParameterException
     */
    public function __construct($config = array()) {
        $self = 'log_facilities['.$config['index'].']';
        
        // Callback based facilities need at least a log callback ...
        if(!array_key_exists('log', $config))
            throw new ConfigMissingParameterException($self.'[log]');
        
        // ... which must be callable
        if(!is_callable($config['log']))
            throw new ConfigBadParameterException($self.'[log]');
        
        $this->callbacks['log'] = $config['log'];
        
        if(array_key_exists('close', $config)) {
            if(!is_callable($config['close']))
                throw new ConfigBadParameterException($self.'[close]');
            
            $this->callbacks['close'] = $config['close'];
        }
        
        if(array_key_exists('open', $config)) {
            if(!is_callable($config['open']))
                throw new ConfigBadParameterException($self.'[open]');
            
            $config['open']();
        }
    }
    
    /**
     * Log a message, fallback on error_log
     * 
     * @param string $level
     * @param string $message
     */
    public function log($level, $message) {
        $this->callbacks['log'](ApplicationContext::getProcess(), $level, $message);
    }
    
    /**
     * Destructor
     */
    public function __destruct() {
        if(array_key_exists('close', $this->callbacks))
            $this->callbacks['close']();
    }
}
