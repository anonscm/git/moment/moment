<?php

/**
 *     Moment - SyslogLogger.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Syslog based logger
 */
class SyslogLogger extends AbstractLogger {
    /**
     * Constructor
     *
     * @param array $config
     *
     * @throws ConfigBadParameterException
     */
    public function __construct($config = array()) {
        // PHP syslog arguments may be given
        $i = false;
        if(array_key_exists('ident', $config)) $i = $config['ident'];
        
        $o = 0;
        if(array_key_exists('option', $config)) $o = $config['option'];
        
        $f = 0;
        if(array_key_exists('facility', $config)) $f = $config['facility'];
        
        // Open syslog backend with given options, report failure if any
        if($i || $o || $f)
            if(!openlog($i, $o, $f))
                throw new ConfigBadParameterException('log_facilities['.$config['index'].']');
    }
    
    /**
     * Log a message, fallback on error_log
     * 
     * @param string $level
     * @param string $message
     */
    public function log($level, $message) {
        $priorities = array('error' => LOG_ERR, 'warn' => LOG_WARNING, 'info' => LOG_INFO, 'debug' => LOG_DEBUG);
        syslog($priorities[$level], $message);
    }
    
    /**
     * Destructor
     */
    public function __destruct() {
        closelog();
    }
}
