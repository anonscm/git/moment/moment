<?php

/**
 *     Moment - FileLogger.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * File based logger
 */
class FileLogger extends AbstractLogger {
    /**
     * File handle
     */
    private $file_handle;

    /**
     * Constructor
     *
     * @param array $config
     *
     * @throws ConfigBadParameterException
     * @throws ConfigMissingParameterException
     * @throws UtilitiesCannotChangeFileGroupException
     * @throws UtilitiesCannotChangeFileOwnerException
     */
    public function __construct($config = array()) {
        $self = 'log_facilities['.$config['index'].']';
        
        // Needs at least a path
        if(!array_key_exists('path', $config))
            throw new ConfigMissingParameterException($self.'[path]');
        
        // If defined rotation rate must be valid
        if(
            array_key_exists('rotate', $config) &&
            !in_array($config['rotate'], array('hourly', 'daily', 'weekly', 'monthly', 'yearly'))
        )
            throw new ConfigBadParameterException($self.'[rotate]');
        
        // If defined file mode should be > 0600
        if(
            array_key_exists('file_mode', $config) &&
            (
                !is_int($config['file_mode']) ||
                ($config['file_mode'] < 0600)
            )
        )
            throw new ConfigBadParameterException($self.'[file_mode]');
        
        // Build file path
        $file = $config['path'];
        $ext = '';
        
        if(preg_match('`^(.*/)?([^/]+)\.([a-z0-9]+)$`i', $file, $m)) {
            $file = $m[1].$m[2];
            $ext = $m[3];
            
        }else if(substr($file, -1) == '/') {
            $file .= 'application';
            $ext = 'log';
        }
        
        // Extend name if process separated files enabled
        if(array_key_exists('separate_processes', $config))
            $file .= '_'.ApplicationContext::getProcess();
        
        // Extend name if rotation is enabled
        if(array_key_exists('rotate', $config)) {
            switch($config['rotate']) {
                case 'hourly' :  $file .= '_'.date('Y-m-d').'_'.date('H').'h'; break;
                case 'daily' :   $file .= '_'.date('Y-m-d'); break;
                case 'weekly' :  $file .= '_'.date('Y').'_week_'.date('W'); break;
                case 'monthly' : $file .= '_'.date('Y-m'); break;
                case 'yearly' :  $file .= '_'.date('Y'); break;
            }
        }
        
        // Add file extension
        if($ext) $file .= '.'.$ext;
        
        $exists = file_exists($file);
        
        // Open file for writing, log with PHP internal logger if any problem to avoid loops
        $this->file_handle = fopen($file, 'a');
        
        if($this->file_handle) {
            if(!$exists) {
                // Log file just created, update modes
                $mode = array_key_exists('file_mode', $config) ? (int)$config['file_mode'] : 0644;
                chmod($file, $mode);
                
                Utilities::ensureFileOwnership($file);
            }
            
        } else {
            error_log('[Application logging error] Could not log to '.$file);
        }
    }
    
    /**
     * Log a message, fallback on error_log
     * 
     * @param string $level
     * @param string $message
     */
    public function log($level, $message) {
        $message = '['.date('Y-m-d H:i:s').'] '.trim($message);
        
        if($this->file_handle) {
            fwrite($this->file_handle, $message."\n");
            
        } else {
            error_log($message);
        }
    }
    
    /**
     * Destructor
     */
    public function __destruct() {
        if($this->file_handle)
            fclose($this->file_handle);
    }
}
