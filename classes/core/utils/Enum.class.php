<?php

/**
 *     Moment - Enum.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Enum like type base
 *
 * Should be used for any limited values option handling
 */
abstract class Enum {
    /**
     * Defined values cache
     */
    protected static $constCache = array();
    
    /**
     * Get defined values
     *
     * @return array
     */
    public static function getConstants() {
        $class = get_called_class();
        // Use Reflexion to get constants defined in child class
        if(!array_key_exists($class, self::$constCache)) {
            $reflect = new ReflectionClass(get_called_class());
            self::$constCache[$class] = array_values($reflect->getConstants());
        }
        
        return self::$constCache[$class];
    }
    
    
    /**
     * Check if a value is known
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isValid($value) {
        return in_array($value, static::getConstants(), true);
    }
}

