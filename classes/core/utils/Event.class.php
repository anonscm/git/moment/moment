<?php

/**
 *     Moment - Event.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Event handler (hook)
 */
class Event {
    /**
     * Identifier
     */
    private $id = '';
    
    /**
     * Data
     */
    public $data = array();
    
    /**
     * Result
     */
    public $result = null;
    
    /**
     * Propagation stopped
     */
    private $propagation_stopped = false;
    
    /**
     * Default prevented
     */
    private $default_prevented = false;
    
    /**
     * Handlers
     */
    private static $handlers = array();
    
    /**
     * Order constants
     */
    const BEFORE = 'before';
    const AFTER  = 'after';
    
    /**
     * Constructor
     * 
     * @param string $id
     * @param mixed ... data that will be passed to handlers / default
     */
    public function __construct($id) {
        $this->id = $id;
        $this->data = array_slice(func_get_args(), 1);
    }
    
    /**
     * Stop propagation
     */
    public function stopPropagation() {
        $this->propagation_stopped = true;
    }
    
    /**
     * Stop propagation
     */
    public function preventDefault() {
        $this->default_prevented = true;
    }
    
    /**
     * Getter
     * 
     * @param string $property property to get
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'id', 'propagation_stopped', 'default_prevented'
        ))) return $this->$property;
        
        throw new PropertyAccessException($this, $property);
    }
    
    /**
     * Register a handler
     * 
     * @param string $order Event::BEFORE, Event::AFTER
     * @param string $id
     * @param callable $handler
     * 
     * @throws EventBadOrderException
     * @throws EventHandlerIsNotCallableException
     */
    public static function register($order, $id, $handler) {
        $order = strtolower($order);
        if(!in_array($order, array(self::BEFORE, self::AFTER)))
            throw new EventBadOrderException($order);
        
        if(!is_callable($handler))
            throw new EventHandlerIsNotCallableException();
        
        if(!array_key_exists($id, self::$handlers))
            self::$handlers[$id] = array(self::BEFORE => array(), self::AFTER => array());
        
        self::$handlers[$id][$order][] = $handler;
    }
    
    /**
     * Trigger an event
     * 
     * @param callable $default_handler callable default handler
     * 
     * @return mixed last returned data
     * 
     * @throws EventHandlerIsNotCallableException
     */
    public function trigger($default_handler = null) {
        if($default_handler && !is_callable($default_handler))
            throw new EventHandlerIsNotCallableException();
        
        // Run BEFORE handlers if any
        if(array_key_exists($this->id, self::$handlers)) {
            foreach(self::$handlers[$this->id][self::BEFORE] as $handler) {
                call_user_func($handler, $this);
                
                if($this->propagation_stopped) break;
            }
        }
        
        if(!$this->default_prevented && $default_handler)
            $this->result = call_user_func_array($default_handler, $this->data);
        
        // Run AFTER handlers if any
        if(array_key_exists($this->id, self::$handlers)) {
            $this->propagation_stopped = false;
            foreach(self::$handlers[$this->id][self::AFTER] as $handler) {
                call_user_func($handler, $this);
                
                if($this->propagation_stopped) break;
            }
        }
        
        return $this->result;
    }
}
