<?php

/**
 *     Moment - Plugin.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Plugin base
 * 
 * Plugins MUST extend this class
 */
abstract class Plugin {
    /**
     * Dedicated config getter
     * 
     * @param string $parameter_name
     * 
     * @return mixed
     */
    final public static function getConfig($parameter_name = '') {
        $pn = 'plugin.'.strtolower(static::getName());
        if($parameter_name) $pn .= '.'.$parameter_name;
        
        return Config::get($pn);
    }
    
    /**
     * Name getter
     * 
     * @return string
     */
    final public static function getName() {
        return substr(get_called_class(), 0, -6);
    }
    
    /**
     * Path getter
     * 
     * @return string
     */
    final public static function getPath() {
        return PluginManager::getPluginPath(static::getName());
    }
    
    /**
     * Callback name getter
     * 
     * @param string $method
     * 
     * @return string
     */
    final public static function getCallback($method) {
        return static::getName().'Plugin::'.$method;
    }

    /**
     * Render template
     *
     * @param string $id
     * @param array $vars
     */
    final public static function displayTemplate($id, $vars = array()) {
        Template::display('plugin:'.static::getName().':'.$id, $vars);
    }
    
    /**
     * Dedicated log
     * 
     * @param string $level
     * @param mixed $message
     */
    final public static function log($level, $message) {
        $prefix = '[plugin:'.static::getName().'] ';
        
        if(is_scalar($message)) {
            $message = array($message);
        } else {
            $message = explode("\n", print_r($message, true));
        }
        
        foreach($message as $line)
            Logger::log($level, $prefix.$line);
    }
    
    /**
     * Install method
     * 
     * MAY be implemented
     */
    public static function install() {}
    
    /**
     * Update method
     * 
     * MAY be implemented
     */
    public static function update() {}
    
    /**
     * Uninstall method
     * 
     * MAY be implemented
     */
    public static function uninstall() {}
    
    /**
     * Initialization method
     * 
     * MUST be implemented
     */
    public static function initialize() {}
}
