<?php

/**
 *     Moment - MailAttachment.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');


/**
 * Handle mail attachment
 */
class MailAttachment {
    /**
     * Attachment path
     */
    private $path = null;
    
    /**
     * Attachment name
     */
    private $name = null;
    
    /**
     * Attachment contents
     */
    private $content = null;
    
    /**
     * Attachment mime type
     */
    private $mime_type = null;
    
    /**
     * Attachment disposition
     */
    private $disposition = 'attachment';
    
    /**
     * Attachment transfer encoding
     */
    private $transfer_encoding = 'base64';
    
    /**
     * Attachment cid
     */
    private $cid = null;
    
    /**
     * Attachment method
     * Used for ICS
     */
    private $method = null;
    
    /**
     * New line style
     */
    private $nl = "\r\n";
    
    /**
     * Create new empty attachment
     * 
     * @param string $name
     * @param string $cid if inline displayed
     */
    public function __construct($name = null, $cid = null) {
        if($name)
            $this->name = $name;

        if($cid)
            $this->cid = $cid;

        $nl = Config::get('email.newline');
        if($nl) $this->nl = $nl;
    }

    /**
     * Build attachment for sending
     *
     * @return string
     *
     * @throws MailAttachmentNoContentException
     */
    public function build() {
        // Fail if missing data
        if(is_null($this->content) && is_null($this->path))
            throw new MailAttachmentNoContentException($this->path);
        
        // Extract name from path
        if(!$this->name && $this->path) $this->name = basename($this->path);
        
        // Extract mime type from path
        if(!$this->mime_type && $this->name) $this->mime_type = Mime::getFromFile($this->name);
        
        // Set Content-Type part header
        $source = 'Content-Type: '.$this->mime_type.($this->name ? '; name="'.$this->name.'"' : '')
            .($this->method ? ';method='.$this->method : '').$this->nl;
        
        // Set Content-Transfer-Encoding part header
        if($this->transfer_encoding)
            $source .= 'Content-Transfer-Encoding: '.$this->transfer_encoding.$this->nl;
        
        // Set Content-Disposition part header
        $source .= 'Content-Disposition: '.$this->disposition.($this->name ? '; filename="'.$this->name.'"' : '').$this->nl;
        
        // Set Content-ID part header (for embedded attachments)
        if($this->cid)
            $source .= 'Content-ID: '.$this->cid.$this->nl;
        
        // Get file data
        $content = $this->content ? $this->content : file_get_contents($this->path);
        
        // Encode file data if needed
        switch($this->transfer_encoding) {
            case 'base64' : $content = chunk_split(base64_encode($content)); break;
            case 'quoted-printable' : $content = quoted_printable_encode($content); break;
        }
        
        $source .= $this->nl.trim($content).$this->nl;
        
        return $source;
    }
    
    /**
     * Getter
     * 
     * @param string $property property to get
     * 
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'path', 'name', 'content', 'mime_type', 'disposition', 'transfer_encoding', 'cid', 'method'
        ))) return $this->$property;
        
        if($property == 'source') return $this->build();
        
        return null;
    }

    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws CoreCannotReadFileException
     * @throws MailAttachmentBadDispositionException
     * @throws MailAttachmentBadTransferEncodingException
     */
    public function __set($property, $value) {
        if($property == 'path') {
            if(!file_exists($value))
                throw new CoreCannotReadFileException($value);
            
            $this->path = $value;
            $this->name = basename($value);
            $this->content = null;
            
        }else if($property == 'content') {
            $this->path = null;
            $this->content = $value;
            
        }else if($property == 'mime_type') {
            $this->mime_type = $value;
            
        }else if($property == 'name') {
            $this->name = $value;
            
        }else if($property == 'disposition') {
            if(!in_array($value, array('inline', 'attachment')))
                throw new MailAttachmentBadDispositionException($value);
            
            $this->disposition = $value;
            
        }else if($property == 'transfer_encoding') {
            if($value && !in_array($value, array('8bit', 'base64', 'quoted-printable')))
                throw new MailAttachmentBadTransferEncodingException($value);
            
            $this->transfer_encoding = $value;
            
        }else if($property == 'cid'){
            $this->cid = $value;
            
        }else if($property == 'method'){
            $this->method = $value;
        }
    }
}
