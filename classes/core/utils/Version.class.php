<?php

/**
 *     Moment - Version.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Version handling
 */
abstract class Version {
    /**
     * Code version
     */
    const CODE = null;
    
    /**
     * Get code version
     * 
     * @return mixed
     */
    public static function getCodeVersion() {
        return static::CODE;
    }
    
    /**
     * Get installed version
     * 
     * @return mixed
     */
    public static function getInstalledVersion() {
        $class = get_called_class();
        if(preg_match('`^(Application|Core)(.*)Version$`', $class, $m)) {
            $handler = $m[1];
            $key = ($m[2] ? strtolower($m[2]).'_' : '').'version';
            
        } else {
            $handler = 'ApplicationData';
            $key = strtolower($class).'_version';
        }
        
        return $handler::get($key);
    }
}
