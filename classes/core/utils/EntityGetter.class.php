<?php

/**
 *     Moment - EntityGetter.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Entity getter
 */
class EntityGetter {
    
    private $className;
    private $callable;

    /**
     * Constructor
     *
     * @param string $className
     * @param callable $callable
     *
     * @throws ModelViolationException
     */
    public function __construct($className, $callable) {
        if (!$className || !class_exists($className))
            throw new ModelViolationException('Class not found: '.$className);
        
        if (!$callable|| !is_callable($callable))
            throw new ModelViolationException('Not callable');
        
        $this->className = $className;
        $this->callable = $callable;
        
    }

    /**
     * Getter
     *
     * @param string $property
     *
     * @return mixed
     *
     * @throws PropertyAccessException
     */
    public function __get($property) {
        if (property_exists($this, $property))
            return $this->$property;
        
        throw new PropertyAccessException($this,$property);
    }
}