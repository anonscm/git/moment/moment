<?php

/**
 *     Moment - RemoteApplication.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Remote application
 *
 * @property-read string name
 * @property-read string secret
 * @property-read array acl
 * @property-read bool isAdmin
 * @property-read bool canCreateUser
 */
class RemoteApplication {
    /**
     * Application name
     */
    private $name = '';
    
    /**
     * Secret
     */
    private $secret = '';
    
    /**
     * ACLs
     */
    private $acl = array();
    
    /**
     * Admin status
     */
    private $isAdmin = false;
    
    /**
     * Can impersonate non-existing users
     */
    private $canCreateUser = false;

    /**
     * Constructor
     *
     * @param string $name
     * @param array $cfg application as defined in config
     *
     * @throws ConfigBadParameterException
     */
    public function __construct($name, $cfg) {
        $this->name = $name;
        
        if(!array_key_exists('secret', $cfg))
            throw new ConfigBadParameterException('auth_remote.application.applications.'.$name.'.secret');
        
        $this->secret = $cfg['secret'];
        
        if(!array_key_exists('acl', $cfg) || !is_array($cfg['acl']))
            throw new ConfigBadParameterException('auth_remote.application.applications.'.$name.'.acl');
        
        $this->acl = $cfg['acl'];
        
        if(array_key_exists('isAdmin', $cfg) && $cfg['isAdmin'])
            $this->isAdmin = true;
        
        if(array_key_exists('canCreateUser', $cfg) && $cfg['canCreateUser'])
            $this->canCreateUser = true;
    }
    
    /**
     * Check access right
     * 
     * @param string $method
     * @param string $endpoint
     * 
     * @return bool
     */
    public function allowedTo($method, $endpoint) {
        $acl = false;
        
        if(array_key_exists($endpoint, $this->acl))
            $acl = $this->acl[$endpoint];
        
        if(!$acl && array_key_exists('*', $this->acl))
            $acl = $this->acl['*'];
        
        if(!is_array($acl)) return (bool)$acl;
        
        if(array_key_exists($method, $acl))
            return (bool)$acl[$method];
        
        if(array_key_exists('*', $acl))
            return (bool)$acl['*'];
        
        return false;
    }

    /**
     * Getter
     *
     * @param string $property
     *
     * @return mixed
     *
     * @throws PropertyAccessException
     */
    public function __get($property) {
        if(in_array($property, array(
            'name', 'secret', 'acl', 'isAdmin', 'canCreateUser'
        )))
            return $this->$property;
        
        throw new PropertyAccessException($this, $property);        
    }
}
