<?php

/**
 *     Moment - FileIO.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Generic file reader / writer
 */
class FileIO {
    /**
     * @var string path
     */
    protected $path = null;
    
    /**
     * @var resource handle
     */
    protected $handle = null;
    
    /**
     * @var boolean locked
     */
    protected $locked = false;

    /**
     * Constructor
     *
     * @see fopen
     *
     * @param string $path
     * @param string $mode
     *
     * @throws FileIOCannotLockException
     * @throws FileIOCannotOpenFileException
     * @throws FileIOCannotSetGroupException
     * @throws FileIOCannotSetModeException
     * @throws FileIOCannotSetOwnerException
     */
    public function __construct($path, $mode) {
        $this->path = self::fullPath($path);
        
        $this->handle = fopen($this->path, $mode);
        if(!$this->handle)
            throw new FileIOCannotOpenFileException($this, $mode);
        
        if(preg_match('`[wacx]`', $mode)) { // Writting ?
            if(!flock($this->handle, LOCK_EX))
                throw new FileIOCannotLockException($this);
            
            $this->locked = true;
        }
        
        self::setAttributes($this->path);
    }
    
    /**
     * Destructor
     */
    public function __destuct() {
        $this->close();
    }
    
    /**
     * Close handle
     */
    public function close() {
        if(!$this->handle) return;
        
        if($this->locked && !flock($this->handle, LOCK_UN))
            throw new FileIOCannotUnlockException($this);
        
        $this->locked = false;
        
        if(!fclose($this->handle))
            throw new FileIOCannotCloseFileException($this);
        
        $this->handle = null;
    }

    /**
     * Seek to offset
     *
     * @see fseek
     *
     * @param int $offset
     * @param int $whence
     *
     * @throws FileIOCannotSeekIntoFileException
     * @throws FileIOFileClosedException
     */
    public function seek($offset, $whence = SEEK_SET) {
        if(!$this->handle)
            throw new FileIOFileClosedException($this);
        
        if(fseek($this->handle, $offset, $whence) < 0)
            throw new FileIOCannotSeekIntoFileException($this, $offset, $whence);
    }

    /**
     * Read contents
     *
     * @param int $length
     *
     * @return string
     *
     * @throws FileIOCannotReadFileException
     * @throws FileIOFileClosedException
     */
    public function read($length) {
        if(!$this->handle)
            throw new FileIOFileClosedException($this);
        
        $content = fread($this->handle, $length);
        if($content === false)
            throw new FileIOCannotReadFileException($this);
        
        return $content;
    }

    /**
     * Get line
     *
     * @param int $length
     *
     * @return string
     *
     * @throws FileIOFileClosedException
     */
    public function gets($length = null) {
        if(!$this->handle)
            throw new FileIOFileClosedException($this);
        
        return is_null($length) ? fgets($this->handle) : fgets($this->handle, $length);
    }

    /**
     * Get single char
     *
     * @return string
     *
     * @throws FileIOCannotReadFileException
     * @throws FileIOFileClosedException
     */
    public function getc() {
        if(!$this->handle)
            throw new FileIOFileClosedException($this);
        
        $char = fgetc($this->handle);
        if($char === false)
            throw new FileIOCannotReadFileException($this);
        
        return $char;
    }

    /**
     * Write content
     *
     * @param string $content
     * @param int $length
     *
     * @return int
     *
     * @throws FileIOCannotWriteFileException
     * @throws FileIOFileClosedException
     */
    public function write($content, $length = null) {
        if(!$this->handle)
            throw new FileIOFileClosedException($this);
        
        $written = is_null($length) ? fwrite($this->handle, $content) : fwrite($this->handle, $content, $length);
        if($written === false)
            throw new FileIOCannotWriteFileException($this);
        
        return $written;
    }

    /**
     * Getter
     *
     * @param string $property
     *
     * @return mixed
     *
     * @throws PropertyAccessException
     */
    public function __get($property) {
        if($property == 'path') return $this->path;
        
        throw new PropertyAccessException($this, $property);
    }

    /**
     * Read whole file
     *
     * @param string $path
     *
     * @return string
     *
     * @throws FileIOCannotReadFileException
     * @throws FileIONotFoundException
     */
    public static function readWhole($path) {
        $path = self::fullPath($path);
        if(!file_exists($path))
            throw new FileIONotFoundException($path);
        
        $content = file_get_contents($path);
        if($content === false)
            throw new FileIOCannotReadFileException($path);
        
        return $content;
    }
    
    /**
     * Write whole file
     *
     * @param string $path
     * @param string $content
     *
     * @throws FileIOCannotWriteFileException
     * @throws FileIOCannotSetGroupException
     * @throws FileIOCannotSetModeException
     * @throws FileIOCannotSetOwnerException
     */
    public static function writeWhole($path, $content) {
        $path = self::fullPath($path);
        
        $dir = dirname($path);
        self::makePath($dir);
        
        if(file_put_contents($path, $content, LOCK_EX) === false)
            throw new FileIOCannotWriteFileException($path);
        
        self::setAttributes($path);
    }


    /**
     * Set link
     *
     * @param string $target
     * @param $link
     *
     * @throws FileIOCannotCreateLinkException
     * @throws FileIOLinkExistsWithAnotherTargetException
     * @throws FileIOLinkNameTakenException
     */
    public static function link($target, $link) {
        $target = self::fullPath($target);
        $link = self::fullPath($link);
        
        if(is_link($link)) {
            $current = readlink($link);
            if($current == $target) return;
            
            throw new FileIOLinkExistsWithAnotherTargetException($target, $link, $current);
        }
        
        if(is_file($link) || is_dir($link))
            throw new FileIOLinkNameTakenException($link);
        
        if(!link($target, $link))
            throw new FileIOCannotCreateLinkException($target, $link);
    }

    /**
     * Delete a file / directory
     *
     * @param string $path
     * @param boolean $recursive
     *
     * @throws FileIOCannotDeleteDirectoryException
     * @throws FileIOCannotDeleteFileException
     * @throws FileIODirectoryNotEmptyException
     */
    public static function delete($path, $recursive = false) {
        $path = self::fullPath($path);
        
        if(is_file($path) || is_link($path)) {
            if(!unlink($path))
                throw new FileIOCannotDeleteFileException($path);
            
            return;
        }
        
        if(is_dir($path)) {
            $items = array_filter(scandir($path), function($i) {
                return !in_array($i, array('.', '..'));
            });
            
            if(count($items)) {
                if(!$recursive)
                    throw new FileIODirectoryNotEmptyException($path);
                
                foreach($items as $item)
                    self::delete($path.'/'.$item, $recursive);
            }
            
            if(!rmdir($path))
                throw new FileIOCannotDeleteDirectoryException($path);
        }
    }
    
    /**
     * Compute full path
     * 
     * @param string $path
     * 
     * @return string
     */
    public static function fullPath($path) {
        if(substr($path, 0, 1) != '/')
            $path = EKKO_ROOT.'/'.$path;
        
        return $path;
    }
    
    /**
     * Make path
     * 
     * @param string $path
     */
    public static function makePath($path) {
        $parts = array_filter(explode('/', self::fullPath($path)));
        $path = '/';
        
        while($part = array_shift($parts)) {
            $path .= $part;
            self::createDirectory($path);
            $path .= '/';
        }
    }

    /**
     * Make directory
     *
     * @param string $path
     *
     * @throws FileIOCannotCreateDirectoryException
     * @throws FileIOCannotSetGroupException
     * @throws FileIOCannotSetModeException
     * @throws FileIOCannotSetOwnerException
     */
    public static function createDirectory($path) {
        if(is_dir($path)) return;
        
        if(!mkdir($path))
            throw new FileIOCannotCreateDirectoryException($path);
        
        self::setAttributes($path);
    }

    /**
     * Ensure file / directory ownership
     *
     * @param string $path
     *
     * @throws FileIOCannotSetGroupException
     * @throws FileIOCannotSetModeException
     * @throws FileIOCannotSetOwnerException
     */
    public static function setAttributes($path) {
        $mode = null;
        if(is_dir($path)) {
            $mode = Config::get('application_dir_umask');
            if(!$mode) $mode = 0755;
            
        } else if(is_file($path)) {
            $mode = Config::get('application_file_umask');
            if(!$mode) $mode = 0644;
        }
        
        if($mode && !chmod($path, $mode))
            throw new FileIOCannotSetModeException($path, $mode);
        
        $uid = Config::get('application_uid');
        if(!$uid) return; // Should not happen if config is all right
        
        if(fileowner($path) == $uid) return;
        
        if(!chown($path, $uid))
            throw new FileIOCannotSetOwnerException($path, $uid);
        
        $gid = Config::get('application_gid');
        if($gid)
            if(!chgrp($path, $gid))
                throw new FileIOCannotSetGroupException($path, $gid);
    }
}
