<?php

/**
 *     Moment - EntityCache.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Entities and relations cache
 */
class EntityCache {
    const BY_UID = '_by_uid';
    
    const ALIASES = 'aliases';
    const COLLECTIONS = 'collections';
    const RELCOLMAP = 'relcolmap';
    const ENTITIES = 'entities';
    const METADATA = 'metadata';
    const RELATIONS = 'relations';
    
    
    /**
     * Get cached metadata
     * 
     * @param string $class
     * @param string $name
     *
     * @return array|null
     */
    public static function getMetadata($class, $name) {
        $cached = ApplicationCache::get(array(self::METADATA, $class, $name));
        return $cached ? $cached : null;
    }


    /**
     * Store metadata in cache
     *
     * @param string $class
     * @param string $name
     * @param mixed $value
     */
    public static function setMetadata($class, $name, $value) {
        ApplicationCache::set(array(self::METADATA, $class, $name), $value);
    }
    
    
    /**
     * Get cached entity
     * 
     * @param string $class
     * @param mixed $uid
     * 
     * @return Entity|null
     */
    public static function getEntity($class, $uid) {
        if(!is_scalar($uid))
            $uid = $class::buildUID($uid);
        
        $cached = ApplicationCache::get(array(self::ENTITIES, $class, self::BY_UID, $uid));
        return $cached ? $cached['entity'] : null;
    }
    
    
    /**
     * Get cached entity
     * 
     * @param string $class
     * @param string $selector
     * @param mixed $value
     * 
     * @return Entity|null
     */
    public static function getEntityBy($class, $selector, $value) {
        $uid = ApplicationCache::get(array(self::ALIASES, $class, $selector, $value));
        
        return $uid ? self::getEntity($class, $uid) : null;
    }

    /**
     * Store entity in cache
     *
     * @param Entity $entity
     *
     * @throws ModelViolationException
     */
    public static function setEntity($entity) {
        $class = get_class($entity);
        $uid = $entity->getUID();
        if(is_null($uid))
            throw new ModelViolationException('Trying to cache unsaved '.$class);
        
        ApplicationCache::set(array(self::ENTITIES, $class, self::BY_UID, $uid), array(
            'entity' => $entity,
            'aliases' => array()
        ));
    }

    /**
     * Store entity in cache
     *
     * @param Entity $entity
     * @param string $selector
     * @param mixed $value
     *
     * @throws ModelViolationException
     */
    public static function setEntityBy($entity, $selector, $value) {
        $class = get_class($entity);
        
        if(ApplicationCache::exists(array(self::ALIASES, $class, $selector, $value))) return;
        
        $uid = $entity->getUID();
        if(is_null($uid))
            throw new ModelViolationException('Trying to cache unsaved '.$class);
        
        $cached = ApplicationCache::get(array(self::ENTITIES, $class, self::BY_UID, $uid));
        if(!$cached) $cached = array(
            'entity' => $entity,
            'aliases' => array()
        );
        
        ApplicationCache::set(array(self::ALIASES, $class, $selector, $value), $uid);
        
        $cached['aliases'][$selector] = $value;
        
        ApplicationCache::set(array(self::ENTITIES, $class, self::BY_UID, $uid), $cached);
    }

    /**
     * Remove entity from cache
     *
     * @param mixed $entity entity or class
     *
     * @throws ModelViolationException
     */
    public static function dropEntity($entity) {
        if(is_string($entity)) {
            if($entity == '*') {
                ApplicationCache::drop(array(self::ENTITIES, '*'));
            } else {
                ApplicationCache::drop(array(self::ENTITIES, $entity, '*'));
            }
            
            return;
        }
        
        $class = get_class($entity);
        $uid = $entity->getUID();
        if(is_null($uid))
            throw new ModelViolationException('Trying to drop unsaved '.$class.' from cache');
        
        $cached = ApplicationCache::get(array(self::ENTITIES, $class, self::BY_UID, $uid));
        if(!$cached) return;
        
        ApplicationCache::drop(array(self::ENTITIES, $class, self::BY_UID, $uid));
        
        foreach($cached['aliases'] as $selector => $value)
            ApplicationCache::drop(array(self::ALIASES, $class, $selector, $value));
        
        foreach(ApplicationCache::match(array(self::COLLECTIONS, $class)) as $entry) {
            if(!in_array($uid, $entry['data'])) continue;
            
            $uids = array_filter($entry['data'], function($v) use($uid) {
                return $v != $uid;
            });
            
            ApplicationCache::set($entry['path'], $uids);
        }
    }

    /**
     * Get instances of other class related to entity
     *
     * @param Entity $entity
     * @param string $other
     *
     * @return array of Entity children or null if not loaded
     *
     * @throws ModelViolationException
     */
    public static function getRelation($entity, $other) {
        $class = get_class($entity);
        $uid = $entity->getUID();
        if(is_null($uid))
            throw new ModelViolationException('Trying to get relation of unsaved '.$class);
        
        $cached = ApplicationCache::get(array(self::RELATIONS, $class, $uid, $other));
        if(is_null($cached)) return null;
        
        $others = array();
        foreach($cached as $oid)
            $others[] = $other::fromId($oid);
        
        return $others;
    }

    /**
     * Store relations between entity and other class instances
     *
     * @param Entity $entity
     * @param mixed $what array of other class instances
     * @param string|array $cid collection id(s)
     *
     * @throws ModelViolationException
     */
    public static function setRelation($entity, $what, $cid = null) {
        $class = get_class($entity);
        $uid = $entity->getUID();
        if(is_null($uid))
            throw new ModelViolationException('Trying to set relation of unsaved '.$class);
        
        if(is_object($what))
            $what = array($what);
        
        $others = array();
        foreach($what as $other) {
            if(!is_object($other) && !($other instanceof Entity))
                continue;
            
            $others[get_class($other)][] = $other->getUID();
        }
        
        foreach($others as $other_class => $add_uids) {
            $cached = ApplicationCache::get(array(self::RELATIONS, $class, $uid, $other_class));
            if(!$cached) $cached = array();
            
            $cached = array_unique(array_merge($cached, $add_uids));
            
            ApplicationCache::set(array(self::RELATIONS, $class, $uid, $other_class), $cached);
            
            if($cid) {
                $map = ApplicationCache::get(array(self::RELCOLMAP));
                if(!$map) $map = array();
                
                // Get join name
                $joinName = array($class, $other_class);
                sort($joinName);
                $joinName = implode('_', $joinName);
                
                if(!array_key_exists($joinName, $map)) $map[$joinName] = array();
                
                $map[$joinName] = array_unique(array_merge($map[$joinName], (array)$cid));
                
                ApplicationCache::set(array(self::RELCOLMAP), $map);
            }
        }
    }

    /**
     * Drop relations between entity and other class
     *
     * @param Entity $entity
     * @param mixed $what entity or class name or null to drop all relations
     *
     * @throws ModelViolationException
     */
    public static function dropRelation($entity, $what = null) {
        if(is_string($entity)) {
            if($entity == '*') {
                ApplicationCache::drop(array(self::RELATIONS, '*'));
                
                $map = ApplicationCache::get(array(self::RELCOLMAP));
                if($map) {
                    foreach($map as $joinName => $cids)
                        foreach($cids as $cid)
                            ApplicationCache::drop(array(self::COLLECTIONS, $joinName, $cid));
                }
                ApplicationCache::drop(array(self::RELCOLMAP));
                
            } else {
                ApplicationCache::drop(array(self::RELATIONS, $entity, '*'));
                
                $map = ApplicationCache::get(array(self::RELCOLMAP));
                if($map) {
                    $keys = preg_grep('`(^'.$entity.'_|_'.$entity.'$)`', array_keys($map));
    
                    foreach ($keys as $key) {
                        foreach ($map[$key] as $cids){
                            foreach ($cids as $cid) {
                                ApplicationCache::drop(array(self::COLLECTIONS, $key, $cid));
                            }
                        }
                        unset($map[$key]);
                    }
                    ApplicationCache::set(array(self::RELCOLMAP), $map);
                }
            }
            
            return;
        }
        
        $class = get_class($entity);
        $uid = $entity->getUID();
        if(is_null($uid))
            throw new ModelViolationException('Trying to drop relation of unsaved '.$class);
        
        if(is_null($what)) {
            ApplicationCache::drop(array(self::RELATIONS, $class, $uid, '*'));
            
            $map = ApplicationCache::get(array(self::RELCOLMAP));
            
            if($map) {
                $keys = preg_grep('`(^'.$class.'_|_'.$class.'$)`', array_keys($map));
                
                foreach ($keys as $key) {
                    foreach ($map[$key] as $cids){
                        foreach ($cids as $cid) {
                            ApplicationCache::drop(array(self::COLLECTIONS, $key, $cid));
                        }
                    }
                    unset($map[$key]);
                }
                ApplicationCache::set(array(self::RELCOLMAP), $map);
            }
            
            return;
        }
        
        if(is_string($what)) {
            ApplicationCache::drop(array(self::RELATIONS, $class, $uid, $what));
            
            $map = ApplicationCache::get(array(self::RELCOLMAP));
            if($map) {
                // Get join name
                $joinName = array($class, $what);
                sort($joinName);
                $joinName = implode('_', $joinName);
                
                if(array_key_exists($joinName, $map)) {
                    foreach($map[$joinName] as $cid)
                        ApplicationCache::drop(array(self::COLLECTIONS, $joinName, $cid));
    
                    unset($map[$joinName]);
                    ApplicationCache::set(array(self::RELCOLMAP), $map);
                }
            }
            
            return;
        }
        
        if(is_object($what))
            $what = array($what);
        
        $others = array();
        foreach($what as $other) {
            if(!is_object($other) && !($other instanceof Entity))
                continue;
            
            $others[get_class($other)][] = $other->getUID();
        }
        
        foreach($others as $other_class => $drop_uids) {
            $cached = ApplicationCache::get(array(self::RELATIONS, $class, $uid, $other_class));
            if(!$cached) continue;
            
            $cached = array_filter($cached, function($uid) use($drop_uids) {
                return !in_array($uid, $drop_uids);
            });
            
            ApplicationCache::set(array(self::RELATIONS, $class, $uid, $other_class), $cached);
            
            $map = ApplicationCache::get(array(self::RELCOLMAP));
            if($map) {
                // Get join name
                $joinName = array($class, $other_class);
                sort($joinName);
                $joinName = implode('_', $joinName);
    
                if(array_key_exists($joinName, $map)) {
                    foreach($map[$joinName] as $cid)
                        ApplicationCache::drop(array(self::COLLECTIONS, $joinName, $cid));
        
                    unset($map[$joinName]);
                    ApplicationCache::set(array(self::RELCOLMAP), $map);
                }
            }
        }
    }
    
    /**
     * Build collection key
     * 
     * @param mixed $selector
     * 
     * @return string
     */
    public static function getCollectionKey($selector) {
        return hash('sha1', implode('___', array_map(function($s) {
            return is_scalar($s) ? $s : serialize($s);
        }, (array)$selector)));
    }
    
    /**
     * Get cached collection
     * 
     * @param string $class
     * @param array $selector
     * 
     * @return mixed array or null
     */
    public static function getCollection($class, $selector) {
        $key = self::getCollectionKey($selector);
        
        $uids = ApplicationCache::get(array(self::COLLECTIONS, $class, $key));
        
        if (is_null($uids)) return null;
        
        $entities = array();
        foreach($uids as $uid)
            $entities[] = self::getEntity($class, $uid);

        return $entities;
    }
    
    /**
     * Store collection in cache
     * 
     * @param string $class
     * @param array $selector
     * @param array $collection
     * 
     * @return string
     */
    public static function setCollection($class, $selector, $collection) {
        $key = self::getCollectionKey($selector);
        
        ApplicationCache::set(array(self::COLLECTIONS, $class, $key), array_map(function($entity) {
            return $entity->getUID();
        }, $collection));
        
        return $key;
    }
    
    /**
     * Remove collection from cache
     * 
     * @param string $class
     * @param array $selector
     */
    public static function dropCollection($class, $selector = null) {
        if(is_null($selector)) {
            ApplicationCache::drop(array(self::COLLECTIONS, $class, '*'));
            
            return;
        }
        
        $key = self::getCollectionKey($selector);
        
        ApplicationCache::drop(array(self::COLLECTIONS, $class, $key));
    }
}
