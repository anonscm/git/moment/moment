<?php

/**
 *     Moment - Logger.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Log utility
 */
class Logger {
    /**
     * Log levels to log priorities conversion table
     */
    private static $levels = array(
        LogLevels::ERROR    => 0,
        LogLevels::WARN     => 1,
        LogLevels::INFO     => 2,
        LogLevels::DEBUG    => 3
    );
    
    /**
     * Logging facilities
     */
    private static $facilities = null;
    
    /**
     * Setup logging facilities
     */
    private static function setup() {
        if(!is_null(self::$facilities)) return;

        self::$facilities = array(array('logger' => new ErrorlogLogger(), 'level' => LogLevels::DEBUG)); // Failsafe facility so we have at least one if no valid ones defined in config
        
        // Get facilities from config, cast to single type
        $facilities = Config::get('log_facilities');
        if(!$facilities) $facilities = array();
        if(!is_array($facilities)) $facilities = array('type' => $facilities);
        if(!is_numeric(key($facilities))) $facilities = array($facilities);
        
        // Lookup valid facilities
        foreach($facilities as $index => $facility) {
            
            // Casting and facility defaults
            if(!is_array($facility)) $facility = array('type' => $facility);
            
            if(!array_key_exists('type', $facility))
                throw new ConfigMissingParameterException('log_facilities['.$index.'][type]');

            if (!isset($facility['level']) || !LogLevels::isValid($facility['level']))
                $facility['level'] = LogLevels::INFO;
            
            $facility['index'] = $index;
            
            // Facility type based parameter checks
            switch(strtolower($facility['type'])) {
                case 'file' :      $class = 'FileLogger';     break;
                case 'syslog' :    $class = 'SyslogLogger';   break;
                case 'error_log' : $class = 'ErrorlogLogger'; break;
                case 'callable' :  $class = 'CallableLogger'; break;
                
                default :
                    // Unknown facilities are reported
                    throw new ConfigBadParameterException('log_facilities['.$index.'][type]');
            }
            
            if(!class_exists($class))
                throw new ConfigBadParameterException('log_facilities['.$index.'][type]');
            
            $facility['logger'] = new $class($facility);
            self::$facilities[] = $facility;
        }
        
        // Report if no valid facility found (using failsafe)
        if(count($facilities) && count(self::$facilities) < 2)
            throw new ConfigBadParameterException('log_facilities');
        
        // Remove failsafe facility if everything went well
        if(count(self::$facilities) >= 2) array_shift(self::$facilities);
    }
    
    /**
     * Make it so we have at least a error_log logger (use in cli scripts)
     */
    public static function ensureErrorlog() {
        self::setup();
        
        foreach(self::$facilities as $facility)
            if($facility['type'] == 'error_log')
                return;
            
        self::$facilities[] = array(
            'type' => 'error_log',
            'logger' => new ErrorlogLogger(),
            'level' => LogLevels::INFO
        );
    }
    
    /**
     * Log error
     * 
     * @param string $message
     */
    public static function error($message) {
        self::log(LogLevels::ERROR, $message);
    }
    
    /**
     * Log warn
     * 
     * @param string $message
     */
    public static function warn($message) {
        self::log(LogLevels::WARN, $message);
    }
    
    /**
     * Log info
     * 
     * @param string $message
     */
    public static function info($message) {
        self::log(LogLevels::INFO, $message);
    }
    
    /**
     * Log debug
     * 
     * @param string $message
     * @param boolean $trace
     */
    public static function debug($message, $trace = true) {
        self::log(LogLevels::DEBUG, $message, $trace);
    }
    
    /**
     * Log message
     * 
     * @param string $level
     * @param string $message
     * @param boolean $trace
     */
    public static function log($level, $message, $trace = true) {
        // Setup facilities if not already done
        self::setup();
        
        $must_log = false;
        foreach(self::$facilities as $facility){
            if(self::$levels[$level] <= self::$levels[$facility['level']]){
                $must_log = true;
            }
        }
        
        if(!$must_log) return; // Avoid aggregating data if no facility to forward message to
        
        // If message is other than scalar (object, array) then print_r it and log individual lines
        if(!is_scalar($message)) {
            foreach(explode("\n", print_r($message, true)) as $line)
                self::log($level, $line);
            
            return;
        }
        
        // Default log level is error so that log calls without level will always be processed
        if(LogLevels::isValid($level) && !array_key_exists($level, self::$levels))
            $level = LogLevels::ERROR;
        
        // Add call context data if level is debug
        if($level == LogLevels::DEBUG && $trace) {
            // Fecth call stack
            $stack = debug_backtrace();
            
            // Remove Logger internals
            while($stack && array_key_exists('class', $stack[0]) && ($stack[0]['class'] == 'Logger'))
                array_shift($stack);
            
            // If call context is known
            if($stack && array_key_exists('function', $stack[0]) && $stack[0]['function']) {
                $caller = $stack[0];
                
                // Gather code location data
                $s = '';
                if (array_key_exists('file',$caller))
                    $s .= $caller['file'].':'.$caller['line'].' ';
                
                if(array_key_exists('class', $caller)) {
                    if(!array_key_exists('type', $caller)) $caller['type'] = ' ';
                    if($caller['type'] == '::') {
                        $s .= $caller['class'].'::';
                    } else $s .= '('.$caller['class'].')'.$caller['type'];
                }
                
                // Resolve magics so that log is easier to read
                if(in_array($caller['function'], array('__call', '__callStatic'))) {
                    $caller['function'] = $caller['args'][0];
                    $caller['args'] = $caller['args'][1];
                }
                
                // Add arguments (objects are mentionned as just "object" without details)
                $args = array();
                foreach($caller['args'] as $arg) {
                    $a = '';
                    if(is_bool($arg)) {
                        $a = $arg ? '(true)' : '(false)';
                    } else if(is_scalar($arg)) {
                        $a = '('.$arg.')';
                    } else if(is_array($arg)) {
                        $a = array();
                        foreach($arg as $k => $v) $a[] = (is_numeric($k) ? '' : $k.' => ').gettype($v).(is_scalar($v) ? (is_bool($v) ? ($v ? '(true)' : '(false)') : '('.$v.')') : '');
                        $a = '('.implode(', ', $a).')';
                    }
                    $args[] = gettype($arg).$a;
                }
                
                $s .= $caller['function'].'('.implode(', ', $args).')';
                
                $message = $s.' '.$message;
            }
        }
        
        $process = ApplicationContext::getProcess();
        
        // Add authenticated user id if any, except in debug mode as line is already long
        if($process != ProcessTypes::UPGRADE) {
            try {
                // No user id in log if we are recording a low level exception
                // as it may end up in throwing another one while getting
                // user / user id and create a loop ...
                $risky_exception = count(array_filter(debug_backtrace(), function($t) {
                    return array_key_exists('class', $t) && preg_match('`^(Core|Config|DBI).+Exception$`', $t['class']);
                }));
                
                if($level != LogLevels::DEBUG && !$risky_exception && Auth::user())
                    $message = '[user:'.Auth::user()->id.'] '.$message;
                
            } catch(Exception $e) {}
        }
        
        // Build final message ...
        $message = '['.$process.':'.ApplicationContext::getThreadUID().':'.$level.'] '.$message;
        
        // ... and give it to defined facilities
        foreach(self::$facilities as $facility) {
            // Filter based on process if facility requires it
            if(array_key_exists('process', $facility)) {
                $accepted = array_filter(array_map('trim', preg_split('`[\s,|]`', $facility['process'])));
                if(!in_array('*', $accepted) && !in_array($process, $accepted))
                    continue;
            }
            
            // Filter based on level if facility requires it
            if(self::$levels[$level] > self::$levels[$facility['level']])
                continue;
            
            // Forward to facility related method with config
            call_user_func(array($facility['logger'], 'log'), $level, $message);
        }
    }
}
