<?php

/**
 *     Moment - FormFieldText.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Text form field generator / validator
 *
 * Accepts properties :
 *   - required boolean
 *   - placeholder string
 *   - pattern string
 *   - size int
 *   - maxlength int
 */
class FormFieldText extends FormFieldSingle {
    /**
     * HTML type
     */
    const HTML_TYPE = 'text';
    
    /**
     * @var string placeholder
     */
    protected $placeholder = null;
    
    /**
     * @var string pattern
     */
    protected $pattern = null;
    
    /**
     * @var string pcre_pattern
     */
    protected $pcre_pattern = null;
    
    /**
     * @var int size
     */
    protected $size = null;
    
    /**
     * @var int maxlength
     */
    protected $maxlength = null;
    
    
    /**
     * Get constraints
     *
     * @return array
     */
    public function getConstraints() {
        $constraints = parent::getConstraints();
        
        if($this->maxlength)
            $constraints['maxlength'] = $this->maxlength;
        
        if($this->pattern)
            $constraints['pattern'] = $this->pattern;
        
        return $constraints;
    }
    
    /**
     * Get HTML input node
     *
     * @return string
     */
    public function getHTMLInput() {
        $html = '<input type="'.static::HTML_TYPE.'"';
        
        $html .= ' name="'.htmlspecialchars($this->name).'"';
        
        foreach(array('placeholder', 'size', 'maxlength', 'pattern') as $p)
            if($this->$p)
                $html .= ' '.$p.'="'.htmlspecialchars($this->$p).'"';
        
        if($this->required)
            $html .= ' required';
        
        if($this->read_only)
            $html .= ' readonly';
        
        if(!is_null($this->value))
            $html .= ' value="'.htmlspecialchars($this->value).'"';
        
        $html .= ' />'."\n";
        
        return $html;
    }
    
    /**
     * Validate data
     *
     * @param mixed $data
     *
     * @throws FormDataValidationException
     */
    public function validateData($data) {
        parent::validateData($data);
        
        if($this->maxlength && (strlen($data) > $this->maxlength))
            throw new FormDataValidationException($this, 'max_length_exceeded');
        
        $pattern = $this->pcre_pattern ? $this->pcre_pattern : $this->pattern;
        
        if ($pattern && !preg_match('`'.$pattern.'`u', $data))
            throw new FormDataValidationException($this, 'pattern_not_matched');
    }
    
    /**
     * Getter
     *
     * @param string $property
     *
     * @throws PropertyAccessException
     *
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'placeholder', 'pattern', 'pcre_pattern', 'size', 'maxlength'
        ))) return $this->$property;
        
        return parent::__get($property);
    }
    
    /**
     * Setter
     *
     * @param string $property
     * @param mixed $value
     *
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if(in_array($property, array('placeholder'))) {
            $this->$property = (string)$value;
            
        } else if(in_array($property, array('pattern', 'pcre_pattern'))) {
            $this->$property = $value;
            
        } else if(in_array($property, array('size', 'maxlength'))) {
            if(!is_int($value))
                throw new FormFieldBadPropertyException($property, 'not_integer');
            
            if($value < 1)
                throw new FormFieldBadPropertyException($property, 'less_than_1');
            
            $this->$property = $value;
            
        } else {
            parent::__set($property, $value);
        }
    }
}
