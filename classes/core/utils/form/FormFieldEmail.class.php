<?php

/**
 *     Moment - FormFieldEmail.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Email form field emails generator / validator
 * 
 * Accepts properties :
 *   - required boolean
 *   - placeholder string
 *   - pattern string
 *   - size int
 *   - maxlength int
 */
class FormFieldEmail extends FormFieldText {
    /**
     * HTML type
     */
    const HTML_TYPE = 'email';

    /**
     * Validate data
     *
     * @param mixed $data
     *
     * @throws FormDataValidationException
     */
    public function validateData($data) {
        parent::validateData($data);
        
        if($data && !filter_var($data, FILTER_VALIDATE_EMAIL))
            throw new FormDataValidationException($this, 'not_an_email');
    }
}
