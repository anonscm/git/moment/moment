<?php

/**
 *     Moment - FormFieldSelect.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Select form field generator / validator
 */
class FormFieldSelect extends FormFieldEnum {
    /**
     * @var boolean multiple
     */
    protected $multiple = false;
    
    /**
     * Get HTML input node
     *
     * @return string
     */
    public function getHTMLInput() {
        $html = '<select name="'.htmlspecialchars($this->name).'"';
        
        if($this->read_only)
            $html .= ' disabled';
        
        if($this->required)
            $html .= ' required';
        if($this->multiple)
            $html .= ' multiple="multiple"';
        
        $html .= '>'."\n";
        
        foreach($this->values as $value => $label) {
            $html .= "\t".'<option value="'.htmlspecialchars($value).'"';
            
            if (!is_null($this->value)){
                if ((is_array($this->value) && in_array($value, $this->value)) || $value == $this->value ){
                    $html .= ' selected="selected"';
                }
            }
            
            $html .= '>';
            $html .= is_null($label) ? $value : $label;
            $html .= '</option>'."\n";;
        }
        
        $html .= '</select>'."\n";
        
        return $html;
    }
    
    /**
     * Validate data
     *
     * @param mixed $data
     *
     * @throws FormDataValidationException
     */
    public function validateData($data) {
        if ($this->multiple){
            if (!is_array($data))
                throw new FormDataValidationException($this, 'not_array');
            
            foreach ($data as $datum) {
                parent::validateData($datum);
            }
        }else{
            parent::validateData($data);
        }
    }
    
    /**
     * Getter
     *
     * @param string $property
     *
     * @throws PropertyAccessException
     *
     * @return mixed
     */
    public function __get($property) {
        if ($property === 'multiple'){
            return $this->multiple;
        }else{
            return parent::__get($property);
        }
    }
    
    /**
     * Setter
     *
     * @param string $property
     * @param mixed $value
     *
     * @throws FormBadPropertyException
     */
    public function __set($property, $value) {
        if($property == 'multiple') {
            if (!is_bool($value))
                throw new FormBadPropertyException('multiple', 'not_boolean');
            
            $this->multiple = $value;
            
        } else {
            parent::__set($property, $value);
        }
    }
}
