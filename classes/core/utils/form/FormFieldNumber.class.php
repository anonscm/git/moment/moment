<?php

/**
 *     Moment - FormFieldNumber.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Number form field generator / validator
 * 
 * Accepts properties :
 *   - required boolean
 *   - min int
 *   - max int
 *   - step int
 *   - size int
 */
class FormFieldNumber extends FormFieldSingle {
    /**
     * HTML type
     */
    const HTML_TYPE = 'number';
    
    /**
     * @var int min
     */
    protected $min = null;
    
    /**
     * @var int max
     */
    protected $max = null;
    
    /**
     * @var int step
     */
    protected $step = null;
    
    /**
     * @var int size
     */
    protected $size = null;
    
    /**
     * Get constraints
     * 
     * @return array
     */
    public function getConstraints() {
        $constraints = parent::getConstraints();
        
        if(!is_null($this->min))
            $constraints['min'] = $this->min;
        
        if(!is_null($this->max))
            $constraints['max'] = $this->max;
        
        return $constraints;
    }
    
    /**
     * Get HTML input node
     * 
     * @return string
     */
    public function getHTMLInput() {
        $html = '<input type="'.static::HTML_TYPE.'"';
        
        $html .= ' name="'.htmlspecialchars($this->name).'"';
        
        foreach(array('min', 'max', 'step', 'size') as $p)
            if(!is_null($this->$p))
                $html .= ' '.$p.'="'.htmlspecialchars($this->$p).'"';
        
        if($this->required)
            $html .= ' required';
        
        if(is_null($this->value)) {
            $value = min((int)$this->min, (int)$this->max);
            $html .= ' value="'.$value.'"';
            
        } else {
            $html .= ' value="'.htmlspecialchars($this->value).'"';
        }
        
        $html .= ' />'."\n";
        
        return $html;
    }

    /**
     * Validate data
     *
     * @param mixed $data
     *
     * @throws FormDataValidationException
     */
    public function validateData($data) {
        parent::validateData($data);
        
        if(!is_null($this->min) && ((int)$data < $this->min))
            throw new FormDataValidationException($this, 'minimum_exceeded');
    
        if(!is_null($this->max) && ((int)$data > $this->max))
            throw new FormDataValidationException($this, 'maximum_exceeded');
    }
    
    /**
     * Getter
     * 
     * @param string $property
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'min', 'max', 'step', 'size'
        ))) return $this->$property;
        
        return parent::__get($property);
    }

    /**
     * Setter
     *
     * @param string $property
     * @param mixed $value
     *
     * @throws FormFieldBadPropertyException
     */
    public function __set($property, $value) {
        if(in_array($property, array('min', 'max'))) {
            if (!is_int($value))
                throw new FormFieldBadPropertyException($property, 'not integer');
            
            $this->$property = $value;
            
            if (!is_null($this->min) && !is_null($this->max) && ($this->min >= $this->max))
                throw new FormFieldBadPropertyException($property, 'minimum_over_maximum');
            
        } else if ($property === 'step'){
            if (!is_float($value))
                throw new FormFieldBadPropertyException($property, 'not_float');
            
            $this->$property = $value;
            
        } else if(in_array($property, array('size'))) {
            if(!is_int($value))
                throw new FormFieldBadPropertyException($property, 'not_integer');
            
            if($value < 1)
                throw new FormFieldBadPropertyException($property, 'less_than_1');
            
            $this->$property = $value;
            
        } else {
            parent::__set($property, $value);
        }
    }
}
