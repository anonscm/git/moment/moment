<?php

/**
 *     Moment - FormFieldRange.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Range form field generator / validator
 * 
 * Accepts properties :
 *   - required boolean
 *   - step int
 *   - size int
 */
class FormFieldRange extends FormFieldNumber {
    /**
     * HTML type
     */
    const HTML_TYPE = 'range';

    /**
     * Constructor
     *
     * @param string $name
     * @param int $min
     * @param int $max
     * @param array $properties
     * @param int $value
     *
     * @throws FormFieldBadPropertyException
     */
    public function __construct($name, $min, $max, array $properties = array(), $value = null) {
        if(!is_int($min))
            throw new FormFieldBadPropertyException('min', 'must_be_integer');
        
        $properties['min'] = (int)$min;
        
        if(!is_int($max))
            throw new FormFieldBadPropertyException('max', 'must_be_integer');
        
        $properties['max'] = (int)$max;
        
        if(!is_null($value) && !is_int($value))
            throw new FormFieldBadPropertyException('value', 'must_be_integer');
        
        parent::__construct($name, $properties, $value);
    }
}
