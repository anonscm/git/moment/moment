<?php

/**
 *     Moment - FormFieldDate.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Date form field generator / validator
 * 
 * Accepts properties :
 *   - required boolean
 *   - min int
 *   - max int
 */
class FormFieldDate extends FormFieldSingle {
    /**
     * Date type
     */
    const DATE_TYPE = 'date';
    
    /**
     * @var int min
     */
    protected $min = null;
    
    /**
     * @var int max
     */
    protected $max = null;
    
    /**
     * @var string timezone
     */
    protected $timezone = null;
    
    /**
     * Get constraints
     * 
     * @return array
     */
    public function getConstraints() {
        $constraints = parent::getConstraints();
        
        if(!is_null($this->min))
            $constraints['date_min'] = $this->min;
        
        if(!is_null($this->max))
            $constraints['date_max'] = $this->max;
        
        return $constraints;
    }
    
    /**
     * Get HTML input node
     * 
     * @return string
     */
    public function getHTMLInput() {
        $html = ' <input type="text" data-picker="'.static::DATE_TYPE.'"';
        
        $html .= ' name="'.htmlspecialchars($this->name).'"';
        
        foreach($this->getAttributes() as $k => $v)
            $html .= ' data-'.$k.'="'.htmlspecialchars($v).'"';
        
        if($this->required)
            $html .= ' required';
        
        if(!is_null($this->value)) {
            if($this instanceof FormFieldDateTime) {
                $html .= ' value="'.htmlspecialchars(Utilities::formatDate($this->value, true, null, $this->timezone)).'"';
                
            } else if ($this instanceof FormFieldTime) {
                $html .= ' value="'.htmlspecialchars(Utilities::formatTime($this->value, false, $this->timezone)).'"';
                
            } else {
                $html .= ' value="'.htmlspecialchars(Utilities::formatDate($this->value, false, null, $this->timezone)).'"';
            }
        }
        
        $html .= ' />';
        
        return $html;
    }
    
    /**
     * Validate data
     *
     * @param mixed $data
     *
     * @throws FormDataValidationException
     */
    public function validateData($data) {
        parent::validateData($data);
        
        // Data should be an unix timestamp
        if(!is_int($data))
            throw new FormDataValidationException($this, 'bad_format');
        
        if(!is_null($this->min) && ($data < $this->min))
            throw new FormDataValidationException($this, 'minimum_exceeded');
        
        if(!is_null($this->max) && ($data > $this->max))
            throw new FormDataValidationException($this, 'maximum_exceeded');
    }
    
    
    /**
     * @see parent::getAttributes
     */
    public function getAttributes(){
        $attributes = parent::getAttributes();
        
        if ($this->min)
            $attributes['picker-min'] = $this->min;
        
        if ($this->max)
            $attributes['picker-max'] = $this->max;
        
        if ($this->timezone)
            $attributes['timezone'] = $this->timezone;
        
        return $attributes;
    }
    
    
    /**
     * Getter
     * 
     * @param string $property
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'min', 'max','timezone'
        ))) return $this->$property;
        
        return parent::__get($property);
    }
    
    /**
     * Setter
     *
     * @param string $property
     * @param mixed $value
     *
     * @throws FormFieldBadPropertyException
     */
    public function __set($property, $value) {
        if(in_array($property, array('min', 'max'))) {
            if(!is_int($value))
                throw new FormFieldBadPropertyException($property, 'not_integer');
            
            $this->$property = ($value - ($value % 86400));
            
            if(!is_null($this->min) && !is_null($this->max) && ($this->min >= $this->max))
                throw new FormFieldBadPropertyException($property, 'minimum_over_maximum');
            
        } else if ($property === 'timezone'){
            if (!is_string($value))
                throw new FormFieldBadPropertyException($property, 'not_a_string');
            
            $this->timezone = $value;
            
        } else {
            parent::__set($property, $value);
        }
    }
}
