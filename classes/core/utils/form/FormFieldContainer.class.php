<?php

/**
 *     Moment - FormFieldContainer.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Form field container
 */
abstract class FormFieldContainer {
    /**
     * @var string unique form identifier
     */
    protected $id = null;
    
    /**
     * @var (FormField)[] fields
     */
    protected $fields = array();


    /**
     * Constructor
     *
     * @param string $id
     * @param array $fields
     *
     * @throws FormBadPropertyException
     * @throws PropertyAccessException
     */
    public function __construct($id, array $fields = array()) {
        if(!is_string($id) || !$id || !preg_match('`^[a-z][a-z0-9_-]*$`i', $id))
            throw new FormBadPropertyException('id', 'bad_or_missing_identifier');
        
        $this->id = (string)$id;
        
        $this->__set('fields', $fields);
    }

    /**
     * Add field
     *
     * @param FormField $field
     *
     * @throws FormBadPropertyException
     */
    public function addField($field) {
        if (!is_scalar($field) && !is_object($field) && !($field instanceof FormField) && !($field instanceof FormFieldContainer))
            throw new FormBadPropertyException('field', 'not_FormField_or_FormFieldContainer_or_scalar');
    
        $this->fields[] = $field;
    }
    
    /**
     * Add fields
     * 
     * @param array $fields
     */
    public function addFields($fields) {
        foreach($fields as $field)
            $this->addField($field);
    }
    
    
    /**
     * Get HTML representation
     * 
     * @return string
     */
    public abstract function getHTML();
    
    
    /**
     * Allows to get fields
     * 
     * @return array
     */
    public function getFields(){
        $fields = array();
        foreach ($this->fields as $field) {
            if ($field instanceof FormField) $fields[] = $field;
            
            if ($field instanceof FormFieldContainer) 
                $fields = array_merge($fields, $field->getFields());
        }
        return $fields;
    }

    
    /**
     * Getter
     * 
     * @param string $property
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'id', 'fields'
        ))) return $this->$property;
        
        if($property == 'path') return $this->id;
        
        throw new PropertyAccessException($this, $property);
    }
    
    /**
     * Setter
     * 
     * @param string $property
     * @param mixed $value
     * 
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if($property == 'fields') {
            $this->fields = array();
            $this->addFields($value);
            
        } else {
            throw new PropertyAccessException($this, $property);
        }
    }
}
