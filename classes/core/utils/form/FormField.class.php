<?php

/**
 *     Moment - FormField.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Abstract form field generator / validator
 */
abstract class FormField {
    /**
     * @var string $name
     */
    protected $name = null;
    
    /**
     * @var string label
     */
    protected $label = null;
    
    /**
     * @var FormField parent field
     */
    protected $parent = null;
    
    /**
     * Constructor
     * 
     * @param string $name
     */
    public function __construct($name) {
        if(!$name || !is_string($name))
            throw new FormFieldBadNameException();
        
        $this->name = $name;
    }
    
    /**
     * Get constraints
     * 
     * @return array
     */
    abstract public function getConstraints();
    
    /**
     * Get constraints as HTML string
     * 
     * @return string
     */
    public function getHTMLConstraints() {
        if (property_exists($this, 'constraints') && !is_null($this->constraints)){
            return ' <span  data-constraint data-label-constraints>'.Lang::tr($this->constraints).'</span>';
        }
        
        $html = array();
        foreach($this->getConstraints() as $k => $r) {
            $c = Lang::tr('constraint_'.$k);
            
            if(is_scalar($r)) {
                $c = $c->r(array('value' => $r));
                
            } else if(is_array($r)) {
                if (array_key_exists('about', $r))
                    $r['about'] = (string)Lang::tr($r['about']);
                
                $c = $c->r($r);
            }
            
            $html[] = '<span data-constraint="'.$k.'">'.$c.'</span>';
        }
        
        if(!count($html))
            return '';
        
        $sep = (string)Lang::tr('constraints_separator');
        return ' <span data-label-constraints>'.implode($sep, $html).'</span>';
    }
    
    /**
     * Get HTML representation
     * 
     * @return string
     */
    abstract public function getHTML();
    
    /**
     * Validate data
     * 
     * @param mixed $data
     * 
     * @throws FormDataValidationFailedException
     */
    abstract public function validateData($data);
    
    /**
     * Getter
     * 
     * @param string $property
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'name', 'label', 'parent'
        ))) return $this->$property;
        
        if($property == 'html') return $this->getHTML();
        
        if($property == 'path') return ($this->parent ? $this->parent->path.'.' : '').$this->name;
        
        throw new PropertyAccessException($this, $property);
    }
    
    /**
     * Setter
     * 
     * @param string $property
     * @param mixed $value
     * 
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if($property == 'label') {
            $this->label = $value;
            
        } else if($property == 'parent') {
            if(!($value instanceof FormField))
                throw new FormFieldBadPropertyException('parent', 'not_a_formfield');
            
            $this->parent = $value;
            
        } else {
            throw new PropertyAccessException($this, $property);
        }
    }
    
    /**
     * Stringifier
     * 
     * @return string
     */
    public function __toString() {
        return get_class($this).'#'.$this->path;
    }
}
