<?php

/**
 *     Moment - FormControl.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Abstract form control generator
 */
abstract class FormControl {
    /**
     * Action
     */
    const ACTION = 'null';
    
    /**
     * @var string rest resource
     */
    private $resource = null;
    
    /**
     * @var array HTML attributes
     */
    private $attributes = null;
    
    /**
     * Constructor
     * 
     * @param string $resource
     * @param array $attributes
     */
    public function __construct($resource = null, array $attributes = array()) {
        if($resource)
            $this->__set('resource', $resource);
        
        $this->attributes = $attributes;
    }
    
    /**
     * Get action name
     * 
     * @return string
     */
    public static function getAction() {
        return strtolower(substr(get_called_class(), 11));
    }
    
    /**
     * Get HTML representation
     * 
     * @return string
     */
    public function getHTML() {
        $action = static::getAction();
        
        $html = '<button class="button float-right" ';
        
        $html .= ' data-control="'.$action.'"';
        
        if($this->resource)
            $html .= ' data-control-resource="'.$this->resource.'"';
        
        foreach($this->attributes as $k => $v)
            if(!is_null($v) && ($k != 'label'))
                $html .= ' data-control-'.$k.'="'.htmlspecialchars($v).'"';
        
        $html .= '>';
        
        $html .= Lang::tr(array_key_exists('label', $this->attributes) ? $this->attributes['label'] : $action);
        
        $html .= '</button>';
        
        return $html;
    }
    
    /**
     * Getter
     * 
     * @param string $property
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array('resource')))
            return $this->$property;
        
        if($property == 'action') return static::getAction();
        
        throw new PropertyAccessException($this, $property);
    }

    /**
     * Setter
     *
     * @param string $property
     * @param mixed $value
     *
     * @throws FormControlBadResourceException
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if($property == 'resource') {
            if(!preg_match('`^/[a-z]`i', (string)$value))
                throw new FormControlBadResourceException($value);
            
            $this->resource = (string)$value;
            
        } else {
            throw new PropertyAccessException($this, $property);
        }
    }
    
    /**
     * Stringifier
     * 
     * @return string
     */
    public function __toString() {
        return 'FormControl#'.static::getAction();
    }
}
