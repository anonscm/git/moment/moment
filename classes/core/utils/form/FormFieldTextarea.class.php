<?php

/**
 *     Moment - FormFieldTextarea.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Text area form field generator / validator
 * 
 * Accepts properties :
 *   - required boolean
 *   - placeholder string
 *   - maxlength int
 */
class FormFieldTextarea extends FormFieldText {
    /**
     * Get HTML input node
     * 
     * @return string
     */
    public function getHTMLInput() {
        $html = '<textarea';
        
        $html .= ' name="'.htmlspecialchars($this->name).'"';
        
        foreach(array('placeholder', 'maxlength') as $p)
            if($this->$p)
                $html .= ' '.$p.'="'.htmlspecialchars($this->$p).'"';
        
        if($this->required)
            $html .= ' required';
        
        $html .= '>';
        
        if(!is_null($this->value))
            $html .= htmlspecialchars($this->value);
        
        $html .= '</textarea>'."\n";
        
        return $html;
    }
    
    /**
     * Getter
     * 
     * @param string $property
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array('pattern', 'size')))
            throw new PropertyAccessException($this, $property);
        
        return parent::__get($property);
    }
    
    /**
     * Setter
     * 
     * @param string $property
     * @param mixed $value
     * 
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if(in_array($property, array('pattern', 'size')))
            throw new PropertyAccessException($this, $property);
        
        parent::__set($property, $value);
    }
}
