<?php

/**
 *     Moment - FormFieldTime.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Time form field generator / validator
 * 
 * Accepts properties :
 *   - required boolean
 *   - min int
 *   - max int
 */
class FormFieldTime extends FormFieldDate {
    /**
     * Date type
     */
    const DATE_TYPE = 'time';
    
    /**
     * @var string $step
     */
    protected $step = 3600;
    
        
    /**
     * @var integer $step
     */
    protected $defaultTime = 0;
    
    
    /**
     * Get constraints
     * 
     * @return array
     */
    public function getConstraints() {
        $constraints = parent::getConstraints();
        
        unset ($constraints['date_min']);
        unset ($constraints['date_max']);
        
        if(!is_null($this->min))
            $constraints['time_min'] = $this->min;
        
        if(!is_null($this->max))
            $constraints['time_max'] = $this->max;
        
        return $constraints;
    }

    /**
     * Validate data
     *
     * @param mixed $data
     *
     * @throws FormDataValidationException
     */
    public function validateData($data) {
        parent::validateData($data);
        
        // Data shouzzld be an unix timestamp
        if(!is_int($data))
            throw new FormDataValidationException($this, 'bad_format');
        
        if($data < 0)
            throw new FormDataValidationException($this, 'negative_time');
        
        if($data > 24 * 3600)
            throw new FormDataValidationException($this, 'over_24h');
        
        if(!is_null($this->min) && ($data < $this->min))
            throw new FormDataValidationException($this, 'minimum_exceeded');
        
        if(!is_null($this->max) && ($data > $this->max))
            throw new FormDataValidationException($this, 'maximum_exceeded');
    }


    /**
     * Setter
     *
     * @param string $property
     * @param mixed $value
     *
     * @throws FormFieldBadPropertyException
     */
    public function __set($property, $value) {
        
        if(in_array($property, array('min', 'max', 'step', 'defaultTime'))) {
            if(!is_int($value))
                throw new FormFieldBadPropertyException($property, 'not_an_int');
    
            if((int)$value < 0)
                throw new FormFieldBadPropertyException($property, 'negative_time');
    
            if((int)$value > 24 * 3600)
                throw new FormFieldBadPropertyException($property, 'over_24h');
            
            $this->$property = $value;
            
        }else{
            parent::__set($property, $value);
        }
    }
    
    
    /**
     * @see parent::getAttributes
     */
    public function getAttributes(){
        $attributes = parent::getAttributes();
        
        $attributes['picker-step'] = $this->step;
        
        $attributes['picker-default-time'] = $this->defaultTime;
        
        return $attributes;
    }


    /**
     * Getter
     *
     * @see parent::__get
     *
     * @param string $property
     *
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'step', 'defaultTime'
        ))) return $this->$property;
        
        return parent::__get($property);
    }
}