<?php

/**
 *     Moment - FormFieldDateTime.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Date and time form field generator / validator
 * 
 * Accepts properties :
 *   - required boolean
 *   - min int
 *   - max int
 */
class FormFieldDateTime extends FormFieldDate {
    /**
     * Date type
     */
    const DATE_TYPE = 'datetime';
    
    /**
     * Get constraints
     * 
     * @return array
     */
    public function getConstraints() {
        $constraints = parent::getConstraints();
    
        unset ($constraints['date_min']);
        unset ($constraints['date_max']);
        
        if(!is_null($this->min))
            $constraints['datetime_min'] = $this->min;
        
        if(!is_null($this->max))
            $constraints['datetime_max'] = $this->max;
        
        return $constraints;
    }
    
    
    /**
     * Setter
     *
     * @param string $property
     * @param mixed $value
     *
     * @throws FormFieldBadPropertyException
     */
    public function __set($property, $value) {
        if(in_array($property, array('min', 'max'))) {
            if(!is_null($value) && !is_int($value))
                throw new FormFieldBadPropertyException($property, 'not_integer');
            
            $this->$property = $value;
            
            if(!is_null($this->min) && !is_null($this->max) && ($this->min >= $this->max))
                throw new FormFieldBadPropertyException($property, 'minimum_over_maximum');
            
        } else{
            parent::__set($property, $value);
        }
    }
}
