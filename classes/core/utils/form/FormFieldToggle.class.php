<?php

/**
 *     Moment - FormFieldToggle.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Multiple form field generator / validator
 * 
 * Accepts properties :
 *   - min int
 *   - max int
 *   - start int
 */
class FormFieldToggle extends FormFieldContainer{
    
    
    /**
     * @var boolean $state
     */
    protected $state = false;
    
    /**
     * @var (FormField)[] $fields
     */
    protected $fields = array();
    
    /**
     * @var string $uid
     */
    protected $uid = null;
    
    /**
     * @var string $label
     */
    protected $label = null;
    
    
    /**
     * Constructor
     * 
     * @param string $name
     * @param string $label
     * @param array $fields
     * @param array $properties
     */
    public function __construct($name, $label = null, array $fields = array(), $properties = array()) {
        parent::__construct($name, $fields);
        
        $this->uid = $name.'_'.uniqid();
        
        $this->label = $label;
                
        foreach($properties as $k => $v)
            $this->__set($k, $v);

    }
    
    /**
     * Get HTML representation
     * 
     * @return string
     */
    public function getHTML() {
        $html = '<fieldset';
        
        $html .= ' data-field-toggle="'.$this->id.'"';
        $html .= ' data-field-state="'.($this->state ? '1' : '').'"';
        
        $html .= '>'."\n";
        
        $html .= '<a data-toggle="'.  $this->uid.'">';
        $html .= Lang::tr($this->label ? $this->label : 'toggle');
        $html .= '</a>';
        
        $html .= '<div class="callout" id='.$this->uid.' data-toggler data-animate="fade-in fade-out"';
        if ($this->state !== true) $html .= ' style="display:none" aria-expanded="false" ';
        $html .= ' >';
        
        foreach($this->fields as $field) {
            if(is_object($field) && ($field instanceof FormField)) {
                $html .= $field->getHTML()."\n";
            } else {
                $html .= $field."\n";
            }
        }
        
        $html .= '</div>';
        
        $html .= '</fieldset>'."\n";
        
        return $html;
    }
    
    /**
     * Getter
     * 
     * @param string $property
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'state', 'fields', 'uid', 'label'
        ))) return $this->$property;
        
        if($property === 'path') return ($this->parent ? $this->parent->path.'.' : '').$this->name.'[]';
        
        return parent::__get($property);
    }
    
    /**
     * Setter
     * 
     * @param string $property
     * @param mixed $value
     * 
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if($property == 'state') {
            $this->state = (bool) $value;
            
        } else if($property == 'label') {
            $this->label = (string) $value;
            
        } else {
            parent::__set($property, $value);
        }
    }
}
