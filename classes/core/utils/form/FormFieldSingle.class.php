<?php

/**
 *     Moment - FormFieldSingle.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Single form field generator / validator
 * 
 * Accepts properties :
 *   - required boolean
 */
abstract class FormFieldSingle extends FormField {
    /**
     * @var boolean required field
     */
    protected $required = false;
    
    /**
     * @var mixed value
     */
    protected $value = null;
    
    /**
     * @var string hint
     */
    protected $hint = null;
    
    /**
     * @var string constraints alternative text
     */
    protected $constraints = null;
    
    /**
     * @var boolean read_only input
     */
    protected $read_only = false;
    
    /**
     * Constructor
     * 
     * @param string $name
     * @param array $properties
     * @param mixed $value
     */
    public function __construct($name, array $properties = array(), $value = null) {
        parent::__construct($name);
        
        foreach($properties as $k => $v)
            $this->__set($k, $v);
        
        $this->value = $value;
    }
    
    /**
     * Validate data
     * 
     * @param mixed $data
     * 
     * @throws FormDataValidationFailedException
     */
    public function validateData($data) {
        // Data shoud be scalar
        if(!is_scalar($data) && !is_null($data))
            throw new FormDataValidationException($this, 'not_scalar');
        
        if($this->required && !strlen($data))
            throw new FormDataValidationException($this, 'required');
    }
    
    /**
     * Get constraints
     * 
     * @return array
     */
    public function getConstraints() {
        $constraints = array();
        
        if($this->required)
            $constraints['mandatory'] = true;
        
        return $constraints;
    }
    
    /**
     * Get HTML container parts
     * 
     * @return array of HTML prefix, label part and suffix
     */
    public function getHTMLContainer() {
        $html = array('header' => '', 'label' => '', 'footer' => '');
        
        $html['header'] = '<label ';
        
        if($this->required)
            $html['header'] .= ' data-mandatory';
        
        $html['header'] .= '>';
        
        if ($this->label !== false){
            $label = Lang::tr($this->label ? $this->label : $this->name);
            $html['label'] = ' <span data-label-text>'.$label.'</span>';
        }
        
        $html['label'] .= ' '.$this->getHTMLConstraints().' ';
        
        $html['footer'] = '';
        if($this->hint) {
            $hint = is_string($this->hint) ? Lang::tr($this->hint) : $this->hint;
            $html['footer'] .= '<p data-hint class="helptext">'.(string)$hint.'</p>';
        }
        $html['footer'] .= '</label>';
        
        return $html;
    }
    
    /**
     * Get HTML input node
     * 
     * @return string
     */
    abstract public function getHTMLInput();
    
    /**
     * Get HTML representation
     * 
     * @return string
     */
    public function getHTML() {
        $html = $this->getHTMLContainer();
        $input = $this->getHTMLInput();
        
        return $html['header'].$html['label'].$input.$html['footer'];
    }
    
     
    /**
     * Get HTML attributes in array
     * 
     * @return array List of attributes
     */
    public function getAttributes(){
        return array();
    }
    
    
    /**
     * Getter
     * 
     * @param string $property
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'required', 'value', 'hint', 'constraints', 'read_only'
        ))) return $this->$property;
        
        return parent::__get($property);
    }
    
    /**
     * Setter
     * 
     * @param string $property
     * @param mixed $value
     * 
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if($property == 'required') {
            $this->required = (bool)$value;
            
        } else if($property == 'value') {
            $this->value = $value;
            
        } else if($property == 'constraints') {
            $this->constraints = $value;
            
        } else if($property == 'hint') {
            $this->hint = $value;
    
        } else if($property === 'read_only') {
            if(!is_bool($value))
                throw new FormFieldBadPropertyException($property, 'not_boolean');
    
            $this->$property = $value;
            
        } else {
            parent::__set($property, $value);
        }
    }
}
