<?php

/**
 *     Moment - FormFieldMultipleEmails.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Multiple form field email generator / validator
 * 
 * Accepts properties :
 *   - min int
 *   - max int
 *   - start int
 */
class FormFieldMultipleEmails extends FormFieldMultiple {
    
    /**
     * Constructor
     * 
     * @param string $name
     * @param array $properties
     * @param array $values
     */
    public function __construct($name, array $properties = array(), array $values = null) {
        parent::__construct($name, array(
            new FormFieldEmail('email', array(
                'label' => false
            ))
        ), $properties, $values);
    }
    

    /**
     * Get HTML representation
     * 
     * @return string
     */
    public function getHTML() {
        $html = explode(' ', parent::getHTML(), 2);
        return $html[0].' data-multiple-emails '.$html[1];
    }
}
