<?php

/**
 *     Moment - Form.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Form generator / validator
 */
class Form extends FormFieldContainer{
    
    /**
     * @var (FormControl)[] controls
     */
    private $controls = array();
    
    /**
     * @var bool
     */
    private $warnEdited = true;
    
    /**
     * Constructor
     *
     * @param string $id
     * @param array $fields
     * @param array $controls
     * @param bool $warnEdited
     */
    public function __construct($id, array $fields = array(), $controls = array(), $warnEdited = true) {
        parent::__construct($id, $fields);
        
        $this->__set('controls', $controls);
        
        $this->warnEdited = (bool) $warnEdited;
    }


    /**
     * Add control
     *
     * @param mixed $control
     *
     * @throws FormBadPropertyException
     */
    public function addControl($control) {
        if(!is_string($control) && !($control instanceof FormControl))
            throw new FormBadPropertyException('control', 'not string or FormControl');
        
        $this->controls[] = $control;
    }
    
    /**
     * Add controls
     * 
     * @param array $controls
     */
    public function addControls($controls) {
        foreach($controls as $control)
            $this->addControl($control);
    }
    
    /**
     * Get HTML representation
     * 
     * @return string
     */
    public function getHTML() {
        $html = '<fieldset class="small-11" data-form="'.htmlspecialchars($this->id).'" '.($this->warnEdited ? ' data-warn-edited ':'').'>'."\n";
        
        foreach($this->fields as $field) {
            if(is_object($field)) {
                $html .= $field->getHTML()."\n";
            } else {
                $html .= $field."\n";
            }
        }
        
        if(count($this->controls)) {
            $html .= '<fieldset data-controls="1">'."\n";
            
            foreach($this->controls as $control) {
                if(is_object($control) && ($control instanceof FormControl)) {
                    $html .= '  '.$control->getHTML()."\n";
                } else {
                    $html .= '  '.$control."\n";
                }
            }
            
            $html .= '</fieldset>'."\n";
        }
        
        $html .= '</fieldset>'."\n";
        
        return $html;
    }

    /**
     * Validate data
     *
     * @param mixed $data
     *
     * @throws FormDataValidationException
     */
    public function validateData($data) {
        // Data shoud be a data structure
        if(is_object($data)) $data = (array)$data;
        
        if(!is_array($data))
            throw new FormDataValidationException($this, 'not_form_data');
        
        if(count(array_filter(array_keys($data), 'is_int')))
            throw new FormDataValidationException($this, 'not_form_data');
        
        $fields = $this->getFields();
        
        // Propagate checks
        foreach($fields as $field) {
            $field->validateData(array_key_exists($field->name, $data) ? $data[$field->name] : null);
        }
    }
    
    
    /**
     * Setter
     * 
     * @param string $property
     * @param mixed $value
     * 
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if($property == 'controls') {
            $this->controls = array();
            $this->addControls($value);
            
        } else {
            parent::__set($property, $value);
        }
    }
}
