<?php

/**
 *     Moment - FormFieldCheckbox.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Checkbox form field generator / validator
 * 
 * Accepts properties :
 *   - required boolean
 */
class FormFieldCheckbox extends FormFieldSingle {
    /**
     * Get HTML input node
     * 
     * @return string
     */
    public function getHTMLInput() {
        $input = '<input type="checkbox"';
        
        $input .= ' name="'.htmlspecialchars($this->name).'"';
        
        if($this->required)
            $input .= ' required';
        
        if($this->value)
            $input .= ' checked="checked"';
        
        $input .= ' />';
        
        return $input;
    }
    
    /**
     * Get HTML representation
     * 
     * @return string
     */
    public function getHTML() {
        $html = $this->getHTMLContainer();
        $input = $this->getHTMLInput();
        
        return $html['header'].$input.' '.$html['label'].$html['footer'];
    }
}
