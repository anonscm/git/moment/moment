<?php

/**
 *     Moment - FormFieldEnum.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Enum form field generator / validator
 */
abstract class FormFieldEnum extends FormFieldSingle {
    /**
     * @var array values
     */
    protected $values = array();
    
    /**
     * Constructor
     *
     * @param string $name
     * @param array $values
     * @param array $properties
     * @param mixed $value
     */
    public function __construct($name, array $values = array(), array $properties = array(), $value = null) {
        parent::__construct($name, $properties, $value);
        
        $this->__set('values', $values);
    }
    
    /**
     * Add value
     *
     * @param string $value
     * @param string $label
     */
    public function addValue($value, $label = null) {
        $this->values[$value] = $label;
    }
    
    /**
     * Add values
     *
     * @param array $values
     */
    public function addValues($values) {
        foreach($values as $value => $label) {
            $this->addValue($value, $label);
        }
    }
    
    /**
     * Validate data
     *
     * @param mixed $data
     *
     * @throws FormDataValidationException
     */
    public function validateData($data) {
        parent::validateData($data);
        
        // Data shoud be part of values
        if($data && !array_key_exists($data, $this->values))
            throw new FormDataValidationException($this, 'bad_value');
    }
    
    /**
     * Getter
     *
     * @param string $property
     *
     * @throws PropertyAccessException
     *
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'values'
        ))) return $this->$property;
        
        return parent::__get($property);
    }
    
    /**
     * Setter
     *
     * @param string $property
     * @param mixed $value
     *
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if($property == 'values') {
            $this->values = array();
            $this->addValues($value);
            
        } else {
            parent::__set($property, $value);
        }
    }
}
