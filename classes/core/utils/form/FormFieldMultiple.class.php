<?php

/**
 *     Moment - FormFieldMultiple.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Multiple form field generator / validator
 * 
 * Accepts properties :
 *   - min int
 *   - max int
 *   - start int
 */
class FormFieldMultiple extends FormField {
    /**
     * @var int minimum entry count
     */
    protected $min = null;
    
    /**
     * @var int maximum entry count
     */
    protected $max = null;
    
    /**
     * @var int start entry count
     */
    protected $start = 0;
    
    /**
     * @var (FormField)[] $sub
     */
    protected $sub = array();
    
    /**
     * @var array values
     */
    protected $values = null;
    
    /**
     * Constructor
     * 
     * @param string $name
     * @param array $properties
     * @param array $sub
     * @param array $values
     */
    public function __construct($name, array $sub = array(), array $properties = array(), array $values = null) {
        parent::__construct($name);
        
        foreach($properties as $k => $v)
            $this->__set($k, $v);
        
        $this->__set('sub', $sub);
        
        $this->values = $values;
    }

    /**
     * Add sub field
     *
     * @param FormField $field
     *
     * @throws FormFieldBadPropertyException
     */
    public function addSubField($field) {
        if(!($field instanceof FormField))
            throw new FormFieldBadPropertyException('field', 'not_child_of_formfield');
        
        $this->sub[] = $field;
    }
    
    /**
     * Add sub fields
     * 
     * @param array $fields
     */
    public function addSubFields($fields) {
        foreach($fields as $field)
            $this->addSubField($field);
    }
    
    /**
     * Get constraints details
     * 
     * @return array
     */
    public function getConstraints() {
        $constraints = array();
        
        if(!is_null($this->min))
            $constraints['min'] = $this->min;
        
        if(!is_null($this->max))
            $constraints['max'] = $this->max;
        
        return $constraints;
    }
    
    /**
     * Get HTML representation
     * 
     * @return string
     */
    public function getHTML() {
        $html = '<fieldset';
        
        $html .= ' data-multiple="'.htmlspecialchars($this->name).'"';
        $html .= ' data-min-count="'.(int)$this->min.'"';
        $html .= ' data-max-count="'.(int)$this->max.'"';
        $html .= ' data-start-count="'.(int)$this->start.'"';
        $html .= ' data-start-values="'.htmlspecialchars(JSON::encode($this->values)).'"';
        
        $html .= '>'."\n";
        
        $label = Lang::tr($this->label ? $this->label : $this->name);
        
        $html .= "\t".'<legend>';
        $html .= '<span data-label-text>'.$label.'</span>';
        $html .= $this->getHTMLConstraints();
        $html .= '</legend>'."\n";
        
        foreach($this->sub as $sub) {
            if(is_object($sub)) {
                $html .= $sub->getHTML()."\n";
            } else {
                $html .= $sub."\n";
            }
        }
        
        $html .= '</fieldset>'."\n";
        
        return $html;
    }
    
     
    /**
     * Allows to get subs
     * 
     * @return array
     */
    public function getSubs(){
        $fields = array();
        foreach ($this->sub as $field) {
            if ($field instanceof FormField) $fields[] = $field;
            
            if ($field instanceof FormFieldContainer) 
                $fields = array_merge($fields, $field->getFields());
        }
        return $fields;
    }


    /**
     * Validate data
     *
     * @param mixed $data
     *
     * @throws FormDataValidationException
     */
    public function validateData($data) {
        // Data shoud be a set of sub value structures
        
        if(is_null($data)) $data = array(); // Not provided
        
        if(!is_array($data))
            throw new FormDataValidationException($this, 'not_entries');
        
        $iks = array_filter(array_keys($data), 'is_int');
        if(count($data) != count($iks))
            throw new FormDataValidationException($this, 'not_entries');
        
        $subs = $this->getSubs();
        $only_one_sub = (count($subs) === 1) ? reset($subs) : false;
        
        $known_subs = array_map(function($sub) {
            return $sub->name;
        }, $subs);
        
        if($this->min && (count($data) < $this->min))
            throw new FormDataValidationException($this, 'not_enough_entries');
        
        if($this->max && (count($data) > $this->max))
            throw new FormDataValidationException($this, 'too_much_entries');
        
        // Check entries
        foreach($data as $entry) {
            // Entry shoud be a data structure
            
            if(is_object($entry)) $entry = (array)$entry;

            if(is_scalar($entry) && $only_one_sub)
                $entry = array($only_one_sub->name => $entry);

            if (!is_array($entry))
                throw new FormDataValidationException($this, 'not_entry');

            if (count(array_filter(array_keys($entry), 'is_int')))
                throw new FormDataValidationException($this, 'not_entry');

            // Any provided sub-field should be known
            foreach (array_keys($entry) as $name)
                if (!in_array($name, $known_subs))
                    throw new FormDataValidationException($this, 'unknown_sub ' . $name);

            // Propagate checks
            foreach ($subs as $sub)
                $sub->validateData(array_key_exists($sub->name, $entry) ? $entry[$sub->name] : null);
        }
    }
    
    /**
     * Getter
     * 
     * @param string $property
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'min', 'max', 'start', 'sub', 'values'
        ))) return $this->$property;
        
        if($property == 'path') return ($this->parent ? $this->parent->path.'.' : '').$this->name.'[]';
        
        return parent::__get($property);
    }

    /**
     * Setter
     *
     * @param string $property
     * @param mixed $value
     *
     * @throws FormFieldBadPropertyException
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if($property == 'min') {
            if(!is_int($value))
                throw new FormFieldBadPropertyException('min', 'not_integer');
            
            if($value < 0)
                throw new FormFieldBadPropertyException('min', 'less_than_0');
            
            $this->min = $value;
            
        } else if($property == 'max') {
            if(!is_int($value))
                throw new FormFieldBadPropertyException('max', 'not_integer');
            
            if($value < 0)
                throw new FormFieldBadPropertyException('max', 'less_than_0');
            
            $this->max = $value;
            
        } else if($property == 'start') {
            if(!is_int($value))
                throw new FormFieldBadPropertyException('start', 'not_integer');
            
            if($value < 0)
                throw new FormFieldBadPropertyException('start', 'less_than_1');
            
            $this->start = $value;
            
        } else if($property == 'sub') {
            $this->sub = array();
            $this->addSubFields($value);
            
        } else if($property == 'values') {
            $this->values = $value;
            
        } else {
            parent::__set($property, $value);
        }
    }
}
