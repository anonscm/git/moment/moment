<?php

/**
 *     Moment - FormFieldRadio.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Radio form field generator / validator
 */
class FormFieldRadio extends FormFieldEnum {
    /**
     * @var string unique id
     */
    protected $uid = null;
    
    /**
     * Get HTML container parts
     * 
     * @return array of HTML prefix, label part and suffix
     */
    public function getHTMLContainer() {
        if(is_null($this->uid)) $this->uid = uniqid('radio_');
        
        $html = array('header' => '', 'label' => '', 'footer' => '');
        
        $html['header'] = '<fieldset data-radio="'.htmlspecialchars($this->name).'" ';
        
        if($this->required)
            $html['header'] .= ' data-mandatory';
        
        $html['header'] .= '>'."\n";
        
        if ($this->label !== false){
            $label = Lang::tr($this->label ? $this->label : $this->name);
            
            $html['label'] = "\t".'<legend>';
            $html['label'] .= '<span data-label-text>'.$label.'</span>';
            $html['label'] .= $this->getHTMLConstraints();
            $html['label'] .= '</legend>'."\n";
        }
        
        $html['footer'] = '';
        if($this->hint) {
            $hint = is_string($this->hint) ? Lang::tr($this->hint) : $this->hint;
            $html['footer'] .= '<p data-hint class="help-text">'.(string)$hint.'</p>';
        }
        $html['footer'] .= '</fieldset>'."\n";
        
        return $html;
    }
    
    /**
     * Get HTML input node
     * 
     * @return string
     */
    public function getHTMLInput() {
        if(is_null($this->uid)) $this->uid = uniqid('radio_');
        
        $input = '';
        foreach($this->values as $value => $label) {
            $input .= "\t".'<label>';
            $input .= '<input type="radio" name="'.$this->uid.'" value="'.htmlspecialchars($value).'"';
            
            if($this->required)
                $input .= ' required';
            
            if($this->value && ($value == $this->value))
                $input .= ' checked="checked"';
            
            $input .= ' />';
            
            $input .= ' '.(is_null($label) ? $value : Lang::tr($label)).'</label>'."\n";
        }
        
        return $input;
    }
}
