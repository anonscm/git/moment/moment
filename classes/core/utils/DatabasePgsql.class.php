<?php

/**
 *     Moment - DatabasePgsql.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Database managing
 */
class DatabasePgsql {
    /**
     * Check if a table exists
     *
     * @param string $table
     *
     * @return bool
     */
    public static function tableExists($table) {
        $s = DBI::admin()->prepare('SELECT * FROM pg_tables WHERE tablename=:table');
        $s->execute(array(':table' => strtolower($table)));
        return (bool)$s->fetch();
    }
    
    /**
     * Create a table
     * 
     * @param string $table the table name
     * @param array $definition dataMap entry
     * 
     */
    public static function createTable($table, $definition) {
        $columns = array();
        
        foreach($definition as $column => $def) {
            $columns[] = $column.' '.self::columnDefinition($def);
        }
        $query = 'CREATE TABLE '.$table.' ('.implode(', ', $columns).')';
        DBI::admin()->exec($query);
        
        foreach($definition as $column => $def) {
            if(array_key_exists('autoinc', $def) && $def['autoinc']) {
                self::createSequence($table, $column);
            }
        }
    }
    
    /**
     * Remove a table
     * 
     * @param string $table the table name
     * 
     */
    public static function removeTable($table) {
        DBI::admin()->exec('DROP TABLE '.$table);
    }
    
    /**
     * Checks table columns format
     * 
     * @param string $table table name
     * @param array $datamap
     */
    public static function checkTable($table, $datamap) {
        // Gather current columns and primary keys
        $current_columns = array_map(function($c) {
            return $c['column_name'];
        }, DBI::admin()->query('SELECT column_name FROM information_schema.columns WHERE table_name=\''.strtolower($table).'\'')->fetchAll());
        
        // Check required columns
        foreach($datamap as $column => $dfn) {
            if(in_array($column, $current_columns)) { // Column exists, check it
                self::checkColumn($table, $column, $dfn);
                
            } else {
                DBI::admin()->exec('ALTER TABLE '.$table.' ADD '.$column.' '.self::columnDefinition($dfn));
                
                if(array_key_exists('autoinc', $dfn) && $dfn['autoinc'])
                    self::createSequence($table, $column);
            }
        }
        
        // Drop unneeded columns
        foreach($current_columns as $column)
            if(!array_key_exists($column, $datamap))
                DBI::admin()->exec('ALTER TABLE '.$table.' DROP COLUMN '.$column);
    }
    
    /**
     * Create sequence for table column if it does not exist already
     * 
     * @param string $table table name
     * @param string $column column name
     * 
     * @return mixed created sequence name or false if already exists
     */
    private static function createSequence($table, $column) {
        $sequence = self::sequenceExists($table, $column, false);
        if(!$sequence) {
            $sequence = strtolower($table.'_'.$column.'_seq');
            DBI::admin()->exec('CREATE SEQUENCE '.$sequence);
        }
        
        DBI::admin()->exec('ALTER TABLE '.$table.' ALTER COLUMN '.$column.' SET DEFAULT nextval(\''.$sequence.'\')');
        DBI::admin()->exec('ALTER SEQUENCE '.$sequence.' OWNED BY '.$table.'.'.$column);
        
        return $sequence;
    }

    /**
     * Check if sequence exists
     *
     * @param string $table table name
     * @param string $column column name
     * @param bool $test_ownership
     *
     * @return mixed sequence name or false
     */
    private static function sequenceExists($table, $column, $test_ownership = true) {
        if($test_ownership) {
            $s = DBI::admin()->prepare('SELECT c.relname AS seq FROM pg_class c JOIN pg_depend d ON d.objid=c.oid AND d.classid=\'pg_class\'::regclass AND d.refclassid=\'pg_class\'::regclass JOIN pg_class t ON t.oid=d.refobjid JOIN pg_attribute a ON a.attrelid=t.oid AND a.attnum=d.refobjsubid WHERE c.relkind=\'S\' and d.deptype=\'a\' AND t.relname=:table AND a.attname=:column');
            $s->execute(array(':table' => strtolower($table), ':column' => strtolower($column)));
        } else {
            $s = DBI::admin()->prepare('SELECT c.relname AS seq FROM pg_class c WHERE c.relkind = \'S\' AND c.relname = :sequence');
            $s->execute(array(':sequence' => strtolower($table.'_'.$column.'_seq')));
        }
        
        $r = $s->fetch();
        
        return $r ? $r['seq'] : false;
    }
    
    /**
     * Check table column, update if needed
     * 
     * @param string $table table name
     * @param string $column column name
     * @param string $definition column definition
     */
    public static function checkColumn($table, $column, $definition) {
        // Get current definition
        $s = DBI::admin()->prepare('SELECT * FROM information_schema.columns WHERE table_name=:table AND column_name=:column');
        $s->execute(array(':table' => strtolower($table), ':column' => strtolower($column)));
        $column_dfn = $s->fetch();
        
        // Build type matcher
        $typematcher = '';
        $length = null;
        $type = '';
        switch($definition['type']) {
            case 'int':
            case 'uint':
                $size = array_key_exists('size', $definition) ? $definition['size'] : 'medium';
                if(!$size) $size = 'medium';
                $s2s = array('small' => 'smallint', 'medium' => 'integer', 'big' => 'bigint');
                $type = $s2s[$size];
                break;
            
            case 'string':
                $length = (int)$definition['size'];
                $typematcher = 'character varying';
                $type = 'character varying('.$definition['size'].')';
                break;
            
            case 'bool':
            case 'boolean':
                $type = 'boolean';
                break;
            
            case 'text':
                $type = 'text';
                break;
            
            case 'date':
                $type = 'date';
                break;
            
            case 'datetime':
                $type = 'timestamp';
                break;
            
            case 'time':
                $type = 'time';
                break;
        }
        
        // Check type
        if(!$typematcher) $typematcher = $type;
        if(!preg_match('`'.$typematcher.'`i', $column_dfn['data_type'])) {
            Logger::info($column.' type does not match '.$typematcher);
            DBI::admin()->exec('ALTER TABLE '.$table.' ALTER COLUMN '.$column.' TYPE '.$type);
        }
        
        // Check length if any
        if(!is_null($length) && ((int)$column_dfn['character_maximum_length'] != $length)) {
            Logger::info($column.' max length does not match '.$length);
            DBI::admin()->exec('ALTER TABLE '.$table.' ALTER COLUMN '.$column.' TYPE '.$type);
        }
        
        // Check default
        if(array_key_exists('default', $definition) && ($definition['type'] != 'text')) {
            if(is_null($definition['default'])) {
                if(!is_null($column_dfn['column_default'])) {
                    Logger::info($column.' default is not null');
                    DBI::admin()->exec('ALTER TABLE '.$table.' ALTER COLUMN '.$column.' SET DEFAULT NULL');
                }
            }else if(is_bool($definition['default'])) {
                if((bool)$column_dfn['column_default'] != $definition['default']) {
                    Logger::info($column.' default is not '.($definition['default'] ? '1' : '0'));
                    DBI::admin()->exec('ALTER TABLE '.$table.' ALTER COLUMN '.$column.' SET DEFAULT '.($definition['default'] ? '1' : '0'));
                }
            }else if($column_dfn['column_default'] != $definition['default']) {
                Logger::info($column.' default is not "'.$definition['default'].'"');
                DBI::admin()->prepare('ALTER TABLE '.$table.' ALTER COLUMN '.$column.' SET DEFAULT :default')->execute(array(':default' => $definition['default']));
            }
        }
        
        // Options defaults
        foreach(array('null', 'primary', 'unique', 'autoinc') as $k) if(!array_key_exists($k, $definition)) $definition[$k] = false;
        
        // Check nullable
        $is_null = (strtolower($column_dfn['is_nullable']) == 'yes');
        if($definition['null'] && !$is_null) {
            Logger::info($column.' is not nullable');
            DBI::admin()->exec('ALTER TABLE '.$table.' ALTER COLUMN '.$column.' DROP NOT NULL');
        } else if(!$definition['null'] && $is_null) {
            Logger::info($column.' should not be nullable');
            DBI::admin()->exec('ALTER TABLE '.$table.' ALTER COLUMN '.$column.' SET NOT NULL');
        }
        
        // Check primary
        $is_primary = false;
        $s = DBI::admin()->prepare('SELECT pg_attribute.attname FROM pg_attribute JOIN pg_class ON pg_class.oid = pg_attribute.attrelid LEFT JOIN pg_constraint ON pg_constraint.conrelid = pg_class.oid AND pg_attribute.attnum = ANY (pg_constraint.conkey) WHERE pg_class.relkind = \'r\' AND pg_class.relname = :table AND pg_attribute.attname = :column AND pg_constraint.contype = \'p\'');
        $s->execute(array(':table' => strtolower($table), ':column' => strtolower($column)));
        if($s->fetch()) $is_primary = true;
        if($definition['primary'] && !$is_primary) {
            Logger::info($column.' is not primary');
            DBI::admin()->exec('ALTER TABLE '.$table.' ADD CONSTRAINT primary_'.$column.' PRIMARY KEY ('.$column.')');
        } else if(!$definition['primary'] && $is_primary) {
            Logger::info($column.' should not be primary');
            DBI::admin()->exec('ALTER TABLE '.$table.' DROP CONSTRAINT primary_'.$column);
        }
        
        // Check unique
        $is_unique = false;
        $s = DBI::admin()->prepare('SELECT pg_attribute.attname FROM pg_attribute JOIN pg_class ON pg_class.oid = pg_attribute.attrelid LEFT JOIN pg_constraint ON pg_constraint.conrelid = pg_class.oid AND pg_attribute.attnum = ANY (pg_constraint.conkey) WHERE pg_class.relkind = \'r\' AND pg_class.relname = :table AND pg_attribute.attname = :column AND pg_constraint.contype = \'u\'');
        $s->execute(array(':table' => strtolower($table), ':column' => strtolower($column)));
        if($s->fetch()) $is_unique = true;
        if($definition['unique'] && !$is_unique) {
            Logger::info($column.' is not unique');
            DBI::admin()->exec('ALTER TABLE '.$table.' ADD CONSTRAINT unique_'.$column.' UNIQUE ('.$column.')');
        } else if(!$definition['unique'] && $is_unique) {
            Logger::info($column.' should not be unique');
            DBI::admin()->exec('ALTER TABLE '.$table.' DROP CONSTRAINT unique_'.$column);
        }
        
        // Check autoinc
        $is_autoinc = self::sequenceExists($table, $column);
        if($definition['autoinc'] && (!$is_autoinc || ($column_dfn['column_default'] != 'nextval(\''.$is_autoinc.'\'::regclass)'))) {
            Logger::info($column.' is not autoinc');
            self::createSequence($table, $column);
        } else if(!$definition['autoinc'] && $is_autoinc) {
            Logger::info($column.' should not be autoinc');
            DBI::admin()->exec('ALTER TABLE '.$table.' ALTER COLUMN '.$column.' DROP DEFAULT'); // Should we drop the sequence as well ?
        }
    }
    
    /**
     * Get column definition
     * 
     * @param array $definition dataMap entry
     * 
     * @return string sql definition
     */
    private static function columnDefinition($definition) {
        $sql = '';
        
        // Build type part
        switch($definition['type']) {
            case 'int':
            case 'uint':
                $size = array_key_exists('size', $definition) ? $definition['size'] : 'medium';
                if(!$size) $size = 'medium';
                $s2s = array('small' => 'smallint', 'medium' => 'integer', 'big' => 'bigint');
                $sql .= $s2s[$size];
                break;
            
            case 'string':
                $sql .= 'character varying('.$definition['size'].')';
                break;
            
            case 'bool':
            case 'boolean':
                $sql .= 'boolean';
                break;
            
            case 'text':
                $sql .= 'text';
                break;
            
            case 'date':
                $sql .= 'date';
                break;
            
            case 'datetime':
                $sql .= 'timestamp';
                break;
            
            case 'time':
                $sql .= 'time';
                break;
        }
        
        // Add nullable
        if(!array_key_exists('null', $definition) || !$definition['null']) $sql .= ' NOT NULL';
        
        // Add default
        if(array_key_exists('default', $definition) && ($definition['type'] != 'text')) {
            $sql .= ' DEFAULT ';
            $default = $definition['default'];
            
            if(is_null($default)) {
                $sql .= 'NULL';
            }else if(is_bool($default)) {
                $sql .= $default ? '1' : '0';
            }else if(is_numeric($default) && in_array($definition['type'], array('int', 'uint'))) {
                $sql .= $default;
            }else $sql .= '"'.str_replace('"', '\\"', $default).'"';
        }
        
        // Add options
        if(array_key_exists('unique', $definition) && $definition['unique']) $sql .= ' UNIQUE';
        if(array_key_exists('primary', $definition) && $definition['primary']) $sql .= ' PRIMARY KEY';
        
        // Return statment
        return $sql;
    }
}
