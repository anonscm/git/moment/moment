<?php

/**
 *     Moment - FileIOJSON.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * JSON file reader / writer
 */
class FileIOJSON {
    
    /**
     * Read JSON from file
     *
     * @param string $path
     *
     * @return mixed
     *
     * @throws FileIOCannotReadFileException
     * @throws FileIOJSONCannotDecodeException
     * @throws FileIONotFoundException
     */
    public static function read($path) {
        return JSON::decode(FileIO::readWhole($path));
    }

    /**
     * Write JSON to file
     *
     * @param string $path
     * @param $content
     *
     * @throws FileIOCannotWriteFileException
     * @throws FileIOJSONCannotEncodeException
     */
    public static function write($path, $content) {
        FileIO::writeWhole($path, JSON::encode($content));
    }
}
