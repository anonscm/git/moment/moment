<?php

/**
 *     Moment - MailBodyAlternative.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
use MongoDB\BSON\Type;

if (!defined('EKKO_ROOT'))
    die('Missing environment');


/**
 * Handle mail attachment
 */
class MailBodyAlternative {
    /**
     * Attachment contents
     */
    private $content = null;
    
    /**
     * Attachment mime type
     */
    private $mime_type = null;
    
    /**
     * Attachment transfer encoding
     */
    private $transfer_encoding = 'quoted-printable';
    
    /**
     * Method - Used for ICS
     */
    private $method = null;
    
    /**
     * Charset
     */
    private $charset = 'UTF-8';
    
    /**
     * New line style
     */
    private $nl = "\r\n";
    
    /**
     * New line style
     */
    private $priority;
    
    /**
     * Priority constants
     */
    const PRIORITY_LOW = 0;
    const PRIORITY_PLAIN = 10;
    const PRIORITY_HTML = 20;
    const PRIORITY_HIGH = 30;
    
    /**
     * Create new empty attachment
     *
     * @param $mime_type
     * @param int $priority
     */
    public function __construct($mime_type, $priority = 0) {
        $nl = Config::get('email.newline');
        if($nl) $this->nl = $nl;
        
        $this->mime_type = $mime_type;
        
        $this->priority = (int) $priority;
    }
    

    /**
     * TODO
     * 
     * @return type
     */
    public function getHeaders(){
        $headers = array();
        
        $headers['Content-Type'] = $this->mime_type
//            .($this->charset ? '; charset="'.$this->charset.'"': '')
            .($this->charset ? '; charset='.$this->charset.'': '')
            .($this->method ? '; method='.$this->method : '')
            .($this->mime_type === 'text/plain' ? '; format=flowed; delsp=yes' : '');
        
        
        // Set Content-Transfer-Encoding part header
        if($this->transfer_encoding)
            $headers['Content-Transfer-Encoding'] = $this->transfer_encoding;
        
        
        return $headers;
    }
    
    
    /**
     * TODO
     * 
     * @return type
     */
    public function getEncodedContent(){
        $content = $this->content;
        
        if ($this->mime_type === 'text/html'){
            $content = preg_replace('`^.*<body[^>]*>(.+)</body>.*$`ims', '$1', $content);
            
            $styles = array('view/css/mail/mail.css', 'view/core/mail/css/mail.css', 'view/skin/mail.css');
            $css = '';
            foreach($styles as $file)
                if(file_exists(EKKO_ROOT.'/'.$file))
                    $css .= "\n\n".file_get_contents(EKKO_ROOT.'/'.$file);
            $css = trim($css);

            // Build HTML
            $content = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">' . "\n"
                . '<html>' . "\n"
                . "\t" . '<head>' . "\n"
                . "\t\t" . '<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />' . "\n"
                .($css ? "\t\t" . '<style type="text/css">'.$css.'</style>' : '')
                . "\t" . '</head>' . "\n"
                . "\t" . '<body>'
                . $content . "\n"
                . "\t" . '</body>'
                . "\n" . '</html>';
        }
        
        // Encode file data if needed
        switch($this->transfer_encoding) {
            case 'base64' : $content = chunk_split(base64_encode($content)); break;
            case 'quoted-printable' : $content = quoted_printable_encode($content); break;
        }
        
        return $content.$this->nl;
    }
    
    
    /**
     * TODO
     * 
     * @return type
     */
    public function getSource(){
        $results = array();
        foreach ($this->getHeaders() as $header => $value){
            $results[] = $header.': '.$value;
        }
        
        return implode($this->nl, $results).$this->nl.$this->nl.$this->getEncodedContent();
    }

    
    /**
     * Getter
     * 
     * @param string $property property to get
     * 
     * @return property value
     */
    public function __get($property) {
        if(in_array($property, array(
           'content', 'mime_type', 'transfer_encoding', 'method', 'charset', 'priority'
        ))) return $this->$property;
        
        return null;
    }
    
    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     * @throws MailAttachmentBadTransferEncodingException
     */
    public function __set($property, $value) {
        if($property == 'content') {
            $this->content = $value;
    
        }else if($property == 'priority') {
            $this->priority = (int) $value;
            
        }else if($property == 'mime_type') {
            $this->mime_type = $value;
            
        }else if($property == 'transfer_encoding') {
            if($value && !in_array($value, array('8bit', 'base64', 'quoted-printable')))
                throw new MailAttachmentBadTransferEncodingException($value);
            
            $this->transfer_encoding = $value;
            
        }else if($property == 'method'){
            $this->method = $value;
            
        }else if($property == 'charset'){
            $this->charset = $value;
        }
    }
}
