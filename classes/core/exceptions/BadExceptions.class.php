<?php

/**
 *     Moment - BadExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');


/**
 * Bad email exception
 */
class BadEmailException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $email
     */
    public function __construct($email) {
        parent::__construct(
            'bad_email', // Message to give to the user
            array('email' => $email) // Real message to log
        );
    }
}

/**
 * Bad date exception
 */
class BadDateException extends DetailedException {
    /**
     * Constructor
     * 
     * @param mixed $date
     */
    public function __construct($date) {
        parent::__construct(
            'bad_date', // Message to give to the user
            array('date' => $date) // Real message to log
        );
    }
}

/**
 * Bad url exception
 */
class BadURLException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $url
     */
    public function __construct($url) {
        parent::__construct(
            'bad_url', // Message to give to the user
            array('url' => $url) // Real message to log
        );
    }
}

/**
 * Bad lang exception
 */
class BadLangCodeException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $code the bad lang code
     */
    public function __construct($code) {
        parent::__construct(
            'bad_lang_code', // Message to give to the user
            array('code' => $code) // Details to log
        );
    }
}

/**
 * Bad format exception
 */
class BadFormatException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $value the value
     * @param string $format the format
     */
    public function __construct($value, $format) {
        parent::__construct(
            'bad_format', // Message to give to the user
            array('value' => $value, 'format'=> $format) // Details to log
        );
    }
}

