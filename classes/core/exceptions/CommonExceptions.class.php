<?php

/**
 *     Moment - CommonExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Unknown property access exception
 * 
 * To be used in derived classes
 */
class PropertyAccessException extends DetailedException {
    /**
     * Constructor
     * 
     * @param mixed $object object or class name to report access about
     * @param string $property name of the property that was wanted
     */
    public function __construct($object, $property) {
        parent::__construct(
            'no_such_property', // Message to give to the user
            array('object' => (string)$object, 'property' => $property) // Real message to log
        );
    }
}

/**
 * Not found exception
 */
class NotFoundException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $object
     * @param string $selector
     */
    public function __construct($object, $selector) {
        parent::__construct(
            'not_found', // Message to give to the user
            array('object' => $object, 'selector' => $selector), // Real message to log
            null,
            404
        );
    }
}

/**
 * Missing argument exception
 */
class MissingArgumentException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $key
     */
    public function __construct($key) {
        parent::__construct(
            'missing_argument', // Message to give to the user
            array('key' => $key) // Real message to log
        );
    }
}
