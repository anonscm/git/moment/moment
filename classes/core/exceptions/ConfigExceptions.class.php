<?php

/**
 *     Moment - ConfigExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Missing config file exception
 */
class ConfigFileMissingException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $file path of the required file
     */
    public function __construct($file) {
        parent::__construct(
            'config_file_missing', // Message to give to the user
            'File '.$file.' not found' // Details to log
        );
    }
}

/**
 * Bad config parameter exception
 */
class ConfigBadParameterException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $key name of the bad parameter
     */
    public function __construct($key) {
        parent::__construct(
            'config_bad_parameter', // Message to give to the user
            'parameter : '.$key // Details to log
        );
    }
}

/**
 * Missing config parameter exception
 */
class ConfigMissingParameterException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $key name of the missing parameter
     */
    public function __construct($key) {
        parent::__construct(
            'config_missing_parameter', // Message to give to the user
            $key // Details to log
        );
    }
}

/**
 * Override disabled exception
 */
class ConfigOverrideDisabledException extends DetailedException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct(
            'config_override_disabled'
        );
    }
}

/**
 * Validation of parameter override failed exception
 */
class ConfigOverrideValidationFailedException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $key name of the parameter
     * @param string $validator name of the validator
     */
    public function __construct($key, $validator) {
        parent::__construct(
            'config_override_validation_failed', // Message to give to the user
            null,
            array('parameter' => $key, 'validator' => $validator) // Public info
        );
    }
}

/**
 * Override of parameter not allowed exception
 */
class ConfigOverrideNotAllowedException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $key name of the parameter
     */
    public function __construct($key) {
        parent::__construct(
            'config_override_not_allowed', // Message to give to the user
            'parameter : '.$key // Details to log
        );
    }
}

/**
 * Configuration validation failure, MUSTN'T extend LoggingException as logger setup may have failed
 */
class ConfigValidationException extends Exception {
    /**
     * Constructor
     * 
     * @param string $reason
     */
    public function __construct($reason) {
        $msg = 'Configuration validation failed : '.$reason;
        error_log($msg);
        parent::__construct($msg);
    }
}
