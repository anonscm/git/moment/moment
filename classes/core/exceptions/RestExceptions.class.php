<?php

/**
 *     Moment - RestExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Base REST exception
 */
abstract class RestException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $message message id to return to the interface
     * @param int $code http error code
     * @param mixed $details details about what happened (for logging)
     */
    public function __construct($message, $code = 0, $details = '') {
        parent::__construct(
            $message,
            $details,
            RestRequest::getContext()
        );
        $this->code = $code;
    }
}

/**
 * REST authentication required
 */
class RestUndergoingMaintenanceException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_undergoing_maintenance', 403);
    }
}

/**
 * REST authentication required
 */
class RestAuthenticationRequiredException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_authentication_required', 403);
    }
}

/**
 * REST admin required
 */
class RestAdminRequiredException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_admin_required', 403);
    }
}

/**
 * REST ownership required
 */
class RestOwnershipRequiredException extends RestException {
    /**
     * Constructor
     * 
     * @param string $uid user trying to get access
     * @param mixed $resource the wanted resource selector
     */
    public function __construct($uid, $resource) {
        parent::__construct('rest_ownership_required', 403, array('uid' => $uid, 'ressource' => $resource));
    }
}

/**
 * REST missing parameter
 */
class RestMissingParameterException extends RestException {
    /**
     * Constructor
     * 
     * @param string $name name of the missing parameter
     */
    public function __construct($name) {
        parent::__construct('rest_missing_parameter', 400, array('parameter' => $name));
    }
}

/**
 * REST bad parameter
 */
class RestBadParameterException extends RestException {
    /**
     * Constructor
     * 
     * @param string $name name of the bad parameter
     */
    public function __construct($name) {
        parent::__construct('rest_bad_parameter', 400, array('parameter' => $name));
    }
}

/**
 * REST sanity check failed
 */
class RestSanityCheckFailedException extends RestException {
    /**
     * Constructor
     * 
     * @param string $check name of the check
     * @param mixed $value value of the bad data
     * @param mixed $expected expected value
     */
    public function __construct($check, $value, $expected) {
        parent::__construct('rest_sanity_check_failed', 400, 'check "'.$check.'", "'.$expected.'" value was expected but got "'.$value.'" instead');
    }
}

/**
 * Rest method not allowed
 */
class RestMethodNotAllowedException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_method_not_allowed', 405);
    }
}

/**
 * Rest not allowed
 */
class RestNotAllowedException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_not_allowed', 403);
    }
}


/**
 * Rest endpoint not found
 */
class RestEndpointNotFoundException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_endpoint_not_found', 404);
    }
}


/**
 * REST XSRF token not valid required
 */
class RestXSRFTokenInvalidException extends RestException {
    /**
     * Constructor
     * 
     * @param string $token
     */
    public function __construct($token) {
        parent::__construct('rest_xsrf_token_invalid', 403, array('token' => $token));
    }
}


/**
 * REST JSONP only for GET requests
 */
class RestJSONPBadMethodException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_jsonp_bad_method', 405);
    }
}


/**
 * REST JSONP not allowed
 */
class RestJSONPNotAllowedException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_jsonp_not_allowed', 405);
    }
}


/**
 * REST method is not implemented
 */
class RestMethodNotImplementedException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_method_not_implemented', 501);
    }
}


/**
 * REST updatedSince parameter has bad format
 */
class RestUpdatedSinceBadFormatException extends RestException {
    /**
     * Constructor
     * 
     * @param string $updated_since
     */
    public function __construct($updated_since) {
        parent::__construct('rest_updatedsince_bad_format', 400, array('updatedSince' => $updated_since));
    }
}


/**
 * REST option not found
 */
class RestOptionNotFoundException extends RestException {
    /**
     * Constructor
     * 
     * @param string $option
     */
    public function __construct($option) {
        parent::__construct('rest_option_not_found', 400, array('option' => $option));
    }
}


/**
 * REST option mal formed
 */
class RestOptionMalFormedException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_option_mal_formed', 400);
    }
}


/**
 * REST option not allowed
 */
class RestOptionNotAllowedException extends RestException {
    /**
     * Constructor
     * 
     * @param string $option Option not allowed
     */
    public function __construct($option) {
        parent::__construct('rest_option_not_allowed', 400, 'option: '.$option);
    }
}


/**
 * REST option not allowed
 */
class RestOptionsNotAllowedException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_options_not_allowed', 400);
    }
}


/**
 * REST option not supported
 */
class RestOptionNotSupportedException extends RestException {
    /**
     * Constructor
     * @param string $option Option not allowed
     */
    public function __construct($option) {
        parent::__construct('rest_option_not_supported', 400, 'option: '.$option);
    }
}


/**
 * REST option not supported
 */
class RestOptionsNotSupportedException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_options_not_supported', 400);
    }
}


/**
 * REST output conversion failed
 */
class RestOutputFormatNotSupportedException extends RestException {
    /**
     * Constructor
     * 
     * @param string $type
     */
    public function __construct($type) {
        parent::__construct('rest_output_format_not_supported', 400, array('type' => $type));
    }
}


/**
 * REST output conversion failed
 */
class RestOutputConversionException extends RestException {
    /**
     * Constructor
     * 
     * @param string $type
     */
    public function __construct($type) {
        parent::__construct('rest_output_conversion_failed', 500, array('type' => $type));
    }
}

