<?php

/**
 *     Moment - Exceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');


/**
 * Logging exception
 */
class LoggingException extends Exception {
    /**
     * Holds exception unique id
     */
    private $uid = null;
    
    /**
     * Constructor
     * 
     * Logs all info to server log
     * 
     * @param string $msg_code message code to be used to present error
     * @param mixed $log lines to log by categories
     * @param int $http_code HTTP code used by rest server
     */
     public function __construct($msg_code, $log = null, $http_code = 500) {
        $this->uid = ApplicationContext::getUID();
        
        // normalize arguments
        if(!$log) $log = $msg_code;
        if ($log && (!is_array($log) || !preg_match('`[a-z]`', key($log))))
            $log = array('exception' => $log);
        
        // Log info
        if ($log) 
            foreach ($log as $category => $lines) {
                if (!is_array($lines)) 
                    $lines = array($lines);
                
                foreach ($lines as $line) 
                    Logger::error('['.get_class($this).':'.$this->uid.':'.$category.'] '.$line);
            }
        
        parent::__construct($msg_code, $http_code);
    }
    
    /**
     * Uid getter
     * 
     * @return string the exception uid
     */
    public function getUid() {
        return ApplicationContext::getThreadUID().'_'.$this->uid;
    }
}

/**
 * Detailed exception
 */
class DetailedException extends LoggingException {
    /**
     * Public exception details
     */
    private $details = null;
    
    /**
     * Constructor
     * 
     * Logs all info to server log
     * 
     * @param string $msg_code message code to be used to present error
     * @param mixed $internal_details details to log
     * @param mixed $public_details details to give to the user (logged as well)
     * @param int $http_code HTTP code used by rest server
     */
    public function __construct($msg_code, $internal_details = null, $public_details = null, $http_code = 500) {
        // Build data
        $this->details = $public_details;
        
        if(!$internal_details) $internal_details = array();
        if(!is_array($internal_details)) $internal_details = array($internal_details);
        
        if($public_details) {
            if(!is_array($public_details)) $public_details = array($public_details);
            $internal_details = array_merge($public_details, $internal_details);
        }
        
        $log = array(
            'exception' => $msg_code,
            'details' => array(),
            'trace' => explode("\n", $this->getTraceAsString())
        );
        
        // Cast to string(s)
        foreach ($internal_details as $key => $detail) {
            $key = is_int($key) ? '' : $key.' = ';
            
            if (is_scalar($detail)) {
                $log['details'][] = $key.$detail;
                
            } else {
                if(is_object($detail) && ($detail instanceof Exception)) {
                    $subtrace = explode("\n", $detail->getTraceAsString());
                    $detail = array(
                        'message' => $detail->getMessage(),
                        'trace' => array_slice($subtrace, 0, count($subtrace) - count($log['trace']))
                    );
                }
                
                foreach (explode("\n", print_r($detail, true)) as $line)
                    $log['details'][] = $key.$line;
            }
        }
        parent::__construct($msg_code, $log, $http_code);
    }
    
    /**
     * Info getter
     * 
     * @return mixed the exception info
     */
    public function getDetails() {
        return $this->details;
    }
}
