<?php

/**
 *     Moment - FormExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Malformed description
 */
class FormBadPropertyException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $property
     * @param string $problem
     */
    public function __construct($property, $problem) {
        parent::__construct('form_bad_property', array('property' => $property, 'problem' => $problem));
    }
}

/**
 * Basic form exception
 */
class FormException extends DetailedException {
    /**
     * Constructor
     * 
     * @param Form $form
     * @param string $msg_code message code to be used to present error
     * @param mixed $internal_details details to log
     * @param mixed $public_details details to give to the user (logged as well)
     */
    public function __construct($form, $msg_code, $internal_details = null, $public_details = null) {
        $internal_details = $internal_details ? (array)$internal_details : array();
        $internal_details['form'] = (string)$form;
        
        parent::__construct('form_'.$msg_code, $internal_details, $public_details);
    }
}

/**
 * Field bad name
 */
class FormFieldBadNameException extends DetailedException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('form_field_bad_name');
    }
}

/**
 * Field control method
 */
class FormControlBadMethodException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $method
     */
    public function __construct($method) {
        parent::__construct('form_control_bad_method', array('method' => $method));
    }
}

/**
 * Field control resource
 */
class FormControlBadResourceException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $resource
     */
    public function __construct($resource) {
        parent::__construct('form_control_bad_resource', array('resource' => $resource));
    }
}

/**
 * Field bad property
 */
class FormFieldBadPropertyException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $property
     * @param string $problem
     */
    public function __construct($property, $problem) {
        parent::__construct('form_field_bad_property', array('property' => $property, 'problem' => $problem));
    }
}

/**
 * Data validation
 */
class FormDataValidationException extends DetailedException {
    /**
     * Constructor
     * 
     * @param FormField|Form $what
     * @param string $problem
     */
    public function __construct($what, $problem) {
        parent::__construct('form_invalid_data', null, array('what' => (string)$what, 'problem' => $problem), 400);
    }
}

