<?php

/**
 *     Moment - AuthRemoteExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Request came in too late
 */
class AuthRemoteTooLateException extends DetailedException {
    /**
     * Constructor
     * 
     * @param int $by too late by
     */
    public function __construct($by) {
        parent::__construct(
            'auth_remote_too_late', // Message to give to the user
            'by : '.$by.' seconds' // Details to log
        );
        $this->code = 403;
    }
    
}

/**
 * Signature check failed
 */
class AuthRemoteSignatureCheckFailedException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $signed body that was signed
     * @param string $secret secret used while signing
     * @param string $received_signature received signature
     * @param string $signature expected signature
     */
    public function __construct($signed, $secret, $received_signature, $signature) {
        parent::__construct(
            'auth_remote_signature_check_failed', // Message to give to the user
            'signed = '.$signed.', secret = '.$secret.', received_signature = '.$received_signature.', signature = '.$signature
        );
        $this->code = 403;
    }
}

/**
 * Unknown remote application
 */
class AuthRemoteUknownApplicationException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $name name of the remote application
     */
    public function __construct($name) {
        parent::__construct(
            'auth_remote_unknown_application', // Message to give to the user
            'application : '.$name // Details to log
        );
        $this->code = 403;
    }
}

/**
 * User does not accept remote authentication
 */
class AuthRemoteUserRejectedException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $uid user id
     * @param string $reason
     */
    public function __construct($uid, $reason) {
        parent::__construct(
            'auth_remote_user_rejected', // Message to give to the user
            'uid : '.$uid.', reason : '.$reason // Details to log
        );
        $this->code = 503;
    }
}
