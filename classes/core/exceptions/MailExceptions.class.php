<?php

/**
 *     Moment - MailExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Invalid address format exception
 */
class MailAddressInvalidFormatException extends DetailedException {

    /**
     * Constructor
     * 
     * @param string $selector Column used to select user
     */
    public function __construct($selector) {
        parent::__construct(
            'invalid_address_format', // Message to give to the user
            $selector // Real message to log
        );
    }

}

/**
 * No mail address found exception
 */
class MailNoAddressesFoundException extends DetailedException {

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct(
            'no_addresses_found' // Message to give to the user
        );
    }
}

/**
 * Bad attachment disposition
 */
class MailAttachmentBadDispositionException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $disposition
     */
    public function __construct($disposition) {
        parent::__construct(
            'mail_attachment_bad_disposition', // Message to give to the user
            'disposition = '.$disposition // Real message to log
        );
    }
}

/**
 * Bad attachment transfer encoding
 */
class MailAttachmentBadTransferEncodingException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $transfer_encoding
     */
    public function __construct($transfer_encoding) {
        parent::__construct(
            'mail_attachment_bad_transfer_encoding', // Message to give to the user
            'transfer_encoding = '.$transfer_encoding // Real message to log
        );
    }
}

/**
 * No attachment content
 */
class MailAttachmentNoContentException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $path
     */
    public function __construct($path) {
        parent::__construct(
            'mail_attachment_no_content', // Message to give to the user
            'path = '.$path // Real message to log
        );
    }
}
