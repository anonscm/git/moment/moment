<?php

/**
 *     Moment - AuthSPExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Missing service provider delegation class.
 */
class AuthSPMissingDelegationClassException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $name name of the delegation class
     */
    public function __construct($name) {
        parent::__construct(
            'auth_sp_missing_delegation_class', // Message to give to the user
            'class : '.$name // Details to log
        );
    }
}

/**
 * Authentication no found.
 */
class AuthSPAuthenticationNotFoundException extends DetailedException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct(
            'auth_sp_authentication_not_found'
        );
    }
}

/**
 * Missing attribute.
 */
class AuthSPMissingAttributeException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $name name of the attribute
     */
    public function __construct($name) {
        parent::__construct(
            'auth_sp_missing_attribute', // Message to give to the user
            'Attribute : '.$name // real message to log    
        );
    }
}

/**
 * Bad attribute.
 */
class AuthSPBadAttributeException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $name name of the attribute
     */
    public function __construct($name) {
        parent::__construct(
            'auth_sp_bad_attribute', // Message to give to the user
            'Attribute : '.$name // real message to log    
        );
    }
}
