<?php

/**
 *     Moment - UtilitiesExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Bad uid generator unicity checker exception
 */
class UtilitiesUidGeneratorBadUnicityCheckerException extends DetailedException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct(
            'utilities_uid_generator_bad_unicity_checker' // Message to give to the user
        );
    }
}

/**
 * Uid generator tried too much exception
 */
class UtilitiesUidGeneratorTriedTooMuchException extends DetailedException {
    /**
     * Constructor
     * 
     * @param int $tries
     */
    public function __construct($tries) {
        parent::__construct(
            'utilities_uid_generator_tried_too_much', // Message to give to the user
            'tries = '.$tries // Real message to log
        );
    }
}

/**
 * Bad size format exception
 */
class UtilitiesBadSizeFormatException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $size the raw, badly formated size
     */
    public function __construct($size) {
        parent::__construct(
            'bad_size_format', // Message to give to the user
            'size : '.$size // Details to log
        );
    }
}

/**
 * Bad validator
 */
class UtilitiesBadValidatorException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $validator
     */
    public function __construct($validator) {
        parent::__construct('bad_validator', array('validator' => $validator));
    }
}

/**
 * Cannot change file owner exception
 */
class UtilitiesCannotChangeFileOwnerException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $file
     * @param string $owner
     */
    public function __construct($file, $owner) {
        parent::__construct('cannot_change_file_owner', array('file' => $file, 'owner' => $owner));
    }
}

/**
 * Cannot change file group exception
 */
class UtilitiesCannotChangeFileGroupException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $file
     * @param string $group
     */
    public function __construct($file, $group) {
        parent::__construct('cannot_change_file_group', array('file' => $file, 'group' => $group));
    }
}
