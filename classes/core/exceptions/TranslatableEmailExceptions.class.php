<?php

/**
 *     Moment - TranslatableEmailExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Missing token
 */
class TranslatableEmailMissingTokenException extends DetailedException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('translatable_email_missing_token');
    }
}

/**
 * Bad translatable email token
 */
class TranslatableEmailBadTokenException extends DetailedException {
    /**
     * Constructor
     *
     * @param string $token
     */
    public function __construct($token) {
        parent::__construct(
            'translatable_email_bad_token', // Message to give to the user
            array('token' => $token)
        );
    }
}

/**
 * Unknown translatable email
 */
class TranslatableEmailNotFoundException extends DetailedException {
    /**
     * Constructor
     *
     * @param string $selector column used to select translatable email
     */
    public function __construct($selector) {
        parent::__construct(
            'translatable_email_not_found', // Message to give to the user
            $selector // Real message to log
        );
    }
}
