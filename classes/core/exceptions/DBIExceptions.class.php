<?php

/**
 *     Moment - DBIExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Misc connexion exception
 */
class DBIConnexionException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $message error message
     */
    public function __construct($message) {
        parent::__construct(
            'failed_to_connect_to_database', // Message to give to the user
            $message // Details to log
        );
    }
}

/**
 * Unknown remote connexion exception
 */
class DBIUnknownRemoteException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $name
     */
    public function __construct($name) {
        parent::__construct(
            'unknonw_remote_database', // Message to give to the user
            $name // Details to log
        );
    }
}

/**
 * Missing configuration parameter exception
 */
class DBIConnexionMissingParameterException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $parameter name of the required parameter which is missing
     */
    public function __construct($parameter) {
        parent::__construct(
            'dbi_connection_missing_parameter',
            'Parameter: '.$parameter
        );
    }
}

/**
 * Usage exception
 */
class DBIUsageException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $message error message
     * @param mixed $details
     */
    public function __construct($message, $details = null) {
        parent::__construct(
            'database_access_failure', // Message to give to the user
            array($message, $details) // Details to log
        );
    }
}
