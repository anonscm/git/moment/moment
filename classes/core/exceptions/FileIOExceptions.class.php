<?php

/**
 *     Moment - FileIOExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Generic exception
 */
class FileIOException extends DetailedException {
    /**
     * Constructor
     * 
     * @param string $code
     * @param mixed $data
     */
    public function __construct($code, $data) {
        parent::__construct('fileio_'.$code, is_array($data) ? $data : array('path' => $data));
    }
}

/**
 * Not found exception
 */
class FileIONotFoundException extends FileIOException {
    /**
     * Constructor
     * 
     * @param string $path
     */
    public function __construct($path) {
        parent::__construct('not_found', $path);
    }
}

/**
 * Cannot open file exception
 */
class FileIOCannotOpenFileException extends FileIOException {
    /**
     * Constructor
     * 
     * @param FileIO $file
     * @param string $mode
     */
    public function __construct($file, $mode) {
        parent::__construct('cannot_open_file', array('path' => $file->path, 'mode' => $mode));
    }
}

/**
 * Cannot lock exception
 */
class FileIOCannotLockException extends FileIOException {
    /**
     * Constructor
     * 
     * @param FileIO $file
     */
    public function __construct($file) {
        parent::__construct('cannot_lock', $file->path);
    }
}

/**
 * Cannot unlock exception
 */
class FileIOCannotUnlockException extends FileIOException {
    /**
     * Constructor
     * 
     * @param FileIO $file
     */
    public function __construct($file) {
        parent::__construct('cannot_unlock', $file->path);
    }
}

/**
 * Cannot close file exception
 */
class FileIOCannotCloseFileException extends FileIOException {
    /**
     * Constructor
     * 
     * @param FileIO $file
     */
    public function __construct($file) {
        parent::__construct('cannot_close_file', $file->path);
    }
}

/**
 * File closed exception
 */
class FileIOFileClosedException extends FileIOException {
    /**
     * Constructor
     * 
     * @param FileIO $file
     */
    public function __construct($file) {
        parent::__construct('file_closed', $file->path);
    }
}

/**
 * Cannot read file exception
 */
class FileIOCannotReadFileException extends FileIOException {
    /**
     * Constructor
     * 
     * @param mixed $file
     */
    public function __construct($file) {
        parent::__construct('cannot_read_file', (is_object($file) && ($file instanceof FileIO)) ? $file->path : $file);
    }
}

/**
 * Cannot seek into file exception
 */
class FileIOCannotSeekIntoFileException extends FileIOException {
    /**
     * Constructor
     * 
     * @param FileIO $file
     * @param int $offset
     * @param int $whence
     */
    public function __construct($file, $offset, $whence) {
        parent::__construct('cannot_seek_into_file', array('path' => $file->path, 'offset' => $offset, 'whence' => $whence));
    }
}

/**
 * Cannot write file exception
 */
class FileIOCannotWriteFileException extends FileIOException {
    /**
     * Constructor
     * 
     * @param mixed $file
     */
    public function __construct($file) {
        parent::__construct('cannot_write_file', (is_object($file) && ($file instanceof FileIO)) ? $file->path : $file);
    }
}

/**
 * Link exists with another target exception
 */
class FileIOLinkExistsWithAnotherTargetException extends FileIOException {
    /**
     * Constructor
     * 
     * @param string $target
     * @param string $link
     * @param string $current
     */
    public function __construct($target, $link, $current) {
        parent::__construct('link_exists_with_another_target', array('target' => $target, 'link' => $link, 'current' => $current));
    }
}

/**
 * Link name taken exception
 */
class FileIOLinkNameTakenException extends FileIOException {
    /**
     * Constructor
     * 
     * @param string $link
     */
    public function __construct($link) {
        parent::__construct('link_name_taken', array('link' => $link));
    }
}

/**
 * Cannot create link exception
 */
class FileIOCannotCreateLinkException extends FileIOException {
    /**
     * Constructor
     * 
     * @param string $target
     * @param string $link
     */
    public function __construct($target, $link) {
        parent::__construct('cannot_create_link', array('target' => $target, 'link' => $link));
    }
}

/**
 * Cannot delete file exception
 */
class FileIOCannotDeleteFileException extends FileIOException {
    /**
     * Constructor
     * 
     * @param string $path
     */
    public function __construct($path) {
        parent::__construct('cannot_delete_file', $path);
    }
}

/**
 * Cannot create directory exception
 */
class FileIOCannotCreateDirectoryException extends FileIOException {
    /**
     * Constructor
     * 
     * @param string $path
     */
    public function __construct($path) {
        parent::__construct('cannot_create_directory', $path);
    }
}

/**
 * Directory not empty exception
 */
class FileIODirectoryNotEmptyException extends FileIOException {
    /**
     * Constructor
     * 
     * @param string $path
     */
    public function __construct($path) {
        parent::__construct('directory_not_empty', $path);
    }
}

/**
 * Cannot delete directory exception
 */
class FileIOCannotDeleteDirectoryException extends FileIOException {
    /**
     * Constructor
     * 
     * @param string $path
     */
    public function __construct($path) {
        parent::__construct('cannot_delete_directory', $path);
    }
}

/**
 * Cannot set mode exception
 */
class FileIOCannotSetModeException extends FileIOException {
    /**
     * Constructor
     * 
     * @param string $path
     * @param int $mode
     */
    public function __construct($path, $mode) {
        parent::__construct('cannot_set_mode', array('path' => $path, 'mode' => $mode));
    }
}

/**
 * Cannot set owner exception
 */
class FileIOCannotSetOwnerException extends FileIOException {
    /**
     * Constructor
     * 
     * @param string $path
     * @param string $owner
     */
    public function __construct($path, $owner) {
        parent::__construct('cannot_set_owner', array('path' => $path, 'owner' => $owner));
    }
}

/**
 * Cannot set group exception
 */
class FileIOCannotSetGroupException extends FileIOException {
    /**
     * Constructor
     * 
     * @param string $path
     * @param string $group
     */
    public function __construct($path, $group) {
        parent::__construct('cannot_set_group', array('path' => $path, 'group' => $group));
    }
}

/**
 * Not implemented exception
 */
class FileIONotImplementedException extends FileIOException {
    /**
     * Constructor
     * 
     * @param string $method
     */
    public function __construct($method) {
        parent::__construct('not_implemented', array('method' => $method));
    }
}
