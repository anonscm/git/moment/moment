<?php

/**
 *     Moment - RestPluginExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');


/**
 * REST plugin name missing exception
 */
class RestPluginNameMissingException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_plugin_name_missing', 400);
    }
}


/**
 * REST plugin endpoint missing exception
 */
class RestPluginEndpointMissingException extends RestException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('rest_plugin_endpoint_missing', 400);
    }
}


/**
 * REST plugin endpoint missing exception
 */
class RestPluginEndpointNotImplementedException extends RestException {
    /**
     * Constructor
     * 
     * @param string $endpointName The endpoint name
     */
    public function __construct($endpointName) {
        parent::__construct('rest_plugin_endpoint_not_implemented: '.$endpointName, 501);
    }
}


/**
 * REST plugin endpoint missing exception
 */
class RestPluginMethodNotImplementedException extends RestException {
    /**
     * Constructor
     * 
     * @param string $method The method
     */
    public function __construct($method) {
        parent::__construct('rest_plugin_method_not_implemented: '.$method, 501);
    }
}

