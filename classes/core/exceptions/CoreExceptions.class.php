<?php

/**
 *     Moment - CoreExceptions.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * File not found exception
 */
class CoreFileNotFoundException extends DetailedException
{
    /**
     * Constructor
     * 
     * @param string $file the file path
     */
    public function __construct($file)
    {
        parent::__construct(
            'core_file_not_found', // Message to give to the user
            'file : '.$file // Details to log
        );
    }
}

/**
 * File not readable
 */
class CoreCannotReadFileException extends DetailedException
{
    /**
     * Constructor
     * 
     * @param string $file the file path
     */
    public function __construct($file)
    {
        parent::__construct(
            'core_cannot_read_file', // Message to give to the user
            'file : '.$file // Details to log
        );
    }
}

/**
 * File not writable
 */
class CoreCannotWriteFileException extends DetailedException
{
    /**
     * Constructor
     * 
     * @param string $file the file path
     */
    public function __construct($file)
    {
        parent::__construct(
            'core_cannot_write_file', // Message to give to the user
            'file : '.$file // Details to log
        );
    }
}

/**
 * File not deletable
 */
class CoreCannotDeleteFileException extends DetailedException
{
    /**
     * Constructor
     * 
     * @param string $file the file path
     */
    public function __construct($file)
    {
        parent::__construct(
            'core_cannot_delete_file', // Message to give to the user
            'file : '.$file // Details to log
        );
    }
}
