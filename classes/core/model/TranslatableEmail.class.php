<?php

/**
 *     Moment - TranslatableEmail.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Represents a recipient in database
 * 
 * @property array $transfer related transfer
 */
class TranslatableEmail extends Entity {
    /**
     * Database map
     */
    protected static $dataMap = array(
        'id' => array(
            'type' => 'uint',
            'size' => 'medium',
            'primary' => true,
            'autoinc' => true
        ),
        'token' => array(
            'type' => 'string',
            'size' => 60
        ),
        'translation_id' => array(
            'type' => 'string',
            'size' => 255
        ),
        'variables' => array(
            'type' => 'text',
            'transform' => 'json'
        ),
        'created' => array(
            'type' => 'datetime'
        ),
    );
    
    /**
     * Properties
     */
    protected $id = null;
    protected $token = '';
    protected $translation_id = '';
    protected $variables = null;
    protected $created = 0;
    
    /**
     * Loads translatable email from token
     * 
     * @param string $token the token
     * 
     * @throws TranslatableEmailNotFoundException
     * 
     * @return self
     */
    public static function fromToken($token) {
        $statement = DBI::prepare('SELECT * FROM '.self::getDBTable().' WHERE token = :token');
        $statement->execute(array(':token' => $token));
        $data = $statement->fetch();
        if(!$data) throw new TranslatableEmailNotFoundException('token = '.$token);
        
        $email = self::fromData($data);
        
        return $email;
    }

    /**
     * Create a new translatable email
     *
     * @param string $translation_id
     * @param array $variables
     *
     * @return TranslatableEmail
     *
     * @throws UtilitiesUidGeneratorBadUnicityCheckerException
     * @throws UtilitiesUidGeneratorTriedTooMuchException
     */
    public static function create($translation_id, $variables) {
        $email = new self();
        
        // Get translation data and variables
        $email->translation_id = $translation_id;
        
        $email->variables = array();
        if($variables) foreach($variables as $k => $v) {
            // Convert Entity types to type/id pairs for saving
            if($v instanceof Entity) $v = array(
                'entity_type' => get_class($v),
                'entity_uid' => $v->getUID(false)
            );
            $email->variables[$k] = $v;
        }
        
        // Add meta
        $email->created = time();
        
        // Generate token until it is indeed unique
        $email->token = Utilities::generateUID(function($token) {
            $statement = DBI::prepare('SELECT * FROM '.TranslatableEmail::getDBTable().' WHERE token = :token');
            $statement->execute(array(':token' => $token));
            $data = $statement->fetch();
            return !$data;
        });
        
        $email->save();
        
        return $email;
    }
    
    /**
     * Prepare mail to be sent to recipient
     * 
     * @param string $translation_id
     * @param mixed $to recipient
     * @param mixed * translation args
     * 
     * @return ApplicationMail
     */
    public static function prepare($translation_id, $to) {
        $vars = array_slice(func_get_args(), 2);
        $about = count($vars) ? $vars[0] : null;
        if(!is_scalar($to)) array_unshift($vars, $to);
        
        // compute lang from arguments
        $lang = null;
        if(is_object($to) && $to instanceof User) {
            $lang = $to->lang;
            $to = $to->email;
        }
        
        // Translate mail parts
        $email_translation = call_user_func_array(array(Lang::translateEmail($translation_id, $lang), 'replace'), $vars);
        
        // Build mail with body and footer
        $plain = $email_translation->plain->out();
        $html = $email_translation->html->out();
        
        // No need for translatable emails if only one language available ...
        if(count(Lang::getAvailableLanguages()) > 1) {
            // Create object
            $translatable = self::create($translation_id, $vars);
            
            // Translate specific footer
            $footer_translation = Lang::translateEmail('translate_email_footer', $lang)->r($translatable);
            
            $plain .= "\n\n".$footer_translation->plain->out();
            $html .= "\n\n".$footer_translation->html->out();
        }
        
        return new ApplicationMail(new Translation(array(
            'subject' => $email_translation->subject->out(),
            'plain' => $plain,
            'html' => $html,
        )), $to, $about, array_slice($vars, 1));
    }
    
    /**
     * Send to recipient (be it Guest, Recipient ...)
     * 
     * @param string $translation_id
     * @param mixed $to recipient
     * @param mixed * translation args
     */
    public static function quickSend($translation_id, $to) {
        $mail = call_user_func_array(get_called_class().'::prepare', func_get_args());
        
        $mail->send();
    }
    
    /**
     * Translate stored in given language
     * 
     * @param string $lang lang code (use current env lang if null given)
     * 
     * @return Translation
     */
    public function translate($lang = null) {
        // Recreate translation variables
        $variables = array();
        if($this->variables) foreach($this->variables as $k => $v) {
            if(is_object($v)) $v = (array)$v;
            
            // Reverse Entity conversion
            if(array_key_exists('entity_type', $v) && array_key_exists('entity_uid', $v))
                $v = call_user_func($v['entity_type'].'::fromId', $v['entity_uid']);
            
            $variables[$k] = $v;
        }
        
        // Translate mail
        $translation = Lang::translateEmail($this->translation_id, $lang);
        
        // Replace variables
        return call_user_func_array(array($translation, 'replace'), $variables);
    }
    
    /**
     * Getter
     * 
     * @param string $property property to get
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array('id', 'token', 'translation_id', 'variables', 'created'))) return $this->$property;
        
        if($property == 'link') {
            return Config::get('application_url').'translate_email?token='.$this->token;
        }
        
        throw new PropertyAccessException($this, $property);
    }
    
    /**
     * Setter
     * 
     * @param string $property property to get
     * @param mixed $value value to set property to
     * 
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        throw new PropertyAccessException($this, $property);
    }
}
