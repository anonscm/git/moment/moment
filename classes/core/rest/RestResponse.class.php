<?php

/**
 *     Moment - RestResponse.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST request
 */
class RestResponse {
    /**
     *
     * @var mixed Callback type
     */
    private static $callback = null;
    
    
    /**
     *
     * @var array List of mandatory fields
     */
    private static $mandatoryFields = array();
    
    /**
     * Set the callback (jsonp, jsonp POST)
     *
     * @param string $type
     * @param string $name
     *
     * @throws RestBadParameterException
     */
    public static function setCallback($type, $name = null) {
        if (!$type) {
            self::$callback = null;
            return;
        }
        
        if(!in_array($type, array('javascript', 'html')))
            throw new RestBadParameterException('callback_type');
        
        $name = preg_replace('`[^a-z0-9_\.-]`i', '', (string)$name);
        if(!$name)
            throw new RestBadParameterException('callback');
        
        self::$callback = array('type' => $type, 'name' => $name);
    }

    /**
     * Send response
     * 
     * @param mixed $data Data to send
     * @param int $code HTTP code
     *
     * @throws RestBadParameterException
     */
    public static function send($data = null, $code = 200) {
        // Output data
        if(self::$callback) {
            if(is_object($data) && ($data instanceof Exception))
                $data = RestResponseJson::castException($data);
            
            if(!self::$callback['name']) // no callback name : return json exception
                RestResponseJson::renderException(new RestBadParameterException('callback'));
            
            if(self::$callback['type'] === 'javascript') {
                // script embedding call
                header('Content-Type: text/javascript');
                echo self::$callback['name'].'('.JSON::encode($data).');';
                
            } else if(self::$callback['type'] === 'frame') {
                // Frame embeddeding call
                header('Content-Type: text/html');
                echo '<html><body><script type="text/javascript">';
                echo 'window.parent.'.self::$callback['name'].'('.JSON::encode($data).');';
                echo '</script></body></html>';
                
            } else {
                // Unknown call type : return json exception
                RestResponseJson::renderException(new RestBadParameterException('callback type'));
            }
            
            exit;
        }
        
        $format = RestRequest::getOutputProperty('format');
        if(!$format) $format = 'json';
        
        $renderer = 'RestResponse'.ucfirst($format);
        if(!class_exists($renderer)) {
            // Unknown renderer, fall back to json
            $renderer = 'RestResponseJson';
            $data = new RestOutputFormatNotSupportedException($format); // Don't throw
        }
        
        if(is_object($data) && ($data instanceof Exception)) {
            $code = $data->getCode();
            if($code < 400 || $code >= 600) $code = 500;
            
            while($renderer && $renderer::EXCEPTION_RENDERER && $renderer::EXCEPTION_RENDERER !== true) {
                // Climb up the exception renderer fallbacks
                $renderer = 'RestResponse'.ucfirst($renderer::EXCEPTION_RENDERER);
            }
            
            if(!$renderer) // Fallback
                $renderer = 'RestResponsePlain';
        }
        
        try {
            if(is_object($data) && ($data instanceof RestPostResponse)) {
                $data = $data->prepare();
                
            } else {
                RestUtilities::sendResponseCode($code);
            }
            
            if(is_object($data) && ($data instanceof RestResponseRaw)) {
                // Already a renderer
                $renderer = $data;
                
            } else if(is_object($data) && ($data instanceof RestFileDownload)) {
                if (!is_object($data->data) or !($data->data instanceof RestResponseRaw)){
                    // Untyped download
                    $renderer = new RestFileDownload(new $renderer($data->data));
                }else{
                    $renderer = $data;
                }
            } else {
                // Raw data
                $renderer = new $renderer($data);
            }
            
            if(RestRequest::getOutputProperty('download') && !($data instanceof RestFileDownload)) {
                // Download requested, cast to download if needed
                $renderer = new RestFileDownload($renderer);
            }
            
            $renderer->output();
            
        } catch(Exception $e) {
            $code = $e->getCode();
            self::send($e, $code ? $code : 500);
            return;
        }
    }
    
    /**
     * Type of callback asked
     * 
     * @return array The callback
     */
    public static function getCallback() {
        return self::$callback;
    }
    
    /**
     * Add mandatory field(s) to the response
     * 
     * @param array|string $fields Field(s) to add
     */
    public static function addMandatoryFields($fields){
        if (!is_array($fields))
            $fields = array($fields);
        
        foreach ($fields as $field){
            self::$mandatoryFields[] = $field;
        }
    }
    
    /**
     * Get mandatory fields
     * 
     * @return array
     */
    public static function getMandatoryFields() {
        return self::$mandatoryFields;
    }
}
