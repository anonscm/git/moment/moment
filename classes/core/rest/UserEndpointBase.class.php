<?php

/**
 *     Moment - UserEndpointBase.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST user endpoint base
 */
abstract class UserEndpointBase extends RestEndpoint {
    /**
     * Cast a User to an array for response
     * 
     * @param User user
     * 
     * @return array
     */
    public static function cast(User $user) {
        $data = array(
            'id' => $user->id,
            'attributes' => $user->attributes->getAll(),
            'created' => RestUtilities::formatDate($user->created),
            'last_activity' => RestUtilities::formatDate($user->last_activity),
            'lang' => $user->lang
        );
        
        if(Auth::isAdmin()) {
            $data['email'] = $user->email;
        }
        
        return $data;
    }
    
    
    /**
     * Allow JSONP
     * 
     * @param string $method
     * @param array $path
     * 
     * @return boolean
     */
    public static function isJSONPAllowed($method, $path) {
        return ($method == 'get') && preg_match('`^@me/remote_auth_config`', implode('/', $path));
    }

    /**
     * Get user(s)
     *
     * Call examples :
     *  /user : get all users (admin)
     *  /user/@me : get current user (null if no session)
     *  /user/<uid> : get user (admin or current)
     *
     * @param int $id user id to get info about
     * @param string $property
     *
     * @return mixed
     *
     * @throws AuthRemoteUserRejectedException
     * @throws AuthUserNotAllowedException
     * @throws ModelViolationException
     * @throws RestAdminRequiredException
     * @throws RestAuthenticationRequiredException
     * @throws RestOwnershipRequiredException
     */
    public static function get($id = null, $property = null) {
        // "Session getter"
        if($id == '@me') {
            if(!Auth::isAuthenticated()) return null;
            
            $user = Auth::user();
            
            if($property == 'remote_auth_config') {
                $perm = array_key_exists('remote_auth_sync_request', $_SESSION) ? $_SESSION['remote_auth_sync_request'] : null;
                if(!$perm)
                    throw new RestAuthenticationRequiredException();
                
                unset($_SESSION['remote_auth_sync_request']);
                
                if($perm['expires'] < time())
                    throw new RestAuthenticationRequiredException();
                
                $code = func_get_arg(2);
                if(!$code || $code !== $perm['code'])
                    throw new RestAuthenticationRequiredException();
                
                if(!Config::get('auth_remote.user.enabled'))
                    throw new AuthRemoteUserRejectedException($user->id, 'remote auth disabled');
                
                if(!$user->auth_secret)
                    throw new AuthRemoteUserRejectedException($user->id, 'no secret set');
                
                return array('remote_config' => $user->remote_config);
            }
            
            $user = static::cast($user);
            
            if(Auth::isSP() && AuthSP::isImpersonating())
                $user['impersonated'] = true;
            
            return $user;
        }
        
        // Need to be authenticated ...
        if(!Auth::isAuthenticated()) throw new RestAuthenticationRequiredException();
        
        if($id) {
            $user = User::fromId($id);
            
            // Check ownership
            if(!$user->is(Auth::user()) && !Auth::isAdmin())
                throw new RestOwnershipRequiredException(Auth::user()->id, 'user = '.$user->id);
            
            return self::cast($user);
        }
        
        if(!Auth::isAdmin())
            throw new RestAdminRequiredException();

        $users = RestUtilities::getEntities('User', array('id', 'attributes'));
        
        $data = array();
        foreach($users as $user)
            $data[] = static::cast($user);

        return $data;
    }
    
    /**
     * Create new user profile
     * 
     * @throws RestAuthenticationRequiredException
     * @throws RestBadParameterException
     */
    public static function post() {
        if(!Auth::isAuthenticated() || !Auth::isRemoteApplication())
            throw new RestAuthenticationRequiredException();
    
        // Need to be admin...
        if(!Auth::isAdmin()) throw new RestAdminRequiredException();
        
        $input = RestRequest::getInput();
        
        if(!property_exists($input, 'uid'))
            throw new RestMissingParameterException('uid');
        
        if(!is_string($input->uid))
            throw new RestBadParameterException('uid');
        
        if(!property_exists($input, 'emails'))
            throw new RestMissingParameterException('emails');
        
        if(!is_array($input->emails))
            throw new RestBadParameterException('emails');
        
        if(!property_exists($input, 'name'))
            throw new RestMissingParameterException('name');
        
        if(!is_string($input->name))
            throw new RestBadParameterException('name');
        
        $user = User::create($input->uid);
        
        foreach((array)$input as $attr => $value) {
            if($attr == 'uid') continue;
            $user->attributes->$attr = $value;
        }
        
        $user->save();
        
        return new RestPostResponse('/user/'.$user->id, static::cast($user));
    }
        
    /**
     * Update user properties
     * 
     * Call examples :
     *  /user/@me : update current user
     * 
     * @param mixed $id
     * 
     * @throws RestAuthenticationRequiredException
     * @throws RestAdminRequiredException
     * @throws RestBadParameterException
     */
    public static function put($id) {
        if(!Auth::isAuthenticated() || !Auth::isSP())
            throw new RestAuthenticationRequiredException();
        
        if($id != '@me')
            throw new RestBadParameterException('id');
        
        $input = RestRequest::getInput();
        
        if(property_exists($input, 'impersonated_user_id')) {
            if($input->impersonated_user_id && !Auth::isAdmin())
                throw new RestAdminRequiredException();
            
            AuthSP::impersonate($input->impersonated_user_id);
        }
    }
}
