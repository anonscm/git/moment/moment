<?php

/**
 *     Moment - RestStructuredResponse.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST raw response
 */
class RestStructuredResponse extends RestResponseRaw {
    /**
     * Constructor
     * 
     * @param mixed $data
     * 
     * @return mixed
     */
    public static function clean($data) {
        if(is_object($data) && ($data instanceof Exception))
            return $data;
        
        $fields = RestRequest::getOutputProperty('fields');
        if(!is_null($fields)) {
            $fields->merge(RestResponse::getMandatoryFields());
            self::cleanData($data, $fields);
        }
        
        return $data;
    }
    
    /**
     * Allows to clean data to send to the reponse
     * 
     * @param mixed $data Data to cleans
     * @param RestResponseFields $fs Fields to hold on the response
     */
    private static function cleanData(&$data, RestResponseFields $fs) {
        $keys = array_keys($fs->fields);
        
        if (!is_array($data)) return;
        
        $dataKeys = array_filter(array_keys($data), function($k){
            return !is_int($k);
        });
        
        // Check if at least one key string, it is an entity
        if (count($dataKeys)){
            foreach (array_keys($data) as $k){
                if (!in_array($k, $keys)) unset($data[$k]);
            }
            foreach ($fs->fields as $field => $sub){
                if (is_bool($sub)) continue;
                if (!array_key_exists($field, $data)) continue;

                self::cleanData($data[$field], $sub);
            }
        }else{
            foreach ($data as &$item){
                self::cleanData($item, $fs);
            }
        }
        $data = array_filter($data);
    }
    
    /**
     * Cast exception
     * 
     * @param Exception $e
     * 
     * @return array
     */
    public static function castException($e) {
        return array(
            'message' => $e->getMessage(),
            'uid' => method_exists($e, 'getUid') ? $e->getUid() : null,
            'details' => method_exists($e, 'getDetails') ? $e->getDetails() : null
        );
    }
}
