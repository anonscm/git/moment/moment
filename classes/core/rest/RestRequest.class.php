<?php

/**
 *     Moment - RestRequest.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST request
 */
class RestRequest {
    /**
     * Main request properties
     */
    private static $method   = null;
    private static $endpoint = null;
    private static $path     = array();
    private static $options  = array();
        
    
    /*
     * Input data
     */
    private static $input = null;
    
    /**
     * Properties of the request (content type, charset ...)
     */
    private static $properties = array();
    
    /**
     * Output properties the client asked for
     */
    private static $outputProperties = array(
        'count' => null,
        'startIndex' => null,
        'format' => 'json',
        'filter' => null,
        'fields' => null,
        'order' => array(),
        'updatedSince' => null
    );
    
    /**
     * Custom filters holder
     *
     * A custom filter is addressed by its key and must be a callable
     * that takes the given expression as 1st parameter and the list of
     * variables as 2nd parameter.
     *
     * The callable is to validate the given expression at run time and
     * throw if anything wrong.
     */
    private static $custom_filters = null;
    
    /**
     * Constructor
     */
    public static function init() {
        if(is_null(self::$custom_filters)) {
            self::$custom_filters = (new Event('rest_custom_filters'))->trigger(function() {
                return array();
            });
        }
        
        // Split request path to get tokens
        if(array_key_exists('PATH_INFO', $_SERVER)) self::$path = array_filter(explode('/', $_SERVER['PATH_INFO']));
        
        // Get method from possible headers
        foreach(array('X_HTTP_METHOD_OVERRIDE', 'REQUEST_METHOD') as $k) {
            if(!array_key_exists($k, $_SERVER)) continue;
            self::$method = strtoupper($_SERVER[$k]);
        }
        
        // Record called method (for log), fail if unknown
        if(!RestMethods::isValid(self::$method))
            throw new RestMethodNotAllowedException();
        
        /*
         * NOT USED ANYMORE
         *
        
        // Get return format from whole path
        $last = (string) array_pop(self::$path);
        if(preg_match('`^(.+)\.([a-z0-9_-]+)$`', $last, $m)) {
            self::setFormat($m[2]);
            $last = $m[1];
        }
        if($last) array_push(self::$path, $last);
        
        */
        
        // Get endpoint (first token), fail if none
        $endpoint_part = array_shift(self::$path);
        if(!$endpoint_part) throw new RestEndpointNotFoundException();
        
        $endpoint_part = preg_split('`[\s\+]+`', trim($endpoint_part));
        self::$endpoint = array_shift($endpoint_part);
        self::$options = $endpoint_part;
        
        // Get request content type from possible headers
        $type = array_key_exists('CONTENT_TYPE', $_SERVER) ? $_SERVER['CONTENT_TYPE'] : null;
        if(!$type && array_key_exists('HTTP_CONTENT_TYPE', $_SERVER)) $type = $_SERVER['HTTP_CONTENT_TYPE'];
        
        // Parse content type
        $type_parts = array_map('trim', explode(';', $type));
        $type = array_shift($type_parts);
        self::$properties['type'] = $type;
        
        foreach($type_parts as $part) {
            $part = array_map('trim', explode('=', $part));
            if(count($part) == 2) self::$properties[$part[0]] = $part[1];
        }
        
        // Parse body
        switch($type) {
            case 'text/plain' :
                self::$input = trim(HttpRequest::body());
                break;
            
            case 'application/octet-stream' :
                // Don't sanitize binary input, don't copy either (memory) !
                self::$input = function() {
                    return HttpRequest::body();
                };
                break;
            
            case 'application/x-www-form-urlencoded' :
                $data = array();
                $body = HttpRequest::body();
                if($body) {
                    parse_str($body, $data);
                    self::$input = (object)$data;
                }
                break;
            
            case 'text/xml':
                $body = HttpRequest::body();
                if($body)
                    self::$input = RestUtilities::parseXML($body);
                break;
            
            case 'application/json' :
            default :
                $body = trim(HttpRequest::body());
                if($body)
                    self::$input = JSON::decode($body);
        }
        
        // JSONP specifics
        $jsonp = false;
        if(array_key_exists('callback', $_GET)) {
            if(self::$method != 'get')
                throw new RestJSONPBadMethodException();
            
            RestResponse::setCallback('javascript', $_GET['callback']);
            $jsonp = true;
        }
        
        if(array_key_exists('frame_callback', $_GET)) {
            if(self::$method != 'get' && self::$method != 'post')
                throw new RestJSONPBadMethodException();
            
            RestResponse::setCallback('frame', $_GET['callback']);
            $jsonp = true;
        }
        
        if($jsonp) {
            // Forward to handler, fail if unknown or method not implemented
            $class = ucfirst(self::$endpoint).'Endpoint';
            if(
                !file_exists(EKKO_ROOT.'/classes/endpoints/'.$class.'.class.php') &&
                !file_exists(EKKO_ROOT.'/classes/core/endpoints/'.$class.'.class.php')
            )
                throw new RestEndpointNotFoundException();
                
            $allowed = call_user_func($class.'::isJSONPAllowed', self::$method, self::$path);
            
            $cfg = Config::get('rest.jsonp_rules');
            if(is_array($cfg)) {
                $url = '/'.self::$endpoint.(count(self::$path) ? '/'.implode('/', self::$path) : '');
                
                foreach($cfg as $matcher => $rule) {
                    if(preg_match('`^(get|post)\s+(.+)$`i', $matcher, $parts)) {
                        if(self::$method != strtolower($parts[1])) continue;
                        $matcher = $parts[2];
                    }
                    
                    $matcher = str_replace('`', '\\`', $matcher);
                    if(!preg_match('`'.$matcher.'`', $url)) continue;
                    
                    $allowed = $rule;
                }
                
            } else if(!is_null($cfg)) {
                $allowed = (bool)$cfg;
            }
            
            if(!$allowed) throw new RestJSONPNotAllowedException();
        }
        
        // Get response filters
        foreach($_GET as $k => $v) {
            switch($k) {
                case 'count':
                case 'startIndex':
                    if(preg_match('`^[0-9]+$`', $v)) self::$outputProperties[$k] = (int)$v;
                    break;

                case 'format':
                    self::setFormat($v);
                    break;

                case 'filterOp':
                    if(is_array($v)) {
                        $and = array();
                        foreach($v as $field => $test){
                            foreach(array('equals', 'startWith', 'contains', 'present') as $op){
                                if(array_key_exists($op, $test))
                                    $and[] = array($op => array($field => $test[$op]));
                            }
                        }
                        self::$outputProperties['filter'] = new RestFilter(array('and' => $and));
                    }
                    break;

                case 'filter':
                    $filter = JSON::decode((string)$v);
                    if(!$filter)
                        throw new RestBadParameterException('filter');

                    self::$outputProperties['filter'] = new RestFilter($filter);
                    break;

                case 'fields':
                    $fields = array_filter(array_unique(array_map('trim', explode(',', $v))));
                    
                    if(!count($fields))
                        throw new RestBadParameterException('fields');

                    self::$outputProperties['fields'] = new RestResponseFields($fields);
                    break;

                case 'sortOrder':
                    if ($v === 'asc') $v = 'ascending';
                    if ($v === 'desc') $v = 'descending';
                        
                    if(!in_array($v, array('ascending', 'descending')))
                        throw new RestBadParameterException('sortOrder');

                    self::$outputProperties['order']['*'] = $v;
                    break;

                case 'order':
                    $order = JSON::decode((string)$v);
                    if(!$order)
                        throw new RestBadParameterException('order');

                    foreach((array)$order as $sk => $o) {
                        if(!preg_match('`^[a-z][a-z0-9_]*$`i', $sk))
                            throw new RestBadParameterException('order['.$sk.']');

                        if ($o === 'asc') $o = 'ascending';
                        if ($o === 'desc') $o = 'descending';
                        if($o != 'ascending' && $o != 'descending')
                            throw new RestBadParameterException('order['.$sk.'] = '.$o);

                        self::$outputProperties['order'][$sk] = $o;
                    }
                    break;

                case 'updatedSince':
                    // updatedSince takes ISO date, relative N days|weeks|months|years format and epoch timestamp (UTC)
                    $updatedSince = null;
                    if(preg_match('`^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(Z|[+-][0-9]{2}:[0-9]{2})$`', $v)) {
                        // ISO date
                        $localetz = new DateTimeZone(Config::get('timezone'));
                        $offset = $localetz->getOffset(new DateTime($v));
                        $updatedSince = strtotime($v) + $offset;
                    }else if(preg_match('`^([0-9]+)\s*(hour|day|week|month|year)s?$`', $v, $m)) {
                        // Relative N day|days|week|weeks|month|months|year|years format
                        $updatedSince = strtotime('-'.$m[1].' '.$m[2]);
                    }else if(preg_match('`^-?[0-9]+$`', $v)) $updatedSince = (int)$v; // Epoch timestamp

                    if(!$updatedSince || !is_numeric($updatedSince))
                        throw new RestUpdatedSinceBadFormatException($updatedSince);

                    self::$outputProperties['updatedSince'] = $updatedSince;
                    break;
            }
        }
    }
    
    /**
     * Set format from given data
     *
     * @param string $raw
     */
    public static function setFormat($raw) {
        $equiv = array(
            'txt' => 'plain',
            'htm' => 'html',
            'jpg' => 'jpeg'
        );
        
        $types = array(
            'json' => 'structured',
            'xml' => 'structured',
            'csv' => 'table',
            'pdf' => 'pdf',
            'plain' => 'text',
            'html' => 'text',
            'png' => 'image',
            'gif' => 'image',
            'jpg' => 'image'
        );
        
        if(preg_match('`^((?:download:)?)([a-z0-9_-]+)((?::.+)?)$`i', $raw, $m)) {
            $fmt = array_key_exists($m[2], $equiv) ? $equiv[$m[2]] : $m[2];
            
            self::$outputProperties['download'] = ($m[1] == 'download:');
            self::$outputProperties['format'] = $fmt;
            self::$outputProperties['format_type'] = array_key_exists($fmt, $types) ? $types[$fmt] : null;
            self::$outputProperties['format_options'] = substr($m[3], 1);
        }
    }
    
    /**
     * Get current context
     */
    public static function getContext() {
        return array(
            'method' => self::$method,
            'endpoint' => self::$endpoint,
            'path' => self::$path
        );
    }
    
    /**
     * Get method
     *
     * @return string The method
     */
    static function getMethod() {
        return self::$method;
    }

    /**
     * Get endpoint
     *
     * @return string The endpoint
     */
    static function getEndpoint() {
        return self::$endpoint;
    }

    /**
     * Get path
     *
     * @return array The path
     */
    static function getPath() {
        return self::$path;
    }
    
    
    /**
     * Get input
     *
     * @param array $doNotSanitize
     * @return stdClass The input
     */
    static function getInput(array $doNotSanitize = array()) {
        $doNotSanitize = array_map(function($regexp){
            return '`'.str_replace('*', '[^/]+', $regexp).'`';
        }, $doNotSanitize);
        
        return self::sanitize(self::$input, $doNotSanitize);
    }
    
    
    /**
     * @param $data
     * @param array $doNotSanitize
     * @param string $path
     * @return array|mixed
     */
    private static function sanitize($data, array $doNotSanitize = array(), $path = ''){
        if (is_object($data)){
            foreach (get_object_vars($data) as $k => $v) {
                $v = self::sanitize($v, $doNotSanitize, $path ? $path.'/'.$k : $k);
                $data->$k = $v;
            }
            return $data;
            
        }else if (is_array($data)){
            return array_map(function($d, $k) use ($doNotSanitize, $path) {
                return self::sanitize($d, $doNotSanitize,
                    $path ? $path.'/'.$k : $k);
                
            }, $data, array_keys($data));
            
        }else{
            $toSanitize = true;
            foreach ($doNotSanitize as $regexp){
                if (preg_match($regexp, $path))
                    $toSanitize = false;
            }
            if ($toSanitize) $data = Utilities::sanitizeInput($data);
            return $data;
            
        }
    }
    
    
    /**
     * Get options
     *
     * @return array The options
     */
    static function getOptions() {
        return self::$options;
    }

    
    /**
     * Get properties
     *
     * @param string $value Get a specific output property
     *
     * @return array|string|null The property / properties
     */
    static function getProperty($value = null) {
        if (!is_null($value) && isset(self::$properties[$value]))
            return self::$properties[$value];
        
        return null;
    }


    /**
     * Get output properties
     *
     * @param string $value Get $value a specific output property
     *
     * @return mixed
     */
    static function getOutputProperty($value = null) {
        if (!is_null($value) && isset(self::$outputProperties[$value]))
            return self::$outputProperties[$value];
        
        return null;
    }
    
    /**
     * Get all properties
     *
     * @return array The properties
     */
    static function getProperties() {
        return self::$properties;
    }

    /**
     * Get all output properties
     *
     * @return array The output properties
     */
    static function getOutputProperties() {
        return self::$outputProperties;
    }
}
