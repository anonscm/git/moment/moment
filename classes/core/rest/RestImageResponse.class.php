<?php

/**
 *     Moment - RestImageResponse.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST image response base
 */
class RestImageResponse extends RestResponseRaw {
    /**
     * @var resource data
     */
    protected $data = null;

    /**
     * Constructor
     *
     * @param mixed $data image resource or exception
     *
     * @throws RestOutputConversionException
     */
    public function __construct($data) {
        $type = static::getType();
        if(!function_exists('image'.$type))
            throw new RestOutputConversionException('image : unknown type');
        
        if(is_object($data) && ($data instanceof Exception)) {
            $msg = $data->getMessage();
            
            if(method_exists($data, 'getUid'))
                $msg .= ' (uid: '.$data->getUid().')';
            
            if(method_exists($data, 'getDetails'))
                $msg .= ', details: '.JSON::encode($data->getDetails());
            
            $font = 2;
            $width = imagefontwidth($font) * strlen($msg) + 10;
            $height = imagefontheight($font) + 10;
            
            $data = imagecreate($width, $height);
            
            $bg_color = imagecolorallocate($data, 255, 255, 255);
            $fg_color = imagecolorallocate($data, 0, 0, 0);
            
            imagefill($data, 0, 0, $bg_color);
            imagestring($data, $font, 5, 5, $msg, $fg_color);
        }
        
        if(!is_resource($data) || (get_resource_type($data) != 'gd'))
            throw new RestOutputConversionException('image : non-resource data');
        
        parent::__construct($data);
    }
    
    /**
     * Output data
     */
    public function output() {
        header('Content-Type: '.static::MIME_TYPE);
        
        call_user_func('image'.static::getType(), $this->data);
        
        imagedestroy($this->data);
        $this->data = null;
        
        exit;
    }
    
    /**
     * Get type
     * 
     * @return string
     */
    public static function getType() {
        return strtolower(substr(get_called_class(), 12));
    }
    
    /**
     * Exception rendering shorthand
     * 
     * @param Exception $e
     */
    public static function renderException($e) {
        (new static($e))->output();
    }
}
