<?php

/**
 *     Moment - RestServer.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST server
 */
class RestServer {
    /**
     * Process the request
     */
    public static function process() {
        try {
            // Init RestRequest
            RestRequest::init();
            
            // If undergoing maintenance report it as an error
            if(Config::get('maintenance')) throw new RestUndergoingMaintenanceException();
            
            Logger::debug('Got "'.RestRequest::getMethod().'" request for endpoint "'.RestRequest::getEndpoint().'/'.implode('/', RestRequest::getPath()).'" with '.strlen(HttpRequest::body()).' bytes payload');
            
            // Get authentication state (fills auth data in relevant classes)
            try{
                Auth::isAuthenticated();
            }  catch (Exception $e){
                
                if (preg_match('`^Auth(Remote|SPBad|SPMissingAttribute|User)`',get_class($e)))
                    throw new RestNotAllowedException();
                    
                throw $e;
            }
            
            if(Auth::isRemoteApplication()) {
                // Remote applications must honor ACLs
                $application = AuthRemote::application();
                
                if(!$application->allowedTo(RestRequest::getMethod(), RestRequest::getEndpoint()))
                    throw new RestNotAllowedException();
                
            } else if(Auth::isRemoteUser()) {
                // Nothing peculiar to do
                
            } else if(in_array(RestRequest::getMethod(), array('post', 'put', 'delete'))) {
                // SP or Guest, lets do XSRF check
                $token_name = 'HTTP_X_SECURITY_TOKEN';
                $token = array_key_exists($token_name, $_SERVER) ? $_SERVER[$token_name] : '';
                
                if(RestRequest::getMethod() == 'post' && array_key_exists('security-token', $_POST))
                    $token = $_POST['security-token'];
                
                if(!$token || !Utilities::checkSecurityToken($token))
                    throw new RestXSRFTokenInvalidException($token);
            }
            
            $event = new Event('rest_request');
            
            $data = $event->trigger(function() {
                
                // Forward to handler, fail if unknown or method not implemented
                $class = ucfirst(RestRequest::getEndpoint()).'Endpoint';
                if(
                    !file_exists(EKKO_ROOT.'/classes/endpoints/'.$class.'.class.php') &&
                    !file_exists(EKKO_ROOT.'/classes/core/endpoints/'.$class.'.class.php')
                )
                    throw new RestEndpointNotFoundException();

                if(RestRequest::getMethod() === 'options') {
                    // OPTIONS call received, get default answer
                    $options = get_class_methods($class);

                    // Check if endpoint implements custom
                    if(method_exists($class, 'options')) {
                        $options = call_user_func_array($class.'::options', RestRequest::getPath());
                    }

                    // Cleaning and formatting
                    $options = array_filter(array_unique(array_map('strtoupper', (array)$options)), function($method) {
                        return RestMethods::isValid($method);
                    });

                    // Send answer and exit as per rfc
                    header('Allow: '.implode(', ', $options));
                    header('Content-Length: 0');
                    exit;

                } else {
                    if (!method_exists($class, RestRequest::getMethod())) throw new RestMethodNotImplementedException();

                    Logger::debug('Forwarding call to ' . $class . '::' . RestRequest::getMethod() . '() handler');

                    return call_user_func_array($class . '::' . RestRequest::getMethod(), RestRequest::getPath());
                }
            });
            
            // Output data
            Logger::debug('Got data to send back');
            RestResponse::send($data);
            
        } catch(Exception $e) { // Return exceptions as HTTP errors
            RestResponse::send($e);
        }
    }
}
