<?php

/**
 *     Moment - RestResponseFields.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST response fields
 */
class RestResponseFields {
    
    /**
     * @var (RestResponseFields)[] Fields list
     */
    private $fields = array();
    
    
    /**
     * RestResponseFields constructor
     * 
     * @param array $paths List of paths
     */
    public function __construct(array $paths) {
        $this->merge($paths);
    }


    /**
     * Allows to merge
     *
     * @param array $paths List of paths
     *
     * @throws RestBadParameterException
     */
    public function merge(array $paths){
        $sub = array();
        
        // Group path by prefixes
        foreach ($paths as $path){
            if (!preg_match('`^(_?[a-z][a-z0-9_]*)((?:\.[a-z][a-z0-9_]*)*)$`i', $path, $m))
                throw new RestBadParameterException('fields');
            
            if ($m[2]){
                $sub[$m[1]][] = substr($m[2], 1);
            }else{
                if (!array_key_exists($path, $this->fields)){
                    // Single item this level
                    $this->fields[$path] = true;
                }
            }
        }

        // Create sub collections
        foreach ($sub as $field => $paths){
            if (array_key_exists($field, $this->fields) && is_object($this->fields[$field])){
                $this->fields[$field]->merge($paths);
            }else{
                $this->fields[$field] = new self($paths);
            }
                    
        }
    }
    
    /**
     * Getter for RestResponseFields
     * 
     * @param string $property
     *
     * @return mixed
     *
     * @throws PropertyAccessException
     */
    public function __get($property) {
        if ($property === 'fields'){
            return $this->fields;
        }
        throw new PropertyAccessException($this, $property);
    }
}
