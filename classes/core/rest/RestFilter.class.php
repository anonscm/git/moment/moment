<?php

/**
 *     Moment - RestFilter.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST filter
 *
 * @property array $prefields
 * @property array $postfields
 * @property array $customtests
 * @property RestFilter $upper
 * @property string $path
 * @property RestFilter $root
 * @property string $operator
 * @property RestFilter|mixed $target
 */
class RestFilter {
    /**
     * @var array Allowed pre-filter fields
     */
    private $prefields = array();
    
    /**
     * @var array Allowed post-filter fields
     */
    private $postfields = array();
    
    /**
     * @var array Custom tests
     */
    private $customtests = array();
    
    /**
     * @var RestFilter Upper-level container
     */
    private $upper = null;
    
    /**
     * @var int Type
     */
    private $type = null;
    
    /**
     * Types
     */
    const LOGIC = 1;
    const TEST = 2;
    const CUSTOM = 3;
    
    /**
     * @var string Filter operator
     */
    private $operator = null;
    
    /**
     * @var bool Operator validation state
     */
    private $operator_validated = false;
    
    /**
     * @var int Filter index
     */
    private $idx = null;
    
    /**
     * @var string Filter target
     */
    private $target = null;
    
    /**
     * @var bool Target validation state
     */
    private $target_validated = false;
    
    /**
     * @var mixed Filter argument
     */
    private $argument = null;
    
    /**
     * @var bool Skip (because prefiltered)
     */
    private $skip = false;
    
    /**
     * @var array Pre filters placeholders
     */
    private $placeholders = array();
    
    /**
     * @var int Placeholder uid counter
     */
    private $placeholder_uid_counter = 0;
    
    /**
     * Constructor
     * 
     * {"or": [
     *      {"startWith": {"foo": "bar"}},
     *      {"endWith": {"foo": "bar"}}
     * ]}
     * 
     * {"and": [
     *      {"equals": {"id": 42}},
     *      {"or": [
     *          {"contains": {"name": "foo"}},
     *          {"contains": {"description": "plop"}},
     *          {"present": {"guest": true}}
     *      ]}
     * ]}
     * 
     * Supported logical operators :
     *   - not
     *   - and
     *   - notAnd / nand
     *   - or
     *   - notOr / nor
     *   - exclusiveOr / xor
     *   - notExclusiveOr / nxor
     * 
     * 
     * Supported test operators :
     *   - present : emptyness test
     * 
     *   - contains : string based
     *   - startWith : string based
     *   - endWith : string based
     *   - equals : string based (except for boolean)
     *   - in : string based, look for value in an array
     * 
     *   - lessThan : float based
     *   - moreThan : float based
     * 
     *   - matches : regular expression, case insensitive
     *   - caseMatches : regular expression, case sensitive
     * 
     *   - dateEquals : day equality, takes timestamp or strtotime compatible date
     *   - dateBefore : day comparison, takes timestamp or strtotime compatible date
     *   - dateAfter : day comparison, takes timestamp or strtotime compatible date
     *   - dateBetween : day comparison, takes array of two timestamp or strtotime compatible date
     * 
     *   - dateTimeEquals : day and time equality, takes timestamp or strtotime compatible date
     *   - dateTimeBefore : day and time comparison, takes timestamp or strtotime compatible date
     *   - dateTimeAfter : day and time comparison, takes timestamp or strtotime compatible date
     *   - dateTimeBetween : day and time comparison, takes array of two timestamp or strtotime compatible date
     * 
     * 
     * Custom tests may use any name except already defined operators / tests
     * 
     * 
     * @param mixed $filter
     * @param RestFilter $upper
     * @param int $idx
     */
    public function __construct($filter, RestFilter $upper = null, $idx = null) {
        $this->upper = $upper;
        $this->idx = $idx;
        
        $this->parse($filter);
    }

    /**
     * Validate (and reformat) date argument
     *
     * @param mixed $date
     *
     * @return int
     *
     * @throws RestBadParameterException
     */
    public function parseDate($date) {
        if(preg_match('`^[0-9]+$`', $date))
            $date = (int) $date;
        
        if(!is_int($date))
            $date = strtotime($date);
        
        if(!is_int($date)) 
            throw new RestBadParameterException($this->path.' expects date|timestamp');
        
        return $date;
    }
    
    /**
     * Parse raw filter, no field validation yet
     * 
     * @param mixed $filter
     * 
     * @throws RestBadParameterException
     */
    private function parse($filter) {
        if(is_object($filter)) $filter = (array)$filter;
        
        if(!is_array($filter))
            throw new RestBadParameterException($this->path.' is not an array');
        
        if(count($filter) > 1)
            throw new RestBadParameterException($this->path.' has more than one operator');
        
        if(count(array_filter(array_keys($filter), 'is_int')))
            throw new RestBadParameterException($this->path.' has non-operators');
        
        $this->operator = key($filter);
        $target = (array)reset($filter);
        
        if(in_array($this->operator, array('not', 'and', 'notAnd', 'nand', 'or', 'notOr', 'nor', 'exclusiveOr', 'xor', 'notExclusiveOr', 'nxor'))) {
            $this->type = self::LOGIC;
            $this->operator_validated = true;
            
        } else if(in_array($this->operator, array('present', 'contains', 'startWith', 'endWith', 'equals', 'in', 'matches', 'caseMatches', 
            'dateBetween', 'dateBefore', 'dateAfter', 'dateTimeBetween', 'dateTimeBefore', 'dateTimeAfter', 'dateEquals', 'dateTimeEquals'))) {
            $this->type = self::TEST;
            $this->operator_validated = true;
            
        } else {
            $this->type = self::CUSTOM;
        }
        
        if(!count($target))
            throw new RestBadParameterException($this->path.' has no sub-filter');
        
        if($this->operator == 'not') {
            if(count($target) > 1)
                throw new RestBadParameterException($this->path.' has more than one sub-filter');
            
            $this->target = new self($target, $this);
            
        } else if($this->type == self::LOGIC) {
            if(count(preg_grep('`[^0-9]`', array_keys($target))))
                throw new RestBadParameterException($this->path.' is not a list of sub-filters');
            
            $this->target = array();
            foreach(array_values($target) as $idx => $sub)
                $this->target[] = new self($sub, $this, $idx);
            
        } else if($this->type == self::TEST) {
            if(count($target) > 1)
                throw new RestBadParameterException($this->path.' contains more than one field');
            
            $field = key($target);
            $argument = reset($target);
            
            if(!$field || is_int($field))
                throw new RestBadParameterException($this->path.' does not describe a field');
            
            if(!preg_match('`^[a-z][a-z0-9_\.]*$`i', $field))
                throw new RestBadParameterException($this->path.' "'.$field.'" is a bad field name');
            
            if($this->operator === 'dateBetween' || $this->operator === 'dateTimeBetween') {
                if(!is_array($argument))
                    throw new RestBadParameterException($this->path.' expects array argument');
                
                if(count($argument) !== 2 )
                    throw new RestBadParameterException($this->path.' expects two dates');
                
                foreach($argument as &$date)
                    $date = $this->parseDate($date);
                
                sort($argument);
                
            } else if(in_array($this->operator, array(
                'dateEquals', 'dateBefore', 'dateAfter',
                'dateTimeEquals', 'dateTimeBefore', 'dateTimeAfter'
            ))) {
                $argument = $this->parseDate($argument);
                
            } else if($this->operator == 'lessThan' || $this->operator == 'moreThan') {
                if(!preg_match('`^-?[0-9]+(\.[0-9]+)$`', $argument))
                    throw new RestBadParameterException($this->path.' expects int|float');
                
                $argument = (float)$argument;
                
            } else if($this->operator === 'in'){
                if (!is_array($argument)) throw new RestBadParameterException($this->path.' expects array argument');
                
            } else if(!is_scalar($argument)){
                throw new RestBadParameterException($this->path.' expects scalar argument');
            }
            
            $this->target = $field;
            $this->argument = $argument;
            
        } else { // Custom
            $this->argument = $target;
        }
    }
    
    /**
     * Add pre-filtering field
     * 
     * @param string $field
     */
    public function addPrefilteringField($field) {
        if($this->upper) {
            $this->upper->addPrefilteringField($field);
            
        } else {
            foreach((array)$field as $f)
                $this->prefields[] = $f;
        }
    }

    /**
     * Add post-filtering field
     *
     * @param string $field
     * @param callable $getter
     *
     * @throws RestBadParameterException
     */
    public function addPostfilteringField($field, $getter = null) {
        if($this->upper) {
            $this->upper->addPostfilteringField($field, $getter);
            
        } else {
            if(!is_array($field)) $field = array($field => $getter);
            
            foreach($field as $f => $g) {
                if(is_int($f)) {
                    $f = $g;
                    $g = null;
                }
                
                if($g && !is_callable($g))
                    throw new RestBadParameterException('getter for field "'.$f.'" is not callable');
                
                $this->postfields[$f] = $g;
            }
        }
    }
    
    /**
     * Add custom test
     *
     * @param string $name
     * @param callable $test will receive filter argument and test subject
     *
     * @param bool $isPreFilter
     * @throws RestBadParameterException
     */
    public function addCustomTest($name, $test = null, $isPreFilter = false) {
        if($this->upper) {
            $this->upper->addCustomTest($name, $test, $isPreFilter);
            
        } else {
            if(!is_array($name)) $name = array($name => $test);
            
            foreach($name as $n => $t) {
                if(!$t || !is_callable($t))
                    throw new RestBadParameterException('test "'.$n.'" is not callable');
                
                $this->customtests[$n] = array('test' => $t, 'isPreFilter' => $isPreFilter);
            }
        }
    }
    
    
    /**
     * @param string $operator
     *
     * @return bool
     */
    public function usesOperator($operator){
        if ($this->type === self::TEST || $this->type === self::CUSTOM)
            return $this->operator === $operator;
    
        foreach($this->target as $sub){
            if ($sub->usesOperator($operator)) return true;
        }
        
        return false;
    }
    

    /**
     * Add placeholder
     *
     * @param mixed $value
     * @param string $field
     *
     * @return string
     */
    public function addPlaceHolder($value = null, $field = null) {
        if(is_null($field)) $field = $this->target;
        if(is_null($value)) $value = $this->argument;
        
        if($this->upper)
            return $this->upper->addPlaceHolder($value, $field);
        
        $uid = ':'.$field.'__'.$this->placeholder_uid_counter++;
        $this->placeholders[$uid] = $value;
        
        return $uid;
    }
    
    /**
     * Check if field (target) is allowed
     * 
     * @param string $order
     * 
     * @return mixed
     */
    public function isTargetAllowed($order = null) {
        $ok = false;
        
        if ($this->type === self::CUSTOM){
            if ($this->root->customtests[$this->operator]['isPreFilter'])
                $ok = 'pre';
            else
                $ok = 'post';
        }else if($this->type === self::TEST){
            if (array_key_exists($this->target, $this->root->postfields))
                $ok = 'post';
    
            if (in_array($this->target, $this->root->prefields))
                $ok = 'pre';
        }
        
        return is_null($order) ? $ok : ($ok == $order);
    }

    /**
     * Validate filter
     *
     * @param boolean $fatal
     *
     * @throws RestBadParameterException
     */
    public function validate($fatal = true) {
        if($this->type == self::LOGIC) return;
        if($this->operator_validated && $this->target_validated) return;
        
        // Validate test
        if(($this->type == self::CUSTOM) && !$this->operator_validated) {
            if(!array_key_exists($this->operator, $this->root->customtests)) {
                if($fatal)
                    throw new RestBadParameterException($this->path.' doesn\'t relate to a callable custom filter');
                
            } else {
                $this->operator_validated = true;
            }
        }
        
        // Validate field
        if ($this->type === self::CUSTOM)
            $this->target_validated = true;
        
        if(!$this->target_validated) {
            if(!$this->isTargetAllowed()) {
                if($fatal)
                    throw new RestBadParameterException($this->path.' cannot be applied on field '.$this->target);
                
            } else {
                $this->target_validated = true;
            }
        }
    }

    /**
     * Getter
     *
     * @param string $property
     *
     * @return mixed
     *
     * @throws PropertyAccessException
     */
    public function __get($property) {
        if($property == 'prefields') return $this->prefields;
        
        if($property == 'postfields') return $this->postfields;
        
        if($property == 'customtests') return $this->customtests;
        
        if($property == 'upper') return $this->upper;
        
        if($property == 'path') return ($this->upper ? $this->upper->path : 'filter').'>'.$this->operator.($this->idx ? '['.$this->idx.']' : '');
        
        if($property == 'root') return $this->upper ? $this->upper->root : $this;
        
        if($property == 'operator') return $this->operator;
        
        if($property == 'target') return $this->target;
        
        throw new PropertyAccessException($this, $property);
    }

    /**
     * Setter
     *
     * @param string $property
     * @param mixed $value
     *
     * @throws PropertyAccessException
     * @throws RestBadParameterException
     */
    public function __set($property, $value) {
        if($property == 'prefields') {
            $this->addPrefilteringField($value);
            
        } else if($property == 'postfields') {
            $this->addPostfilteringField($value);
            
        } else if($property == 'customtests') {
            $this->addCustomTest($value);
            
        } else {
            throw new PropertyAccessException($this, $property);
        }
    }
    
    /**
     * Cancel prefiltering
     */
    public function cancelPrefiltering() {
        if($this->type == self::LOGIC) {
            if($this->operator == 'not') {
                $this->target->cancelPrefiltering();
                
            } else {
                foreach($this->target as $sub)
                    $sub->cancelPrefiltering();
            }
        }
        
        $this->skip = false;
    }
    
    /**
     * Extract pre-filtering clause
     * 
     * @param boolean $just_test
     * @param boolean $dont_skip No post filtering needed
     * 
     * @return string
     */
    public function getPrefilter($just_test = false, $dont_skip = false) {
        $this->validate(); // Fatal
        
        // Logic operators
        if($this->operator == 'not') {
            $pre = $this->target->getPrefilter($just_test);
            if(!$pre) return null;
            
            $this->skip = !$dont_skip;
            
            return 'NOT ('.$pre.')';
        }
        
        if($this->operator == 'and') {
            $parts = array();
            foreach($this->target as $sub){
                $pre = $sub->getPrefilter($just_test);
                if ($pre) $parts[] = $pre;
            }
            
            if (!count($parts)) return null;
                
            $this->skip = !$dont_skip && !$this->needPostFiltering();
            
            return '('.implode(') AND (', $parts).')';
        }
        
        if($this->operator == 'notOr' || $this->operator == 'nor') {
            $parts = array();
            foreach($this->target as $sub){
                $pre = $sub->getPrefilter($just_test);
                if ($pre) $parts[] = 'NOT ('.$pre.')';
            }
            
            if (!count($parts)) return null;
                
            $this->skip = !$dont_skip && !$this->needPostFiltering();
            
            return '('.implode(') AND (', $parts).')';
        }
        
        if($this->operator == 'or') {
            foreach($this->target as $sub)
                if(!$sub->getPrefilter(true))
                    return null;
                
            $parts = array();
            foreach($this->target as $sub)
                $parts[] = $sub->getPrefilter();
            
            $this->skip = !$dont_skip && !$this->needPostFiltering();
            
            return '('.implode(') OR (', $parts).')';
        }
        
        if($this->operator == 'notAnd' || $this->operator == 'nand') {
            foreach($this->target as $sub)
                if(!$sub->getPrefilter(true))
                    return null;
                
            $parts = array();
            foreach($this->target as $sub)
                $parts[] = 'NOT ('.$sub->getPrefilter().')';
            
            $this->skip = !$dont_skip && !$this->needPostFiltering();
            
            return '('.implode(') OR (', $parts).')';
        }
        
        if($this->operator == 'exclusiveOr' || $this->operator == 'xor') {
            $parts = array();
            foreach($this->target as $sub){
                $pre = $sub->getPrefilter(false, true);
                if (!$sub) $parts[] = $pre;
            }
            
            $this->skip = !$dont_skip && !$this->needPostFiltering();
            
            return '('.implode(') XOR (', $parts).')';
        }
        
        if($this->operator == 'notExclusiveOr' || $this->operator == 'nxor')
            return null; // Not possible to prefilter nxor
        
        // Test operators
        if(!$this->isTargetAllowed('pre'))
            return null;
        
        // Field can be pre-tested, check operator
        if(!in_array($this->operator, array(
            'present', 'contains', 'startWith', 'endWith', 'equals',
            'lessThan', 'moreThan',
            'dateEquals', 'dateBefore', 'dateAfter', 'dateBetween',
            'dateTimeEquals', 'dateTimeBefore', 'dateTimeAfter', 'dateTimeBetween'
        )) && (
            !array_key_exists($this->operator, $this->root->customtests)
            || !$this->root->customtests[$this->operator]['isPreFilter']
        ))
            return null;
        
        // Just testing ?
        if($just_test) return true;
        
        $this->skip = !$dont_skip;
        
        $escaped = str_replace(array('_', '%'), array('\\_', '\\%'), $this->argument);
        
        if($this->operator == 'present') {
            if($this->argument) {
                return $this->target.' IS NOT NULL AND '.$this->target.' <> ""';
                
            } else {
                return $this->target.' IS NULL OR '.$this->target.' <=> ""';
            }
        }
        
        if($this->operator == 'contains') {
            $uid = $this->addPlaceHolder('%'.$escaped.'%');
            return $this->target.' LIKE '.$uid;
        }
        
        if($this->operator == 'startWith') {
            $uid = $this->addPlaceHolder($escaped.'%');
            return $this->target.' LIKE '.$uid;
        }
        
        if($this->operator == 'endWith') {
            $uid = $this->addPlaceHolder('%'.$escaped);
            return $this->target.' LIKE '.$uid;
        }
        
        if($this->operator == 'equals') {
            $a = $this->argument;
            if(is_bool($a))
                $a = $a ? '1' : '0';
            
            $uid = $this->addPlaceHolder($a);
            return $this->target.' = '.$uid;
        }
        
        if($this->operator == 'lessThan') {
            $uid = $this->addPlaceHolder($this->argument);
            return $this->target.' < '.$uid;
        }
        
        if($this->operator == 'moreThan') {
            $uid = $this->addPlaceHolder($this->argument);
            return $this->target.' > '.$uid;
        }
        
        if($this->operator == 'dateEquals') {
            $uid = $this->addPlaceHolder(date('Y-m-d', $this->argument));
            return 'DATE('.$this->target.') = '.$uid;
        }
        
        if($this->operator == 'dateBefore') {
            $uid = $this->addPlaceHolder(date('Y-m-d', $this->argument));
            return 'DATE('.$this->target.') < '.$uid;
        }
        
        if($this->operator == 'dateAfter') {
            $uid = $this->addPlaceHolder(date('Y-m-d', $this->argument));
            return 'DATE('.$this->target.') > '.$uid;
        }
        
        if($this->operator == 'dateBetween') {
            $uid0 = $this->addPlaceHolder(date('Y-m-d', $this->argument[0]));
            $uid1 = $this->addPlaceHolder(date('Y-m-d', $this->argument[1]));
            return 'DATE('.$this->target.') > '.$uid0.' AND DATE('.$this->target.') < '.$uid1;
        }
        
        if($this->operator == 'dateTimeEquals') {
            $uid = $this->addPlaceHolder(date('Y-m-d H:i:s', $this->argument));
            return $this->target.' = '.$uid;
        }
        
        if($this->operator == 'dateTimeBefore') {
            $uid = $this->addPlaceHolder(date('Y-m-d H:i:s', $this->argument));
            return $this->target.' < '.$uid;
        }
        
        if($this->operator == 'dateTimeAfter') {
            $uid = $this->addPlaceHolder(date('Y-m-d H:i:s', $this->argument));
            return $this->target.' > '.$uid;
        }
        
        if($this->operator == 'dateTimeBetween') {
            $uid0 = $this->addPlaceHolder(date('Y-m-d H:i:s', $this->argument[0]));
            $uid1 = $this->addPlaceHolder(date('Y-m-d H:i:s', $this->argument[1]));
            return $this->target.' > '.$uid0.' AND '.$this->target.' < '.$uid1;
        }
        
        if (array_key_exists($this->operator, $this->root->customtests)){
            return $this->root->customtests[$this->operator]['test']($this->argument);
        }
        
        return null;
    }
    
    /**
     * Get prefilter placeholders
     * 
     * @return array
     */
    public function getPlaceHolders() {
        if($this->upper) return $this->upper->getPlaceHolders();
        
        return $this->placeholders;
    }
    
    /**
     * Check if post filtering is needed
     * 
     * @return boolean
     */
    public function needPostFiltering() {
        if($this->type == self::LOGIC) {
            if($this->operator == 'not')
                return $this->target->needPostFiltering();
                
            foreach($this->target as $sub)
                if($sub->needPostFiltering())
                    return true;
            
            return false;
        }
        
        return !$this->skip;
    }

    /**
     * Apply post filtering (verify if object matches)
     *
     * @param Entity $subject
     *
     * @return bool
     *
     * @throws RestBadParameterException
     */
    public function matches(Entity $subject) {
        if($this->skip) return true;
        
        $this->validate(); // Fatal
        
        // Logic operators
        if($this->operator == 'not')
            return !$this->target->matches($subject);
        
        if($this->operator == 'and') {
            foreach($this->target as $sub)
                if(!$sub->matches($subject))
                    return false;
            
            return true;
        }
        
        if($this->operator == 'notAnd' || $this->operator == 'nand') {
            foreach($this->target as $sub)
                if(!$sub->matches($subject))
                    return true;
            
            return false;
        }
        
        if($this->operator == 'or') {
            foreach($this->target as $sub)
                if($sub->matches($subject))
                    return true;
            
            return false;
        }
        
        if($this->operator == 'notOr' || $this->operator == 'nor') {
            foreach($this->target as $sub)
                if($sub->matches($subject))
                    return false;
            
            return true;
        }
        
        if($this->operator == 'exclusiveOr' || $this->operator == 'xor') {
            $m = 0;
            foreach($this->target as $sub) {
                if($sub->matches($subject)) $m++;
                
                if($m > 1) return false;
            }
            
            return ($m == 1);
        }
        
        if($this->operator == 'notExclusiveOr' || $this->operator == 'nxor') {
            $m = 0;
            foreach($this->target as $sub)
                if($sub->matches($subject)) $m++;
            
            return ($m != 1);
        }
        
        // Test operators
        if($this->type == self::CUSTOM && !$this->root->customtests[$this->operator]['isPreFilter'])
            return (bool)$this->root->customtests[$this->operator]['test']($this->argument, $subject);
        
        $field = $this->target;
        
        if (!array_key_exists($field, $this->root->postfields))
            throw new RestBadParameterException($this->path.' refers to unavailable property');
        
        $getter = $this->root->postfields[$field];
        $values = $getter ? $getter($subject) : $subject->$field;

        foreach ((array) $values as $value){

            if($this->operator == 'present')
                if ((bool)$subject->$field == (bool)$this->argument)
                    return true;

            if($this->operator == 'contains')
                if (mb_strpos((string)$value, (string)$this->argument) !== false)
                    return true;

            if($this->operator == 'startWith')
                if (mb_strpos((string)$value, (string)$this->argument) === 0)
                    return true;

            if($this->operator == 'endWith')
                if (mb_substr((string)$value, -1 * mb_strlen((string)$this->argument)) === (string)$this->argument)
                    return true;

            if($this->operator == 'equals') {
                if(is_bool($this->argument)) {
                    if ($value === $this->argument) return true;
                } else {
                    if ((string)$value === (string)$this->argument) return true;
                }
            }
            
            if($this->operator == 'in') {
                if (in_array($value, $this->argument, true)) return true;
            }

            if($this->operator == 'lessThan')
                if ((float)$value < (float)$this->argument)
                    return true;

            if($this->operator == 'moreThan')
                if ((float)$value > (float)$this->argument)
                    return true;

            if($this->operator == 'matches')
                if (preg_match('`'.str_replace('`', '\\`', $this->argument).'`i', $value))
                    return true;

            if($this->operator == 'caseMatches')
                if (preg_match('`'.str_replace('`', '\\`', $this->argument).'`', $value))
                    return true;

            if($this->operator == 'dateEquals')
                if (floor($value / 86400) < floor($this->argument / 86400))
                    return true;

            if($this->operator == 'dateBefore')
                if (floor($value / 86400) < floor($this->argument / 86400))
                    return true;

            if($this->operator == 'dateAfter')
                if (floor($value / 86400) > floor($this->argument / 86400))
                    return true;

            if($this->operator == 'dateBetween')
                if (floor($value / 86400) > floor($this->argument[0] / 86400) && floor($value / 86400) < floor($this->argument[1] / 86400))
                    return true;

            if($this->operator == 'dateTimeEquals')
                if ($value == $this->argument)
                    return true;

            if($this->operator == 'dateTimeBefore')
                if ($value < $this->argument)
                    return true;

            if($this->operator == 'dateTimeAfter')
                if ($value > $this->argument)
                    return true;

            if($this->operator == 'dateTimeBetween')
                if ($value > $this->argument[0] && $value < $this->argument[1])
                    return true;
        }
        return false;
    }
}
