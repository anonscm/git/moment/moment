<?php

/**
 *     Moment - RestFileDownload.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST file download
 */
class RestFileDownload {
    /**
     * @var string name
     */
    protected $name = 'file';
    
    /**
     * @var mixed data
     */
    protected $data = null;
    
    /**
     * Constructor
     * 
     * @param mixed $data raw data or RestResponseX
     * @param string $name downloaded file name
     */
    public function __construct($data, $name = null) {
        if(!$name) {
            $name = RestRequest::getEndpoint();
            
            $path = RestRequest::getPath();
            if(count($path))
                $name .= '_'.implode('_', $path);
            
            $class = (is_object($data) && ($data instanceof RestResponseRaw)) ? get_class($data) : null;
            if($class && defined($class.'::FILE_EXTENSION')) {
                $format = $class::FILE_EXTENSION;
                
            } else {
                $format = RestRequest::getOutputProperty('format');
            }
            
            if(!$format) $format = 'json';
            $name .= '.'.$format;
        }
        
        $this->name = $name;
        
        $this->data = $data;
    }
    
    /**
     * Output data
     */
    public function output() {
        // At this point data should be a renderer
        if(!is_object($this->data) || !($this->data instanceof RestResponseRaw))
            RestResponsePlain::renderException(new RestOutputConversionException('download data'));
        
        // UTF8 filename handling
        $ua = array_key_exists('HTTP_USER_AGENT', $_SERVER) ? $_SERVER['HTTP_USER_AGENT'] : '';
        if(preg_match('`msie (7|8)`i', $ua) && !preg_match('`opera`i', $ua)) {
            // IE7, IE8 but not opera that MAY match
            header('Content-Disposition: attachment; filename='.rawurlencode($this->name));
            
        } else if(preg_match('`android`i', $ua)) {
            // Android OS
            $name = preg_replace('`[^a-z0-9\._\-\+,@£\$€!½§~\'=\(\)\[\]\{\}]`i', '_', $this->name);
            header('Content-Disposition: attachment; filename="'.$name.'"');
            
        } else {
            // All others, see RFC 5987
            header('Content-Disposition: attachment; filename="'.$this->name.'"; filename*=UTF-8\'\''.rawurlencode($this->name));
        }
        
        header('Content-Transfer-Encoding: binary');
        
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Pragma: public');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        
        $this->data->output();
    }
    
    /**
     * Getter
     * 
     * @param string $property property to get
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if($property == 'name') return $this->name;
        
        if($property == 'data') return $this->data;
        
        throw new PropertyAccessException($this, $property);
    }}
