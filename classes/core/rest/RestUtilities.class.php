<?php

/**
 *     Moment - RestUtilities.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST server
 */
class RestUtilities {
    
    /**
     * Flatten arguments (recursive)
     *
     * @param $a array multi-dimensionnal array
     * @param $p string parent key stack
     *
     * @return array single dimension array
     */
    public static function flatten($a, $p = null) {
        $o = array();
        ksort($a);
        foreach($a as $k => $v) {
            if(is_array($v)) {
                foreach(self::flatten($v, $p ? $p.'['.$k.']' : $k) as $s) $o[] = $s;
            }else $o[] = ($p ? $p.'['.$k.']' : $k).'='.$v;
        }
        return $o;
    }
    
    /**
     * Convert raw data to xml for output
     *
     * @param mixed $data
     * @param DOMElement $node
     *
     * @return SimpleXMLElement
     */
    public static function toXML($data, DOMElement $node = null) {
        $root = !(bool)$node;
        if(!$node) {
            $doc = new DOMDocument('1.0', 'utf-8');
            if(Config::get('debug')) $doc->formatOutput = true;
            
            $node = $doc->appendChild(new DOMElement('data'));
        }
        
        if(is_scalar($data) || is_null($data)) {
            if(is_bool($data)) {
                $node->setAttribute('type', 'boolean');
                $node->nodeValue = $data ? '1' : '0';
                
            } else if(is_int($data)) {
                $node->setAttribute('type', 'integer');
                $node->nodeValue = (int)$data;
                
            } else if(is_float($data)) {
                $node->setAttribute('type', 'float');
                $node->nodeValue = (float)$data;
                
            } else if(is_null($data)) {
                $node->setAttribute('type', 'null');
                $node->nodeValue = '';
                
            } else if(preg_match('`[<>]`', $data)) {
                $node->setAttribute('type', 'string');
                $node->appendChild($node->ownerDocument->createCDATASection($data));
                
            } else {
                $node->setAttribute('type', 'string');
                $node->appendChild($node->ownerDocument->createTextNode($data));
            }
            
        } else {
            if(!is_array($data)) $data = (array)$data;
            
            if(count(array_filter(array_keys($data), 'is_int'))) {
                // Numerical index array
                $node->setAttribute('type', 'array');
                
                foreach($data as $idx => $subdata) {
                    $item = $node->appendChild(new DOMElement('item'));
                    self::toXML($subdata, $item);
                    $item->setAttribute('index', $idx);
                }
                
            } else {
                // Alpha keys => object like
                $node->setAttribute('type', 'object');
                
                foreach($data as $key => $value) {
                    $property = $node->appendChild(new DOMElement($key));
                    self::toXML($value, $property);
                }
            }
        }
        
        if(!$root) return $node;
        
        $node->normalize();
        
        return $node->ownerDocument->saveXML();
    }
    
    /**
     * Convert XML into raw data
     *
     * @param mixed $source string or DOMElement
     *
     * @return mixed
     *
     * @todo validation
     */
    public static function parseXML($source) {
        if(is_string($source)) {
            $doc = new DOMDocument();
            $doc->loadXML($source);
            $doc->normalize();
            
            $source = $doc->childNodes[0];
        }
        
        $type = $source->getAttribute('type');
        if($type) {
            if($type == 'object') {
                $data = array();
                foreach($source->childNodes as $node) {
                    if($node->nodeType == XML_TEXT_NODE) continue;
                    
                    $data[$node->nodeName] = self::parseXML($node);
                }
                
                return (object)$data;
            }
            
            if($type == 'array') {
                $items = array();
                foreach($source->childNodes as $node) {
                    if($node->nodeType == XML_TEXT_NODE) continue;
                    
                    if($node->hasAttribute('index') && is_int($node->getAttribute('index'))) {
                        $items[(int)$node->getAttribute('index')] = self::parseXML($node);
                        
                    } else {
                        $items[] = self::parseXML($node);
                    }
                }
                
                return $items;
            }
            
            if($type == 'boolean')
                return (bool)$source->nodeValue;
            
            if($type == 'integer')
                return (int)$source->nodeValue;
            
            if($type == 'float')
                return (float)$source->nodeValue;
            
            
            if($type == 'string') {
                $txt = $source->textContent;
                
                // CDATA ?
                if($source->hasChildNodes()) {
                    $node = $source->childNodes[0];
                    
                    if($node->nodeType == XML_CDATA_SECTION_NODE)
                        $txt = $node->textContent;
                }
                
                return $txt;
            }
        }
        
        // Untyped data, try to guess ...
        
        $scalar = true;
        foreach($source->childNodes as $node)
            $scalar &= ($node->nodeType == XML_TEXT_NODE);
        
        if($scalar) {
            $value = $source->textContent;
            
            if(preg_match('`^-?[1-9][0-9]*$`', $value)) return (int)$value;
            
            if(preg_match('`^-?[0-9]+\.[0-9]+$`', $value)) return (float)$value;
            
            return (string)$value;
        }
        
        // Not scalar
        $entries = array();
        foreach($source->childNodes as $node) {
            if($node->nodeType == XML_TEXT_NODE) continue;
            
            if(array_key_exists($node->nodeName, $entries)) {
                if(!is_array($entries[$node->nodeName]))
                    $entries[$node->nodeName] = (array)$entries[$node->nodeName];
            }
            
            $value = self::parseXML($node);
            
            if(array_key_exists($node->nodeName, $entries) && is_array($entries[$node->nodeName])) {
                $entries[$node->nodeName][] = $value;
                
            } else {
                $entries[$node->nodeName] = $value;
            }
        }
        
        return $entries;
    }
    
    /**
     * Send HTTP response code (supports)
     *
     * @param $code int http code
     *
     * @return int
     */
    public static function sendResponseCode($code = 200) {
        if(function_exists('http_response_code'))
            return call_user_func('http_response_code', $code);
        
        $messages = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Moved Temporarily',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Time-out',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Large',
            415 => 'Unsupported Media Type',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Time-out',
            505 => 'HTTP Version not supported'
        );
        
        $protocol = array_key_exists('SERVER_PROTOCOL', $_SERVER) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0';
        header($protocol.' '.$code.' '.$messages[$code]);
        
        return $code;
    }
    
    /**
     * Format a date to multiple types
     *
     * @param int $date the date
     * @param bool $with_time
     *
     * @return array
     */
    public static function formatDate($date, $with_time = false) {
        if(is_null($date)) return null;
        
        return array(
            'raw' => $date,
            'formatted' => is_int($date) ? Utilities::formatDate($date, $with_time) : null
        );
    }
    
    /**
     * Sort collection according to (remaining) instructions
     *
     * @param array $entities
     * @param array $order sort instructions
     * @param array $allowed_fields
     *
     * @return array
     *
     * @throws ModelViolationException
     * @throws RestBadParameterException
     */
    public static function sortEntities($entities, $order = array(), $allowed_fields = array()) {
        if(!is_array($order) || !count($order)) return $entities;
        
        if(count(array_filter(array_map(function($o) {
            return !($o instanceof Entity);
        }, $entities))))
            throw new ModelViolationException('Trying to sort non-entities');
        
        foreach($order as $f => $o) {
            if($f == '*' && !method_exists($entities[0], 'compareWith'))
                throw new RestBadParameterException('sortOrder used on non-naturally sortable entities');
            
            if(!in_array($f, $allowed_fields))
                throw new RestBadParameterException('order['.$f.'] does not refer to a sortable field');
        }
        
        usort($entities, function($a, $b) use($order) {
            foreach($order as $f => $o) {
                if($f == '*') return $a->compareWith($b);
                
                $va = $a->$f;
                $vb = $b->$f;
                
                if(is_int($va) || is_float($va)) {
                    $vo = $vb - $va;
                    if($vo < 0) $vo = -1;
                    if($vo > 0) $vo = 1;
                    
                } else {
                    $vo = strcmp($va, $vb);
                }
                
                if($vo == 0) continue;
                
                if($o == 'descending') return -1 * $vo;
                
                return $vo;
            }
            
            return 0;
        });
        
        return $entities;
    }
    
    /**
     * Get entity subset
     *
     * @param mixed $getter static callable (must take sql clause and array of placeholders), class name (must be descendant of Entity) or array of already fetched objects
     * @param array $allowed_fields list of fields allowed for filtering / sorting, may be numerically indexed or field name indexed to refer to a custom value getter (callable)
     * @param array $selectors if null RestRequest::getOutputProperties will be used
     *
     * @return array
     *
     * @throws ModelViolationException
     * @throws RestBadParameterException
     */
    public static function getEntities($getter, $allowed_fields = array(), $selectors = null) {
        $post = false;
        $pre = false;
        
        if(!$selectors)
            $selectors = RestRequest::getOutputProperties();
        
        if($selectors['filter']) {
            $selectors['filter']->addPostfilteringField($allowed_fields);
            $post = true;
            $pre = false;
        }
        
        $updated_since = null;
        if(is_int($selectors['updatedSince']) && ($selectors['updatedSince'] > 0))
            $updated_since = (int)$selectors['updatedSince'];
        
        $count = null;
        if(is_int($selectors['count']) && ($selectors['count'] > 0))
            $count = (int)$selectors['count'];
        
        $start_index = null;
        if(is_int($selectors['startIndex']) && ($selectors['startIndex'] > 0))
            $start_index = (int)$selectors['startIndex'];
        
        if(is_array($getter)) {
            $entities = $getter;
            
        } else {
            if (is_object($getter) && $getter instanceof EntityGetter){
                $class = $getter->className;
                $getter = $getter->callable;
            }else if (is_string($getter)){
                if(strpos($getter, '::')) {
                    $method = explode("::", $getter);
                    $class= $method[0];
                    
                } else {
                    $class = $getter;
                    $getter .= '::all';
                }
            }
            
            $entities = null;
            
            if(!is_subclass_of($class, Entity::name()))
                throw new ModelViolationException('Trying to filter non-entities');
            
            if($selectors['filter']) {
                $fields = array_keys(call_user_func($class.'::getDataMap'));
                $prefields = array();
                foreach($allowed_fields as $k => $v) {
                    if(is_int($k)) $k = $v;
                    if(in_array($k, $fields))
                        $prefields[] = $k;
                }
                
                if(count($prefields))
                    $selectors['filter']->addPrefilteringField($prefields);
                
                $pre = $selectors['filter']->getPrefilter();
                $post = $selectors['filter']->needPostFiltering();
                $ph = $selectors['filter']->getPlaceHolders();
                
                if($pre) {
                    if(!is_null($updated_since)) {
                        if(method_exists($class, 'getUpdatedSincePrefilter')) {
                            $uid = ':updated_since___'.uniqid();
                            $ph[$uid] = date('Y-m-d H:i:s', $updated_since);
                            $pre = '('.$pre.') AND ('.call_user_func($class.'::getUpdatedSincePrefilter', $uid).')';
                            $updated_since = null; // Applied
                        }
                    }
                    
                    if(!$post) {
                        // Apply limit since no post filtering remains
                        
                        if(!is_null($count)) {
                            if(!is_null($start_index)) {
                                $pre .= ' LIMIT '.$count.' OFFSET '.$start_index;
                                
                            } else {
                                $pre .= ' LIMIT '.$count;
                            }
                            
                        } else if(!is_null($start_index)) {
                            // See MySQL LIMIT about this wierdness
                            $pre .= ' LIMIT 18446744073709551614 OFFSET '.$start_index;
                        }
                    }
                    
                    $entities = array_values(call_user_func($getter,$pre, $ph));
                }
            }
            
            if(is_null($entities))
                $entities = array_values(call_user_func($getter));
        }
        
        if(!$entities || !count($entities))
            return array();
        
        if($post) $entities = array_filter($entities, function($entity) use($selectors) {
            try {
                return $selectors['filter']->matches($entity);
            }catch (Exception $e){
                return false;
            }
        });
        
        if(!is_null($updated_since) && method_exists($entities[0], 'wasUpdatedSince')) {
            $entities = array_filter($entities, function($entity) use($updated_since) {
                return $entity->wasUpdatedSince($updated_since);
            });
        }
        
        if(!$pre || $post) {
            if (!is_null($count)) {
                if (!is_null($start_index)) {
                    $entities = array_slice($entities, $start_index, $count);
                    
                } else {
                    $entities = array_slice($entities, 0, $count);
                }
                
            } else if (!is_null($start_index)) {
                $entities = array_slice($entities, $start_index);
            }
        }
        
        if($selectors['order']) {
            $fields = array();
            foreach($allowed_fields as $k => $v) {
                if(is_int($k)) $k = $v;
                $fields[] = $k;
            }
            $entities = self::sortEntities($entities, $selectors['order'], $fields);
        }
        
        return $entities;
    }
}