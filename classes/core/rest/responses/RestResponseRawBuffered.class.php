<?php

/**
 *     Moment - RestResponseRawBuffered.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST buffered raw response
 */
class RestResponseRawBuffered extends RestResponseRaw {
    /**
     * @const string mime type (no inheritance)
     */
    const MIME_TYPE = 'application/octet-stream';
    
    /**
     * @const mixed exception support (bool or fallback renderer name) (no inheritance)
     */
    const EXCEPTION_RENDERER = false;
    
    /**
     * @var callable Data chunk getter
     */
    protected $getter = null;
    
    /**
     * @var integer Estimated output size
     */
    protected $size = null;

    /**
     * Constructor
     *
     * @param callable $getter data chunk getter
     * @param integer $size estimated output size
     *
     * @throws RestOutputConversionException
     */
    public function __construct($getter, $size = null) {
        if(!$getter || !is_callable($getter))
            throw new RestOutputConversionException('buffered getter');
        
        $this->getter = $getter;
        
        if(!is_null($size) && (!is_int($size) || ($size < 0)))
            throw new RestOutputConversionException('buffered size');
        
        $this->size = $size;
    }
    
    /**
     * Output data
     */
    public function output() {
        header('Content-Type: '.static::MIME_TYPE);
        
        if(!is_null($this->size))
            header('Content-Length: '.$this->size);

        $getter = $this->getter;
        while(strlen($data = $getter())) {
            echo $data;
            flush();
        }
        
        exit;
    }
    
    /**
     * Getter
     * 
     * @param string $property property to get
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if($property == 'type') return static::MIME_TYPE;
        
        if($property == 'size') return $this->size;
        
        if($property == 'exception_renderer') return static::EXCEPTION_RENDERER;
        
        throw new PropertyAccessException($this, $property);
    }
}
