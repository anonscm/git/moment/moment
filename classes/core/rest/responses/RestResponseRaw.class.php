<?php

/**
 *     Moment - RestResponseRaw.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST raw response
 */
class RestResponseRaw {
    /**
     * @const string mime type
     */
    const MIME_TYPE = 'application/octet-stream';
    
    /**
     * @const mixed exception support (bool or fallback renderer name)
     */
    const EXCEPTION_RENDERER = false;
    
    /**
     * @var string data
     */
    protected $data = null;
    
    /**
     * Constructor
     * 
     * @param string $data
     */
    public function __construct($data) {
        $this->data = $data;
    }
    
    /**
     * Output data
     */
    public function output() {
        header('Content-Type: '.static::MIME_TYPE);
        header('Content-Length: '.strlen($this->data));
        
        echo $this->data;
        
        exit;
    }
    
    /**
     * Getter
     * 
     * @param string $property property to get
     * 
     * @throws PropertyAccessException
     * 
     * @return mixed
     */
    public function __get($property) {
        if($property == 'type') return static::MIME_TYPE;
        
        if($property == 'data') return $this->data;
        
        if($property == 'exception_renderer') return static::EXCEPTION_RENDERER;
        
        throw new PropertyAccessException($this, $property);
    }
}
