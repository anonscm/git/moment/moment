<?php

/**
 *     Moment - RestResponseJson.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST json response
 */
class RestResponseJson extends RestStructuredResponse {
    /**
     * @const string mime type
     */
    const MIME_TYPE = 'application/json';
    
    /**
     * @const mixed exception support (bool or fallback renderer name)
     */
    const EXCEPTION_RENDERER = true;

    /**
     * Constructor
     *
     * @param mixed $data
     *
     * @throws RestOutputConversionException
     */
    public function __construct($data) {
        if(is_object($data) && ($data instanceof Exception)) {
            $data = static::castException($data);
            
        } else {
            $data = self::clean($data);
        }
        
        $json = JSON::encode($data);
        if($data && !$json)
            throw new RestOutputConversionException('json : encoding failed');
        
        parent::__construct($json);
    }
    
    /**
     * Exception rendering shorthand
     * 
     * @param Exception $e
     */
    public static function renderException($e) {
        (new static($e))->output();
    }
}
