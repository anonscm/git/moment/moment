<?php

/**
 *     Moment - RestResponsePdf.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST pdf response
 */
class RestResponsePdf extends RestResponseRaw {
    /**
     * @const string mime type
     */
    const MIME_TYPE = 'application/pdf';
    
    /**
     * @const mixed exception support (bool or fallback renderer name)
     */
    const EXCEPTION_RENDERER = true;
    
    /**
     * Constructor
     * 
     * @param mixed $data
     */
    public function __construct($data) {
        $dump = false;
        $paper = array('a4', 'portrait');
        
        if(is_object($data) && ($data instanceof Exception)) {
            $data = Template::process('exception', array('exception' => $data));
            
        } else if(is_array($data)) {
            $dump = true;
            
            if(RestRequest::getOutputProperty('format_options') == 'json') {
                $data = '<pre>'.str_replace('\/', '/', json_encode($data, JSON_PRETTY_PRINT, 2048)).'</pre>';
                
            } else {
                $paper = array('a4', 'landscape');
                
                $data = RestStructuredResponse::clean($data);
                
                $lines = RestResponseCsv::getTable($data);
                $data = array('<table>');
                foreach($lines as $line) {
                    $data[] = '<tr>'.implode('', array_map(function($v) {
                        return '<td>'.htmlentities($v).'</td>';
                    }, $line)).'</tr>';
                }
                
                $data[] = '</table>';
                
                $data = implode('', $data);
            }
            
        } else if(!is_scalar($data) && !is_null($data)) {
            $data = Template::process('exception', array(
                'exception' => new RestOutputConversionException('pdf : bad data')
            ));
        }
        
        require_once(EKKO_ROOT.'/lib/core/dompdf/dompdf_config.inc.php');
        
        $styles = array('view/css/core/pdf.css');
        if($dump)
            $styles[] = 'view/css/core/pdf_dump.css';
        
        $styles[] = 'view/css/pdf.css';
        $styles[] = 'view/skin/pdf.css';
        
        $css = '';
        foreach($styles as $cssfile)
            if(file_exists(EKKO_ROOT.'/'.$cssfile))
                $css .= "\n\n".file_get_contents(EKKO_ROOT.'/'.$cssfile);
        
        $css = trim($css);
        
        if($css)
            $data = '<style type="text/css">'.$css.'</style>'.$data;
        
        $pdf = new DOMPDF();
        
        call_user_func_array(array($pdf, 'set_paper'), $paper);
        
        $pdf->load_html($data);
        $pdf->render();
        
        parent::__construct($pdf->output());
    }
}
