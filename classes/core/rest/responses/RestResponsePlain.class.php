<?php

/**
 *     Moment - RestResponsePlain.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * REST plain response
 */
class RestResponsePlain extends RestResponseRaw {
    /**
     * @const string mime type
     */
    const MIME_TYPE = 'text/plain';
    
    /**
     * @const string real file ext
     */
    const FILE_EXTENSION = 'txt';
    
    /**
     * @const mixed exception support (bool or fallback renderer name)
     */
    const EXCEPTION_RENDERER = true;

    /**
     * Constructor
     *
     * @param mixed $data
     *
     * @throws RestOutputConversionException
     */
    public function __construct($data) {
        if(is_object($data) && ($data instanceof Exception)) {
            $msg = $data->getMessage();
            
            if(method_exists($data, 'getUid'))
                $msg .= ' (uid: '.$data->getUid().')';
            
            if(method_exists($data, 'getDetails'))
                $msg .= ', details: '."\n".print_r($data->getDetails(), true);
            
            $data = $msg;
        }
        
        if(!is_scalar($data) && !is_null($data))
            throw new RestOutputConversionException('plain : non-scalar data');
        
        parent::__construct((string)$data);
    }
    
    /**
     * Exception rendering shorthand
     * 
     * @param Exception $e
     */
    public static function renderException($e) {
        (new static($e))->output();
    }
}

