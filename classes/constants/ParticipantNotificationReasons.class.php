<?php

/**
 *     Moment - ParticipantNotificationReasons.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class containing participant reasons
 */
class ParticipantNotificationReasons extends Enum {
    const ANSWER_CREATED = 'answer_created';
    const ANSWER_UPDATED = 'answer_created';
    const REMINDER = 'reminder';
    const TOKEN_RECOVERY = 'token_recovery';
}
