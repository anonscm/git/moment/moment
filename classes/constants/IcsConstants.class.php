<?php

/**
 *     Moment - IcsConstants.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class containing ICS constants
 */
class IcsConstants extends Enum{
    const DATE_FORMAT       = "Ymd\THis";
    const DATE_ONLY_FORMAT  = "Ymd";
    const DATE_FORMAT_UTC   = "Ymd\THis\Z";
    const STATUS_CANCELLED  = "CANCELLED";
    const STATUS_CONFIRMED  = "CONFIRMED";
    const STATUS_TENTATIVE  = "TENTATIVE";
    const METHOD_REQUEST    = "REQUEST";
    const METHOD_PUBLISH    = "PUBLISH";
    const METHOD_CANCEL     = "CANCEL";
    const TIME_ZONE         = "Europe/Paris";
    const VERSION           = "2.0";
    const PARTSTAT_ACCEPTED  = "ACCEPTED";
    const PARTSTAT_NEEDS_ACTION  = "NEEDS-ACTION";
    const PARTSTAT_TENTATIVE  = "TENTATIVE";
    const PARTSTAT_DECLINED  = "DECLINED";

}