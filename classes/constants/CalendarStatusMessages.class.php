<?php

/**
 *     Moment - CalendarStatusMessages.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class containing question types
 */
class CalendarStatusMessages extends Enum {
    const UNKNOWN = 'unknown';
    const OK = 'calendar_ok';
    const DOWN = 'calendar_unavailable';
    const NOT_FOUND = 'calendar_not_found';
    const FORBIDDEN = 'access_to_calendar_forbidden';
    const TIMEOUT = 'access_to_calendar_timed_out';
    const ERROR = 'access_to_calendar_failed';
    const INCOMPATIBLE = 'calendar_is_not_compatible';
    const OVERSIZED = 'calendar_is_oversized';
}
