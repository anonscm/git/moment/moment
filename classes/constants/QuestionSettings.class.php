<?php

/**
 *     Moment - QuestionSettings.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class QuestionSettings
 *
 * Setting for each questions
 */
class QuestionSettings extends Settings {

    const FORCE_UNIQUE_CHOICE = 'force_unique_choice';
    const ENABLE_MAYBE_CHOICES = 'enable_maybe_choices';

    /**
     * Mandatory options for a question
     * @var type
     */
    protected static $BASE_SETTINGS = array(
        self::FORCE_UNIQUE_CHOICE => array(
            'type' => 'boolean',
            'possible_values' => array(0, 1),
            'default' => 0
        ),
        self::ENABLE_MAYBE_CHOICES => array(
            'type' => 'boolean',
            'possible_values' => array(0, 1),
            'default' => 0
        )
    );


}
