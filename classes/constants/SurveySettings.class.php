<?php

/**
 *     Moment - SurveySettings.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class SurveySettings
 *
 * Settings for each surveys
 */
class SurveySettings extends Settings {


    /**
     * Existing settings for a survey
     * @var type
     */
    protected static $BASE_SETTINGS = array(
        'auto_close' => array(
            'type' => 'date',
            'default' => NULL
        ),
        'enable_anonymous_answer' => array(
            'type' => 'boolean',
            'default' => 0
        ),
        'disable_answer_edition' => array(
            'type' => 'boolean',
            'default' => 0
        ),
        'enable_update_notification' => array(
            'type' => 'boolean',
            'default' => 0
        ),
        'limit_participants' => array(
            'type' => 'boolean',
            'default' => 0
        ),
        'limit_participants_nb' => array(
            'type' => 'number',
            'default' => 10,
            'properties' => array(
                'min' => 1
            )
        ),
        'reply_access' => array(
            'type' => 'select',
            'default' => Survey::OPENED_TO_EVERYONE,
            'values' => array(
                Survey::OPENED_TO_EVERYONE,
                Survey::OPENED_TO_AUTHENTICATED,
                Survey::OPENED_TO_GUESTS
            )
        ),
        'hide_answers' => array(
            'type' => 'boolean',
            'default' => 0
        ),
        'hide_comments' => array(
            'type' => 'boolean',
            'default' => 0
        ),
        'show_participant_name' => array(
            'type' => 'boolean',
            'default' => 1
        ),
        'show_participant_email' => array(
            'type' => 'boolean',
            'default' => 0
        ),
        'dont_notify_on_reply' => array(
            'type' => 'boolean',
            'default' => 0
        ),
        'time_zone' => array(
            'type' => 'select',
            'no_translation' => true,
            'default' => 'UTC',
            'values' => array()
        ),
        'dont_receive_invitation_copy' => array(
            'type' => 'boolean',
            'default' => 0
        )
    );

    private static $initialized = false;

    /**
     * Get all settings (inits non scalar values)
     *
     * @param Survey $survey
     * @return array
     */
    public static function getSettings(Survey $survey = null) {
        if(!self::$initialized) {
            //Computing default auto_close date (Current + 1 month)
            self::$BASE_SETTINGS['auto_close']['default'] = strtotime("+1 month", time());
            self::$BASE_SETTINGS['auto_close']['properties']['min'] = strtotime("+1 day", time());
            self::$BASE_SETTINGS['time_zone']['values'] = DateTimeZone::listIdentifiers(DateTimeZone::ALL_WITH_BC);

            self::$initialized = true;
        }
        $maxOpenedDuration = Config::get('max_opened_duration') ? Config::get('max_opened_duration') : 6;
        //Max auto_close date is creation date + <max_opened_duration> months
        if (isset($survey)) {
            self::$BASE_SETTINGS['auto_close']['properties']['max'] = strtotime('+' . $maxOpenedDuration . ' month', $survey->created);

            //If reopened and not closed max auto_close is max(creation date + max_opened_duration months, reopened_date + 1 month)
            if($survey->reopened != null && $survey->closed == null) {
                $reopenedPlusOneMonth = strtotime('+1 month', $survey->reopened);
                self::$BASE_SETTINGS['auto_close']['properties']['max'] = max(self::$BASE_SETTINGS['auto_close']['properties']['max'], $reopenedPlusOneMonth);
            } else if($survey->closed != null) {
                //If closed max auto_close is max(creation date + max_opened_duration months, current + 1 month)
                $currentPlusOneMonth = strtotime('+1 month', time());
                self::$BASE_SETTINGS['auto_close']['properties']['max'] = max(self::$BASE_SETTINGS['auto_close']['properties']['max'], $currentPlusOneMonth);
            }

        } else if (!isset(self::$BASE_SETTINGS['auto_close']['properties']['max'])) { //No reinit of default value
            self::$BASE_SETTINGS['auto_close']['properties']['max'] = strtotime('+' . $maxOpenedDuration . ' month', time());
        }

        return parent::getSettings();
    }

    /**
     * Returns all known survey settings for this (optionnal) survey
     *
     * @param Survey $survey
     * @return array
     */
    public static function getSurveySettings(Survey $survey = null) {
        self::getSettings($survey);

        return self::$BASE_SETTINGS;
    }

    /**
     * Validates every settings with the matching field
     *
     * @param $settings
     * @param Survey $survey
     * @return mixed|void
     */
    public static function validateSettings($settings, Survey $survey = null) {
        //First we have to get/init specific setting for this survey
        self::getSettings($survey);

        //If we are validating a draft and no auto_close date is available (min > max) we ignore min validation
        if(isset($survey) && $survey->is_draft && self::$BASE_SETTINGS['auto_close']['properties']['min'] >= self::$BASE_SETTINGS['auto_close']['properties']['max']) {
            self::$BASE_SETTINGS['auto_close']['properties']['ignore_min'] = true;
            $settings->auto_close = (int) $settings->auto_close;
        }

        return parent::validateSettings($settings);
    }
}
