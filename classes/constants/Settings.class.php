<?php

/**
 *     Moment - Settings.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
abstract class Settings {

    protected static $BASE_SETTINGS;

    /**
     * Returns all known settings
     *
     * @return array
     */
    public static function getSettings() {
        return static::$BASE_SETTINGS;
    }

    /**
     * Return default value for setting
     * @param type $setting_name
     * @return type
     * @throws SurveyMissingSettingPropertyException
     */
    public static function getDefaultSetting($setting_name) {
        if (!array_key_exists($setting_name, static::$BASE_SETTINGS))
            throw new SurveyMissingSettingPropertyException($setting_name);

        if (!array_key_exists('default', static::$BASE_SETTINGS[$setting_name]))
            throw new SurveyMissingSettingPropertyException('default');

        return static::$BASE_SETTINGS[$setting_name]['default'];
    }

    /**
     * Adds a setting. This setting will then be stored in settings
     *
     * @param string $name
     * @param array $setting
     * @throws SurveyBaseSettingOverrideNotAllowedException
     * @throws SurveyMissingSettingPropertyException
     */
    public static function addSetting($name, array $setting) {

        if (array_key_exists($name, static::$BASE_SETTINGS)) {
            //Todo create settingexceptions
            throw new SurveyBaseSettingOverrideNotAllowedException($name);
        }

        //Mandatory setting fields
        $mandatoryFields = array('type', 'default');

        foreach ($mandatoryFields as $field) {
            if (!isset($setting[$field])) {
                throw new SurveyMissingSettingPropertyException($field);
            }
        }

        static::$BASE_SETTINGS[$name] = $setting;
    }

    /**
     * Validates every settings with the matching field
     *
     * @param $settings
     */
    public static function validateSettings($settings) {
        if(is_array($settings))
            $settings = (object) $settings;

        foreach (static::getSettings() as $name => $setting) {
            if (isset($settings->$name)) {
                $field = self::getField($name, isset($setting['properties']) ? $setting['properties'] : array(), '');
                $field->validateData($settings->$name);
            }
        }
    }

    /**
     * Get html input for setting
     * @param string $name setting name
     * @param array $properties
     * @param mixed $current_value current value
     * @return string
     */
    public static function getInput($name, $properties = array(), $current_value = null) {
        if (array_key_exists($name, static::$BASE_SETTINGS)) {

            $field_html_start = '';
            $field_html_end = '';

            //Field may need to be encapsulated to be hidden
            if (isset($properties['hide']) && $properties['hide']) {
                $field_html_start = '<span class="hidden">';
                $field_html_end = '</span>';
                unset($properties['hide']);
            }

            //Field may need a tip (Uses foundation tooltips)
            if(isset($properties['tip'])) {

                $tip =  $properties['tip'];
                unset($properties['tip']);
                $field = static::getField($name, $properties, $current_value);
                $container = $field->getHTMLContainer();
                return $container['header'].$container['label']
                    . ' <i class="fa fa-info-circle top" data-tooltip aria-haspopup="true" data-disable-hover="false" data-v-offset="15" tabindex="1" title="'.$tip.'"></i>'
                    . $field->getHTMLInput()
                    . $container['footer'];
            } else {
                $input = static::getField($name, $properties, $current_value)->getHTML();
            }

            return $field_html_start . $input . $field_html_end;
        }
    }

    public static function getField($name, $properties = array(), $current_value = null) {
        if(!array_key_exists($name, static::$BASE_SETTINGS))
            return null;

        if (!isset($properties['label'])) {
            $properties['label'] = (isset(static::$BASE_SETTINGS[$name]['label'])) ? static::$BASE_SETTINGS[$name]['label'] : $name;
        }

        //Merging default properties and custom
        if (isset(static::$BASE_SETTINGS[$name]['properties'])) {
            $properties = array_merge(static::$BASE_SETTINGS[$name]['properties'], $properties);
        }

        $array_settings_name = 'settings.' . $name;

        switch (static::$BASE_SETTINGS[$name]['type']) {
            case 'date' :

                //Possibility to ignore min validation (typically when working on a draft that should have been closed long ago)
                if (isset($properties['ignore_min']) && $properties['ignore_min']) {
                    unset($properties['min']);
                    unset($properties['ignore_min']);
                }

                $field = new FormFieldDate($array_settings_name, $properties, empty($current_value) ? null : $current_value);
                break;
            case 'select' :
                //Each values maybe have a translation
                $values = array_combine(static::$BASE_SETTINGS[$name]['values'], static::$BASE_SETTINGS[$name]['values']);

                if (!isset(static::$BASE_SETTINGS[$name]['no_translation'])) {
                    $values = array_map('Lang::tr', $values);
                }

                $field = new FormFieldSelect($array_settings_name, $values, $properties, $current_value);
                break;
            case 'number' :
                $field = new FormFieldNumber($array_settings_name, $properties, $current_value);
                break;
            case 'text' :
                $field = new FormFieldText($array_settings_name, $properties, $current_value);
                break;
            default :
                $field = new FormFieldSwitch($array_settings_name, $properties, $current_value);
                break;
        }

        return $field;
    }

}
