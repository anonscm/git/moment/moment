<?php

/**
 *     Moment - CalendarStatusCodes.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class containing question types
 */
class CalendarStatusCodes extends Enum {
    const UNKNOWN = -1;
    const OK = 200;
    const DOWN = 400;
    const NOT_FOUND = 404;
    const FORBIDDEN = 403;
    const TIMEOUT = 408;
    const ERROR = 500;
    const INCOMPATIBLE = 600;
    const OVERSIZED = 700;
}
