<?php

/**
 *     Moment - SurveyFilters.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
class SurveyFilters {

    private static $FILTERS = null;

    /**
     * @var array Cache
     */
    private static $FILTERED_SURVEYS = array();

    /**
     * Returns all known filters
     *
     * @param array $surveys (If set it inits counts for each filters)
     * @param bool $renew (Renew filters)
     * @param array $filters_aimed (To limit counts to aimed filters)
     * @return array all filters
     */
    public static function getSurveyFilters(array $surveys = null, $renew = false, array $filters_aimed = null) {

        //Inits filters
        self::initFilters($renew);

        //Init counts if surveys is set
        if (!is_null($surveys)) {
            self::counts($surveys, $filters_aimed);
        }

        return self::$FILTERS;
    }

    /**
     * Apply all known filters to the survey
     * @param Survey $survey
     * @return string (all css classes for this survey)
     */
    public static function applyAll(Survey $survey) {
        //Init filter is necessary if not already called
        self::initFilters();

        $classes = '';
        foreach (self::getSurveyFilters() as $filter_name => $filter) {
            $class_name = self::apply($filter_name, $survey);
            $classes .= empty($class_name) ? '' : ' ' . $class_name;
        }
        return $classes;
    }

    /**
     * Apply list of filters to the survey
     * @param array $filters array of filter names
     * @param Survey $survey
     * @return string
     */
    public static function applyFilters(array $filters, Survey $survey) {
        //Init filter is necessary if not already called
        self::initFilters();

        $classes = '';
        foreach ($filters as $filter_name) {
            $class_name = self::apply($filter_name, $survey);
            $classes .= empty($class_name) ? '' : ' ' . $class_name;
        }
        return $classes;
    }

    /**
     * Check if survey matches at least one filter in filters
     * @param array $filters
     * @param Survey $survey
     * @return bool
     */
    public static function matchesAny(array $filters, Survey $survey) {
        //Init filter is necessary if not already called
        self::initFilters();

        foreach ($filters as $filter_name) {
            if (self::applyFilter($filter_name, $survey)['result'])
                return true;
        }
        return false;
    }

    /**
     * Checks if survey matches no filters
     * @param array $filters
     * @param Survey $survey
     * @return bool
     */
    public static function matchesNo(array $filters, Survey $survey) {
        return !self::matchesAny($filters, $survey);
    }

    /**
     * Initialize filters (if necessary or asked)
     *
     * Filters can be overwritten listening on survey_filters_init event
     * @param bool $renew
     */
    private static function initFilters($renew = false) {
        if (is_null(self::$FILTERS) || $renew) {
            self::$FILTERS = (new Event('survey_filters_init'))->trigger(function () {
                return array(
                    'all' => array(
                        'filter_class' => 'selected',
                        'class' => '',
                        'filter' => '',
                        'callback' => function () {
                            return true;
                        }
                    ),
                    'opened' => array(
                        'filter_class' => 'opened',
                        'class' => 'opened',
                        'filter' => '.opened',
                        'callback' => function (Survey $survey) {
                            return !$survey->is_draft && !$survey->isClosed();
                        }
                    ),
                    'draft' => array(
                        'filter_class' => 'draft',
                        'class' => 'draft',
                        'filter' => '.draft',
                        'callback' => function (Survey $survey) {
                            return $survey->is_draft;
                        }
                    ),
                    'closed' => array(
                        'filter_class' => 'closed',
                        'class' => 'closed',
                        'filter' => '.closed',
                        'callback' => function (Survey $survey) {
                            return $survey->is_closed;
                        }
                    ),
                    'answered' => array(
                        'class' => 'has_answered',
                        'filter' => '.has_answered',
                        'callback' => function (Survey $survey) {
                            return Participant::authenticatedUserHasAnswered($survey);
                        }
                    ),
                    'not_answered' => array(
                        'class' => '',
                        'filter' => ':not(.has_answered)',
                        'callback' => function (Survey $survey) {
                            //A draft cannot be answered
                            if ($survey->is_draft) return false;

                            return !Participant::authenticatedUserHasAnswered($survey);
                        }
                    )
                );
            });
        }
    }

    /**
     * Get class(es) string if survey matches filter
     * @param $filter_name
     * @param Survey $survey
     * @return mixed
     */
    private static function apply($filter_name, Survey $survey) {
        return self::applyFilter($filter_name, $survey)['class'];
    }

    /**
     * Check if survey matches filter
     * @param $filter_name
     * @param Survey $survey
     * @return mixed
     */
    private static function matches($filter_name, Survey $survey) {
        return self::applyFilter($filter_name, $survey)['result'];
    }

    /**
     * Apply a filter on a survey
     * @param $filter_name
     * @param Survey $survey
     * @return mixed array ('result' => true|false, 'class' => <class_for_this_filter>)
     */
    private static function applyFilter($filter_name, Survey $survey) {

        //If result is not found in cache we compute result
        if (!isset(self::$FILTERED_SURVEYS[$survey->id][$filter_name])
            && isset(self::$FILTERS[$filter_name]['callback'])
        ) {

            $result = call_user_func(self::$FILTERS[$filter_name]['callback'], $survey);

            if (!isset(self::$FILTERED_SURVEYS[$survey->id])) {
                self::$FILTERED_SURVEYS[$survey->id] = array();
            }

            //Caching result
            self::$FILTERED_SURVEYS[$survey->id][$filter_name] = array(
                'class' => $result ? self::$FILTERS[$filter_name]['class'] : '',
                'result' => $result
            );
        }

        return self::$FILTERED_SURVEYS[$survey->id][$filter_name];
    }

    /**
     * Counts matches per filter on an array of surveys
     * @param array $surveys
     * @param array $filters_aimed
     */
    private static function counts(array $surveys, array $filters_aimed = null) {

        //Filter all counter
        self::$FILTERS['all']['count'] = 0;

        foreach (self::$FILTERS as $filter_name => &$filter) {
            if (!isset($filter['count'])) {
                $filter['count'] = 0;
            }
        }

        foreach ($surveys as $survey) {
            $matches = false;
            foreach (self::$FILTERS as $filter_name => &$filter) {
                if (!isset($filters_aimed) || !in_array($filter_name, $filters_aimed))
                    continue;

                if (self::matches($filter_name, $survey)) {
                    $filter['count']++;
                    $matches = true;
                }
            }
            if ($matches) {
                self::$FILTERS['all']['count']++;
            }
        }
    }

}
