<?php

/**
 *     Moment - PropositionTypes.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class containing proposition types
 */
class PropositionTypes extends Enum {
    const TEXT = 'text'; //ex. "Proposition 1"
    const DAY = 'day'; //ex. "20/06/2016"
    const DAY_HOUR = 'day_hour'; //ex. "20/06/2016 10:00"
    const RANGE_OF_DAYS = 'range_of_days'; //ex. "20/06/2016-21/06/2016"
    const RANGE_OF_DAYS_HOURS = 'range_of_days_hours'; //ex. "20/06/2016 10:00-21/06/2016 11:00"
    const RANGE_OF_HOURS = 'range_of_hours'; //ex. "20/06/2016 10:00-11:00"
    const MIXED = 'mixed';
}
