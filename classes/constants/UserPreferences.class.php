<?php

/**
 *     Moment - UserSettings.class.php
 *
 * Copyright (C) 2024  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class UserSettings
 *
 */
class UserPreferences extends Settings {

    /**
     * Existing settings for a survey
     * @var type
     */
    protected static $BASE_SETTINGS = null;

    public static function getInput($name, $properties = array(), $current_value = null) {
        static::loadConfig();

        if(is_null($current_value) && isset(static::$BASE_SETTINGS[$name]) && isset(self::$BASE_SETTINGS[$name]['default'])) {
            $current_value = self::$BASE_SETTINGS[$name]['default'];
        }

        return parent::getInput($name, $properties, $current_value);
    }

    public static function getAllInputs(User $user) {
        static::loadConfig();

        $inputs = [];

        foreach (static::$BASE_SETTINGS as $name => $setting) {
            $userPreference = $user->getPreference($name);
            $value = $userPreference ? $userPreference->value : null;

            $inputs[] = static::getInput($name, array(), $value);
        }
        return $inputs;
    }

    public static function castValue($name, $value) {
        static::loadConfig();
        if(!isset(static::$BASE_SETTINGS[$name]))
            return $value;

        switch (static::$BASE_SETTINGS[$name]['type']) {
            case 'date' :
            case 'select' :
            case 'text' :
                if(is_string($value))
                    return $value;
                break;
            case 'number' :
                if(is_numeric($value))
                    return intval($value);
                break;
            default :
                return filter_var($value, FILTER_VALIDATE_BOOLEAN);
                break;
        }

        return $value;
    }

    private static function loadConfig() {
        if(is_null(static::$BASE_SETTINGS)) {
            static::$BASE_SETTINGS = Config::get('user_preferences') ?? [];
        }
    }

}
