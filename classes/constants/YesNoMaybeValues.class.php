<?php

/**
 *     Moment - YesNoMaybeValues.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class containing yes no maybe values
 */
class YesNoMaybeValues extends Enum {
    const YES = 'selected_value_yes';
    const NO = 'selected_value_no';
    const MAYBE = 'selected_value_maybe';

    public static $ICONS = array(
        YesNoMaybeValues::YES => array(
            'icon' => 'fa fa-check '
        ),
        YesNoMaybeValues::NO => array(
            'icon' => 'fa fa-times '
        ),
        YesNoMaybeValues::MAYBE => array(
            'icon' => 'fa fa-check '
        ),
        Answer::UNDEFINED => array(
            'icon' => 'fa fa-question '
        ),
        'forbidden' => array(
            'icon' => 'fa fa-ban'
        )
    );

    public static function convert($value) {
        if ($value == -1) {
            return self::NO;
        }
        if ($value == 0) {
            return self::MAYBE;
        }
        if ($value == 1) {
            return self::YES;
        }
    }

    public static function shorten($value) {
        switch ($value) {
            case self::YES :
                return 'svy';
            case self::MAYBE :
                return 'svm';
            default :
                return 'svn';
        }
    }


    public static function convertToInt($value) {
        switch ($value) {
            case self::YES :
                return 1;
            case self::MAYBE :
                return 0;
            default :
                return 0;
        }
    }

    public static function convertToCustom($value, array $equivalences = array()) {
        if(isset($equivalences[$value])) {
            return $equivalences[$value];
        }

        return self::convertToInt($value);
    }
}
