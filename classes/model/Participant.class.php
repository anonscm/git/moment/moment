<?php

/**
 *     Moment - Participant.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class Participant
 *
 * @property-read Answer[] $sorted_answers
 * @property Answer[] $Answers
 */
class Participant extends Entity {

    /**
     * Database map
     */
    protected static $dataMap = array(
        'id' => array(
            'type' => 'uint',
            'size' => ' ', // This is a tricks to use INT (and not medium or big)
            'primary' => true,
            'autoinc' => true
        ),
        'uid' => array(
            'type' => 'string',
            'size' => 255,
            'primary' => true
        ),
        'type' => array(
            'type' => 'string',
            'size' => 20,
            'primary' => true
        ),
        'name' => array(
            'type' => 'string',
            'size' => 255,
        ),
        'email' => array(
            'type' => 'string',
            'size' => 255,
        ),
        'answers_created' => array(
            'type' => 'datetime',
            'default' => null,
            'null' => true
        ),
        'answers_updated' => array(
            'type' => 'datetime',
            'default' => null,
            'null' => true
        ),
    );

    /**
     * Relations
     */
    protected static $hasMany = array('Answer');

    protected static $hasOne = array('Survey');

    /**
     * Properties
     */
    protected $id;
    protected $uid;
    protected $type = '';
    protected $name = '';
    protected $email = '';
    protected $answers_created = null;
    protected $answers_updated = null;

    private $generated_hash = null;
    private $newly_created = false;


    /**
     * Retrieves a participant for this survey in the current context
     *
     * @param Survey $survey
     * @param array $data
     * @param boolean $create_when_needed
     * @return Participant
     * @throws ParticipantNotFoundException
     */
    public static function getParticipant(Survey $survey, array $data, $create_when_needed = true) {

        //if $data contains type 'ultimate' we try to get the ultimate  participant
        if (Auth::user() && isset($data['participant'])
            && isset($data['participant']->type)
            && $data['participant']->type === ParticipantTypes::ULTIMATE
            && ($survey->isOwner(Auth::user()->email) || Auth::isAdmin())
        ) {
            return self::getUltimateParticipant($survey);
        }

        //If the survey is opened to everyone declarative identity is allowed
        if (Survey::OPENED_TO_EVERYONE == $survey->reply_access) {
            //Check if there is a token in URL (GET)
            if (isset($_GET['participant_token']) && !empty($_GET['participant_token'])) {
                Logger::debug('Participant identified by token in GET');
                //Check if this token an survey matches a participant
                return self::fromSurveyAndDeclarativeUser($survey, $_GET['participant_token']);
            }
            //Check if there is a token in headers
            if (isset($_SERVER['HTTP_X_PARTICIPANT_TOKEN']) && !empty($_SERVER['HTTP_X_PARTICIPANT_TOKEN'])) {
                Logger::debug('Participant identified by token in HTTP_HEADER');
                //Check if this token an survey matches a participant
                return self::fromSurveyAndDeclarativeUser($survey, $_SERVER['HTTP_X_PARTICIPANT_TOKEN']);
            }
            //Check if there is a participant in request data
            if (isset($data['participant'])) {
                Logger::debug('Participant exists in data');

                //Check if there is a token is request data
                if (isset($data['participant']->participant_token) && !empty($data['participant']->participant_token)) {
                    Logger::debug('Participant identified by token in data');
                    //Check if this token an survey matches a participant
                    return self::fromSurveyAndDeclarativeUser($survey, $data['participant']->participant_token);
                }
                //Check if there are declarative data
                if ($create_when_needed && ((isset($data['participant']->email) && !empty($data['participant']->email))
                        || (isset($data['participant']->name) && !empty($data['participant']->name)))
                ) {
                    return self::createDeclarativeParticipant($survey, (array)$data['participant']);
                }
            }

            //Survey enables anonymous answers (Authenticated != Anonymous)
            if ($create_when_needed && !Auth::user() && $survey->enable_anonymous_answer) {
                return self::createDeclarativeParticipant($survey, array());
            }
        }

        //If here, then user must be authenticated to get a participant
        if (Auth::user()) {
            try {
                Logger::debug('Participant identified by authentication');
                return self::fromSurveyAndAuthenticatedUser($survey);
            } catch (ParticipantNotFoundException $e) {
                //We haven't found a participant for authenticated user -> we create it if asked
                if ($create_when_needed)
                    return self::createAuthenticatedParticipant($survey);
            }
        }

        //No Participant have been found nor created
        throw new ParticipantNotFoundException($survey, 'no_way_to_identify');
    }

    /**
     * Get the ultimate participant for survey, the final answers owner
     * @param Survey $survey
     * @param bool $create_when_needed
     * @return mixed
     * @throws ParticipantNotFoundException
     */
    public static function getUltimateParticipant(Survey $survey, $create_when_needed = true) {

        $results = self::all(self::getRelationColumn('Survey', 'id') . ' = :survey_id AND type = :type',
            array(':survey_id' => $survey->id, ':type' => ParticipantTypes::ULTIMATE));

        if (count($results) > 0) {
            return array_pop($results);
        }

        //Ultimate participant doesn't exist yet, we may need to create it (only owners or admin can create it)
        if ($create_when_needed
            && Auth::user()
            && ($survey->isOwner(Auth::user()->email) || Auth::isAdmin())
        ) {
            return self::createUltimateParticipant($survey);
        }

        //No Participant have been found nor created
        throw new ParticipantNotFoundException($survey, 'no_way');
    }

    /**
     *
     * @param type $uid
     * @return type
     */
    public static function unicityChecker($uid) {
        return (count(self::all('uid = :uid',
                array(':uid' => $uid))) == 0);
    }

    /**
     * Check if current user has answered the survey
     * <b>Important :</b>It doesn't create a participant
     * @param Survey $survey
     * @param array $data
     * @return boolean TRUE if participant exists and answered, FALSE otherwise
     */
    public static function hasAnswered(Survey $survey, array $data = array()) {
        try {
            //We try to get a participant (without creating it)
            $participant = Participant::getParticipant($survey, $data, false);
            return !is_null($participant->answers_created);
        } catch (ParticipantNotFoundException $ex) {
            return false;
        }
    }

    /**
     * Check if current user has answered the survey
     * <b>Important :</b>It doesn't create a participant
     * @param Survey $survey
     * @return boolean TRUE if participant exists and answered, FALSE otherwise
     */
    public static function authenticatedUserHasAnswered(Survey $survey) {
        $check = DBI::prepare('SELECT count(*) AS count FROM ' . Participant::getDBTable()
            . ' WHERE survey_id = :survey_id AND email = :email AND answers_created IS NOT NULL AND (type = :type1 OR type = :type2)');
        $check->execute(
            array(
                ':survey_id' => $survey->id,
                ':email' => Auth::user()->email,
                ':type1' => ParticipantTypes::AUTHENTICATED, ':type2' => ParticipantTypes::DECLARATIVE
            ));
        return ($check->fetchColumn(0) > 0);
    }

    /**
     * Creates and saves a new declarative participant
     * @param Survey $survey
     * @param array $participant_data
     * @return Participant
     * @throws ParticipantAlreadyExistsException
     * @throws ParticipantMissingPropertyException
     */
    private static function createDeclarativeParticipant(Survey $survey, array $participant_data) {

        //We need to check that a participant hasn't that email for this survey
        $present = true;
        $existing = null;
        try {
            if (!empty($participant_data) && isset($participant_data['email']) && !empty($participant_data['email'])) {
                $existing = Participant::fromSurveyAndDeclarativeEmail($survey, $participant_data['email']);
            } else {
                //We can't match a participant with no email, so we consider it's not present
                $present = false;
            }
        } catch (ParticipantNotFoundException $e) {
            $present = false;
        }

        //Anonymous participant has no participant data
        if ($survey->enable_anonymous_answer && empty($participant_data)) {
            $present = false;
        }

        //Non anonymous participant must have a name and email
        if (!$survey->enable_anonymous_answer
            && empty($participant_data['name'])
        ) {
            throw new ParticipantMissingPropertyException('name', $participant_data);
        }
        if (!$survey->enable_anonymous_answer
            && empty($participant_data['email'])
        ) {
            throw new ParticipantMissingPropertyException('email', $participant_data);
        }

        // If someone with this email has already created a participant => fail
        if ($present) {
            if($existing) {
                // If it's a declarative participant we send a code
                if(ParticipantTypes::DECLARATIVE === $existing->type && !$survey->disable_answer_edition) {
                    ParticipantRecoveryUtil::sendCodeForParticipant($existing);
                }

                throw new ParticipantAlreadyExistsException($existing->type, !$survey->disable_answer_edition);
            }

            throw new ParticipantAlreadyExistsException(null);
        }

        $participant = new self();

        //Generating uid 
        $participant->uid = Utilities::generateUID(self::name() . '::unicityChecker');

        //Setting type
        $participant->type = ParticipantTypes::DECLARATIVE;

        if (!empty($participant_data)) {
            if (isset($participant_data['email']) && !empty($participant_data['email'])) {
                $participant->email = $participant_data['email'];
            }

            if (isset($participant_data['name']) && !empty($participant_data['name'])) {
                $participant->name = $participant_data['name'];
            }
        }

        //Linking with survey
        $participant->Survey = $survey;

        $participant->newly_created = true;

        $participant->save();

        return $participant;
    }

    /**
     * Creates and saves a new authenticated participant
     * @param Survey $survey
     * @return Participant
     */
    private static function createAuthenticatedParticipant(Survey $survey) {

        $participant = new self();

        //uid is users uid
        $participant->uid = Auth::user()->id;

        //Setting type
        $participant->type = ParticipantTypes::AUTHENTICATED;

        $participant->email = Auth::user()->email;
        $participant->name = Auth::user()->name;

        //Linking with survey
        $participant->Survey = $survey;

        $participant->newly_created = true;

        $participant->save();

        return $participant;
    }

    /**
     * Creates and saves a new ultimater participant
     * @param Survey $survey
     * @return Participant
     */
    private static function createUltimateParticipant(Survey $survey) {

        $participant = new self();

        //uid is owner uid
        $participant->uid = Auth::user()->email;

        //Setting type
        $participant->type = ParticipantTypes::ULTIMATE;

        //In case someone else (admin) would have created ultimate
        $participant->email = Auth::user()->email;
        $participant->name = Auth::user()->name;

        //Linking with survey
        $participant->Survey = $survey;

        $participant->newly_created = true;

        $participant->save();

        return $participant;
    }

    /**
     * Get all participants matching a specific email
     * @param User $user
     * @return array
     */
    public static function fromAuthenticatedUser(User $user) {
        return self::all('email = :email AND (type = :type1 OR type = :type2)',
            array(':email' => $user->email, ':type1' => ParticipantTypes::AUTHENTICATED, ':type2' => ParticipantTypes::DECLARATIVE));
    }

    /**
     * Return all participants for a survey
     *
     * <b>Important</b> Filters respondent only
     * @param $survey_id
     * @return array
     */
    public static function fromSurveyId($survey_id) {

        return self::all('survey_id = :survey_id AND answers_created IS NOT NULL',
            array(':survey_id' => $survey_id));
    }

    /**
     * Get participant by its autoincrement id
     * @param string $id
     * @return mixed
     */
    public static function fromIdOnly($id = '', $type = null) {
        $query = 'id = :id';
        $placeholder = array(':id' => $id);

        if($type) {
            $query .= ' AND type = :type';
            $placeholder[':type'] = $type;
        }
        
        $results = self::all($query, $placeholder);
        if (count($results) > 0)
            return array_pop($results);
    }

    /**
     * Get Participant from specified Survey
     *
     * @param Survey $survey the survey
     * @param $email email of the participant
     * @return Participant
     * @throws ParticipantNotFoundException
     *
     */
    public static function fromSurveyAndEmail(Survey $survey, $email) {
        $results = self::all('email = :email AND ' . self::getRelationColumn('Survey', 'id') . ' = :survey_id',
            array(':email' => $email, ':survey_id' => $survey->id));

        if (count($results) > 0)
            return array_pop($results);

        throw new ParticipantNotFoundException($survey, $email);
    }

    /**
     * Get Participant from specified Survey and declarative user identified by token
     *
     * @param Survey $survey the survey
     * @param $email
     * @return Participant
     * @throws ParticipantNotFoundException
     * @internal param string $token the token
     *
     */
    public static function fromSurveyAndDeclarativeEmail(Survey $survey, $email) {

        $results = self::all('email = :email AND ' . self::getRelationColumn('Survey', 'id') . ' = :survey_id',
            array(':email' => $email, ':survey_id' => $survey->id/*, ':type' => ParticipantTypes::DECLARATIVE*/));

        if (count($results) > 0)
            return array_pop($results);

        throw new ParticipantNotFoundException($survey, $email);
    }


    /**
     * Get Participant from specified Survey and declarative user identified by token
     *
     * @param Survey $survey the survey
     * @param string $token the token
     *
     * @return Participant
     * @throws ParticipantInvalidTokenException
     */
    private static function fromSurveyAndDeclarativeUser(Survey $survey, $token) {


        $results = self::all('uid = :uid AND ' . self::getRelationColumn('Survey', 'id') . ' = :survey_id AND type = :type',
            array(':uid' => $token, ':survey_id' => $survey->id, ':type' => ParticipantTypes::DECLARATIVE));

        if (count($results) > 0)
            return array_pop($results);

        throw new ParticipantInvalidTokenException($survey, $token);
    }

    /**
     * Get Participant from specified Survey and authenticated user (Since 1.7 It can be DECLARATIVE or AUTHENTICATED)
     *
     * @param Survey $survey the survey
     *
     * @return Participant
     * @throws ParticipantNotFoundException
     */
    private static function fromSurveyAndAuthenticatedUser(Survey $survey) {
        $results = self::all(self::getRelationColumn('Survey', 'id') . ' = :survey_id AND email = :email AND type <> :type',
            array(':email' => Auth::user()->email, ':survey_id' => $survey->id, ':type' => ParticipantTypes::ULTIMATE));

        if (count($results) > 0)
            return array_pop($results);

        throw new ParticipantNotFoundException($survey, Auth::user()->id);
    }

    /**
     * Casts a Participant to array
     * @param Participant $participant
     * @param array $current_user
     * @param boolean $with_answers
     * @param bool $with_comments
     * @return array
     */
    public static function cast(Participant $participant,
                                array $current_user = array('role' => SurveyUserRoles::NONE)) {

        $array = array(
            'id' => $participant->id,
            'participant_hash' => $participant->hash,
            'type' => $participant->type,
            'has_email' => ($participant->email != '')
        );
        if (SurveyUserRoles::OWNER == $current_user['role']
            || $participant->Survey->show_participant_name == 1
        ) {
            $array['name'] = $participant->ensure_name;
        }
        if (SurveyUserRoles::OWNER == $current_user['role']
            || $participant->Survey->show_participant_email == 1
        ) {
            $array['email'] = $participant->email;
        }

        if (isset($current_user['can']['view_answers']) && $current_user['can']['view_answers']) {
            $array['answers'] = array_map(array(Answer::name(), 'cast'), $participant->Answers);
        }

        if (isset($current_user['email']) && $participant->email === $current_user['email']) {
            $array['participant_token'] = $participant->uid;
        }

        return $array;
    }

    /**
     * Casts a Collection of array
     * @param Participant $participant
     * @param array $current_user
     * @return array
     */
    public static function castCollection(array $participants,
                                          array $current_user = array('role' => SurveyUserRoles::NONE)) {

        $array = array();
        foreach ($participants as $participant) {
            $array[] = self::cast($participant, $current_user);
        }

        return $array;
    }

    /**
     * Send notification to a list of participants
     *
     * @param Participant $participant
     * @param string $participant_notification_type
     * @param string $participant_notification_reason
     * @return bool
     */
    public static function sendNotification(Participant $participant,
                                            $participant_notification_type,
                                            $participant_notification_reason = ParticipantNotificationReasons::REMINDER) {
        switch ($participant_notification_type) {
            //We want to (re-)send the participant_token to the participant
            case (ParticipantNotificationTypes::TOKEN) :
                return MomentApplicationMail::prepareTokenMail($participant, $participant_notification_reason)->send();
                break;
            //We want to (re-)send the participant_token to the participant
            case (ParticipantNotificationTypes::DELETION_CONFIRMATION) :
                return MomentApplicationMail::prepareParticipantDeletionConfirmationMail($participant)->send();
                break;
        }
        return false;
    }

    /**
     * Delete associated answers, comments
     */
    public function beforeDelete() {
        foreach ($this->Answers as $answer) {
            $answer->delete();
        }
    }

    /**
     * Updates participant name with data
     * @param array $participant_data
     */
    public function update(array $participant_data) {

        if (array_key_exists('name', $participant_data)
            && !empty($participant_data['name'])
        ) {
            $this->name = $participant_data['name'];
        }
        $this->save();
    }

    /**
     * Saving participant
     */
    public function save() {
        // An authenticated user has created/updated his participation we may need to invalidate his calendar
        if(ParticipantTypes::AUTHENTICATED == $this->type) {
            MyCalUtil::invalidateIcsCacheForUser(Auth::user()->email);
        }

        if ($this->storedInDatabase) {
            EventLog::push($this, EventLogTypes::UPDATED, $this->Survey->id);
            parent::save();
        } else {
            parent::save();
            EventLog::push($this, EventLogTypes::CREATED, $this->Survey->id);
        }

        // Record User (if any) activity
        $this->recordUserActivity();
    }

    /**
     * Deleting participant
     */
    public function delete() {
        EventLog::push($this, EventLogTypes::DELETED);
        $this->recordUserActivity();

        parent::delete();
    }

    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return property value
     */
    public function __get($property) {
        if (in_array($property, array(
            'id', 'uid', 'type', 'name', 'email', 'answers_created', 'answers_updated', 'newly_created'
        ))) {
            return $this->$property;
        }

        if ($property === 'ensure_name') {
            return ($this->name === '') ? Lang::tr('anonymous')->out() : $this->name;
        }

        if ($property === 'sorted_answers') {
            $sorted = array();

            foreach ($this->Answers as $answer) {
                $sorted[$answer->Question->id] = $answer;
            }

            return $sorted;
        }

        if ($property === 'hash') {
            if (is_null($this->generated_hash)) {
                //Hash is id.type.email. Timestamp
                $this->generated_hash = substr(sha1($this->id . $this->type . $this->email . microtime()), 0, 8);

            }
            return $this->generated_hash;
        }

        return parent::__get($property);
    }


    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if (in_array($property, array(
            'answers_created', 'answers_updated'
        ))) {
            $this->$property = $value;
        } else {
            parent::__set($property, $value);
        }

    }

    /**
     * Check if participant is anonymous
     * @return bool
     */
    public function isAnonymous() {
        return empty($this->email);
    }

    /**
     * Record User activity for each Users that match this declarative Participant
     *
     * @return void
     */
    private function recordUserActivity() {
        try {
            if (ParticipantTypes::DECLARATIVE === $this->type && $this->email != '') {
                foreach (User::fromUserEmail($this->email) as $user) {
                    $user->recordActivity();
                    //We update only the most recent (fromUserEmail is sorted by last_activity desc)
                    break;
                }
            }
        } catch(Exception $e) {
            Logger::error($e->getMessage());
            //We should not stop here
        }
    }
}
