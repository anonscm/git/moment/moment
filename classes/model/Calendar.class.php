<?php

/**
 *     Moment - Calendar.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class Calendar
 */
class Calendar extends Entity {

    /**
     * Datamap
     * @var array
     */
    protected static $dataMap = array(
        'id' => array(//represent id of calendar
            'type' => 'uint',
            'size' => 'medium',
            'primary' => true,
            'autoinc' => true
        ),
        'url' => array( //represent url of calendar
            'type' => 'string',
            'size' => 255
        ),
        'name' => array( //represent url of calendar
            'type' => 'string',
            'size' => 255,
            'null' => true
        ),
        'checked' => array(
            'type' => 'datetime',
            'null' => true
        ),
        'status' => array( //represent status (code / message ...)
            'type' => 'text',
            'default' => '[]',
            'transform' => 'json'
        ),
        'settings' => array( //represent settings for this calendar
            'type' => 'text',
            'default' => '[]',
            'transform' => 'json'
        ),
    );

    /**
     * Relation
     */
    protected static $hasOne = array('User');

    protected $id = null;
    protected $url = null;
    protected $name = null;
    protected $checked = null;
    protected $status = array();
    protected $settings = array();

    public static function create(User $user, array $data) {

        $cal = new self();
        $cal->User = $user;

        if(!array_key_exists('url', $data))
            throw new MissingArgumentException('calendar.url');

        $cal->url = $data['url'];

        $settable = array('name', 'settings', 'color');
        foreach($settable as $key) {
            if(isset($data[$key])) {
                $cal->$key = $data[$key];
            }
        }

        $cal->save();

        //Trigger status refreshing
        $cal->refreshStatus();

        return $cal;
    }

    /**
     * Getters
     * @param string $property
     * @return mixed
     */
    public function __get($property) {
        if (in_array($property, array(
            'id', 'url', 'checked'
        ))) {
            return $this->$property;
        }
        if ('status' === $property) {
            //Setting default values
            if(!isset($this->status['code'])) {
                $this->status['code'] = CalendarStatusCodes::UNKNOWN;
            }
            if(!isset($this->status['message']) || !CalendarStatusMessages::isValid($this->status['message'])) {
                $this->status['message'] = CalendarStatusMessages::UNKNOWN;
            }

            // Checking if status must be refreshed
            $freshLimitDate = strtotime('-' . Config::get('calendar.status_validity_duration') . ' minutes', time());

            if(!$this->checked || $this->checked < $freshLimitDate) {
                //Checking url (it updates status and checked)
                $this->refreshStatus();
            }

            return $this->$property;
        }
        if ('fresh_status' === $property) {
            //Fresh status means that we need to refresh status
            $this->refreshStatus();

            return $this->status;
        }
        if ('name' === $property) {
            //Default name is domain name
            if($this->name == null) {
                $this->name = parse_url($this->url, PHP_URL_HOST);
            }
            return $this->name;
        }
        if ('settings' === $property) {
            //Default color
            if(!isset($this->settings['color'])) {
                $this->settings['color'] = CalendarUtil::getColor();
            }
            return $this->settings;
        }

        return parent::__get($property);
    }

    /**
     * Setters
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value) {
        if (in_array($property, array(
            'id', 'url', 'name', 'checked', 'status', 'settings'
        ))) {
            $this->$property = $value;
        } else if ($property === 'color') {
            $this->settings[$property] = $value;
        } else {
            parent::__set($property, $value);
        }
    }

    /**
     * Update Calendar Entity
     * @param User $user
     * @param array $data
     * @return $this
     */
    public function update(User $user, array $data) {

        $this->url = $data['url'];
        $this->name = $data['name'];

        $settable = array('settings', 'color');
        foreach($settable as $key) {
            if(isset($data[$key])) {
                $this->$key = $data[$key];
            }
        }

        //Reinit status
        $this->status = array();
        $this->checked = null;

        $this->save();

        return $this;
    }

    private function refreshStatus() {
        CalendarManager::checkUrl($this->url);
    }

}