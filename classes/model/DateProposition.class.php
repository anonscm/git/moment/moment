<?php

/**
 *     Moment - DateProposition.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class DateProposition
 */
class DateProposition extends Proposition {

    /**
     * Database map
     */
    protected static $dataMap = array(
        'base_day' => array(
            'type' => 'date'
        ),
        'base_time' => array(
            'type' => 'time',
            'default' => null,
            'null' => true
        ),
        'end_day' => array(
            'type' => 'date',
            'null' => true
        ),
        'end_time' => array(
            'type' => 'time',
            'default' => null,
            'null' => true
        )
    );

    protected static $hasOne = array('Question');

    /**
     * Properties
     */
    protected $base_day = 0;
    protected $base_time = null;
    protected $end_day = null;
    protected $end_time = null;

    /**
     * Private properties
     */
    private $time_zone_offset = null;
    private $raw_label = null;

    /**
     * Create a new date proposition
     *
     * @param Question $question
     * @param array $proposition_data
     * @return DateProposition
     * @throws PropositionMissingPropertyException
     */
    public static function create(Question $question, array $proposition_data = array()) {

        //The only mandatory field is base_day
        if (!array_key_exists('base_day', $proposition_data))
            throw new PropositionMissingPropertyException('base_day', $proposition_data);


        // TODO Check validity

        $proposition = parent::create($question, $proposition_data);
        $proposition->base_day = $proposition_data['base_day'];

        if (isset($proposition_data['base_time']) && $proposition_data['base_time'] !== false) {
            $proposition->base_time = $proposition_data['base_time'];
        }

        if (isset($proposition_data['end_day']) && $proposition_data['end_day'] !== false) {
            $proposition->end_day = $proposition_data['end_day'];
        }

        if (isset($proposition_data['end_time']) && $proposition_data['end_time'] !== false) {
            $proposition->end_time = $proposition_data['end_time'];
        }

        //Saving date proposition
        $proposition->save();

        return $proposition;
    }

    /**
     * Casts a DateProposition into an array
     * @param DateProposition $proposition
     *
     * @return array
     */
    public static function cast(DateProposition $proposition) {
        //Getting $proposition type
        $type = $proposition->type;

        $array = array(
            'id' => $proposition->id,
            'type' => $type,
            'base_day' => $proposition->base_day,
            'base_day_local' => $proposition->base_day_local,
            'options' => $proposition->options
        );

        //The type of proposition determine what we send
        switch ($type) {
            case PropositionTypes::DAY_HOUR :
                $array['base_time'] = $proposition->base_time;
                break;
            case PropositionTypes::RANGE_OF_DAYS :
                $array['end_day'] = $proposition->end_day;
                $array['end_day_local'] = $proposition->end_day_local;
                break;
            case PropositionTypes::RANGE_OF_DAYS_HOURS :
                if (isset($proposition->base_time))
                    $array['base_time'] = $proposition->base_time;
                $array['end_day'] = $proposition->end_day;
                if (isset($proposition->end_time))
                    $array['end_time'] = $proposition->end_time;
                break;
            case PropositionTypes::RANGE_OF_HOURS :
                $array['base_time'] = $proposition->base_time;
                $array['end_time'] = $proposition->end_time;
                break;
        }

        return $array;
    }

    /**
     * Compare two propositions
     * @param mixed $proposition_a
     * @param mixed $proposition_b
     * @return bool
     */
    public static function compare($proposition_a, $proposition_b) {
        if (!$proposition_a instanceof DateProposition || !$proposition_b instanceof DateProposition) {
            return 0;
        }

        $position_a_bd = $proposition_a->base_day;
        $position_a_bt = isset($proposition_a->base_time) ? $proposition_a->base_time : 0;

        $position_b_bd = $proposition_b->base_day;
        $position_b_bt = isset($proposition_b->base_time) ? $proposition_b->base_time : 0;

        // Propositions start on the same date / hour => we must compare end date / hour
        if(($position_b_bd + $position_b_bt) == ($position_a_bd + $position_a_bt)) {
            $position_a_ed = isset($proposition_a->end_day) ? $proposition_a->end_day : 0;
            $position_a_et = isset($proposition_a->end_time) ? $proposition_a->end_time : 0;

            $position_b_ed = isset($proposition_b->end_day) ? $proposition_b->end_day : 0;
            $position_b_et = isset($proposition_b->end_time) ? $proposition_b->end_time : 0;

            return ($position_b_ed + $position_b_et) < ($position_a_ed + $position_a_et);
        }

        return ($position_b_bd + $position_b_bt) < ($position_a_bd + $position_a_bt);
    }

    /**
     * Utility to determine proposition type
     * @param stdClass $proposition
     * @return string
     */
    public static function getType($proposition) {

        //We compute the type every time it is asked
        //Default type is DAY
        $type = PropositionTypes::DAY;
        if (isset($proposition->base_time))
            $type = PropositionTypes::DAY_HOUR;
        if (isset($proposition->end_day)
            && isset($proposition->base_day)
            && ($proposition->end_day >= $proposition->base_day)
        ) {
            if (PropositionTypes::DAY_HOUR === $type) {
                $type = PropositionTypes::RANGE_OF_DAYS_HOURS;
            } else {
                $type = PropositionTypes::RANGE_OF_DAYS;
            }
        }
        if (isset($proposition->end_time) && (isset($proposition->base_time) || $proposition->end_day)) {
            if ($type == PropositionTypes::DAY_HOUR) {
                $type = PropositionTypes::RANGE_OF_HOURS;
            } else {
                $type = PropositionTypes::RANGE_OF_DAYS_HOURS;
            }
        }

        return $type;
    }

    /**
     * Get proposition from values
     * @param Question $question
     * @param array $data
     * @return DateProposition
     * @throws PropositionNotFoundException
     */
    public static function fromValues(Question $question, array $data): DateProposition {
        $ph = [':question_id' => $question->id];
        foreach(['base_day', 'base_time', 'end_day', 'end_time'] as $key) {
            if (isset($data[$key])) {
                $value = $data[$key];
                switch (static::$dataMap[$key]['type']) { // Basic types transformations/casting
                    case 'date':
                        $value = (new DateTime('@' . $value, new DateTimeZone('UTC')))->format('Y-m-d');
                        break;

                    case 'time':
                        $value = (new DateTime('@' . $value, new DateTimeZone('UTC')))->format('H:i:s');
                        break;
                }

                $ph[':' . $key] = $value;
            }
        }
        $query = implode(' AND ', array_map(function($k,$v) { return trim($k, ':').' = ' . $k; }, array_keys($ph), $ph));

        $propositions = static::all($query, $ph);
        if(count($propositions) === 1)
            return array_shift($propositions);

        throw new PropositionNotFoundException();
    }


    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return property value
     */
    public function __get($property) {
        if (in_array($property, array(
            'base_day', 'base_time', 'end_day', 'end_time'
        ))) {
            return $this->$property;
        }

        if ('base_day_local' == $property) {
            return $this->base_day - $this->__get('time_zone_offset');
        }

        if ('end_day_local' == $property) {
            return $this->end_day - $this->__get('time_zone_offset');
        }

        if ('base_time_local' == $property) {
            $base_time_local = $this->base_time + $this->__get('time_zone_offset');
            return $base_time_local + (($base_time_local >= 86400)?-86400:0);
        }

        if ('end_time_local' == $property) {
            $end_time_local = $this->end_time + $this->__get('time_zone_offset');
            return $end_time_local + (($end_time_local >= 86400)?-86400:0);
        }

        if('time_zone_offset' == $property) {
            if(!isset($this->time_zone_offset)) {
                $this->time_zone_offset = ($this->Question->Survey->time_zone != null && $this->Question->Survey->time_zone != '') ? DateUtil::getTimezoneOffset($this->Question->Survey->time_zone, $this->base_day) : 0;
            }
            return $this->time_zone_offset;
        }

        if ('type' == $property) {
            return self::getType($this);
        }

        if ('label' == $property) {

            $label = '<span data-timestamp="' . $this->base_day . '" data-localize="date_readable_single_line"></span>';
            if (isset($this->base_time)) {
                $label = '<span data-timestamp="' . ($this->base_day + $this->base_time) . '" data-localize="date_time_readable"></span>';
            }
            if ($this->end_day) {
                $label .= ' ' . Lang::tr('to_day') . ' <span data-timestamp="' . $this->end_day . '" data-localize="date_readable_single_line"></span>';
            }
            if (isset($this->end_time)) {
                $label .= ' ' . Lang::tr('to_hour') . ' <span data-timestamp="' . ($this->base_day + $this->end_time) . '" data-localize="time"></span>';
            }
            return $label;
        }

        if ('raw_label' == $property) {
            //using already set raw_label if present
            if(isset($this->raw_label)) {
                return $this->raw_label;
            }

            //TODO by default use the tz one the survey
            $timezone = array_key_exists('tz', $_GET) ? $_GET['tz'] : $this->Question->Survey->time_zone;

            $label = DateUtil::toLocale($this->base_day, $timezone, 'date');
            if (isset($this->base_time)) {
                $label = DateUtil::toLocale($this->base_day + $this->base_time, $timezone, 'date');
                $label .= ' ' . DateUtil::toLocale($this->base_day + $this->base_time, $timezone, 'time');
            }
            if ($this->end_day) {
                $tmp = ' ' . Lang::tr('to_day') . ' ' . DateUtil::toLocale($this->end_day, $timezone, 'date') . ' ';
                if (isset($this->end_time)) {
                    $tmp .= ' '. DateUtil::toLocale($this->end_day + $this->end_time, $timezone, 'time');
                }
                $label .= $tmp;
            } else if (isset($this->end_time)) {
                $label .= ' ' . Lang::tr('to_hour') . ' ' . DateUtil::toLocale($this->base_day + $this->end_time, $timezone, 'time') . ' ';
            }
            $label .= ' (' . $timezone . ')';

            //Storing raw_label for next usage
            $this->raw_label = $label;

            return $label;
        }

        return parent::__get($property);
    }

    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if (in_array($property, array(
            'base_day', 'base_time', 'end_day', 'end_time'
        ))) {
            //Raw label changes to
            $this->raw_label = null;

            $this->$property = $value;
        } else {
            parent::__set($property, $value);
        }
    }

    /**
     * Test if property is not null !! Important uses magic getter
     * @param $property
     * @return bool
     */
    public function __isset($property) {
        //Explicitly call magic getter
        $value = $this->__get($property);
        return isset($value);
    }

    /**
     * Update the proposition with data.
     * <b>Important</b> It creates a new DateProposition if something changes, to preserve integrity
     * @param Question $question
     * @param array $proposition_data
     * @throws PropositionMissingPropertyException
     */
    public function update(Question $question, array $proposition_data = array()) {

        parent::update($question, $proposition_data);

        //The only mandatory field is base_day
        if (!array_key_exists('base_day', $proposition_data))
            throw new PropositionMissingPropertyException('base_day', $proposition_data);

        //Test if something changed
        if ($this->base_day != $proposition_data['base_day']
            || (!array_key_exists('base_time', $proposition_data) && !empty($this->base_time))
            || (!empty($proposition_data['base_time']) && $this->base_time != $proposition_data['base_time'])
            || (!array_key_exists('end_day', $proposition_data) && !empty($this->end_day))
            || (!empty($proposition_data['end_day']) && $this->end_day != $proposition_data['end_day'])
            || (!array_key_exists('end_time', $proposition_data) && !empty($this->end_time))
            || (!empty($proposition_data['end_time']) && $this->end_time != $proposition_data['end_time'])
        ) {
            $new_proposition = self::create($question, $proposition_data);
            $this->id = $new_proposition->id;
        } else { //No time value changed, maybe options changed
            $this->save();
        }

    }

    public function asSlot() {
        $proposition_slot = array();
        switch ($this->__get('type')) {
            case PropositionTypes::DAY : // 01/01/01 from 00:00:00 to 23:59:59
                $proposition_slot['start'] = $this->base_day_local;
                $proposition_slot['end'] = $this->base_day_local + 86399;
                break;
            case PropositionTypes::DAY_HOUR : // 01/01/01 from 10:00:00 to 10:59:59
                $proposition_slot['start'] = $this->base_day + $this->base_time;
                $proposition_slot['end'] = $proposition_slot['start'] + 3599;
                break;
            case PropositionTypes::RANGE_OF_DAYS : // From 01/01/01 from 10:00:00 to 02/01/01 23:59:59
                $proposition_slot['start'] = $this->base_day_local;
                $proposition_slot['end'] = $this->end_day_local + 86399;
                break;
            case PropositionTypes::RANGE_OF_DAYS_HOURS :
                $proposition_slot['start'] = $this->base_day + $this->base_time;
                $proposition_slot['end'] = $this->end_day + $this->end_time;
                break;
            case PropositionTypes::RANGE_OF_HOURS :
                $proposition_slot['start'] = $this->base_day + $this->base_time;
                $proposition_slot['end'] = $this->base_day + $this->end_time;
                break;
        }
        return $proposition_slot;
    }

}
