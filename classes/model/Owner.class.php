<?php

/**
 *     Moment - Owner.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class Owner
 */
class Owner extends Entity {

    /**
     * Database map (We can't use hasOne Survey)
     */
    protected static $dataMap = array(
        'name' => array(
            'type' => 'string',
            'size' => 255,
        ),
        'email' => array(
            'type' => 'string',
            'size' => 255,
            'primary' => true,
            'index' => true
        ),
        'survey_id' => array(
            'type' => 'string',
            'size' => Survey::UID_LENGTH,
            'primary' => true
        ),
    );

    /**
     * Properties
     */
    protected $name = '';
    protected $email = '';
    protected $survey_id = '';

    /**
     * Create a new owner
     *
     * @param Survey $survey
     * @param $owner_data
     * @return Owner
     * @throws GuestInvalidEmailException
     * @throws GuestSurveyNotSavedException
     * @internal param $owner_email
     */
    public static function create(Survey $survey, $owner_data) {

        if ($owner_data instanceof User) {
            $owner_data = array(
                'uid' => $owner_data->id,
                'name' => $owner_data->name,
                'email' => $owner_data->email
            );
        }

        //Check if survey is stored
        if (!$survey->__get('storedInDatabase'))
            throw new OwnerSurveyNotSavedException();

        //Check mail validity
        if (!array_key_exists('email', $owner_data)
            || empty($owner_data['email'])
            || !filter_var($owner_data['email'], FILTER_VALIDATE_EMAIL)
        )
            throw new OwnerInvalidEmailException($owner_data['email']);

        $owner = new self();
        $owner->name = (isset($owner_data['name'])) ? $owner_data['name'] : '';
        $owner->email = $owner_data['email'];
        $owner->survey_id = $survey->id;


        //Saving guest
        $owner->save();

        return $owner;
    }

    /**
     * Checks if owner exists for survey
     *
     * @param $email
     * @param Survey $survey the survey
     * @return bool
     */
    public static function exists($email, Survey $survey) {
        return count(self::all('email = :email AND survey_id = :survey_id',
                array(':email' => $email, ':survey_id' => $survey->id))) > 0;
    }

    /**
     * Get all Owners from Survey
     *
     * @param Survey $survey the survey
     *
     * @return array
     */
    public static function fromSurvey(Survey $survey) {
        return self::all('survey_id = :survey_id', array(':survey_id' => $survey->id));
    }

    /**
     * Get all Owners where email is $email
     *
     * @param string $email the email
     *
     * @return array
     */
    public static function fromEmail($email) {
        return self::all('email = :email', array(':email' => $email));
    }

    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return property value
     */
    public function __get($property) {
        if (in_array($property, array(
            'name', 'email', 'survey_id'
        ))) {
            return $this->$property;
        }

        if (strtolower($property) === 'survey') {
            return Survey::fromId($this->survey_id);
        }

        return parent::__get($property);
    }

    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if (in_array($property, array(
            'name', 'email', 'survey_id'
        ))) {
            $this->$property = $value;
        } else {
            parent::__set($property, $value);
        }

    }

    /**
     * Updates an owner (only name is editable)
     *
     * @param array $owner_data
     * @throws QuestionBadPropertyException
     * @throws QuestionCrossAccessException
     * @throws QuestionMissingPropertyException
     */
    public function update(array $owner_data) {

        $this->name = (isset($owner_data['name'])) ? $owner_data['name'] : '';

        $this->save();
    }
}
