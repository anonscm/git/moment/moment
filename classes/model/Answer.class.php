<?php

/**
 *     Moment - Answer.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class Answer
 *
 * @property Choice[] $choices
 */
class Answer extends Entity {

    /**
     * Constant used when no choice is made for proposition
     */
    const UNDEFINED = 'undefined';

    /**
     * Database map
     */
    protected static $dataMap = array(
        'id' => array(
            'type' => 'uint',
            'size' => ' ', // This is a tricks to use INT (and not medium or big)
            'primary' => true,
            'autoinc' => true
        ),
        'choices' => array(
            'type' => 'text',
            'default' => '[]',
            'transform' => 'json'
        ),
        'comment' => array(
            'type' => 'text',
            'size' => 255,
            'null' => true
        ),
        'hide_comment' => array(
            'type' => 'boolean',
            'default' => 0
        )
    );

    protected static $hasOne = array('@Participant', 'Question');

    protected $id = null;
    protected $choices = '';
    protected $comment = null;
    protected $hide_comment = false;

    private $sorted_choices = null;

    /**
     * Create a new answer for participant
     *
     * @param Participant $participant
     * @param Question $question
     * @param array $answer_data
     * @return Answer
     * @throws AnswerMissingPropertyException
     */
    public static function create(Participant $participant, Question $question, array $answer_data) {

        if (!array_key_exists('choices', $answer_data))
            throw new AnswerMissingPropertyException('choices', $answer_data);

        $answer = new self();

        $answer->Participant = $participant;
        $answer->Question = $question;

        //Dealing with choices
        AnswerUtil::validateChoices($participant, $question, $answer_data, array());

        //We complete choices (with negative choices)
        $answer_data = AnswerUtil::completeChoices($question, $answer_data);

        $answer->choices = array();
        foreach ($answer_data['choices'] as $choice_data) {
            $answer->choices[] = new Choice($answer, (array)$choice_data);
        }

        if (array_key_exists('comment', $answer_data) && $answer_data['comment'] !== '') {
            $answer->comment = $answer_data['comment'];
        }

        //Saving answer
        $answer->save();

        //Updating participant info
        $answer->Participant->answers_created = time();
        $answer->Participant->save();

        return $answer;
    }

    /**
     * Casts an answer to an array
     * @param Answer $answer
     * @param bool $with_comment
     * @param array $current_user
     * @return array
     * @internal param array $current_user
     */
    public static function cast(Answer $answer, $with_comment = false, array $current_user = array('role' => SurveyUserRoles::NONE)) {

        $has_comment = $answer->comment
            && (!$answer->hide_comment || (isset($current_user['role']) && SurveyUserRoles::OWNER === $current_user['role']))
            && (!$answer->Question->Survey->hide_comments || (isset($current_user['role']) && SurveyUserRoles::OWNER === $current_user['role']));

        $casted = array(
            'answer_id' => $answer->id,
            'question_id' => $answer->Question->id,
            'participant' => Participant::cast($answer->Participant),
            'choices' => $answer->choices,
            'has_comment' => $has_comment
        );

        if($has_comment) {
            $casted['comment_masked'] = $answer->hide_comment;
        }

        if($with_comment)
            $casted['comment'] = $answer->comment;

        return $casted;
    }

    /**
     * Fills Entity object from DB $data
     * @param $data
     * @return Answer
     * @throws ModelViolationException
     * @throws NotFoundException
     */
    public static function fillFromEntityIteratorDBData($data) {
        return new self(null, $data);
    }

    /**
     * Saving answer
     */
    public function save() {
        if ($this->storedInDatabase) {
            EventLog::push($this, EventLogTypes::UPDATED);
            parent::save();
        } else {
            parent::save();
            EventLog::push($this, EventLogTypes::CREATED);
        }

    }

    /**
     * Deleting answer
     */
    public function delete() {
        EventLog::push($this, EventLogTypes::DELETED);
        parent::delete();
    }

    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return property value
     */
    public function __get($property) {
        if (in_array($property, array(
            'id', 'comment', 'hide_comment'
        ))) {
            return $this->$property;
        }

        if ($property === 'choices') {
            if (is_null($this->sorted_choices)) {
                $this->sorted_choices = array();
                foreach ($this->choices as $choice_data) {
                    if ($choice_data instanceof Choice) {
                        $proposition_id = $choice_data->proposition_id;
                        $choice = $choice_data;
                    } else {
                        $proposition_id = $choice_data['proposition_id'];
                        $choice = new Choice($this, $choice_data);
                    }

                    $this->sorted_choices[$proposition_id] = $choice;
                }
                $this->choices = null;
            }
            return $this->sorted_choices;
        }

        if ($property === 'labels') {
            return $this->getLabels();
        }

        if ($property === 'raw_labels') {
            return $this->getLabels(true, true);
        }

        return parent::__get($property);
    }

    public function update(array $answer_data) {
        if (!array_key_exists('choices', $answer_data))
            throw new AnswerMissingPropertyException('choices', $answer_data);

        //Dealing with choices
        AnswerUtil::validateChoices($this->Participant, $this->Question, $answer_data, $this->__get('choices'));

        //We complete choices
        $answer_data = AnswerUtil::completeChoices($this->Question, $answer_data);

        $this->choices = array();
        foreach ($answer_data['choices'] as $choice_data) {
            $this->choices[] = new Choice($this, (array)$choice_data);
        }

        if (array_key_exists('comment', $answer_data)) {
            $this->comment = ($answer_data['comment'] !== '') ? $answer_data['comment'] : null;
        }

        //Updating update date of update
        $this->Participant->answers_updated = time();
        $this->Participant->save();

        //Saving answer
        $this->save();

        return $this;
    }

    /**
     * Used to prevent showing comment
     */
    public function maskComment() {
        $this->hide_comment = true;

        $this->save();
    }

    /**
     * Used to enable showing comment
     */
    public function unmaskComment() {
        $this->hide_comment = false;

        $this->save();
    }

    /**
     * Removes a choice
     *
     * TODO optimize
     * @param Choice $choice
     */
    public function removeChoice(Choice $choice_to_delete) {
        $new_choices = array();
        foreach ($this->__get('choices') as $choice) {
            if ($choice_to_delete->proposition_id === $choice->proposition_id)
                continue;

            $new_choices[] = $choice;
        }

        $this->choices = $new_choices;

        $this->save();
    }

    /**
     * Removes all choices
     *
     * TODO optimize
     * @param Choice $choice
     */
    public function removeAllChoices() {
        $this->choices = array();
        $this->save();
    }

    /**
     * Get value for a specified proposition id
     * @param $proposition_id
     * @return string
     */
    public function getValue($proposition_id) {
        $choices = $this->__get('choices');
        return isset($choices[$proposition_id]) ?
            $choices[$proposition_id]->value
            : self::UNDEFINED;
    }

    /**
     * Get value for a specified proposition id
     * @param $proposition_id
     * @return string
     */
    public function getLabels($as_string = true, $raw = false) {
        $label = array();
        foreach ($this->__get('choices') as $choice) {
            if (YesNoMaybeValues::YES === $choice->value) {
                $label[] = ($raw) ? $choice->proposition->raw_label : $choice->proposition->label;
            }
        }

        return ($as_string) ? implode(',', $label) : $label;
    }
}
