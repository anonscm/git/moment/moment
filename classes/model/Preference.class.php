<?php

/**
 *     Moment - Preference.class.php
 *
 * Copyright (C) 2024  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Represents a Preference in database
 *
 */
class Preference extends Entity {

    /**
     * Stores already seen Motds
     * @var array
     */
    protected static $dataMap = array(
        'id' => array(//represent id of calendar
            'type' => 'uint',
            'size' => 'medium',
            'primary' => true,
            'autoinc' => true
        ),
        'name' => array(
            'type' => 'string',
            'size' => 50,
            'null' => true,
            'default' => null
        ),
        'value' => array(
            'type' => 'string',
            'size' => 100,
            'null' => true,
            'default' => null
        ),
    );

    /**
     * Relation
     */
    protected static $hasOne = array('User');

    protected $id = null;
    protected $name = null;
    protected $value = null;

    /**
     * Getters
     * @param string $property
     * @return mixed
     */
    public function __get($property) {
        if (in_array($property, array(
            'id', 'name'
        ))) {
            return $this->$property;
        }
        if ($property === 'value') {
            return UserPreferences::castValue($this->name, $this->$property);
        }

        return parent::__get($property);
    }

    /**
     * Setters
     * @param string $property
     * @param string $value
     *
     * @throws ModelViolationException
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if ('value' === $property) {
            $this->$property = $value;
        } else {
            parent::__set($property, $value);
        }
    }

    public static function create(User $user, string $name, string $value) {

        $pref = new self();
        $pref->User = $user;

        $pref->name = $name;
        $pref->value = $value;

        $pref->save();

        return $pref;
    }

}
