<?php

/**
 *     Moment - User.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Represents an user in database
 *
 * Copy this file as User.class.php if you want to get minimal behaviour,
 * if you want to extend the user model see User.sample.class.php instead
 *
 * @property string $last_motd_time;
 * @property string $calendar_hash = null;
 * @property Calendar[] $Calendars
 */
class User extends UserBase {

    /**
     * Calendar hash length
     */
    const CALENDAR_HASH_LENGTH = 32;

    /**
     * Stores already seen Motds
     * @var array
     */
    protected static $dataMap = array(
        'last_motd_time' => array(
            'type' => 'string',
            'size' => 255,
            'null' => true,
            'default' => null
        ),
        'calendar_hash' => array(
            'type' => 'string',
            'size' => self::CALENDAR_HASH_LENGTH,
            'null' => true,
            'default' => null,
            'index' => true,
            'unique' => true
        ),
        'first_email' => array(
            'type' => 'string',
            'size' => 255,
            'null' => true,
            'default' => null,
            'index' => true
        )
    );

    /**
     * Relation
     */
    protected static $hasMany = array('Calendar', 'Preference');

    protected ?string $last_motd_time = null;
    protected ?string $calendar_hash = null;
    protected ?string $first_email = null;

    /**
     * Get a user by its calendar_hash
     * @param $hash the calendar_hash
     * @return User the requested user if found
     */
    public static function fromCalendarHash($hash) {
        $users = self::all('calendar_hash = :hash', array(':hash' => $hash));
        if($users) {
            //There must be only one user with this calendar_hash
            return array_shift($users);
        }
        return false;
    }

    /**
     * Get a users by its email sorted by last activity desc (most recent first)
     * @param $email email of the users
     * @return User[] the requested users if found
     */
    public static function fromUserEmail($email) {
        return self::all('first_email = :email ORDER BY last_activity DESC', array(':email' => $email));
    }

    /**
     * Saving answer
     */
    public function save() {
        $this->first_email = $this->email;
        parent::save();
    }

    /**
     * Allows to get email sender
     * @return string
     */
    public function getEmailSender() {
        return $this->email;
    }

    /**
     * Getter
     *
     * @param string $property
     * @return null|property
     * @throws KeyGeneratorTriedTooMuchException
     * @throws PropertyAccessException
     */
    public function __get($property) {
        if ($property === 'last_motd_time'){
            return $this->last_motd_time;
        }
        if ($property === 'calendar_hash'){
            if(!isset($this->calendar_hash)) {
                $this->refreshCalendarHash();
            }
            return $this->calendar_hash;
        }
        if ($property === 'first_email'){
            return $this->first_email;
        }

        return parent::__get($property);
    }

    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws BadLangCodeException
     * @throws ModelViolationException
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {

        if ($property === 'last_motd_time') {
            $this->last_motd_time = $value;
        } else if ($property == 'auth_secret') {
            $this->$property = $value;
        } else if ($property == 'calendar_hash') {
            $this->$property = $value;
        } else if ($property == 'first_email') {
            $this->$property = $value;
        } else {
            parent::__set($property, $value);
        }
    }

    /**
     * Delete associated Calendars, Preferences
     */
    public function beforeDelete() {
        foreach ($this->Calendars as $calendar) {
            $calendar->delete();
        }

        foreach ($this->Preferences as $preference) {
            $preference->delete();
        }
    }

    /**
     * Get user value for preference
     * @param $name
     * @return Preference|null
     */
    public function getPreference($name) {
        foreach($this->Preferences as $preference) {
            if($preference->name === $name)
                return $preference;
        }
        return null;
    }

    /**
     * Get user value for preference
     * @param $name
     * @return mixed
     */
    public function getPreferenceValue($name) {
        $pref = $this->getPreference($name);
        return $pref ? $pref->value : null;
    }

    /**
     * Refreshes users calendar hash
     * @throws KeyGeneratorTriedTooMuchException
     */
    private function refreshCalendarHash() {
        $this->calendar_hash = KeyGenerator::generateKey(self::CALENDAR_HASH_LENGTH, function ($calendar_hash) {
            $check = DBI::prepare('SELECT calendar_hash FROM ' . self::getDBTable() . ' WHERE calendar_hash = :calendar_hash');
            $check->execute(array(':calendar_hash' => $calendar_hash));
            return !$check->fetch();
        });
        $this->save();
    }

}
