<?php

/**
 *     Moment - Question.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class Question
 *
 * @property-read QuestionAnswerTypes $answer_type
 */
class Question extends Entity {

    /**
     * Database map
     */
    protected static $dataMap = array(
        'id' => array(
            'type' => 'uint',
            'size' => 'medium',
            'primary' => true,
            'autoinc' => true
        ),
        'title' => array(
            'type' => 'string',
            'size' => 255,
        ),
        'type' => array(
            'type' => 'string',
            'size' => 4,
        ),
        'position' => array(
            'type' => 'uint',
            'size' => 'tiny'
        ),
        'options' => array(
            'type' => 'text',
            'default' => '[]',
            'transform' => 'json'
        )
    );

    /**
     * Relation
     */
    protected static $hasMany = array('TextProposition', 'DateProposition', 'Answer');

    protected static $hasOne = array('Survey');

    /**
     * Properties
     */
    protected $id = null;
    protected $title = '';
    protected $type = '';
    protected $position = 0;
    protected $options = array();
    protected $proposition_type = null;

    /**
     * Internals used for caching resource (CPU) consuming value
     */
    private $sorted_propositions = null;
    private $grouped_propositions = null;
    private $filtered_answers = null;
    private $has_maybe_answers = false;


    /**
     * Create a new question
     *
     * @param Survey $survey
     * @param array $data
     * @return Question
     * @throws QuestionBadTypeException
     * @throws QuestionMissingPropertyException
     * @throws QuestionSurveyNotSavedException
     * @throws QuestionBadPropertyException
     */
    public static function create(Survey $survey, array $data) {

        $mandatoryFields = array(
            'type', 'position', 'options', 'propositions'
        );

        foreach ($mandatoryFields as $field) {
            if (!array_key_exists($field, $data))
                throw new QuestionMissingPropertyException($field, $data);
        }

        // Check validity
        if (!QuestionTypes::isValid($data['type']))
            throw new QuestionBadTypeException($data['type']);

        //Check if survey has an id
        if (!$survey->__get('storedInDatabase'))
            throw new QuestionSurveyNotSavedException();

        $question = new self();
        $question->Survey = $survey;

        $question->title = substr($data['title'], 0, self::$dataMap['title']['size']);
        $question->type = $data['type'];
        $question->position = $data['position'];

        //Check options
        QuestionSettings::validateSettings((array)$data['options']);
        $question->options = $data['options'];

        //Dealing with propositions (We need at least one proposition)
        if (!is_array($data['propositions'])
            || (!$survey->is_draft && empty($data['propositions']))
        )
            throw new QuestionBadPropertyException('propositions', $data);

        //Saving question
        $question->save();

        $proposition_class = self::getPropositionClass($question);
        //The proposition position in array is the position it will be stored.
        //-> Only works for text proposition
        $position = 0;

        //We group propositions before (in case someone has set the same header twice)
        $proposition_groups = self::groupPropositions($question->type, $data['propositions']);
        foreach ($proposition_groups as $groupName => $groupMembers) {
            foreach ($groupMembers as $proposition_data) {
                $proposition_data->position = $position;
                $proposition_class::create($question, (array)$proposition_data);
                $position++;
            }
        }

        return $question;
    }

    /**
     * Get all Questions from Survey
     *
     * @param Survey $survey the survey
     *
     * @return array
     */
    public static function fromSurvey(Survey $survey) {
        return self::all('survey_id = :survey_id', array(':survey_id' => $survey->id));
    }

    /**
     * Return proposition classname associated with the question type
     * @param Question $question the question
     * @return type
     */
    public static function getPropositionClass(Question $question) {
        //Two known types of question text/date, there are two types of proposition
        return (QuestionTypes::DATE == $question->type) ? 'DateProposition' : 'TextProposition';
    }

    /**
     * Casts a question into to an array for response
     *
     * @param Question $question
     * @return array
     * @throws ModelViolationException
     */
    public static function cast(Question $question, $group_propositions = true) {


        $data = array(
            'id' => $question->id,
            'title' => $question->title,
            'type' => $question->type,
            'proposition_type' => $question->__get('proposition_type'),
            'position' => $question->position,
            'options' => $question->options,
            'propositions' => array()
        );

        //We get all propositions
        //Then we cast every proposition to array
        $proposition_class = Question::getPropositionClass($question);

        if ($group_propositions) {
            $propositions = $question->__get('grouped_propositions');

            foreach ($propositions as $header => $members) {
                foreach ($members as $proposition) {
                    $data['propositions'][$header][] = $proposition_class::cast($proposition);
                }
            }
        } else {
            $propositions = $question->__get('sorted_propositions');

            foreach ($propositions as $proposition) {
                $data['propositions'][] = $proposition_class::cast($proposition);
            }
        }

        if (!$question->Survey->is_draft && empty($propositions))
            throw new ModelViolationException('There exists a question without proposition');

        return $data;
    }

    /**
     * Compare two questions  by its positions
     * @param mixed $question_a
     * @param mixed $question_b
     * @return int
     */
    public static function compare($question_a, $question_b) {
        $position_a = 0;
        $position_b = 0;
        if (is_array($question_a)) {
            $position_a = isset($question_a['position']) ? $question_a['position'] : 0;
        } else if ($question_a instanceof Question) {
            $position_a = $question_a->position;
        }

        if (is_array($question_b)) {
            $position_b = isset($question_b['position']) ? $question_b['position'] : 0;
        } else if ($question_b instanceof Question) {
            $position_b = $question_b->position;
        }

        //Same position then 0
        if($position_b === $position_a) {
            return 0;
        }

        return ($position_b < $position_a) ? 1 : -1;
    }

    /**
     * Group proposition by common header
     * @param $question_type
     * @param array $propositions
     * @return array
     */
    public static function groupPropositions($question_type, array $propositions = array()) {
        //Grouping proposition by base_day if
        $proposition_groups = array();
        if ($question_type == QuestionTypes::DATE) {
            foreach ($propositions as $proposition) {
                $proposition = (object)$proposition;
                $propositionType = DateProposition::getType($proposition);

                // All Surveys updated before fix must be shown as before fix (grouped server-side vs client-side)
                if(isset($proposition->Question) && Config::get('timezone_pivot') < $proposition->Question->Survey->updated) {
                    //Date proposition are not grouped server-side
                    $groupName = $propositionType . '_' . $proposition->id;
                } else {
                    $groupName = (PropositionTypes::RANGE_OF_DAYS === $propositionType) ?
                        $proposition->base_day . '_' . $propositionType . '_' . $proposition->end_day
                        : $proposition->base_day . '_' . $propositionType;
                }

                if (!isset($proposition_groups[$groupName]))
                    $proposition_groups[$groupName] = array();

                if (PropositionTypes::DAY === $propositionType) {
                    //Not two proposition DAY for the same date
                    if (count($proposition_groups[$groupName]) === 0) {
                        $proposition_groups[$groupName][] = $proposition;
                    }
                } else {
                    $proposition_groups[$groupName][] = $proposition;
                }
            }

        } else if ($question_type == QuestionTypes::TEXT) {
            foreach ($propositions as $proposition) {
                $proposition = (object)$proposition;
                if (!isset($proposition_groups[$proposition->header]))
                    $proposition_groups[$proposition->header] = array();

                $proposition_groups[$proposition->header][] = $proposition;
            }
        }
        return $proposition_groups;
    }

    /**
     * Delete associated propositions
     */
    public function beforeDelete() {

        $proposition_class = self::getPropositionClass($this);
        foreach ($this->getRelated($proposition_class) as $proposition) {
            $proposition->delete();
        }

        //Delete related answers
        foreach ($this->Answers as $answer) {
            $answer->delete();
        }

    }

    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return mixed value
     */
    public function __get($property) {
        if (in_array($property, array(
            'title', 'id', 'type', 'position', 'options', 'has_maybe_answers'
        ))) {
            return $this->$property;
        }

        // Statement is an always set title for the question, title is not mandatory but there is always a statement (e.g. "Question n")
        if('statement' == $property) {
            return (is_null($this->title) || '' == $this->title)?Lang::tr('question').' '.($this->position + 1):$this->title;
        }

        //Computing answer type
        if ($property === 'answer_type') {
            if (!$this->options[QuestionSettings::FORCE_UNIQUE_CHOICE]) {
                if ($this->options[QuestionSettings::ENABLE_MAYBE_CHOICES]) {
                    return QuestionAnswerTypes::YESNOMAYBE;
                } else {
                    return QuestionAnswerTypes::MULTIPLE;
                }
            } else {
                return QuestionAnswerTypes::UNIQUE;
            }
        }

        if ($property === 'proposition_type') {
            if(isset($this->proposition_type))
                return $this->proposition_type;
            //Collecting all proposition types
            $propositionTypes = [];
            foreach ($this->propositions as $proposition) {
                $propositionTypes[$proposition->type] = true;
            }

            //Default proposition type should match question type
            $this->proposition_type = (QuestionTypes::DATE == $this->type)?PropositionTypes::DAY:PropositionTypes::TEXT;

            //Only one proposition type
            if(1 === count($propositionTypes)) {
                $this->proposition_type = array_key_first($propositionTypes);
            } else if(2 == count($propositionTypes)
                && isset($propositionTypes[PropositionTypes::DAY])
                && isset($propositionTypes[PropositionTypes::RANGE_OF_DAYS])) {
                //DATE and RANGE_OF_DAY only => it roughly is a RANGE_OF_DAY
                $this->proposition_type = PropositionTypes::RANGE_OF_DAYS;
            } else if(isset($propositionTypes[PropositionTypes::RANGE_OF_DAYS]) || QuestionTypes::TEXT == $this->type) {
                //There is at list one RANGE_OF_DAY and other types => it's a MIXED (same if a Text question has more than one proposition type)
                $this->proposition_type = PropositionTypes::MIXED;
            }
            return $this->proposition_type;
        }

        if ($property === 'propositions') {
            $proposition_class = self::getPropositionClass($this);

            return $proposition_class::fromQuestion($this);
        }

        // Sorted proposition chronolically (for DATE) or position (for TEXT)
        if ($property === 'sorted_propositions') {

            //Caching sorting
            if (!isset($this->sorted_propositions)) {
                $propositions = $this->propositions;
                //Sort propositions if possible
                $proposition_class = Question::getPropositionClass($this);
                if (!empty($propositions) && method_exists($proposition_class, 'compare'))
                    usort($propositions, array($proposition_class, 'compare'));

                $this->sorted_propositions = $propositions;
            }

            return $this->sorted_propositions;
        }

        //Grouped propositions by header
        if ($property === 'grouped_propositions') {
            //Caching grouping
            if (!isset($this->grouped_propositions)) {
                $this->grouped_propositions = self::groupPropositions($this->type, $this->__get('sorted_propositions'));
            }
            return $this->grouped_propositions;
        }

        //Answers We only get declarative and authenticated answers
        if (strtolower($property) === 'answers') {

            //Caching filtering
            if (!isset($this->filtered_answers)) {
                $answers = parent::__get($property);
                $this->filtered_answers = array();
                //We use key first
                $authorized_types = array(ParticipantTypes::AUTHENTICATED => true, ParticipantTypes::DECLARATIVE => true);
                foreach ($answers as $answer) {
                    if (isset($authorized_types[$answer->Participant->type]))
                        $this->filtered_answers[] = $answer;
                }
            }
            return $this->filtered_answers;
        }

        //Counting Answers We only get declarative and authenticated answers
        if (strtolower($property) === 'answers_count') {
            //No need to instantiate participants nor answers for it
            $check = DBI::prepare('SELECT count(*) AS count'
                . ' FROM ' . Answer::getDBTable()
                . ' WHERE question_id = :question_id AND participant_type != :participant_type;');
            $check->execute(array(':question_id' => $this->id, ':participant_type' => ParticipantTypes::ULTIMATE));

            return $check->fetchColumn(0);
        }

        //If setting is defined (For direct access to options)
        $options = (array)$this->options;

        if (isset($options[$property])) {
            return $options[$property];
        }

        if (strtolower($property) === 'input_mode') {
            return false;
        }

        return parent::__get($property);
    }

    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if (in_array($property, array(
            'id', 'title', 'type', 'position', 'options', 'has_maybe_answers'
        ))) {
            $this->$property = $value;
        } else {
            parent::__set($property, $value);
        }

    }

    /**
     * Updates a question
     *
     * @param Survey $survey
     * @param array $data
     * @throws QuestionBadPropertyException
     * @throws QuestionCrossAccessException
     * @throws QuestionMissingPropertyException
     */
    public function update(Survey $survey, array $data) {

        if ($survey->id !== $this->Survey->id)
            throw new QuestionCrossAccessException();

        $mandatoryFields = array(
            'title', 'options', 'position'
        );

        foreach ($mandatoryFields as $field) {
            if (!array_key_exists($field, $data))
                throw new QuestionMissingPropertyException($field, $data);
        }

        $this->title = substr($data['title'], 0, self::$dataMap['title']['size']);
        $this->position = $data['position'];

        //Check options
        QuestionSettings::validateSettings((array)$data['options']);

        //Changing from multiple to unique choice (We should clear invalid former answers)
        if(QuestionAnswerTypes::UNIQUE != $this->answer_type && $data['options']->force_unique_choice) {
            $this->removeNonUniqueChoice();
        }

        $this->options = $data['options'];

        //Saving question
        $this->save();

        $proposition_class = self::getPropositionClass($this);
        $proposition_class_plural = $proposition_class . 's';
        //Check propositions (mandatory)
        if (!array_key_exists('propositions', $data))
            throw new QuestionMissingPropertyException('propositions', $data);

        //At least one proposition
        if (!is_array($data['propositions'])
            || (!$survey->is_draft && empty($data['propositions']))
        )
            throw new QuestionBadPropertyException('propositions', $data);

        $proposition_ids = array();
        //The proposition position in array is the position it will be stored.
        //-> Only works for text proposition
        $position = 0;

        $proposition_groups = self::groupPropositions($this->type, $data['propositions']);
        foreach ($proposition_groups as $groupName => $groupMembers) {
            foreach ($groupMembers as $proposition_data) {
                if (is_object($proposition_data))
                    $proposition_data = (array)$proposition_data;

                //Setting position
                $proposition_data['position'] = $position;
                $position++;

                //CREATE
                if (!isset($proposition_data['id'])) {
                    //We create a new proposition only if no exact same proposition exists
                    $new_proposition = $proposition_class::createIfNotExists($this, $proposition_data);
                    $proposition_ids[] = $new_proposition->id;
                } else {

                    try {
                        //Get Proposition
                        $proposition = $proposition_class::fromId($proposition_data['id']);
                        $proposition->update($this, $proposition_data);

                    } catch(NotFoundException $nfe) {
                        //Proposition id does not match an existing proposition
                        $proposition = $proposition_class::create($this, $proposition_data);
                    }

                    $proposition_ids[] = $proposition->id;
                }
            }
        }

        //DELETE
        foreach ($this->$proposition_class_plural as $proposition) {
            //The id is not present in the new proposition list -> we must delete proposition
            if (!in_array($proposition->id, $proposition_ids)) {
                //Droping relation
                $this->dropRelated($proposition);
                //Drop entity
                $proposition->delete();
            }
        }
    }

    /**
     * Apply constraints on question
     */
    public function applyConstraints() {
        foreach ($this->propositions as $proposition) {
            if (method_exists($proposition, 'applyConstraints')) {
                $proposition->applyConstraints();
            }
        }
    }

    /**
     * Apply constraints on question
     */
    public function hasConstraints($type = 'limit_choice'): bool {
        foreach ($this->propositions as $proposition) {
            if(isset($proposition->options['constraints'])) {
                foreach ($proposition->options['constraints'] as $constraint) {
                    if($constraint['type'] === $type)
                        return true;
                }
            }
        }
        return false;
    }

    /**
     * Removes non unique choices from answers for this question
     * @throws Exception
     */
    private function removeNonUniqueChoice() {
        //Iterating over question Answers
        $answersIterator = new EntityIterator(
            Answer::name(),
            'question_id = :question_id AND participant_type != :participant_type',
            array(':question_id' => $this->id, ':participant_type' => ParticipantTypes::ULTIMATE)
        );

        foreach($answersIterator as $answer) {
            $choices_to_delete = array();
            $yes_count = 0;
            foreach($answer->choices as $choice) {
                $yes_count += (YesNoMaybeValues::NO != $choice->value)?1:0;
                if($yes_count > 1) {
                    $answer->removeAllChoices();
                    break;
                }
            }
        }
    }
}
