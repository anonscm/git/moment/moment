<?php

/**
 *     Moment - Survey.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Require environment (fatal)
if (!defined('EKKO_ROOT'))
    die('Missing environment');

/**
 * Class Survey
 *
 * @property-read Participant $ultimate_participant
 * @property-read boolean $is_closed
 */
class Survey extends Entity {

    /**
     * Survey UID length
     */
    const UID_LENGTH = 8; // Setting the length to 6 allows for 2176782336 simultaneous surveys (n_max = 36^len)

    /**
     * Survey rights
     */
    const OPENED_TO_EVERYONE = 'opened_to_everyone';
    const OPENED_TO_GUESTS = 'opened_to_guests';
    const OPENED_TO_AUTHENTICATED = 'opened_to_authenticated';
    const OPENED_TO_NOONE = 'opened_to_noone';

    /**
     * Answer access
     */
    const ANSWER_VIEW_ACCESS_REPLIERS = 'answer_access_repliers';
    const ANSWER_VIEW_ACCESS_REPLIERS_WITH_ANSWERS = 'answer_access_repliers_with_answers';
    const ANSWER_VIEW_ACCESS_OWNER = 'answer_access_owner';

    /**
     * Comment access
     */
    const COMMENT_VIEW_ACCESS_REPLIERS = 'comment_access_repliers';
    const COMMENT_VIEW_ACCESS_REPLIERS_WITH_ANSWERS = 'comment_access_repliers_with_answers';
    const COMMENT_VIEW_ACCESS_OWNER = 'comment_access_owner';

    /**
     * DataTable name is overloaded because of the plural mechanism which transforms Surveys to Surveies
     */
    protected static $dataTable = 'Surveys';

    /**
     * Database map
     */
    protected static $dataMap = array(
        'id' => array(
            'type' => 'string',
            'size' => self::UID_LENGTH,
            'primary' => true
        ),
        'title' => array(
            'type' => 'string',
            'size' => 255,
        ),
        'place' => array(
            'type' => 'string',
            'size' => 255,
            'null' => true
        ),
        'description' => array(
            'type' => 'text',
            'null' => true
        ),
        'is_draft' => array(
            'type' => 'boolean',
            'default' => 0
        ),
        'created' => array(
            'type' => 'datetime',
            'null' => true
        ),
        'updated' => array(
            'type' => 'datetime',
            'null' => true
        ),
        'closed' => array(
            'type' => 'datetime',
            'null' => true
        ),
        'reopened' => array(
            'type' => 'datetime',
            'null' => true
        ),
        'settings' => array(
            'type' => 'text',
            'default' => '[]',
            'transform' => 'json'
        )
    );

    /**
     * Relation
     */
    protected static $hasMany = array('Question', 'Participant');


    /**
     * Properties
     */
    protected $id = null;
    protected $title = '';
    protected $place = null;
    protected $description = null;
    protected $is_draft = 0;
    protected $created = null;
    protected $updated = null;
    protected $closed = null;
    protected $reopened = null;
    protected $settings = array();

    /**
     * Internals
     */
    private $ultimate_participant = null;


    /**
     * Creates a survey from input data
     *
     *
     * @param array $data
     * @return static
     * @throws Exception
     * @throws SurveyMissingPropertyException
     */
    public static function create(array $data) {

        if (!array_key_exists('title', $data)
            || !$data['title']
        )
            throw new SurveyMissingPropertyException('title', $data);

        $publicFields = array(
            'title', 'place', 'description', 'settings', 'is_draft'
        );

        //Shortening title size to max size
        $data['title'] = substr($data['title'], 0, self::$dataMap['title']['size']);

        //Validating settings
        SurveySettings::validateSettings($data['settings']);

        $survey = new self();

        //Generation of the unique id referencing the survey
        $survey->id = self::generateUID();
        $survey->created = time();
        $user = Auth::user();

        foreach ($publicFields as $field) {
            if (array_key_exists($field, $data)) {
                $survey->__set($field, $data[$field]);
            }
        }

        $survey->save();

        try {
            //Creating owner
            Owner::create($survey, $user);

            //Check other owners (optionnal)
            if (array_key_exists('owners', $data) && $data['owners']) {
                if (!is_array($data['owners']))
                    throw new SurveyBadPropertyException('owners', $data);

                foreach ($data['owners'] as $owner_data) {
                    if (is_object($owner_data))
                        $owner_data = (array)$owner_data;

                    //CREATE
                    if (array_key_exists('email', $owner_data) && !Owner::exists($owner_data['email'], $survey)) {
                        Owner::create($survey, $owner_data);
                    }
                }
            }

            //Check guests (optionnal)
            if (array_key_exists('guests', $data) && $data['guests']) {
                if (!is_array($data['guests']))
                    throw new SurveyBadPropertyException('guests', $data);

                $addedGuests = array();
                foreach ($data['guests'] as $guest) {
                    if (is_object($guest))
                        $guest = (array)$guest;

                    //Only add guest once
                    if(isset($addedGuests[$guest['email']]))
                        continue;

                    Guest::create($survey, $guest['email']);

                    $addedGuests[$guest['email']] = true;
                }
            }

            //Check questions (mandatory except for drafts)
            if (!$survey->is_draft && !array_key_exists('questions', $data))
                throw new SurveyMissingPropertyException('questions', $data);

            //At least one question
            if (!is_array($data['questions'])
                || (!$survey->is_draft && empty($data['questions']))
            )
                throw new SurveyBadPropertyException('questions', $data);

            //TODO check if more than one question if they all have a title

            foreach ($data['questions'] as $question) {
                Question::create($survey, (array)$question);
            }

            //No notification when draft
            if (!$survey->is_draft) {
                //Sending notification to owner
                MomentApplicationMail::prepareCreationEmail($survey)->send();

                //Sening invitation to guest
                $survey->sendInvitations();
            }

        } catch (Exception $e) {

            //Integrity for draft is not mandatory
            if (!isset($data['is_draft']) || !$data['is_draft']) {
                //Silent delete
                $survey->delete(false);

                throw $e;
            }
        }
        // Survey is created/updated => corresponding events cache must be cleared
        $survey->clearUsersIcsCache();

        return $survey;
    }

    /**
     * Generate a new survey id
     *
     * @return string
     *
     * @throws SurveyUIDGeneratorTriedTooMuchException
     */
    private static function generateUID() {
        return KeyGenerator::generateKey(self::UID_LENGTH, function ($uid) {
            $check = DBI::prepare('SELECT id FROM ' . self::getDBTable() . ' WHERE id = :id');
            $check->execute(array(':id' => $uid));
            return !$check->fetch();
        });
    }

    /**
     * Get all surveys (Can only be used by admin)
     * @return array
     * @throws AuthUserNotAllowedException
     */
    public static function allSurveys() {
        if (!Auth::isAdmin())
            throw new AuthUserNotAllowedException();

        return self::all('1 ORDER BY updated DESC');
    }

    /**
     * Get surveys by owner
     * @param User $user
     * @return array
     */
    public static function ownedBy(User $user) {
        $surveys = array();
        //Getting all owner entities with the corresponding email
        foreach (Owner::fromEmail($user->email) as $owner) {
            $surveys[] = $owner->Survey;
        }

        //Sorting by create date manually
        usort($surveys, array('Survey', 'compare'));

        return $surveys;
    }

    /**
     * Get survey from "human readable id"
     * @param string $human_readable_id the id
     * @return Survey
     * @throws NotFoundException
     */
    public static function fromHumanReadableId($human_readable_id) {


        $id = $human_readable_id;

        if (strrpos($human_readable_id, '-') !== false) {
            //We have to get the internal id
            $id = substr($human_readable_id, strrpos($human_readable_id, '-') + 1);
        }

        if (KeyGenerator::validateKey($id, self::UID_LENGTH)) {
            //No check on title (if id is good we give access to the survey)
            return self::fromId($id);

        }
        throw new NotFoundException(static::name(), $id);
    }

    /**
     * Get Surveys where user is invited
     * @param User $user
     *
     * @return array $surveys
     */
    public static function fromGuestUser(User $user) {
        $surveys = array();
        //Getting all guest entities with the corresponding email
        foreach (Guest::fromEmail($user->email) as $guest) {
            $surveys[] = $guest->Survey;
        }

        //Sorting by create date manually
        usort($surveys, array('Survey', 'compare'));

        return $surveys;
    }

    /**
     * Get Surveys where user has answered
     * @param User $user
     * @return array
     */
    public static function answeredBy(User $user) {
        $surveys = array();

        $participants = Participant::fromAuthenticatedUser($user);
        //Sorting participant from newer to oldest
        usort($participants, function ($participant_a, $participant_b) {
            $last_activity_a = max($participant_a->answers_updated, $participant_a->answers_created);
            $last_activity_b = max($participant_b->answers_updated, $participant_b->answers_created);
            return $last_activity_b > $last_activity_a;
        });

        //Getting all participants with the corresponding email
        foreach ($participants as $participant) {
            if ($participant->answers_created)
                $surveys[] = $participant->Survey;
        }

        return $surveys;
    }

    /**
     * Compare two surveys by date
     * @param mixed $survey_a
     * @param mixed $survey_b
     * @return bool
     */
    public static function compare($survey_a, $survey_b) {
        $ref_date_a = isset($survey_a->updated) ? $survey_a->updated : $survey_a->created;
        $ref_date_b = isset($survey_b->updated) ? $survey_b->updated : $survey_b->created;
        return ($ref_date_a < $ref_date_b) ? 1 : -1;
    }

    /**
     * Delete associated participants, guests and questions
     */
    public function beforeDelete() {

        foreach ($this->Owners as $owner) {
            $owner->delete();
        }

        foreach ($this->Participants as $participant) {
            $participant->delete();
        }

        foreach ($this->Guests as $guest) {
            $guest->delete();
        }

        foreach ($this->Questions as $question) {
            $question->delete();
        }
    }

    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return mixed property value
     */
    public function __get($property) {
        if (in_array($property, array(
            'id', 'title', 'place',
            'created', 'updated', 'closed', 'reopened', 'settings', 'is_draft'
        ))) {
            return $this->$property;
        }

        //Everytime we need survey question the must be sorted by position
        if (strtolower($property) === 'questions') {
            //Sort questions if possible
            $questions = parent::__get('Questions');
            usort($questions, array(Question::name(), 'compare'));
            return $questions;
        }

        if ($property === 'owner_names') {
            $owner_names = array();
            foreach ($this->Owners as $owner) {
                if (Auth::User() && strtolower(Auth::User()->email) == strtolower($owner->email) && $owner->name == '') {
                    $owner->name = Auth::User()->name;
                    $owner->save();
                }

                $owner_names[] = ($owner->name == '') ? $owner->email : $owner->name;
            }
            return implode(", ", $owner_names);
        }

        if ($property === 'owner_emails') {
            $owner_emails = array();
            foreach ($this->Owners as $owner) {
                $owner_emails[] = $owner->email;
            }
            return implode(", ", $owner_emails);
        }

        if ($property === 'path') {
            return Config::get('application_url') . 'survey/' . $this->getHumanReadableId();
        }

        if ($property === 'shortened_path') {
            return Config::get('application_url') . 'survey/' . $this->getHumanReadableId(true);
        }

        if ($property === 'update_path') {
            return Config::get('application_url') . 'survey/update/' . $this->id;
        }

        if ($property === 'results_path') {
            return Config::get('application_url') . 'survey/results/' . $this->id;
        }

        if ($property === 'human_readable_id') {
            return $this->getHumanReadableId();
        }

        if ($property === 'questions_count') {
            //No need to instantiate participants nor answers for it
            $check = DBI::prepare('SELECT count(*) AS count FROM ' . Question::getDBTable() . ' WHERE survey_id = :survey_id');
            $check->execute(array(':survey_id' => $this->id));
            return $check->fetchColumn(0);
        }

        if ($property === 'participants_count') {
            //No need to instantiate participants nor answers for it
            $check = DBI::prepare('SELECT count(*) AS count FROM ' . Participant::getDBTable() . ' WHERE survey_id = :survey_id AND answers_created IS NOT NULL AND type != "' . ParticipantTypes::ULTIMATE . '";');
            $check->execute(array(':survey_id' => $this->id));
            return $check->fetchColumn(0);
        }

        if (strtolower($property) === 'ultimate_participant') {
            if (!isset($this->ultimate_participant)) {
                try {
                    $this->ultimate_participant = Participant::getUltimateParticipant($this, false);
                } catch (ParticipantNotFoundException $nfe) {
                    //No ultimate participant found, not that important at the moment
                    $this->ultimate_participant = false;
                }
            }
            return $this->ultimate_participant;
        }

        if ($property === 'answers') {
            $answers = array();
            foreach ($this->Participants as $participant) {
                if ($participant->Answers)
                    $answers[] = $participant->Answers;
            }
            return $answers;
        }

        if ($property === 'comments') {
            $comments = array();
            foreach ($this->Participants as $participant) {
                if ($participant->Comments)
                    $comments[] = $participant->Comments;
            }
            return $comments;
        }

        //Get an array of results as array (used for export)
        if ($property === 'results') {
            return $this->getResults();
        }

        if ($property === 'is_closed') {
            return $this->isClosed();
        }

        if ($property === 'has_date_questions') {
            foreach ($this->Questions as $question) {
                if (QuestionTypes::DATE === $question->type) {
                    return true;
                }
            }

            return false;
        }

        // If survey needs information about timezone
        if ($property === 'has_timezone_sensible_questions') {
            foreach ($this->Questions as $question) {
                if (QuestionTypes::DATE === $question->type) {
                    $proposition_type = $question->proposition_type;
                    if(PropositionTypes::DAY === $proposition_type
                        || PropositionTypes::DAY_HOUR === $proposition_type
                        || PropositionTypes::RANGE_OF_HOURS === $proposition_type
                        || PropositionTypes::RANGE_OF_DAYS_HOURS === $proposition_type) {
                        return true;
                    }
                }
            }

            return false;
        }

        if ($property === 'parsed_description') {
            require_once EKKO_ROOT . '/lib/markdown/Michelf/MarkdownExtra.inc.php';

            if($this->description === null)
                return '';

            return \Michelf\MarkdownExtra::defaultTransform($this->description);
        }

        if ($property === 'raw_description') {
            return $this->description;
        }

        if ($property === 'description') {
            if($this->description == '' || $this->description === null) {
                return '';
            }
            require_once EKKO_ROOT . '/lib/markdown/Michelf/MarkdownExtra.inc.php';

            return strip_tags(\Michelf\MarkdownExtra::defaultTransform($this->description));
        }

        //If setting is defined
        if (is_array($this->settings)) {
            if (isset($this->settings[$property])) {
                return $this->settings[$property];
            }
        } else {
            if (isset($this->settings->$property)) {
                return $this->settings->$property;
            }
        }

        //If setting is known but not defined
        $surveySettings = SurveySettings::getSurveySettings($this);
        if (array_key_exists($property, $surveySettings)) {
            return $surveySettings[$property]['default'];
        }

        if (strtolower($property) === 'owners') {
            return Owner::fromSurvey($this);
        }

        if (strtolower($property) === 'guests') {
            return Guest::fromSurvey($this);
        }

        return parent::__get($property);
    }

    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if (in_array($property, array(
            'title', 'id', 'place', 'description', 'owner_name', 'owner_email', 'created', 'expires', 'closed', 'reopened', 'guests', 'settings', 'is_draft'
        ))) {
            $this->$property = $value;
        } else {
            parent::__set($property, $value);
        }

    }

    /**
     * Updates a survey from input data
     *
     *
     * @param array $data
     */
    public function update(array $data) {

        //Saving former state
        $was_draft = $this->is_draft;

        $publicFields = array(
            'title', 'place', 'description', 'settings', 'is_draft'
        );

        //Shortening title size to max size
        $data['title'] = substr($data['title'], 0, self::$dataMap['title']['size']);

        if(is_array($data['settings']))
            $data['settings'] = (object) $data['settings'];

        //Validating settings
        SurveySettings::validateSettings($data['settings'], $this);

        $data['settings']->time_zone = $this->time_zone;

        foreach ($publicFields as $field) {
            if (array_key_exists($field, $data)) {
                $this->__set($field, $data[$field]);
            }
        }

        //Check owners
        if (array_key_exists('owners', $data)) {
            if (!is_array($data['owners']))
                throw new SurveyBadPropertyException('owners', $data);

            //If no owner is found => we keep currently authenticated user as owner
            $data_owners = array();
            foreach ($data['owners'] as $owner_data) {
                if (is_object($owner_data))
                    $owner_data = (array)$owner_data;

                if (!isset($owner_data))
                    continue;

                //CREATE
                if (array_key_exists('email', $owner_data)) {
                    if (!Owner::exists($owner_data['email'], $this)) {
                        Owner::create($this, $owner_data);
                    }
                    //No owners update
                    $data_owners[] = $owner_data['email'];
                }
            }

            //DELETE
            foreach ($this->Owners as $owner) {
                //The key is not present in the new owner list -> we must delete guest
                //IMPORTANT : an owner cannot delete himself
                if (!in_array($owner->email, $data_owners) && strtolower($owner->email) != strtolower(Auth::user()->email)) {
                    //Droping relation
                    $this->dropRelated($owner);

                    //Drop entity
                    $owner->delete();
                }
            }
        }

        //Check guests
        if (array_key_exists('guests', $data)) {
            if (!is_array($data['guests']))
                throw new SurveyBadPropertyException('guests', $data);

            $data_guests = array();
            foreach ($data['guests'] as $guest_data) {
                if (is_object($guest_data))
                    $guest_data = (array)$guest_data;

                //CREATE
                if (array_key_exists('email', $guest_data)) {
                    $guest = Guest::fromEmailAndSurvey($guest_data['email'], $this);
                    if (!isset($guest)) {
                        Guest::create($this, $guest_data['email']);
                    } elseif($guest->email != $guest_data['email']) {
                        //There is a change in email (lowercase to uppercase or something like that)
                        //Email being primary we must delete and then re-create
                        $guest->delete();
                        Guest::create($this, $guest_data['email']);
                    }
                    //No guest update
                    $data_guests[] = strtolower($guest_data['email']);
                }
            }
            //DELETE
            foreach ($this->Guests as $guest) {
                //The key is not present in the new guest list -> we must delete guest
                if (!in_array(strtolower($guest->email), $data_guests)) {
                    //Droping relation
                    $this->dropRelated($guest);

                    //Drop entity
                    $guest->delete();
                }
            }

        }

        //Check questions (mandatory)
        if (!$this->is_draft && !array_key_exists('questions', $data))
            throw new SurveyMissingPropertyException('questions', $data);

        //At least one question
        if (!is_array($data['questions'])
            || (!$this->is_draft && empty($data['questions']))
        )
            throw new SurveyBadPropertyException('questions', $data);

        $question_ids = array();

        //question order in request => question order
        $position = 0;
        foreach ($data['questions'] as $question_data) {
            if (is_object($question_data))
                $question_data = (array)$question_data;

            $question_data['position'] = $position;
            $position++;

            //CREATE
            if (empty($question_data['id'])) {
                //question does not exist, Creates it and add it to survey
                $new_question = Question::create($this, $question_data);
                $question_ids[] = $new_question->id;
            } else {
                try {
                    //Get question
                    $question = Question::fromId($question_data['id']);
                    $question->update($this, $question_data);

                } catch(NotFoundException $nfe) {
                    //Question id does not match an existing question
                    $question = Question::create($this, $question_data);
                }

                $question_ids[] = $question->id;
            }
        }

        //DELETE
        foreach ($this->Questions as $question) {
            //The id is not present in the new question list -> we must delete question
            if (!in_array($question->id, $question_ids)) {
                //Droping relation
                $this->dropRelated($question);
                //Drop entity
                $question->delete();
            }
        }

        //Notification when was draft and not anymore
        if (!$this->is_draft) {

            if ($was_draft) {
                //Sending notification to owner
                MomentApplicationMail::prepareCreationEmail($this)->send();

                //Sending invitation to guests
                $this->sendInvitations();
            } else {

                $notify_new_guest = array_key_exists('notify_new_guests', $data) && $data['notify_new_guests'];
                //Sending invitation to guest
                $this->sendInvitations($notify_new_guest, false);

                $notify_update = array_key_exists('notify_update', $data) && $data['notify_update'];
                if($notify_update) {
                    //Sending update notification
                    //Sending closing email to participants and owners (except the one that made the modification)
                    $recipients = array();
                    foreach ($this->Owners as $owner) {
                        if ($owner->email != Auth::user()->email)
                            $recipients[$owner->email] = $owner;
                    }
                    foreach ($this->Participants as $participant) {
                        if ($participant->email && $participant->email != '') {
                            $recipients[$participant->email] = $participant;
                        }
                    }
                    foreach ($recipients as $recipient_email => $entity) {
                        MomentApplicationMail::prepareModificationEmail($this, $recipient_email, $entity)->send();
                    }
                }

            }
        }
        // Saving only when everything went well
        $this->save();

        // Survey is created/updated => corresponding events cache must be cleared
        $this->clearUsersIcsCache();

        return $this;
    }

    /**
     * Saving survey, updating "updated"
     */
    public function save() {
        if ($this->storedInDatabase) {
            EventLog::push($this, EventLogTypes::UPDATED);
        } else {
            EventLog::push($this, EventLogTypes::CREATED);
        }

        $this->updated = time();
        parent::save();
    }

    /**
     * Deleting survey
     * @param bool $send_notification
     */
    public function delete($send_notification = true) {

        // Survey is deleted => corresponding events cache must be cleared
        $this->clearUsersIcsCache();

        if ($send_notification && !$this->is_draft) {
            //Sending notification to owner
            MomentApplicationMail::prepareDeleteEmail($this)->send();
        }

        EventLog::push($this, EventLogTypes::DELETED);
        parent::delete();
    }

    /**
     * Checks if $email is in the owner list
     *
     * @param string $email
     * @return boolean TRUE if $mail is in the owner list, FALSE otherwise
     */
    public function isOwner($email) {
        foreach ($this->Owners as $owner) {
            if (!is_null($owner->email) && strtolower($owner->email) === strtolower($email)) {
                return TRUE;
            }
        }
        //$email doesn't match survey owner
        return FALSE;
    }

    /**
     * Checks if $email is in the guest list
     *
     * @param string $email
     * @return boolean TRUE if $mail is in the guest list, FALSE otherwise
     */
    public function isInvited($email) {
        foreach ($this->Guests as $guest) {
            if (!is_null($guest->email) && strtolower($guest->email) === strtolower($email)) {
                return TRUE;
            }
        }
        //There is no guests to this survey
        return FALSE;
    }

    /**
     * Get results as array
     * @param bool $is_owner bypass of Authenticated User role (in CSV mail attachment for example)
     * @return array
     * @throws BadFormatException
     * @throws ConfigBadParameterException
     * @throws ConfigFileMissingException
     * @throws ConfigMissingParameterException
     */
    public function getResults(bool $is_owner = false): array {
        //Checking context, when we try CSV export some sanitizing is required on texts (question titles, participant names, comments)
        $exportingCSV = false;
        if(ProcessTypes::REST == ApplicationContext::getProcess() && 'csv' == RestRequest::getOutputProperty('format')) {
            $exportingCSV = true;
        }

        $results = array();
        //Sum row
        $sum_result = array(
            Lang::tr('participant_name')->out() => Lang::tr('sum_yes')->out()
        );
        //Sum maybe row (only added when maybe is used)
        $maybe_used = false;
        $sum_maybe_result = array(
            Lang::tr('participant_name')->out() => Lang::tr('sum_maybe')->out()
        );

        //Custom output value for YES, NO, MAYBE
        $customValues = array();
        //Checking selectors
        if(isset($_GET['options'])) {
            try {
                $customValues = json_decode($_GET['options'], true);

                if(!is_array($customValues)) {
                    $customValues = array();
                }
            } catch (Exception $e) {
                //Nothing to do if options are not decodable
            }
        }

        //Some parts of results are only available for owners
        $is_owner = $is_owner || (Auth::user() && $this->isOwner(Auth::user()->email));

        //If user is owner there is an email column
        if($is_owner) {
            $sum_result[Lang::tr('email')->out()] = '';
            $sum_maybe_result[Lang::tr('email')->out()] = '';
        }
        //Saving/caching translations
        $tr = array(
            'participant_name' => Lang::tr('participant_name')->out(),
            'email' => Lang::tr('email')->out(),
            'comment' => Lang::tr('comment')->out()
        );

        //Counting question by title
        $known_questions =  array();

        foreach ($this->Questions as $question) {
            $propositions = $question->sorted_propositions;

            // Question title must be unique in result array (it's used as key)
            $title = $question->statement;
            if(isset($known_questions[$title])) {
                $title = $title.' ('.$known_questions[$title]++.')';
            } else {
                $known_questions[$title] = 1;
            }

            if($exportingCSV) {
                // Securing for CSV injection
                $title = CSVUtil::sanitize($title);
            }

            //Initializing sums
            foreach ($propositions as $proposition) {
                if (!isset($sum_result[$title][$proposition->raw_label])) {
                    $sum_result[$title][$proposition->raw_label] = 0;
                }
                if (!isset($sum_maybe_result[$title][$proposition->raw_label])) {
                    $sum_maybe_result[$title][$proposition->raw_label] = 0;
                }
            }

            //Iterating over question Answers
            $answersIterator = new EntityIterator(
                Answer::name(),
                'question_id = :question_id AND participant_type != :participant_type',
                array(':question_id' => $question->id, ':participant_type' => ParticipantTypes::ULTIMATE)
            );

            foreach($answersIterator as $answer) {
                $answer_participant = $answer->Participant;

                // Securing participant Name from CSV injection
                $result = array(
                    $tr['participant_name'] => $exportingCSV?
                        CSVUtil::sanitize($answer_participant->ensure_name)
                        :$answer_participant->ensure_name
                );

                //Owner has emails in his export
                if($is_owner)
                    $result[$tr['email']] = $answer_participant->email;

                //Ultimate participant should not be shown
                if (ParticipantTypes::ULTIMATE === $answer_participant->type)
                    continue;

                foreach ($propositions as $proposition) {
                    try {
                        $value = (isset($answer->choices[$proposition->id]))?$answer->choices[$proposition->id]->value:Answer::UNDEFINED;
                        $result[$title][$proposition->raw_label] = YesNoMaybeValues::convertToCustom($value, $customValues);
                        $sum_result[$title][$proposition->raw_label] += YesNoMaybeValues::convertToInt($value);
                        $sum_maybe_result[$title][$proposition->raw_label] += (YesNoMaybeValues::MAYBE == $value) ? 1 : 0;
                        $maybe_used = $maybe_used || (YesNoMaybeValues::MAYBE == $value);
                    } catch (NotFoundException $nfe) {
                        //There is a choice for a non existing proposition
                        $answer->removeChoice($answer->choices[$proposition->id]);
                    }
                }

                // Adding comment column by question
                try {
                    if((!$this->hide_comments && !$answer->hide_comment) || $is_owner) {
                        // Securing for CSV injection
                        $result[$title][$tr['comment']] = $exportingCSV?CSVUtil::sanitize($answer->comment):$answer->comment;
                    }
                } catch (NotFoundException $nfe) {
                    //Nothing to do here
                }
                //Important to Notice : Non answered question will not be present in array
                if(isset($results[$answer_participant->id])) {
                    $results[$answer_participant->id] = $results[$answer_participant->id] + $result;
                } else {
                    $results[$answer_participant->id] = $result;
                }
            }
        }

        //If no participant
        if ($this->participants_count === 0) {
            foreach ($this->Questions as $question) {
                foreach ($question->propositions as $proposition) {
                    if (!isset($sum_result[$question->statement][$proposition->raw_label])) {
                        $sum_result[$question->statement][$proposition->raw_label] = 0;
                    }
                    if (!isset($sum_maybe_result[$question->statement][$proposition->raw_label])) {
                        $sum_maybe_result[$question->statement][$proposition->raw_label] = 0;
                    }
                }
            }
        }

        //Adding sum row
        $results[] = $sum_result;

        if ($maybe_used) {
            //Adding sum maybe row
            $results[] = $sum_maybe_result;
        }
        return $results;
    }

    /**
     * Sends Invitation email to all guests
     *
     * @param bool $notify_new_guest
     * @param bool $force
     * @param string $reason
     */
    public function sendInvitations($notify_new_guest = true, $force = false, $reason = '') {
        if ($notify_new_guest) {
            foreach ($this->Guests as $guest) {
                //When sending massive invitation we only send to not yet invited guest (Not sent if guest already participated)
                if (!$guest->participated && ($force || !$guest->invitation_sent)) {
                    MomentApplicationMail::prepareInvitationEmail($this, $guest->email, $reason)->send();

                    $guest->invitation_sent = true;
                    $guest->save();
                }
            }
        }
        //If owner wants to receive a copy each time we send invitation
        if (!$this->dont_receive_invitation_copy) {
            foreach ($this->Owners as $owner) {
                MomentApplicationMail::prepareInvitationEmail($this, $owner->email, $reason)->send();
            }
        }
    }

    /**
     * Sends Notification about a new participant
     * @param Participant $participant
     */
    public function sendNewParticipationNotification(Participant $participant) {
        MomentApplicationMail::prepareNewReplierEmail($this, $participant)->send();
    }

    /**
     * Sends Notifications about the final answers selected
     * @throws MailNoAddressesFoundException
     */
    public function sendFinalAnswersSelectedNotifications() {
        //Sending final answer selected email to guests and participants and owner
        //Note a guest may also be a participant, no need to send two emails for those
        //And likely owner can be guest AND participant
        $recipients = $this->getAllRecipients();
        foreach ($recipients as $recipient_email) {
            MomentApplicationMail::prepareFinalAnswersSelectedEmail($this, $recipient_email)->send();
        }
    }

    /**
     * Closes the survey, setting "closed" to current timestamp or $forced_date
     * @param null $forced_date
     */
    public function close($forced_date = null, $reason = '') {
        if (!isset($this->closed)) {
            Logger::info('Closing ' . $this->id . ' ' . $reason);

            $this->closed = (isset($forced_date)) ? $forced_date : time();

            EventLog::push($this, EventLogTypes::CLOSED);

            //Sending closing email to guests and participants and owner
            //Note a guest may also be a participant, no need to send two emails for those
            //And likely owner can be guest AND participant
            $recipients = $this->getAllRecipients();
            foreach ($recipients as $recipient_email) {
                MomentApplicationMail::prepareClosedEmail($this, $reason, $recipient_email)->send();
            }

            $this->save();

            // Survey is closed => corresponding events cache must be cleared
            $this->clearUsersIcsCache();
        }
    }

    /**
     * Check wether survey is closed or not, closes survey if needed
     * @return bool
     */
    public function isClosed() {
        $current_timestamp = time();

        //a draft can't be closed
        if ($this->is_draft == 1)
            return false;

        //If we have a close date -> it's closed
        if (isset($this->closed)) {
            return true;
        }

        //If current time is > auto_close date
        if (is_numeric($this->auto_close)) {

            // For example : 01/12/2017 00:00 (GMT) is 1512086400 ($this->auto_close)
            // and survey must be closed on 01/12/2017 00:00 (GMT+2) (1512079200) ($this->auto_close - offset)
            // If we take current_timestamp of 01/12/2017 00:00 (GMT+1) (1512082800)
            // Survey will correctly close if we compare $current_timestamp (1512082800) to ($this->auto_close - offset) (1512079200)
            $timestamp_of_closure = $this->auto_close;

            // If we know the timezone we must adapt auto_close to midnight in this timezone
            if($this->time_zone) {
                // 1512086400 - 7200 = 1512079200
                $timestamp_of_closure = $this->auto_close - DateUtil::getTimezoneOffset($this->time_zone);
            }

            // 1512082800 >= 1512079200 => TRUE => we must close this survey (actually one hour too late)
            if($current_timestamp >= $timestamp_of_closure) {
                //Closing the survey, it should be auto_close date
                $this->close($timestamp_of_closure, 'auto_close_reason');

                return true;
            }
        }

        //If current participant number is >= max number of participant
        if ($this->limit_participants && count($this->Participants) >= $this->limit_participants_nb) {
            //Closing the survey
            $this->close(null, 'participant_limit_reached_reason');
            return true;
        }

        return false;
    }

    /**
     * Reopen the survey with a new auto_close date (mandatory) and if needed a new limit for the number of participant
     *
     * @param $data
     * @throws SurveyMissingPropertyException
     */
    public function reopen($data) {

        //Validate data
        if(!isset($data['settings']))
            throw new SurveyMissingPropertyException('settings', $data);

        //New auto_close date is mandatory
        if(!isset($data['settings']->auto_close))
            throw new SurveyMissingPropertyException('settings.auto_close', $data);

        SurveySettings::validateSettings($data['settings']);

        //Check if survey meets conditions

        //It should not have been closed after max_opened_duration
        //It should not have ultimate participant
        //It should be closed
        $maxOpenedDuration = Config::get('max_opened_duration') ? Config::get('max_opened_duration') : 6;
        if(($this->closed > strtotime('+' . $maxOpenedDuration . ' month', $this->created)
            && $this->ultimate_participant == null)
           || $this->ultimate_participant != null
           || $this->closed == null
        ) {
            throw new SurveyActionNotAllowedException('reopen', $data);
        }


        //Closed date is reset
        $this->closed = null;

        //Store reopening date
        $this->reopened = time();

        //New auto close date
        $this->settings['auto_close'] = $data['settings']->auto_close;

        //New limit if needed
        if(isset($data['settings']->limit_participants_nb))
            $this->settings['limit_participants_nb'] = $data['settings']->limit_participants_nb;


        //Sending reopenind email to guests and participants and owner
        //Note a guest may also be a participant, no need to send two emails for those
        //And likely owner can be guest AND participant
        $recipients = $this->getAllRecipients();
        foreach ($recipients as $recipient_email) {
            MomentApplicationMail::prepareReopenedEmail($this, $recipient_email)->send();
        }

        //Logging event
        EventLog::push($this, EventLogTypes::REOPENED);

        // Survey is reopened => corresponding events cache must be updated
        $this->clearUsersIcsCache();

        $this->save();

    }

    /**
     * Apply constraints on survey
     */
    public function applyConstraints() {

        //At the moment there are only constraint on questions
        foreach ($this->Questions as $question) {
            if (method_exists($question, 'applyConstraints')) {
                $question->applyConstraints();
            }
        }
    }

    /**
     * Get displayble info on the survey
     */
    public function infos() {
        $infos = array();

        //Survey is closed
        if ($this->isClosed()) {
            $infos['alert']['is_closed'] = Lang::tr('survey_is_closed_since') . ' : <span data-timestamp="' . $this->closed . '" data-localize="date_time" /><span>';
        } elseif (is_numeric($this->auto_close) && time() > ($this->auto_close - 86400)) {
            //Survey is soon to be closed
            $infos['alert']['soon_closed'] = Lang::tr('survey_closes_soon') . ' : <span data-timestamp="' . $this->auto_close . '" data-localize="date" /><span>';
        }

        if ($this->limit_participants) {
            $level = ($this->participants_count >= ($this->limit_participants_nb * 0.75)) ? 'alert' : 'primary';
            //Survey is limited in number of participants and there is less than n percent left
            $infos[$level]['limit_participants'] = Lang::tr('limited_participant_number') . ' : ' . sprintf(Lang::tr('number_left'), ($this->limit_participants_nb - $this->participants_count));
        }

        if ($this->disable_answer_edition) {
            $infos['warning']['disable_answer_edition'] = Lang::tr('answer_edition_is_disabled');
        }

        return $infos;
    }

    /**
     * Get the title cleaned
     *
     * @return mixed|string
     */
    public function cleanTitle() {
        $str = htmlentities($this->title, ENT_NOQUOTES, 'utf-8');

        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // e.g. '&oelig;'
        return preg_replace('#&[^;]+;#', '', $str); // delete-other chars
    }

    /**
     * Get a human readable id for the survey
     *
     * Example : this-is-a-survey-<id>
     * @param bool $shortened get a shortened version of URL
     * @return type
     */
    private function getHumanReadableId($shortened = false) {
        $url = preg_replace('~[^\\pL0-9_]+~u', '-', $this->cleanTitle()); // substitutes anything but letters, numbers and '_' with separator
        $url = trim($url, "-");
        $url = iconv("utf-8", "us-ascii//TRANSLIT", $url); // TRANSLIT does the whole job
        $url = strtolower($url);
        $url = preg_replace('~[^-a-z0-9_]+~', '', $url); // keep only letters, numbers, '_' and separator

        if ($shortened && strlen($url) > 20) {
            $url = substr($url, 0, 20) . '...';
        }

        return $url . "-" . $this->id;
    }

    /**
     * Clear ics file generated for users linked to this survey (owners and Participants)
     */
    private function clearUsersIcsCache() {
        // A owner created/updated a survey we may need to invalidate his calendar
        foreach ($this->Owners as $owner) {
            MyCalUtil::invalidateIcsCacheForUser($owner->email);
        }

        if($this->storedInDatabase) {
            // A owner created/updated a survey we may need to invalidate participants calendar
            foreach ($this->Participants as $participant) {
                if (ParticipantTypes::AUTHENTICATED == $participant->type) {
                    MyCalUtil::invalidateIcsCacheForUser($participant->email);
                }
            }
        }
    }

    /**
     * Get all recipients emails (owners, guests, participants) excluding duplicates
     * @return array of string emails
     */
    private function getAllRecipients() {
        $recipients = array();

        foreach ($this->Owners as $owner) {
            $recipients[$owner->email] = $owner->email;
        }
        foreach ($this->Guests as $guest) {
            $recipients[$guest->email] = $guest->email;
        }
        foreach ($this->Participants as $participant) {
            if ($participant->email && $participant->email != '') {
                $recipients[$participant->email] = $participant->email;
            }
        }

        return $recipients;
    }

}
