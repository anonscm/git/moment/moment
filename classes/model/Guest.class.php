<?php

/**
 *     Moment - Guest.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class Guest
 */
class Guest extends Entity {

    /**
     * Database map
     */
    protected static $dataMap = array(

        'email' => array(
            'type' => 'string',
            'size' => 255,
            'primary' => true,
            'index' => true
        ),
        'survey_id' => array(
            'type' => 'string',
            'size' => Survey::UID_LENGTH,
            'primary' => true
        ),
        'invitation_sent' => array(
            'type' => 'bool',
            'default' => false
        )
    );

    /**
     * Properties
     */
    protected $email = '';
    protected $invitation_sent = false;
    protected $survey_id = '';

    /**
     * Create a new guest
     *
     * @param Survey $survey
     * @param $guest_email
     * @return Guest
     * @throws GuestInvalidEmailException
     * @throws GuestSurveyNotSavedException
     */
    public static function create(Survey $survey, $guest_email) {

        //Check if survey is stored
        if (!$survey->__get('storedInDatabase'))
            throw new GuestSurveyNotSavedException();

        //Check mail validity
        if (empty($guest_email)
            || !filter_var($guest_email, FILTER_VALIDATE_EMAIL)
        )
            throw new GuestInvalidEmailException($guest_email);

        $guest = new self();
        $guest->email = $guest_email;
        $guest->survey_id = $survey->id;


        //Saving guest
        $guest->save();

        return $guest;
    }

    /**
     * Checks if guest exists for survey
     *
     * @param $email
     * @param Survey $survey the survey
     * @return bool
     */
    public static function exists($email, Survey $survey) {
        return count(self::all('email = :email AND survey_id = :survey_id',
                array(':email' => $email, ':survey_id' => $survey->id))) > 0;
    }

    /**
     * Get all Guests from Survey
     *
     * @param Survey $survey the survey
     *
     * @return array
     */
    public static function fromSurvey(Survey $survey) {
        return self::all('survey_id = :survey_id', array(':survey_id' => $survey->id));
    }

    /**
     * Get all Guests where email is $email
     *
     * @param string $email the email
     *
     * @return array
     */
    public static function fromEmail($email) {
        return self::all('email = :email', array(':email' => $email));
    }

    /**
     * Get Guest where email is $email and $survey is $survey
     *
     * @param string $email the email
     * @param Survey $survey
     * @return array
     */
    public static function fromEmailAndSurvey($email, Survey $survey) {
        $results = self::all('survey_id = :survey_id AND email = :email', array(':survey_id' => $survey->id, ':email' => $email));

        if (count($results) > 0) {
            return array_pop($results);
        }
        return null;
    }

    /**
     * Get guests emails from a owner email (used for suggestion)
     * @param $email, the owner email
     * @return array
     */
    public static function getGuestEmailsFormOwnerEmail($email) {
        $check = DBI::prepare('SELECT email FROM ' . self::getDBTable() . ' WHERE survey_id IN(SELECT survey_id FROM Owners WHERE email = :email)');
        $check->execute(array(':email' => $email));

        return $check->fetchAll(PDO::FETCH_COLUMN, 0);
    }


    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return property value
     */
    public function __get($property) {
        if (in_array($property, array(
            'email', 'invitation_sent', 'survey_id'
        ))) {
            return $this->$property;
        }

        if (strtolower($property) === 'survey') {
            return Survey::fromId($this->survey_id);
        }

        if ($property === 'hash') {
            return substr(sha1($this->survey . $this->email . microtime()), 0, 8);
        }

        //Checks if guests participated
        if ($property === 'participated') {
            try {
                Participant::fromSurveyAndEmail($this->Survey, $this->email);
                return true;
            } catch (ParticipantNotFoundException $e) {
                return false;
            }
        }


        return parent::__get($property);
    }

    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if (in_array($property, array(
            'invitation_sent', 'survey_id'
        ))) {
            $this->$property = $value;
        } else {
            parent::__set($property, $value);
        }

    }

}
