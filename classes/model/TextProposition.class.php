<?php

/**
 *     Moment - TextProposition.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class TextProposition
 */
class TextProposition extends Proposition {

    /**
     * Database map
     */
    protected static $dataMap = array(
        'position' => array(
            'type' => 'uint',
            'size' => 'tiny'
        ),
        'header' => array(
            'type' => 'string',
            'size' => 255,
        ),
        'text' => array(
            'type' => 'string',
            'size' => 255,
            'null' => true
        )
    );

    protected static $hasOne = array('Question');

    /**
     * Properties
     */
    protected $position = '';
    protected $header = '';
    protected $text = null;

    /**
     * Create a new text proposition
     *
     * @param Question $question
     * @param array $proposition_data
     * @return TextProposition
     * @throws PropositionMissingPropertyException
     */
    public static function create(Question $question, array $proposition_data = array()) {

        $mandatoryFields = array(
            'position', 'header'
        );

        foreach ($mandatoryFields as $field) {
            if (!array_key_exists($field, $proposition_data))
                throw new PropositionMissingPropertyException($field, $proposition_data);
        }

        // TODO Check validity  
        $proposition = parent::create($question, $proposition_data);
        $proposition->position = $proposition_data['position'];
        //Shortening header and text to max size
        $proposition->header = substr($proposition_data['header'], 0, self::$dataMap['header']['size']);
        if (array_key_exists('text', $proposition_data)) {
            $proposition->text = substr($proposition_data['text'], 0, self::$dataMap['text']['size']);
        }


        //Saving text proposition
        $proposition->save();

        return $proposition;
    }

    /**
     * Casts a TextProposition into an arry
     * @param TextProposition $proposition
     *
     * @return array
     */
    public static function cast(TextProposition $proposition) {
        return array(
            'id' => $proposition->id,
            'type' => $proposition->type,
            'position' => $proposition->position,
            'header' => $proposition->header,
            'text' => $proposition->text,
            'options' => $proposition->options
        );
    }

    /**
     * Compare two propositions
     * @param mixed $proposition_a
     * @param mixed $proposition_b
     * @return bool
     */
    public static function compare($proposition_a, $proposition_b) {
        $position_a = 0;
        $position_b = 0;
        if (is_array($proposition_a) && array_key_exists('position', $proposition_a))
            $position_a = $proposition_a['position'];

        if ($proposition_a instanceof TextProposition)
            $position_a = $proposition_a->position;

        if (is_array($proposition_b) && array_key_exists('position', $proposition_b))
            $position_b = $proposition_b['position'];

        if ($proposition_b instanceof TextProposition)
            $position_b = $proposition_b->position;

        return $position_b < $position_a;
    }

    /**
     * Get proposition from values
     * @param Question $question
     * @param array $data
     * @return TextProposition
     * @throws PropositionNotFoundException
     */
    public static function fromValues(Question $question, array $data): TextProposition {
        $ph = [':question_id' => $question->id];
        foreach(['header', 'text'] as $key) {
            if (isset($data[$key])) {
                $ph[':' . $key] = $data[$key];
            }
        }
        $query = implode(' AND ', array_map(function($k,$v) { return trim($k, ':').' = ' . $k; }, array_keys($ph), $ph));

        $propositions = static::all($query, $ph);
        if(count($propositions) === 1) {
            $proposition = array_shift($propositions);
            //We update position if needed
            if(isset($data['position']) && $proposition->position !== $data['position']) {
                $proposition->position = $data['position'];
                $proposition->save();
            }
            return $proposition;
        }

        throw new PropositionNotFoundException();
    }


    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return property value
     */
    public function __get($property) {
        if (in_array($property, array(
            'position', 'header', 'text'
        ))) {
            return $this->$property;
        }

        if ('type' == $property) {
            return PropositionTypes::TEXT;
        }

        if ('label' === $property
            || 'raw_label' === $property
        ) {
            $label = $this->header;
            if ($this->text) {
                $label .= ' - ' . $this->text;
            }
            return $label;
        }


        return parent::__get($property);
    }

    /**
     * Setter
     *
     * @param string $property property to get
     * @param mixed $value value to set property to
     *
     * @throws PropertyAccessException
     */
    public function __set($property, $value) {
        if (in_array($property, array(
            'position', 'header', 'text'
        ))) {
            $this->$property = $value;
        } else {
            parent::__set($property, $value);
        }
    }

    /**
     * Update the proposition with data.
     * @param Question $question
     * @param array $proposition_data
     * @throws PropositionMissingPropertyException
     */
    public function update(Question $question, array $proposition_data = array()) {

        parent::update($question, $proposition_data);

        $mandatoryFields = array(
            'position', 'header'
        );

        foreach ($mandatoryFields as $field) {
            if (!array_key_exists($field, $proposition_data))
                throw new PropositionMissingPropertyException($field, $proposition_data);
        }

        // TODO Check validity  

        $this->position = $proposition_data['position'];

        //Shortening header and text to max size
        $this->header = substr($proposition_data['header'], 0, self::$dataMap['header']['size']);
        if (array_key_exists('text', $proposition_data)) {
            $this->text = substr($proposition_data['text'], 0, self::$dataMap['text']['size']);
        } else { //Case no text
            $this->text = '';
        }

        //Saving text proposition
        $this->save();
    }

}
