<?php

/**
 *     Moment - EventLog.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class EventLog
 */
class EventLog extends Entity {


    /**
     * Database map
     */
    protected static $dataMap = array(
        'id' => array(
            'type' => 'uint',
            'size' => ' ', // This is a tricks to use INT (and not medium or big)
            'primary' => true,
            'autoinc' => true
        ),
        'created' => array(
            'type' => 'datetime',
            'null' => false
        ),
        'idp' => array(
            'type' => 'string',
            'size' => 255,
            'null' => true
        ),
        'type' => array(
            'type' => 'string',
            'size' => 15,
        ),
        'subject' => array(
            'type' => 'string',
            'size' => 255,
        ),
        'value' => array(
            'type' => 'string',
            'size' => 255,
            'null' => true
        ),
    );

    protected $id = null;
    protected $created = null;
    protected $idp = null;
    protected $type = '';
    protected $subject = '';
    protected $value = null;

    /**
     *
     * @param Entity $entity
     * @param $type
     * @param null $value
     */
    public static function push(Entity $entity, $type, $value = null) {

        $eventLog = new EventLog();

        $eventLog->created = time();
        if (Auth::user()) {
            $eventLog->idp = Auth::user()->attributes->idp;
        }
        $eventLog->type = $type;

        //If there is an email in subject entity we clear it (__toString urlencodes emails)
        $eventLog->subject = preg_replace("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", '', urldecode($entity->__toString()));

        if (isset($value)) {
            $eventLog->value = $value;
        } else {
            //By default we try to set the id if property exists
            if (property_exists($entity, 'id')) {
                $eventLog->value = $entity->id;
            }
        }

        $eventLog->save();
    }

    /**
     * Getter
     *
     * @param string $property property to get
     *
     * @throws PropertyAccessException
     *
     * @return property value
     */
    public function __get($property) {
        if (in_array($property, array(
            'id', 'created', 'idp', 'type', 'subject', 'value'
        ))) {
            return $this->$property;
        }
    }

}
