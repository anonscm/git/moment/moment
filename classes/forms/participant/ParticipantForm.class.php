<?php

/**
 *     Moment - ParticipantForm.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class ParticipantForm
 */
class ParticipantForm extends Form {

    /**
     * VoteCreateForm constructor
     * @param Survey $survey
     * @param Participant $participant
     */
    public function __construct(Survey $survey, Participant $participant = null) {

        $fields = array(
            new FormFieldHidden('participant_token', array(), (isset($participant) ? $participant->uid : '')),
        );

        if (!(isset($participant) && $participant->isAnonymous())) {
            $fields[] = '<div class="small-12 '. (!isset($participant) ? 'large-6  columns' : 'large-8 small-centered') . '">';
            $fields[] = new FormFieldText('name', array(
                'maxlength' => 100,
                'label' => 'your_name',
                'required' => !$survey->enable_anonymous_answer
            ), (isset($participant) ? $participant->name : ''));
            $fields[] =  '</div>';
        }

        if (!isset($participant)) {
            $fields[] = '<div class="small-12 large-6 columns">';
            $fields[] = new FormFieldEmail('email', array(
                'label' => 'your_email',
                'required' => !$survey->enable_anonymous_answer
            ));
            $fields[] = '</div>';
        }

        parent::__construct('vote_create', $fields);
    }

}
