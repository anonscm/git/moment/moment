<?php

/**
 *     Moment - CalendarForm.class.php
 *
 * Copyright (C) 2020  RENATER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Class CalendarForm
 */
class CalendarForm extends Form {

    /**
     * VoteCreateForm constructor
     * @param Survey $survey
     * @param Participant $participant
     */
    public function __construct(Calendar $calendar = null) {

        $fields = array(
            new FormFieldHidden('id', array(), (isset($calendar) ? $calendar->id : '')),
            new FormFieldText('name', array(
                'placeholder' => Lang::tr('calendar_name_placeholder'),
                'required' => true,
                'maxlength' => 100,
                'label' => 'calendar_name'
            ), (isset($calendar) ? $calendar->name : '')),
            new FormFieldURL('url', array(
                'placeholder' => Lang::tr('calendar_url_placeholder'),
                'required' => true,
                'pattern' => '[^\s]+',
                'constraints' => 'calendar_url_constraint'
            ), (isset($calendar) ? $calendar->url : '')),
            new FormFieldColor('color', array(
                'label' => 'calendar_color',
                'pattern' => '^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$',
                'constraints' => ''
            ), (isset($calendar) ? $calendar->settings->color : CalendarUtil::getColor())),
        );
        parent::__construct('calendar_form', $fields);
    }

}
