<?php

/**
 * This file is part of the BaseProject project.
 * 2015 
 * Copyright (c) RENATER
 */

/**
 * Dedicated exception
 */
class ApplicationRestClientException extends Exception {
    
    private $details;
    private $uid;
    
    public function __construct($message = "", $code = 0, $details = null) {
        $this->details = $details;
        
        if (is_string($message)){
            $message = trim($message);
            if (substr($message, 0,1) === '{' && substr($message, -1) === '}'){
                // Houston, we got a json!
                $json = json_decode($message);
                if (!json_last_error())
                    $message = $json;
            }
        }
        
        if (is_object($message)){
            if (property_exists($message, 'details'))
                $this->details = $message->details;
            
            if (property_exists($message, 'uid'))
                $this->uid = $message->uid;
            
            if (property_exists($message, 'message'))
                $message = $message->message;
            else
                $message = json_encode($message);
        }
        
        parent::__construct($message, $code);
    }
    
    
    public function getDetails(){
        return $this->details;
    }
    
    public function getUid(){
        return $this->uid;
    }
    
    public function getInfo(){
        return array(
            'uid' => $this->uid,
            'message' => $this->getMessage(),
            'code' => $this->getCode(),
            'details' => is_object($this->details) ? (array) $this->details : $this->details,
        );
    }
    
}
