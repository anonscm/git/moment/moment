<?php

/**
 * This file is part of the BaseProject project.
 * 2015 
 * Copyright (c) RENATER
 */

if(!defined('EKKO_CLIENT_ROOT')){
    if (!defined('EKKO_ROOT')) die ('Missing environment');
    define('EKKO_CLIENT_ROOT', EKKO_ROOT.'/lib/client');
}
    

// @dependency DO NOT ALTER
require_once EKKO_CLIENT_ROOT.'/core/ApplicationRestClientException.class.php';

// @dependency DO NOT ALTER
require_once EKKO_CLIENT_ROOT.'/core/ApplicationRestClientAuthApplication.class.php';

// @dependency DO NOT ALTER
require_once EKKO_CLIENT_ROOT.'/core/ApplicationRestClientAuthUser.class.php';

/**
 * REST client for ApplicationBase
 */
class ApplicationRestClient {
    /**
     * @var string base url to the application's rest service
     */
    protected $base_url = null;
    
    /**
     * @var ApplicationRestClientAuth authentication
     */
    protected $authentication = null;
    
    /**
     * @var resource cURL handle
     */
    protected $handle = null;
    
    /**
     * @var boolean debug mode
     */
    protected $debug = false;
    
    /**
     * Constructor
     * 
     * @param string $base_url base url to ApplicationBase's rest service
     * @param ApplicationRestClientAuth $authentication Authentication mode instance
     * @param mixed $user
     * 
     * @throws ApplicationRestClientException
     */
    public function __construct($base_url, ApplicationRestClientAuth $authentication = null) {
        if(!function_exists('curl_init'))
            throw new ApplicationRestClientException('Missing cURL');
        
        if(!$base_url)
            throw new ApplicationRestClientException('Missing base URL');
        
//        if(!filter_var($base_url, FILTER_VALIDATE_URL))
//            throw new ApplicationRestClientException('Bad base URL');
        
        if(substr($base_url, -1) == '/')
            $base_url = substr($base_url, 0, -1);
        
        if(!preg_match('`/rest(\.php)?$`', $base_url))
            $base_url .= '/rest.php';
        
        $this->base_url = $base_url;
        
        if($authentication)
            $this->__set('authentication', $authentication);
    }
    
    
    /**
     * Destructor of ApplicationRestClient
     */
    public function __destruct() {
        if($this->handle)
            curl_close ($this->handle);
    }
    
    /**
     * Serialize arguments (recursive)
     * 
     * @param array $args
     * @param $prefix
     * 
     * @return array single dimension array
     */
    public static function serializeArguments($args, $prefix = null) {
        ksort($args);
        
        $serialized = array();
        foreach($args as $key => $value) {
            if($prefix) $key = $prefix.'['.$key.']';
            
            if(is_array($value)) {
                foreach(self::serializeArguments($value, $key) as $sub)
                    $serialized[] = $sub;
                
            } else {
                $serialized[] = $key.'='.$value;
            }
        }
        
        return $prefix ? $serialized : implode('&', $serialized);
    }
    
    /**
     * Output debug message
     * 
     * @param string $message
     */
    public function debugMessage($message) {
        if($this->debug) echo $message."\n";
    }
    
    /**
     * Getter
     * 
     * @param string $property
     * 
     * @return mixed
     * 
     * @throws ApplicationRestClientException
     */
    public function __get($property) {
        if($property == 'base_url')
            return $this->base_url;
        
        if($property == 'debug')
            return $this->debug;
        
        if($property == 'authentication')
            return $this->authentication;
        
        throw new ApplicationRestClientException('No such property : '.$property);
    }
    
    /**
     * Getter
     * 
     * @param string $property
     * @param mixed $value
     * 
     * @throws ApplicationRestClientException
     */
    public function __set($property, $value) {
        if($property == 'authentication') {
            if(is_null($value)) {
                $this->authentication = null;
                $this->debugMessage('Set authentication to none');
                
            } else if(is_object($value) && ($value instanceof ApplicationRestClientAuth)) {
                $this->authentication = $value;
                $this->debugMessage('Set authentication to '.$value);
                
            } else throw new ApplicationRestClientException('Not a valid authentication mode');
            
        } else if($property == 'debug') {
            $this->debug = (bool)$value;
            
        } else throw new ApplicationRestClientException('No such property : '.$property);
    }
    
    /**
     * Make a signed call to the application
     * 
     * @param string $method HTTP method to use
     * @param string $path path to make the request to (under the rest service)
     * @param array $args query arguments
     * @param mixed $content request body
     * 
     * @return mixed response
     * 
     * @throws ApplicationRestClientException
     */
    protected function call($method, $path, $args = array(), $content = null, $options = array()) {
        if(!in_array($method, array('get', 'post', 'put', 'delete'))) throw new Exception('Method not allowed', 405);
        
        $this->debugMessage('Send '.$method.' request to '.$path);
        
        if(substr($path, 0, 1) != '/') $path = '/'.$path;
        if($path == '/') throw new ApplicationRestClientException('Missing endpoint', 400);
        
        $content_type = 'application/json';
        if(array_key_exists('Content-Type', $options))
            $content_type = $options['Content-Type'];
        
        if($content) {
            switch($content_type) {
                case 'text/xml':
                    if(is_object($content)) {
                        if($content instanceof SimpleXMLElement)
                            $content = $content->asXML();
                        
                        if($content instanceof DOMNode)
                            $content = $content->ownerDocument;
                        
                        if($content instanceof DOMDocument)
                            $content = $content->saveXML();
                        
                        if(!is_string($content))
                            throw new ApplicationRestClientException('Could not encode to XML');
                    }
                    
                    if(!is_string($content))
                        throw new ApplicationRestClientException('Not an XML string');
                    
                    break;
                    
                case 'application/json':
                    $content = json_encode($content);
                    if(json_last_error())
                        throw new ApplicationRestClientException('Could not encode content to JSON');
                    
                    break;
            }
        }
        
        if($this->authentication) {
            $url = $this->authentication->sign($method, $this->base_url.$path, $args, $content);
            $this->debugMessage('Authentication applied, signed URL is '.$url);
            
        } else {
            $args = self::serializeArguments($args);
            $url = $this->base_url.$path.($args ? '?'.$args : '');
            $this->debugMessage('No authentication set, URL is '.$url);
        }
    
        $url = str_replace(' ', '+', $url);
        
        if(!$this->handle)
            $this->handle = curl_init();
        
        curl_setopt($this->handle, CURLOPT_URL, $url);
        
        curl_setopt($this->handle, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: '.$content_type
        ));
        
        curl_setopt($this->handle, CURLOPT_HEADER, true);
        curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->handle, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->handle, CURLOPT_SSL_VERIFYPEER, false);
        
        curl_setopt($this->handle, CURLOPT_CUSTOMREQUEST, strtoupper($method));
        
        curl_setopt($this->handle, CURLOPT_POST, strtoupper($method) === 'POST');
        curl_setopt($this->handle, CURLOPT_POSTFIELDS, $content);
        
        $response = curl_exec($this->handle);
        $code = (int)curl_getinfo($this->handle, CURLINFO_HTTP_CODE);
        $header_len = (int)curl_getinfo($this->handle, CURLINFO_HEADER_SIZE);
        
        $errno = curl_errno($this->handle);
        $error = curl_error($this->handle);
        
        $this->debugMessage('cURL error status : ('.$errno.') '.$error);
        $this->debugMessage('HTTP response : ('.$code.') '.strlen($response).' bytes');
        
        if($error)
            throw new ApplicationRestClientException('Client error', $errno, $error);
        
        $hdr = explode("\r\n", substr($response, 0, $header_len));
        $headers = array();
        foreach(array_filter(array_map('trim', $hdr)) as $line) {
            if (!preg_match('`^([a-z][a-z0-9-]*):\s*(.+)$`i', $line, $m)) continue;
            $headers[$m[1]][] = $m[2];
        }
        $hdr = null; // Free memory
        
        foreach($headers as $key => $value)
            if(count($value) == 1)
                $headers[$key] = $value[0];
        
        $body = trim(substr($response, $header_len));
        $response = null; // Free memory
        
        $this->debugMessage('Parsed '.count($headers).' headers, remaining body has '.strlen($body).' bytes');
        
        if($code != 200)
            if(($method != 'post') || ($code != 201))
//                throw new ApplicationRestClientException('Http error ('.$code.')'.($body ? ' : '.$body : ''));
                throw new ApplicationRestClientException($body, $code);
        
        if(!$body)
            throw new ApplicationRestClientException('Empty response');
        
        $content_type = array_key_exists('Content-Type', $headers) ? $headers['Content-Type'] : null;
        
        switch($content_type) {
            case 'text/xml':
                $body = simplexml_load_string($body);
                if($body === false)
                    throw new ApplicationRestClientException('Could not parse response as XML');
                
                break;
                
            case 'application/json':
                $body = json_decode($body);
                if(json_last_error())
                    throw new ApplicationRestClientException('Could not parse response as JSON');
                
                break;
        }
        
        return (object)array(
            'code' => $code,
            'headers' => $headers,
            'body' => $body
        );
    }
    
    /**
     * Validator for updatedSince parameter
     * 
     * @param mixed $value the raw parameter
     * 
     * @throws ApplicationRestClientException
     */
    private function validateUpdatedSince($value) {
        if(
            !preg_match('`^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(Z|[+-][0-9]{2}:[0-9]{2})$`', $value) // ISO
            && !preg_match('`^([0-9]+)\s*(hour|day|week|month|year)s?$`', $value) // offset
            && !preg_match('`^[0-9]+$`', $value) // Epoch
        ) throw new ApplicationRestClientException('Invalid updatedSince value');
    }
    
    /**
     * Make a GET request
     * 
     * @param string $path path to make the request to (under the rest service)
     * @param array $args GET arguments
     * 
     * @return mixed the response
     */
    public function get($path, $args = array(), $options = array()) {
        return $this->call('get', $path, $args, null, $options);
    }
    
    /**
     * Make a POST request
     * 
     * @param string $path path to make the request to (under the rest service)
     * @param array $args GET arguments
     * @param mixed $content request body
     * 
     * @return mixed the response
     */
    public function post($path, $args = array(), $content = null, $options = array()) {
        return $this->call('post', $path, $args, $content, $options);
    }
    
    /**
     * Make a PUT request
     * 
     * @param string $path path to make the request to (under the rest service)
     * @param array $args GET arguments
     * @param mixed $content request body
     * 
     * @return mixed the response
     */
    public function put($path, $args = array(), $content = null, $options = array()) {
        return $this->call('put', $path, $args, $content, $options);
    }
    
    /**
     * Make a DELETE request
     * 
     * @param string $path path to make the request to (under the rest service)
     * @param array $args GET arguments
     * 
     * @return mixed the response
     */
    public function delete($path, $args = array(), $options = array()) {
        return $this->call('delete', $path, $args,  null, $options);
    }
}
