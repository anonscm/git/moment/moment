<?php

/**
 * This file is part of the BaseProject project.
 * 2015 
 * Copyright (c) RENATER
 */

if(!defined('EKKO_CLIENT_ROOT')){
    if (!defined('EKKO_ROOT')) die ('Missing environment');
    define('EKKO_CLIENT_ROOT', EKKO_ROOT.'/lib/client');
}

// @dependency DO NOT ALTER
require_once EKKO_CLIENT_ROOT.'/core/ApplicationRestClientAuth.class.php';

/**
 * Application based REST Auth
 */
class ApplicationRestClientAuthApplication extends ApplicationRestClientAuth {
    /**
     * Constructor
     * 
     * @param string $application
     * @param string $secret
     * @param string $algorithm
     */
    public function __construct($application, $secret, $algorithm = null) {
        parent::__construct(
            array(
                'remote_application' => $application
            ),
            $secret,
            $algorithm
        );
    }
    
    /**
     * Getter
     * 
     * @param string $property
     * 
     * @return mixed
     */
    public function __get($property) {
        if($property == 'user')
            return array_key_exists('remote_user', $this->args) ? $this->args['remote_user'] : null;
        
        return parent::__get($property);
    }
    
    /**
     * Getter
     * 
     * @param string $property
     * @param mixed $value
     * 
     * @throws ApplicationRestClientException
     */
    public function __set($property, $value) {
        if($property == 'user') {
            if(is_null($value)) {
                unset($this->args['remote_user']);
                return;
            }
            
            $this->args['remote_user'] = self::validateUser($value); // throws
            
        } else {
            parent::__set($property, $value);
        }
    }
}
