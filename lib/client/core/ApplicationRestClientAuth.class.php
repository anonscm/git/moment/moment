<?php

/**
 * This file is part of the BaseProject project.
 * 2015 
 * Copyright (c) RENATER
 */


/**
 * Signature based REST Auth
 */
class ApplicationRestClientAuth {
    /**
     * @var array args
     */
    protected $args = array();
    
    /**
     * @var string secret
     */
    protected $secret = null;
    
    /**
     * @var string algorithm
     */
    protected $algorithm = 'sha1';
    
    /**
     * Constructor
     * 
     * @param array $args
     * @param string $secret
     * @param string $algorithm
     */
    public function __construct(array $args, $secret, $algorithm = null) {
        $this->args = $args;
        $this->secret = $secret;
        
        if($algorithm)
            $this->algorithm = $algorithm;
    }
    
    /**
     * Validate user descriptor and standardize it
     * 
     * @param mixed $user uid or user descriptor
     * 
     * @throws ApplicationRestClientException
     */
    public function validateUser($user) {
        if(!is_array($user) && !is_string($user))
            throw new ApplicationRestClientException('Not a user descriptor');
        
        if(!is_array($user))
            $user = array('uid' => $user);
        
        if(!array_key_exists('uid', $user))
            throw new ApplicationRestClientException('Not a user descriptor');
        
        if(!is_string($user['uid']) || !$user['uid'])
            throw new ApplicationRestClientException('Not a valid user descriptor');
        
        return $user;
    }
    
    /**
     * Sign a call
     * 
     * @param string $method HTTP method to use
     * @param string $path path to make the request to (under the rest service)
     * @param array $args GET arguments
     * @param mixed $content request body
     * 
     * @return string signed URL
     */
    public function sign($method, $path, $args = array(), $content = null) {
        // Add own args
        foreach($this->args as $key => $value)
            $args[$key] = $value;
        
        // Add current timestamp
        $args['timestamp'] = time();
        
        // Sort and serialize
        ksort($args);
        $args = ApplicationRestClient::serializeArguments($args);
        
        // Build signed string
        $signed = $method.'&'.preg_replace('`https?://`', '', $path).'?'.$args;
        if($content) $signed .= '&'.$content;
        
        // Compute signature
        $signature = hash_hmac($this->algorithm, $signed, $this->secret);
        
        return $path.'?'.$args.'&signature='.$signature;
    }
    
    /**
     * Stringify
     * 
     * @return string
     */
    public function __toString() {
        return get_called_class().'?'.ApplicationRestClient::serializeArguments($this->args);
    }
}
