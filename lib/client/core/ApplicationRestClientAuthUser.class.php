<?php

/**
 * This file is part of the BaseProject project.
 * 2015 
 * Copyright (c) RENATER
 */

if(!defined('EKKO_CLIENT_ROOT')){
    if (!defined('EKKO_ROOT')) die ('Missing environment');
    define('EKKO_CLIENT_ROOT', EKKO_ROOT.'/lib/client');
}

// @dependency DO NOT ALTER
require_once EKKO_CLIENT_ROOT.'/core/ApplicationRestClientAuth.class.php';

/**
 * User based REST Auth
 */
class ApplicationRestClientAuthUser extends ApplicationRestClientAuth {
    /**
     * Constructor
     * 
     * @param mixed $user
     * @param string $secret
     * @param string $algorithm
     */
    public function __construct($user, $secret, $algorithm = null) {
        parent::__construct(
            array(
                'remote_user' => self::validateUser($user)
            ),
            $secret,
            $algorithm
        );
    }
}
