<?php

/**
 * This file is part of the BaseProject project.
 * 2015 
 * Copyright (c) RENATER
 */

define('EKKO_ROOT', realpath(dirname(__FILE__).'/../../../'));

define('DEBUG', 0);

/**
 * Rest client PHAR builder
 */

if(!isset($argv))
    die('Must run from cli'."\n");

$clients = array_slice($argv, 1);

if(!count($clients)) {
    echo 'Usage : '.$argv[0].' <client> [<client> ...]'."\n";
    echo 'client : rest client class name'."\n";
    echo "\n";
    exit;
}

$readonly = ini_get('phar.readonly');
if(preg_match('`^(on|1)$`i', $readonly))
    die('Phar archive creation disabled in PHP config, you must set phar.readonly to "Off"'."\n");

$src = EKKO_ROOT.'/lib/client';
$dest = EKKO_ROOT.'/lib/client/phar';

$deps_lookup = function($code, $phar_name) {
    $lines = explode("\n", $code);
    $out = array();
    $deps = array();
    
    while(!is_null($line = array_shift($lines))) {
        if(preg_match('`^\s*//\s*@dependency(\s|$)`', $line)) {
            $line = array_shift($lines);
            if(preg_match('`^\s*((?:require|include)(?:_once))?\s*\(?EKKO_CLIENT_ROOT\s*\.\s*[\'"]/([^\.]+\.class\.php)[\'"]\s*;`', $line, $m)) {
                $deps[] = $m[2];
                $out[] = $m[1].' \'phar://'.$phar_name.'/'.$m[2].'\';';
                $line = null;
                
            } else {
                echo '@dependency found but next line does not look like an include'."\n";
            }
        }
        
        if(!is_null($line))
            $out[] = $line;
    }
    
    $out = implode("\n", $out);
    
    if(DEBUG) {
        echo '======================================'."\n";
        echo 'Dependencies : '.implode(', ', $deps)."\n";
        echo "\n\n";
        echo substr($out, 0, 768).' [...]'."\n";
        echo "\n\n";
        echo '======================================'."\n";
        echo "\n\n";
    }
    
    return array('code' => $out, 'dependencies' => $deps);
};

$phar_options = FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::KEY_AS_FILENAME;

foreach($clients as $client) {
    if(substr($client, -10) == '.class.php')
        $client = substr($client, 0, -10);
    
    $stub = ($client == 'ApplicationRestClient' ? 'core/'.$client : $client).'.class.php';
    
    if(!file_exists($src.'/'.$stub)) {
        echo $client.' not found '."\n";
        continue;
    }
    
    $included = array();
    $bundle = array($stub);
    $phar_name = $client.'.phar';
    $phar = new Phar($dest.'/'.$phar_name, $phar_options, $phar_name);
    
    while($file = array_shift($bundle)) {
        $out = $deps_lookup(file_get_contents($src.'/'.$file), $phar_name);
        $phar[$file] = $out['code'];
        $included[] = $file;
        
        echo 'Included '.$file.', adding '.count($out['dependencies']).' dependencies to stack'."\n";
        
        foreach($out['dependencies'] as $file) {
            if(in_array($file, $included)) continue;
            $bundle[] = $file;
        }
    }
    
    $phar->setStub($phar->createDefaultStub($stub));
}
