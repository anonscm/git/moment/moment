<?php

/**
 * Generic exceptions
 */
class WebDavException extends Exception {
    /**
     * Constructor
     * 
     * @param string $message
     * @param array $details
     * @param int $code
     */
    public function __construct($message, $details = array(), $code = 0) {
        $message = 'WebDav client exception : '.$message;
        
        if($details) {
            $message = array($message);
            foreach($details as $k => $v)
                $message[] = ($k ? $k.': ' : '').$v;
            
            $message = implode("\n\t", $message);
        }
        
        parent::__construct($message, $code);
    }
}

/**
 * Missing base URL
 */
class WebDavMissingBaseURLException extends WebDavException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('missing base URL');
    }
}

/**
 * Generic error
 */
class WebDavErrorException extends WebDavException {
    /**
     * Constructor
     * 
     * @param int $errno
     * @param string $error
     */
    public function __construct($errno, $error) {
        parent::__construct('request error', array('errno' => $errno, 'error' => $error), $errno);
    }
}

/**
 * Multiple errors
 */
class WebDavErrorsException extends WebDavException {
    /**
     * Constructor
     * 
     * @param array $causes
     */
    public function __construct($causes) {
        parent::__construct('errors', $causes);
    }
}

/**
 * Cannot read file
 */
class WebDavCannotReadFileException extends WebDavException {
    /**
     * Constructor
     * 
     * @param string $path
     */
    public function __construct($path) {
        parent::__construct('cannot read file', array('path' => $path));
    }
}
