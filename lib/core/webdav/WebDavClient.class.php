<?php

/**
 * WebDav Client (mainly for documentation update)
 * 
 * Inspired from saber-dav
 */
class WebDavClient {
    /**
     * Authentication types
     */
    const AUTH_BASIC    = 1;
    const AUTH_DIGEST   = 2;
    const AUTH_NTLM     = 4;
    
    /**
     * Encodings
     */
    const ENCODING_IDENTITY = 1;
    const ENCODING_DEFLATE  = 2;
    const ENCODING_GZIP     = 4;
    const ENCODING_ALL      = 7;
    
    /**
     * @var string base URL
     */
    private $base_url = '';
    
    /**
     * @var array curl settings
     */
    private $curl_settings = array(
        CURLOPT_NOBODY => false,
        CURLOPT_HEADER => true,
        CURLOPT_RETURNTRANSFER => true
    );
    
    /**
     * @var resource Curl handle
     */
    private $handle = null;
    
    /**
     * @var resource dummy resource
     */
    private $dummy_resource = null;
    
    /**
     * Debug flag
     */
    public $debug = false;

    /**
     * Build a new client
     *
     * @param array $settings
     *
     * @throws WebDavException
     * @throws WebDavMissingBaseURLException
     */
    public function __construct($settings) {
        if(!function_exists('curl_init'))
            throw new WebDavException('missing cURL');
        
        if(!array_key_exists('base_url', $settings))
            throw new WebDavMissingBaseURLException();
        
        $this->base_url = $settings['base_url'];
        
        if(array_key_exists('proxy', $settings))
            $this->curl_settings[CURLOPT_PROXY] = $settings['proxy'];
        
        if(array_key_exists('username', $settings)) {
            
            $username = $settings['username'];
            $password = array_key_exists('password', $settings) ? $settings['password'] : '';
            
            if(array_key_exists('auth_type', $settings)) {
                $type = 0;
                if($settings['auth_type'] & self::AUTH_BASIC)
                    $type |= CURLAUTH_BASIC;
                
                if($settings['auth_type'] & self::AUTH_DIGEST)
                    $type |= CURLAUTH_DIGEST;
                
                if($settings['auth_type'] & self::AUTH_NTLM)
                    $type |= CURLAUTH_NTLM;
                
            } else {
                $type = CURLAUTH_BASIC | CURLAUTH_DIGEST;
            }
            
            $this->curl_settings[CURLOPT_HTTPAUTH] = $type;
            $this->curl_settings[CURLOPT_USERPWD] = $username.':'.$password;
        }
        
        if(array_key_exists('encoding', $settings)) {
            $encodings = array();
            
            if($settings['encoding'] & self::ENCODING_IDENTITY)
                $encodings[] = 'identity';
            
            if($settings['encoding'] & self::ENCODING_DEFLATE)
                $encodings[] = 'deflate';
            
            if($settings['encoding'] & self::ENCODING_GZIP)
                $encodings[] = 'gzip';
            
            $this->curl_settings[CURLOPT_ENCODING] = implode(',', $encodings);
        }
        
        $this->handle = curl_init();
        
        $this->dummy_resource = fopen('php://stdin', 'rb');
    }
    
    /**
     * Destructor
     */
    public function __destruct() {
        curl_close($this->handle);
        $this->handle = null;
        
        fclose($this->dummy_resource);
        $this->dummy_resource = null;
    }

    /**
     * Sends request
     *
     * @param string $method
     * @param string $url
     * @param mixed $body
     * @param array $headers
     *
     * @return array
     *
     * @throws WebDavErrorException
     */
    private function call($method, $url, $body = null, $headers = array()) {
        $method = strtoupper($method);
        $settings = $this->curl_settings;
        
        switch($method) {
            case 'HEAD' :
                $settings[CURLOPT_NOBODY] = true;
                $settings[CURLOPT_CUSTOMREQUEST] = 'HEAD';
                $settings[CURLOPT_POSTFIELDS] = '';
                $settings[CURLOPT_PUT] = false;
                break;
            
            case 'GET' :
                $settings[CURLOPT_CUSTOMREQUEST] = 'GET';
                $settings[CURLOPT_POSTFIELDS] = '';
                $settings[CURLOPT_PUT] = false;
                break;
            
            default :
                if(is_resource($body)) {
                    $settings[CURLOPT_PUT] = true; // Even if post, curl weirdness
                    $settings[CURLOPT_INFILE] = $body;
                    
                } else {
                    $settings[CURLOPT_POSTFIELDS] = (string)$body;
                }
                
                $settings[CURLOPT_CUSTOMREQUEST] = $method;
        }
        
        $curl_headers = array();
        foreach($headers as $key => $values)
            foreach((array)$values as $value)
                $curl_headers[] = $key.': '.$value;
        
        $settings[CURLOPT_HTTPHEADER] = $curl_headers;
        
        $url = implode('/', array_map(function($part) {
            return urlencode($part);
        }, explode('/', $url)));
        
        $settings[CURLOPT_URL] = $this->base_url.$url;
        
        if($this->debug) {
            $cst = array();
            foreach(get_defined_constants() as $k => $v)
                if(substr($k, 0, 8) == 'CURLOPT_')
                    $cst[$v] = $k;
            
            echo "\n".'### cURL request'."\n";
            foreach($settings as $k => $v) {
                if(!is_scalar($v))
                    $v = gettype($v);
                
                if($k == CURLOPT_USERPWD)
                    $v = preg_replace('`^([^:]+):(.).*(.)$`', '$1:$2.....$3', $v);
                
                echo $cst[$k].': '.$v."\n";
            }
            echo "\n";
        }
        
        curl_setopt_array($this->handle, $settings);
        
        $response = curl_exec($this->handle);
        $info = curl_getinfo($this->handle);
        $errno = curl_errno($this->handle);
        $error = curl_error($this->handle);
        
        if(is_resource($body)) // cURL dumbness fix
            curl_setopt($this->handle, CURLOPT_INFILE, $this->dummy_resource);
        
        if($errno)
            throw new WebDavErrorException($errno, $error);
        
        $header_part = trim(substr($response, 0, $info['header_size']));
        $headers = array();
        foreach(preg_split('`[\r\n]+`', $header_part) as $entry) {
            $parts = explode(':', $entry, 2);
            if(substr($parts[0], 0, 6) == 'HTTP/1')
                $parts = array('http_status', explode(' ', $entry, 2)[1]);
            
            $headers[trim($parts[0])][] = trim($parts[1]);
        }
        
        $response = substr($response, $info['header_size']);
        
        $code = (int)$info['http_code'];
        if($code >= 400)
            throw new WebDavErrorException($code, $response);
        
        if($this->debug) {
            echo "\n".'### cURL response'."\n";
            foreach($headers as $k => $h)
                foreach($h as $v)
                    echo $k.': '.$v."\n";
            
            echo "\n";
        }
        
        return array(
            'code' => $code,        // integer (HTTP response code)
            'headers' => $headers,  // Always arrays even if only one value
            'body' => $response     // string
        );
    }
    
    /**
     * Transforms SimpleXML tree into associative array (ignores attributes)
     * 
     * @param SimpleXMLElement $xml
     * 
     * @return array
     */
    private static function xml2array($xml) {
        if(!$xml->count())
            return (string)$xml;
        
        $res = array();
        
        foreach($xml->children() as $node)
            $res[$node->getName()][] = self::xml2array($node);
        
        foreach($res as $k => $v)
            if(count($v) == 1)
                $res[$k] = $v[0];
        
        return $res;
    }

    /**
     * Runs a PROPFIND
     *
     * @param string $url
     * @param array $properties
     * @param int $depth 0 or 1 (child resources)
     *
     * @return array
     *
     * @throws WebDavErrorException
     */
    public function propFind($url, $properties, $depth = 0) {
        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom->formatOutput = true;
        $root = $dom->createElementNS('DAV:', 'd:propfind');
        
        $prop = $dom->createElement('d:prop');
        foreach($properties as $property) {
            list($ns, $name) = explode(':', $property, 2);
            if($ns == 'DAV:') {
                $el = $dom->createElement('d:'.$name);
            } else {
                $el = $dom->createElementNS($ns, 'x:'.$name);
            }
            $prop->appendChild($el);
        }
        $dom->appendChild($root)->appendChild($prop);
        
        $body = $dom->saveXML();
        
        $res = $this->call('PROPFIND', $url, $body, array(
            'Content-Type' => 'application/xml',
            'Depth' => $depth,
        ));
        
        $xml = simplexml_load_string($res['body']);
        if(!$xml)
            throw new WebDavErrorException(0, 'Not and XML response');
        
        if(($xml->getName() != 'multistatus') || !in_array('DAV:', $xml->getNamespaces()))
            throw new WebDavErrorException(0, 'Not a DAV:multistatus response');
        
        $result = array();
        foreach($xml->response as $item) {
            $props = null;
            foreach($item->propstat as $propstat) {
                if((string)$propstat->status !== 'HTTP/1.1 200 OK') continue;
                
                $props = self::xml2array($propstat->prop);
            }
            
            if(is_null($props)) continue;
            
            if(!$depth) return $props;
            
            $result[(string)$item->href] = $props;
        }
        
        return $result;
    }

    /**
     * Updates a list of properties on the server
     *
     * The list of properties must have clark-notation properties for the keys,
     * and the actual (string) value for the value. If the value is null, an
     * attempt is made to delete the property.
     *
     * @param string $url
     * @param array $properties
     * @param int $depth
     *
     * @return bool
     *
     * @throws WebDavErrorException
     * @throws WebDavErrorsException
     */
    function propPatch($url, array $properties, $depth = 1) {
        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom->formatOutput = true;
        $root = $dom->createElementNS('DAV:', 'd:proppatch');
        
        $set = $dom->createElement('d:set');
        $prop = $dom->createElement('d:prop');
        foreach($properties as $property => $value) {
            list($ns, $name) = explode(':', $property, 2);
            if($ns == 'DAV:') {
                $el = $dom->createElement('d:'.$name, $value);
            } else {
                $el = $dom->createElementNS($ns, 'x:'.$name, $value);
            }
            $prop->appendChild($el);
        }
        $set->appendChild($prop);
        $dom->appendChild($root)->appendChild($set);
        
        $body = $dom->saveXML();
        
        $res = $this->call('PROPPATCH', $url, $body, array(
            'Content-Type' => 'application/xml',
            'Depth' => $depth,
        ));
        
        $xml = simplexml_load_string($res['body']);
        if(!$xml)
            throw new WebDavErrorException(0, 'Not and XML response');
        
        if(($xml->getName() != 'multistatus') || !in_array('DAV:', $xml->getNamespaces()))
            throw new WebDavErrorException(0, 'Not a DAV:multistatus response');
        
        $errors = array();
        foreach($xml->response as $item) {
            foreach($item->propstat as $propstat) {
                preg_match('^`HTTP/1.1\s+([0-9]+)\s+(.*)`$', (string)$propstat->status, $m);
                
                $code = (int)$m[1];
                if($code < 400) continue;
                
                $errors[(string)$item->href] = array('code' => $code, 'message' => $m[2]);
            }
        }
        
        if(count($errors))
            throw new WebDavErrorsException($errors);
    }
    
    /**
     * Options request
     *
     * This method returns all the features from the 'DAV:' header as an array.
     * If there was no DAV header, or no contents this method will return an
     * empty array.
     *
     * @param string $url
     *
     * @return array
     */
    public function options($url) {
        $res = $this->call('OPTIONS', $url);
        
        if(!array_key_exists('Dav', $res['headers']))
            return array();
        
        return array_unique(array_filter(array_map('trim', explode(',', $res['headers']['Dav'][0]))));
    }
    
    /**
     * Get path head info
     * 
     * @param string $path
     */
    public function head($path) {
        return $this->call('HEAD', $path)['headers'];
    }

    /**
     * Get path
     *
     * @param string $path
     *
     * @return array
     */
    public function get($path) {
        return $this->call('GET', $path);
    }

    /**
     * Send a local file
     *
     * @param string $source
     * @param string $destination
     *
     * @throws Exception
     * @throws WebDavCannotReadFileException
     */
    public function post($source, $destination) {
        if(!is_file($source))
            throw new WebDavCannotReadFileException($source);
        
        $fh = fopen($source, 'r');
        
        try {
            $this->call('POST', $destination, $fh);
            
        } catch(Exception $e) {
            fclose($fh);
            
            throw $e;
        }
        
        fclose($fh);
    }

    /**
     * Send a new version of a local file
     *
     * @param string $source
     * @param string $destination
     *
     * @throws Exception
     * @throws WebDavCannotReadFileException
     */
    public function put($source, $destination) {
        if(!is_file($source))
            throw new WebDavCannotReadFileException($source);
        
        $fh = fopen($source, 'r');
        
        try {
            $this->call('PUT', $destination, $fh);
            
        } catch(Exception $e) {
            fclose($fh);
            
            throw $e;
        }
        
        fclose($fh);
    }
    
    /**
     * Delete path
     * 
     * @param string $path
     */
    public function delete($path) {
        $this->call('DELETE', $path);
    }
    
    /**
     * Delete path
     * 
     * @param string $path
     */
    public function mkcol($path) {
        $this->call('MKCOL', $path);
    }
    
    /**
     * Copy
     * 
     * @param string $source
     * @param string $destination
     * @param boolean $overwrite
     * @param integer $depth
     */
    public function copy($source, $destination, $overwrite = false, $depth = 0) {
        $headers = array('Destination' => preg_match('`^https?://`', $destination) ? $destination : $this->base_url.$destination);
        if(!$overwrite) $headers['Overwrite'] = 'F';
        if($depth) $headers['Depth'] = $depth;
        
        $this->call('COPY', $source, null, $headers);
    }
    
    /**
     * Move
     * 
     * @param string $source
     * @param string $destination
     * @param boolean $overwrite
     * @param integer $depth
     */
    public function move($source, $destination, $overwrite = false, $depth = 0) {
        $headers = array('Destination' => preg_match('`^https?://`', $destination) ? $destination : $this->base_url.$destination);
        if(!$overwrite) $headers['Overwrite'] = 'F';
        if($depth) $headers['Depth'] = $depth;
        
        $this->call('MOVE', $source, null, $headers);
    }
}
