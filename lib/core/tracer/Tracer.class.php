<?php

/**
 * This file is part of the BaseProject project.
 * 2015 
 * Copyright (c) RENATER
 */

// Ticks frequency setting
declare(ticks=1);

/**
 * Statement Tracer for performance analysis purposes
 */
class Tracer implements JsonSerializable {
    /**
     * @var string Target file
     */
    private static $out = '';
    
    /**
     * @var string Context
     */
    private static $context = '';
    
    /**
     * @var array Skipped functions
     */
    private static $skip = array();
    
    /**
     * @var Tracer trace
     */
    private static $trace = null;
    
    /**
     * @var array last
     */
    private static $last = array();
    
    
    /**
     * @var string Called function
     */
    private $call = null;
    
    /**
     * @var array Args
     */
    private $args = null;
    
    /**
     * @var string File
     */
    private $file = null;
    
    /**
     * @var Tracer[] Sub calls
     */
    private $sub = array();
    
    /**
     * @var int Time
     */
    private $time = 0;
    
    /**
     * @var int Memory
     */
    private $memory = 0;
    
    /**
     * @var boolean Done flag
     */
    private $done = false;
    
    
    /**
     * Setup tracing
     * 
     * @param array $config
     */
    public static function setup($config) {
        // Check if path was provided
        if(!array_key_exists('path', $config))
            return;
        
        // Create file name and open it for writing
        self::$out = $config['path'].'/trace_'.date('Ymd_His').'_'.uniqid().'.json';
        
        // Register skipped functions if any
        if(array_key_exists('skip', $config))
            self::$skip = (array)$config['skip'];
        
        // Set context
        self::$context = (php_sapi_name() === 'cli') ? implode(' ', isset($argv) ? $argv : array()) : $_SERVER['REQUEST_METHOD'].' '.$_SERVER['REQUEST_URI'];
        
        // Create root Tracer
        self::$trace = new self(array('function' => 'MAIN', 'args' => array()));
        
        // Register tick handler
        register_tick_function(get_class().'::trace');
        
        // Register shutdown
        register_shutdown_function(get_class().'::save');
    }
    
    /**
     * Get trace entry ID
     * 
     * @param array $trace
     * 
     * @return string
     */
    private static function getTraceID($trace) {
        return substr(md5(print_r($trace, true)), -8);
    }
    
    /**
     * Get whole trace path
     * 
     * @param array $trace
     * 
     * @return array
     */
    private static function getPath($trace) {
        return array_map(get_class().'::getTraceID', $trace);
    }
    
    /**
     * Cast arg to string
     * 
     * @param mixed $arg
     * 
     * @return string
     */
    private static function castArg($arg) {
        if(is_object($arg))
            return method_exists($arg, '__toString') ? (string)$arg : get_class($arg).'{}';
        
        if(is_array($arg))
            return 'array:'.count($arg);
        
        if(is_bool($arg))
            return $arg ? 'true' : 'false';
        
        if(is_null($arg))
            return 'null';
        
        if(is_int($arg) || is_float($arg))
            return $arg;
        
        $arg = str_replace(array('"', "\n"), array('\\"', '\\n'), $arg);
        
        if(strlen($arg) > 128)
            $arg = substr($arg, 0, 124).' ...';
            
        return '"'.$arg.'"';
    }
    
    /**
     * Register trace item
     */
    public static function trace() {
        // Full trace
        $trace = array_reverse(debug_backtrace());
        array_pop($trace); // Self
        
        $c = $trace[count($trace) - 1];
        
        if(array_key_exists('class', $c) && ($c['class'] == get_class()))
            return;
        
        $call = (array_key_exists('class', $c) ? $c['class'] : '').(array_key_exists('type', $c) ? $c['type'] : '').$c['function'];
        
        if(in_array($call, self::$skip))
            return;
        
        $path = self::getPath($trace);
        
        // Same as last path
        if(implode('/', $path) == implode('/', self::$last))
            return;
        
        self::$last = $path;
        
        $t = self::$trace;
        
        while(count($path)) {
            $id = $path[0];
            $s = $t->getSubTrace($id);
            
            if(!$s) break;
            
            $t = $s;
            array_shift($path);
            array_shift($trace);
        }
        
        foreach($t->sub as $s)
            $s->done();
        
        while(count($path)) {
            $s = new self(array_shift($trace));
            $t->addSubTrace(array_shift($path), $s);
            $t = $s;
        }
    }
    
    /**
     * Compute global stats
     * 
     * @return array
     */
    private static function getGlobalStats() {
        $stats = self::$trace->getStats();
        
        foreach($stats as $call => $s) {
            $s['count'] = count($s['time']);
            
            $tt = array_sum($s['time']);
            $s['time'] = array(
                'min' => min($s['time']),
                'max' => max($s['time']),
                'total' => $tt,
                'avg' => $tt / $s['count']
            );
            
            $s['memory'] = array(
                'min' => min($s['memory']),
                'max' => max($s['memory']),
                'avg' => array_sum($s['memory']) / $s['count']
            );
            
            $stats[$call] = $s;
        }
        
        uasort($stats, function($a, $b) {
            $t = $b['time']['total'] - $a['time']['total'];
            
            if($t > 0) return 1;
            if($t < 0) return -1;
            return 0;
        });
        
        return $stats;
    }
    
    /**
     * Save trace Tracer
     */
    public static function save() {
        self::$trace->done();
        
        $stats = self::getGlobalStats();
        
        $data = array(
            'time' => time(),
            'context' => self::$context,
            'trace' => self::$trace,
            'stats' => $stats
        );
        
        $json = json_encode($data, JSON_PRETTY_PRINT);
        
        file_put_contents(self::$out, $json);
        
        $idx_file = dirname(self::$out).'/index.json';
        
        $index = array();
        if(file_exists($idx_file))
            $index = (array)json_decode(file_get_contents($idx_file));
        
        $index[substr(basename(self::$out), 0, -5)] = array(
            'context' => $data['context'],
            'time' => $data['time']
        );
        
        $index = json_encode($index, JSON_PRETTY_PRINT);
        
        file_put_contents($idx_file, $index);
    }

    /**
     * Create Tracer
     *
     * @param array $trace
     */
    private function __construct(array $trace) {
        $this->call = (array_key_exists('class', $trace) ? $trace['class'] : '')
            .(array_key_exists('type', $trace) ? $trace['type'] : '')
            .$trace['function'];
        
        $this->args = array_map(get_class().'::castArg', $trace['args']);
        
        $this->file = array_key_exists('file', $trace) ? $trace['file'].':'.$trace['line'] : null;
        
        $this->time = microtime(true);
        $this->memory = memory_get_usage();
    }

    /**
     * Add sub call
     *
     * @param string $id
     * @param Tracer $trace
     *
     * @throws Exception
     */
    private function addSubTrace($id, $trace) {
        if($this->done)
            throw new Exception('Cannot add sub to closed trace');
        
        $this->sub[$id] = $trace;
    }
    
    /**
     * Get sub trace
     * 
     * @param string $id
     * 
     * @return mixed
     */
    private function getSubTrace($id) {
        return array_key_exists($id, $this->sub) ? $this->sub[$id] : null;
    }
    
    /**
     * Flag trace as done
     */
    private function done() {
        if($this->done) return;
        
        foreach($this->sub as $trace)
            $trace->done();
        
        $this->time = microtime(true) - $this->time;
        $this->memory = memory_get_usage() - $this->memory;
        $this->done = true;
    }
    
    /**
     * Compute call stats
     * 
     * @return array
     */
    private function getStats() {
        $stats = array();
        foreach($this->sub as $trace) {
            foreach($trace->getStats() as $call => $s) {
                
                if(!array_key_exists($call, $stats))
                    $stats[$call] = array('time' => array(), 'memory' => array());
                
                $stats[$call]['time'] = array_merge($stats[$call]['time'], $s['time']);
                $stats[$call]['memory'] = array_merge($stats[$call]['memory'], $s['memory']);
            }
        }
        
        $sub_time = array_sum(array_map(function($c) {
            return $c['time'];
        }, $stats));
        
        $sub_memory = array_sum(array_map(function($c) {
            return $c['time'];
        }, $stats));
        
        $stats[$this->call] = array(
            'time' => array($this->time - $sub_time),
            'memory' => array($this->memory - $sub_memory)
        );
        
        return $stats;
    }
    
    /**
     * Serialize to JSON
     * 
     * @return array
     */
    public function jsonSerialize() {
        return array(
            'call' => $this->call,
            'args' => $this->args,
            'file' => $this->file,
            'time' => $this->time,
            'memory' => $this->memory,
            'sub' => $this->sub
        );
    }

    /**
     * Getter
     *
     * @param string $property
     *
     * @return mixed
     */
    public function __get($property) {
        if(in_array($property, array(
            'call', 'args', 'file', 'time',
            'memory', 'sub', 'done'
        )))
            return $this->$property;
        
        return null;
    }
}
