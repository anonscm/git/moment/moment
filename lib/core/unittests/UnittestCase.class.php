<?php

/**
 * Common unit test case file
 */
abstract class UnittestCase extends PHPUnit_Framework_TestCase {
    
    /**
     * @see parent::setUpBeforeClass()
     */
    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
    }
    
    
    /**
     * Assert $callable throws an exception
     *
     * @param callable $callable Callable to test
     * @param string $expectedException Expected exception
     * @param null $expectedCode Expected code
     * @param null $expectedMessage Expected message
     */
    public function assertException(callable $callable, $expectedException = 'Exception', $expectedCode = null, $expectedMessage = null) {
        $expectedException = ltrim((string) $expectedException, '\\');
        if (!class_exists($expectedException) && !interface_exists($expectedException)) {
            $this->fail(sprintf('An exception of type "%s" does not exist.', $expectedException));
        }
        
        try {
            $callable();
            
        } catch (\Exception $e) {
            $class = get_class($e);
            $message = $e->getMessage();
            $code = $e->getCode();
            
            $errorMessage = 'Failed asserting the class of exception';
            if ($message && $code) {
                $errorMessage .= sprintf(' (message was %s, code was %d)', $message, $code);
            } elseif ($code) {
                $errorMessage .= sprintf(' (code was %d)', $code);
            }
            $errorMessage .= '.';
            $this->assertInstanceOf($expectedException, $e, $errorMessage);
            
            if ($expectedCode !== null) {
                $this->assertEquals($expectedCode, $code, sprintf('Failed asserting code of thrown %s.', $class));
            }
            if ($expectedMessage !== null) {
                $this->assertEquals($expectedMessage, $message, sprintf('Failed asserting the message of thrown %s.', $class));
            }
            return;
        }
        
        $errorMessage = 'Failed asserting that exception';
        if (strtolower($expectedException) !== 'exception') {
            $errorMessage .= sprintf(' of type %s', $expectedException);
        }
        $errorMessage .= ' was thrown.';
        $this->fail($errorMessage);
    }
    
    
    /**
     * Display error on test on stdout
     *
     * @param string $test name of the test
     * @param mixed $reason message or exception
     */
    public function displayError($test, $reason) {
        if(is_object($reason) && ($reason instanceof Exception))
            $reason = $reason->getMessage();
        
        $this->displayMessage(array(
            get_class($this)."::".$test.' : [KO]'."\n",
            'Reason: '.$reason
        ));
    }
    
    /**
     * Display info about a test on stdout
     *
     * @param string $test name of the test
     * @param string $message additional message to show
     */
    public function displayInfo($test, $message) {
        $this->displayMessage(array(
            get_class()."::".$test.' : [OK]'."\n",
            $message ? 'Message: '.$message : null
        ));
    }
    
    /**
     * Print message
     *
     * @param mixed $lines
     */
    private function displayMessage($lines) {
        echo "\n---------------------------------------------------------------------------------------\n";
        echo implode("\n", array_filter((array)$lines));
        echo "\n---------------------------------------------------------------------------------------\n";
    }
}
