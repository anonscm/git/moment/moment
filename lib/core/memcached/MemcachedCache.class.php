<?php

require_once dirname(__FILE__).'/MemcachedCacheExceptions.class.php';

/**
 * Memcached based cache handler
 */
class MemcachedCache {
    /**
     * @var array connexion
     */
    private static $connexion = null;

    /**
     * Setup connextion
     *
     * @param array $config
     *
     * @throws MemcachedCacheConfigurationException
     * @throws MemcachedCacheNotInstalledException
     */
    public static function open(array $config) {
        if(!class_exists('Memcached'))
            throw new MemcachedCacheNotInstalledException();
        
        if(!array_key_exists('servers', $config) || !is_array($config['servers']))
            throw new MemcachedCacheConfigurationException('servers');
        
        if(array_key_exists('persistent_id', $config)) {
            $mc = new Memcached($config['persistent_id']);
            
        } else {
            $mc = new Memcached();
            
            register_shutdown_function(function() {
                self::$connexion->quit();
            });
        }
        
        $mc->setOption(Memcached::OPT_LIBKETAMA_COMPATIBLE, true);
        //$mc->setOption(Memcached::OPT_BINARY_PROTOCOL, true);
        $mc->setOption(Memcached::OPT_COMPRESSION, true);
        
        if(array_key_exists('prefix', $config)) {
            if(!is_string($config['prefix']))
                throw new MemcachedCacheConfigurationException('prefix');
            
            $mc->setOption(Memcached::OPT_PREFIX_KEY, $config['prefix']);
        }
        
        if(!count($mc->getServerList())) { // Do not re-add if persistence
            $servers = array();
            foreach($config['servers'] as $idx => $server) {
                if(!is_string($server))
                    throw new MemcachedCacheConfigurationException('servers['.$idx.']');
                
                if(preg_match('`^(.+):([0-9]+)$`', $server, $m)) {
                    $servers[] = array($m[1], (int)$m[2]);
                    
                } else {
                    $servers[] = array($server, 11211);
                }
            }
            
            $mc->addServers($servers);
        }
        
        /*
        if(array_key_exists('auth', $config)) {
            if(
                !array_key_exists('username', $config['auth'])
                || !is_string($config['auth']['username'])
                || !$config['auth']['username']
            )
                throw new MemcachedCacheConfigurationException('auth[username]');
            
            if(
                !array_key_exists('password', $config['auth'])
                || !is_string($config['auth']['password'])
                || !$config['auth']['password']
            )
                throw new MemcachedCacheConfigurationException('auth[password]');
            
            $mc->setSaslAuthData($config['auth']['username'], $config['auth']['password']);
        }
        */
        
        self::$connexion = $mc;
    }

    /**
     * Check if "path" exists
     *
     * @param array $path
     *
     * @return bool
     *
     * @throws MemcachedCacheNotOpenException
     */
    public static function exists($path) {
        if(!self::$connexion)
            throw new MemcachedCacheNotOpenException();
        
        return (bool)self::$connexion->get(implode('/', $path));
    }

    /**
     * List sub entries
     *
     * @param array $path
     *
     * @return array of paths
     *
     * @throws MemcachedCacheNotOpenException
     */
    public static function index($path) {
        if(!self::$connexion)
            throw new MemcachedCacheNotOpenException();
        
        $key = implode('/', $path).'/';

        $keys = preg_grep('`^'.preg_quote($key, '`').'`', self::$connexion->getAllKeys());

        return array_map(function($key) {
            return explode('/', $key);
        }, $keys);
    }

    /**
     * Get cached data
     *
     * @param array $path
     *
     * @return mixed
     *
     * @throws MemcachedCacheNotOpenException
     */
    public static function get($path) {
        if(!self::$connexion)
            throw new MemcachedCacheNotOpenException();
        
        $data = self::$connexion->get(implode('/', $path));
        if(!$data) return null;
        
        return unserialize($data);
    }

    /**
     * List sub entries whose key matches prefix
     *
     * @param array $path
     *
     * @return array of path and data pairs
     *
     * @throws MemcachedCacheNotOpenException
     */
    public static function match($path) {
        if(!self::$connexion)
            throw new MemcachedCacheNotOpenException();
        
        $key = implode('/', $path).'/';
        $keys = preg_grep('`^'.preg_quote($key, '`').'`', self::$connexion->getAllKeys());
        
        $entries = array();
        foreach($keys as $key)
            $entries[] = array('path' => explode('/',$key), 'data' => self::$connexion->get($key));
        
        return $entries;
    }

    /**
     * Set cache entry
     *
     * @param array $path
     * @param string $data to store
     *
     * @return mixed
     *
     * @throws MemcachedCacheNotOpenException
     */
    public static function set($path, $data) {
        if(!self::$connexion)
            throw new MemcachedCacheNotOpenException();
        
        self::$connexion->set(implode('/', $path), serialize($data));
    }

    /**
     * Drop path
     *
     * @param array $path
     *
     * @throws MemcachedCacheNotOpenException
     */
    public static function drop($path) {
        if(!self::$connexion)
            throw new MemcachedCacheNotOpenException();
        
        $key = implode('/', $path);
        if(substr($key, -1) == '*') {
            $key = substr($key, 0, -1);
            
            $drop = array();
            foreach(self::$connexion->getAllKeys() as $k)
                if(substr($k, 0, strlen($key)) == $key)
                    $drop[] = $k;
            
            if(count($drop))
                self::$connexion->deleteMulti($drop);
            
        } else {
            self::$connexion->delete($key);
        }
    }
}
