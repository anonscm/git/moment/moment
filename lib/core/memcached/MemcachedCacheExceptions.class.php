<?php

/**
 * Generic exceptions
 */
class MemcachedCacheException extends Exception {
    /**
     * Constructor
     * 
     * @param string $message
     * @param array $details
     * @param int $code
     */
    public function __construct($message, $details = array(), $code = 0) {
        $message = 'MemcachedCache exception : '.$message;
        
        if($details) {
            $message = array($message);
            foreach($details as $k => $v)
                $message[] = ($k ? $k.': ' : '').$v;
            
            $message = implode("\n\t", $message);
        }
        
        parent::__construct($message, $code);
    }
}

/**
 * Not installed
 */
class MemcachedCacheNotInstalledException extends MemcachedCacheException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('Memcached not installed');
    }
}

/**
 * Missing or bad configuration
 */
class MemcachedCacheConfigurationException extends MemcachedCacheException {
    /**
     * Constructor
     * 
     * @param string $name
     */
    public function __construct($name) {
        parent::__construct('missing or bad configuration parameter "'.$name.'"');
    }
}

/**
 * Not open
 */
class MemcachedCacheNotOpenException extends MemcachedCacheException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('not open');
    }
}

/**
 * Not implemented
 */
class MemcachedCacheNotImplementedException extends MemcachedCacheException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('not implemented');
    }
}
