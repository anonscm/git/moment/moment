<?php

/**
 * Generic exceptions
 */
class CryptoException extends Exception {
    /**
     * Constructor
     * 
     * @param string $message
     * @param array $details
     * @param int $code
     */
    public function __construct($message, $details = array(), $code = 0) {
        $message = 'Crypto exception : '.$message;
        
        if($details) {
            $message = array($message);
            foreach($details as $k => $v)
                $message[] = ($k ? $k.': ' : '').$v;
            
            $message = implode("\n\t", $message);
        }
        
        parent::__construct($message, $code);
    }
}

/**
 * Not installed
 */
class CryptoNotInstalledException extends CryptoException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('Mcrypt not installed');
    }
}

/**
 * Bad IV size
 */
class CryptoBadIVSizeException extends CryptoException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('bad IV size');
    }
}

/**
 * Decryption failed
 */
class CryptoDecryptionFailedException extends CryptoException {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct('decryption failed, bad password or corrupt data');
    }
}
