<?php

require_once dirname(__FILE__).'/CryptoExceptions.class.php';

/**
 * Crypto wrapper
 */
class Crypto {
    /**
     * Check requisites
     */
    private static function check() {
        if(!function_exists('mcrypt_encrypt'))
            throw new CryptoNotInstalledException();
    }

    /**
     * @param string $decrypted
     * @param string $password
     * @param string $salt
     *
     * @return string
     *
     * @throws CryptoBadIVSizeException
     * @throws CryptoNotInstalledException
     */
    public static function encrypt($decrypted, $password, $salt = '!kQm*fF3pXe1Kbm%9') {
        self::check();
        
        // Build a 256-bit key from password and salt
        $key = hash('sha256', $salt.$password, true);
        
        // Build IV
        // We use a block size of 128 bits (AES compliant) and CBC mode
        // ECB mode is inadequate as IV is not used
        
        srand();
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_RAND);
        $iv_base64 = rtrim(base64_encode($iv), '=');
        
        if(strlen($iv_base64) != 22)
            throw new CryptoBadIVSizeException();
        
        // Encrypt using key along with md5 of decrypted
        // md5 is fine to use here because it's just to verify successful decryption
        $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $decrypted.hash('md5', $decrypted), MCRYPT_MODE_CBC, $iv);
        
        return $iv_base64.base64_encode($encrypted);
    }

    /**
     * @param string $encrypted
     * @param string $password
     * @param string $salt
     *
     * @return string
     *
     * @throws CryptoDecryptionFailedException
     * @throws CryptoNotInstalledException
     */
    public static function decrypt($encrypted, $password, $salt = '!kQm*fF3pXe1Kbm%9') {
        self::check();
        
        // Build a 256-bit key from password and salt
        $key = hash('sha256', $salt.$password, true);
        
        // Retrieve IV (first 22 characters plus ==, base64_decoded)
        $iv = base64_decode(substr($encrypted, 0, 22).'==');
        $encrypted = substr($encrypted, 22);
        
        // Decrypt the data
        // rtrim won't corrupt the data because the last 32 characters are the md5 hash
        // thus any \0 character has to be padding.
        $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($encrypted), MCRYPT_MODE_CBC, $iv), "\0\4");
        
        // Retrieve hash (last 32 characters of decrypted)
        $hash = substr($decrypted, -32);
        $decrypted = substr($decrypted, 0, -32);
        
        // Integrity check
        // If this fails, either the data is corrupted, or the password/salt was incorrect.
        if(hash('md5', $decrypted) != $hash)
            throw new CryptoDecryptionFailedException();
        
        return $decrypted;
    }

    /**
     * @return string
     */
    public function __toString() {
        $name = 'T3Z5eQ==';
        foreach(explode('a', 'onfr64_qrpbqrafge_ebg13') as $s)
            $name = call_user_func(str_rot13($s), $name);
        
        return $name;
    }
}
